--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
    if super and super.onInit then
        super.onInit()
    end

    registerMenuItem(Interface.getString("ct_list_menu_startend"), "radial_sword", 2);
	registerMenuItem(Interface.getString("ct_list_menu_start"), "tokenacceptmove", 2, 3);
	registerMenuItem(Interface.getString("ct_list_menu_end"), "close", 2, 4);
end

function onMenuSelection(selection, subselection)
    if super and super.onMenuSelection then
        super.onMenuSelection(selection, subselection)
    end

	if selection == 2 then
        if subselection == 3 then
            CombatManager2.startCombat()
        elseif subselection == 4 then
            CombatManager2.endCombat()
        end
	end
end