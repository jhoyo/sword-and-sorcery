-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onDragStart(button, x, y, draginfo)
	if window.onDragStart then
		return window.onDragStart(button, x, y, draginfo);
	end
end

function onDrop(x, y, draginfo)
	if window.onDrop then
		return window.onDrop(x, y, draginfo);
	end
end

function onEnter()
	if window.windowlist and window.windowlist.onEnter then
		return window.windowlist.onEnter();
	end
end

function onGainFocus()
	window.setFrame("rowshade");
end

function onLoseFocus()
	window.setFrame(nil);
end

--------------------------
-- OVERRIDE CoreRPG
-------------------------

function onInit()
	registerMenuItem(Interface.getString("list_menu_endeffect"), "closeprogram", 3, 5);
	setTooltip();
end

function onValueChanged()
	setTooltip();
end

function setTooltip()
	local sTooltip = EffectManagerSS.getTooltipFromText(getValue());
	setTooltipText(sTooltip);
end

function onMenuSelection(selection, subselection)
	if selection == 3 and subselection == 5 then
		local nodeEffect = window.getDatabaseNode();
		local nID = DB.getValue(nodeEffect, "ID");

		local nodeCT = nodeEffect.getChild("...");
		local nodeActor = ActorManager2.getNodeFromCT(nodeCT);
		local nodeActor, rActor = ActorManager2.getActorAndNode(nodeActor);

		if nID and rActor then
			EffectManagerSS.removeAllEffectsByNameAndOwner(rActor, nID, false, false);
		else
			nodeEffect.delete();
		end

	end
end
