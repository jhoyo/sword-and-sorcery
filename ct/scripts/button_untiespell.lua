
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local bConc;
local bTied;
local nDif;
local nAdv;

function onInit()
  checkVisibility();
  DB.addHandler(window.getDatabaseNode().getNodeName(), "onChildUpdate", checkVisibility);
end

function onClose()
  DB.removeHandler(window.getDatabaseNode().getNodeName(), "onChildUpdate", checkVisibility);
end

function checkVisibility()
  -- Magic user
  local _, rActor = ActorManager2.getCurrentCharacter(true);
  local bMagic = ActorManager2.isMagicUser(rActor);
  if not bMagic then
    return setVisible(false);
  end

  -- Concentration
  local nodeEffect = window.getDatabaseNode();
  local nLevel = DB.getValue(nodeEffect, "level", -1);
	local nID = DB.getValue(nodeEffect, "ID", -1);
  bConc = (nLevel >= 0) or (nID >= 0);

  -- Tied
  local sExpr = DataCommon.keyword_states["TIED"] .. "(%d+)";
  local sEffect = DB.getValue(nodeEffect, "label", "");
  nDif = sEffect:match(sExpr);
  bTied = nDif and nDif > 0;

  -- Advantage
  nAdv = 0;
  if bConc or bTied then
    if sEffect:find(DataCommon.keyword_inactive["curse"]) > 0 then
      nAdv = -1;
    end
    if sEffect:find(DataCommon.keyword_inactive["hidden"]) > 0 and checkHidden(rActor) then
      nAdv = nAdv - 1;
    end
    if DB.getValue(nodeEffect, 'source_name',"") == rActor['sCTNode'] then
      nAdv = nAdv + 1;
    end

  
  setVisible(bConc or bTied);

  -- Update tooltip
  local sTooltip = "";
  if bTied and nDif then
    sTooltip = Interface.getString("untie_spell") .. ": " .. tostring(nDif);
  end
  if bConc then
    if bTied and nDif then
      sTooltip = sTooltip .. "\nMays+Click: ";
    end
    sTooltip = sTooltip .. Interface.getString("disperse_spell");
  end
  if nAdv < 0 then
    sTooltip = sTooltip .. "\n(" .. Interface.getString("technique_disadvantage") .. tostring(-nAdv) .. ")" ;
  elseif nAdv > 1 then
    sTooltip = sTooltip .. "\n(" .. Interface.getString("effect_checks_advcheck") .. tostring(nAdv) .. ")" ;
  end
  setTooltipText(sTooltip);
end

function checkHidden(rActor)
  local nDist = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DETECTMAGIC"], true, {});
  if nDetect <= 0 then
    return true;
  end

  local sName = DB.getValue(nodeEffect, 'source_name',"");
	if sName == "" then
		sName = DB.getPath(nodeEffect.getChild("..."));
	end
	local nodeTarget = DB.findNode(sName);
  local nDist = getTokenDistance(nodeTarget, ActorManager.getCTNode(rActor));

  if nDist and nDist > 0 then
    return nDist > nDetect;
  else
    return true;
  end

end

function onButtonPress(x, y)
  action();
end

function onDragStart(button, x, y, draginfo)
  action(draginfo);
  return true;
end

function action(draginfo)
  -- Create the action
  local node = window.getDatabaseNode();
  local rActor, rAction;
  if bTied then
    rActor, rAction = EffectManagerSS.getUntiespellAction(node, nDif, nAdv);
  else
    rActor, rAction = EffectManagerSS.getDispellAction(node, nAdv)
  end

  -- Make the roll
  ActionSkill.performRoll(draginfo, rActor, rAction);
end
