--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()

	-- Acquire token reference, if any
	linkToken();

	-- Set up the PC links
	onLinkChanged();
	onFactionChanged();
	onHealthChanged();
	onVigorChanged();

	-- Register the deletion menu item for the host
	registerMenuItem(Interface.getString("list_menu_deleteitem"), "delete", 6);
	registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 6, 7);
end


function updateDisplay()
	local sFaction = friendfoe.getStringValue();

	if DB.getValue(getDatabaseNode(), "active", 0) == 1 then
		name.setFont("sheetlabel");
		nonid_name.setFont("sheetlabel");

		active_spacer_top.setVisible(true);
		active_spacer_bottom.setVisible(true);

		if sFaction == "friend" then
			setFrame("ctentrybox_friend_active");
		elseif sFaction == "neutral" then
			setFrame("ctentrybox_neutral_active");
		elseif sFaction == "foe" then
			setFrame("ctentrybox_foe_active");
		else
			setFrame("ctentrybox_active");
		end
	else
		name.setFont("sheettext");
		nonid_name.setFont("sheettext");

		active_spacer_top.setVisible(false);
		active_spacer_bottom.setVisible(false);

		if sFaction == "friend" then
			setFrame("ctentrybox_friend");
		elseif sFaction == "neutral" then
			setFrame("ctentrybox_neutral");
		elseif sFaction == "foe" then
			setFrame("ctentrybox_foe");
		else
			setFrame("ctentrybox");
		end
	end
end

function linkToken()
	local imageinstance = token.populateFromImageNode(tokenrefnode.getValue(), tokenrefid.getValue());
	if imageinstance then
		TokenManager.linkToken(getDatabaseNode(), imageinstance);
	end
end

function onMenuSelection(selection, subselection)
	if selection == 6 and subselection == 7 then
		delete();
	end
end



function delete()
	CombatManager.deleteCombatant(getDatabaseNode());
end


function onLinkChanged()
	-- If a PC, then set up the links to the char sheet
	local sClass, sRecord = link.getValue();
	linkPCFields();
	onIDChanged();
	if sClass == "charsheet" then
		name.setLine(false);
	end
end

function onHealthChanged()
	local nPercentWounded, sState, sColor = ActorHealthManager2.getPercentWounded(getDatabaseNode(), true);

	vit_wounds.setColor(sColor);
	vit_status.setColor(sColor);
	vit_status.setValue(sState);

end

function onVigorChanged()
	local _, sState, sColor = ActorHealthManager2.getPercentTired(getDatabaseNode(), true);

	vigor_wounds.setColor(sColor);
	vigor_status.setColor(sColor);
	vigor_status.setValue(sState);
end

function onIDChanged()
	local nodeRecord = getDatabaseNode();
	local sClass = DB.getValue(nodeRecord, "link", "", "");
	if sClass == "npc" then
		local bID = LibraryData.getIDState("npc", nodeRecord, true);
		name.setVisible(bID);
		nonid_name.setVisible(not bID);
		isidentified.setVisible(true);
	else
		name.setVisible(true);
		nonid_name.setVisible(false);
		isidentified.setVisible(false);
	end
end

function onFactionChanged()
	-- Update the entry frame
	updateDisplay();

	-- If not a friend, then show visibility toggle
	if friendfoe.getStringValue() == "friend" then
		tokenvis.setVisible(false);
	else
		tokenvis.setVisible(true);
	end
end

function onVisibilityChanged()
	TokenManager.updateVisibility(getDatabaseNode());
	windowlist.onVisibilityToggle();
end

function onActiveChanged(bActive)
	-- setActiveVisible();
	if bActive then
		if windowlist.window.button_global_auxiliar_skills.getValue() > 0 then
			setSkillsVisible(1);
			auxiliar_skills.setValue(1);
		elseif windowlist.window.button_global_auxiliar_inventory.getValue() > 0 then
			setInventoryVisible(1);
			auxiliar_inventory.setValue(1);
		elseif windowlist.window.button_global_auxiliar_actions.getValue() > 0 then
			setActionsVisible(1);
			auxiliar_actions.setValue(1);
		end
	end
	if windowlist.window.button_global_targeting.getValue() > 0 then
		button_section_targets.setValue(bActive and 1 or 0);
	end
	if windowlist.window.button_global_effects.getValue() > 0 then
		button_section_effects.setValue(bActive and 1 or 0);
	end

end

function linkPCFields()
	local nodeChar = link.getTargetDatabaseNode();
	if nodeChar then
		name.setLink(nodeChar.createChild("name", "string"));
		senses.setLink(nodeChar.createChild("senses", "string"), "string");
		size.setLink(nodeChar.createChild("size", "string"), "string");
		is_mounted.setLink(nodeChar.createChild("is_mounted", "number"));

		vit_max.setLink(nodeChar.createChild("health.vit.max", "number"));
		vit_temp.setLink(nodeChar.createChild("health.vit.temp", "number"));
		vit_wounds.setLink(nodeChar.createChild("health.vit.damage", "number"));
		vigor_max.setLink(nodeChar.createChild("health.vigor.max", "number"));
		vigor_temp.setLink(nodeChar.createChild("health.vigor.temp", "number"));
		vigor_wounds.setLink(nodeChar.createChild("health.vigor.damage", "number"));
		essence_wounds.setLink(nodeChar.createChild("health.essence.damage", "number"));
		essence_max.setLink(nodeChar.createChild("health.essence.max", "number"));
		essence_locked.setLink(nodeChar.createChild("encumbrance.essence", "number"));

	end
end

--
-- SECTION VISIBILITY FUNCTIONS
--



function getSectionToggle(sKey)
	local bResult = false;

	local sButtonName = "button_section_" .. sKey;
	local cButton = self[sButtonName];
	if cButton then
		bResult = (cButton.getValue() == 1);
		if (sKey == "active") and self.isActive() and not self.isPC() then
			bResult = true;
		end
	end

	return bResult;
end
function onSectionChanged(sKey)
	local bShow = self.getSectionToggle(sKey);

	local sSectionName = "sub_" .. sKey;
	local cSection = self[sSectionName];
	if cSection then
		if bShow then
			local sSectionClassByRecord = string.format("ct_section_%s_%s", sKey, self.getRecordType());
			if Interface.isWindowClass(sSectionClassByRecord) then
				cSection.setValue(sSectionClassByRecord, getDatabaseNode());
			else
				local sSectionClass = "ct_section_" .. sKey;
				cSection.setValue(sSectionClass, getDatabaseNode());
			end
		else
			cSection.setValue("", "");
		end
		cSection.setVisible(bShow);
	end

	local sSummaryName = "summary_" .. sKey;
	local cSummary = self[sSummaryName];
	if cSummary then
		cSummary.onToggle();
	end
end


function onSectionChanged(sKey)
	local bShow = self.getSectionToggle(sKey);

	local sSectionName = "sub_" .. sKey;
	local cSection = self[sSectionName];
	if cSection then
		if bShow then
			local sSectionClassByRecord = string.format("ct_section_%s_%s", sKey, self.getRecordType());
			if Interface.isWindowClass(sSectionClassByRecord) then
				cSection.setValue(sSectionClassByRecord, getDatabaseNode());
			else
				local sSectionClass = "ct_section_" .. sKey;
				cSection.setValue(sSectionClass, getDatabaseNode());
			end
		else
			cSection.setValue("", "");
		end
		cSection.setVisible(bShow);
	end

	local sSummaryName = "summary_" .. sKey;
	local cSummary = self[sSummaryName];
	if cSummary then
		cSummary.onToggle();
	end
end


function setActionsVisible(v)	
	if v > 0 then
		local nodeChar = link.getTargetDatabaseNode();
		windowlist.window.updateAuxiliarWindow("charsheet_actions", nodeChar);
	else
		windowlist.window.updateAuxiliarWindow();
	end

	windowlist.updateAuxiliarWindowButtons(self, "actions");
end


function setInventoryVisible(v)	
	if v > 0 then
		local nodeChar = link.getTargetDatabaseNode();
		windowlist.window.updateAuxiliarWindow("charsheet_inventory", nodeChar);
	else
		windowlist.window.updateAuxiliarWindow();
	end

	windowlist.updateAuxiliarWindowButtons(self, "inventory");
end

function setSkillsVisible(v)	
	if v > 0 then
		local nodeChar = link.getTargetDatabaseNode();
		windowlist.window.updateAuxiliarWindow("charsheet_skills", nodeChar);
	else
		windowlist.window.updateAuxiliarWindow();
	end

	windowlist.updateAuxiliarWindowButtons(self, "skills");
end

--
--	HELPERS
--

function getRecordType()
	local sClass = link.getValue();
	local sRecordType = LibraryData.getRecordTypeFromDisplayClass(sClass);
	return sRecordType;
end
function isRecordType(s)
	return (self.getRecordType() == s);
end
function isPC()
	return self.isRecordType("charsheet");
end
function isActive()
	return (active.getValue() == 1);
end