--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

slots = {};
local nSpacing = 22;
local nodePower = nil;
local nSizeX = 20;
local nSizeY = nSizeX;
local nOffsetX  = nSizeX/2;

function onHover()
	updateView();
end

function onInit()
	-- Add handlers
	local nodeCT = window.getDatabaseNode();
	if User.isHost() or (nodeCT and nodeCT.isOwner()) then
		DB.addHandler(DB.getPath(nodeCT, "main_actions_used"), "onUpdate", updateView);
		DB.addHandler(DB.getPath(nodeCT, "reserved_actions"), "onUpdate", updateView);
		DB.addHandler(DB.getPath(nodeCT, "fast_actions_used"), "onUpdate", updateView);
		DB.addHandler(DB.getPath(nodeCT, "reactions_used"), "onUpdate", updateView);
	end

	-- Get the view
	updateView();
end

function onCLose()
	-- Remove handlers
	local nodeCT = window.getDatabaseNode();
	if User.isHost() or (nodeCT and nodeCT.isOwner()) then
		DB.removeHandler(DB.getPath(nodeCT, "main_actions_used"), "onUpdate", updateView);
		DB.removeHandler(DB.getPath(nodeCT, "reserved_actions"), "onUpdate", updateView);
		DB.removeHandler(DB.getPath(nodeCT, "fast_actions_used"), "onUpdate", updateView);
		DB.removeHandler(DB.getPath(nodeCT, "reactions_used"), "onUpdate", updateView);
	end
end

function calculateSlots()
	local nodeCT = window.getDatabaseNode();
	local nActions, nFast, nReactions, _, nReserved = CombatManager2.getMaxActions(nodeCT);
	local nUsedActions, nUsedFast, nUsedReactions = CombatManager2.getUsedActions(nodeCT);

	local aActions = {};
	for ind = 1, nUsedActions do
		table.insert(aActions, "action_action_used");
	end
	for ind = nUsedActions + 1, nActions do
		table.insert(aActions, "action_action");
	end
	for ind = 1, nReserved do
		table.insert(aActions, "action_action_reserved");
	end
	for ind = 1, nUsedFast do
		table.insert(aActions, "action_fast_used");
	end
	for ind = nUsedFast + 1, nFast do
		table.insert(aActions, "action_fast");
	end
	for ind = 1, nUsedReactions do
		table.insert(aActions, "action_reaction_used");
	end
	for ind = nUsedReactions + 1, nReactions do
		table.insert(aActions, "action_reaction");
	end
	return aActions;
end

function useAction(sType)
	if sType and type(sType) == "string" then
		if sType == "action_action" then
			CombatManager2.useAction(window.getDatabaseNode(), "action");
			if Input.isControlPressed() or Input.isShiftPressed() then
				CombatManager2.recoverAction(window.getDatabaseNode(), "reserved");
			end
		elseif sType == "action_fast" then
			CombatManager2.useAction(window.getDatabaseNode(), "fast");
		elseif sType == "action_reaction" then
			CombatManager2.useAction(window.getDatabaseNode(), "reaction");
		elseif sType == "action_action_reserved" then
			CombatManager2.useAction(window.getDatabaseNode(), "reserved");
		end
	end
end

function recoverAction(sType)
	if sType and type(sType) == "string" then
		if sType == "action_action_used" then
			CombatManager2.recoverAction(window.getDatabaseNode(), "action");
		elseif sType == "action_fast_used" then
			CombatManager2.recoverAction(window.getDatabaseNode(), "fast");
		elseif sType == "action_reaction_used" then
			CombatManager2.recoverAction(window.getDatabaseNode(), "reaction");
		end
	end
end

function updateView()
	-- Don't act if not required
	local rActor = ActorManager.resolveActor(window.getDatabaseNode());
	local nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
	if User.isHost() or (nodeActor and nodeActor.isOwner()) then
		-- Clear
		for _, v in pairs(slots) do
			v.destroy();
		end
		slots = {}
		local aActions = calculateSlots();

		-- Build the slots
		local nX = 0;
		local widget = nil;

		for k, v in pairs(aActions) do
			widget = addBitmapWidget(v);
			nX = nSpacing * (k - 1) + nOffsetX;
			widget.setPosition("left", nX, 0);
			widget.setSize(nSizeX,  nSizeY)
			slots[k] = widget; 
		end

		-- Determine final width of control based on slots
		setAnchoredWidth(nX + nSizeX);
	end
end

function onClickDown(button, x, y)
	return true;
end

function onClickRelease(button, x, y)
	-- Get the array of actions
	local aActions = calculateSlots();

	-- Get the clicked icon
	local k = math.floor(x / nSpacing) + 1;

	-- Act upon the selected action
	if aActions[k] then
		useAction(aActions[k]);
		recoverAction(aActions[k]);
	end
	-- updateView()

	return true;
end
