--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	super.onInit();
	onHealthChanged();
	onVigorChanged();
end


function onHealthChanged()
	local _, sState, sColor = ActorHealthManager2.getPercentWounded(getDatabaseNode(), true);

	vit_wounds.setColor(sColor);
	vit_status.setColor(sColor);
	vit_status.setValue(sState);
end

function onVigorChanged()
	local _, sState, sColor = ActorHealthManager2.getPercentTired(getDatabaseNode(), true);

	vigor_wounds.setColor(sColor);
	vigor_status.setColor(sColor);
	vigor_status.setValue(sState);
end


function onLinkChanged()
	-- If a PC, then set up the links to the char sheet
	local sClass, sRecord = link.getValue();
	-- if sClass == "charsheet" then
	linkPCFields();
	name.setLine(false);
	-- end
	onIDChanged();
end


function onHover(state)
	updateLinkVisibility();
end

function updateLinkVisibility()
	link_client.updateLinkVisibility();
end

function updateDisplay()
	super.updateDisplay();
	local sOption;
	local rActor = ActorManager.resolveActor(getDatabaseNode());
	local nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
	if nodeActor and nodeActor.isOwner() then
		sOption = "detailed";
	else
		sOption = "status";
	end

	if sOption == "detailed" then
		vit_max.setVisible(true);
		vit_temp.setVisible(true);
		vit_wounds.setVisible(true);
		vit_status.setVisible(false);

		vigor_max.setVisible(true);
		vigor_temp.setVisible(true);
		vigor_wounds.setVisible(true);
		vigor_status.setVisible(false);

		essence_max.setVisible(true);
		essence_locked.setVisible(true);
		essence_wounds.setVisible(true);

		box_actions.setVisible(true);
		actions_vigor.setVisible(true);
	elseif sOption == "status" then
		vit_max.setVisible(false);
		vit_temp.setVisible(false);
		vit_wounds.setVisible(false);
		vit_status.setVisible(true);

		vigor_max.setVisible(false);
		vigor_temp.setVisible(false);
		vigor_wounds.setVisible(false);
		vigor_status.setVisible(true);

		essence_max.setVisible(false);
		essence_locked.setVisible(false);
		essence_wounds.setVisible(false);

		box_actions.setVisible(false);
		actions_vigor.setVisible(false);
	else
		vit_max.setVisible(false);
		vit_temp.setVisible(false);
		vit_wounds.setVisible(false);
		vit_status.setVisible(false);

		vigor_max.setVisible(false);
		vigor_temp.setVisible(false);
		vigor_wounds.setVisible(false);
		vigor_status.setVisible(false);

		essence_max.setVisible(false);
		essence_locked.setVisible(false);
		essence_wounds.setVisible(false);

		box_actions.setVisible(false);
		actions_vigor.setVisible(false);
	end
end

