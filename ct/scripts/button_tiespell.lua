
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
  checkVisibility();
  DB.addHandler(window.getDatabaseNode().getNodeName(), "onChildUpdate", checkVisibility);
end

function onClose()
  DB.removeHandler(window.getDatabaseNode().getNodeName(), "onChildUpdate", checkVisibility);
end

function checkVisibility()
  local bCond = EffectManagerSS.isTiableEffect(window.getDatabaseNode());
  setVisible(bCond);
end

function onButtonPress(x, y)
  action();
end

function onDragStart(button, x, y, draginfo)
  action(draginfo);
  return true;
end

function action(draginfo)
  -- Create the action
  local node = window.getDatabaseNode();
  local rActor, rAction = EffectManagerSS.getTiespellAction(node);

  -- Make the roll
  ActionSkill.performRoll(draginfo, rActor, rAction);
end
