-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
    auxiliar.setVisible(false);
    auxiliar_frame.setVisible(false);

	OptionsManager.registerCallback("Turns", onOptionTurnsChanged);

	onOptionTurnsChanged();
end

function onOptionTurnsChanged()
	button_ct_nextactor_notendturn.setVisible(OptionsManager.isOption("Turns", "several"));
    for _,v in pairs(list.getWindows()) do
        v.box_actions.updateView();
    end
end

function updateAuxiliarWindow(sClass, nodeCT)
    if sClass and nodeCT then
        contentanchor.setAnchor("right", "", "center", "absolute", "0");
        auxiliar.setValue(sClass, nodeCT);
        auxiliar.setVisible(true);
        auxiliar_frame.setVisible(true);
    else
        contentanchor.setAnchor("right", "", "right", "absolute", "-30");
        auxiliar.setValue();
        auxiliar.setVisible(false);
        auxiliar_frame.setVisible(false);
    end
end