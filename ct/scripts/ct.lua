-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local enableglobaltoggle = true;
local enablevisibilitytoggle = true;

function onInit()
	Interface.onHotkeyActivated = onHotkey;

	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);
	
	onVisibilityToggle();
	
	
	local node = getDatabaseNode();
	DB.addHandler(DB.getPath(node, "*.name"), "onUpdate", onNameOrTokenUpdated);
	DB.addHandler(DB.getPath(node, "*.nonid_name"), "onUpdate", onNameOrTokenUpdated);
	DB.addHandler(DB.getPath(node, "*.isidentified"), "onUpdate", onNameOrTokenUpdated);
	DB.addHandler(DB.getPath(node, "*.token"), "onUpdate", onNameOrTokenUpdated);
	
	OptionsManager.registerCallback("WNDC", onOptionWNDCChanged);
end

function onClose()
	local node = getDatabaseNode();
	DB.removeHandler(DB.getPath(node, "*.name"), "onUpdate", onNameOrTokenUpdated);
	DB.removeHandler(DB.getPath(node, "*.nonid_name"), "onUpdate", onNameOrTokenUpdated);
	DB.removeHandler(DB.getPath(node, "*.isidentified"), "onUpdate", onNameOrTokenUpdated);
	DB.removeHandler(DB.getPath(node, "*.token"), "onUpdate", onNameOrTokenUpdated);

	OptionsManager.unregisterCallback("WNDC", onOptionWNDCChanged);
end

function onOptionWNDCChanged()
	for _,v in pairs(getWindows()) do
		v.onHealthChanged();
		v.onVigorChanged();
		v.onVigorChanged();
	end
end

function onNameOrTokenUpdated(vNode)
	for _,w in pairs(getWindows()) do
		w.summary_targets.onTargetsChanged();
		if w.sub_targets.subwindow then
			for _,wTarget in pairs(w.sub_targets.subwindow.targets.getWindows()) do
				wTarget.onRefChanged();
			end
		end
		if w.sub_effects and w.sub_effects.subwindow then
			for _,wEffect in pairs(w.sub_effects.subwindow.effects.getWindows()) do
				wEffect.target_summary.onTargetsChanged();
			end
		end
	end
end

function addEntry(bFocus)
	local w = createWindow();
	if bFocus and w then
		w.name.setFocus();
	end
	return w;
end

function onMenuSelection(selection)
	if selection == 5 then
		addEntry(true);
	end
end

function onSortCompare(w1, w2)
	return CombatManager.onSortCompare(w1.getDatabaseNode(), w2.getDatabaseNode());
end

function onHotkey(draginfo)
	local sDragType = draginfo.getType();
	if sDragType == "combattrackernextactor" then
		CombatManager.nextActor();
		return true;
	elseif sDragType == "combattrackernextround" then
		CombatManager.nextRound(1);
		return true;
	end
end

function toggleVisibility()
	if not enablevisibilitytoggle then
		return;
	end
	
	local visibilityon = window.button_global_visibility.getValue();
	for _,v in pairs(getWindows()) do
		if visibilityon ~= v.tokenvis.getValue() then
			v.tokenvis.setValue(visibilityon);
		end
	end
end

function onVisibilityToggle()
	local anyVisible = 0;
	for _,v in pairs(getWindows()) do
		if (v.friendfoe.getStringValue() ~= "friend") and (v.tokenvis.getValue() == 1) then
			anyVisible = 1;
		end
	end
	
	enablevisibilitytoggle = false;
	window.button_global_visibility.setValue(anyVisible);
	enablevisibilitytoggle = true;
end


function onDrop(x, y, draginfo)
	if draginfo.isType("shortcut") then
		return CampaignDataManager.handleDrop("combattracker", draginfo); 
	end
	
	-- Capture any drops meant for specific CT entries
	local win = getWindowAt(x,y);
	if win then
		local nodeWin = win.getDatabaseNode();
		if nodeWin then
			return CombatDropManager.handleAnyDrop(draginfo, nodeWin.getNodeName())
		end
	end
end

function updateAuxiliarWindowButtons(win, sType)
	for _,w in pairs(getWindows()) do
		local bCond1 = w ~= win;
		if bCond1 or sType ~= "skills" then
			w.auxiliar_skills.setValue(0);
		end
		if bCond1 or sType ~= "inventory" then
			w.auxiliar_inventory.setValue(0);
		end
		if bCond1 or sType ~= "actions" then
			w.auxiliar_actions.setValue(0);
		end
	end
end
function updateAuxiliarWindowButtons(win, sType)
	for _,w in pairs(getWindows()) do
		local bCond1 = w ~= win;
		if bCond1 or sType ~= "skills" then
			w.auxiliar_skills.setValue(0);
		end
		if bCond1 or sType ~= "inventory" then
			w.auxiliar_inventory.setValue(0);
		end
		if bCond1 or sType ~= "actions" then
			w.auxiliar_actions.setValue(0);
		end
	end
end