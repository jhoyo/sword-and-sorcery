--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function addCondition(aCondition, aNames)  

    if aCondition then
        bonus.setValue(aCondition["mod"] or 0);
        tiring.setValue(aCondition["tiring"] or 0);
        description.setValue(aCondition["label"] or "");
        if aCondition["hours_long"] then
            hours_long.setValue(aCondition["hours_long"]);
        end        
        if aCondition["hours_short"] then
            hours_short.setValue(aCondition["hours_short"]);
        end
    end

    if aNames then
        recalculateChars(aNames);
    end
end

function update(bEditMode)
	idelete.setVisible(bEditMode);
end

function recalculateChars(aNames)
    for _,sName in pairs(aNames) do
        local win;
        for _,w in pairs(chars.getWindows()) do
            local sLabel = w.name.getValue();
            if sLabel and sLabel == sName then
                win = w;
            elseif not Utilities.inArray(aNames, sLabel) then
                w.close();
            end
        end
        if not win then
            win = chars.createWindow();
            win.name.setValue(sName);
        end
    end

    local nWindows = chars.getWindowCount();
    local nWidth = 90 * (nWindows) + 5 * (nWindows - 1);
    chars.setAnchoredWidth(tostring(nWidth));
end
