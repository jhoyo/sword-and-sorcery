--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function addData(aItem, sKey, aNames)
    if not aItem then
        return;
    end
    
    title.setValue(aItem["title"] or "");
    label.setValue(sKey or "");
    for _,aCondition in pairs(aItem["conditions"]) do
        subitem.addEntry(aCondition, aNames);
    end
end