--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function action(draginfo)
	-- Get roll action
	local sStat = DB.getValue(DB.findNode("partysheet"), "stat", "");
	local sSkill = DB.getValue(DB.findNode("partysheet"), "skill", "");
	local nDif = DB.getValue(DB.findNode("partysheet"), "dif", 0);
	local nSecret = DB.getValue(DB.findNode("partysheet"), "hiderollresults", 0);
	local nSave = DB.getValue(DB.findNode("partysheet"), "rollsave", 0);
	local rUsers = User.getActiveUsers();

	-- Create message
	local msgOOB = {};
	msgOOB.type = PartyManager2.OOB_MSGTYPE_ASKFORCHECK;
	msgOOB.sStat = sStat;
	msgOOB.sSkill = sSkill;
	msgOOB.nSecret = nSecret;
	msgOOB.nSave = nSave;
	msgOOB.nDif = nDif;

	-- Characters
	for _,v in pairs(window.list.getWindows()) do
		if v.selected.getValue() == 1 then
			local rActor = ActorManager.resolveActor(v.link.getTargetDatabaseNode());
			local nodeActor, _ = ActorManager2.getActorAndNode(rActor);
			local sUser = nodeActor.getOwner();
			-- if sUser and Utilities.inArray(rUsers, sUser) then -- Currently rolls can't be made secret like this
			if nSecret == 0 and sUser and Utilities.inArray(rUsers, sUser) then
				-- Create window info
				local nodeWindow = DB.createChild("ask_for_roll");
				nodeWindow.setPublic(true);
				msgOOB.sPathWindow = nodeWindow.getPath();
				DB.setValue(nodeWindow, "nDif", "number", nDif);
				DB.setValue(nodeWindow, "nSecret", "number", nSecret);
				DB.setValue(nodeWindow, "nSave", "number", nSave);
				DB.setValue(nodeWindow, "sStat", "string", sStat);
				DB.setValue(nodeWindow, "sSkill", "string", sSkill);
				DB.setValue(nodeWindow, "sPath", "string", nodeActor.getPath());
				Comm.deliverOOBMessage(msgOOB, sUser);
			else
				PartyManager2.performRoll(nodeActor, msgOOB)
			end
		end
	end

	-- NPCs
	for _,v in pairs(window.list_npc.getWindows()) do
		if v.selected.getValue() == 1 then
			local rActor = ActorManager.resolveActor(v.link.getTargetDatabaseNode());
			PartyManager2.performRoll(rActor, msgOOB)
		end
	end

end

function action_old(draginfo)
	-- Add characters
	local aParty = {};
	for _,v in pairs(window.list.getWindows()) do
		if v.selected.getValue() == 1 then
			local rActor = ActorManager.resolveActor(v.link.getTargetDatabaseNode());
			if rActor then
				table.insert(aParty, rActor);
			end
		end
	end
	-- Add npcs
	for _,v in pairs(window.list_npc.getWindows()) do
		if v.selected.getValue() == 1 then
			local nodeCT = DB.findNode(v.path.getValue());
			local rActor = ActorManager.resolveActor(nodeCT);
			if rActor then
				table.insert(aParty, rActor);
			end
		end
	end
	if #aParty == 0 then
		return false;
	end

	-- Get roll action
	local sStat = DB.getValue(DB.findNode("partysheet"), "stat", "");
	local sSkill = DB.getValue(DB.findNode("partysheet"), "skill", "");
	local nDif = DB.getValue(DB.findNode("partysheet"), "dif", 0);
	local bSecret = DB.getValue(DB.findNode("partysheet"), "hiderollresults", 0) == 1;
	local bSave = DB.getValue(DB.findNode("partysheet"), "rollsave", 0) == 1;

	local rAction = {};
	rAction.sStat = sStat;
	if nDif ~= 0 then
		rAction.nDif = nDif;
	end
	rAction.bSecret = bSecret;
	if bSave then
		rAction.type = "save";
	else
		rAction.type = "skill";
	end

	-- Iterate through characters
	for _, rActor in pairs(aParty) do
		-- Find node
		if bSave then
			rAction.nodeSkill = ActorManager2.getSaveNode(rActor, sSkill);
		else
			rAction.nodeSkill = ActorManager2.getSkillNode(rActor, sSkill);
		end

		-- Make the roll
		ActionSkill.performRoll(nil, rActor, rAction);
	end



	-- local sAbilityStat = DB.getValue("partysheet.checkselected", ""):lower();
	--
	-- ModifierStack.lock();
	-- for _,v in pairs(aParty) do
	-- 	ActionCheck.performPartySheetRoll(nil, v, sAbilityStat);
	-- end
	-- ModifierStack.unlock(true);

	return true;
end

function onButtonPress()
	return action();
end
