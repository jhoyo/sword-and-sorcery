--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onButtonPress()
    return action();
end

function action(draginfo)
    local sTypeRest = window.resttype.getStringValue();

    -- Get rest time
    local nTime = 0;
    if sTypeRest ~= "menu_reststory" then
        for _,v in pairs(window.restlist.getWindows()) do
            for _,w in pairs(v.subitem.getWindows()) do
                if w.all.getValue() > 0 or w.chars.any() then
                    local nValue = 0;
                    if sTypeRest ~= "menu_restlong" then
                        nValue = w.hours_short.getValue();
                    else
                        nValue = w.hours_long.getValue();
                    end
                    if nValue > nTime then
                        nTime = nValue;
                    end
                end
            end
        end
    end

    -- Rest characters
    for _,v in pairs(window.list.getWindows()) do
        if v.selected.getValue() == 1 then
            local rActor = ActorManager.resolveActor(v.link.getTargetDatabaseNode());
            local sName = v.name.getValue();
            for _,w in pairs(window.totalrest.getWindows()) do
                if w.name.getValue() == sName then
                                
                    local nRest = w.rest.getValue();
                    local nTiring = w.tiring.getValue();

                    ActorManager2.rest(rActor, nRest, nTiring, sTypeRest, nTime);
                end

            end
        end
    end

	-- Pass time
    local nTurns = Utilities.round(nTime * DataCommon.turns_per_unit["hour"]);
    -- EffectManagerSS.passTimeReduceEffectDuration(nTurns);
	CalendarManager.adjustHours(nTime);
    CalendarManager2.outputDateAndTime();
end