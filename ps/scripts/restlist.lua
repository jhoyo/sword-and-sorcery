--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
    if super and super.onInit then
        super.onInit();
    end

    if getDatabaseNode().getChildCount() < 2 then
        createDefault();
    end
end

function createDefault()
    Debug.chat("Reseting")
    DB.deleteChildren(getDatabaseNode());

    local aNames = getCharNames();

    -- Create defaults
    if User.isHost() then
        for sKey,aItem in pairs(DataCommon.rest_conditions) do
            local win;
            for _,w in pairs(getWindows()) do
                local sLabel = w.label.getValue();
                if sLabel and sLabel == sKey then
                    win = w;
                end
            end
            if not win then
                win = createWindow();
            end
            win.addData(aItem, sKey, aNames);
        end
    end

    window.totalrest.calculateTotals(sName);
end

function getCharNames()
    local aNames = {};
    for _,w in pairs(window.list.getWindows()) do
        table.insert(aNames, w.name.getValue());
    end
    return aNames;
end

function update(bEditMode)
    for _,w in pairs(getWindows()) do
        w.subitem.update(bEditMode);
        w.iadd.setVisible(bEditMode);
    end
end

function recalculateChars()
    local aNames = getCharNames();
    for _,w in pairs(getWindows()) do
        w.subitem.recalculateChars(aNames);
    end
    window.totalrest.recalculateChars(aNames);
end