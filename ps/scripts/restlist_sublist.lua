--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function selectOne(win, sName)
    for _,w in pairs(getWindows()) do
        w.all.setValue(0);
        if w.description.getValue() ~= win.description.getValue() then
            for _,ww in pairs(w.chars.getWindows()) do
                if ww.name.getValue() == sName then
                    ww.rest.setValue(0);
                end
            end
        end
    end

    window.windowlist.window.totalrest.calculateTotals(sName);
end


function setAll(win)
    for _,w in pairs(getWindows()) do
        local nValue = 0;
        if w == win then nValue = 1; end
        w.all.setValue(nValue);
        for _,ww in pairs(w.chars.getWindows()) do
            ww.rest.setValue(nValue);
        end
    end

    window.windowlist.window.totalrest.calculateTotals();
end

function addEntry(aCondition, aNames)
    local w = createWindow();
    if not aNames then
        aNames = window.windowlist.getCharNames();
    end
    w.addCondition(aCondition, aNames);
    w.update(not aCondition);
end

function update(bEditMode)
    for _,w in ipairs(getWindows()) do
		w.update(bEditMode);
	end
end

function recalculateChars(aNames)
    for _,w in ipairs(getWindows()) do
        w.recalculateChars(aNames);
    end
end
