function onInit()
  update();
end

function update()
  if Utilities.inArray(DataCommon.abilities_gift, getName()) then
    setVisible(Utilities.userHasMagicSenses() or User.isHost());
  end
end
