--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_ASKFORCHECK = "askforcheck";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_ASKFORCHECK, ask_for_check);
	if User.isHost() then
		local node = DB.createNode("ask_for_roll");
		node.setPublic(true);
		for _, v in pairs(node.getChildren()) do
			v.delete();
		end
	end
end

function ask_for_check(msgOOB)
	local node = DB.findNode(msgOOB.sPathWindow);
	w = Interface.openWindow("ask_for_roll", node);
end

function performRollFromWindow(w)
	msgOOB = {};
	msgOOB.nDif = w.nDif.getValue();
	msgOOB.nSecret = w.nSecret.getValue();
	msgOOB.nSave = w.nSave.getValue();
	msgOOB.sStat = w.sStat.getValue();
	msgOOB.sSkill = w.sSkill.getValue();
	local sPath = w.sPath.getValue();
	local nodeActor = DB.findNode(sPath);
	performRoll(nodeActor, msgOOB);
	w.close();
end

function performRoll(nodeActor, msgOOB)
	-- Get action table
	local rAction = {};
	local nodeActor, rActor = ActorManager2.getActorAndNode(nodeActor);
	rAction.sStat = msgOOB.sStat;
	if nDif ~= 0 then
		rAction.nDif = msgOOB.nDif;
	end
	rAction.nSecret = msgOOB.nSecret;
	if msgOOB.nSave == 1 then
		rAction.type = "save";
		rAction.nodeSkill = ActorManager2.getSaveNode(nodeActor, msgOOB.sSkill);
	else
		rAction.type = "skill";
		rAction.nodeSkill = ActorManager2.getSkillNode(nodeActor, msgOOB.sSkill);
	end
	ActionSkill.performRoll(nil, rActor, rAction);
end


function linkPCFields(nodePS)
	local nodeChar = PartyManager.mapPStoChar(nodePS);

	PartyManager.linkRecordField(nodeChar, nodePS, "name", "string");
	PartyManager.linkRecordField(nodeChar, nodePS, "token", "token", "token");

	PartyManager.linkRecordField(nodeChar, nodePS, "race", "string");

	PartyManager.linkRecordField(nodeChar, nodePS, "health.vit.damage", "number", "vit_wounds");
	PartyManager.linkRecordField(nodeChar, nodePS, "health.vit.max", "number", "vit_total");
	PartyManager.linkRecordField(nodeChar, nodePS, "health.vit.temp", "number", "vit_temp");

	PartyManager.linkRecordField(nodeChar, nodePS, "health.vigor.damage", "number", "vigor_wounds");
	PartyManager.linkRecordField(nodeChar, nodePS, "health.vigor.max", "number", "vigor_total");
	PartyManager.linkRecordField(nodeChar, nodePS, "health.vigor.temp", "number", "vigor_temp");

	for _, v in pairs(DataCommon.abilities) do
		PartyManager.linkRecordField(nodeChar, nodePS, "abilities." .. v .. ".current", "number", v);
	end
end
