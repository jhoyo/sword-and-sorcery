--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
  nodeCTlist = DB.findNode("combattracker.list");
  DB.addHandler(DB.getPath(nodeCTlist), "onChildAdded", update);
  DB.addHandler(DB.getPath(nodeCTlist), "onChildDeleted", update);
  DB.addHandler(DB.getPath(nodeCTlist, "*.name"), "onChildAdded", update);
  DB.addHandler(DB.getPath(nodeCTlist, "*.token"), "onChildAdded", update);

  update();
end

function onClose()
  nodeCTlist = DB.findNode("combattracker.list");
  DB.removeHandler(DB.getPath(nodeCTlist), "onChildAdded", update);
  DB.removeHandler(DB.getPath(nodeCTlist), "onChildDeleted", update);
  DB.removeHandler(DB.getPath(nodeCTlist, "*.name"), "onChildAdded", update);
  DB.removeHandler(DB.getPath(nodeCTlist, "*.token"), "onChildAdded", update);
end


function update()
  destroyAllWindows();

  -- Get the combat tracker list
  nodeCTlist = DB.findNode("combattracker.list");

  -- Add items
  for k, nodeCT in pairs(nodeCTlist.getChildren()) do
    local sLink = DB.getValue(nodeCT, "link", "");
    if sLink == "npc" then
      local sName = DB.getValue(nodeCT, "name", "");
      local sPath = DB.getPath(nodeCT);
      local sToken = DB.getValue(nodeCT, "token", "");
      addEntry(sName, sPath, sToken)
    end
  end

end

function destroyAllWindows()
  for _, v in pairs(getDatabaseNode().getChildren()) do
    v.delete()
  end
end

function addEntry(sName, sPath, sToken)
  local w = createWindow();
  w.name.setValue(sName);
  w.path.setValue(sPath);
  w.token.setValue(sToken);
  w.token.setTooltipText(sName)
end
