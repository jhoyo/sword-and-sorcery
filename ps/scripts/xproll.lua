--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onButtonPress()
    return action();
end

function action(draginfo)
    local nXP = DB.getValue(DB.findNode("partysheet"), "xp", 0);
    local sNode = "exp.session";
    if Input.isShiftPressed() then
        sNode = "exp.story";
    end
    for _,v in pairs(window.list.getWindows()) do
        if v.selected.getValue() == 1 then
            -- Add XP
            local nCharXP = v.XP.getValue();
            local nodeChar = v.link.getTargetDatabaseNode()
            local nTotalXP = DB.getValue(nodeChar, sNode, 0);
            DB.setValue(nodeChar, sNode, "number", nTotalXP + nXP + nCharXP);

            -- Message
            local sMode = "chat_success";
            if nXP + nCharXP <= 0 then
                sMode = "chat_miss";
            end
            local msg = {font = "msgfont", icon = "XP", mode=sMode};
            msg.text = "[" .. tostring(nXP + nCharXP) .. " XP] -> " .. DB.getValue(nodeChar, "name", "");
            local sOwner = nodeChar.getOwner();
            Comm.deliverChatMessage(msg, {sOwner, ""});

        end
    end
end