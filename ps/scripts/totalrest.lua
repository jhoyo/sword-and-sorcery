--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
    calculateTotals();
end

function recalculateChars(aNames)
    for _,sName in pairs(aNames) do
        local win;
        for _,w in pairs(getWindows()) do
            local sLabel = w.name.getValue();
            if sLabel and sLabel == sName then
                win = w;
            elseif not Utilities.inArray(aNames, sLabel) then
                w.close();
            end
        end
        if not win then
            win = createWindow();
            win.name.setValue(sName);
            calculateTotals(sName);
        end
    end

    local aUsedNames = {};
    for _,w in pairs(getWindows()) do
        local sLabel = w.name.getValue();
        if Utilities.inArray(aUsedNames, sLabel) then
            w.close();
        else
            table.insert(aUsedNames, sLabel);
        end
    end

    local nWindows = getWindowCount();
    local nWidth = 90 * (nWindows) + 5 * (nWindows - 1);
    setAnchoredWidth(tostring(nWidth));
end

function calculateTotals(sName)
    if not sName then
        local aChars = window.restlist.getCharNames();
        for _,v in pairs(aChars) do
            calculateTotals(v);
        end

    else
        -- Get global
        local nGlobal = window.global_rest.getValue();
        local nChar = window.list.getRestValue(sName);
        local sTypeRest = window.resttype.getStringValue();
        local aFilter;
        if sTypeRest == "menu_reststory" then
            aFilter = {};
        elseif sTypeRest == "menu_restlong" then
            aFilter = {DataCommon.rest_labels["long"]};
        else
            aFilter = {DataCommon.rest_labels["short"]};
        end

        -- Get individual
        local nInd = 0;
        local nTiring = 0;
        for _,wMain in pairs(window.restlist.getWindows()) do
            for _,wSub in pairs(wMain.subitem.getWindows()) do
                if wSub.all.getValue() == 1 then
                    nInd = nInd + wSub.bonus.getValue();
                    nTiring = nTiring + wSub.tiring.getValue();
                else
                    for _,wChar in pairs(wSub.chars.getWindows()) do
                        if wChar.name.getValue() == sName and wChar.rest.getValue() == 1 then
                            nInd = nInd + wSub.bonus.getValue();
                            nTiring = nTiring + wSub.tiring.getValue();
                        end
                    end
                end
            end
        end

        -- Get Effects and aptitudes
        local nodeChar = ActorManager2.findCharByName(sName);
        local nOther = 0;
        local nPain = 0;
        if nodeChar then
            nOther = ActorManager2.getAptitudeCategory(DataCommon.aptitude["soldier_rest"]);
            local nState = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["REST"]}, true, aFilter);
            nOther = nOther + nState;

            nPain = math.min(0, ActorManager2.getPenalizer(nodeChar, "pain") + nChar);
        end

        -- Save value
        local nTotal = nInd + nGlobal + nOther + nPain;
        nTotal = math.max(DataCommon.rest_limits["min"], math.min(DataCommon.rest_limits["max"], nTotal))
        for _,w in pairs(getWindows()) do
            if w.name.getValue() == sName then
                w.rest.setValue(nTotal);
                w.tiring.setValue(nTiring);
                w.tiring.setVisible(nTiring ~= 0);
            end
        end
    end

end