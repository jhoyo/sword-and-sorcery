function onInit()
  if super and super.onInit() then
    super.onInit();
  end
  local node = getDatabaseNode();
  if not node or node.isReadOnly() then
    self.update(true);
  end
end

function update(bReadOnly, bForceHide)
  local bLocalShow;
  if bForceHide then
    bLocalShow = false;
  else
    bLocalShow = true;
    if bReadOnly and getValue() == 0 then
      bLocalShow = false;
    end
  end

  setReadOnly(bReadOnly);
  setVisible(bLocalShow);

  local sLabel = getName() .. "_label";
  if window[sLabel] then
    window[sLabel].setVisible(bLocalShow);
  end

  return bLocalShow;
end
