--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

sources = {};
hasSources = false;

function sourceUpdate()
	if self.onSourceUpdate then
		self.onSourceUpdate();
	end
end

function calculateSources()
	local n = -99;
	for k, s in pairs(sources) do
		if s then
			n = math.max(n, onSourceValue(s, k));
		end
	end
	return n;
end

function onSourceValue(source, sourcename)
	return source.getValue();
end

function onSourceUpdate(source)
	setValue(calculateSources());
end

function addSource(name, sType)
	if not sType then
		sType = "number";
	end
	local node = window.getDatabaseNode().createChild(name, sType);
	if node then
		sources[name] = node;
		node.onUpdate = sourceUpdate;
		hasSources = true;
	end
end

function onInit()
	if super and super.onInit then
		super.onInit();
	end

	if source and type(source[1]) == "table" then
		for k, v in ipairs(source) do
			if v.name and type(v.name) == "table" then
				if v.string then
					addSource(v.name[1], "string");
				else
					addSource(v.name[1], "number");
				end
			end
		end
	end

	if hasSources then
		sourceUpdate();
	end
end
