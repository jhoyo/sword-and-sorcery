function onLockChanged()
    if header and header.subwindow then
        header.subwindow.update();
    end
    if contents and contents.subwindow then
        contents.subwindow.update();
	elseif main and main.subwindow then
		main.subwindow.update();
    end
    if stats and stats.subwindow then
        stats.subwindow.update();
    end

    if text then
		local bReadOnly = WindowManager.getReadOnlyState(getDatabaseNode());
		text.setReadOnly(bReadOnly);
	elseif notes then
		local bReadOnly = WindowManager.getReadOnlyState(getDatabaseNode());
		notes.setReadOnly(bReadOnly);
	elseif description then
		local bReadOnly = WindowManager.getReadOnlyState(getDatabaseNode());
		description.setReadOnly(bReadOnly);
	end
end

function onIDChanged()
    onLockChanged();
end