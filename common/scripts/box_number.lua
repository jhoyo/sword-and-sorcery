--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--


local widget;

-- function onInit()
-- 	updateView(2)
-- end

function updateView(nValue, sFrame, sTooltip, bNotHideOnZero)
	if widget and type(widget) == "textwidget" then
		widget.destroy()
	end

	if nValue and (bNotHideOnZero or nValue ~= 0) then
		setVisible(true);
		-- widget = addTextWidget("sheetlabel", tonumber(nValue));
		widget = addTextWidget("sheetlabel", nValue);
		widget.setPosition("center", 0, 0)
		widget.setColor("FFFFFFFF");

		if not sFrame then
			sFrame = "fieldblue";
		end
		setFrame(sFrame, 1, 1, 1, 1);

		if sTooltip then
			setTooltipText(sTooltip);
		end
	else
		setVisible(false);
	end
end