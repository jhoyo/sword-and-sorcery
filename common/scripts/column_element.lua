--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--
function update(bReadOnly, bForceHide)
    if super and super.update then
        super.update(bReadOnly, bForceHide);
    end
    if type(bReadOnly) ~= "nil" and type(bReadOnly) ~= "boolean" then
        bReadOnly = false;
    end

    local bLocalShow;
    if bForceHide then
        bLocalShow = false;
    else
        bLocalShow = true;
        if bReadOnly and not nohide and isEmpty() then
            bLocalShow = false;
        end
    end
    
    setReadOnly(bReadOnly);
    setVisible(bLocalShow);
    
    local sLabel = getName() .. "_label";
    if window[sLabel] then
        window[sLabel].setVisible(bLocalShow);
    end
    if separator then
        if window[separator[1]] then
            window[separator[1]].setVisible(bLocalShow);
        end
    end
    
    if self.onVisUpdate then
        self.onVisUpdate(bLocalShow, bReadOnly);
    end
    
    return bLocalShow;
end

function onVisUpdate(bLocalShow, bReadOnly)
    if bReadOnly then
        setFrame(nil);
    else
        setFrame("fielddark", 7,5,7,5);
    end
end