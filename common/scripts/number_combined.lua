--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

atributes = {};
skills = {};
specialties = {};
aptitudes = {};
is_specialty = {};
has_skills = false;
stringNode = nil;

function onInit()
	if super and super.onInit then
		super.onInit();
	end
	addCharacterNode();
  addAtributesAndSkills();
  addAptitudes();
	addHandlers();
  sourceUpdate();
end

function onClose()
	removeHandlers();
end

function getNodeChar()
	local nodeChar;
	if stringNode then
		nodeChar = window.getDatabaseNode().getChild(stringNode);
	else
		nodeChar = window.getDatabaseNode('...');
	end
	return nodeChar;
end

function addCharacterNode()
	if nodecharstring and nodecharstring[1] then
		stringNode = nodecharstring[1];
	end
end

function addHandlers()
  local nodeChar = getNodeChar();
  DB.addHandler(DB.getPath(nodeChar, "level"), "onUpdate", sourceUpdate);
  if penalizer then
    DB.addHandler(DB.getPath(nodeChar, "health.penal.total"), "onUpdate", sourceUpdate);
  end

	if sidehand then
		local nodeSide = ActorManager2.getSkillNode(nodeChar, Interface.getString("skill_value_lefthanded"));
		if nodeSide then
			nodeSide.getChild('prof').onUpdate = sourceUpdate;
			nodeSide.getChild('misc').onUpdate = sourceUpdate;
		end
	end
  if atributecontainer and atributecontainer[1] then
    for _, v in pairs(atributecontainer) do
      DB.addHandler(DB.getPath(nodeChar, v), "onUpdate", addAtributesAndSkills);
    end
  end
  if atributewindowcontainer and atributewindowcontainer[1] then
    for _, v in pairs(atributewindowcontainer) do
      DB.addHandler(DB.getPath(window.getDatabaseNode(), v), "onUpdate", addAtributesAndSkills);
    end
  end
  if skillcontainer and skillcontainer[1] then
    for _, v in pairs(skillcontainer) do
      DB.addHandler(DB.getPath(nodeChar, v), "onUpdate", addAtributesAndSkills);
    end
  end
  if skillwindowcontainer and skillwindowcontainer[1] then
    for _, v in pairs(skillwindowcontainer) do
      DB.addHandler(DB.getPath(window.getDatabaseNode(), v), "onUpdate", addAtributesAndSkills);
    end
  end
end

function removeHandlers()
  local nodeChar = getNodeChar();
  DB.removeHandler(DB.getPath(nodeChar, "level"), "onUpdate", sourceUpdate);
  if penalizer then
    DB.addHandler(DB.getPath(nodeChar, "health.penal.total"), "onUpdate", sourceUpdate);
  end

  if atributecontainer and atributecontainer[1] then
    for _, v in pairs(atributecontainer) do
      DB.removeHandler(DB.getPath(nodeChar, v), "onUpdate", addAtributesAndSkills);
    end
  end
  if atributewindowcontainer and atributewindowcontainer[1] then
    for _, v in pairs(atributewindowcontainer) do
      DB.removeHandler(DB.getPath(window.getDatabaseNode(), v), "onUpdate", addAtributesAndSkills);
    end
  end
  if skillcontainer and skillcontainer[1] then
    for _, v in pairs(skillcontainer) do
      DB.removeHandler(DB.getPath(nodeChar, v), "onUpdate", addAtributesAndSkills);
    end
  end
  if skillwindowcontainer and skillwindowcontainer[1] then
    for _, v in pairs(skillwindowcontainer) do
      DB.removeHandler(DB.getPath(window.getDatabaseNode(), v), "onUpdate", addAtributesAndSkills);
    end
  end
end

function addAptitudes()
  local nodeChar = getNodeChar();
  if aptitude and aptitude[1] then
    local aLists = {"disadvantagelist", "genericlist", "raciallist", "professionlist"};
    for _, v in pairs(aptitude) do
      v = DataCommon.aptitude[v]:lower();
      for _,sList in pairs(aLists) do
        local nodeList = nodeChar.createChild(sList);
        for _,node in pairs(nodeList.getChildren()) do
          if DB.getValue(node, "name", ""):lower():find(v) then
            local nodeFinal = node.getChild("category");
            aptitudes[v] = nodeFinal;
            nodeFinal.onUpdate = sourceUpdate;
          end
        end
      end
    end
  end
end

function addAtribute(name, isContainer, isInWindow)
  local nodeChar = getNodeChar();
  local sName = "";
	if isContainer and isInWindow then
		sName = window.getDatabaseNode().getChild(name).getValue():lower();
  elseif isContainer then
    sName = nodeChar.getChild(name).getValue():lower();
  else
		sName = name:lower();
  end
	sName = DataCommon.abilities_translation[sName];
	local node;
	if sName then
	  local sNameTotal = 'abilities.' .. sName .. '.current';
	  node = nodeChar.getChild(sNameTotal);
	end
  if node then
    atributes[sName] = node;
		node.onUpdate = sourceUpdate;
  end
end

function addSkill(name, isContainer, isInWindow)
  local nodeChar = getNodeChar();
  local sName = "";
	if isContainer and isInWindow then
		sName = window.getDatabaseNode().getChild(name).getValue():lower();
  elseif isContainer then
    sName = nodeChar.getChild(name).getValue():lower();
  else
		sName = name:lower();
  end
  nodeSkill, is_specialty, nodeSpecialty = ActorManager2.getSkillNode(nodeChar, sName)
	if not nodeSkill then
		nodeSkill, is_specialty, nodeSpecialty = ActorManager2.getSkillNode(nodeChar, Interface.getString(sName));
	end
  if nodeSkill then
    skills[sName] = nodeSkill;
		nodeSkill.getChild("prof").onUpdate = sourceUpdate;
    has_skills = true;
    if is_specialty then
      specialties[sName] = nodeSpecialty;
      nodeSpecialty.getChild("misc").onUpdate = sourceUpdate;
		else
			nodeSkill.getChild("misc").onUpdate = sourceUpdate;
    end
  end
end

function addAtributesAndSkills()
  atributes = {};
  if atribute and atribute[1] then
    for _, v in pairs(atribute) do
      addAtribute(v);
    end
  end
  if atributecontainer and atributecontainer[1] then
    for _, v in pairs(atributecontainer) do
      addAtribute(v, true);
    end
  end
  if atributewindowcontainer and atributewindowcontainer[1] then
    for _, v in pairs(atributewindowcontainer) do
      addAtribute(v, true, true);
    end
  end
  skills = {};
  if skill and skill[1] then
    for _, v in pairs(skill) do
      addSkill(v);
    end
  end
  if skillcontainer and skillcontainer[1] then
    for _, v in pairs(skillcontainer) do
      addSkill(v, true);
    end
  end
  if skillwindowcontainer and skillwindowcontainer[1] then
    for _, v in pairs(skillwindowcontainer) do
      addSkill(v, true, true);
    end
  end
  sourceUpdate();
end

function onAtributeValue(source, sourcename)
	return source.getValue();
end

function onSkillValue(source, sourcename, specialty)
  return ActorManager2.getSkillTotal(source, specialty, sidehand);
end

function onAptitudeValue(source, sourcename)
  return source.getValue();
end

function onSourceUpdate(source)
	setValue(calculateSources());
end

function sourceUpdate()
	if self.onSourceUpdate then
		self.onSourceUpdate();
	end
end

function calculateSources()
	local nSk = 0;
	local nAtr = 0;
  local nMultAtr = 0;
  local nMultSk = 0;
  local nPassive = 0;
  local nApt = 0;
  local nPen = 0;
	for name, atr in pairs(atributes) do
		if atributes[name] then
			nAtr = nAtr + self.onAtributeValue(atributes[name], name); 
		end
	end
	for name, sk in pairs(skills) do
		if skills[name] then
			nSk = nSk + self.onSkillValue(skills[name], name); 
		end
	end
	for name, apt in pairs(aptitudes) do
		if apt then
			nApt = nApt + self.onAptitudeValue(apt, name); 
		end
	end
  if atributemultiplier and atributemultiplier[1] then
    nMultAtr = atributemultiplier[1];
  else
    nMultAtr = 1;
  end
  if skillmultiplier and skillmultiplier[1] then
    nMultSk = skillmultiplier[1];
  else
    nMultSk = 1;
  end
  if passive then
    nPassive = DataCommon.passive_base;
  end
  if penalizer then
    nPen = ActorManager2.getPenalizer(getNodeChar());
  end
  return nMultAtr * nAtr + nMultSk * nSk + nPassive + nPen + nApt;
end

function setSkillMultiplier(number)
	if not skillmultiplier then
		skillmultiplier = {};
	end
	skillmultiplier[1] = number;
	setValue(calculateSources());
end

function setAtributelMultiplier(number)
	if not atributemultiplier then
		atributemultiplier = {};
	end
	atributemultiplier[1] = number;
	setValue(calculateSources());
end
