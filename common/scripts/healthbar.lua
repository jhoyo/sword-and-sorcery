--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local ctrlBar = nil;
local sBarFillColor = "006600";

local bHealthBar = false;
local bUsageBar = false;
local bAutomaticText = true;
local sTextPrefix = "";

local sDamageNodePath = nil;
local sMaxNodePath = nil;
local sTempNodePath = nil;

local nTemp = 0;
local nMax = 0;
local nDamage = 0;

local sMode = "vit";

function onInit()
	if fillcolor and fillcolor[1] then
		sBarFillColor = fillcolor[1];
	end
	if source and source[1] then
		local node = window.getDatabaseNode();
		if node and source[1].max and source[1].max[1] and source[1].max[1] ~= "" then
			sMaxNodePath = DB.getPath(node, source[1].max[1]);
			if string.match(source[1].max[1], "vigor") then
				sMode = "vigor";
			end
		end
		if node and source[1].used and source[1].used[1] and source[1].used[1] ~= "" then
			sDamageNodePath = DB.getPath(node, source[1].used[1]);
		end
		if node and source[1].temp and source[1].temp[1] and source[1].temp[1] ~= "" then
			sTempNodePath = DB.getPath(node, source[1].temp[1]);
		end
	end

	if textprefix and textprefix[1] then
		if textprefix[1].text and textprefix[1].text[1] then
			sTextPrefix = textprefix[1].text[1];
		elseif textprefix[1].textres and textprefix[1].textres[1] then
			sTextPrefix = Interface.getString(textprefix[1].textres[1]);
		end
	end
end

function onClose()
	if sMaxNodePath then
		DB.removeHandler(sMaxNodePath, "onUpdate", onMaxChanged);
	end
	if sTempNodePath then
		DB.removeHandler(sTempNodePath, "onUpdate", onTempChanged);
	end
	if sDamageNodePath then
		DB.removeHandler(sDamageNodePath, "onUpdate", onDamageChanged);
	end
end

function onFirstLayout()
	initialize();
end

function initialize()
	if sMaxNodePath then
		setMax(DB.getValue(sMaxNodePath, 0), true);
		DB.addHandler(sMaxNodePath, "onUpdate", onMaxChanged);
	end
	if sTempNodePath then
		setTemp(DB.getValue(sTempNodePath, 0), true);
		DB.addHandler(sTempNodePath, "onUpdate", onTempChanged);
	end
	if sDamageNodePath then
		setDamage(DB.getValue(sDamageNodePath, 0), true);
		DB.addHandler(sDamageNodePath, "onUpdate", onDamageChanged);
	end

	ctrlBar = window.createControl("progressbarfill", getName() .. "_fill");
	update();
end

function setSourceMax(vMax)
	local sNewMaxNodePath = resolveSource(vMax);
	if sNewMaxNodePath == sMaxNodePath then return; end

	if sMaxNodePath then
		DB.removeHandler(sMaxNodePath, "onUpdate", onMaxChanged);
	end
	sMaxNodePath = sNewMaxNodePath;
	if sMaxNodePath then
		DB.addHandler(sMaxNodePath, "onUpdate", onMaxChanged);
	end
	onMaxChanged();
end

function setSourceTemp(vTemp)
	local sNewTempNodePath = resolveSource(vTemp);
	if sNewTempNodePath == sTempNodePath then return; end

	if sTempNodePath then
		DB.removeHandler(sTempNodePath, "onUpdate", onTempChanged);
	end
	if sDamageNodePath then
		DB.removeHandler(sDamageNodePath, "onUpdate", onDamageChanged);
	end
	sTempNodePath = sNewTempNodePath;
	sDamageNodePath = nil;
	bDamageMode = false;
	if sTempNodePath then
		DB.addHandler(sTempNodePath, "onUpdate", onTempChanged);
	end
	onTempChanged();
end

function setSourceDamage(vDamage)
	local sNewDamageNodePath = resolveSource(vDamage);
	if sNewDamageNodePath == sDamageNodePath then return; end

	if sTempNodePath then
		DB.removeHandler(sTempNodePath, "onUpdate", onTempChanged);
	end
	if sDamageNodePath then
		DB.removeHandler(sDamageNodePath, "onUpdate", onDamageChanged);
	end
	sDamageNodePath = sNewDamageNodePath;
	sTempNodePath = nil;
	bDamageMode = true;
	if sDamageNodePath then
		DB.addHandler(sDamageNodePath, "onUpdate", onDamageChanged);
	end
	onDamageChanged();
end

function resolveSource(vSource)
	if type(vSource) == "string" then
		return vSource;
	elseif type(vSource) == "databasenode" then
		return vSource.getPath();
	end
	return nil;
end

function setFillColor(sNewBarFillColor)
	sBarFillColor = sNewBarFillColor;
	update();
end

function setText(sText)
	setTooltipText(sText);
	if ctrlBar then
		ctrlBar.setTooltipText(sText);
	end
end

function setAutoText()
	local sTextFinal = "";
	local sText = "" .. getCurrent() .. " / " .. getMax();
	if (sTextPrefix or "") ~= "" then
		if User.isHost() then
			sTextFinal = sTextPrefix .. ": " .. sText;
		else
			sTextFinal = sTextPrefix;
		end
	end
	setText(sTextFinal);
end

function onTempChanged() setTemp(DB.getValue(sTempNodePath, 0)); end
function onDamageChanged() setDamage(DB.getValue(sDamageNodePath, 0)); end
function onMaxChanged() setMax(DB.getValue(sMaxNodePath, 0)); end

function getTemp() return nTemp; end
function getDamage() return nDamage; end
function getMax() return nMax; end
function getCurrent()
	local nCurrent = math.max(0, getDamage() - getTemp());
	return math.min(getMax(), nCurrent);
end
function getPercent() return 1 - getCurrent()/getMax(); end

function setTemp(nValue, bSkipUpdate)
	local nNewTemp = math.max(nValue, 0);
	if nTemp == nNewTemp then return; end
	nTemp = nNewTemp;
	if self.onValueChanged then
		self.onValueChanged();
	end
	if not bSkipUpdate then
		update();
	end
end

function setDamage(nValue, bSkipUpdate)
	local nNewDamage = math.max(nValue, 0);
	if nDamage == nNewDamage then return; end
	nDamage = nNewDamage;
	if self.onValueChanged then
		self.onValueChanged();
	end
	if not bSkipUpdate then
		update();
	end
end

function setMax(nValue, bSkipUpdate)
	local nNewMax = math.max(nValue, 0);
	if nMax == nNewMax then return; end
	nMax = nNewMax;
	if self.onValueChanged then
		self.onValueChanged();
	end
	if not bSkipUpdate then
		update();
	end
end

function update()
	if sMode == "vit" then
		_, _, sBarFillColor = ActorHealthManager2.getPercentWounded(nil, false, getMax(), getDamage(), getTemp())
	else
		_, _, sBarFillColor = ActorHealthManager2.getPercentTired(nil, false, getMax(), getDamage(), getTemp())
	end

	if not ctrlBar then return; end

	local w,h = getSize();
	if (h < 2) or (w < 2) then
	    ctrlBar.setVisible(false);
	else
	    ctrlBar.setVisible(true);

		local bHorizontal = (h < w);
		local bOffset;
		if reverse and reverse[1] then
			bOffset = bHorizontal;
		else
			bOffset = not bHorizontal;
		end

		local nPercent = getPercent();

		local sName = getName();
		local x,y = getPosition();
		local nFrom, nLen;
		if bHorizontal then
			nLen = math.floor(((w - 2) * nPercent) + 0.5);
			if bOffset then
				nFrom = (w - nLen) - 1;
			else
				nFrom = 1;
			end
			ctrlBar.setAnchor("left", sName, "left", "absolute", nFrom);
			ctrlBar.setAnchor("top", sName, "top", "absolute", 1);
			ctrlBar.setAnchor("right", sName, "right", "absolute", nFrom - (w - nLen));
			ctrlBar.setAnchor("bottom", sName, "bottom", "absolute", -1);
		else
			nLen = math.floor(((h - 2) * nPercent) + 0.5);
			if bOffset then
				nFrom = (h - nLen) - 1;
			else
				nFrom = 1;
			end
			ctrlBar.setAnchor("left", sName, "left", "absolute", 1);
			ctrlBar.setAnchor("top", sName, "top", "absolute", nFrom);
			ctrlBar.setAnchor("right", sName, "right", "absolute", -1);
			ctrlBar.setAnchor("bottom", sName, "bottom", "absolute", nFrom - (h - nLen));
		end

		ctrlBar.setBackColor(sBarFillColor);

		if bHealthBar then
			if Session.IsHost or OptionsManager.isOption("SHPC", "detailed") then
				setAutoText();
			else
				setText("");
			end
		elseif bUsageBar then
			if Session.IsHost or OptionsManager.isOption("SHPC", "detailed") then
				setAutoText();
			else
				setText("");
			end
		elseif bAutomaticText then
			setAutoText();
		end
	end
end
