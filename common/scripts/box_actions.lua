--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local slots = {};
local nSpacing = 21;
local nodePower = nil;
local nSizeX = 20;
local nSizeY = nSizeX;
local nOffsetX  = nSizeX/2-2;

function updateView(aActions, sTooltip)
		
	-- Clear
	for _, v in pairs(slots) do
		v.destroy();
	end
	slots = {};

	-- Build the slots
	local nX = 0;
	local widget = nil;

	for k, v in pairs(aActions) do
		widget = addBitmapWidget(v);
		nX = nSpacing * (k - 1) + nOffsetX;
		widget.setPosition("left", nX, 0);
		widget.setSize(nSizeX,  nSizeY)
		table.insert(slots, widget)
	end

	setTooltipText(sTooltip);

	-- Determine final width of control based on slots
	setAnchoredWidth(nX + nSizeX);
end
