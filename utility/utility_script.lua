--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

------------------
-- USER
------------------

function userHasMagicSenses()
  local sUserUsername = User.getUsername();
  local bMagic = false;
  for k, v in pairs(DB.findNode("charsheet").getChildren()) do
    local sCharUsername = v.getOwner();
    if sUserUsername == sCharUsername then
      bMagic = bMagic or ActorManager2.isMagicUser(v);
    end
  end
  return bMagic;
end

--------------------
-- ARRAYS
--------------------

function shallowcopy(orig)
  local orig_type = type(orig);
  local copy;
  if orig_type == 'table' then
    copy = {};
    for orig_key, orig_value in pairs(orig) do
        copy[orig_key] = orig_value;
    end
  else -- number, string, boolean, etc
    copy = orig;
  end
  return copy;
end

function nTimesInArray(array, value)
	local nVal = 0;
	for _, v in pairs(array) do
		if v == value then
			nVal = nVal + 1;
		end
	end
	return nVal;
end

function inArray(array, value)
  if not array or type(value) == "undefined" then
    return false, nil;
  end
	for k, v in pairs(array) do
		if v == value then
			return true, k;
		end
	end
	return false, nil;
end

function concatenateArrays(array1, array2)
	local n1 = #array1;
	local n2 = #array2;
	local array;
	if n1 == 0 then
		return deepcopy(array2);
	elseif n2 == 0 then
		return deepcopy(array1);
	elseif n1 < n2 then
		for _, v in pairs(array1) do
			table.insert(array2, v);
		end
		return array2;
	else
		for _, v in pairs(array2) do
			table.insert(array1, v);
		end
		return array1;
	end
end

function deepcopy(array)
  if type(array) == "table" then
    local new = {};
    for k, v in pairs(array) do
      if type(k) == "number" or type(k) == "string" then
        new[k] = v;
      else
        table.insert(new, v);
      end
    end
    return new;
  end
  return array;
end

function mergeStrings(array)
  if not array then
    return "";
  end
  local sString = "";
  local nIndex = 1;
  for _,v in pairs(array) do
    if nIndex == 1 then
      v = StringManager.capitalize(v);
    elseif nIndex == #array then
      v = " " .. Interface.getString("and") .. " " .. v;
    else
      v = ", " .. v;
    end
    sString = sString .. v;
    nIndex = nIndex + 1;
  end
  return sString;
end




------------------------------
-- DICE
------------------------------

function clauses_2_string(rAction, bUseOther)
  if not rAction.clauses and bUseOther then
    rAction.clauses ={ {roll = rAction.sRoll, stat = rAction.sAbility, statmult = rAction.nMult, height = rAction.nHeight, dmgtype = rAction.sTypesDamage }};
  end


  local string = "";

  if rAction and #rAction.clauses > 0 then
    local divide_strings = false;
    for _,vClause in ipairs(rAction.clauses) do
      if vClause.roll then -- New clause type
        if divide_strings then
          string = string .. "\n";
        end
        divide_strings = true;

        string = string .. vClause.roll;

        if vClause.stat ~= "" and vClause.statmult ~= 0 then
          if vClause.statmult < 0 then
            string = string .. " (-) ";
          else
            string = string .. " (+) ";
          end
          if vClause.statmult ~= 1 then
            string = string .. tostring(math.abs(vClause.statmult)) .. " x ";
          end
          string = string .. StringManager.capitalize(DataCommon.abilities_translation_inv[vClause.stat]);
        end

        if vClause.height ~= 0 then
          if vClause.height < 0 then
            string = string .. " (-) ";
          else
            string = string .. " (+) ";
          end
          string = string .. tostring(vClause.height) .. " x " .. Interface.getString("power_label_heightening");
        end

        if vClause.dmgtype then
          string = string .. " " .. vClause.dmgtype;
        end


      else -- Old clause type
        local aHealDice = {};
    		local nHealMod = 0;
    		for _,vClause in ipairs(rAction.clauses) do
    			for _,vDie in ipairs(vClause.dice) do
    				table.insert(aHealDice, vDie);
    			end
    			nHealMod = nHealMod + vClause.modifier;
    		end
    		string = StringManager.convertDiceToString(aHealDice, nHealMod);
      end
    end

  end

  return string;
end

function change_dice_cat(aDice, nCat)
  local aNew = {}
  if nCat ~= 0 then
    if aDice == {} then
      aDice = {"d0"};
    end
    for _, vDie in pairs(aDice) do
      nDie = tonumber(vDie:match("%d+"));
      nDie = math.max(0, (nDie or 0) + nCat);
      table.insert(aNew, "d" .. tostring(nDie));
    end
  else
    aNew = aDice;
  end
  return aNew
end

function analyzeDiceString(sString, bArray)
  -- If die array is required, CoreRPG already does most of the work
  if not sString then
    if bArray then
      return {}, 0, "";
    else
      return 0, 0, 0, "";
    end
  end
  sString = sString:lower();
  if bArray then
    local aDice, nMod = DiceManager.convertStringToDice(sString, true);
    local i,j = sString:find(" %D*");
    local sExtra = "";
    if i and j then
      sExtra = sString:sub(i,j);
    end
    return aDice, nMod, sExtra;

  else
    -- If we are looking for category, this is direct
    local sDice, sD, sCat, sSign, sMod, sExtra = sString:match("(%d-)(d?)(%d-)([+-]?)(%d*)) ?(.*)");
    if sD == "" and sMod == "" then
      sMod = sCat;
    end

    local nDice = 0;
    local nCat = 0;
    if sDice ~= "" and sCat ~= "" then
      nDice = tonumber(sDice);
      nCat = tonumber(sCat);
    end

    local nMod = 0;
    if sMod ~= "" then
      if sSign == "-" then
        nMod = -tonumber(sMod);
      else
        nMod = tonumber(sMod);
      end
    end

    return nDice, nCat, nMod, sExtra;
  end
end

function getDiceAndMod(rSource, sRoll, sAbility, nMult, nHeight, nLevel)
  -- Parse roll
  local aDice, nMod = analyzeDiceString(sRoll, true);
  -- Add category
  local nCat = 0;
  if rSource and nMult ~= 0 and inArray(DataCommon.abilities_translation_inv, sAbility) then    
    nCat = nCat + nMult * ActorManager2.getAbility(rSource, sAbility);
  end
  if nCat and nHeight then
    nCat = nCat + nLevel * nHeight;
  end
  aDice = change_dice_cat(aDice, nCat);

  return aDice, nMod;

end

function add_mod_and_height(aDice, sMod, nHeight, nLevel)
  -- Analyze mod string
  local nDice, nCat, nMod = analyzeDiceString(sMod);

  -- Add heightening
  nHeight = nHeight or 0;
  nLevel = nLevel or 0;
  nCat = nCat + nHeight * nLevel;
  if nDice < 1 and nHeight * nLevel > 0 then
    nDice = 1;
  end

  -- Generate bonus string
  local sBonus = "";
  if nDice > 0 and nCat > 0 then
    sBonus = tostring(nDice) .. "d" .. tostring(nCat);
    if nMod > 0 then
      sBonus = sBonus .. "+";
    end
  end
  if nMod ~= 0 then
    sBonus = sBonus .. tostring(nMod);
  end

  -- Add dice to dice array
  if nDice > 0 and nCat > 0 then
    for i=1,nDice do
      table.insert(aDice, "d" .. tostring(nCat));
    end
  end

  return aDice, nMod, sBonus;

end

function addEffectDice(aDice, aAddDice, sExtra)
  if aAddDice and (type(aAddDice) == "table") then
    if not sExtra then
      sExtra = "d";
    end
    if not aDice then
      aDice = {};
    end

    for _,vDie in pairs(aAddDice) do
      if vDie and vDie:sub(1,1) == "-" then
        table.insert(aDice, "-" .. sExtra .. vDie:sub(3));
      else
        table.insert(aDice, sExtra .. vDie:sub(2));
      end
    end
  end
  return aDice
end

function roll_result_2_string(nRoll, nDice, nBonus, nAdv)
  local sString = "";
  local nSum = 0;
  if nRoll then
    sString = tonumber(nRoll);
    nSum = 1;
    if nAdv and nAdv ~= 0 then
      if nAdv > 1 then
        sString = sString .. string.format(" (Vx%d)", nAdv);
      elseif nAdv == 1 then
        sString = sString .. " (V)";
      elseif nAdv == -1 then
        sString = sString .. " (D)";
      else
        sString = sString .. string.format(" (Dx%d)", -nAdv);
      end
    end
  end
  if nDice and nDice ~= 0 then
    nSum = nSum + 1;
    if nDice > 0 then
      sString = sString .. string.format(" + %d", nDice);
    else
      sString = sString .. string.format(" - %d", -nDice);
    end
  end
  if nBonus and nBonus ~= 0 then
    nSum = nSum + 1;
    if nBonus > 0 then
      sString = sString .. string.format(" + %d", nBonus);
    else
      sString = sString .. string.format(" - %d", -nBonus);
    end
  end
  if nSum > 1 then
    nTotal = (nRoll or 0 ) + (nDice or 0 ) + (nBonus or 0 );
    sString = sString .. string.format(" = %d", nTotal);
  end
  return sString;

end

----------------------------------
-- MATH
-----------------------------------
function round(n)
  local nFloor = math.floor(n);
  if n - nFloor < 0.5 then
    return nFloor;
  else
    return nFloor + 1;
  end
end

-------------------------------------
-- TIME
-------------------------------------

function getDurationFromString(sDuration, bDurAndUnits)
  local sTurn = sDuration:match("(%d+) " .. Interface.getString("turn"));
  local sMin = sDuration:match("(%d+) " .. Interface.getString("minute"));
  local sHour = sDuration:match("(%d+) " .. Interface.getString("hour"));
  local sDay = sDuration:match("(%d+) " .. Interface.getString("day"));
  if bDurAndUnits then
    if sTurn then
      return tonumber(sTurn), "";
    elseif sMin then
      return tonumber(sMin), "minute";
    elseif sHour then
      return tonumber(sHour), "hour";
    elseif sDay then
      return tonumber(sDay), "day";
    end
  end
  local nDur = 999999;
  if sTurn then
    nDur = tonumber(sTurn);
  elseif sMin then
    nDur = tonumber(sMin) * DataCommon.turns_per_unit["minute"];
  elseif sHour then
    nDur = tonumber(sHour) * DataCommon.turns_per_unit["hour"];
  elseif sDay then
    nDur = tonumber(sDay) * DataCommon.turns_per_unit["day"];    
  end
  return nDur;
end

-------------------------------------
-- STRINGS
-------------------------------------

function keywordDescription(sKeywords, aDesc)
  if not sKeywords or sKeywords == "" then
    return "";
  end
  sKeywords = sKeywords:lower();
  local sDescription = "";
  local bNewLine = false;
  for sKey, sDesc in pairs(aDesc) do
    local sNew = "";
    if sKey:find("%?%(%%") then
      local X, Y = sKeywords:match(sKey);
      if X then
        if not Y then
          Y = X;
          X = 0;
        end
        sKey = sKey:gsub("%(", "");
        sKey = sKey:gsub("%)", "");
        sKey = sKey:gsub("%+", "");
        sKey = sKey:gsub("%?", "");
        sNew = StringManager.capitalize(string.format(sKey, X, Y)) .. ": " .. string.format(sDesc, X, Y);
      end
    elseif sKey:find("%(") then
      local X = sKeywords:match(sKey);
      if X then
        sKey = sKey:gsub("%(", "");
        sKey = sKey:gsub("%)", "");
        sKey = sKey:gsub("%+", "");
        sKey = sKey:gsub("%?", "");
        sKey = sKey:gsub("-", "");
        sNew = StringManager.capitalize(string.format(sKey, X)) .. ": " .. string.format(sDesc, X);
      end
    else
      if sKeywords:find(sKey) then
        sNew = StringManager.capitalize(sKey) .. ": " .. sDesc;
      end
    end
    if sNew ~= "" then
      if bNewLine then
        sDescription = sDescription .. "\n";
      else
        bNewLine = true;
      end
      sDescription = sDescription .. sNew;
    end
  end
  return sDescription;
end

function number_2_roman(nValue)
  local sValue = "";
  if nValue < 0 then
    sValue = "-";
    nValue = math.abs(nValue);
  elseif nValue >= 4000 then
    return "Too high";
  end

  local nMult = math.floor(nValue / 1000);
  for ind = 1,nMult do
    sValue = sValue .. "M";
  end
  nValue = nValue - nMult * 1000;

  if nValue >= 900 then
    sValue = sValue .. "CM";
    nValue = nValue - 900;
  elseif nValue >= 500 then
    sValue = sValue .. "D";
    nValue = nValue - 500;
  elseif nValue >= 400 then
    sValue = sValue .. "CD";
    nValue = nValue - 400;
  end
  nMult = math.floor(nValue / 100);
  for ind = 1,nMult do
    sValue = sValue .. "C";
  end
  nValue = nValue - nMult * 100;

  if nValue >= 90 then
    sValue = sValue .. "XC";
    nValue = nValue - 90;
  elseif nValue >= 50 then
    sValue = sValue .. "L";
    nValue = nValue - 50;
  elseif nValue >= 40 then
    sValue = sValue .. "XL";
    nValue = nValue - 40;
  end
  nMult = math.floor(nValue / 10);
  for ind = 1,nMult do
    sValue = sValue .. "X";
  end
  nValue = nValue - nMult * 10;

  if nValue >= 9 then
    sValue = sValue .. "IX";
    nValue = nValue - 9;
  elseif nValue >= 5 then
    sValue = sValue .. "V";
    nValue = nValue - 5;
  elseif nValue >= 4 then
    sValue = sValue .. "IV";
    nValue = nValue - 40;
  end
  nMult = math.floor(nValue);
  for ind = 1,nMult do
    sValue = sValue .. "I";
  end

  return sValue;
end

function addModules(aList)
  local aNewList = {};
  local aModules = Module.getModules();
  -- Debug.chat(aModules)
  for _,sItem in pairs(aList) do
    table.insert(aNewList, sItem);
    for _,sMod in pairs(aModules) do 
      table.insert(aNewList, sItem .. "@" .. sMod);
    end
  end
  return aNewList;
end