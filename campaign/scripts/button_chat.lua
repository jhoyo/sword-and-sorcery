function action(draginfo)

  -- Initial variables
	local nodeWin = window.getDatabaseNode();
  local sName = nodeWin.getNodeName();
  local sMessage = "";
  local sSep = "\n----------------------------------------------------"

  -- First case: spell
  if sName:find("spell") or sName:find("powers") then
    -- Heading
    local sType = DB.getText(nodeWin, "reftype_cycler", "");
    local sName = DB.getText(nodeWin, "name", "");
    local sLevel = DB.getText(nodeWin, "level", "");
    sMessage = Interface.getString(sType) .. ": " .. sName;
    if sType == "power_spell" or sType == "power_item_spell" then
      sMessage = sMessage .. " " .. Interface.getString("power_label_level") .. " " .. sLevel;
    end
    sMessage = sMessage .. "\n\n";

    -- Types
    sMessage = sMessage .. Interface.getString("ref_label_source") .. ": " .. DB.getText(nodeWin, "source", "");
    local sAux = DB.getText(nodeWin, "rarity", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("spell_rarity") .. ": " .. Interface.getString(sAux);
    end
    local sAux = DB.getText(nodeWin, "school", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_school") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "elements", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_elements") .. ": " .. sAux;
    end

    -- Mechanics
    sMessage = sMessage .. sSep;
    local sAux = DB.getText(nodeWin, "castingtime", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_castingtime") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "range", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_range") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "vigor_cost", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_vigor_cost") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "essence_cost", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_essence_cost") .. ": " .. sAux;
      local sAux2 = DB.getText(nodeWin, "essence_target", "");
      if sAux2 ~= "" then
        sMessage = sMessage .. " (" .. Interface.getString(sAux2) .. ")";
      end
    end
    local sAux = DB.getText(nodeWin, "objective", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_objective") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "components", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_components") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "duration", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("ref_label_duration") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "keywords", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("spell_keywords") .. ": " .. sAux;
    end

    -- Description
    sMessage = sMessage .. sSep .. "\n" .. DB.getText(nodeWin, "description", "");
    local nodeList = nodeWin.getChild("heightenings");
    if nodeList then
      sMessage = sMessage .. sSep;
      for _, v in pairs(nodeList.getChildren()) do
        local sTag = DB.getText(v, "leveltag", "");
        local sDesc = DB.getText(v, "description", "");
        if sTag ~= "" and sDesc ~= "" then
          sMessage = sMessage .. "\n" .. sTag .. ": " .. sDesc;
        end
      end
    end

  else
    -- Heading
    local sType = DB.getText(nodeWin, "cycler", "");
    local sName = DB.getText(nodeWin, "name", "");
    sMessage = Interface.getString(sType) .. ": " .. sName .. "\n";

    -- Features
    local sAux = DB.getText(nodeWin, "subtype", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("type") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "combattype", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("item_label_subtype") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "level", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("label_level") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "requirements", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("label_requirements") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "extra_exp", "");
    if sAux ~= "" and sAux ~= "0" then
      sMessage = sMessage .. "\n" .. Interface.getString("label_extra_exp") .. ": " .. sAux;
    end
    local sAux = DB.getText(nodeWin, "actions", "");
    if sAux ~= "" then
      sMessage = sMessage .. "\n" .. Interface.getString("spell_actions") .. ": " .. sAux;
    end

    -- Description
    sMessage = sMessage .. sSep .. "\n" .. DB.getText(nodeWin, "text", "");


  end

  -- Send message
	msg = {font = "narratorfont", mode = "spell_chat", secret=Input.isShiftPressed()
  };
	msg.text = sMessage;
	Comm.deliverChatMessage(msg);

	return true;
end

function onDragStart(button, x, y, draginfo)
	return action(draginfo);
end

function onClickRelease(x, y)
	return action();
end

function ORIGINAL()
  local sName1 = DB.getText(nodeWin, "name", 0);
  local sLevel1 = "Level " .. DB.getText(nodeWin, "level", 0) .. " " .. DB.getText(nodeWin, "school", 0);
  local sCastingtime1 = "Casting Time: " .. DB.getText(nodeWin, "castingtime", 0);
  local sRange1 = "Range: " .. DB.getText(nodeWin, "range", 0);
  local sComponents1 = "Components: " .. DB.getText(nodeWin, "components", 0);
  local sDuration1 = "Duration: " .. DB.getText(nodeWin, "duration", 0);
  local sDescription1 = "Description:\n\n" .. DB.getText(nodeWin, "description", 0);
  local sSource1 = "Source: " .. DB.getText(nodeWin, "source", 0);

  local sMessage = "Spell: " .. sName1 .. "\n\n" .. sLevel1 .. "\n-------\n" .. sCastingtime1 .. "\n" .. sRange1 .. "\n" .. sComponents1 .. "\n" .. sDuration1 .. "\n-------\n" .. sDescription1 .. "\n-------\n" .. sSource1 .. "\n\n" ;
end
