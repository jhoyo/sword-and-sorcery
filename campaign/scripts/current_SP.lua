--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

aLists = {"skilllist_basic", "skilllist_learned", "skilllist_specific", "skilllist_weaponary", "skilllist_magic"};

function onInit()
  local nodeChar = getDatabaseNode().getChild('...');

  for _, sList in pairs(aLists) do
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.profession"), "onUpdate", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.prof"), "onUpdate", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.specialty"), "onChildUpdate", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList)), "onChildAdded", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList)), "onChildDeleted", update);
  end

  update();
end

function onClose()
  local nodeChar = getDatabaseNode().getChild('...');

  for _, sList in pairs(aLists) do
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.profession"), "onUpdate", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.prof"), "onUpdate", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.specialty"), "onChildUpdate", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList)), "onChildAdded", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList)), "onChildDeleted", update);
  end
end

function update()
  local nodeChar = getDatabaseNode().getChild('...');

  local nSP = 0;
  local aCostNoProf = DataCommon.SP_cost["no_prof"];
  local aCostNoProfBasic = DataCommon.SP_cost["no_prof_basic"];

  for _, sSkillList in pairs(aLists) do
    local aList = DB.getChild(nodeChar, sSkillList);
    local aCost = DataCommon.SP_cost[sSkillList];
    for _, nodeSkill in pairs(DB.getChildren(aList)) do
      local nMast = DB.getValue(nodeSkill, "prof", 0);
      local nProf = DB.getValue(nodeSkill, "profession", 0);
      if nMast > 0 then
        nSP = nSP + aCost[nMast];
        if nProf == 0 and sSkillList == "skilllist_basic" then
          nSP = nSP + aCostNoProfBasic[nMast];
        elseif nProf == 0 then
          nSP = nSP + aCostNoProf[nMast];
        end
      end
      local nodeSpecList = nodeSkill.createChild("specialty");
      -- for _, nodeSpec in pairs(DB.getChildren(nodeSpecList)) do
      --   local nSpec = DB.getValue(nodeSpec, "is_specialty", 0);
      --   if nSpec > 0 then
      --     nSP = nSP + aCost[5];
      --     if nProf == 0 then
      --       nSP = nSP + aCostNoProf[5];
      --     end
      --   end
      -- end
    end
  end

  setValue(nSP);

end
