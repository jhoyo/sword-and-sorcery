local widget = nil;

function onInit()
	if icons and icons[1] then
		setIcon(icons[1]);
	end
	changeVisibility(false);
end

function setIcon(sIcon)
	if widget then
		widget.destroy();
	end

	if sIcon then
		widget = addBitmapWidget(sIcon);
		widget.setPosition("topleft", 2, 8);
	end
end

function onClickDown()
	return true;
end

function onClickRelease()
	changeVisibility();
	return true;
end

function changeVisibility(bForceVisibility)
	if target and target[1] then
		local aTargets = StringManager.split(target[1], ",", true);
		if bForceVisibility == nil then
			local bVisible = window[aTargets[1]].isVisible();
			bForceVisibility = not bVisible;
		end

		if bForceVisibility then
			setFont("subwindowsmalltitle");
		else
			setFont("subwindowsmalltitle_disabled");
		end
		for _, v in pairs(aTargets) do
			window[v].setVisible(bForceVisibility);
		end
	end

end
