function onInit()
  local nodeChar = window.getDatabaseNode();
  DB.addHandler(DB.getPath(nodeChar, "health.vit.damage"), "onUpdate", update);
  DB.addHandler(DB.getPath(nodeChar, "health.vit.max"), "onUpdate", update);
  update();
end

function update()
  local nodeChar = window.getDatabaseNode();
  local damage = nodeChar.getChild("health.vit.damage").getValue();
  if damage < 0 then
    damage = 0;
  end
  local max = nodeChar.getChild("health.vit.max").getValue();
  local pen = 0;
  if max > 0 then
    local current = damage/max;
    if current >= DataCommon.vitality_thresholds[1] then
      pen = -4;
    elseif current >= DataCommon.vitality_thresholds[2] then
      pen = -2;
    elseif current >= DataCommon.vitality_thresholds[3] then
      pen = -1;
    end
  end
  setValue(pen);
end

function onClose()
  local nodeChar = window.getDatabaseNode();
  DB.removeHandler(DB.getPath(nodeChar, "health.vit.damage"), "onUpdate", update);
  DB.removeHandler(DB.getPath(nodeChar, "health.vit.max"), "onUpdate", update);
end
