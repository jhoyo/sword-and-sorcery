--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);

	-- Start with one empty item if there is none
	constructDefault();
end

function update(bReadOnly)
	for _,w in ipairs(getWindows()) do
		w.idelete.setVisibility(not bReadOnly);
		w.name.setEnabled(not bReadOnly);
	end
end

function addEntry(bFocus)
	local w = createWindow();
	w.idelete.setVisibility(true);
	return w;
end

function onMenuSelection(item)
	if item == 5 then
		addEntry(true);
	end
end

-- Create default skill selection
function constructDefault()
	-- Collect existing entries
	if not getWindows() then
		addEntry(true);
	end
end
