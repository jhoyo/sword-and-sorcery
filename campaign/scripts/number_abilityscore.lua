--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	super.onInit();
	updateCurrent();
	updateTooltip();

	local nodeChar = getDatabaseNode().getChild('....');
	local nodeCT = ActorManager2.getCTNode(nodeChar);
	local sAbility = getName();
	DB.addHandler(DB.getPath(nodeChar, "abilities." .. sAbility ..".damage"), "onUpdate", updateCurrent);

	-- Add handlers with limits
	if window[self.target[1] .. "_weight"] then
		DB.addHandler(DB.getPath(nodeChar, "encumbrance.limits.weight"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_armor"] then
		DB.addHandler(DB.getPath(nodeChar, "encumbrance.limits.armor"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_armor_magic"] then
		DB.addHandler(DB.getPath(nodeChar, "encumbrance.limits.magic"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_armor_natural"] then
		DB.addHandler(DB.getPath(nodeChar, "encumbrance.limits.nonatural"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_helmet"] then
		DB.addHandler(DB.getPath(nodeChar, "encumbrance.limits.helmet"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_shield"] then
		DB.addHandler(DB.getPath(nodeChar, "encumbrance.limits.shield"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_shield_natural"] then
		DB.addHandler(DB.getPath(nodeChar, "encumbrance.limits.shield_nonatural"), "onUpdate", updateCurrent);
	end

	-- Add other handlers
	DB.addHandler(DB.getPath(nodeChar, "level"), "onUpdate", updateTooltip);	
	if nodeCT then
		DB.addHandler(DB.getPath(nodeCT, "effects"), "onChildUpdate", updateCurrent);
		DB.addHandler(DB.getPath(nodeCT, "effects"), "onChildAdded", updateCurrent);
		DB.addHandler(DB.getPath(nodeCT, "effects"), "onChildDeleted", updateCurrent);
	end
	if Utilities.inArray(DataCommon.abilities_gift, sAbility) then
		DB.addHandler(DB.getPath(nodeChar, "protection.magic"), "onUpdate", updateCurrent);
	end
end


function onClose()
	if super and super.onClose then
		super.onClose();
	end

	local nodeChar = getDatabaseNode().getChild('....');
	local nodeCT = ActorManager2.getCTNode(nodeChar);
	local sAbility = getName();
	DB.removeHandler(DB.getPath(nodeChar, "abilities." .. sAbility ..".damage"), "onUpdate", updateCurrent);

	-- Remove handlers with limits
	if window[self.target[1] .. "_weight"] then
		DB.removeHandler(DB.getPath(nodeChar, "encumbrance.limits.weight"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_armor"] then
		DB.removeHandler(DB.getPath(nodeChar, "encumbrance.limits.armor"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_armor_magic"] then
		DB.removeHandler(DB.getPath(nodeChar, "encumbrance.limits.magic"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_armor_natural"] then
		DB.removeHandler(DB.getPath(nodeChar, "encumbrance.limits.nonatural"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_helmet"] then
		DB.removeHandler(DB.getPath(nodeChar, "encumbrance.limits.helmet"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_shield"] then
		DB.removeHandler(DB.getPath(nodeChar, "encumbrance.limits.shield"), "onUpdate", updateCurrent);
	end
	if window[self.target[1] .. "_shield_natural"] then
		DB.removeHandler(DB.getPath(nodeChar, "encumbrance.limits.shield_nonatural"), "onUpdate", updateCurrent);
	end
	if nodeCT then
		DB.removeHandler(DB.getPath(nodeCT, "effects"), "onChildUpdate", updateCurrent);
		DB.removeHandler(DB.getPath(nodeCT, "effects"), "onChildAdded", updateCurrent);
		DB.removeHandler(DB.getPath(nodeCT, "effects"), "onChildDeleted", updateCurrent);
		DB.removeHandler(DB.getPath(nodeChar, "level"), "onUpdate", updateTooltip);
	end
end


function update(bReadOnly)
	setReadOnly(bReadOnly);
end

function updateCurrent()
	local nodeChar = getDatabaseNode().getChild('....');
	local sAbility = getName();
	local nCurrent = ActorManager2.getCurrentAbilityValue(nodeChar, sAbility);
	local nBase = getValue();

	local currentctrl = window[self.target[1] .. "_current"];
	if currentctrl then
		currentctrl.setValue(nCurrent);
	end

	local currentctrl = window[self.target[1] .. "_currenttext"];
	if currentctrl then
		currentctrl.setValue(string.format("%d", nCurrent));
		if nCurrent < nBase then
			currentctrl.setColor(DataCommon.vitality_colors[6]);
		elseif nCurrent > nBase then
			currentctrl.setColor(DataCommon.vitality_colors[1]);
		else
			currentctrl.setColor("000000");
		end
	end
	updateTooltip();
end

function onValueChanged()
	updateCurrent();
end

function onWheel(notches)
	if Input.isShiftPressed() then
		local nodeDamage = getDatabaseNode().getParent().createChild("damage");
		local nValue = nodeDamage.getValue() or 0;
		nValue = math.max(0, nValue + notches);
		nodeDamage.setValue(nValue);
		return true;
	end
end

function action(draginfo)
	local rActor = ActorManager.resolveActor(window.getDatabaseNode());
	-- ActionCheck.performRoll(draginfo, rActor, self.target[1]);
	rAction = {sStat = self.target[1], }
	ActionSkill.performRoll(draginfo, rActor, rAction);
	return true;
end

function onDragStart(button, x, y, draginfo)
	if rollable then
		return action(draginfo);
	end
end

function onDoubleClick(x, y)
	if rollable then
		return action();
	end
end

function updateTooltip()
	-- Cost
	local node = getDatabaseNode();
	local nodeChar = node.getChild("....");

	local nValue = node.getValue();
	local nLevel = DB.getValue(nodeChar, "level", 0);
	local sTooltip = tostring(nValue) .. " -> " .. tostring(nValue+1) .. ": ";
	if nValue < 2 then
		sTooltip = Interface.getString("tooltip_stat_aptitude_prof");
	elseif nValue >= DataCommon.abilities_max_value then
		sTooltip = Interface.getString("tooltip_stat_max_value");
	else
		sTooltip = sTooltip .. tostring(DataCommon.abilities_PA_individual[nValue]) .. " " .. Interface.getString("PA");
		local nMinLevel = DataCommon.abilities_min_level[nValue];
		if nMinLevel > nLevel then
			sTooltip = sTooltip .. string.format(Interface.getString("tooltip_stat_requires_level"), nMinLevel);
		end
	end

	-- Status and damage
	local sStat = node.getParent().getName();
	local _,_, sExtra = ActorManager2.getCurrentAbilityValue(nodeChar, sStat);

	setTooltipText(sTooltip .. sExtra);
end
