--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function adjustCounter(nAdj)
	-- Adjust stored ammo
	local bAdjustAmmo = false;
	if not Input.isShiftPressed() then
		local nAmmo = window.ammocounter.getCurrentValue();
		local nMax = window.ammocounter.getMaxValue();
		if nAdj > 0 and nAmmo < nAdj then
			local msg = {font = "msgfont", icon = "ammo", mode="chat_miss", text=Interface.getString("char_message_rechargewithnoammo")};
			Comm.deliverChatMessage(msg);
			return;
		elseif (nAdj < 0) and (nAmmo >= nMax) then
			-- Nothing to do
		else
			bAdjustAmmo = true;
		end
	end
	if bAdjustAmmo then
		window.ammocounter.adjustCounter(-nAdj);
	end

	-- Spend actions
	local bAgjustActions = true;
	if nAdj > 0 and not Input.isControlPressed() then
		local nodeChar = window.getDatabaseNode().getChild("...");
		local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
		local nodeCT = ActorManager2.getCTNode(nodeChar);
		if nodeCT then
			local nRecharge = window.recharge_time.getValue();
			local aActions = "action";

			-- State for modifying recharge time
			local nState = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["RECHARGE"]}, true, {});
			if nRecharge >= 1 and  nRecharge == -nState then
				nRecharge = 0.5;
			elseif -nState > nRecharge then
				nRecharge = 0;
			else
				nRecharge = nRecharge + nState;
			end

			-- Perform the recharge
			if nRecharge > 0 then
				aActions = {};
				for ind = 1,nRecharge do
					table.insert(aActions, "action");
				end
				-- Fractional recharge means fast action
				if nRecharge - math.floor(nRecharge) > 0 then
					table.insert(aActions, "fast");
				end
			end
			bAgjustActions = CombatManager2.useAction(nodeCT, aActions);
		end
	end
	if bAgjustActions then
		super.adjustCounter(nAdj);
	end
end