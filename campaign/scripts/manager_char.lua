--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	ItemManager.setCustomCharAdd(onCharItemAdd);
	ItemManager.setCustomCharRemove(onCharItemDelete);
	initWeaponIDTracking();
	-- createSavedNPCsNode();
end

function outputUserMessage(sResource, ...)
	local sFormat = Interface.getString(sResource);
	local sMsg = string.format(sFormat, ...);
	ChatManager.SystemMessage(sMsg);
end

-- function createSavedNPCsNode()
-- 	if User.isHost() then
-- 		local node = DB.findNode("npcs_saved");
-- 		if not node then
-- 			local root = DB.getRoot();
-- 			node = root.createChild("npcs_saved")
-- 			node.setPublic(true);
-- 		end
		
-- 		local nodeCharsheet = DB.findNode("charsheet");
-- 		if nodeCharsheet then
-- 			for k, v in pairs(nodeCharsheet.getChildren()) do
-- 				local username = v.getOwner();
-- 				if username ~= "" then
-- 					local newNode = node.createChild(username);
-- 					newNode.setPublic(true);
-- 					DB.setOwner(newNode, username);
-- 				end
-- 			end
-- 		end
-- 	end
-- end

--
-- CLASS MANAGEMENT
--

function sortClasses(a,b)
	return DB.getValue(a, "name", "") < DB.getValue(b, "name", "");
end

function getClassLevelSummary(nodeChar, bShort)
	if not nodeChar then
		return "";
	end

	local aClasses = {};

	local aSorted = {};
	for _,nodeChild in pairs(DB.getChildren(nodeChar, "classes")) do
		table.insert(aSorted, nodeChild);
	end
	table.sort(aSorted, sortClasses);

	for _,nodeChild in pairs(aSorted) do
		local sClass = DB.getValue(nodeChild, "name", "");
		local sSpecialty = DB.getValue(nodeChild, "specialty", "");
		if sSpecialty ~= "" then
			sClass = sClass .. " (" .. sSpecialty .. ")";
		end
		local nLevel = DB.getValue(nodeChild, "level", 0);
		if bShort then
			sClass = sClass:sub(1,3);
		end
		table.insert(aClasses, sClass .. " " .. math.floor(nLevel*100)*0.01);
	end

	local sSummary = table.concat(aClasses, " / ");
	return sSummary;
end

--
-- ITEM/FOCUS MANAGEMENT
--

function onCharItemAdd(nodeItem)
	local sTypeLower = StringManager.trim(DB.getValue(DB.getPath(nodeItem, "type"), ""):lower());
	if StringManager.contains({"mount", "vehicle", "animal" }, sTypeLower) then
		DB.setValue(nodeItem, "carried", "number", 0);
	else
		DB.setValue(nodeItem, "carried", "number", 1);
	end

	addToWeaponDB(nodeItem);
	addToPowerDB(nodeItem);
end

function onCharItemDelete(nodeItem)
	DB.setValue(nodeItem, "carried", "number", 0);
	removeFromWeaponDB(nodeItem);
	removeFromPowerDB(nodeItem);
end

function updateEncumbrance(nodeChar)
	if not nodeChar then
		return;
	end
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);

	local nEncTotal = 0;
	local nEquipSpeed = 0;
	local nEquipSprint = 5;
	local nEquipLimit = 99;
	local nArmorLimit = 99;
	local nArmorMagicLimit = 99;
	local nHelmetLimit = 99;
	local nNonaturalLimit = 99;
	local nShieldLimit = 0;
	local nShieldNonaturalLimit = 0;
	local nEquipInit = 0;
	local nEquipFast = 1;
	local nNoisy = 0;
	local nMetallic = 0;
	local nCritDef = 0;
	-- Add effects of items
	local nCount, nWeight, nEssence;
	for _,vNode in pairs(DB.getChildren(nodeChar, "inventorylist")) do
		-- Weight
		local nCarried = DB.getValue(vNode, "carried", 0);
		if nCarried ~= 0 then
			nCount = DB.getValue(vNode, "count", 0);
			if nCount < 0 then
				nCount = 0;
			end
			nWeight = DB.getValue(vNode, "weight", 0);

			nEncTotal = nEncTotal + (nCount * nWeight);
		end

		-- Other
		if nCarried == 2 then
			local nInit = DB.getValue(vNode, "init", 0);

			-- Armors modify many things
			local bIsArmor, sTypeLower, sSubtypeLower, bIsSummoned = ItemManager2.isArmor(vNode);
			if bIsArmor then
				local nLimit = DB.getValue(vNode, "limit", 99);
				local nSpeed = DataCommon.armor_speed_pen[sSubtypeLower] or DataCommon.armor_speed_pen["no_armor"];
				local nSprint = DataCommon.armor_sprint_mult[sSubtypeLower] or DataCommon.armor_sprint_mult["no_armor"];

				-- Check competence
				local bCond1 = DataCommon.armor_summoned ~= sTypeLower;
				local bCond2 = DataCommon.armor_clothes ~= sSubtypeLower;
				local bCond3 = DataCommon.armor_summoned ~= sSubtypeLower;
				if bCond1 and bCond2 and bCond3 then
					local nodeSkill = ActorManager2.getSkillNode(nodeChar, sSubtypeLower);
					local nMast = DB.getValue(nodeSkill, "prof", 0);
					nLimit = nLimit + DataCommon.armor_mastery_limit[nMast];
					nSprint = nSprint + (DataCommon.armor_mastery_sprint[nMast] or 0);
					nInit = nInit + DataCommon.armor_mastery_init[nMast];
					nCritDef = nCritDef + (DataCommon.armor_mastery_critdefense[nMast] or 0);
				end

				-- Check noisy and helmet
				local sProp = DB.getValue(vNode, "properties", ""):lower();
				if sProp:find(DataCommon.armor_keywords["noisy"]) then
					nNoisy = 1;
				end
				if sProp:find(DataCommon.armor_keywords["metallic"]) then
					nMetallic = 1;
				end

				-- Update values
				nEquipSpeed = math.min(nEquipSpeed, nSpeed);
				nEquipSprint = math.min(nEquipSprint, nSprint);

				-- Update limits
				nArmorLimit = math.min(nArmorLimit, nLimit);
				if not ItemManager2.isNatural(vNode) then
					nNonaturalLimit = math.min(nNonaturalLimit, nLimit);
				end
				if not sProp:find(DataCommon.armor_keywords["helmet"]) then
					nHelmetLimit = math.min(nHelmetLimit, nLimit);
				end
				if not (DataCommon.armor_summoned == sTypeLower or sProp:find(DataCommon.armor_keywords["ether"])) then
					nArmorMagicLimit = math.min(nArmorMagicLimit, nLimit);
				end

			elseif ItemManager2.isShield(vNode) then
				local nBonus = DB.getValue(vNode, "bonus_aux", 0);
				local nodeList = vNode.createChild("attacks");
				local nDefMelee = 0;
				local nDefRanged = 0;
				for _,nodeAttack in pairs(nodeList.getChildren()) do
					local aProps = ItemManager2.scanProperties(DB.getValue(nodeAttack, "properties_attack", ""));
					nDefMelee = math.max(nDefMelee, aProps["defense_melee"]);
					nDefRanged = math.max(nDefRanged, aProps["defense_ranged"]);
				end
				local nMod = math.max(1, math.max(nDefMelee, nDefRanged) + nBonus);
				if not DataCommon.weapon_summoned ~= sTypeLower then
					nShieldLimit = math.max(nShieldLimit, nMod);
				end
				if not ItemManager2.isNatural(vNode) then
					nShieldNonaturalLimit = math.max(nShieldNonaturalLimit, nMod);
				end

			end
			nEquipInit = nEquipInit + math.min(nInit, 0);
		end
	end
	if nEquipSprint < 3 then
		nEquipSprint = 0;
		nEquipFast = 0;
	elseif nEquipSprint > 5 then
		nEquipSprint = 5;
	end
	nEquipSprint = math.min(5, nEquipSprint);
	nArmorLimit = math.max(nArmorLimit, 2);
	nArmorLimit = math.max(nArmorLimit, 2);

	-- Add weight of items
	local nCoin = DB.getValue(nodeChar, "coins.platinum", 0) *  DataCommon.coin_weights[1];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.gold", 0) *  DataCommon.coin_weights[2];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.electrum", 0) *  DataCommon.coin_weights[3];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.silver", 0) *  DataCommon.coin_weights[4];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.copper", 0) *  DataCommon.coin_weights[5];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.slot1.amount", 0) *  DataCommon.coin_weights[1];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.slot2.amount", 0) *  DataCommon.coin_weights[2];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.slot3.amount", 0) *  DataCommon.coin_weights[3];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.slot4.amount", 0) *  DataCommon.coin_weights[4];
	nCoin = nCoin + DB.getValue(nodeChar, "coins.slot5.amount", 0) *  DataCommon.coin_weights[5];

	-- Add weight of character if it is a mount
	local nEncRider = 0;
	local nWeightRider = 0;
	if DB.getValue(nodeChar, "is_mount", 0) > 0 then
		local nodeRider = nodeChar.getParent();
		if DB.getValue(nodeRider, "is_mounted", 0) > 0 then
			nEncRider = DB.getValue(nodeRider, "encumbrance.load", 0);
			nWeightRider = ActorManager2.getWeight(nodeRider);
		end
	end
	nEncTotal = nEncTotal + nCoin + nEncRider + nWeightRider;

	-- If it is a rider, update mount encumbrance
	if DB.getValue(nodeChar, "is_mounted", 0) > 0 then
		local nodeMount = nodeChar.createChild("mount_npc");
		updateEncumbrance(nodeMount);
	end

	-- Get other info
	local nExtraStrength = EffectManagerSS.getEffectsBonus(nodeChar, {DataCommon.keyword_states["CARRYSTR"]}, true, {}) + ActorManager2.getAptitudeCategory(nodeChar, DataCommon.aptitude["hefty"]);
	local nStrength = ActorManager2.getCurrentAbilityValue(nodeChar, "strength");
	nStrength = nStrength + nExtraStrength;
	local sSize = DB.getValue(nodeChar, "size", "medium"):lower();
	local nSize = DataCommon.size_carry_multiplier[sSize] or 1;

	-- Set weight values
	DB.setValue(nodeChar, "encumbrance.load", "number", nEncTotal);
	DB.setValue(nodeChar, "encumbrance.noisy", "number", nNoisy);
	DB.setValue(nodeChar, "encumbrance.metallic", "number", nMetallic);

	local nCat = math.floor(nEncTotal / (nStrength * nSize));
	DB.setValue(nodeChar, "encumbrance.category", "number", nCat);

	local nWeightLimit = 99;
	local nWeightInit = 0;
	if nCat >= 1 and nCat <= 2 then
		nWeightLimit = 9 - nCat;
		nWeightInit = -nCat;
	elseif nCat >= 3 and nCat <= 4  then
		nWeightLimit = 6;
		nWeightInit = -3;
	elseif nCat >= 5 and nCat <= 6  then
		nWeightLimit = 5;
		nWeightInit = -4;
	elseif nCat >= 7 and nCat <= 10  then
		nWeightLimit = 4;
		nWeightInit = -5;
	elseif nCat >= 11 and nCat <= 14  then
		nWeightLimit = 3;
		nWeightInit = -6;
	elseif nCat >= 15 and nCat <= 20  then
		nWeightLimit = 2;
		nWeightInit = -7;
	elseif nCat >= 21  then
		nWeightLimit = 1;
		nWeightInit = -8;
	end

	local nWeightSpeed = 0;
	if nCat > 10 then
		nWeightSpeed = -2;
	elseif nCat > 4 then
		nWeightSpeed = -1;
	end

	local nWeightSprint = 5;
	if nCat > 3 then
		nWeightSprint = 0
	elseif nCat > 1 then
		nWeightSprint = 3;
	elseif nCat == 1 then
		nWeightSprint = 4;
	end

	local nWeightRun = 1;
	local sRun = Interface.getString("yes");
	if nCat > 7 then
		nWeightRun = 0;
		sRun = Interface.getString("no");
	end
	
	-- Inventory totals
	local nInitInv = math.min(nWeightInit + nEquipInit + math.floor(nStrength / 2), 0);
	DB.setValue(nodeChar, "encumbrance.init_pen", "number", nInitInv);	
	local nSpeedInv = math.min(nWeightSpeed, nEquipSpeed);
	DB.setValue(nodeChar, "encumbrance.vel_pen", "number", nSpeedInv);
	local nAux = math.min(nEquipSprint, nWeightSprint);
	DB.setValue(nodeChar, "encumbrance.sprint", "string", "x" .. tostring(nAux));
	DB.setValue(nodeChar, "encumbrance.run", "string", sRun);

	-- Limit totals
	DB.setValue(nodeChar, "encumbrance.limits.weight", "number", nWeightLimit);
	DB.setValue(nodeChar, "encumbrance.limits.armor", "number", nArmorLimit);
	DB.setValue(nodeChar, "encumbrance.limits.nonatural", "number", nNonaturalLimit);
	DB.setValue(nodeChar, "encumbrance.limits.magic", "number", nArmorMagicLimit);
	DB.setValue(nodeChar, "encumbrance.limits.helmet", "number", nHelmetLimit);
	DB.setValue(nodeChar, "encumbrance.limits.shield", "number", -nShieldLimit);
	DB.setValue(nodeChar, "encumbrance.limits.shield_nonatural", "number", -nShieldNonaturalLimit);

	-- Speed totals	
	calculateSpeed(nodeChar);

	-- Initiative window
	calculateInitiative(nodeChar);

	-- Critical defense
	DB.setValue(nodeChar, "defense.critical", "number", nCritDef);


end

function calculateSpeed(nodeChar)
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);

	-- Penalizer
	local nPen = ActorManager2.getPenalizer(nodeChar);
	local nPenSprint = 5;
	local nPenRun = 1;
	local nPenWalk = 1;
	local nPenFast = 1;
	local nPenSpeed = 0;
	local nPenSpeed = 0;
	if nPen <= -3 then
		nPenSprint = 0;
		nPenFast = 0;
	end
	if nPen <= -6 then
		nPenSpeed = -2;
	elseif nPen <= -4 then
		nPenSpeed = -1;
	end
	if nPen <= -6 then
		nPenSpeed = -2;
	elseif nPen <= -4 then
		nPenSpeed = -1;
	end
	if nPen <= -5 then
		nPenRun = 0;
	end
	if nPen <= -7 then
		nPenWalk = 0;
	end
	if nPen == -1 then
		nPenSprint = 4;
	elseif nPen == -2 then
		nPenSprint = 3;
	end

	-- States
	local nEffectVel, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["VEL"]}, true);

	local bNoSprint = EffectManagerSS.hasAnyEffectCondition(rActor, {DataCommon.conditions["No-sprint"], DataCommon.conditions["Confused"], DataCommon.conditions["Stupified"], DataCommon.conditions["Stuned"] }) or ActorManager2.hasAptitude(rActor, DataCommon.aptitude["slow"]);

	local bNoRun = EffectManagerSS.hasAnyEffectCondition(rActor, {DataCommon.conditions["No-run"], DataCommon.conditions["Distracted"], DataCommon.conditions["Fascinated"], DataCommon.conditions["Dizzy"], DataCommon.conditions["Unwilled"] }) or ActorManager2.hasAptitude(rActor, DataCommon.aptitude["very_slow"]);
	
	local bNoWalk = EffectManagerSS.hasAnyEffectCondition(rActor, {DataCommon.conditions["No-walk"], DataCommon.conditions["Prone"] });

	local bNoMove = EffectManagerSS.hasAnyEffectCondition(rActor, {DataCommon.conditions["Paralyzed"], DataCommon.conditions["Petrified"], DataCommon.conditions["Unconscious"], DataCommon.conditions["Asleep"], DataCommon.conditions["Entangled"] });
	
	-- Get values
	local nSpeedBase = DB.getValue(nodeChar, "speed.base", 5);
	local nSpeedMisc = DB.getValue(nodeChar, "speed.misc", 0);
	local nSpeedInv = DB.getValue(nodeChar, "encumbrance.vel_pen", 0);
	local sSprint = DB.getValue(nodeChar, "encumbrance.sprint", "x5");
	local nSprintInv = tonumber(string.sub(sSprint, 2));
	local nSpeedInv = DB.getValue(nodeChar, "encumbrance.vel_pen", 0);
	local sRun = DB.getValue(nodeChar, "encumbrance.run", Interface.getString("yes")):lower();
	local sSize = DB.getValue(nodeChar, "size", "medium"):lower();
	local nSprintSizeMult = DataCommon.size_sprint_multiplier[sSize] or 1;
	local nSRunSizeMult = DataCommon.size_run_multiplier[sSize] or 1;
	local nWeightRun = 0;
	if sRun == Interface.getString("yes"):lower() then
		nWeightRun = 1;
	end
	local nFastInv = 1;
	if nSprintInv == 0 then
		nFastInv = 0;
	end

	-- Cuadruped
	local nRunQuad = 1;
	local nSprintQuad = 1;
	if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["quadruped"]) then
		nRunQuad = DataCommon.quadruped_run_mult;
		nSprintQuad = DataCommon.quadruped_sprint_mult;
	end

	-- Aptitudes
	local nApt = ActorManager2.getAptitudeCategory(nodeChar, DataCommon.aptitude["light_feet"]);
	DB.setValue(nodeChar, "speed.apt", "number", nApt);

	-- Totals
	local nSpeed = nSpeedBase + nSpeedMisc + nEffectVel + math.min(nSpeedInv, nPenSpeed) + nApt;
	DB.setValue(nodeChar, "speed.total", "number", nSpeed - nEffectVel);

	local nAux = math.min(nPenFast, nFastInv);
	if bNoSprint or bNoRun or bNoWalk or bNoMove then
		nAux = 0;
	end
	DB.setValue(nodeChar, "speed.fast", "number", nAux);
	
	local nAux = math.min(nPenSprint, nSprintInv);
	if bNoSprint or bNoRun or bNoWalk or bNoMove then
		nAux = 0;
	end
	DB.setValue(nodeChar, "speed.sprint", "number", nAux * nSpeed * nSprintSizeMult * nSprintQuad);
	
	local nAux = math.min(nPenRun, nWeightRun);
	if bNoRun or bNoWalk or bNoMove then
		nAux = 0;
	end
	DB.setValue(nodeChar, "speed.run", "number", nAux * nSpeed * nSRunSizeMult * nRunQuad);
	
	local nAux = nPenWalk;
	if bNoWalk or bNoMove then
		nAux = 0;
	end
	DB.setValue(nodeChar, "speed.walk", "number", nAux * nSpeed / 2);
	
	local nAux = 1;
	if bNoMove then
		nAux = 0;
	end
	DB.setValue(nodeChar, "speed.careful", "number", nAux);
	setShowSpeed(nodeChar);
end

function setShowSpeed(nodeChar, bCallback)
	if not nodeChar then
		return;
	end
	local bIsMount = DB.getValue(nodeChar, "is_mount", 0) > 0;
	local bIsMounted = not bIsMount and DB.getValue(nodeChar, "is_mounted", 0) > 0;
	local nFast = DB.getValue(nodeChar, "speed.fast", 0);
	local nSprint = DB.getValue(nodeChar, "speed.sprint", 0);
	local nRun = DB.getValue(nodeChar, "speed.run", 0);
	local nWalk = DB.getValue(nodeChar, "speed.walk", 0);
	local nCareful = DB.getValue(nodeChar, "speed.careful", 0);
	if bIsMount then
		local nodeRider = nodeChar.getParent();
		
		if DB.getValue(nodeRider, "is_mounted", 0) > 0 then
			nFast = math.min(nFast, DB.getValue(nodeRider, "speed.fast", 0));
			nSprint = math.min(nSprint, DB.getValue(nodeRider, "speed.sprint", 0));
			nRun = math.min(nRun, DB.getValue(nodeRider, "speed.run", 0));
			nWalk = math.min(nWalk, DB.getValue(nodeRider, "speed.walk", 0));
			nCareful = math.min(nCareful, DB.getValue(nodeRider, "speed.careful", 0));
			if not bCallback then
				setShowSpeed(nodeRider, true);
			end
		end
	elseif bIsMounted then
		local nodeMount = nodeChar.createChild("mount_npc");
		nFast = math.min(nFast, DB.getValue(nodeMount, "speed.fast", 0));
		nSprint = math.min(nSprint, DB.getValue(nodeMount, "speed.sprint", 0));
		nRun = math.min(nRun, DB.getValue(nodeMount, "speed.run", 0));
		nWalk = math.min(nWalk, DB.getValue(nodeMount, "speed.walk", 0));
		nCareful = math.min(nCareful, DB.getValue(nodeMount, "speed.careful", 0));
		if not bCallback then
			setShowSpeed(nodeMount, true);
		end
	end
	DB.setValue(nodeChar, "speed.show.fast", "number", nFast);
	DB.setValue(nodeChar, "speed.show.sprint", "number", nSprint);
	DB.setValue(nodeChar, "speed.show.run", "number", nRun);
	DB.setValue(nodeChar, "speed.show.walk", "number", nWalk);
	DB.setValue(nodeChar, "speed.show.careful", "number", nCareful);
end

function calculateInitiative(nodeChar, bCalculateIni)
	-- Initiative window
	local nPen = ActorManager2.getPenalizer(nodeChar);

	local nImprovedIni = ActorManager2.getAptitudeCategory(nodeChar, DataCommon.aptitude["improved_ini"]);
	local nAgi = ActorManager2.getCurrentAbilityValue(nodeChar, "agility");
	local nLimit = DB.getValue(nodeChar, "encumbrance.limit", 99);
	nAgi = math.min(nAgi + nImprovedIni, nLimit);
	DB.setValue(nodeChar, "initiative.stat", "number", nAgi);

	local nInitMisc = DB.getValue(nodeChar, "initiative.misc", 0);
	local nInitInv = DB.getValue(nodeChar, "encumbrance.init_pen", 0);

	local nInit = nAgi + nInitMisc + nInitInv + nPen;
	DB.setValue(nodeChar, "initiative.total", "number", nInit);
	sColor, sTooltip = setShowInit(nodeChar);

	local nInitCheck = nInit;
	if bCalculateIni then
		local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
		local aDice = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CHECK"]}, false, {DataCommon.abilities_translation_inv["agility"]});
		if aDice and #aDice > 0 then
			local nBonus = 0;
			for _,v in pairs(aDice) do
				nBonus = nBonus + CombatManager2.simpleRoll(v);
			end
			local nAgiBonus = math.min(nAgi + nImprovedIni + nBonus, nLimit);
			nInitCheck = nAgiBonus + nInitMisc + nInitInv + nPen;
		end
	end
	return sColor, sTooltip, nInitCheck;
end

function setShowInit(nodeChar, bCallback)
	if not nodeChar then
		return;
	end
	local sTooltip = "";
	local sColor = "000000";
	local bIsMount = DB.getValue(nodeChar, "is_mount", 0) > 0;
	local bIsMounted = not bIsMount and DB.getValue(nodeChar, "is_mounted", 0) > 0;
	local nInit = DB.getValue(nodeChar, "initiative.total", 0);
	if bIsMount then
		local nodeRider = nodeChar.getParent();
		
		if DB.getValue(nodeRider, "is_mounted", 0) > 0 then
			local nInitOther = DB.getValue(nodeRider, "initiative.total", 0);
			if nInitOther < nInit then
				sColor = "AA0000";
				sTooltip = Interface.getString("char_tooltip_init_limited_rider"):format(nInit);
				nInit = nInitOther;
			else
				sTooltip = Interface.getString("char_tooltip_init_rider"):format(nInitOther);
			end
			if not bCallback then
				setShowInit(nodeRider, true);
			end
		end

	elseif bIsMounted then
		local nodeMount = nodeChar.createChild("mount_npc");local nInitOther = DB.getValue(nodeMount, "initiative.total", 0);
		if nInitOther < nInit then
			sColor = "AA0000";
			sTooltip = Interface.getString("char_tooltip_init_limited_mount"):format(nInit);
			nInit = nInitOther;
		else
			sTooltip = Interface.getString("char_tooltip_init_mount"):format(nInitOther);
		end
		if not bCallback then
			setShowInit(nodeMount, true);
		end
	end
	DB.setValue(nodeChar, "initiative.show", "number", nInit);
	return sColor, sTooltip;
end

function setTokenShow(nodeChar)
	local bIsMount = DB.getValue(nodeChar, "is_mount", 0) > 0;
	local bIsMounted = not bIsMount and DB.getValue(nodeChar, "is_mounted", 0) > 0;
	local sSize = DB.getValue(nodeChar, "size");
	local nodeMount = nodeChar.createChild("mount_npc");

	if bIsMount then
		local nodeRider = nodeChar.getParent();
		setTokenShow(nodeRider);
	else
		local sToken = DB.getValue(nodeChar, "token_real", "");
		if sToken == "" then
			sToken = DB.getValue(nodeChar, "token", "");
		end

		if bIsMounted then
			sSize = DB.getValue(nodeMount, "size");
			sToken = DB.getValue(nodeChar, "token_mounted", "");
			-- if sToken == "" then
			-- 	sToken = DB.getValue(nodeMount, "token", "");
			-- end
		end
		DB.setValue(nodeChar, "token", "token", sToken);
		local nodeCT = ActorManager2.getCTNode(nodeChar);
		if nodeCT then
			DB.setValue(nodeCT, "token", "token", sToken);
			CombatManager.replaceCombatantToken(nodeCT);
			CombatManager2.setTokenSize(nodeCT, sSize); 
			-- Eliminar/añadir montura al CT
			if bIsMounted and nodeMount then
				ActionSummon.removeMountFromCT(nodeChar);
			elseif nodeMount then
				ActionSummon.addMountToCT(nodeChar, nodeCT, nodeMount);
			end
		end
	end
end

function calculateLevel(nodeChar)
	local nodeList = nodeChar.createChild("classes");
	local nLevel = 0;
	for _,nodeClass in pairs(nodeList.getChildren()) do
		nLevel = math.max(nLevel, DB.getValue(nodeClass, "level", 0));
	end
	local nLimit = DataCommon.level_max;
	if ActorManager2.hasAptitude(nodeChar, DataCommon.aptitude["ascendant"]) then
		nLimit = DataCommon.level_max_ascendant;
	elseif ActorManager2.hasAptitude(nodeChar, DataCommon.aptitude["god"]) then
		nLimit = DataCommon.level_max_god;
	end
	nLevel = math.min(nLevel, nLimit);
	DB.setValue(nodeChar, "level", "number", nLevel);
end


function calculateMaxVit(nodeChar)
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
	local nCon = ActorManager2.getCurrentAbilityValue(rActor, "constitution");
	local nFort = DB.getValue(nodeChar, "health.fortitude", 3);
	local nExtra = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["robust"]) + ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["die_hard"]);
	local nExtra = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["robust"]) + ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["die_hard"]);
	local nTotal = (nCon + nExtra) * nFort;
	DB.setValue(nodeChar, "health.vit.max", "number", nTotal);
end

function calculateMaxVigor(nodeChar)
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
	local nCon = ActorManager2.getCurrentAbilityValue(rActor, "constitution");
	local nWill = ActorManager2.getCurrentAbilityValue(rActor, "willpower");
	local nEnd = DB.getValue(nodeChar, "health.endurance", 3);
	local nExtra = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["vigorous"]) + ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["indefatigable"]);
	local nExtra = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["vigorous"]) + ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["indefatigable"]);
	local nTotal = math.floor(((nCon + nWill) / 2 + nExtra) * nEnd);
	DB.setValue(nodeChar, "health.vigor.max", "number", nTotal);
end

function calculateMaxSanity(nodeChar)
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
	local nInt = ActorManager2.getCurrentAbilityValue(rActor, "intelligence");
	local nWill = ActorManager2.getCurrentAbilityValue(rActor, "willpower");
	local nExtra = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["stable_mind"]);
	local nTotal = math.floor(((nInt + nWill) / 2 + nExtra) * DataCommon.base_sanity_mult);
	local nTotal = math.floor(((nInt + nWill) / 2 + nExtra) * DataCommon.base_sanity_mult);
	DB.setValue(nodeChar, "health.sanity.max", "number", nTotal);
end

function calculateMaxSpirit(nodeChar)
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
	local nTotal = 0;
	if not ActorManager2.hasAptitude(rActor, DataCommon.aptitude["no_spirit"]) then
		local nCha = ActorManager2.getCurrentAbilityValue(rActor, "charisma");
		local nExtra = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["strong_spirit"]);
		nTotal = nCha + nExtra + DataCommon.base_spirit;
	end
	DB.setValue(nodeChar, "health.spirit.max", "number", nTotal);
end

function calculateMaxSpirit(nodeChar)
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
	local nTotal = 0;
	if not ActorManager2.hasAptitude(rActor, DataCommon.aptitude["no_spirit"]) then
		local nCha = ActorManager2.getCurrentAbilityValue(rActor, "charisma");
		local nExtra = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["strong_spirit"]);
		nTotal = nCha + nExtra + DataCommon.base_spirit;
	end
	DB.setValue(nodeChar, "health.spirit.max", "number", nTotal);
end

function calculateMaxEssence(nodeChar)
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
	local nSpirit = DB.getValue(nodeChar, "health.spirit.max", 7);
	local nDamageTemp = DB.getValue(nodeChar, "health.spirit.damage", 0);
	local nDamagePerm = DB.getValue(nodeChar, "health.spirit.damage_perm", 0);
	local nProf = ActorManager2.getLevel(rActor);
	local nExtra = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["essence_reserve"]);
	local nExtra2, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ESSENCE"]}, true, {});
	DB.setValue(nodeChar, "health.essence.max", "number", nSpirit - nDamagePerm - nDamageTemp + nProf + nExtra + nExtra2);
end

function updateEssence(nodeChar)
	local nEssenceTotal = 0;

	for _,vNode in pairs(DB.getChildren(nodeChar, "inventorylist")) do
		-- Essence
		local nCount = DB.getValue(vNode, "count", 0);
		local nEssence = DB.getValue(vNode, "essence", 0);
		local nBond = DB.getValue(vNode, "bond", 0);
		nEssenceTotal = nEssenceTotal + nBond*nEssence*nCount;
	end
	nodeChar.createChild("encumbrance.essence").setValue(nEssenceTotal);
end

--

--
-- WEAPON MANAGEMENT
--


function resetEquipedWeapons(nodeChar)
	-- Main hand
	local nodeMain = nodeChar.createChild("main_weapon");
	nodeMain.createChild("name", "string").setValue("");
	nodeMain.createChild("ability", "string").setValue("");
	nodeMain.createChild("skill", "string").setValue("");
	nodeMain.createChild("bonus", "number").setValue(0);

	-- Side hand
	local nodeSide = nodeChar.createChild("side_weapon");
	nodeSide.createChild("name", "string").setValue("");
	nodeSide.createChild("ability", "string").setValue("");
	nodeSide.createChild("skill", "string").setValue("");
	nodeSide.createChild("bonus", "number").setValue(0);
end

function removeFromWeaponDB(nodeItem)
	if not nodeItem then
		return false;
	end

	-- Check to see if any of the weapon nodes linked to this item node should be deleted
	local sItemNode = nodeItem.getNodeName();
	local sItemNode2 = "....inventorylist." .. nodeItem.getName();
	local bFound = false;
	for _,v in pairs(DB.getChildren(nodeItem, "...weaponlist")) do
		local sClass, sRecord = DB.getValue(v, "shortcut", "", "");
		if sRecord == sItemNode or sRecord == sItemNode2 then
			bFound = true;
			v.delete();
		end
	end

	return bFound;
end

function addToPowerDB(nodeItem)
	-- Parameter validation
	local rPowers = ItemManager2.getLinkedPowers(nodeItem);
	if #rPowers == 0 then
		return;
	end
	local sItemPath = nodeItem.getPath();

	-- Get the effect list we are going to add to
	local nodeChar = nodeItem.getChild("...");
	if not nodeChar or nodeChar.getPath() == "" then
		return;
	end
	local nodePowerList = nodeChar.createChild("powers");
	if not nodePowerList then
		Debug.chat('Unable to create item powers')
		return;
	end

	-- Determine identification
	local nItemID = 0;
	if LibraryData.getIDState("item", nodeItem, true) then
		nItemID = 1;
	end
	if nItemID == 0 then
		return;
	end

	-- Determine bonding
	local bBonded = true;
	if DB.getValue(nodeItem, "essence", 0) > 0 then
		bBonded = DB.getValue(nodeItem, "bond", 0) > 0;
	end

	-- Iterate powers
	for k, rPower in pairs(rPowers) do
		-- Find the power if it exists or create a new one
		local nodeNew = DB.findNode(rPower["power_path"]);
		if bBonded and (rPower["link_identifier"] ~= "") then
			local sLink = DB.getValue(nodeNew, "link_identifier", "");
			local bCond1 = sLink  ~= rPower["link_identifier"];
			local bCond2 = nodeNew and not nodeNew.getPath():find(nodeChar.getPath());
			if nodeNew and (bCond1 or bCond2) then
				nodeNew.delete();
				nodeNew = nil;
			end
			Debug.chat("nodeNew", nodeNew)
			if not nodeNew then
				nodeNew = nodePowerList.createChild();
				DB.copyNode(rPower["link_identifier"], nodeNew);
				DB.setValue(nodeNew, "link_identifier", "string", rPower["link_identifier"]);
				DB.setValue(nodeNew, "group", "string", Interface.getString("power_groupname_itempowers"));
				DB.setValue(nodeNew, "linked_power", "string", rPower["item_power_path"]);
				local nodeItemPower = DB.findNode(rPower["item_power_path"])
				DB.setValue(nodeItemPower, "link_power", "string", nodeNew.getPath());
				DB.setValue(nodeNew, "reftype", "string", "power_item_spell");
			end
			-- Update power recharge
			DB.setValue(nodeNew, "name", "string", rPower["name"]);
			DB.setValue(nodeNew, "charges", "number", rPower["charges"]);
			if rPower["type"] == "" then
				DB.setValue(nodeNew, "usesperiod", "string", "power_label_useperiod_consumed");
				local nCount = DB.getValue(nodeItem, "count", 0);
				DB.setValue(nodeNew, "charges", "number", nCount);
				DB.setValue(nodeNew, "cast", "number", 0);
			elseif rPower["type"] == "item_power_type_charges" then
				DB.setValue(nodeNew, "usesperiod", "string", "");
			elseif rPower["type"] == "item_power_type_rec_charges" then
				DB.setValue(nodeNew, "charges_recovery", "number", rPower["recovery"]);
				if rPower["recovery_period"] == "" then
					DB.setValue(nodeNew, "usesperiod", "string", "power_label_useperiod_daily");
				else
					DB.setValue(nodeNew, "usesperiod", "string", "power_label_useperiod_enc");
				end
			end
			-- Update item link
			DB.setValue(nodeNew, "item_path", "string", sItemPath);
		elseif nodeNew then
			nodeNew.delete();
		end
	end
end

function removeFromPowerDB(nodeItem)
	-- Parameter validation
	local rPowers = ItemManager2.getLinkedPowers(nodeItem);
	if #rPowers == 0 then
		return;
	end

	for k, rPower in pairs(rPowers) do
		local nodePower = DB.findNode(rPower["power_path"]);
		if nodePower then
			nodePower.delete();
		end
	end

end

function addToWeaponDB(nodeItem)
	-- Parameter validation
	if not ItemManager2.isWeapon(nodeItem) then
		return;
	end

	-- Get the weapon list we are going to add to
	local nodeChar = nodeItem.getChild("...");
	-- Debug.chat(nodeChar)
	local nodeWeaponList = nodeChar.createChild("weaponlist");
	if not nodeWeaponList then
		Debug.chat('Unable to create weapon action')
		return;
	end

	-- Determine identification
	local nItemID = 0;
	if LibraryData.getIDState("item", nodeItem, true) then
		nItemID = 1;
	end

	-- Grab some information from the source node to populate the new weapon entries
	local sName;
	if nItemID == 1 then
		sName = DB.getValue(nodeItem, "name", "");
	else
		sName = DB.getValue(nodeItem, "nonid_name", "");
		if sName == "" then
			sName = Interface.getString("item_unidentified");
		end
		sName = "** " .. sName .. " **";

		local sPath = DB.getValue(nodeItem, "original_path", "");
		if sPath ~= "" then
			nodeItem = DB.findNode(sPath);
		end
	end

	--  Get other info
	local sSkill = DB.getValue(nodeItem, "subtype", 0);
	local bSideHand = sSkill == Interface.getString("skill_value_shields");
	local bTwoHands = true;

	-- Create new weapon
	local nodeWeapon = nodeWeaponList.createChild();
	if nodeWeapon then
		-- Save most of the info
		local sWeaponPath = nodeWeapon.getPath();
		DB.setValue(nodeItem, "weapon_path", "string", sWeaponPath);
		DB.setValue(nodeWeapon, "shortcut", "windowreference", "item", "....inventorylist." .. nodeItem.getName());
		DB.setValue(nodeWeapon, "name", "string", sName);

		-- Configure attacks
		if nodeItem.getChild("attacks") then
			local nodeAttackList = nodeWeapon.createChild("attacks");
			for _, v in pairs(nodeItem.getChild("attacks").getChildren()) do
				-- Add attack only if damage field is not empty
				local sDamage = DB.getValue(v, "damage_attack", "");
				if sDamage ~= "" then
					local nodeAttack = nodeAttackList.createChild();
					-- Link
					-- DB.setValue(nodeAttack, "attack_path", v.getPath());
					-- DB.setValue(v, "attack_path", "string", nodeAttack.getPath());
					DB.setValue(v, "attack_path", "string", nodeAttack.getName());

					-- Attack type
					local nType = DB.getValue(v, "type_attack", 1);
					DB.setValue(nodeAttack, "type_attack", "number", nType);

					-- Properties
					local sProperties = DB.getValue(v, "properties_attack", "");
					local aProp = ItemManager2.scanProperties(sProperties);

					-- Ability
					local sAbility = DataCommon.abilities_translation_inv['strength'];
					local bGift = aProp["gift"] or sSkill:lower() == DataCommon.weapon_keywords["gift"];
					if bGift then
						sAbility, sSkill = ActorManager2.getBestGift(nodeChar);
					elseif nType > 0 then
						sAbility = DataCommon.abilities_translation_inv['dexterity'];
					elseif aProp["finesse"] then
						nStr = ActorManager2.getAbility(nodeChar, 'strength');
						nDex = ActorManager2.getAbility(nodeChar, 'dexterity');
						if nDex >= nStr then
							sAbility = DataCommon.abilities_translation_inv['dexterity'];
						end
					end
					DB.setValue(nodeAttack, "attack_ability", "string", sAbility);

					-- Skill
					DB.setValue(nodeAttack, "attack_skill", "string", sSkill);

					-- Hands
					bSideHand = bSideHand or aProp["side_hand"];
					bTwoHands = bTwoHands and aProp["two_hands"];

					-- Other
					ItemManager2.updateAttack(v, nodeAttack);
				else
					v.delete();
				end

			end

			-- Set hands
			if bSideHand then
				DB.setValue(nodeWeapon, "hands", "number", 1);
			elseif bTwoHands then
				DB.setValue(nodeWeapon, "hands", "number", 2);
			end
		end

	end


end

function initWeaponIDTracking()
	DB.addHandler("charsheet.*.inventorylist.*.isidentified", "onUpdate", onItemIDChanged);
end

function onItemIDChanged(nodeItemID)
	local nodeItem = nodeItemID.getChild("..");
	local nodeChar = nodeItemID.getChild("....");

	local sPath = nodeItem.getPath();
	for _,vWeapon in pairs(DB.getChildren(nodeChar, "weaponlist")) do
		local _,sRecord = DB.getValue(vWeapon, "shortcut", "", "");
		if sRecord == sPath then
			checkWeaponIDChange(vWeapon);
		end
	end
end

function checkWeaponIDChange(nodeWeapon)
	local _,sRecord = DB.getValue(nodeWeapon, "shortcut", "", "");
	if sRecord == "" then
		return;
	end
	local nodeItem = DB.findNode(sRecord);
	if not nodeItem then
		return;
	end

	local bItemID = LibraryData.getIDState("item", DB.findNode(sRecord), true);
	local bWeaponID = (DB.getValue(nodeWeapon, "isidentified", 1) == 1);
	if bItemID == bWeaponID then
		return;
	end

	local sName;
	if bItemID then
		sName = DB.getValue(nodeItem, "name", "");
	else
		sName = DB.getValue(nodeItem, "nonid_name", "");
		if sName == "" then
			sName = Interface.getString("item_unidentified");
		end
		sName = "** " .. sName .. " **";
	end
	DB.setValue(nodeWeapon, "name", "string", sName);

	local nBonus = 0;
	if bItemID then
		DB.setValue(nodeWeapon, "attackbonus", "number", DB.getValue(nodeWeapon, "attackbonus", 0) + DB.getValue(nodeItem, "bonus", 0));
		local aDamageNodes = UtilityManager.getSortedTable(DB.getChildren(nodeWeapon, "damagelist"));
		if #aDamageNodes > 0 then
			DB.setValue(aDamageNodes[1], "bonus", "number", DB.getValue(aDamageNodes[1], "bonus", 0) + DB.getValue(nodeItem, "bonus", 0));
		end
	else
		DB.setValue(nodeWeapon, "attackbonus", "number", DB.getValue(nodeWeapon, "attackbonus", 0) - DB.getValue(nodeItem, "bonus", 0));
		local aDamageNodes = UtilityManager.getSortedTable(DB.getChildren(nodeWeapon, "damagelist"));
		if #aDamageNodes > 0 then
			DB.setValue(aDamageNodes[1], "bonus", "number", DB.getValue(aDamageNodes[1], "bonus", 0) - DB.getValue(nodeItem, "bonus", 0));
		end
	end

	if bItemID then
		DB.setValue(nodeWeapon, "isidentified", "number", 1);
	else
		DB.setValue(nodeWeapon, "isidentified", "number", 0);
	end
end

--
-- CONDITIONS
--

function calculateSensitivity(nodeChar, sState)
	-- Extract the useful info
	Debug.chat('Moved to managerActor2 function')
end

--
-- DEFENSES
--

function updateForcedDefenses(nodeChar, bForce, sForce)
	-- If bForce is false, we are going to unforce the defense
	if not bForce then
		nodeChar.getChild(sForce).setValue(0);
	-- If bForce is True, put that value in 1 and the rest in 0
	else
		for k, v in pairs(DataCommon.defense_force) do
			if v == sForce then
				nodeChar.getChild(v).setValue(1);
			else
				nodeChar.getChild(v).setValue(0);
			end
		end
	end
end

--
-- ACTIONS
--

function addMountDB(draginfo, nodeChar)
	local sClass, sRecord = draginfo.getShortcutData();
	local nodeNPC;
	-- Safety
	if StringManager.contains({"npc"}, sClass) then
		nodeNPC = DB.findNode(sRecord);
		if not nodeNPC then
			return;
		end
	else
		return;
	end

	-- Store character token
	if DB.getValue(nodeChar, "token_real", "") == "" then
		DB.setValue(nodeChar, "token_real", "token", DB.getValue(nodeChar, "token", ""));
	end
	-- Remove previous mount 
	local nodeNew = nodeChar.createChild("mount_npc");
	DB.deleteChildren(nodeNew);
	-- Create new mount npc
	DB.copyNode(nodeNPC, nodeNew);
	DB.setValue(nodeNew, "is_mount", "number", 1);
	-- Set link
	local nodeLink = nodeChar.createChild("mount_link");
	DB.setValue(nodeChar, "mount_link", "windowreference", "npc", nodeNew.getPath());
	-- Set name
	local sName = DB.getValue(nodeNew, "name", "");
	DB.setValue(nodeChar, "mount_name", "string", sName)
	return true;

end

--
-- CHARACTER SHEET DROPS
--

function addInfoDB(nodeChar, sClass, sRecord)
	-- Validate parameters
	if not nodeChar then
		return false;
	end

	if sClass == "reference_background" then
		addBackgroundRef(nodeChar, sClass, sRecord);
	elseif sClass == "reference_backgroundfeature" then
		addClassFeatureDB(nodeChar, sClass, sRecord);
	elseif sClass == "reference_race" or sClass == "reference_subrace" then
		addRaceRef(nodeChar, sClass, sRecord);
	elseif sClass == "reference_class" then
		addClassRef(nodeChar, sClass, sRecord);
	elseif sClass == "reference_classability" or sClass == "reference_classfeature" then
		addClassFeatureDB(nodeChar, sClass, sRecord);
	elseif sClass == "reference_skill" then
		addSkillRef(nodeChar, sClass, sRecord);
	elseif sClass == "reference_technique" then
		addTechniqueDB(nodeChar, sClass, sRecord);
	elseif sClass == "reference_recipe" then
		addRecipeeDB(nodeChar, sClass, sRecord);
	else
		return false;
	end

	return true;
end

function resolveRefNode(sRecord)
	local nodeSource = DB.findNode(sRecord);
	if not nodeSource then
		local sRecordSansModule = StringManager.split(sRecord, "@")[1];
		nodeSource = DB.findNode(sRecordSansModule .. "@*");
		if not nodeSource then
			outputUserMessage("char_error_missingrecord");
		end
	end
	return nodeSource;
end


function addClassFeatureDB(nodeChar, sClass, sRecord, nodeClass)
	local nodeSource = resolveRefNode(sRecord);
	if not nodeSource then
		return;
	end

	-- Get the list we are going to add to
	local nodeList
	if sClass == "reference_backgroundfeature" or sClass == "reference_racialtrait" or sClass == "reference_subracialtrait" then
		nodeList = nodeChar.createChild("raciallist");
	else
		nodeList = nodeChar.createChild("professionlist");
	end
	if not nodeList then
		return false;
	end

	-- Get the class name
	local sClassName = DB.getValue(nodeSource, "...name", "");

	-- If the feature already exists, increase its category
	local sOriginalName = DB.getValue(nodeSource, "name", "");
	local sOriginalNameLower = StringManager.trim(sOriginalName:lower());
	local sFeatureName = sOriginalName;
	for _,v in pairs(nodeList.getChildren()) do
		if DB.getValue(v, "name", ""):lower() == sOriginalNameLower then
			local nCat = DB.getValue(v, "category", 0) + 1;
			DB.setValue(v, "category", "number", nCat);
			outputUserMessage("char_abilities_message_feature_increase", DB.getValue(v, "name", ""), DB.getValue(nodeChar, "name", ""), nCat);
			return true;
		end
	end

	-- Add the item
	local vNew = nodeList.createChild();
	DB.copyNode(nodeSource, vNew);
	DB.setValue(vNew, "name", "string", sFeatureName);
	DB.setValue(vNew, "source", "string", DB.getValue(nodeSource, "...name", ""));
	DB.setValue(vNew, "locked", "number", 1);
	DB.setValue(vNew, "category", "number", 1);
	DB.setValue(vNew, "subtype", "string", sClassName);

	-- Specific fields
	if sClass == "reference_backgroundfeature" then
		DB.setValue(vNew, "type", "string", Interface.getString("type_aptitude_generic"));
		DB.setValue(vNew, "requirements", "string", Interface.getString("type_aptitude_background"));
		DB.setValue(vNew, "level", "string", "0");
	elseif sClass == "reference_racialtrait" or sClass == "reference_subracialtrait" then
		DB.setValue(vNew, "type", "string", Interface.getString("type_aptitude_racial"));
		DB.setValue(vNew, "requirements", "string", Interface.getString("type_aptitude_troncal"));
		DB.setValue(vNew, "level", "string", "0");
	else
		DB.setValue(vNew, "type", "string", Interface.getString("type_aptitude_professional"));
		DB.setValue(vNew, "requirements", "string", Interface.getString("type_aptitude_troncal"));
	end


	-- Announce
	outputUserMessage("char_abilities_message_featureadd", DB.getValue(vNew, "name", ""), DB.getValue(nodeChar, "name", ""));
	return true;
end

function addTechniqueDB(nodeChar, sClass, sRecord)
	local nodeSource = resolveRefNode(sRecord);
	if not nodeSource then
		return;
	end

	-- Get correct list
	local sType = DB.getValue(nodeSource, "subcycler", "");
	local sMainType = DB.getValue(nodeSource, "cycler", "");
	local sList = "";
	local bCombat = true;
	
	if sMainType == "type_technique_combat" then
		if sType == "type_maic_technique_innate" then
			Debug.chat('There are not combat innate techniques');
			return;
		elseif sType == "type_maic_technique_basic" then
			sList = "basiclistcombat";
		elseif sType == "type_maic_technique_advanced" then
			sList = "advancedlistcombat";
		elseif sType == "type_maic_technique_master" then
			sList = "masterlistcombat";
		elseif sType == "type_maic_technique_secret" then
			Debug.chat('There are not combat secret techniques');
			return;
		else
			Debug.chat('Unable to add Tecnhique, wrong subtype:', sType);
			return;
		end
	elseif sMainType == "type_technique_magic" then
		bCombat = false;
		sList = "listmagic";
	else
		Debug.chat('Unable to add Tecnhique, wrong type:', sMainType);
		return;
	end

	local nodeList = nodeChar.createChild(sList);

	-- Check if it is already present
	local sName = DB.getValue(nodeSource, "name", "");
	local nodeNew;
	for k, v in pairs(nodeList.getChildren()) do
		if sName == DB.getValue(v, "name", "-") then
			nodeNew = v;
			Debug.chat("Technique overwritten: ", sName);
		end
	end
	if not nodeNew then
		nodeNew = nodeList.createChild();
	end

	-- Add it
	DB.copyNode(nodeSource, nodeNew);

	-- Add magic technique if required
	if sMainType == "type_technique_combat" then
		Debug.chat("TODO: Add power to list")
	end
end

function addRecipeeDB(nodeChar, sClass, sRecord)
	local nodeSource = resolveRefNode(sRecord);
	if not nodeSource then
		return;
	end

	-- Check if the recipe is already there
	local nodeList = nodeChar.createChild("recipeslist");
	local sName = DB.getValue(nodeSource, "name", "");
	for _,node in pairs(nodeList.getChildren()) do
		local sNameOther = DB.getValue(node, "name", "");
		if sName == sNameOther then
			Debug.chat("Recipe already known");
			return;
		end
	end

	-- Add element
	local nodeNew = nodeList.createChild();
	DB.copyNode(nodeSource, nodeNew);

	-- Recalculate max days
	DB.setValue(nodeNew, "category", "string", "");
	DB.setValue(nodeNew, "category", "string", DB.getValue(nodeSource, "category", ""));

	-- Recalculate max days
	DB.setValue(nodeNew, "category", "string", "");
	DB.setValue(nodeNew, "category", "string", DB.getValue(nodeSource, "category", ""));
end



-- function addTraitDB(nodeChar, sClass, sRecord)
-- 	local nodeSource = resolveRefNode(sRecord);
-- 	if not nodeSource then
-- 		return;
-- 	end
--
-- 	local sTraitType = CampaignDataManager2.sanitize(DB.getValue(nodeSource, "name", ""));
-- 	if sTraitType == "" then
-- 		sTraitType = nodeSource.getName();
-- 	end
--
-- 	if sTraitType == "abilityscoreincrease" then
-- 		local bApplied = false;
-- 		local sAdjust = DB.getText(nodeSource, "text"):lower();
--
-- 		if sAdjust:match("your ability scores each increase") then
-- 			for _,v in pairs(DataCommon.abilities) do
-- 				addAbilityAdjustment(nodeChar, v, 1);
-- 				bApplied = true;
-- 			end
-- 		else
-- 			local aIncreases = {};
--
-- 			local n1, n2;
-- 			local a1, a2, sIncrease = sAdjust:match("your (%w+) and (%w+) scores increase by (%d+)");
-- 			if a1 then
-- 				local nIncrease = tonumber(sIncrease) or 0;
-- 				aIncreases[a1] = nIncrease;
-- 				aIncreases[a2] = nIncrease;
-- 			else
-- 				for a1, sIncrease in sAdjust:gmatch("your (%w+) score increases by (%d+)") do
-- 					local nIncrease = tonumber(sIncrease) or 0;
-- 					aIncreases[a1] = nIncrease;
-- 				end
-- 				for a1, sDecrease in sAdjust:gmatch("your (%w+) score is reduced by (%d+)") do
-- 					local nDecrease = tonumber(sDecrease) or 0;
-- 					aIncreases[a1] = nDecrease * -1;
-- 				end
-- 			end
--
-- 			for k,v in pairs(aIncreases) do
-- 				addAbilityAdjustment(nodeChar, k, v);
-- 				bApplied = true;
-- 			end
--
-- 			if sAdjust:match("two other ability scores of your choice increase") or
-- 					sAdjust:match("two different ability scores of your choice increase") then
-- 				local aAbilities = {};
-- 				for _,v in ipairs(DataCommon.abilities) do
-- 					if not aIncreases[v] then
-- 						table.insert(aAbilities, StringManager.capitalize(v));
-- 					end
-- 				end
-- 				onAbilitySelectDialog(nodeChar, aAbilities, 2);
-- 			else
-- 				a1, a2, sIncrease = sAdjust:match("either your (%w+) or your (%w+) increases by (%d+)");
-- 				if a1 then
-- 					local aAbilities = {};
-- 					for _,v in ipairs(DataCommon.abilities) do
-- 						if (v == a1) or (v == a2) then
-- 							table.insert(aAbilities, StringManager.capitalize(v));
-- 						end
-- 					end
-- 					if #aAbilities > 0 then
-- 						onAbilitySelectDialog(nodeChar, aAbilities, 1);
-- 					end
-- 				end
-- 			end
-- 		end
-- 		if not bApplied then
-- 			return false;
-- 		end
--
-- 	elseif sTraitType == "age" then
-- 		return false;
--
-- 	elseif sTraitType == "alignment" then
-- 		return false;
--
-- 	elseif sTraitType == "size" then
-- 		local sSize = DB.getText(nodeSource, "text");
-- 		sSize = sSize:match("[Yy]our size is (%w+)");
-- 		if not sSize then
-- 			sSize = "Medium";
-- 		end
-- 		DB.setValue(nodeChar, "size", "string", sSize);
--
-- 	elseif sTraitType == "speed" then
-- 		local sSpeed = DB.getText(nodeSource, "text");
--
-- 		local sWalkSpeed = sSpeed:match("walking speed is (%d+) feet");
-- 		if not sWalkSpeed then
-- 			sWalkSpeed = sSpeed:match("land speed is (%d+) feet");
-- 		end
-- 		if sWalkSpeed then
-- 			local nSpeed = tonumber(sWalkSpeed) or 30;
-- 			DB.setValue(nodeChar, "speed.base", "number", nSpeed);
-- 			outputUserMessage("char_abilities_message_basespeedset", nSpeed, DB.getValue(nodeChar, "name", ""));
-- 		end
--
-- 		local aSpecial = {};
-- 		local bSpecialChanged = false;
-- 		local sSpecial = StringManager.trim(DB.getValue(nodeChar, "speed.special", ""));
-- 		if sSpecial ~= "" then
-- 			table.insert(aSpecial, sSpecial);
-- 		end
--
-- 		local sSwimSpeed = sSpeed:match("swimming speed of (%d+) feet");
-- 		if sSwimSpeed then
-- 			bSpecialChanged = true;
-- 			table.insert(aSpecial, "Swim " .. sSwimSpeed .. " ft.");
-- 		end
--
-- 		local sFlySpeed = sSpeed:match("flying speed of (%d+) feet");
-- 		if sFlySpeed then
-- 			bSpecialChanged = true;
-- 			table.insert(aSpecial, "Fly " .. sFlySpeed .. " ft.");
-- 		end
--
-- 		local sClimbSpeed = sSpeed:match("climbing speed of (%d+) feet");
-- 		if sClimbSpeed then
-- 			bSpecialChanged = true;
-- 			table.insert(aSpecial, "Climb " .. sClimbSpeed .. " ft.");
-- 		end
--
-- 		if bSpecialChanged then
-- 			DB.setValue(nodeChar, "speed.special", "string", table.concat(aSpecial, ", "));
-- 		end
--
-- 	elseif sTraitType == "fleetoffoot" then
-- 		local sFleetOfFoot = DB.getText(nodeSource, "text");
--
-- 		local sWalkSpeedIncrease = sFleetOfFoot:match("walking speed increases to (%d+) feet");
-- 		if sWalkSpeedIncrease then
-- 			DB.setValue(nodeChar, "speed.base", "number", tonumber(sWalkSpeedIncrease));
-- 		end
--
-- 	elseif sTraitType == "darkvision" then
-- 		local sSenses = DB.getValue(nodeChar, "senses", "");
-- 		if sSenses ~= "" then
-- 			sSenses = sSenses .. ", ";
-- 		end
-- 		sSenses = sSenses .. DB.getValue(nodeSource, "name", "");
--
-- 		local sText = DB.getText(nodeSource, "text");
-- 		if sText then
-- 			local sDist = sText:match("%d+");
-- 			if sDist then
-- 				sSenses = sSenses .. " " .. sDist;
-- 			end
-- 		end
--
-- 		DB.setValue(nodeChar, "senses", "string", sSenses);
--
-- 	elseif sTraitType == "superiordarkvision" then
-- 		local sSenses = DB.getValue(nodeChar, "senses", "");
--
-- 		local sDist = nil;
-- 		local sText = DB.getText(nodeSource, "text");
-- 		if sText then
-- 			sDist = sText:match("%d+");
-- 		end
-- 		if not sDist then
-- 			return false;
-- 		end
--
-- 		-- Check for regular Darkvision
-- 		local sTraitName = DB.getValue(nodeSource, "name", "");
-- 		if sSenses:find("Darkvision (%d+)") then
-- 			sSenses = sSenses:gsub("Darkvision (%d+)", sTraitName .. " " .. sDist);
-- 		else
-- 			if sSenses ~= "" then
-- 				sSenses = sSenses .. ", ";
-- 			end
-- 			sSenses = sSenses .. sTraitName .. " " .. sDist;
-- 		end
--
-- 		DB.setValue(nodeChar, "senses", "string", sSenses);
--
-- 	elseif sTraitType == "languages" then
-- 		local bApplied = false;
-- 		local sText = DB.getText(nodeSource, "text");
-- 		local sLanguages = sText:match("You can speak, read, and write ([^.]+)");
-- 		if not sLanguages then
-- 			sLanguages = sText:match("You can read and write ([^.]+)");
-- 		end
-- 		if not sLanguages then
-- 			return false;
-- 		end
--
-- 		sLanguages = sLanguages:gsub("and ", ",");
-- 		sLanguages = sLanguages:gsub("one extra language of your choice", "Choice");
-- 		sLanguages = sLanguages:gsub("one other language of your choice", "Choice");
-- 		-- EXCEPTION - Kenku - Languages - Volo
-- 		sLanguages = sLanguages:gsub(", but you.*$", "");
-- 		for s in string.gmatch(sLanguages, "(%a[%a%s]+)%,?") do
-- 			addLanguageDB(nodeChar, s);
-- 			bApplied = true;
-- 		end
-- 		return bApplied;
--
-- 	elseif sTraitType == "extralanguage" then
-- 		addLanguageDB(nodeChar, "Choice");
-- 		return true;
--
-- 	elseif sTraitType == "subrace" then
-- 		return false;
--
-- 	else
-- 		local sText = DB.getText(nodeSource, "text", "");
--
-- 		if sTraitType == "stonecunning" then
-- 			-- Note: Bypass due to false positive in skill proficiency detection
-- 		else
-- 			checkSkillProficiencies(nodeChar, sText);
-- 		end
--
-- 		-- Get the list we are going to add to
-- 		local nodeList = nodeChar.createChild("traitlist");
-- 		if not nodeList then
-- 			return false;
-- 		end
--
-- 		-- Add the item
-- 		local vNew = nodeList.createChild();
-- 		DB.copyNode(nodeSource, vNew);
-- 		DB.setValue(vNew, "source", "string", DB.getValue(nodeSource, "...name", ""));
-- 		DB.setValue(vNew, "locked", "number", 1);
--
-- 		if sClass == "reference_racialtrait" then
-- 			DB.setValue(vNew, "type", "string", "racial");
-- 		elseif sClass == "reference_subracialtrait" then
-- 			DB.setValue(vNew, "type", "string", "subracial");
-- 		elseif sClass == "reference_backgroundtrait" then
-- 			DB.setValue(vNew, "type", "string", "background");
-- 		end
--
-- 		-- Special handling
-- 		local sNameLower = DB.getValue(nodeSource, "name", ""):lower();
-- 		if sNameLower == TRAIT_DWARVEN_TOUGHNESS then
-- 			applyDwarvenToughness(nodeChar, true);
-- 		elseif sNameLower == TRAIT_NATURAL_ARMOR then
-- 			calcItemArmorClass(nodeChar);
-- 		elseif sNameLower == TRAIT_CATS_CLAWS then
-- 			local aSpecial = {};
-- 			local sSpecial = StringManager.trim(DB.getValue(nodeChar, "speed.special", ""));
-- 			if sSpecial ~= "" then
-- 				table.insert(aSpecial, sSpecial);
-- 			end
-- 			table.insert(aSpecial, "Climb 20 ft.");
-- 			DB.setValue(nodeChar, "speed.special", "string", table.concat(aSpecial, ", "));
-- 		end
-- 	end
--
-- 	-- Announce
-- 	outputUserMessage("char_abilities_message_traitadd", DB.getValue(nodeSource, "name", ""), DB.getValue(nodeChar, "name", ""));
-- 	return true;
-- end

-- function parseSkillsFromString(sSkills)
-- 	local aSkills = {};
-- 	sSkills = sSkills:gsub("and ", "");
-- 	sSkills = sSkills:gsub("or ", "");
-- 	local nPeriod = sSkills:match("%.()");
-- 	if nPeriod then
-- 		sSkills = sSkills:sub(1, nPeriod);
-- 	end
-- 	for sSkill in string.gmatch(sSkills, "(%a[%a%s]+)%,?") do
-- 		local sTrim = StringManager.trim(sSkill);
-- 		table.insert(aSkills, sTrim);
-- 	end
-- 	return aSkills;
-- end

-- function pickSkills(nodeChar, aSkills, nPicks, nProf)
-- 	-- Check for empty or missing skill list, then use full list
-- 	if not aSkills then
-- 		aSkills = {};
-- 	end
-- 	if #aSkills == 0 then
-- 		for k,_ in pairs(DataCommon.skilldata) do
-- 			table.insert(aSkills, k);
-- 		end
-- 		table.sort(aSkills);
-- 	end
--
-- 	-- Add links (if we can find them)
-- 	for k,v in ipairs(aSkills) do
-- 		local rSkillData = DataCommon.skilldata[v];
-- 		if rSkillData then
-- 			aSkills[k] = { text = v, linkclass = "skill", linkrecord = "reference.skilldata." .. rSkillData.lookup .. "@*" };
-- 		end
-- 	end
--
-- 	-- Display dialog to choose skill selection
-- 	local rSkillAdd = { nodeChar = nodeChar, nProf = nProf };
-- 	local wSelect = Interface.openWindow("select_dialog", "");
-- 	local sTitle = Interface.getString("char_build_title_selectskills");
-- 	local sMessage = string.format(Interface.getString("char_build_message_selectskills"), nPicks);
-- 	wSelect.requestSelection (sTitle, sMessage, aSkills, CharManager.onClassSkillSelect, rSkillAdd, nPicks);
-- end
--
-- function checkSkillProficiencies(nodeChar, sText)
-- 	-- Tabaxi - Cat's Talent - Volo
-- 	local sSkill, sSkill2 = sText:match("proficiency in the ([%w%s]+) and ([%w%s]+) skills");
-- 	if sSkill and sSkill2 then
-- 		CharManager.addSkillDB(nodeChar, sSkill, 1);
-- 		CharManager.addSkillDB(nodeChar, sSkill2, 1);
-- 		return true;
-- 	end
-- 	-- Elf - Keen Senses - PHB
-- 	-- Half-Orc - Menacing - PHB
-- 	-- Goliath - Natural Athlete - Volo
-- 	local sSkill = sText:match("proficiency in the ([%w%s]+) skill");
-- 	if sSkill then
-- 		CharManager.addSkillDB(nodeChar, sSkill, 1);
-- 		return true;
-- 	end
-- 	-- Bugbear - Sneaky - Volo
-- 	-- (FALSE POSITIVE) Dwarf - Stonecunning
-- 	sSkill = sText:match("proficient in the ([%w%s]+) skill");
-- 	if sSkill then
-- 		CharManager.addSkillDB(nodeChar, sSkill, 1);
-- 		return true;
-- 	end
-- 	-- Orc - Menacing - Volo
-- 	sSkill = sText:match("trained in the ([%w%s]+) skill");
-- 	if sSkill then
-- 		CharManager.addSkillDB(nodeChar, sSkill, 1);
-- 		return true;
-- 	end
--
-- 	-- Half-Elf - Skill Versatility - PHB
-- 	-- Human (Variant) - Skills - PHB
-- 	local sPicks = sText:match("proficiency in (%w+) skills? of your choice");
-- 	if sPicks then
-- 		local nPicks = convertSingleNumberTextToNumber(sPicks);
-- 		pickSkills(nodeChar, nil, nPicks);
-- 		return true;
-- 	end
-- 	-- Cleric - Acolyte of Nature - PHB
-- 	local nMatchEnd = sText:match("proficiency in one of the following skills of your choice()")
-- 	if nMatchEnd then
-- 		pickSkills(nodeChar, parseSkillsFromString(sText:sub(nMatchEnd)), 1);
-- 		return true;
-- 	end
-- 	-- Lizardfolk - Hunter's Lore - Volo
-- 	sPicks, nMatchEnd = sText:match("proficiency with (%w+) of the following skills of your choice()")
-- 	if sPicks then
-- 		local nPicks = convertSingleNumberTextToNumber(sPicks);
-- 		pickSkills(nodeChar, parseSkillsFromString(sText:sub(nMatchEnd)), nPicks);
-- 		return true;
-- 	end
-- 	-- Cleric - Blessings of Knowledge - PHB
-- 	-- Kenku - Kenuku Training - Volo
-- 	sPicks, nMatchEnd = sText:match("proficient in your choice of (%w+) of the following skills()")
-- 	if sPicks then
-- 		local nPicks = convertSingleNumberTextToNumber(sPicks);
-- 		local nProf = 1;
-- 		if sText:match("proficiency bonus is doubled") then
-- 			nProf = 2;
-- 		end
-- 		pickSkills(nodeChar, parseSkillsFromString(sText:sub(nMatchEnd)), nPicks, nProf);
-- 		return true;
-- 	end
-- 	return false;
-- end

-- function addFeatDB(nodeChar, sClass, sRecord)
-- 	local nodeSource = resolveRefNode(sRecord);
-- 	if not nodeSource then
-- 		return;
-- 	end
--
-- 	-- Get the list we are going to add to
-- 	local nodeList = nodeChar.createChild("featlist");
-- 	if not nodeList then
-- 		return false;
-- 	end
--
-- 	-- Make sure this item does not already exist
-- 	local sName = DB.getValue(nodeSource, "name", "");
-- 	for _,v in pairs(nodeList.getChildren()) do
-- 		if DB.getValue(v, "name", "") == sName then
-- 			return flase;
-- 		end
-- 	end
--
-- 	-- Add the item
-- 	local vNew = nodeList.createChild();
-- 	DB.copyNode(nodeSource, vNew);
-- 	DB.setValue(vNew, "locked", "number", 1);
--
-- 	-- Special handling
-- 	local sNameLower = sName:lower();
-- 	if sNameLower == FEAT_TOUGH then
-- 		applyTough(nodeChar, true);
-- 	else
-- 		local sText = DB.getText(nodeSource, "text", "");
-- 		checkFeatAdjustments(nodeChar, sText);
--
-- 		if (sNameLower == FEAT_DRAGON_HIDE) or (sNameLower == FEAT_MEDIUM_ARMOR_MASTER) then
-- 			calcItemArmorClass(nodeChar);
-- 		end
-- 	end
--
-- 	-- Announce
-- 	outputUserMessage("char_abilities_message_featadd", DB.getValue(vNew, "name", ""), DB.getValue(nodeChar, "name", ""));
-- 	return true;
-- end

-- function checkFeatAdjustments(nodeChar, sText)
-- 	-- Ability increase
-- 	-- PHB - Actor, Durable, Heavily Armored, Heavy Armor Master, Keen Mind, Linguist
-- 	-- XGtE - Dwarven Fortitude, Infernal Constitution
-- 	local sAbility, nAdj, nAbilityMax = sText:match("[Ii]ncrease your (%w+) score by (%d+), to a maximum of (%d+)");
-- 	if sAbility then
-- 		addAbilityAdjustment(nodeChar, sAbility, tonumber(nAdj) or 0, tonumber(nAbilityMax) or 20);
-- 	else
-- 		-- PHB - Athlete, Lightly Armored, Moderately Armored, Observant, Tavern Brawler, Weapon Master
-- 		-- XGtE - Fade Away, Fey Teleportation, Flames of Phlegethos, Orcish Fury, Squat Nimbleness
-- 		local sAbility1, sAbility2, nAdj, nAbilityMax = sText:match("[Ii]ncrease your (%w+) or (%w+) score by (%d+), to a maximum of (%d+)");
-- 		if sAbility1 and sAbility2 then
-- 			onAbilitySelectDialog(nodeChar, { sAbility1, sAbility2 }, 1, nAbilityMax);
-- 		else
-- 			-- XGtE - Dragon Fear, Dragon Hide, Second Chance
-- 			local sAbility1, sAbility2, sAbility3, nAdj, nAbilityMax = sText:match("[Ii]ncrease your (%w+), (%w+), or (%w+) score by (%d+), to a maximum of (%d+)");
-- 			if sAbility1 and sAbility2 and sAbility3 then
-- 				onAbilitySelectDialog(nodeChar, { sAbility1, sAbility2, sAbility3 }, 1, nAbilityMax);
-- 			else
-- 				-- XGtE - Elven Accuracy
-- 				local sAbility1, sAbility2, sAbility3, sAbility4, nAdj, nAbilityMax = sText:match("[Ii]ncrease your (%w+), (%w+), (%w+), or (%w+) score by (%d+), to a maximum of (%d+)");
-- 				if sAbility1 and sAbility2 and sAbility3 and sAbility4 then
-- 					onAbilitySelectDialog(nodeChar, { sAbility1, sAbility2, sAbility3, sAbility4 }, 1, nAbilityMax);
-- 				else
-- 					-- PHB - Resilient
-- 					local nAdj, nAbilityMax = sText:match("[Ii]ncrease the chosen ability score by (%d+), to a maximum of (%d+)");
-- 					if nAdj and nAbilityMax then
-- 						onAbilitySelectDialog(nodeChar, nil, 1, nAbilityMax, true);
-- 					end
-- 				end
-- 			end
-- 		end
-- 	end
--
-- 	-- Armor proficiency
-- 	-- PHB - Heavily Armored, Moderately Armored, Lightly Armored
-- 	local sArmorProf = sText:match("gain proficiency with (%w+) armor and shields");
-- 	if sArmorProf then
-- 		addProficiencyDB(nodeChar, "armor", StringManager.capitalize(sArmorProf) .. ", shields");
-- 	else
-- 		sArmorProf = sText:match("gain proficiency with (%w+) armor");
-- 		if sArmorProf then
-- 			addProficiencyDB(nodeChar, "armor", StringManager.capitalize(sArmorProf));
-- 		end
-- 	end
--
-- 	-- Weapon proficiency
-- 	-- PHB - Tavern Brawler, Weapon Master
-- 	if sText:match("are proficient with improvised weapons") then
-- 		addProficiencyDB(nodeChar, "weapons", "Improvised");
-- 	else
-- 		local sWeaponProfChoices = sText:match("gain proficiency with (%w+) weapons? of your choice");
-- 		if sWeaponProfChoices then
-- 			local nWeaponProfChoices = convertSingleNumberTextToNumber(sWeaponProfChoices);
-- 			if nWeaponProfChoices > 0 then
-- 				addProficiencyDB(nodeChar, "weapons", "Choice (x" .. nWeaponProfChoices .. ")");
-- 			end
-- 		end
-- 	end
--
-- 	-- Skill proficiency
-- 	-- XGtE - Prodigy
-- 	if sText:match("one skill proficiency of your choice") then
-- 		pickSkills(nodeChar, nil, 1);
-- 	else
-- 		-- XGtE - Squat Nimbleness
-- 		local sSkillProf, sSkillProf2 = sText:match("gain proficiency in the (%w+) or (%w+) skill");
-- 		if sSkillProf and sSkillProf2 then
-- 			pickSkills(nodeChar, { sSkillProf, sSkillProf2 }, 1);
-- 		else
-- 			-- PHB - Skilled
-- 			local sSkillPicks = sText:match("gain proficiency in any combination of (%w+) skills or tools");
-- 			local nSkillPicks = convertSingleNumberTextToNumber(sSkillPicks);
-- 			if nSkillPicks > 0 then
-- 				pickSkills(nodeChar, nil, nSkillPicks);
-- 			end
-- 		end
-- 	end
--
-- 	-- Tool proficiency
-- 	-- XGtE - Prodigy
-- 	if sText:match("one tool proficiency of your choice") then
-- 		addProficiencyDB(nodeChar, "tools", "Choice");
-- 	end
--
-- 	-- Extra language choices
-- 	-- PHB - Linguist
-- 	local sLanguagePicks = sText:match("learn (%w+) languages? of your choice");
-- 	if sLanguagePicks then
-- 		local nPicks = convertSingleNumberTextToNumber(sLanguagePicks);
-- 		if nPicks == 1 then
-- 			addLanguageDB(nodeChar, "Choice");
-- 		elseif nPicks > 1 then
-- 			addLanguageDB(nodeChar, "Choice (x" .. nPicks .. ")");
-- 		end
-- 	else
-- 		-- Known languages
-- 		-- XGtE - Fey Teleportation
-- 		local sLanguage = sText:match("learn to speak, read, and write (%w+)");
-- 		if sLanguage then
-- 			addLanguageDB(nodeChar, sLanguage);
-- 		else
-- 			-- Known languages
-- 			-- XGtE - Prodigy
-- 			if sText:match("fluency in one language of your choice") then
-- 				addLanguageDB(nodeChar, "Choice");
-- 			end
-- 		end
-- 	end
--
-- 	-- Initiative increase
-- 	-- PHB - Alert
-- 	local sInitAdj = sText:match("gain a ([+-]?%d+) bonus to initiative");
-- 	if sInitAdj then
-- 		nInitAdj = tonumber(sInitAdj) or 0;
-- 		if nInitAdj ~= 0 then
-- 			DB.setValue(nodeChar, "initiative.misc", "number", DB.getValue(nodeChar, "initiative.misc", 0) + nInitAdj);
-- 			outputUserMessage("char_abilities_message_initadd", nInitAdj, DB.getValue(nodeChar, "name", ""));
-- 		end
-- 	end
--
-- 	-- Passive perception increase
-- 	-- PHB - Observant
-- 	local sPassiveAdj = sText:match("have a ([+-]?%d+) bonus to your passive [Ww]isdom %([Pp]erception%)");
-- 	if sPassiveAdj then
-- 		nPassiveAdj = tonumber(sPassiveAdj) or 0;
-- 		if nPassiveAdj ~= 0 then
-- 			DB.setValue(nodeChar, "perceptionmodifier", "number", DB.getValue(nodeChar, "perceptionmodifier", 0) + nPassiveAdj);
-- 			outputUserMessage("char_abilities_message_passiveadd", nPassiveAdj, DB.getValue(nodeChar, "name", ""));
-- 		end
-- 	end
--
-- 	-- Speed increase
-- 	-- PHB - Mobile
-- 	-- XGtE - Squat Nimbleness
-- 	local sSpeedAdj = sText:match("[Yy]our speed increases by (%d+) feet");
-- 	if not sSpeedAdj then
-- 		sSpeedAdj = sText:match("[Ii]ncrease your walking speed by (%d+) feet");
-- 	end
-- 	nSpeedAdj = tonumber(sSpeedAdj) or 0;
-- 	if nSpeedAdj > 0 then
-- 		DB.setValue(nodeChar, "speed.misc", "number", DB.getValue(nodeChar, "speed.misc", 0) + nSpeedAdj);
-- 		outputUserMessage("char_abilities_message_basespeedadj", nSpeedAdj, DB.getValue(nodeChar, "name", ""));
-- 	end
-- end

-- function addLanguageDB(nodeChar, sLanguage)
-- 	-- Get the list we are going to add to
-- 	local nodeList = nodeChar.createChild("languagelist");
-- 	if not nodeList then
-- 		return false;
-- 	end
--
-- 	-- Make sure this item does not already exist
-- 	if sLanguage ~= "Choice" then
-- 		for _,v in pairs(nodeList.getChildren()) do
-- 			if DB.getValue(v, "name", "") == sLanguage then
-- 				return false;
-- 			end
-- 		end
-- 	end
--
-- 	-- Add the item
-- 	local vNew = nodeList.createChild();
-- 	DB.setValue(vNew, "name", "string", sLanguage);
--
-- 	-- Announce
-- 	outputUserMessage("char_abilities_message_languageadd", DB.getValue(vNew, "name", ""), DB.getValue(nodeChar, "name", ""));
-- 	return true;
-- end

function addBackgroundRef(nodeChar, sClass, sRecord)
	local nodeSource = resolveRefNode(sRecord);
	if not nodeSource then
		return;
	end

	-- Notify
	outputUserMessage("char_abilities_message_backgroundadd", DB.getValue(nodeSource, "name", ""), DB.getValue(nodeChar, "name", ""));

	-- Add the name and link to the main character sheet
	DB.setValue(nodeChar, "background", "string", DB.getValue(nodeSource, "name", ""));
	DB.setValue(nodeChar, "backgroundlink", "windowreference", sClass, nodeSource.getNodeName());

	-- Add perks
	addAtribute(nodeChar, nodeSource);
	addProficiencies(nodeChar, nodeSource);
	addMagic(nodeChar, nodeSource);
	addEquipment(nodeChar, nodeSource, true);
	addLanguages(nodeChar, nodeSource);
	addReadWrite(nodeChar, nodeSource);

	-- Add skill Points (reset)
	local nTotalSP = DB.getValue(nodeSource, "extra_skill_points", 0);
	DB.setValue(nodeChar, "total_SP", "number", nTotalSP);

	-- Add XP (reset)
	local nTotalXP = DB.getValue(nodeSource, "XP", 0);
	DB.setValue(nodeChar, "exp.trans", "number", nTotalXP);

	-- Add features
	for _,v in pairs(DB.getChildren(nodeSource, "features")) do
		addClassFeatureDB(nodeChar, "reference_backgroundfeature", v.getPath());
	end
end

function addRaceRef(nodeChar, sClass, sRecord)
	local nodeSource = resolveRefNode(sRecord);
	if not nodeSource then
		return;
	end

	if sClass == "reference_race" then
		local aTable = {};
		aTable["char"] = nodeChar;
		aTable["class"] = sClass;
		aTable["record"] = nodeSource;

		aTable["suboptions"] = {};
		local sRaceLower = DB.getValue(nodeSource, "name", ""):lower();
		local aMappings = LibraryData.getMappings("race");
		for _,vMapping in ipairs(aMappings) do
			for _,vRace in pairs(DB.getChildrenGlobal(vMapping)) do
				if sRaceLower == StringManager.trim(DB.getValue(vRace, "name", "")):lower() then
					for _,vSubRace in pairs(DB.getChildren(vRace, "subraces")) do
						table.insert(aTable["suboptions"], { text = DB.getValue(vSubRace, "name", ""), linkclass = "reference_subrace", linkrecord = vSubRace.getPath() });
					end
				end
			end
		end

		if #(aTable["suboptions"]) == 0 then
			addRaceSelect(nil, aTable);
		elseif #(aTable["suboptions"]) == 1 then
			addRaceSelect(aTable["suboptions"], aTable);
		else
			-- Display dialog to choose subrace
			local wSelect = Interface.openWindow("select_dialog", "");
			local sTitle = Interface.getString("char_build_title_selectsubrace");
			local sMessage = string.format(Interface.getString("char_build_message_selectsubrace"), DB.getValue(nodeSource, "name", ""), 1);
			wSelect.requestSelection(sTitle, sMessage, aTable["suboptions"], addRaceSelect, aTable);
		end
	else
		local sSubRaceName = DB.getValue(nodeSource, "name", "");

		local aTable = {};
		aTable["char"] = nodeChar;
		aTable["class"] = "reference_race";
		aTable["record"] = nodeSource.getChild("...");
		aTable["suboptions"] = { { text = DB.getValue(nodeSource, "name", ""), linkclass = "reference_subrace", linkrecord = sRecord } };

		addRaceSelect(aTable["suboptions"], aTable);
	end
end

function addRaceSelect(aSelection, aTable)
	-- If subraces available, make sure that exactly one is selected
	if aSelection then
		if #aSelection ~= 1 then
			outputUserMessage("char_error_addsubrace");
			return;
		end
	end

	local nodeChar = aTable["char"];
	local nodeSource = aTable["record"];

	-- Determine race to display on sheet and in notifications
	local sRace = DB.getValue(nodeSource, "name", "");
	local sSubRace = nil;
	if aSelection then
		if type(aSelection[1]) == "table" then
			sSubRace = aSelection[1].text;
		else
			sSubRace = aSelection[1];
		end
		if sSubRace:match(sRace) then
			sRace = sSubRace;
		else
			sRace = sRace .. " (" .. sSubRace .. ")";
		end
	end

	-- Notify
	outputUserMessage("char_abilities_message_raceadd", sRace, DB.getValue(nodeChar, "name", ""));

	-- Add the name and link to the main character sheet
	DB.setValue(nodeChar, "race", "string", sRace);
	DB.setValue(nodeChar, "racelink", "windowreference", aTable["class"], nodeSource.getNodeName());

	-- Add traits
	for _,v in pairs(DB.getChildren(nodeSource, "traits")) do
		addClassFeatureDB(nodeChar, "reference_racialtrait", v.getPath());
	end

	-- Rest
	addBaseSkillsMastery(nodeChar);
	addMain(nodeChar, nodeSource);
	addAtribute(nodeChar, nodeSource);
	addProficiencies(nodeChar, nodeSource);
	addMagic(nodeChar, nodeSource);
	addEquipment(nodeChar, nodeSource);
	addLanguages(nodeChar, nodeSource);
	addSensitivities(nodeChar, nodeSource);
	addEffects(nodeChar, nodeSource);

	-- Add subrace
	if sSubRace then
		for _,vSubRace in ipairs(aTable["suboptions"]) do
			if sSubRace == vSubRace.text then
				-- Traits
				for _,v in pairs(DB.getChildren(DB.getPath(vSubRace.linkrecord, "traits"))) do
					addClassFeatureDB(nodeChar, "reference_subracialtrait", v.getPath());
				end
				-- Rest
				local nodeSubrace = DB.findNode(DB.getPath(vSubRace.linkrecord));
				addAtribute(nodeChar, nodeSubrace);
				addProficiencies(nodeChar, nodeSubrace);
				addMagic(nodeChar, nodeSubrace);
				addEquipment(nodeChar, nodeSubrace);
				addLanguages(nodeChar, nodeSubrace);
				addSensitivities(nodeChar, nodeSubrace);
				addEffects(nodeChar, nodeSource);
				break;
			end
		end
	end

end

function addBaseSkillsMastery(nodeChar)
	local nodeList = DB.createChild(nodeChar, "skilllist_basic");
	for _, v in pairs(nodeList.getChildren()) do
		local nMast = DB.getValue(v, "prof", 0);
		DB.setValue(v, "prof", "number", math.max(nMast, 1));
	end
end

function getClassSpecializationOptions(nodeClass)
	local aOptions = {};
	for _,v in pairs(DB.getChildrenGlobal(nodeClass, "abilities")) do
		table.insert(aOptions, { text = DB.getValue(v, "name", ""), linkclass = "reference_specialization", linkrecord = v.getPath(), node = v });
	end
	return aOptions;
end

function addClassRef(nodeChar, sClass, sRecord)
	local nodeSource = resolveRefNode(sRecord)
	if not nodeSource then
		return;
	end

	-- Get the list we are going to add to
	local nodeList = nodeChar.createChild("classes");
	if not nodeList then
		return;
	end

	-- Notify
	outputUserMessage("char_abilities_message_classadd", DB.getValue(nodeSource, "name", ""), DB.getValue(nodeChar, "name", ""));

	-- Keep some data handy for comparisons
	local sClassName = DB.getValue(nodeSource, "name", "");
	local sClassNameLower = StringManager.trim(sClassName):lower();

	-- Check to see if the character already has this class; or create a new class entry
	local nodeClass = nil;
	local sRecordSansModule = StringManager.split(sRecord, "@")[1];
	local aCharClassNodes = nodeList.getChildren();
	for _,v in pairs(aCharClassNodes) do
		local _,sExistingClassPath = DB.getValue(v, "shortcut", "", "");
		local sExistingClassPathSansModule = StringManager.split(sExistingClassPath, "@")[1];
		if sExistingClassPathSansModule == sRecordSansModule then
			nodeClass = v;
			break;
		end
	end
	if not nodeClass then
		for _,v in pairs(aCharClassNodes) do
			local sExistingClassName = StringManager.trim(DB.getValue(v, "name", "")):lower();
			if (sExistingClassName == sClassNameLower) and (sExistingClassName ~= "") then
				nodeClass = v;
				break;
			end
		end
	end
	local bExistingClass = false;
	if nodeClass then
		bExistingClass = true;
	else
		nodeClass = nodeList.createChild();
	end

	-- Any way you get here, overwrite or set the class reference link with the most current
	DB.setValue(nodeClass, "shortcut", "windowreference", sClass, sRecord);

	-- Add basic class information
	local nLevel = 0;
	if bExistingClass then
		nLevel = DB.getValue(nodeClass, "level", 0) + 1;
	else
		DB.setValue(nodeClass, "name", "string", sClassName);
	end
	DB.setValue(nodeClass, "level", "number", nLevel);

	-- Calculate total level
	local nTotalLevel = DB.getValue(nodeChar, "level", 0);
	if nLevel > nTotalLevel then
		DB.setValue(nodeChar, "level", "number", nLevel);	
	end
	-- for _,vClass in pairs(nodeList.getChildren()) do
	-- 	nTotalLevel = nTotalLevel + DB.getValue(vClass, "level", 0);
	-- end

	-- Add level 0 perks
	if nLevel == 0 then
		addProficiencies(nodeChar, nodeSource, nTotalLevel == 0);
		addMagic(nodeChar, nodeSource, nodeClass);
		addLanguages(nodeChar, nodeSource);
		addReadWrite(nodeChar, nodeSource);
		if not Input.isShiftPressed() then
			addEquipment(nodeChar, nodeSource);
		end
	end

	-- Add skill Points
	local nSP = 0;
	if nTotalLevel == 0 then
		nSP = DB.getValue(nodeSource, "level_0_skill_points", 0);
	elseif nLevel > 0 then
		nSP = DB.getValue(nodeSource, "level_skill_points", 0);
	end
	local nTotalSP = DB.getValue(nodeChar, "total_SP", 0) + nSP;
	DB.setValue(nodeChar, "total_SP", "number", nTotalSP);

	-- Add Global Level Advantages
	local bCondSkip = nLevel < nTotalLevel;
	if not bCondSkip then
		local nXP = DB.getValue(nodeChar, "exp.int", 0);
		local nInt = DB.getValue(nodeChar, "int.base", 3);
		DB.setValue(nodeChar, "exp.int", "number", nXP + DataCommon.XP_int_mult * (nInt - DataCommon.XP_int_threshold));

		if Utilities.inArray(DataCommon.levels_SP_int, nTotalLevel) then
			local nSP = DB.getValue(nodeChar, "SP.int", 0);
			DB.setValue(nodeChar, "SP.int", "number", nSP + nInt - DataCommon.XP_int_threshold);
		end

		-- local nPA = DB.getValue(nodeChar, "PA_max", 0);
		-- DB.setValue(nodeChar, "PA_max", "number", nPA + (DataCommon.levels_PA[nTotalLevel] or 0));

		if not Input.isShiftPressed() then
			if Utilities.inArray(DataCommon.levels_luck, nTotalLevel) then
				local nLuck = DB.getValue(nodeChar, "luck.total", 0);
				DB.setValue(nodeChar, "luck.total", "number", nLuck+1);
			end
			if Utilities.inArray(DataCommon.levels_essence, nTotalLevel) then
				local nEssence = DB.getValue(nodeChar, "health.extra_essence", 0);
				DB.setValue(nodeChar, "health.extra_essence", "number", nEssence+1);
			end
			if Utilities.inArray(DataCommon.levels_fortitude, nTotalLevel) then
				-- Display dialog to choose one option
				local wSelect = Interface.openWindow("select_dialog", "");
				local aOptions = {Interface.getString("fortitude"), Interface.getString("endurance")}
				local sTitle = Interface.getString("char_build_title_selectspecialization");
				local sMessage = Interface.getString("char_build_message_forttitude");
				wSelect.requestSelection (sTitle, sMessage, aOptions, addFortitudeEndurance, nodeChar);
			end
		end
	end

	-- Determine whether a specialization is added this level
	local nodeSpecializationFeature = nil;
	local aSpecializationOptions = {};
	for _,v in pairs(DB.getChildren(nodeSource, "features")) do
		if (isCorrectLevel(DB.getValue(v, "level", "0"), nLevel)) and (DB.getValue(v, "specializationchoice", 0) == 1) then
			nodeSpecializationFeature = v;
			aSpecializationOptions = getClassSpecializationOptions(nodeSource);
			break;
		end
	end

	-- Add features, with customization based on whether specialization is added this level
	local rClassAdd = { nodeChar = nodeChar, nodeSource = nodeSource, nodeClass = nodeClass, nLevel = nLevel};
	if #aSpecializationOptions == 0 then
		addClassFeatureHelper(nil, rClassAdd);
	elseif #aSpecializationOptions == 1 then
		addClassFeatureHelper( { aSpecializationOptions[1].text }, rClassAdd);
	else
		-- Display dialog to choose specialization
		local wSelect = Interface.openWindow("select_dialog", "");
		local sTitle = Interface.getString("char_build_title_selectspecialization");
		local sMessage = string.format(Interface.getString("char_build_message_selectspecialization"), DB.getValue(nodeSpecializationFeature, "name", ""), 1);
		wSelect.requestSelection (sTitle, sMessage, aSpecializationOptions, addClassFeatureHelper, rClassAdd);
	end
end

function addFortitudeEndurance(aSelection, nodeChar)

	-- Check to see if we added specialization
	if aSelection then
		if #aSelection ~= 1 then
			outputUserMessage("char_error_fortitude");
			return;
		end

		if aSelection[1] == Interface.getString("endurance") then
			local nValue = DB.getValue(nodeChar, "health.endurance", 3);
			DB.setValue(nodeChar, "health.endurance", "number", nValue + 1);
			outputUserMessage("char_abilities_message_fortitude_increase", Interface.getString("endurance"), DB.getValue(nodeChar, "name", ""), nValue+1);
		else
			local nValue = DB.getValue(nodeChar, "health.fortitude", 3);
			DB.setValue(nodeChar, "health.fortitude", "number", nValue + 1);
			outputUserMessage("char_abilities_message_fortitude_increase", Interface.getString("fortitude"), DB.getValue(nodeChar, "name", ""), nValue+1);
		end
	end
end

function addSpecialization(nodeChar, nodeSource, nodeClass)
	-- Add proficiencies
	addProficiencies(nodeChar, nodeSource);

	-- Add skill Points
	local nSP = DB.getValue(nodeSource, "extra_skill_points", 0);
	local nTotalSP = DB.getValue(nodeChar, "total_SP", 0) + nSP;
	DB.setValue(nodeChar, "total_SP", "number", nTotalSP);

	-- Add the rest
	addMagic(nodeChar, nodeSource, nodeClass);
	addLanguages(nodeChar, nodeSource);
	addReadWrite(nodeChar, nodeSource);
end

function addEquipment(nodeChar, nodeSource, bAddCommonEquipment)
	local aItems = StringManager.split(DB.getValue(nodeSource, "equipment", ""), ",");
	for _, sItem in pairs(aItems) do
		if sItem:sub(1,1) == " " then
			sItem = sItem:sub(2,sItem:len());
		end
		ItemManager2.addItemToChar(sItem, nodeChar, 1);
	end

	if bAddCommonEquipment then
		for _, v in pairs(DataCommon.initial_items) do
			ItemManager2.addItemToChar(v.name, nodeChar, v.count);
		end
	end
end

function addLanguages(nodeChar, nodeSource)
	local aLanguages = StringManager.split(DB.getValue(nodeSource, "languages", ""), ",");
	local nodeList = DB.getChild(nodeChar, "languagelist");
	if not nodeList then
		return;
	end
	for _, sLanguage in pairs(aLanguages) do
		if sLanguage:sub(1,1) == " " then
			sLanguage = sLanguage:sub(2,sLanguage:len());
		end
		local newNode = DB.createChild(nodeList);
		DB.setValue(newNode, "name", "string", sLanguage);
	end
end

function addReadWrite(nodeChar, nodeSource)
	local nRead = DB.getValue(nodeSource, "read_write", "");
	if nRead > 0 then
		DB.setValue(nodeChar, "read_write", "number", nRead);
	end
end

function addEffects(nodeChar, nodeSource)
	local nodeToCopy = nodeSource.createChild("effectlist");
	local nodeTarget = nodeChar.createChild("effectlist");
	if nodeToCopy and nodeTarget then
		DB.copyNode(nodeToCopy, nodeTarget);
	end
end

function addMain(nodeChar, nodeSource)
	-- Check if node is PC
	local nSuma = 0;
	if ActorManager2.isPC() then
		nSuma = 1;
	end
	local sType = DB.getValue(nodeSource, "race_type", "");
	DB.setValue(nodeChar, "type", "string", sType);
	local nFort = DB.getValue(nodeSource, "fortitude", 4) + nSuma;
	DB.setValue(nodeChar, "health.fortitude", "number", nFort);
	local nEnd = DB.getValue(nodeSource, "endurance", 4) + nSuma;
	DB.setValue(nodeChar, "health.endurance", "number", nEnd);
	local nSpeed = DB.getValue(nodeSource, "speed", 4);
	DB.setValue(nodeChar, "speed.base", "number", nSpeed);
	local sArmorRace = DB.getValue(nodeSource, "natural_armor", ""):lower();
	local sArmorChar = DB.getValue(nodeChar, "protection.natural_armor", ""):lower();
	local sArmor = ActorManager2.sumProtection({sArmorRace, sArmorChar}, true);
	DB.setValue(nodeChar, "protection.natural_armor", "string", sArmor);
	local sSize = DB.getValue(nodeSource, "race_size", "");
	if sSize ~= "" then
		DB.setValue(nodeChar, "size", "string", sSize);
	end
end


function addAtribute(nodeChar, nodeSource)
	local sAtribute = DB.getValue(nodeSource, "stat_normal", ""):lower();
	local aOptions = {};
	if sAtribute == "" or sAtribute == Interface.getString("char_build_subrace") then
		-- Nothing to do
		return;
	elseif sAtribute == Interface.getString("char_build_any") then
		-- There is a choosing
		aOptions = DataCommon.abilities_nogift_translated;
	else
		-- Ver cuales hay que escoger
		for _, v in pairs(DataCommon.abilities_nogift_translated) do
			if sAtribute:find(v) then
				table.insert(aOptions, v);
			end
		end
	end

	if #aOptions == 1 then
		-- Add the only one
		local sPath = "abilities." .. DataCommon.abilities_translation[aOptions[1]] .. ".base";
		local nValue = DB.getValue(nodeChar, sPath, 3);
		DB.setValue(nodeChar, sPath, "number", nValue + 1);
		outputUserMessage("char_abilities_message_atribute_increase", aOptions[1], DB.getValue(nodeChar, "name", ""), nValue + 1)
	elseif #aOptions > 1 then
		-- Display dialog to choose one option
		local wSelect = Interface.openWindow("select_dialog", "");
		local sTitle = Interface.getString("char_build_title_atribute");
		local sMessage = Interface.getString("char_build_message_atribute");
		wSelect.requestSelection (sTitle, sMessage, aOptions, addAtributeHelper, nodeChar);
	end
end

function addAtributeHelper(aSelection, nodeChar)
	-- Check to see if we added an atribute to select
	if aSelection then
		if #aSelection ~= 1 then
			outputUserMessage("char_error_fortitude");
			return;
		end

		local sPath = "abilities." .. DataCommon.abilities_translation[aSelection[1]] .. ".base";
		local nValue = DB.getValue(nodeChar, sPath, 0);
		DB.setValue(nodeChar, sPath, "number", nValue + 1);
	end
end

function addMagic(nodeChar, nodeSource, nodeClass)
	local sStat = DB.getValue(nodeSource, "stat", ""):lower();
	if sStat == "-" or sStat == "" or sStat == " " then
		return;
	end
	sStat = DataCommon.abilities_translation[sStat];
	local sCat = DB.getValue(nodeSource, "category", "");
	if not sStat or sCat == "" then
		return;
	end
	if not sStat or sCat == "" then
		return;
	end

	local sPath = "abilities." .. sStat .. ".base";
	DB.setValue(nodeChar, sPath, "number", DataCommon.initial_magic_stat[sCat]);

	-- sPath = "category_" .. sStat:lower();
	-- DB.setValue(nodeChar, sPath, "string", sCat);

	-- sPath = "magic." .. sStat:lower() .. ".prepares";
	-- DB.setValue(nodeChar, sPath, "number", nPrep);

	local nPrep = DB.getValue(nodeSource, "prepares_spells", 0);
	if nPrep > 0 then
		DB.setValue(nodeChar, "magic.methodic", "number", 1);
	end

	if nodeClass then
		DB.setValue(nodeClass, "magic_type", "string", sStat);
		DB.setValue(nodeClass, "magic_category", "string", sCat);
	end

	sPath = "skilllist_magic." .. sStat .. "_skill";
	DB.setValue(nodeChar, sPath .. ".profession", "number", 1);
	if DB.getValue(nodeChar, sPath .. ".prof", 0) < 1 then
		DB.setValue(nodeChar, sPath .. ".prof", "number", 1);
	end
	return;
end

function addProficiencies(nodeChar, nodeSource, bReset)
	local aSkillTypes = {"basic_skills", "learned_skills", "specific_skills", "weaponary_skills"};
	local aSkillLists = {"skilllist_basic", "skilllist_learned", "skilllist_specific", "skilllist_weaponary"};
	-- Reset background proficiencies not used
	if bReset then
		for k, sSkillType in pairs(aSkillLists) do
			local nodeList = DB.createChild(nodeChar, sSkillType);
			local nThreshold = 1;
			if k == 1 then
				nThreshold = 2;
			end
			for _, v in pairs(nodeList.getChildren()) do
				local nMast = DB.getValue(v, "prof", 0);
				local nProf = DB.getValue(v, "profession", 0);
				if (nMast < nThreshold) and (nProf > 0) then
					DB.setValue(v, "profession","number", 0);
				end
			end
		end
	end

	-- Get skill names
	for k, sSkillType in pairs(aSkillTypes) do
		local aSkills = StringManager.split(StringManager.trim(DB.getValue(nodeSource, sSkillType, "")), ",");
		for _, sSkill in pairs(aSkills) do
			sSkill = StringManager.trim(sSkill);
			local nodeSkill, _, _ = ActorManager2.getSkillNode(nodeChar, sSkill);
			-- If skill doesn't exist, add it
			if not nodeSkill then
				nodeSkill = DB.createChild(DB.createChild(nodeChar, aSkillLists[k]));
				DB.setValue(nodeSkill, "name", "string", sSkill);
				DB.createChild(nodeSkill, "specialization");
			end
			DB.setValue(nodeSkill, "profession", "number", 1);
		end
	end
	return;
end

function addSensitivitiesHelper(nodeChar, nodeSource, sPathChar, sPathRace)
	local sValue = DB.getValue(nodeSource, sPathRace, "");
	local sCurrent = DB.getValue(nodeChar, sPathChar, "");
	if sValue ~= "" then
		if sCurrent == "" then
			sCurrent = sValue;
		else
			sCurrent = sCurrent .. ", " .. sValue;
		end
	end
	DB.setValue(nodeChar, sPathChar, "string", sCurrent);
end

function addSensitivities(nodeChar, nodeSource)
	addSensitivitiesHelper(nodeChar, nodeSource, "sensitivity.damage.vulnerable", "damage_vulnerability");
	addSensitivitiesHelper(nodeChar, nodeSource, "sensitivity.damage.resist", "damage_resistence");
	addSensitivitiesHelper(nodeChar, nodeSource, "sensitivity.damage.inmune", "damage_inmunity");
	addSensitivitiesHelper(nodeChar, nodeSource, "sensitivity.damage.absorb", "damage_absorption");

	addSensitivitiesHelper(nodeChar, nodeSource, "sensitivity.states.vulnerable", "state_vulnerability");
	addSensitivitiesHelper(nodeChar, nodeSource, "sensitivity.states.resist", "state_resistence");
	addSensitivitiesHelper(nodeChar, nodeSource, "sensitivity.states.inmune", "state_inmunity");
	addSensitivitiesHelper(nodeChar, nodeSource, "sensitivity.states.absorb", "state_absorption");
end

function addClassFeatureHelper(aSelection, rClassAdd)
	local nodeSource = rClassAdd.nodeSource;
	local nodeChar = rClassAdd.nodeChar;
	local nodeClass = rClassAdd.nodeClass;

	-- Check to see if we added specialization
	if aSelection then
		if #aSelection ~= 1 then
			outputUserMessage("char_error_addclassspecialization");
			return;
		end

		local aSpecializationOptions = getClassSpecializationOptions(rClassAdd.nodeSource);
		for _,v in ipairs(aSpecializationOptions) do
			if v.text == aSelection[1] then
				-- addClassFeatureDB(nodeChar, "reference_classability", v.linkrecord, nodeClass);
				DB.setValue(nodeClass, "specialty", "string", v.text);
				outputUserMessage("char_abilities_message_specialityadd", v.text, DB.getValue(nodeChar, "name", ""));
				addSpecialization(nodeChar, v.node, nodeClass);
				break;
			end
		end
	end

	-- Add features
	local aMatchingClassNodes = {};
	local sClassNameLower = StringManager.trim(DB.getValue(nodeSource, "name", "")):lower();
	local aMappings = LibraryData.getMappings("class");
	for _,vMapping in ipairs(aMappings) do
		for _,vNode in pairs(DB.getChildrenGlobal(vMapping)) do
			local sExistingClassName = StringManager.trim(DB.getValue(vNode, "name", "")):lower();
			if (sExistingClassName == sClassNameLower) and (sExistingClassName ~= "") then
				table.insert(aMatchingClassNodes, vNode);
				if nodeSource then
					nodeSource = nil;
				end
			end
		end
	end
	if nodeSource then
		table.insert(aMatchingClassNodes, nodeSource);
	end
	local aAddedFeatures = {};
	for _,vNode in ipairs(aMatchingClassNodes) do
		for _,vFeature in pairs(DB.getChildren(vNode, "features")) do
			if (isCorrectLevel(DB.getValue(vFeature, "level", "0"), rClassAdd.nLevel)) and (DB.getValue(vFeature, "specializationchoice", 0) == 0) then
				local sFeatureName = DB.getValue(vFeature, "name", "");
				local sFeatureSpec = DB.getValue(vFeature, "specialization", "");
				if (sFeatureSpec == "") or (sFeatureSpec == DB.getValue(nodeClass, "specialty", "")) then
					local sFeatureNameLower = StringManager.trim(sFeatureName):lower();
					if not aAddedFeatures[sFeatureNameLower] then
						local sClass = sFeatureSpec;
						if not sClass or sClass == "" then
							sClass = DB.getValue(nodeClass, "name", "");
						end
						addClassFeatureDB(nodeChar, sClass, vFeature.getPath(), rClassAdd.nodeClass);
						aAddedFeatures[sFeatureNameLower] = true;
					end
				end
			end
		end
	end
end

function isCorrectLevel(sLevels, nLevel)
	aLevels = StringManager.split(StringManager.trim(sLevels), ',');
	for _, v in pairs(aLevels) do
		if tonumber(v) == nLevel then
			return true;
		end
	end
	return false;
end

function addSkillRef(nodeChar, sClass, sRecord)
	local nodeSource = resolveRefNode(sRecord);
	if not nodeSource then
		return;
	end

	-- Add skill entry
	local nodeSkill = addSkillDB(nodeChar, DB.getValue(nodeSource, "name", ""));
	if nodeSkill then
		DB.setValue(nodeSkill, "text", "formattedtext", DB.getValue(nodeSource, "text", ""));
	end
end

-- function calcSpellcastingLevel(nodeChar)
-- 	local nCurrSpellClass = 0;
-- 	for _,vClass in pairs(DB.getChildren(nodeChar, "classes")) do
-- 		if DB.getValue(vClass, "casterlevelinvmult", 0) > 0 then
-- 			nCurrSpellClass = nCurrSpellClass + 1;
-- 		end
-- 	end
--
-- 	local nCurrSpellCastLevel = 0;
-- 	for _,vClass in pairs(DB.getChildren(nodeChar, "classes")) do
-- 		if DB.getValue(vClass, "casterpactmagic", 0) == 0 then
-- 			local nClassSpellSlotMult = DB.getValue(vClass, "casterlevelinvmult", 0);
-- 			if nClassSpellSlotMult > 0 then
-- 				local nClassSpellCastLevel = DB.getValue(vClass, "level", 0);
-- 				if nCurrSpellClass > 1 then
-- 					nClassSpellCastLevel = math.floor(nClassSpellCastLevel  * (1 / nClassSpellSlotMult));
-- 				else
-- 					nClassSpellCastLevel = math.ceil(nClassSpellCastLevel  * (1 / nClassSpellSlotMult));
-- 				end
-- 				nCurrSpellCastLevel = nCurrSpellCastLevel + nClassSpellCastLevel;
-- 			end
-- 		end
-- 	end
--
-- 	return nCurrSpellCastLevel;
-- end

-- function calcPactMagicLevel(nodeChar)
-- 	local nPactMagicLevel = 0;
-- 	for _,vClass in pairs(DB.getChildren(nodeChar, "classes")) do
-- 		if DB.getValue(vClass, "casterpactmagic", 0) > 0 then
-- 			local nClassSpellSlotMult = DB.getValue(vClass, "casterlevelinvmult", 0);
-- 			if nClassSpellSlotMult > 0 then
-- 				local nClassSpellCastLevel = DB.getValue(vClass, "level", 0);
-- 				nClassSpellCastLevel = math.ceil(nClassSpellCastLevel  * (1 / nClassSpellSlotMult));
-- 				nPactMagicLevel = nPactMagicLevel + nClassSpellCastLevel;
-- 			end
-- 		end
-- 	end
--
-- 	return nPactMagicLevel;
-- end

-- function addAdventureDB(nodeChar, sClass, sRecord)
-- 	local nodeSource = resolveRefNode(sRecord);
-- 	if not nodeSource then
-- 		return;
-- 	end
--
-- 	-- Get the list we are going to add to
-- 	local nodeList = nodeChar.createChild("adventurelist");
-- 	if not nodeList then
-- 		return nil;
-- 	end
--
-- 	-- Copy the adventure record data
-- 	local vNew = nodeList.createChild();
-- 	DB.copyNode(nodeSource, vNew);
-- 	DB.setValue(vNew, "locked", "number", 1);
--
-- 	-- Notify
-- 	outputUserMessage("char_logs_message_adventureadd", DB.getValue(nodeSource, "name", ""), DB.getValue(nodeChar, "name", ""));
-- end

--------------------------------
-- FEATS
--------------------------------

-- function hasTrait(nodeChar, sTrait)
-- 	return (getTraitRecord(nodeChar, sTrait) ~= nil);
-- end
--
-- function getTraitRecord(nodeChar, sTrait)
-- 	local sTraitLower = StringManager.trim(sTrait):lower();
-- 	for _,v in pairs(DB.getChildren(nodeChar, "traitlist")) do
-- 		if StringManager.trim(DB.getValue(v, "name", "")):lower() == sTraitLower then
-- 			return v;
-- 		end
-- 	end
-- 	return nil;
-- end
--
-- function hasFeature(nodeChar, sFeature)
-- 	local sFeatureLower = sFeature:lower();
-- 	for _,v in pairs(DB.getChildren(nodeChar, "featurelist")) do
-- 		if DB.getValue(v, "name", ""):lower() == sFeatureLower then
-- 			return true;
-- 		end
-- 	end
--
-- 	return false;
-- end
--
-- function hasFeat(nodeChar, sFeat)
-- 	return (getFeatRecord(nodeChar, sFeat) ~= nil);
-- end
--
-- function getFeatRecord(nodeChar, sFeat)
-- 	if not sFeat then
-- 		return nil;
-- 	end
-- 	local sFeatLower = sFeat:lower();
-- 	for _,v in pairs(DB.getChildren(nodeChar, "featlist")) do
-- 		if DB.getValue(v, "name", ""):lower() == sFeatLower then
-- 			return v;
-- 		end
-- 	end
-- 	return nil;
-- end
--
-- function applyDwarvenToughness(nodeChar, bInitialAdd)
--
-- end
--
-- function applyDraconicResilience(nodeChar, bInitialAdd)
--
-- end
--
-- function applyUnarmoredDefense(nodeChar, nodeClass)
--
-- end
--
-- function applyTough(nodeChar, bInitialAdd)
--
-- end

----------------------------
-- OTHER
----------------------------

function convertSingleNumberTextToNumber(s)
	if s then
		if s == "one" then return 1; end
		if s == "two" then return 2; end
		if s == "three" then return 3; end
		if s == "four" then return 4; end
		if s == "five" then return 5; end
		if s == "six" then return 6; end
		if s == "seven" then return 7; end
		if s == "eight" then return 8; end
		if s == "nine" then return 9; end
	end
	return 0;
end
