--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--
local aProfLists = { "class@*", "class", "reference.classdata@*" };
local aAptitudeLists = { "aptitude", "aptitude@*", "reference.aptitudedata" };
local aProfs = {};
local aSpecs = {};
local nWindowsPerPage = 40;
local aKeys = {};
local nCurrentPage = 1;
local nPages = 1;
local aRomans = {[1]="I", [2]="II", [3]="III", [4]="IV", [5]="V", [6]="VI", [7]="VII", [8]="VIII", [9]="IX", [10]="X", [11]="XI" };


function onInit()
    getDataArrays();
    constructDefault();
end


function constructDefault()
    closeAll();

    for _,sProf in pairs(aKeys) do
        local aProf = aProfs[sProf];
        if aProf["view"] and aProf["page"] == nCurrentPage then
            local w = createWindow();
            w.profession.setValue(sProf:upper());
            constructListAptitudes(w.aptitudes, aProf["aptitudes"]);
            constructListSpecs(w.specializations, aProf["specialties"]);
        end
    end
end

function constructListSpecs(wList, aSpecs)
    for sSpec,aSpec in pairs(aSpecs) do
        if aSpec["view"] then
            local w = wList.createWindow();
            w.specialization.setValue(StringManager.capitalize(sSpec));
            constructListAptitudes(w.aptitudes, aSpec, true);
        end
    end

end


function constructListAptitudes(wList, aProf, bSpec)
    for k,aApt in pairs(aProf) do
        if k ~= "view" and aApt["view"] then 
            local w = wList.createWindow();
            w.link.setValue("reference_aptitude", aApt["path"])
            w.name.setValue(aApt["name"]);

            local sOrder = "b";
            if aApt["free"] then
                sOrder = "a";
            end
            sOrder = sOrder .. aApt["levels_label"] .. aApt["name"];
            w.order.setValue(sOrder);

            local aLabels = getArrayLabels(aApt["levels"], aApt["levels"]=="X")
            updateLabel(w.level_0, aLabels[0], aApt["free"], bSpec);
            updateLabel(w.level_1, aLabels[1], aApt["free"], bSpec);
            updateLabel(w.level_2, aLabels[2], aApt["free"], bSpec);
            updateLabel(w.level_3, aLabels[3], aApt["free"], bSpec);
            updateLabel(w.level_4, aLabels[4], aApt["free"], bSpec);
            updateLabel(w.level_5, aLabels[5], aApt["free"], bSpec);
            updateLabel(w.level_6, aLabels[6], aApt["free"], bSpec);
            updateLabel(w.level_7, aLabels[7], aApt["free"], bSpec);
            updateLabel(w.level_8, aLabels[8], aApt["free"], bSpec);
            updateLabel(w.level_9, aLabels[9], aApt["free"], bSpec);
            updateLabel(w.level_10, aLabels[10], aApt["free"], bSpec);
        end
    end
end

function assignPages()
    -- Assign
    local nCurrent = 0;
    nPages = 1;
    for k,sProf in pairs(aKeys) do
        local aProf = aProfs[sProf];        
        if nCurrent > nWindowsPerPage then
            nPages = nPages + 1;
            nCurrent = 0;
        end
        aProf["page"] = nPages;
        if aProf["view"] then
            nCurrent = nCurrent + aProf["windows"];
        end
    end
    -- Adjust views and sizes
    updateWindowView();
end

function updateWindowView()
    if nPages > 1 then
        local sPageText = string.format(Interface.getString("label_page_info"), nCurrentPage, nPages)
        window.pageanchor.setVisible(true);
		window.page_info.setValue(sPageText);
		window.page_info.setVisible(true);
		window.page_start.setVisible(nCurrentPage > 1);
		window.page_prev.setVisible(nCurrentPage > 1);
		window.page_next.setVisible(nCurrentPage < nPages);
		window.page_end.setVisible(nCurrentPage < nPages);
    else
		window.pageanchor.setVisible(false);
		window.page_info.setVisible(false);
		window.page_start.setVisible(false);
		window.page_prev.setVisible(false);
		window.page_next.setVisible(false);
		window.page_end.setVisible(false);
	end
end



function getDataArrays()
    -- Loop in professions
    for _,sPath in pairs(aProfLists) do
        local nodeList = DB.findNode(sPath);
        if nodeList then
            for _, nodeProf in pairs(nodeList.getChildren()) do
                local aSpecialties = {};
                local sName = DB.getValue(nodeProf, "name", ""):lower();

                local nodeSpecs = DB.getChild(nodeProf, "abilities")
                if nodeSpecs then
                    for _,nodeSpec in pairs(nodeSpecs.getChildren()) do
                        local sSpec = DB.getValue(nodeSpec, "name", ""):lower();
                        aSpecs[sSpec] = sName;
                        aSpecialties[sSpec] = {["view"]=true};
                    end
                end

                local aProf = {["specialties"] = aSpecialties, ["aptitudes"] = {}, ["view"]=true, ["windows"]=#aSpecialties+1};
                aProfs[sName] = aProf;
            end
        end
    end

    -- Loop in aptitudes
    for _,sPath in pairs(aAptitudeLists) do
        local nodeList = DB.findNode(sPath);
        if nodeList then
            for _, nodeApt in pairs(nodeList.getChildren()) do
                local sName = DB.getValue(nodeApt, "name", "");
                local sPath = DB.getPath(nodeApt);
                local aSources = AptitudeManager.getSources(nodeApt);
                local aLevels = AptitudeManager.getAptitudeAllLevels(nodeApt);
                local sLevels = getLevelStringForOrder(nodeApt);
                local bFree = AptitudeManager.isFree(nodeApt);
                local aApt = {["name"] = sName, ["path"]=sPath, ["levels"]=aLevels, ["levels_label"]=sLevels, ["free"]=bFree, ["view"]=true};

                -- Assign it to the correct aptitude and specialization
                for _,sSource in pairs(aSources) do
                    if aProfs[sSource] then
                        table.insert(aProfs[sSource]["aptitudes"], Utilities.deepcopy(aApt));
                        aProfs[sSource]["windows"] = aProfs[sSource]["windows"] + 1;
                    end
                    if aSpecs[sSource] then
                        local sProf = aSpecs[sSource];
                        table.insert(aProfs[sProf]["specialties"][sSource], Utilities.deepcopy(aApt));
                        aProfs[sProf]["windows"] = aProfs[sProf]["windows"] + 1;
                    end
                end
            end
        end
    end

    -- Sort by key for representation
    for k in pairs(aProfs) do
        table.insert(aKeys, k);
    end
    table.sort(aKeys);
    assignPages();
end

function search(sSearch)
    if not sSearch then
        sSearch = "";
    else
        sSearch = sSearch:lower();
    end

    -- Loops
    for sProf,aProf in pairs(aProfs) do
        aProf["windows"] = 1;
        local cond1 = sProf:find(sSearch);
        local bForceProf = false;
        for sSpec,aSpec in pairs(aProf["specialties"]) do
            local cond2 = cond1 or sSearch=="" or sSpec:find(sSearch);
            bForceProf = bForceProf or cond2;
            local bForceSpec = false;
            for k,aApt in pairs(aSpec) do
                if k ~= "view" then
                    local cond3 = cond1 or cond2 or sSearch=="" or aApt["name"]:lower():find(sSearch);
                    bForceSpec = bForceSpec or cond3;
                    bForceProf = bForceProf or cond3;
                    aApt["view"] = cond3;
                    if cond3 then
                        aProf["windows"] = aProf["windows"] + 1;
                    end
                end
            end
            aSpec["view"] = bForceSpec;
            if bForceSpec then
                aProf["windows"] = aProf["windows"] + 1;
            end
        end
        for k,aApt in pairs(aProf["aptitudes"]) do
            local cond3 = cond1 or sSearch=="" or aApt["name"]:lower():find(sSearch);
            bForceProf = bForceProf or cond3;
            aApt["view"] = cond3;
            if cond3 then
                aProf["windows"] = aProf["windows"] + 1;
            end
        end
        aProf["view"] = bForceProf;
    end

    -- Update
    assignPages();
    constructDefault();
end


function getArrayLabels(aLevels, bX)
    local aEmpty = {[0]="", [1]="", [2]="", [3]="", [4]="", [5]="", [6]="", [7]="", [8]="", [9]="", [10]="" };

    for k,v in pairs(aLevels) do
        if bX then
            aEmpty[v] = "x";
        else
            aEmpty[v] = aRomans[k];
        end
    end
    return aEmpty;
end

function updateLabel(w, sName, bFree, bSpec)
    if sName ~= "" then
        w.setValue(sName);
        if bSpec then
            if bFree then
                w.setFrame("fieldorange_gold", 5, 4, 5, 4);
                w.setTooltipText(Interface.getString("type_aptitude_free"))
            else
                w.setFrame("fieldorange", 5, 4, 5, 4);
            end
        else
            if bFree then
                w.setFrame("fieldblue_gold", 5, 4, 5, 4);
                w.setTooltipText(Interface.getString("type_aptitude_free"))
            else
                w.setFrame("fieldblue", 5, 4, 5, 4);
            end
        end
    end
end

function getLevelStringForOrder(nodeApt)
    local sLevel = DB.getValue(nodeApt, "level", "");
    if sLevel == "10" then
        return sLevel;
    elseif sLevel == "X" then
        return "0"
    else
        return "0" .. sLevel;
    end
end

function setCurrentPage(nValue)
    nCurrentPage = math.min(nPages, math.max(1, nValue));
end
function getCurrentPage()
    return nCurrentPage;
end
function getMaxPages()
    return nPages;
end
