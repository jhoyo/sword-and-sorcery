--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.

function addEntry(bFocus)
	local w = createWindow();
	if bFocus and w then
		w.name.setFocus();
	end
	return w;
end

function addSpecialtyReference(nodeSource)
	if not nodeSource then
		return;
	end

	local sName = StringManager.trim(DB.getValue(nodeSource, "name", ""));
	if sName == "" then
		return;
	end

	local wSkill = nil;
	local sAbility = "";
	for _,w in pairs(getWindows()) do
		if StringManager.trim(w.name.getValue()) == sName then
			wSkill = w;
			break;
		end
	end
	if not wSkill then
		wSkill = createWindow();

		wSkill.name.setValue(sName);
		if DataCommon.skilldata_basic[sName] then
			wSkill.stat.setStringValue(DataCommon.skilldata_basic[sName].stat);
		else
			sAbility = DB.getValue(nodeSource, "cycler_ability", ""):lower();
			wSkill.stat.setStringValue(sAbility);
		end
	end
	if wSkill then
		DB.setValue(wSkill.getDatabaseNode(), "text", "formattedtext", DB.getValue(nodeSource, "text", ""));
		DB.setValue(wSkill.getDatabaseNode(), "stat", "string", DB.getValue(nodeSource, "stat", ""));
	end
end
