--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);

	-- Construct default skills
	local node = getDatabaseNode();
	constructDefaultSkills();
end

function onClose()
	-- deleteUnusedSkills();
end

function onAbilityChanged()
	for _,w in ipairs(getWindows()) do
		if w.isCustom() then
			w.idelete.setVisibility(bEditMode);
		else
			w.idelete.setVisibility(false);
		end
		w.iadd.setVisible(bEditMode);
	end
end

function onListChanged()
	update();
end

function update()
	local bEditMode = (window.parentcontrol.window.skills_iedit.getValue() == 1);
	for _,w in ipairs(getWindows()) do
		if w.isCustom() then
			w.idelete.setVisibility(bEditMode);
		else
			w.idelete.setVisibility(false);
		end
		w.iadd.setVisible(bEditMode);
	end
end

function addEntry(bFocus)
	local w = createWindow();
	w.setCustom(true);
	if bFocus and w then
		w.name.setFocus();
	end
	return w;
end

function onMenuSelection(item)
	if item == 5 then
		addEntry(true);
	end
end

-- Create default skill selection
function constructDefaultSkills()
	-- Get correct data
	local aSkills, sType = getCorrectData();
	-- Collect existing entries
	local entrymap = {};

	for _,w in pairs(getWindows()) do
		local sLabel = w.name.getValue();

		if aSkills[sLabel] then
			if not entrymap[sLabel] then
				entrymap[sLabel] = { w };
			else
				table.insert(entrymap[sLabel], w);
			end
		else
			w.setCustom(true);
		end
	end

	-- Set properties and create missing entries for all known skills
	local sPath = DB.getPath(getDatabaseNode()) .. ".";
	local sName = "";
	for k, t in pairs(aSkills) do
		local bExists = true;
		local matches = entrymap[k];

		if not matches then
			if DataCommon.skilldata_magic_nodenames[k] then
				sName = sPath .. DataCommon.skilldata_magic_nodenames[k];
			else
				sName = nil;
			end
			local w = createWindow(sName);
			if w then
				w.name.setValue(k);
				if t.stat then
					w.stat.setStringValue(t.stat);
				else
					w.stat.setStringValue("");
				end
				matches = { w };
				bExists = false;
			end
		end

		-- Update properties
		for _, match in pairs(matches) do
			match.setCustom(bCustom);
			if t.disarmorstealth then
				match.armorwidget.setVisible(true);
			end
			if not bExists then
				if t.lim then
					match.limited.setValue(t.lim);
				else
					match.limited.setValue(0);
				end
			end
			if not bExists then
				if t.min then
					match.prof.setValue(t.min)
				else
					match.prof.setValue(0)
				end
			end
			match.type.setValue(Interface.getString(sType));
			local _, sRecord = match.shortcut.getValue();
			if sRecord == match.getDatabaseNode().getPath() then
				match.shortcut.setValue("skill", "reference.skilldata." .. t.lookup .. "@*");
			end
		end
	end
end

function addSkillReference(nodeSource)
	-- Get correct data
	local aSkills, _ = getCorrectData();
	if not nodeSource then
		return;
	end

	local sName = StringManager.trim(DB.getValue(nodeSource, "name", ""));
	if sName == "" then
		return;
	end

	local wSkill = nil;
	local sAbility = "";
	for _,w in pairs(getWindows()) do
		if StringManager.trim(w.name.getValue()) == sName then
			wSkill = w;
			break;
		end
	end
	if not wSkill then
		wSkill = createWindow();

		wSkill.name.setValue(sName);
		if aSkills[sName] then
			wSkill.stat.setStringValue(aSkills[sName].stat);
			wSkill.limited.setValue(aSkills[sName].limited);
			wSkill.setCustom(false);
		else
			sAbility = DB.getValue(nodeSource, "cycler_ability", ""):lower();
			wSkill.stat.setStringValue(sAbility);
			wSkill.limited.setValue(DB.getValue(nodeSource, "limited", 0));
			wSkill.setCustom(true);
		end
	end
	if wSkill then
		DB.setValue(wSkill.getDatabaseNode(), "text", "formattedtext", DB.getValue(nodeSource, "text", ""));
		DB.setValue(wSkill.getDatabaseNode(), "stat", "string", DB.getValue(nodeSource, "stat", ""));
		DB.setValue(wSkill.getDatabaseNode(), "limited", "number", DB.getValue(nodeSource, "limited", 0));
	end
end

function getCorrectData()
	local sPath = DB.getPath(getDatabaseNode());
	if sPath:find("basic") then
		return DataCommon.skilldata_basic, "ref_type_skill_basic"
	elseif sPath:find("learned") then
		return DataCommon.skilldata_learned, "ref_type_skill_learned"
	elseif sPath:find("magic") then
		return DataCommon.skilldata_magic, "ref_type_skill_magic"
	elseif sPath:find("specific") then
		return DataCommon.skilldata_specific, "ref_type_skill_specific"
	elseif sPath:find("weaponary") then
		return DataCommon.skilldata_weaponary, "ref_type_skill_weaponary"
	end
end

function deleteUnusedSkills()
	for _,node in pairs(getDatabaseNode().getChildren()) do
		local nProf = DB.getValue(node, "profession", 0);
		local nMast = DB.getValue(node, "prof", 0);
		local nLim = DB.getValue(node, "limited", 0);
		if nProf + nMast + nLim == 0 then
			node.delete();
		end
	end
end
