--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local aLists = {"disadvantagelist", "genericlist", "raciallist", "professionlist"};

function onInit()
  local nodeChar = getDatabaseNode().getChild('...');

  for _, sList in pairs(aLists) do
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.category"), "onUpdate", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.level"), "onUpdate", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.extra_exp"), "onUpdate", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.requirements"), "onUpdate", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.type"), "onUpdate", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList)), "onChildAdded", update);
    DB.addHandler(DB.getPath(DB.getChild(nodeChar, sList)), "onChildDeleted", update);
  end

  update();
end

function onClose()
  local nodeChar = getDatabaseNode().getChild('...');

  for _, sList in pairs(aLists) do
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.category"), "onUpdate", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.level"), "onUpdate", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.extra_exp"), "onUpdate", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.requirements"), "onUpdate", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList), "*.type"), "onUpdate", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList)), "onChildAdded", update);
    DB.removeHandler(DB.getPath(DB.getChild(nodeChar, sList)), "onChildDeleted", update);
  end
end

function update()
  local nodeChar = getDatabaseNode().getChild('...');

  local nXP = 0;
  local nCost0 = DataCommon.XP_level_0
  local nCostMult = DataCommon.XP_level_mult
  local sTroncal = Interface.getString("type_aptitude_troncal");
  local sFree = Interface.getString("type_aptitude_free");
  local sBackground = Interface.getString("type_aptitude_background");

  for _, sList in pairs(aLists) do
    local nodeList = DB.getChild(nodeChar, sList);
    for _, nodeApt in pairs(DB.getChildren(nodeList)) do
      local sReq = DB.getValue(nodeApt, "requirements", "");
      if sReq ~= sTroncal and sReq ~= sFree and sReq ~= sBackground then
        local nCat = DB.getValue(nodeApt, "category", 1);
        local nExtra = DB.getValue(nodeApt, "extra_exp", 1);
        local sType = DB.getValue(nodeApt, "type", "");
        local sLevels = DB.getValue(nodeApt, "level", "0");
        local aLevels = StringManager.split(StringManager.trim(sLevels), ',');

        if sList == "disadvantagelist" or sType == Interface.getString("type_aptitude_flaw") then
          nXP = nXP - nExtra;
        else
          for k, nLevel in pairs(aLevels) do
            if k <= nCat then
              nXP = nXP + nCost0 + nCostMult * nLevel + nExtra;
            end
          end
        end

      end
    end
  end

  setValue(nXP);

end
