--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	update();
end


function VisDataCleared()
	update();
end

function InvisDataAdded()
	update();
end

function updateControl(sControl, bReadOnly, bID)
	if not self[sControl] then
		return false;
	end

	if not bID then
		return self[sControl].update(bReadOnly, true);
	end

	return self[sControl].update(bReadOnly);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);
	local sType = DB.getValue(nodeRecord, "cycler", "");
	local bCombat = sType ~= "type_technique_magic";
	local bMagic = sType ~= "type_technique_combat";

	local bSection1 = false;
	if updateControl("requirements", bReadOnly, true) then bSection1 = true; end
	if updateControl("subcycler", bReadOnly, true) then bSection1 = true; end

	local bSection2 = false;
	if updateControl("combatcycler", bReadOnly, bCombat) then bSection2 = true; end
	if updateControl("actions", bReadOnly, bCombat) then bSection2 = true; end
	if updateControl("vigor_cost", bReadOnly, bCombat) then bSection2 = true; end

	local bSection3 = false;
	if updateControl("magiccycler", bReadOnly, bMagic) then bSection3 = true; end
	if updateControl("vigor_mod", bReadOnly, bMagic) then bSection3 = true; end
	if updateControl("dif_mod", bReadOnly, bMagic) then bSection3 = true; end
	if updateControl("disadvantage", bReadOnly, bMagic) then bSection3 = true; end
	if updateControl("technique_disadvantage", bReadOnly, bMagic) then bSection3 = true; end
	if updateControl("unstable_mod", bReadOnly, bMagic) then bSection3 = true; end
	if updateControl("actions_mod", bReadOnly, bMagic) then bSection3 = true; end
	if updateControl("vigor_mod", bReadOnly, bMagic) then bSection3 = true; end

	local bSection4 = DB.getValue(nodeRecord, "text", "") ~= "";
	text.setVisible(bSection4);
	text.setReadOnly(bReadOnly);

	divider1.setVisible(bSection1 and bSection2);
	divider2.setVisible((bSection1 or bSection2) and bSection3);
	divider3.setVisible((bSection1 or bSection2 or bSection3) and bSection4);
end
