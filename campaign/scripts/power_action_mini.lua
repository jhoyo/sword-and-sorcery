--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	local node = getDatabaseNode();
	local sNode = node.getNodeName();
	DB.addHandler(sNode, "onChildUpdate", onDataChanged);

	local node2 = DB.getChild(node, ".....cast_techniques");
	DB.addHandler(DB.getPath(node2), "onChildUpdate", onDataChanged);
	onDataChanged();
end

function onClose()
	local sNode = getDatabaseNode().getNodeName();
	DB.removeHandler(sNode, "onChildUpdate", onDataChanged);
end

function onDataChanged()
	updateDisplay();
	updateViews();
end

function updateDisplay()
	local sType = DB.getValue(getDatabaseNode(), "type", "");

	if sType == "cast" then
		button.setIcons("button_action_cast", "button_action_cast_down");
	elseif sType == "damage" then
		button.setIcons("button_action_damage", "button_action_damage_down");
	elseif sType == "effect" then
		button.setIcons("button_action_effect", "button_action_effect_down");
	elseif sType == "attack" then
		button.setIcons("button_action_attack", "button_action_attack_down");
	elseif sType == "save" then
		button.setIcons("button_action_save", "button_action_save_down");
	elseif sType == "skill" then
		button.setIcons("button_action_skill", "button_action_skill_down");
	elseif sType == "summon" then
		button.setIcons("button_action_summon", "button_action_summon_down");
	elseif sType == "heal" then
		button.setIcons("button_action_heal", "button_action_heal_down");
	elseif sType == "affliction" then
		button.setIcons("button_action_affliction", "button_action_affliction_down");
	elseif sType == "cost" then
		button.setIcons("button_action_cost", "button_action_cost_down");
	end

	if sType == "cast" then
		parameter.setTooltipText(Interface.getString("power_tooltip_cast_level"));
	elseif sType == "cost" then
		parameter.setTooltipText(Interface.getString("tooltip_power_dosage"));
	else
		parameter.setTooltipText(Interface.getString("power_tooltip_height_level"));
	end
end

function updateViews()
	local sType = DB.getValue(getDatabaseNode(), "type", "");

	if sType == "cast" then
		onCastChanged();
	elseif sType == "damage" then
		onDamageChanged();
	elseif sType == "effect" then
		onEffectChanged();
	elseif sType == "attack" then
		onAttackChanged();
	elseif sType == "save" then
		onSaveChanged();
	elseif sType == "summon" then
		onSummonChanged();
	elseif sType == "roll" then
		onRollChanged();
	elseif sType == "skill" then
		onSkillChanged();
	elseif sType == "heal" then
		onHealChanged();
	elseif sType == "affliction" then
		onAfflictionChanged();
	elseif sType == "cost" then
		onCostChanged();
	end
end


function onCostChanged()
	local sString = PowerManager.getPCPowerCostActionText(getDatabaseNode());
	local sTooltip = "COST: " .. sString;
	button.setTooltipText(sTooltip);
end

function onAfflictionChanged()
	local sString = PowerManager.getPCPowerAfflictionActionText(getDatabaseNode());
	local sTooltip = "AFF: " .. (sString or "");
	button.setTooltipText(sTooltip);
end

function onRollChanged()
	local sString = PowerManager.getPCPowerRollActionText(getDatabaseNode());
	local sTooltip = "ROLL: " .. sString;
	button.setTooltipText(sTooltip);
end

function onAttackChanged()
	local sAttack = PowerManager.getPCPowerAttackActionText(getDatabaseNode());
	if sAttack ~= '' then
		local sTooltip = "ATK: " .. sAttack;
		button.setTooltipText(sTooltip);
	end
end

function onSkillChanged()
	local sSkill = PowerManager.getPCPowerSkillActionText(getDatabaseNode());
	if sSkill ~= '' then
		local sTooltip = "SKILL: " .. sSkill;
		button.setTooltipText(sTooltip);
	end
end

function onSaveChanged()
	local sSave = PowerManager.getPCPowerSaveActionText(getDatabaseNode());
	local sTooltip = "SAVE: " .. sSave;
	button.setTooltipText(sTooltip);
end

function onCastChanged()
	local node = getDatabaseNode();
	local sTooltip = "CAST: Level " .. DB.getValue(node, "cast_level", 0);
	local sExtraTooltip = PowerManager.getPCPowerCastActionText(node)
	if sExtraTooltip ~= "" then
		sTooltip = sTooltip .. ", " .. sExtraTooltip;
	end
	button.setTooltipText(sTooltip);
end

function onDamageChanged()
	local sDamage = PowerManager.getPCPowerDamageActionText(getDatabaseNode());
	if sDamage ~= "" then
		button.setTooltipText("DMG: " .. sDamage);
	end
end

function onHealChanged()
	local sHeal = PowerManager.getPCPowerHealActionText(getDatabaseNode());
	if sHeal ~= "" then
		button.setTooltipText("HEAL: " .. sHeal);
	end
end

function onEffectChanged()
	local sEffect = PowerManager.getPCPowerEffectActionText(getDatabaseNode());
	if sEffect ~= "" then
		button.setTooltipText("EFFECT: " .. sEffect);
	end
end

function onSummonChanged()
	local sSummon = PowerManager.getPCPowerSummonActionText(getDatabaseNode());
	button.setTooltipText("SUMMON: " .. sSummon);
end
