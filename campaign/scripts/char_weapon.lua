--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	setRadialOptions();
	onDataChanged();

	local node = getDatabaseNode();
	DB.addHandler(node.getNodeName(), "onChildUpdate", onDataChanged);
	DB.addHandler(node.getNodeName(), "onDelete", deleteItem);
end


function onClose()
	local node = getDatabaseNode();
	DB.removeHandler(node.getNodeName(), "onChildUpdate", onDataChanged);
	DB.removeHandler(node.getNodeName(), "onDelete", deleteItem);
end

function deleteItem(node)
	local _, sPath = DB.getValue(node, "shortcut");
	local nodeDelete = DB.findNode(sPath);
	if nodeDelete then nodeDelete.delete(); end
end

function onMenuSelection(selection, subselection)
	if selection == 6 and subselection == 7 then
		local node = getDatabaseNode();
		if node then
			node.delete();
		else
			close();
		end
	elseif selection == 4 then
		attacklist.addEntry(true);
	end
end

function setRadialOptions()
	resetMenuItems();
	registerMenuItem(Interface.getString("list_menu_createattack"), "pointer", 4);
	registerMenuItem(Interface.getString("list_menu_deleteitem"), "delete", 6);
	registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 6, 7);
end



local m_sClass = "";
local m_sRecord = "";

function onLinkChanged()
	local node = getDatabaseNode();
	local sClass, sRecord = DB.getValue(node, "shortcut", "", "");
	if sClass ~= m_sClass or sRecord ~= m_sRecord then
		m_sClass = sClass;
		m_sRecord = sRecord;

		local sInvList = DB.getPath(DB.getChild(node, "..."), "inventorylist") .. ".";
		if sRecord:sub(1, #sInvList) == sInvList then
			carried.setLink(DB.findNode(DB.getPath(sRecord, "carried")));
		end
	end
end

function onAttackChanged()
	if action_attacks then
		for _, w in pairs(action_attacks) do
			w.onAttackChanged();
		end
	end
end

function onDamageChanged()
	if action_attacks then
		for _, w in pairs(action_attacks) do
			w.onDamageChanged();
		end
	end
end

function onDataChanged()
	onLinkChanged();
	onAttackChanged();
	onDamageChanged();
	updateView();
end

function updateView()
	local nRecharge = 0;
	local nChamber = 0;
	for _, w in pairs(attacklist.getWindows()) do
		local nTypeAttack = w.type_attack.getValue() or 0;
		if nTypeAttack == 1 then
			local sProperties = w.properties.getValue();
			local aProp = ItemManager2.scanProperties(sProperties);
			nRecharge = aProp["recharge"];
			nChamber = aProp["chamber"];
		end
		local bDistance = nTypeAttack == 1 and nRecharge > 0 and nChamber > 0;
		label_ammo.setVisible(nTypeAttack > 0);
		maxammo.setVisible(nTypeAttack > 0);
		ammocounter.setVisible(nTypeAttack > 0);
		label_recharged.setVisible(bDistance);
		rechargecounter.setVisible(bDistance);
		maxrecharge.setValue(nChamber);
		recharge_time.setValue(nRecharge);
	end
end

