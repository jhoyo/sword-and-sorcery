--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local bStoredReadOnly = false;

function onInit()
	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);

	-- -- Start with one empty item if there is none
	-- constructDefault();
end

function update(bReadOnly)
	if type(bReadOnly) == "nil" then
		bReadOnly = bStoredReadOnly;
	else
		bStoredReadOnly = bReadOnly;
	end
	
	for _,w in ipairs(getWindows()) do
		w.idelete.setVisibility(not bReadOnly);
		w.type_attack.setEnabled(not bReadOnly);
		w.range_attack.setEnabled(not bReadOnly);
		w.damage_attack.setEnabled(not bReadOnly);
		w.properties_attack.setEnabled(not bReadOnly);
		if bReadOnly then
			w.properties_attack.setFrame("fieldlight", 7,5,7,5);
			w.range_attack.setFrame("fieldlight", 7,5,7,5);
			w.damage_attack.setFrame("fieldlight", 7,5,7,5);
		else
			w.properties_attack.setFrame("fielddark", 7,5,7,5);
			w.range_attack.setFrame("fielddark", 7,5,7,5);
			w.damage_attack.setFrame("fielddark", 7,5,7,5);
		end
	end
end

function addEntry(bFocus)
	local w = createWindow();
	if bFocus and w then
		w.damage_attack.setFocus();
	end
	w.idelete.setVisibility(true);
	return w;
end

function onMenuSelection(item)
	if item == 5 then
		addEntry(true);
	end
end

-- -- Create default
-- function constructDefault()
-- 	-- Collect existing entries
-- 	if not getWindows() then
-- 		addEntry(true);
-- 	end
-- end
