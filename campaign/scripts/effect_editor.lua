--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
    updateData();
    updateCostVisibility();
end

function updateData(bForced)
    local nodeAction = getDatabaseNode();
    if bForced or DB.getValue(nodeAction, "initialized_duration", 0) > 0 then
        DB.setValue(nodeAction, "initialized_duration", "number", 0);

        local nodeSpell = nodeAction.getChild("...");
        if not nodeSpell then
            return;
        end
        local sTypeSpell = DB.getValue(nodeSpell, "reftype", "");
        local nodeItem;
        local rActor = ActorManager.resolveActor(nodeAction.getChild("....."));
        local sItemPath = DB.getValue(nodeSpell, "item_path", "");
        if sItemPath ~= "" then
            nodeItem = DB.findNode(sItemPath);
        end

        if ItemManager2.isPotion(nodeItem) or not ActorManager2.isMagicUser(rActor) then
            DB.setValue(nodeAction, "durtype", "string", "effect_duration_autotie");
        elseif DB.getValue(nodeSpell, "type_spell", "") == "power_ritual" then
            DB.setValue(nodeAction, "durtype", "string", "effect_duration_oncast");
        elseif sTypeSpell == "power_spell" or sTypeSpell == "power_item_spell" then
            DB.setValue(nodeAction, "durtype", "string", "effect_duration_conc");
        end
        
        local nDur, sUnits = PowerManager.getConcDuration(nodeSpell, true);

        if nDur > 0 then
            DB.setValue(nodeAction, "durmod", "number", nDur);
            DB.setValue(nodeAction, "durunit", "string", sUnits);
        end

    end

end


function updateCostVisibility()
    local node = getDatabaseNode();
    local bVis = DB.getValue(node, "cost_apply", 0) == 1;
    local bNumber = DB.getValue(node, "cost_type", "") == "";
    cost_type.setVisible(bVis);
    cost_number.setVisible(bVis and bNumber);
    cost_resource.setVisible(bVis);
    costcalculate.setVisible(bVis);
    cost_initial_label.setVisible(bVis);
    cost_initial.setVisible(bVis);
end

function updateDataCost(bForced)
    local nodeAction = getDatabaseNode();
    if bForced or DB.getValue(nodeAction, "initialized_cost", 0) > 0 then
        DB.setValue(nodeAction, "initialized_cost", "number", 0);
        local nApply = 0;
        local sType = "";
        local nCost = 0;
        local sResource = "";
        local nInitial = 0;

        local nodeSpell = nodeAction.getChild("...");
        if not nodeSpell then
            return;
        end
        local sTypeSpell = DB.getValue(nodeSpell, "reftype", "");
        local sKeywords = DB.getValue(nodeSpell, "keywords", ""):lower();
        
        -- Spell cost
        if (sTypeSpell == "power_spell" or sTypeSpell == "power_item_spell") and sKeywords:find(DataCommon.magic_keywords["tiring"]) then
            nApply = 1;
            sType = "power_cost_type_spell";            
            if sKeywords:find(DataCommon.magic_keywords["blood_magic"]) then
                sResource = "power_cost_resource_vitality";
            end
        -- Activity cost
        else
            local sVigor = DB.getValue(nodeSpell, "vigor_cost", "");
            -- sCost, sResource = sVigor:match("(%d+) (%a+)");
            nCost, sResource = PowerManager.getActivityCost(sVigor);
            if nCost > 0 then
                if sResource == "vitality" then
                    sResource = "power_cost_resource_vitality";
                    nApply = 1;
                elseif sResource == "vigor" then
                    sResource = "";
                    nApply = 1;
                end
            end
            -- Styles are personal, target self
            if sKeywords:find(DataCommon.magic_keywords["style"]) then
                DB.setValue(nodeAction, "targeting", "string", "self");
                nInitial = 1;
            end
        end
        DB.setValue(nodeAction, "cost_apply", "number", nApply);
        DB.setValue(nodeAction, "cost_type", "string", sType);
        DB.setValue(nodeAction, "cost_number", "number", nCost);
        DB.setValue(nodeAction, "cost_resource", "string", sResource);
        DB.setValue(nodeAction, "cost_initial", "number", nInitial);
    end

end