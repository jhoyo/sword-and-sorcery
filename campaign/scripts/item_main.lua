--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	correctRarity();
	update();
	BuildMenu();
	local node = getDatabaseNode();
	ItemManager2.updateModifiedItem(node);
	DB.addHandler(node.getPath(), "onDelete", deleteOriginal);
end

function onClose()
	local node = getDatabaseNode();
	DB.removeHandler(node.getPath(), "onDelete", deleteOriginal);
end

function deleteOriginal()
	local node = getDatabaseNode();
	local nodeOriginal = DB.findNode(DB.getValue(node, "original_path", ""));
	DB.setValue(node, "original_path", "string", "")
	DB.setValue(node, "modified", "number", 0)
	DB.setValue(node, "link_original_item", "windowreference", "", "");
	if nodeOriginal then nodeOriginal.delete(); end
end

function correctRarity()
	local node = getDatabaseNode();
	local sRarity = DB.getValue(node, "rarity", "");
	if not Utilities.inArray(DataCommon.rarities, sRarity) then
		DB.setValue(node, "rarity", "string", "");
	end
end

function BuildMenu()
	local bCond = getDatabaseNode().getPath():find("inventorylist");
	if bCond then
		local sNode = getDatabaseNode();
		registerMenuItem(Interface.getString("list_menu_updateitem"), "tokenbagreset", 5);
	end
end

function onMenuSelection(selection)
	if selection == 5 then
		local node = getDatabaseNode();
		CharManager.onCharItemDelete(node);
		CharManager.onCharItemAdd(node);
	end
end


function VisDataCleared()
	update();
end

function InvisDataAdded()
	update();
end

function updateControl(sControl, bReadOnly, bID)
	if not self[sControl] then
		return false;
	end

	if not bID then
		return self[sControl].update(bReadOnly, true);
	end

	if not self[sControl].update then
		Debug.chat("sControl",sControl);
	end

	return self[sControl].update(bReadOnly);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);
	local bID = LibraryData.getIDState("item", nodeRecord);

	local bWeapon, sTypeLower, sSubtypeLower = ItemManager2.isWeapon(nodeRecord);
	local bRanged = ItemManager2.isRanged(nodeRecord);
	local bArmor = ItemManager2.isArmor(nodeRecord);
	local bAmmunition = ItemManager2.isAmmunition(nodeRecord);
	local bService = ItemManager2.isService(nodeRecord);
	local bArcaneFocus = (sTypeLower == "rod") or (sTypeLower == "staff") or (sTypeLower == "wand");

	local nAttacks = item_attacks.getWindowCount();
	local nRunes = item_runes.getWindowCount();
	local nModifications = item_modifications.getWindowCount();
	local nPowers = item_powers.getWindowCount();
	local nEffects = item_effects.getWindowCount();

	local bSection1 = false;
	if User.isHost() then
		if updateControl("nonid_name", bReadOnly, true) then bSection1 = true; end;
	else
		updateControl("nonid_name", false);
	end
	if (User.isHost() or not bID) then
		if updateControl("nonidentified", bReadOnly, true) then bSection1 = true; end;
	else
		updateControl("nonidentified", false);
	end
	if updateControl("modified", bReadOnly, bID) then bSection1 = true; end

	local bSection2 = false;
	if updateControl("type", bReadOnly, bID) then bSection2 = true; end
	if updateControl("subtype", bReadOnly, bID) then bSection2 = true; end
	if updateControl("rarity", bReadOnly, bID) then bSection2 = true; end

	local bSection3 = false;
	if updateControl("cost", bReadOnly, bID) then bSection3 = true; end
	if updateControl("weight", bReadOnly, bID) then bSection3 = true; end
	if updateControl("hardness", bReadOnly, bID and not bService) then bSection3 = true; end
	if updateControl("dents", bReadOnly, bID and not bService) then bSection3 = true; end
	-- if updateControl("dents_aux", bReadOnly, bID) then bSection3 = true; end
	if updateControl("material", bReadOnly, bID and not bService) then bSection3 = true; end

	-- Modifications
	local bSection5 = bSection5 or nModifications > 0;
	if bReadOnly and nModifications > 0 then
		modifications_label.setVisible(bID);
		iadd_modification.setVisible(false);
		item_modifications.update(bID);
	elseif bReadOnly then
		modifications_label.setVisible(false);
		iadd_modification.setVisible(false);
	else
		modifications_label.setVisible(bID);
		iadd_modification.setVisible(bID);
		item_modifications.update(false);
	end
	-- item_modifications.setVisible(true);

	local bSection4 = false;
	if updateControl("init", bReadOnly, bID and (bWeapon or bArmor)) then bSection4 = true; end
	if updateControl("bonus", bReadOnly, bID) then 
		bSection4 = true; 
		local bAux = (not bReadOnly or bonus_aux.getValue() ~= 0 or not bWeapon);
		bonus_aux.setVisible(bAux);
	end
	if updateControl("properties", bReadOnly, bID and (bAmmunition or bArmor)) then bSection4 = true; end


	-- Ammunition
	if updateControl("projectiles", bReadOnly, bID and (bRanged or bAmmunition)) then bSection4 = true; end

	-- Armors
	if updateControl("protection", bReadOnly, bID and bArmor) then bSection4 = true; end
	if updateControl("limit", bReadOnly, bID and bArmor) then bSection4 = true; end

	-- Weapons
	if bReadOnly and nAttacks > 0 then
		attacks_label.setVisible(bWeapon and bID);
		iadd_attacks.setVisible(false);
		item_attacks.update(bWeapon and bID);
	elseif bReadOnly then
		attacks_label.setVisible(false);
		iadd_attacks.setVisible(false);
	else
		attacks_label.setVisible(bWeapon and bID);
		iadd_attacks.setVisible(bWeapon and bID);
		item_attacks.update(false);
	end
	item_attacks.setVisible(bWeapon and bID);

	-- Magic
	local bSection5 = false;
	if updateControl("essence", bReadOnly, bID and not bService) then bSection5 = true; end
	if updateControl("rune_space", bReadOnly, bID and not bService) then bSection5 = true; end

	-- Runes
	local bSection5 = bSection5 or nRunes > 0;
	if not bID then
		runes_label.setVisible(false);
		iadd_runes.setVisible(false);
		item_runes.update(false);
	elseif bReadOnly and nRunes > 0 then
		runes_label.setVisible(bID);
		iadd_runes.setVisible(false);
		item_runes.update(bID);
	elseif bReadOnly then
		runes_label.setVisible(false);
		iadd_runes.setVisible(false);
	else
		runes_label.setVisible(bID);
		iadd_runes.setVisible(bID);
		item_runes.update(false);
	end
	-- item_runes.setVisible(true);
	number_runes.setValue(nRunes);

	-- Powers
	local bSection5 = bSection5 or nPowers > 0;
	if bReadOnly and nPowers > 0 then
		powers_label.setVisible(bID);
		iadd_power.setVisible(false);
		item_powers.update(bID);
	elseif bReadOnly then
		powers_label.setVisible(false);
		iadd_power.setVisible(false);
	else
		powers_label.setVisible(bID);
		iadd_power.setVisible(bID);
		item_powers.update(false);
	end
	-- item_powers.setVisible(true);

	-- Effects
	local bSection5 = bSection5 or nEffects > 0;
	if bReadOnly and nEffects > 0 then
		effects_label.setVisible(bID);
		iadd_effect.setVisible(false);
		item_effects.update(bID);
	elseif bReadOnly then
		effects_label.setVisible(false);
		iadd_effect.setVisible(false);
	else
		effects_label.setVisible(bID);
		iadd_effect.setVisible(bID);
		item_effects.update(false);
	end
	item_effects.setVisible(bID);
	number_effects.setValue(nEffects)

	local bSection6 = bID;
	description.setVisible(bID);
	description.setReadOnly(bReadOnly);

	divider.setVisible(bSection1 and bSection2);
	divider2.setVisible((bSection1 or bSection2) and bSection3);
	divider3.setVisible((bSection1 or bSection2 or bSection3) and bSection4);
	divider4.setVisible((bSection1 or bSection2 or bSection3 or bSection4) and bSection5);
	divider5.setVisible((bSection1 or bSection2 or bSection3 or bSection4 or bSection5) and bSection6);

	-- Original item
	-- local sOriginal = DB.getValue(nodeRecord, "original_path", "");
	-- link_original.setVisible(User.isHost() and (sOriginal ~= ""));

end


function onDrop(x, y, draginfo)
	local sDragType = draginfo.getType();

	if sDragType == "shortcut" then
		local sLabel = draginfo.getDescription();
		local sClass, sRecord = draginfo.getShortcutData();
		local cond = sLabel and sClass and sRecord;
		if cond and (sClass == "itemtemplate") then
			local nodeRune = DB.findNode(sRecord);
			local sType = DB.getValue(nodeRune, "type", "");
			local sName = DB.getValue(nodeRune, "name", sLabel);

			local w;
			if sType == "template_rune" then
				w = item_runes.createWindow();
				local nLevel = DB.getValue(nodeRune, "level", 0);
				w.level.setValue(nLevel);
				w.name.setValue(sName);
				w.link.setValue(sClass, sRecord);
				w.link_identifier.setValue(sRecord);
			elseif sType == "template_upgrade" then
				w = item_modifications.createWindow();
				w.name.setValue(sName);
				w.link.setValue(sClass, sRecord);
				w.link_identifier.setValue(sRecord);
			elseif sType == "template_material" then
				local sMaterialOld = material.getValue():lower();
				local sMaterialBase = DB.getValue(nodeRune, "material_base", ""):lower();
				local bCond = (sMaterialOld == "") or (sMaterialBase == "") or (sMaterialBase == Interface.getString(""):lower()) or sMaterialBase:find(sMaterialOld);
				if bCond then
					material.setValue(sName);
					material_aux.setValue(sClass, sRecord);
					material_path.setValue(sRecord);
				else
					Debug.chat("unable to add material " .. sName .. " with base material " .. sMaterialBase ..  " to item with base material " .. sMaterialOld );
				end
			else
				Debug.chat("unable to add modification " .. sName .. " of type " .. sType );
			end
			ItemManager2.updateModifiedItem(getDatabaseNode(), true);
			return true;
		end
	end
end
