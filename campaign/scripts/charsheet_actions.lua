--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onDrop(x, y, draginfo)
    if contents.subwindow.actions.subwindow.onDrop(x, y, draginfo) then
        return true;
    end
    if contents.subwindow.weapons.onDrop(x, y, draginfo) then
        return true;
    end
    return false;
end

function updateView()
    local sMode = DB.getValue(getDatabaseNode(), "powermode", "");
    local bMethodic = DB.getValue(getDatabaseNode(), "magic.methodic", 0) == 1;
    local bNotEdit = not (actions_iedit.getValue() == 1);

    vigor_icon.setVisible(bNotEdit and sMode~="preparation");
    vigor.setVisible(bNotEdit and sMode~="preparation");
    essence_icon.setVisible(bNotEdit and sMode~="preparation");
    essence.setVisible(bNotEdit and sMode~="preparation");
    vit_icon.setVisible(bNotEdit and sMode~="preparation");
    vit.setVisible(bNotEdit and sMode~="preparation");

    material_icon.setVisible(bNotEdit and sMode~="combat");
    material.setVisible(bNotEdit and material~="combat");
    gem_copper_icon.setVisible(bNotEdit);
    gem_copper.setVisible(bNotEdit);
    gem_bronze_icon.setVisible(bNotEdit);
    gem_bronze.setVisible(bNotEdit);
    gem_silver_icon.setVisible(bNotEdit);
    gem_silver.setVisible(bNotEdit);
    gem_gold_icon.setVisible(bNotEdit);
    gem_gold.setVisible(bNotEdit);

    local nodeChar = getDatabaseNode();
    local bMethodic = ActorManager2.isMethodicChanneler(nodeChar);

    prepared_spells.setVisible(bMethodic and bNotEdit and sMode=="preparation" and bMethodic);
    prepared_icon.setVisible(bMethodic and bNotEdit and sMode=="preparation" and bMethodic);
    prepared_spells_max.setVisible(bMethodic and bNotEdit and sMode=="preparation" and bMethodic);
    prepared_spells_sep.setVisible(bMethodic and bNotEdit and sMode=="preparation" and bMethodic);

    contents.subwindow.onModeChanged();
end

function onInit()
    CharManager.setShowInit(getDatabaseNode());
    CharManager.setShowSpeed(getDatabaseNode());
    updateView();
end