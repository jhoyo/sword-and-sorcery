--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	update();
end


function VisDataCleared()
	update();
end

function InvisDataAdded()
	update();
end

function updateControl(sControl, bReadOnly, bID)
	if not self[sControl] then
		return false;
	end

	if not bID then
		return self[sControl].update(bReadOnly, true);
	end

	return self[sControl].update(bReadOnly);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);
	local sType = DB.getValue(nodeRecord, "reftype", "");
	local bFlaw = sType == "type_aptitude_flaw";

	local bSection1 = false;
	if updateControl("extra_exp", bReadOnly, true) then bSection1 = true; end
	if updateControl("level", bReadOnly, true) then bSection1 = true; end
	if updateControl("requirements", bReadOnly, true) then bSection1 = true; end
	if bFlaw then
		extra_exp_label.setValue(Interface.getString("label_bonus_exp"));
	else
		extra_exp_label.setValue(Interface.getString("label_extra_exp"));
	end

	local bSection2 = DB.getValue(nodeRecord, "text", "") ~= "";
	text.setVisible(bSection1);
	text.setReadOnly(bReadOnly);

	divider1.setVisible(bSection1 and bSection2);
end

