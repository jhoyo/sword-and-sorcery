--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onListChanged()
  update();
  if listheader and listheader[1] and not isVisible() then
    window[listheader[1]].setVisible(true);
  end
end

function update()
  local bEdit = (window.parentcontrol.window.actions_iedit.getValue() == 1);
  for _,w in ipairs(getWindows()) do
    w.idelete.setVisibility(bEdit);
  end
end

function addEntry(bFocus)
  local w = createWindow();
  if bFocus then
    w.name.setFocus();
  end
  return w;
end


function addAptitudeReference(nodeSource)
  if not nodeSource then
    return;
  end

  local sName = StringManager.trim(DB.getValue(nodeSource, "name", ""));
  if sName == "" then
    return;
  end

  local wAptitude = nil;
  local sAbility = "";
  for _,w in pairs(getWindows()) do
    if StringManager.trim(w.name.getValue()) == sName then
      wAptitude = w;
      break;
    end
  end
  if wAptitude then
    local nodeAptitude = wAptitude.getDatabaseNode();
    local nCategory =  DB.getValue(nodeAptitude, "category", 1) + 1;
    DB.setValue(nodeAptitude, "category", "number", nCategory);

  else
    wAptitude = addEntry();
    wAptitude.name.setValue(sName);
    wAptitude.category.setValue(1);
    local nodeNew = wAptitude.getDatabaseNode();
    DB.copyNode(nodeSource, nodeNew);
  	DB.setValue(nodeNew, "locked", "number", 1);
  end
end


-- function onDrop(x, y, draginfo)
--   if draginfo.isType("shortcut") then
--     local sClass, sRecord = draginfo.getShortcutData();
--     Debug.chat('sClass',sClass)
--
--     if StringManager.contains({"reference_technique", "technique"}, sClass) then
--       CharManager.addInfoDB(getDatabaseNode(), sClass, sRecord);
--       return true;
--     end
--   end
-- end
