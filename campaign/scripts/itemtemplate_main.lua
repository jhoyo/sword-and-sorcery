--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	update();
	calculateRuneCost();
end

function calculateRuneCost()
	local node = getDatabaseNode();
	local sType = DB.getValue(node, "type", "");
	local sCost = DB.getValue(node, "cost_add", "");
	if sType == "template_rune" and sCost == "" then
		local nLevel = DB.getValue(node, "level", 0);
		local sKey = DB.getValue(node, "features", "");
		sCost = ItemManager2.calculateRuneCost(nLevel, sKey);
		DB.setValue(node, "cost_add", "string", sCost);
	end
end



function updateControl(sControl, bReadOnly, bShow)
	if not self[sControl] then
		return false;
	end

	if not self[sControl].update then
		Debug.chat("missing ", sControl)
	end

	return self[sControl].update(bReadOnly, not bShow);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);
	local sWeapon = Interface.getString("item_weapon");

	-- Conditions
	local bRune =  DB.getValue(nodeRecord, "type", "") == "template_rune";
	local bMaterial =  DB.getValue(nodeRecord, "type", "") == "template_material";
	local sString = DB.getValue(nodeRecord, "subtype", ""):lower();
	local bArmor = sString:find(Interface.getString("item_armor"));
	local bTool = sString:find(Interface.getString("item_tool"));
	local bWeapon = sString == sWeapon or sString:find(sWeapon .. " ");
	-- local nPowers = item_powers.getWindowCount();
	-- local nEffects = item_effects.getWindowCount();
	
	local bSection = false;
	if updateControl("subtype", bReadOnly, true) then bSection = true; end
	if updateControl("material_base", bReadOnly, bMaterial) then bSection = true; end
	if updateControl("natural", bReadOnly, bMaterial) then bSection = true; end
	if updateControl("level", bReadOnly, bRune) then bSection = true; end
	if updateControl("spell", bReadOnly, bRune) then bSection = true; end
	if updateControl("features", bReadOnly, bRune) then bSection = true; end

	local bSection2 = false;
	if updateControl("cost_add", bReadOnly, not bMaterial) then bSection2 = true; end
	if updateControl("rarity", bReadOnly, not bRune) then bSection2 = true; end
	if updateControl("weight", bReadOnly, true) then 
		bSection2 = true; 
		local bAux = bRune and (not bReadOnly or weight_aux.getValue() ~= 0);
		weight_sep.setVisible(bAux);
		weight_aux.setVisible(bAux);
	end
	if updateControl("cost_mult", bReadOnly, not bRune) then bSection2 = true; end
	if updateControl("hardness", bReadOnly, true) then 
		bSection2 = true; 
		local bAux = bRune and (not bReadOnly or hardness_aux.getValue() ~= 0);
		hardness_sep.setVisible(bAux);
		hardness_aux.setVisible(bAux);
	end
	if updateControl("dents", bReadOnly, not bRune) then bSection2 = true; end

	
	if updateControl("bonus_def", bReadOnly, bWeapon) then 
		bSection3 = true; 
		local bAux = bRune and (not bReadOnly or bonus_def_aux.getValue() ~= 0);
		bonus_def_sep.setVisible(bAux);
		bonus_def_aux.setVisible(bAux);
	end

	
	if updateControl("range_attack", bReadOnly, bWeapon) then 
		bSection3 = true; 
		local bAux = bRune and (not bReadOnly or range_attack_aux.getValue() ~= 0);
		range_attack_sep.setVisible(bAux);
		range_attack_aux.setVisible(bAux);
	end

	local bSection3 = false;
	if updateControl("init", bReadOnly, (bWeapon or bArmor)) then 
		bSection3 = true;
		local bAux = bRune and (not bReadOnly or init_aux.getValue() ~= 0);
		init_sep.setVisible(bAux);
		init_aux.setVisible(bAux);
	end
	if updateControl("properties", bReadOnly, true) then bSection3 = true; end
	if updateControl("properties_perheight", bReadOnly, (bWeapon or bArmor) and bRune) then bSection3 = true; end
	if updateControl("bonus", bReadOnly, (bWeapon or bArmor or bTool)) then 
		bSection3 = true; 
		local bAux = bRune and (not bReadOnly or bonus_aux.getValue() ~= 0);
		bonus_sep.setVisible(bAux);
		bonus_aux.setVisible(bAux);
	end
	if updateControl("bonus_def", bReadOnly, bWeapon) then 
		bSection3 = true; 
		local bAux = bRune and (not bReadOnly or bonus_def_aux.getValue() ~= 0);
		bonus_def_sep.setVisible(bAux);
		bonus_def_aux.setVisible(bAux);
	end
	if updateControl("damage_cat", bReadOnly, bWeapon) then 
		bSection3 = true;
		local bAux = bRune and (not bReadOnly or damage_cat_aux.getValue() ~= 0);
		damage_cat_sep.setVisible(bAux);
		damage_cat_aux.setVisible(bAux);
	end
	if updateControl("damage_dice", bReadOnly, bWeapon) then 
		bSection3 = true;
		local bAux = bRune and (not bReadOnly or damage_dice_aux.getValue() ~= 0);
		damage_dice_sep.setVisible(bAux);
		damage_dice_aux.setVisible(bAux);
	end
	if updateControl("damagetype", bReadOnly, bWeapon) then bSection3 = true; end

	if updateControl("protection", bReadOnly, bArmor) then bSection3 = true; end
	if updateControl("protection_perheight", bReadOnly, bArmor and bRune) then bSection3 = true; end
	if updateControl("limit", bReadOnly, bArmor) then 
		bSection3 = true; 
		local bAux = bRune and (not bReadOnly or limit_aux.getValue() ~= 0);
		limit_sep.setVisible(bAux);
		limit_aux.setVisible(bAux);
	end

	local bSection4 = false;
	if updateControl("essence", bReadOnly, bRune) then 
		bSection4 = true; 
		local bAux = bRune and (not bReadOnly or essence.getValue() ~= 0);
		essence_sep.setVisible(bAux);
		essence_aux.setVisible(bAux);
	end
	if updateControl("rune_space", bReadOnly, true) then 
		bSection4 = true; 
		local bAux = bRune and (not bReadOnly or rune_space_aux.getValue() ~= 0);
		rune_space_sep.setVisible(bAux);
		rune_space_aux.setVisible(bAux);
	end
	-- local bSection4 = bSection4 or nPowers > 0;
	-- if bReadOnly and nPowers > 0 then
	-- 	powers_label.setVisible(true);
	-- 	iadd_power.setVisible(false);
	-- 	item_powers.update(true);
	-- elseif bReadOnly then
	-- 	powers_label.setVisible(false);
	-- 	iadd_power.setVisible(false);
	-- else
	-- 	powers_label.setVisible(true);
	-- 	iadd_power.setVisible(true);
	-- 	item_powers.update(false);
	-- end
	-- item_powers.setVisible(true);
	-- number_runes.setValue(nPowers)

	-- local bSection4 = bSection4 or nEffects > 0;
	-- if bReadOnly and nEffects > 0 then
	-- 	effects_label.setVisible(true);
	-- 	iadd_effect.setVisible(false);
	-- 	item_effects.update(true);
	-- elseif bReadOnly then
	-- 	effects_label.setVisible(false);
	-- 	iadd_effect.setVisible(false);
	-- else
	-- 	effects_label.setVisible(true);
	-- 	iadd_effect.setVisible(true);
	-- 	item_effects.update(false);
	-- end
	-- item_effects.setVisible(true);
	-- number_effects.setValue(nEffects)

	description.setReadOnly(bReadOnly);

	divider.setVisible(bSection and bSection2);
	divider2.setVisible((bSection or bSection2) and bSection3);
	divider3.setVisible(bSection or bSection2 or bSection3);
	divider4.setVisible((bSection1 or bSection2 or bSection3) and bSection4);
	
end
