--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local bStoredReadOnly = false;

function onInit()
	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);
end

function onListChanged()
	update();
end

function update(bReadOnly, bForceHide)
	if type(bReadOnly) == "undefined" then
		bReadOnly = bStoredReadOnly;
	else
		bStoredReadOnly = bReadOnly;
	end

	setVisible(not bForceHide);
	for _,w in ipairs(getWindows()) do
		w.idelete.setVisibility(not bReadOnly);
		w.category.setReadOnly(bReadOnly);
		if bReadOnly then
			w.category.setFrame(nil);
		else
			w.category.setFrame("fielddark", 7,5,7,5);
		end
		w.damage.setReadOnly(bReadOnly);
		w.effect.setReadOnly(bReadOnly);
	end
end

function addEntry(bFocus)
	local w = createWindow();
	w.idelete.setVisibility(true);
	return w;
end

function onMenuSelection(item)
	if item == 5 then
		addEntry(true);
	end
end
