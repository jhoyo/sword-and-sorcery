--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function collapse()
	disadvantagestitle.collapse();
	generictitle.collapse();
	racialtitle.collapse();
	professiontitle.collapse();
	languagestitle.collapse();
end

function expand()
	disadvantagestitle.expand();
	generictitle.expand();
	racialtitle.expand();
	professiontitle.expand();
	languagestitle.expand();
end

-- function onDrop(x, y, draginfo)
-- 	if draginfo.isType("shortcut") then
-- 		local sClass, sRecord = draginfo.getShortcutData();
-- 		return CharManager.addInfoDB(getDatabaseNode(), sClass, sRecord);
-- 	end
-- end
function onDrop(x, y, draginfo)
	if draginfo.isType("shortcut") then
		local sClass, sRecord = draginfo.getShortcutData();
		if StringManager.contains({"reference_aptitude", "aptitude"}, sClass) then
			local node = draginfo.getDatabaseNode();
			local sType = DB.getValue(node, "reftype", "");
			if sType == "type_aptitude_flaw" then
				disadvantage.addAptitudeReference(node);
			elseif sType == "type_aptitude_generic" then
				generic.addAptitudeReference(node);
			elseif sType == "type_aptitude_racial" then
				racial.addAptitudeReference(node);
			elseif sType == "type_aptitude_professional" then
				profession.addAptitudeReference(node);
			end

		elseif StringManager.contains({"reference_technique", "technique"}, sClass) then
			CharManager.addInfoDB(getDatabaseNode(), sClass, sRecord);
		end
		return true;
	end
end
