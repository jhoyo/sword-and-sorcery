--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);
end

function update(bReadOnly)
	for _,w in ipairs(getWindows()) do
		w.idelete.setVisibility(not bReadOnly);
		w.recovery_period.setEnabled(not bReadOnly);
		w.charges.setEnabled(not bReadOnly);
		w.type.setEnabled(not bReadOnly);
	end
	setVisible(true);
end

function addEntry(bFocus)
	local w = createWindow();
	w.idelete.setVisibility(true);
	return w;
end

function onMenuSelection(item)
	if item == 5 then
		addEntry(true);
	end
end