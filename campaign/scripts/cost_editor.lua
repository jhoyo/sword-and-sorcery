--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
    updateData();
end

function updateData(bForced)
    local nodeAction = getDatabaseNode();
    if bForced or DB.getValue(nodeAction, "initialized_cost", 0) > 0 then
        DB.setValue(nodeAction, "initialized_cost", "number", 0);

        local nodePower = nodeAction.getChild("...");
        if not nodePower then
            return;
        end
        
        local sActions = DB.getValue(nodePower, "castingtime", "");
        local sCost = DB.getValue(nodePower, "vigor_cost", "");

        if sActions ~= DataCommon.standard then
            DB.setValue(nodeAction, "actions", "string", sActions);
        end
        if sCost ~= DataCommon.standard then
            DB.setValue(nodeAction, "cost", "string", sCost);
        end

    end

end

