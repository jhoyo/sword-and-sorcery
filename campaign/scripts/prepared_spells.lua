--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local aSkills = {"arcane_skill", "sacred_skill", "primal_skill", "psychic_skill", "elemental_skill"};

function onInit()
    local nodeChar = getDatabaseNode().getChild("...");
	DB.addHandler(DB.getPath(nodeChar, "abilities.intelligence.base"), "onUpdate", count);
	DB.addHandler(DB.getPath(nodeChar, "abilities.intelligence.damage"), "onUpdate", count);
	DB.addHandler(DB.getPath(nodeChar, "skilllist_magic.*.prof"), "onUpdate", count);
	DB.addHandler(DB.getPath(nodeChar, "professionlist.*.name"), "onUpdate", count);
	DB.addHandler(DB.getPath(nodeChar, "professionlist.*.category"), "onUpdate", count);
    count();
end

function onClose()
    local nodeChar = getDatabaseNode().getChild("...");
	DB.removeHandler(DB.getPath(nodeChar, "abilities.intelligence.base"), "onUpdate", count);
	DB.removeHandler(DB.getPath(nodeChar, "abilities.intelligence.damage"), "onUpdate", count);
	DB.removeHandler(DB.getPath(nodeChar, "skilllist_magic.*.prof"), "onUpdate", count);
	DB.removeHandler(DB.getPath(nodeChar, "professionlist.*.name"), "onUpdate", count);
	DB.removeHandler(DB.getPath(nodeChar, "professionlist.*.category"), "onUpdate", count);
end


function count()
    local nodeChar = getDatabaseNode().getChild("...");

    -- Get Intelligence value
    local nInt = ActorManager2.getCurrentAbilityValue(nodeChar, "intelligence");
    local nExtra = ActorManager2.getAptitudeCategory(nodeChar, DataCommon.aptitude["prepare_extra_spell"]);

    -- Get skill value
    local nSkill = 0;
    local nodeList = nodeChar.createChild("skilllist_magic");
    for _,sSkill in pairs(aSkills) do
        local nMast = DB.getValue(nodeList, sSkill .. ".prof", 0);
        nSkill = nSkill + ActorManager2.getLevel(nodeChar, nMast);
    end

    -- Set values
    setValue(math.floor(nSkill + nInt + nExtra));
    setTooltipText(Interface.getString("power_tooltip_prepared_spells") .. tostring(nInt + nExtra))
end