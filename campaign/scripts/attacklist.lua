--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	DB.addHandler(DB.getPath(getDatabaseNode()), "onChildAdded", onChildAdded);
	registerMenuItem(Interface.getString("list_menu_createattack"), "insert", 5);

	-- Remove empty attacks
	-- removeEmpty();
	-- -- Start with one empty item if there is none
	-- constructDefault();
end

function onClose()
	DB.removeHandler(DB.getPath(getDatabaseNode()), "onChildAdded", onChildAdded);
end

function onListChanged()
	update();
end

function onChildAdded()
	applyFilter();
	update();
end

function onFilter(w)
	return true;
end

function update()
	applyFilter();
	for _,w in ipairs(getWindows()) do
		if w.idelete then
			w.idelete.setVisibility(true);
		end
	end
end

function addEntry(bFocus)
	local w = createWindow();
end

function onMenuSelection(item)
	if item == 5 then
		addEntry(true);
	end
end

-- Create default skill selection
function constructDefault()
	-- Collect existing entries
	if not getWindows() then
		addEntry(true);
	end
end

-- Remove empty attacks on init
function removeEmpty()
	for _,w in ipairs(getWindows()) do
		if w.damageview.getValue() == "" then
			w.getDatabaseNode().delete();
		end
	end
end
