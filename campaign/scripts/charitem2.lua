--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	if super and super.onInit then
		super.onInit();
	end
	-- Debug.chat("node", getDatabaseNode())
	-- updateEffects();
	-- local nodeRecord = getDatabaseNode();
	-- DB.addHandler(DB.getPath(nodeRecord, "carried"), "onUpdate", updateEffects);

	registerMenuItem(Interface.getString("char_menuitem_buy"), "windowexport", 7);
	registerMenuItem(Interface.getString("option_val_1"), "windowexport", 7, 1);
	registerMenuItem(Interface.getString("option_val_2"), "num2", 7, 2);
	registerMenuItem(Interface.getString("option_val_3"), "num3", 7, 4);
	registerMenuItem(Interface.getString("option_val_4"), "num4", 7, 5);
	registerMenuItem(Interface.getString("option_val_5"), "num5", 7, 6);
	registerMenuItem(Interface.getString("option_val_10"), "num10", 7, 7);
	registerMenuItem(Interface.getString("option_val_20"), "num20", 7, 8);

	registerMenuItem(Interface.getString("char_menuitem_sell"), "shortcutson", 8);
	registerMenuItem(Interface.getString("option_val_1"), "shortcutson", 8, 1);
	registerMenuItem(Interface.getString("option_val_2"), "num2", 8, 2);
	registerMenuItem(Interface.getString("option_val_3"), "num3", 8, 3);
	registerMenuItem(Interface.getString("option_val_4"), "num4", 8, 5);
	registerMenuItem(Interface.getString("option_val_5"), "num5", 8, 6);
	registerMenuItem(Interface.getString("option_val_10"), "num10", 8, 7);
	registerMenuItem(Interface.getString("option_val_20"), "num20", 8, 8);

	updateBonding(getDatabaseNode());

	DB.addHandler(DB.getPath(getDatabaseNode(), "essence"), "onUpdate", updateBonding);
	DB.addHandler(DB.getPath(getDatabaseNode(), "count"), "onUpdate", updateLinkedPowerCharges);

	updateIcon();
end

function onClose()
	DB.removeHandler(DB.getPath(getDatabaseNode(), "essence"), "onUpdate", updateBonding);
	DB.removeHandler(DB.getPath(getDatabaseNode(), "count"), "onUpdate", updateLinkedPowerCharges);
end

function onDelete()
	updateSpellMaterials();
end


function onMenuSelection(selection, subselection)
	if super and super.onMenuSelection then
		super.onMenuSelection(selection, subselection);
	end

	local node = getDatabaseNode();
	local nodeChar = node.getChild("...");
	if DB.getValue(nodeChar, "is_mount", 0) > 0 then
		nodeChar = nodeChar.getParent();
	end
	if selection == 7 then
		local nNum = 0;
		if subselection < 3 then
			nNum = subselection;
		elseif subselection < 7 then
			nNum = subselection - 1;
		elseif subselection == 7 then
			nNum = 10;
		elseif subselection == 8 then
			nNum = 20;
		end
		ItemManager2.buyItem(node, nNum, nodeChar, true);

	elseif selection == 8 then
		local nNum = 0;
		if subselection < 4 then
			nNum = subselection;
		elseif subselection < 7 then
			nNum = subselection - 1;
		elseif subselection == 7 then
			nNum = 10;
		elseif subselection == 8 then
			nNum = 20;
		end
		ItemManager2.sellItem(node, nNum, nodeChar);
	end
end

function updateSpellMaterials()
	local sName = name.getValue();
	local bCarried = carried.getValue() > 0;
	local nCount = 0;
	local nodeChar = getDatabaseNode().getChild("...");

	if bCarried then
		nCount = count.getValue();
	end
	if sName == DataCommon.magic_materials then
		DB.setValue(nodeChar, "items.material", "number", nCount);
	elseif sName == DataCommon.magic_gems[0] then
		DB.setValue(nodeChar, "items.gem_0", "number", nCount);
	elseif sName == DataCommon.magic_gems[3] then
		DB.setValue(nodeChar, "items.gem_1", "number", nCount);
	elseif sName == DataCommon.magic_gems[6] then
		DB.setValue(nodeChar, "items.gem_2", "number", nCount);
	elseif sName == DataCommon.magic_gems[9] then
		DB.setValue(nodeChar, "items.gem_3", "number", nCount);
	end
	updateIcon();
end

function updateIcon()
	
	local sName = name.getValue();
	if sName == DataCommon.magic_materials then
		icon.setVisible(true);
		icon.setIcon("component_material");
	elseif sName == DataCommon.magic_gems[0] then
		icon.setVisible(true);
		icon.setIcon("component_gem_0");
	elseif sName == DataCommon.magic_gems[3] then
		icon.setVisible(true);
		icon.setIcon("component_gem_1");
	elseif sName == DataCommon.magic_gems[6] then
		icon.setVisible(true);
		icon.setIcon("component_gem_2");
	elseif sName == DataCommon.magic_gems[9] then
		icon.setVisible(true);
		icon.setIcon("component_gem_3");
	else
		icon.setVisible(false);
		icon.setIcon("");
	end
end

function updateBonding(node)
	if not node then return; end

	local sPath = node.getNodeName();
	local nEssence = 0;
	if sPath:find("essence") then
		nEssence = node.getValue();
		node = node.getParent();
	else
		nEssence = DB.getValue(node, "essence", 0);
	end
	if nEssence == 0 then
		no_bond.setVisible(true);
		bond.setVisible(false);
	else
		no_bond.setVisible(false);
		bond.setVisible(true);
	end
end

function updateLinkedPowerCharges()
	local node = getDatabaseNode();
	local nodePowers = node.createChild("powers");
	for _,nodePower in pairs(nodePowers.getChildren()) do
		local sLink = DB.getValue(nodePower, "link_power", "");
		local sType = DB.getValue(nodePower, "type", "");
		if sType == "" then
			local nodeSpell = DB.findNode(sLink);
			if nodeSpell then
				local nCount = DB.getValue(node, "count", 0);
				DB.setValue(nodeSpell, "charges", "number", nCount);
				DB.setValue(nodeSpell, "cast", "number", 0);
			end
		end
	end
end



function onDelete(source)
	local sPath = DB.getValue(getDatabaseNode(), "original_path", "");
	if sPath ~= "" then
		Debug.chat("Borrando ", sPath)
		DB.deleteNode(sPath);
	end
end

