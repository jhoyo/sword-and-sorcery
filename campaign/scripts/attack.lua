--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	setRadialOptions();
	local nodeAttack = getDatabaseNode();
	local nodeChar = nodeAttack.getChild(".....");
	DB.addHandler(nodeAttack.getNodeName(), "onChildUpdate", onDataChanged);
	DB.addHandler(DB.getPath(nodeChar, "abilities.*.current"), "onUpdate", onDataChanged);
	DB.addHandler(DB.getPath(nodeChar, "skilllist_weaponary"), "onChildUpdate", onDataChanged);

	onDataChanged();
end

function onClose()
	local nodeAttack = getDatabaseNode();
	local nodeChar = nodeAttack.getChild(".....");
	DB.removeHandler(nodeAttack.getNodeName(), "onChildUpdate", onDataChanged);
	DB.removeHandler(DB.getPath(nodeChar, "abilities.*.current"), "onUpdate", onDataChanged);
	DB.removeHandler(DB.getPath(nodeChar, "skilllist_weaponary"), "onChildUpdate", onDataChanged);
end

function onMenuSelection(selection, subselection)
	if selection == 6 and subselection == 7 then
		local node = getDatabaseNode();
		if node then
			node.delete();
		else
			close();
		end
	end
end

function setRadialOptions()
	resetMenuItems();

	registerMenuItem(Interface.getString("list_menu_deleteattack"), "delete", 6);
	registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 6, 7);
end

function onDataChanged()
	-- onLinkChanged();
	checkAtribute();
	onOtherChanged();
	onAttackChanged(true);
	onDamageChanged();

	local nodeChar = getDatabaseNode().getChild(".....");
	ActorManager2.updateDefenses(nodeChar);
end

function getNode()
	return getDatabaseNode();
end

function onAttackChanged(bUpdateDefenses)
	-- Get character nodes
	local nodeAttack = getDatabaseNode();
	local nodeWeapon = nodeAttack.getChild("...");
	local nodeChar = nodeAttack.getChild(".....");
	local _, sPath = DB.getValue(nodeWeapon, "shortcut", "");
	if sPath and sPath ~= "" then
		local nodeItem = DB.findNode(sPath);

		-- Get atribute value
		local sAbility = DB.getValue(nodeAttack, "attack_ability", ""):lower();
		local nAbility = 0;
		if sAbility ~= "" then
			nAbility = ActorManager2.getAbility(nodeChar, sAbility);
		end

		-- Get skill value
		local nodeSkill, nodeSpecialty;
		local nHands = DB.getValue(nodeWeapon, "hands", 0);
		local bSidehand = false;
		if nHands == 1 then
			bSideHand = true;
		end

		nodeSkill, _, nodeSpecialty = ActorManager2.getSkillNode(nodeChar, DB.getValue(nodeAttack, "attack_skill", ""));
		local nSkill = ActorManager2.getSkillTotal(nodeSkill, nodeSpecialty, bSideHand);

		-- Get penalizer, state modifiers and weapon bonus
		local nPen = ActorManager2.getPenalizer(nodeChar);
		local nState = DB.getValue(nodeWeapon, "attack_state", 0);
		local nBonus = DB.getValue(nodeItem, "bonus", 0);
		local nDamage = DB.getValue(nodeItem, "dents_aux", 0);
		local nMisc = DB.getValue(nodeAttack, "attack_misc", 0);
		local nProps = DB.getValue(nodeAttack, "attack_props", 0);

		attackview.setValue(nAbility + nSkill + nState + nPen + nBonus + nMisc + nProps - nDamage);

		if bUpdateDefenses then
			local nodeChar = nodeAttack.getChild(".....");
			local nHands = DB.getValue(nodeWeapon, "hands", 0);
			local sHands = "main_hand";
			local bTwoHands = nHands == 2;
			if nHands == 1 then
				sHands = "side_hand";
			end
			ActorManager2.updateDefenses(nodeChar, nodeWeapon, sHands, bTwoHands);
		end
	end
end

function onDamageChanged()
	-- Get character nodes
	local nodeAttack = getDatabaseNode();
	local sDamage = DB.getValue(nodeAttack, "damageview", "")

	damageview.setValue(sDamage);
end

function onOtherChanged()
	-- Get nodes
	local nodeAttack = getDatabaseNode();
	local nodeWeapon = nodeAttack.getChild("...");

	-- Update properties
	local sProperties = DB.getValue(nodeAttack, "properties", "")
	if properties then
		properties.setValue(sProperties);
	end
	-- Update related variables
	local aProp = ItemManager2.scanProperties(sProperties);
	local nVar = aProp["attack"] + aProp["length"];
	if aProp["large"] then
		nVar = nVar - 1;
	end
	local nVar = aProp["attack"] + aProp["length"];
	if aProp["large"] then
		nVar = nVar - 1;
	end
	DB.setValue(nodeAttack, "attack_props", "number", nVar);
end

function onAttackAction(draginfo)
	-- Get nodes
	local nodeAttack = getDatabaseNode();
	local nodeWeapon = nodeAttack.getChild("...");
	local nodeChar = nodeAttack.getChild(".....");
	local rActor = ActorManager.resolveActor(nodeChar);
	-- local sPath = DB.getValue(nodeWeapon, "item_path", "");
	local _, sPath = DB.getValue(nodeWeapon, "shortcut", "");
	local nodeItem = DB.findNode(sPath);


	-- Get the attack modifiers
	local rAction = {};
	rAction.label = DB.getValue(nodeWeapon, "name", "");
	rAction.hand = DB.getValue(nodeWeapon, "hands", 0);
	rAction.ability = DB.getValue(nodeAttack, "attack_ability", "");
	rAction.skill = DB.getValue(nodeAttack, "attack_skill", "");
	rAction.type = DB.getValue(nodeAttack, "type_attack", 0);
	rAction.range = DB.getValue(nodeAttack, "range_attack", 0);
	rAction.modifier = DB.getValue(nodeItem, "bonus", 0) - DB.getValue(nodeItem, "dents_aux", 0) + DB.getValue(nodeAttack, "attack_misc", 0);
	rAction.bWeapon = true;

	-- Decrement ammo
	if rAction.type > 0 then
		local sFieldAmmo = "recharge";
		if rAction.type == 2 then
			sFieldAmmo = "maxammo";
		end
		local nAvailableAmmo = DB.getValue(nodeWeapon, sFieldAmmo, 0);
		if nAvailableAmmo < 1 then
			local msg = {font = "msgfont", icon = "ammo", mode="chat_miss", text=Interface.getString("char_message_atkwithnoammo")};
			Comm.deliverChatMessage(msg);
			return;
		else
			DB.setValue(nodeWeapon, sFieldAmmo, "number", nAvailableAmmo - 1);
		end
	end

	-- Add properties modifiers
	local sProp = DB.getValue(nodeAttack, "properties", "");
	local aProp;
	rAction, aProp = ItemManager2.addPropertiesToAction(rAction, sProp);
	rAction.bOpportunity = Input.isControlPressed();

	-- Spend actions
	if not Input.isShiftPressed() and CombatManager2.inCombat() then
		nodeEntry = ActorManager2.getCTNode(nodeChar);
		if CombatManager2.isCurrentTurn(nodeEntry) then
			local bCond = rAction.hand == 1 and (aProp["light"] or Utilities.inArray(DataCommon.shields, rAction.skill))
			if aProp["short"] or bCond then
				CombatManager2.useAction(nodeEntry, "action", true, true);
			else
				CombatManager2.useAction(nodeEntry, "action", false, true);
				DB.setValue(nodeEntry, "action_transformed", "number", 0);
			end
		else
			CombatManager2.useAction(nodeEntry, "reaction", false, true);
			rAction.bOpportunity = true;
		end
	end

	-- Perform roll
	ActionAttack.performRoll(draginfo, rActor, rAction);
	onAttackChanged(true);

	return true;
end



function onDamageAction(draginfo)
	-- Get nodes
	local nodeAttack = getDatabaseNode();
	local nodeWeapon = nodeAttack.getChild("...");
	local nodeChar = nodeAttack.getChild(".....");
	local rActor = ActorManager.resolveActor(nodeChar);
	local sProp = DB.getValue(nodeAttack, "properties", "");
	local sDamage = DB.getValue(nodeAttack, "damageview", "");

	local rAction = {};
	rAction.bWeapon = true;
	rAction.label = DB.getValue(nodeWeapon, "name", "");
	local nType = DB.getValue(nodeAttack, "type_attack", 0);
	local nType = DB.getValue(nodeAttack, "type_attack", 0);
	if nType == 2 then
		rAction.attackType = DataCommon.type_thrown;
	elseif nType == 1 then
		rAction.attackType = DataCommon.type_ranged;
	else
		rAction.attackType = DataCommon.type_melee;
	end
	rAction.range = DB.getValue(nodeAttack, "range", 0);

	-- Check for weapon properties
	local aProps = ItemManager2.scanProperties(sProp);
	rAction.nReroll = aProps["reroll"];
	rAction.nPiercing = aProps["piercing"];
	rAction.bIncorporeal = aProps["incorporeal"];

	-- Analyze damage string
	rAction.aDice, rAction.nMod, rAction.sDamageTypes = Utilities.analyzeDiceString(sDamage, true);

	ActionDamage.performRoll(draginfo, rActor, rAction);
	return true;
end

function checkAtribute()
	-- Get properties
	local nodeAttack = getDatabaseNode();
	local nodeChar = nodeAttack.getChild(".....");
	local sProp = DB.getValue(nodeAttack, "properties", "");
	local nRange = DB.getValue(nodeAttack, "type_attack", 0);
	local aProps = ItemManager2.scanProperties(sProp);

	-- Set correct atribute
	local sStat = DataCommon.abilities_translation_inv["strength"];
	if aProps["stat"] ~= "" then
		sStat = aProps["stat"];
	elseif aProps["gift"] then
		sStat = ActorManager2.getBestGift(nodeChar);
	elseif nRange == 1 then
		sStat = DataCommon.abilities_translation_inv["dexterity"];
	else
		local nCat = ActorManager2.getAptitudeCategory(nodeChar, DataCommon.aptitude["weapon_finesse"])
		local bCond1 = aProps["short"] or aProps["finesse"];
		local bCond2 = aProps["light"] and nCat > 0;
		local bCond3 = (not aProps["two_hands"]) and nCat > 1;
		local bCond4 = aProps["two_hands"] and nCat > 2;
		if bCond1 or bCond2 or bCond3 or bCond4 then
			local nodeChar = nodeAttack.getChild(".....");
			local nStrength = ActorManager2.getCurrentAbilityValue(nodeChar, "strength");
			local nDex = ActorManager2.getCurrentAbilityValue(nodeChar, "dexterity");
			if nDex > nStrength then
				sStat = DataCommon.abilities_translation_inv["dexterity"];
			end
		end
	end
	DB.setValue(nodeAttack, "attack_ability", "string", StringManager.capitalize(sStat));
end
