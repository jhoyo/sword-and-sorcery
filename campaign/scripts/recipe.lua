--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	update();
end


function VisDataCleared()
	update();
end

function InvisDataAdded()
	update();
end

function updateControl(sControl, bReadOnly, bID)
	if not self[sControl] then
		return false;
	end

	if not bID then
		return self[sControl].update(bReadOnly, true);
	end

	return self[sControl].update(bReadOnly);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);
	local sPath = DB.getPath(nodeRecord);

	local bSection1 = false;
	if updateControl("skill", bReadOnly, true) then bSection1 = true; end
	if updateControl("category", bReadOnly, true) then bSection1 = true; end
	if updateControl("rarity", bReadOnly, true) then bSection1 = true; end
	if updateControl("equipment", bReadOnly, true) then bSection1 = true; end
	if updateControl("requirements", bReadOnly, true) then bSection1 = true; end
	if updateControl("supplies", bReadOnly, true) then bSection1 = true; end
	if updateControl("keywords", bReadOnly, true) then bSection1 = true; end
	if updateControl("cost", bReadOnly, true) then bSection1 = true; end

	local bSection2 = false;
	if updateControl("item_name", bReadOnly, true) then 
		bSection2 = true;
		item_link.setVisible(true);
	else
		item_link.setVisible(false);
	end
	item_create.setVisible(sPath:find("charsheet"))

	local bSection3 = DB.getValue(nodeRecord, "text", "") ~= "";
	text.setVisible(bSection3);
	text.setReadOnly(bReadOnly);

	divider1.setVisible(bSection1 and bSection2);
	divider2.setVisible((bSection1 or bSection2) and bSection3);
end

function onDrop(x, y, draginfo)
	local sDragType = draginfo.getType();

	if sDragType == "shortcut" then
		local sLabel = draginfo.getDescription();
		local sClass, sRecord = draginfo.getShortcutData();
		updateLink(sLabel, sClass, sRecord);
	end
end

function updateLink(sLabel, sClass, sRecord, nIndex)
	local node = getDatabaseNode();
	if sLabel and sClass and sRecord then
		item_link.setValue(sClass, sRecord);
		local node = DB.findNode(sRecord);
		local sName = DB.getValue(node, "name", "");
		item_name.setValue(sName);
	end
end
