--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
    updateView();
end

function updateView()
    -- Get data
    local nodeTech = getDatabaseNode();
    local nDis = DB.getValue(nodeTech, "disadvantage", 0);
    local nActions = DB.getValue(nodeTech, "actions_mod", 0);
    local nUnstable = DB.getValue(nodeTech, "unstable_mod", 0);
    local nUnstablePerlevel = DB.getValue(nodeTech, "unstable_mod_aux", 0);
    local nDif = DB.getValue(nodeTech, "dif_mod", 0);
    local nDifPerlevel = DB.getValue(nodeTech, "dif_mod_aux", 0);
    local nVigor = DB.getValue(nodeTech, "vigor_mod", 0);
    local nVigorPerlevel = DB.getValue(nodeTech, "vigor_mod_aux", 0);


    -- Create strings and update visibility
    local bX = false;

    disadvantage.setVisible(nDis ~= 0);
    actions_mod.setVisible(nActions ~= 0);
    
    if nUnstable ~= 0 then
        local sAux = tostring(nUnstable);
        if nUnstablePerlevel > 0 then
            sAux = sAux .. "X";
            bX = true;
        end
        unstable_string.setValue(sAux);
        unstable_string.setVisible(true);
    else
        unstable_string.setVisible(false);
    end

    if nDif  ~= 0 then
        local sAux = tostring(nDif);
        if nDifPerlevel > 0 then
            sAux = sAux .. "X";
            bX = true;
        end
        dif_string.setValue(sAux);
        dif_string.setVisible(true);
    else
        dif_string.setVisible(false);
    end

    if nVigor  ~= 0 then
        local sAux = tostring(nVigor);
        if nVigorPerlevel > 0 then
            sAux = sAux .. "X";
            bX = true;
        end
        vigor_string.setValue(sAux);
        vigor_string.setVisible(true);
    else
        vigor_string.setVisible(false);
    end

    if bX then
        active_cat.setVisible(true);
        active.setVisible(false);
    else
        active_cat.setVisible(false);
        active.setVisible(true);
    end

end