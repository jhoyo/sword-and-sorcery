--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--
function onInit()
    local nodeSkill = window.getDatabaseNode();
    local nodeChar = nodeSkill.getChild("...");
    DB.addHandler(DB.getPath(nodeChar, "health.penal.total"), "onUpdate", onSourceUpdate);
    DB.addHandler(DB.getPath(nodeChar, "level"), "onUpdate", onSourceUpdate);

    super.onInit();
    onSourceUpdate();
end

function onClose()
    local nodeSkill = window.getDatabaseNode();
    local nodeChar = nodeSkill.getChild("...");
    DB.removeHandler(DB.getPath(nodeChar, "health.penal.total"), "onUpdate", onSourceUpdate);
    DB.removeHandler(DB.getPath(nodeChar, "level"), "onUpdate", onSourceUpdate);
end

function onHover()
    onSourceUpdate();
end

function onSourceUpdate(node)
    local nodeSkill = window.getDatabaseNode();
    local nodeChar = nodeSkill.getChild("...");
    local sAtribute = DB.getValue(nodeSkill, "stat", ""):lower();

    -- Value
    local nTotal, nAdv, nState, _, bLimited, bLimitedSkill = ActorManager2.getCheck(nodeChar, sAtribute, nodeSkill);
    setValue(nTotal);

    -- Tooltip
    local sTooltip = Interface.getString("char_tooltip_skill");
    if nAdv > 0 then
        sTooltip = sTooltip .. Interface.getString("char_tooltip_skill_adv") .. tostring(nAdv);
    elseif nAdv < 0 then
        sTooltip = sTooltip .. Interface.getString("char_tooltip_skill_dis") .. tostring(-nAdv);
    end
    if nState ~= 0 then
        sTooltip = sTooltip .. Interface.getString("char_tooltip_skill_bonus") .. tostring(nState);
    end
    if bLimited then
        local nLimit = ActorManager2.getLimit(nodeChar, sAtribute);
        sTooltip = sTooltip .. Interface.getString("char_tooltip_skill_statlimited") .. tostring(nLimit);
    end
    if bLimitedSkill then
        local nTypeLimit = window.limited.getValue();
        local sSkillLimit = DataCommon.ability_limited_value[nTypeLimit];
        local nLimit = ActorManager2.getLimit(nodeChar, sSkillLimit);
        sTooltip = sTooltip .. Interface.getString("char_tooltip_skill_skilllimited") .. tostring(nLimit);
    end
    setTooltipText(sTooltip);

    -- Color
    bLimited = bLimited or bLimitedSkill;
    local sColor = DataCommon.vitality_colors[3]
    if nAdv == 0 and nState == 0 and not bLimited then
        sColor = "000000";
    elseif nAdv >= 0 and nState >= 0 and not bLimited then
        sColor = DataCommon.vitality_colors[1];
    elseif (nAdv <= 0 and nState <= 0) or (nAdv <= 0 and nState <= 0 and bLimited) then
        sColor = DataCommon.vitality_colors[6];
    end
    setColor(sColor);

end


function action(draginfo)
    local nodeSkill = window.getDatabaseNode();
    local nodeChar = nodeSkill.getChild("...");
    local rActor = ActorManager.resolveActor(nodeChar);
    local sName = DB.getValue(nodeSkill, "name", ""):lower();

    local rAction = {};
    if Input.isShiftPressed() or sName == DataCommon.save then
        rAction.type = "save";
    else
        rAction.type = "skill";
    end
    rAction.nodeSkill = nodeSkill;

    ActionSkill.performRoll(draginfo, rActor, rAction);
    return true;
end

function onDragStart(button, x, y, draginfo)
    return action(draginfo);
end

function onDoubleClick(x,y)
    return action();
end