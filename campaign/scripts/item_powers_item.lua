--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--
function onInit()
    resetMenuItems();
    registerMenuItem(Interface.getString("list_menu_deleteitem"), "delete", 6);
    registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 6, 7);

    DB.addHandler(DB.getPath(getDatabaseNode(), "type"), "onUpdate", updateView);
    updateView()
end

function onClose()
    DB.removeHandler(DB.getPath(getDatabaseNode(), "type"), "onUpdate", updateView);
end

function onDelete()
    local sPower = link_power.getValue();
    local nodePower = DB.findNode(sPower);
    if nodePower then
        nodePower.delete();
    end
end

function onMenuSelection(selection, subselection)
    if selection == 6 and subselection == 7 then
        local node = getDatabaseNode();
        if node then
            node.delete();
        else
            close();
        end
    end
end

function updateView()
    local sValue = type.getStringValue();
    if sValue == "" then
        recovery_period.setVisible(false);
        recovery.setVisible(false);
        charges.setVisible(false);
    elseif sValue == "item_power_type_charges" then
        recovery_period.setVisible(false);
        recovery.setVisible(false);
        charges.setVisible(true);
    else
        recovery_period.setVisible(true);
        recovery.setVisible(true);
        charges.setVisible(true);
    end
    local nodeItem = getDatabaseNode().getChild("...");
    level.setVisible(not ItemManager2.isScroll(nodeItem))
end

function onDrop(x, y, draginfo)
    local sDragType = draginfo.getType();

    if sDragType == "shortcut" then
        local sLabel = draginfo.getDescription();
        local sClass, sRecord = draginfo.getShortcutData();
        local cond = sLabel and sClass and sRecord;
        if cond and (sClass == "power") then
            local node = getDatabaseNode();
            local nodeItem = node.getChild("...");

            link.setValue(sClass, sRecord);
            local nodeSpell = DB.findNode(sRecord);
            local sName = DB.getValue(nodeSpell, "name", sLabel);
            local nLevel = DB.getValue(nodeSpell, "level", 0);

            DB.setValue(node, "name", "string", sName);
            DB.setValue(node, "level", "number", nLevel);
            DB.setValue(node, "link_identifier", "string", sRecord);
            
            if ItemManager2.isPotion(nodeItem) or ItemManager2.isPotion(nodeItem) then
                DB.setValue(node, "type", "string", "");
                DB.setValue(node, "charges", "number", 1);
                DB.setValue(node, "recovery", "number", 0);
            elseif ItemManager2.isWand(nodeItem) then
                DB.setValue(node, "type", "string", "item_power_type_charges");
                DB.setValue(node, "charges", "number", 5);
                DB.setValue(node, "recovery", "number", 0);
            elseif ItemManager2.isWand(nodeItem) then
                DB.setValue(node, "type", "string", "item_power_type_rec_charges");
                DB.setValue(node, "charges", "number", 2);
                DB.setValue(node, "recovery", "number", 1);
            end
            ItemManager2.updateModifiedItem(nodeItem, true);
            CharManager.addToPowerDB(nodeItem);
        end
    end
    updateView()
end

function onDelete()
    local sPath = DB.getValue(getDatabaseNode(), "link_power", "");
    if sPath ~= "" then
        local nodePower = DB.findNode(sPath);
        if nodePower then
            nodePower.delete();
        end
    end
end