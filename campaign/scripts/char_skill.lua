--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local iscustom = true;

function onInit()
	setRadialOptions();
end

function onMenuSelection(selection, subselection)
	if selection == 6 and subselection == 7 then
		local node = getDatabaseNode();
		if node then
			node.delete();
		else
			close();
		end
	elseif selection == 4 then
		specialties.addEntry(true);
	end
end

-- This function is called to set the entry to non-custom or custom.
-- Custom entries have configurable stats and editable labels.
function setCustom(state)
	iscustom = state;

	if iscustom then
		name.setEnabled(true);
		name.setLine(true);
	else
		name.setEnabled(false);
		name.setLine(false);
	end

	setRadialOptions();
end

function isCustom()
	return iscustom;
end

function setRadialOptions()
	resetMenuItems();
	registerMenuItem(Interface.getString("char_skills_radial_addsubskill"), "pointer", 4);

	if iscustom then
		registerMenuItem(Interface.getString("list_menu_deleteitem"), "delete", 6);
		registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 6, 7);
	end
end

function addEntry(bFocus)
	local w = createWindow();
	w.setCustom(true);
	w.type.setValue(Interface.getString("ref_type_specialty"));
	if bFocus and w then
		w.name.setFocus();
	end
	return w;
end


function onDrop(x, y, draginfo)
	if draginfo.isType("shortcut") then
		local sClass = draginfo.getShortcutData();
		if sClass == "skill" then
			local node = draginfo.getDatabaseNode();
			local sType = DB.getValue(node, "type", "")
			if sType == Interface.getString("ref_type_specialty") then
				specialties.addSpecialtyReference(node);
			else
				Debug.chat('Try to drag the skill not over an individual skill')
			end

		end
		return true;
	end
end
