--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
    if super and super.onInit then
        super.onInit();
    end

    registerMenuItem(Interface.getString("char_menuitem_buy"), "windowexport", 3);
    registerMenuItem(Interface.getString("option_val_1"), "windowexport", 3, 1);
    registerMenuItem(Interface.getString("option_val_2"), "num2", 3, 2);
    registerMenuItem(Interface.getString("option_val_3"), "num3", 3, 3);
    registerMenuItem(Interface.getString("option_val_4"), "num4", 3, 4);
    registerMenuItem(Interface.getString("option_val_5"), "num5", 3, 5);
    registerMenuItem(Interface.getString("option_val_10"), "num10", 3, 6);
    registerMenuItem(Interface.getString("option_val_20"), "num20", 3, 8);
end

function onMenuSelection(selection, subselection)
    if super and super.onMenuSelection then
        super.onMenuSelection(selection, subselection);
    end

    local node = getDatabaseNode().getParent();
    local nodeChar = getCurrentChar();
    if selection == 3 then
        local nNum = 0;
        if subselection == 1 or subselection == 2 or subselection == 3 or subselection == 4 or subselection == 5 then
            nNum = subselection;
        elseif subselection == 6 then
            nNum = 10;
        elseif subselection == 8 then
            nNum = 20;
        end
    ItemManager2.buyItem(node, nNum, nodeChar, false);
    end
end

function getCurrentChar()
    local sUser = User.getCurrentIdentity();
    local nodeChar;
    if sUser and sUser ~= "" then 
        nodeChar = DB.findNode("charsheet." .. sUser);
    else
        Debug.chat("No character found");
    end
    return nodeChar
end