--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local WIDGET_NAME = "romannumber";

function onInit()
    updateView();

    local nodeApt = getDatabaseNode().getParent();
    local nodeChar = nodeApt.getChild("...");
    DB.addHandler(DB.getPath(nodeChar, "exp.current"), "onUpdate", updateView);
    DB.addHandler(DB.getPath(nodeChar, "exp.total"), "onUpdate", updateView);
    DB.addHandler(DB.getPath(nodeChar, "level"), "onUpdate", updateView);
    DB.addHandler(DB.getPath(nodeApt, "extra_exp"), "onUpdate", updateView);
    DB.addHandler(DB.getPath(nodeApt, "level"), "onUpdate", updateView);
end

function onCLose()
    local nodeApt = getDatabaseNode().getParent();
    local nodeChar = nodeApt.getChild("...");
    DB.removeHandler(DB.getPath(nodeChar, "exp.current"), "onUpdate", updateView);
    DB.removeHandler(DB.getPath(nodeChar, "exp.total"), "onUpdate", updateView);
    DB.removeHandler(DB.getPath(nodeChar, "level"), "onUpdate", updateView);
    DB.removeHandler(DB.getPath(nodeApt, "extra_exp"), "onUpdate", updateView);
    DB.removeHandler(DB.getPath(nodeApt, "level"), "onUpdate", updateView);
end

function onValueChanged()
    window.checkAutoEffects();
    updateView();
end

function updateView()
    -- Hide font
    setColor("00000000");
    -- Update frame and tooltip
    local nodeApt = getDatabaseNode().getParent();
    local nCurrent = getValue();
    local sTooltip = "";
    if AptitudeManager.isFree(nodeApt) then
        setFrame("fieldgreen", 5, 4, 5, 4);
        sTooltip = Interface.getString("tooltip_aptitude_free");
    elseif AptitudeManager.isComplete(nodeApt, nCurrent) then
        setFrame("fieldpurple", 5, 4, 5, 4);
        sTooltip = Interface.getString("tooltip_aptitude_toprank");
    else
        local nNextXP = AptitudeManager.getNextXP(nodeApt, nCurrent)
        local nNextLevel = AptitudeManager.getNextLevel(nodeApt, nCurrent)
        local nodeChar = nodeApt.getChild("...");
        local nAvailableXP = DB.getValue(nodeChar, "exp.total", 0) - DB.getValue(nodeChar, "exp.current", 0);
        local nCurrentLevel = DB.getValue(nodeChar, "level", 0);
        sTooltip = string.format(Interface.getString("tooltip_aptitude_nextrank"), nNextXP);

        if nNextLevel > nCurrentLevel then
            setFrame("fieldorange", 5, 4, 5, 4);
            sTooltip = sTooltip .. string.format(Interface.getString("tooltip_stat_requires_level"), tostring(nNextLevel));
        elseif nAvailableXP < nNextXP then
            setFrame("fieldblue_red", 5, 4, 5, 4);
        else
            setFrame("fieldblue", 5, 4, 5, 4);
        end

    end
    setTooltipText(sTooltip);

    -- Update widget
    local sValue = Utilities.number_2_roman(nCurrent);
    local widget = getTextWidget();
    widget.setText(sValue)
end

function getTextWidget()
    local widget = findWidget(WIDGET_NAME);
    if not widget then
        widget = addTextWidget("sheetlabel","");
        widget.setName(WIDGET_NAME);
        widget.setPosition("center", 0, 1)
        widget.setColor("FFFFFFFF");
    end
    return widget;
end