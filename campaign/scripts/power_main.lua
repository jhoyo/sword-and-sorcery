--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	onSummaryChanged();
	update();

	registerMenuItem(Interface.getString("power_menu_addaction"), "pointer", 5);
	registerMenuItem(Interface.getString("power_menu_addspellcast"), "radial_spellcast", 5, 7);
	registerMenuItem(Interface.getString("power_menu_addattack"), "radial_sword", 5, 2);
	registerMenuItem(Interface.getString("power_menu_addsaveroll"), "radial_saveroll", 5, 3);
	registerMenuItem(Interface.getString("power_menu_adddamage"), "radial_damage", 5, 4);
	registerMenuItem(Interface.getString("power_menu_addeffect"), "radial_effect", 5, 5);
	registerMenuItem(Interface.getString("power_menu_addheal"), "radial_heal", 5, 6);
	registerMenuItem(Interface.getString("power_menu_moreactions"), "tokenbagzoom", 5, 8);
	registerMenuItem(Interface.getString("power_menu_addsummon"), "radial_summon", 5, 8, 5);
	registerMenuItem(Interface.getString("power_menu_addroll"), "radial_roll", 5, 8, 6);
	registerMenuItem(Interface.getString("power_menu_addskill"), "radial_skill", 5, 8, 7);
	registerMenuItem(Interface.getString("power_menu_addaffliction"), "hotkeyclear", 5, 8, 8);
	registerMenuItem(Interface.getString("power_menu_addcost"), "radial_cost", 5, 8, 1);

	registerMenuItem(Interface.getString("power_menu_reparse"), "textlist", 6);

	-- TODO: Check this. Check to see if we should automatically parse attack description
	-- local nodeAttack = getDatabaseNode();
	-- local nParse = DB.getValue(nodeAttack, "parse", 0);
	-- if nParse ~= 0 then
	-- 	DB.setValue(nodeAttack, "parse", "number", 0);
	-- 	PowerManager.parsePCPower(nodeAttack);
	-- end
end

function onSummaryChanged()
	local nLevel = level.getValue();
	local sSchool = school.getValue();

	local aText = {};
	if nLevel >= 0 then
		table.insert(aText, Interface.getString("level") .. " " .. nLevel);
	end
	if sSchool ~= "" then
		table.insert(aText, sSchool);
	end

	summary_label.setValue(StringManager.capitalize(table.concat(aText, " ")));
end

function updateControl(sControl, bReadOnly, bShow)
	if not self[sControl] then
		return false;
	end

	if not self[sControl].update then
		Debug.chat("missing ", sControl)
	end

	return self[sControl].update(bReadOnly, not bShow);
end

function update()
	local bReadOnly = WindowManager.getReadOnlyState(getDatabaseNode());
	local nodeRecord = getDatabaseNode();
	local sType = DB.getValue(nodeRecord, "reftype", "");
	local bSpell =  sType ~= "power_ability";

	local bSection1 = false;
	if updateControl("shortdescription", bReadOnly, true) then bSection1 = true; end;

	local bSection2 = false;
	if updateControl("level", bReadOnly, bSpell) then bSection2 = true; end;
	if updateControl("school", bReadOnly, bSpell) then bSection2 = true; end;
	if updateControl("type_spell", bReadOnly, bSpell) then bSection2 = true; end;
	if updateControl("elements", bReadOnly, bSpell) then bSection2 = true; end;
	if updateControl("rarity", bReadOnly, bSpell) then bSection2 = true; end;
	if updateControl("source", bReadOnly, true) then bSection2 = true; end;
	if (not bReadOnly) or (level.getValue() == 0 and school.getValue() == "") then
		summary_label.setVisible(false);
	else
		summary_label.setVisible(true);
		bSection2 = true;
	end

	local bSection3 = false;
	if updateControl("castingtime", bReadOnly, true) then bSection3 = true; end;
	if updateControl("range", bReadOnly, true) then bSection3 = true; end;
	if updateControl("components", bReadOnly, bSpell) then bSection3 = true; end;
	if updateControl("duration", bReadOnly, true) then bSection3 = true; end;
	if updateControl("description", bReadOnly, true) then bSection3 = true; end;
	if updateControl("keywords", bReadOnly, true) then bSection3 = true; end;
	if updateControl("objective", bReadOnly, true) then bSection3 = true; end;
	if updateControl("vigor_cost", bReadOnly, true) then bSection3 = true; end;
	local bEssence = updateControl("essence_cost", bReadOnly, bSpell);
	updateControl("essence_target", bReadOnly, bEssence);
	if bEssence then bSection3 = true; end;
	updateControl("heightened_list", bReadOnly, bSpell)
	heightening_label.setVisible(bSpell);
	iadd_heighening.setVisible(bSpell and not bReadOnly);

	local bSection4 = false;
	local actions = getDatabaseNode().getChild('actions');
	if actions and actions.getChildCount() > 0 then
		heightening_label.setVisible(true);
		bSection4 = true;
	else
		heightening_label.setVisible(not bReadOnly);
	end;
	iadd_heighening.setVisible(not bReadOnly);

	divider.setVisible(bSection1 and bSection2);
	divider2.setVisible((bSection1 or bSection2) and bSection3);
	divider3.setVisible((bSection1 or bSection2 or bSection3) and bSection4);
end

function createAction(sType)
	local nodeAttack = getDatabaseNode();
	if nodeAttack then
		local nodeActions = nodeAttack.createChild("actions");
		if nodeActions then
			local nodeAction = nodeActions.createChild();
			if nodeAction then
				DB.setValue(nodeAction, "type", "string", sType);
				if sType == "effect" then
					DB.setValue(nodeAction, "initialized_cost", "number", 1);
					DB.setValue(nodeAction, "initialized_duration", "number", 1);
				elseif sType == "cost" then
					DB.setValue(nodeAction, "initialized_cost", "number", 1);
				end
			end
		end
	end
end

function onMenuSelection(selection, subselection, subsub)
	if selection == 6 and subselection == 7 then
		getDatabaseNode().delete();
	elseif selection == 6 then
		Debug.chat('Parse still not implemented.')
		-- PowerManager.parsePCPower(getDatabaseNode());
		-- activatedetail.setValue(1);
	elseif selection == 5 then
		if subselection == 1 then
			createAction("cast");
		elseif subselection == 2 then
			createAction("attack");
		elseif subselection == 3 then
			createAction("save");
		elseif subselection == 4 then
			createAction("damage");
		elseif subselection == 5 then
			createAction("effect");
		elseif subselection == 6 then
			createAction("heal");
		elseif subselection == 8 then
			if subsub == 5 then
				createAction("summon");
			elseif subsub == 6 then
				createAction("roll");
			elseif subsub == 7 then
				createAction("skill");
			elseif subsub == 8 then
				createAction("affliction");
			elseif subsub == 1 then
				createAction("cost");
			end
		end
	end
end
