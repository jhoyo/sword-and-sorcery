--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	setRadialOptions();
end

function onMenuSelection(selection, subselection)
	if selection == 6 and subselection == 7 then
		local node = getDatabaseNode();
		if node then
			node.delete();
		else
			close();
		end
	end
end

function setRadialOptions()
	resetMenuItems();

	registerMenuItem(Interface.getString("list_menu_deletespecialty"), "delete", 6);
	registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 6, 7);
end
