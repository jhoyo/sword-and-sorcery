--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local sortLocked = false;

function setSortLock(isLocked)
	sortLocked = isLocked;
end

function onInit()
	onEncumbranceChanged();
	clearEmptyItems();

	registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);

	local node = getDatabaseNode();
	DB.addHandler(DB.getPath(node, "*.carried"), "onUpdate", onCarriedChanged);
	DB.addHandler(DB.getPath(node, "*.weight"), "onUpdate", onEncumbranceChanged);
	DB.addHandler(DB.getPath(node, "*.count"), "onUpdate", onEncumbranceChanged);
	DB.addHandler(DB.getPath(node, "*.init"), "onUpdate", onCarriedChanged);
	DB.addHandler(DB.getPath(node, "*.item_attacks"), "onUpdate", onCarriedChanged);
	DB.addHandler(DB.getPath(node, "*.protection"), "onUpdate", onCarriedChanged);
	DB.addHandler(DB.getPath(node, "*.bond"), "onUpdate", onEssenceChanged);
	DB.addHandler(DB.getPath(node, "*.essence"), "onUpdate", onEssenceChanged);
	DB.addHandler(DB.getPath(node), "onChildDeleted", onEncumbranceChanged);
end

function onClose()
	local node = getDatabaseNode();
	DB.removeHandler(DB.getPath(node, "*.carried"), "onUpdate", onCarriedChanged);
	DB.removeHandler(DB.getPath(node, "*.weight"), "onUpdate", onEncumbranceChanged);
	DB.removeHandler(DB.getPath(node, "*.count"), "onUpdate", onEncumbranceChanged);
	DB.removeHandler(DB.getPath(node, "*.init"), "onUpdate", onCarriedChanged);
	DB.removeHandler(DB.getPath(node, "*.item_attacks"), "onUpdate", onCarriedChanged);
	DB.removeHandler(DB.getPath(node, "*.protection"), "onUpdate", onCarriedChanged);
	DB.removeHandler(DB.getPath(node, "*.bond"), "onUpdate", onEssenceChanged);
	DB.removeHandler(DB.getPath(node, "*.essence"), "onUpdate", onEssenceChanged);
	DB.removeHandler(DB.getPath(node), "onChildDeleted", onEncumbranceChanged);
end

function onMenuSelection(selection)
	if selection == 5 then
		addEntry(true);
	end
end


function onCarriedChanged(nodeField)
	local nodeChar = DB.getChild(nodeField, "....");
	local nodeItem = DB.getChild(nodeField, "..");
	if nodeChar then
		local nCarried = nodeField.getValue();
		local sCarriedItem = StringManager.trim(ItemManager.getDisplayName(nodeItem)):lower();
		if sCarriedItem ~= "" then
			for _,vNode in pairs(DB.getChildren(nodeChar, "inventorylist")) do
				if vNode ~= nodeItem then
					local sLoc = StringManager.trim(DB.getValue(vNode, "location", "")):lower();
					if sLoc == sCarriedItem then
						DB.setValue(vNode, "carried", "number", nCarried);
					end
				end
			end
		end
	end

	updateEffects(nodeItem);
	onEncumbranceChanged();
end


function updateEffects(nodeRecord)
	local list = nodeRecord.createChild("effectlist");
	local nEffects = list.getChildCount();
	local sName = DB.getValue(nodeRecord, "name", "");
	if nEffects > 0 and sName ~= "" then
		local nodeChar = nodeRecord.getChild("...");
		local nodeCT = ActorManager2.getCTNode(nodeChar);
		if nodeCT then
			-- Add effects
			local nCarried = DB.getValue(nodeRecord, "carried", 0);
			sName = sName .. "; ";
			if nCarried == 2 then
				for _, effect in pairs(list.getChildren()) do
					local sEffect = DB.getValue(effect, "name", "");
					if sEffect ~= "" then
						EffectManager.addEffect("", "", nodeCT, { sName = sName .. sEffect, nDuration = 0 }, true);
					end
				end

			-- Remove effects
			else
				for _, effect in pairs(list.getChildren()) do
					local sEffect = DB.getValue(effect, "name", "");
					EffectManager.removeEffect(nodeCT, sName .. sEffect);
					
				end
			end

		end
	end
end

function onEncumbranceChanged()
	if CharManager.updateEncumbrance then
		CharManager.updateEncumbrance(window.getDatabaseNode());
	end
end

function onEssenceChanged()
	if CharManager.updateEssence then
		CharManager.updateEssence(window.getDatabaseNode());
	end
end

function onListChanged()
	update();
	updateContainers();
end

function update()
	local bEditMode = (window.parentcontrol.window.inventory_iedit.getValue() == 1);
	for _,w in ipairs(getWindows()) do
		w.idelete.setVisibility(bEditMode);
	end
end

function addEntry(bFocus)
	local w = createWindow();
	if w then
		if bFocus then
			w.name.setFocus();
		end
	end
	return w;
end

function onClickDown(button, x, y)
	return true;
end

function onClickRelease(button, x, y)
	if not getNextWindow(nil) then
		addEntry(true);
	end
	return true;
end

-- function onChildDeleted()
-- 	clearEmptyItems();
-- end

function onSortCompare(w1, w2)
	if sortLocked then
		return false;
	end
	return ItemManager.onInventorySortCompare(w1, w2);
end

function updateContainers()
	ItemManager.onInventorySortUpdate(self);
end

function onDrop(x, y, draginfo)
	return ItemManager.handleAnyDrop(window.getDatabaseNode(), draginfo);
end

function clearEmptyItems()
	for _, w in pairs(getWindows()) do
		if w.name.getValue() == "" then
			w.getDatabaseNode().delete();
		end
	end
end
