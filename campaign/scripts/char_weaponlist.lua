--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	DB.addHandler(DB.getPath(getDatabaseNode()), "onChildAdded", onChildAdded);
	if inCT then
		DB.addHandler(DB.getPath(getDatabaseNode().getChild('..'), "level"), "onUpdate", onProfChanged);
		DB.addHandler(DB.getPath(getDatabaseNode().getChild('..'), "skilllist_weaponary"), "onUpdate", onProfChanged);
		DB.addHandler(DB.getPath(getDatabaseNode().getChild('..'), "abilities"), "onChildUpdate", onProfChanged);
	else
		DB.addHandler(DB.getPath(window.getDatabaseNode(), "level"), "onUpdate", onProfChanged);
		DB.addHandler(DB.getPath(window.getDatabaseNode(), "skilllist_weaponary"), "onUpdate", onProfChanged);
		DB.addHandler(DB.getPath(window.getDatabaseNode(), "abilities"), "onChildUpdate", onProfChanged);
	end

	onModeChanged();
	update();
end

function onClose()
	DB.removeHandler(DB.getPath(getDatabaseNode()), "onChildAdded", onChildAdded);
	if inCT then
		DB.removeHandler(DB.getPath(getDatabaseNode().getChild('..'), "level"), "onUpdate", onProfChanged);
		DB.removeHandler(DB.getPath(getDatabaseNode().getChild('..'), "skilllist_weaponary"), "onUpdate", onProfChanged);
		DB.removeHandler(DB.getPath(getDatabaseNode().getChild('..'), "abilities"), "onChildUpdate", onProfChanged);
	else
		DB.removeHandler(DB.getPath(window.getDatabaseNode(), "level"), "onUpdate", onProfChanged);
		DB.removeHandler(DB.getPath(window.getDatabaseNode(), "skilllist_weaponary"), "onUpdate", onProfChanged);
		DB.removeHandler(DB.getPath(window.getDatabaseNode(), "abilities"), "onChildUpdate", onProfChanged);
	end
end

function onChildAdded()
	onModeChanged();
	update();
end

function onProfChanged()
	for _,w in pairs(getWindows()) do
		w.onAttackChanged(true);
	end
end

function onListChanged()
	update();
end

function onModeChanged()
	local bPrepMode = true;
	if inCharsheet then
		bPrepMode = (DB.getValue(window.getDatabaseNode(), "powermode", "") == "preparation");
	end

	for _,w in ipairs(getWindows()) do
		w.carried.setVisible(bPrepMode);
		w.hands.setVisible(true);
	end
	applyFilter();
end

function onFilter(w)
	if (inCT or (DB.getValue(window.getDatabaseNode(), "powermode", "") == "combat")) and (w.carried.getValue() < 2) then
		return false;
	end
	return true;
end

function update()
	if inCharsheet and window.parentcontrol.window.actions_iedit then
		local bEditMode = (window.parentcontrol.window.actions_iedit.getValue() == 1);
		for _,w in pairs(getWindows()) do
			w.idelete.setVisibility(bEditMode);
			w.iadd.setVisible(bEditMode);
			for _,ww in pairs(w.attacklist.getWindows()) do
				ww.idelete.setVisibility(bEditMode);
			end
		end
	end

	updateHands();
end

function addEntry(bFocus)
	local w = createWindow();
	w.attacklist.addEntry();
	if bFocus and w then
		w.name.setFocus();
	end
	return w;
end

function onDrop(x, y, draginfo)
	if draginfo.isType("shortcut") then
		local sClass, sRecord = draginfo.getShortcutData();
		if LibraryData.isRecordDisplayClass("item", sClass) and ItemManager2.isWeapon(sRecord) then
			return ItemManager.handleAnyDrop(window.getDatabaseNode(), draginfo);
		end
	end
end

function onChildUpdate()
	updateHands();
end

function updateHands(weapon)
	local nHands = 1;
	-- If we give a weapon, that is the one being updated
	if weapon then
		local nCarried = weapon.carried.getValue();

		if nCarried == 2 then
			nHands = weapon.hands.getValue();
			-- Unequip the rest of the weapons used in the same hand
			for _,w in pairs(getWindows()) do
				if w ~= weapon then
					local wCarried = w.carried.getValue();
					local wHands = w.hands.getValue();
					if wCarried == 2 then
						if (nHands == 0 or nHands == 2) and (wHands == 0 or wHands == 2) then
							w.carried.setValue(1);
						end
						if (nHands == 1 or nHands == 2) and (wHands == 1 or wHands == 2) then
							w.carried.setValue(1);
						end
					end
				end
			end
		end
	end

	-- Look for definitive windows
	local wMainHand, wSideHand;

	for _,w in pairs(getWindows()) do
		local nCarried = w.carried.getValue();
		local nHands = w.hands.getValue();
		if nCarried == 2 then
			if (nHands == 0 or nHands == 2) and wMainHand then
				w.carried.setValue(1);
			elseif nHands == 0 or nHands == 2 then
				wMainHand = w;
			end
			if (nHands == 1 or nHands == 2) and wSideHand then
				w.carried.setValue(1);
			elseif nHands == 1 then
				wSideHand = w;
			elseif nHands == 2 then
				local node = w.getDatabaseNode();
				local attacklist = node.getChild("attacks");
				local bDouble = false;
				for _, v in pairs(attacklist.getChildren()) do
					local sProp = DB.getValue(v, "properties", "");
					bDouble = bDouble or (sProp:find(DataCommon.weapon_keywords["double"]) and sProp:find(DataCommon.weapon_keywords["two_hands"]));
				end
				if bDouble then
					wSideHand = w;
				end
			end
		end
	end

	-- Update equiped weapons for defense purposes
	local nodeChar = getDatabaseNode().getChild('..');
	ActorManager2.updateDefenses(nodeChar);

	-- CharManager.resetEquipedWeapons(nodeChar);
	-- if wMainHand then
	-- 	wMainHand.assignWeapon(nodeChar, "main_hand", wMainHand.hands.getValue());
	-- else
	-- 	ItemManager2.clearWeapon(nodeChar, "main_hand");
	-- end
	-- if wSideHand then
	-- 	wSideHand.assignWeapon(nodeChar, "side_hand", wSideHand.hands.getValue());
	-- else
	-- 	ItemManager2.clearWeapon(nodeChar, "side_hand");
	-- end


	-- local nodeChar = getDatabaseNode().getChild('..');
	-- CharManager.resetEquipedWeapons(nodeChar);
	-- local bClearMain = true;
	-- local bClearSide = true;
	--
	-- for _,w in pairs(getWindows()) do
	-- 	local nCarried = w.carried.getValue();
	-- 	local nHands = w.hands.getValue();
	-- 	if nCarried == 2 and (nHands == 0 or nHands == 2) then
	-- 		w.assignWeapon(nodeChar, "main_hand", nHands);
	-- 		bClearMain = false;
	-- 		bClearSide = nHands ~= 2;
	-- 	elseif nCarried == 2 and (nHands == 1) then
	-- 		w.assignWeapon(nodeChar, "side_hand", nHands);
	-- 		bClearSide = false;
	-- 	end
	--
	-- 	if bClearMain then
	-- 		ItemManager2.clearWeapon(nodeChar, "main_hand");
	-- 	end
	-- 	if bClearSide then
	-- 		ItemManager2.clearWeapon(nodeChar, "side_hand");
	-- 	end
	-- end

end
