--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	update();
end


function VisDataCleared()
	update();
end

function InvisDataAdded()
	update();
end

function updateControl(sControl, bReadOnly, bID)
	if not self[sControl] then
		return false;
	end

	if not bID then
		return self[sControl].update(bReadOnly, true);
	end

	return self[sControl].update(bReadOnly);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);
	local sType = DB.getValue(nodeRecord, "cycler", "");
	local bPoison = sType == "type_affliction_poison";
	local bIllness = sType == "type_affliction_illness";

	local bSection1 = false;
	if updateControl("incubation", bReadOnly, true) then bSection1 = true; end
	if updateControl("advance", bReadOnly, true) then bSection1 = true; end
	if updateControl("recovery", bReadOnly, true) then bSection1 = true; end
	if updateControl("peligrosity", bReadOnly, true) then bSection1 = true; end
	if updateControl("initial_cat", bReadOnly, true) then bSection1 = true; end
	if updateControl("keywords", bReadOnly, true) then bSection1 = true; end

	local bSection2 = false;
	if updateControl("category", bReadOnly, not bIllness) then bSection2 = true; end
	if updateControl("rarity", bReadOnly, not bIllness) then bSection2 = true; end
	if updateControl("supplies", bReadOnly, not bIllness) then bSection2 = true; end
	if updateControl("delivery", bReadOnly, not bIllness) then bSection2 = true; end

	if updateControl("contagion", bReadOnly, not bPoison) then bSection2 = true; end
	if updateControl("inmunity", bReadOnly, not bPoison) then bSection2 = true; end

	local bSection3 = true;
	categories.update(bReadOnly);
	if updateControl("end_effect", bReadOnly, true) then bSection3 = true; end

	local bSection4 = DB.getValue(nodeRecord, "text", "") ~= "";
	text.setVisible(bSection3);
	text.setReadOnly(bReadOnly);

	divider1.setVisible(bSection1 and bSection2);
	divider2.setVisible((bSection1 or bSection2) and bSection3);
	divider2.setVisible((bSection1 or bSection2 or bSection3) and bSection4);
end

