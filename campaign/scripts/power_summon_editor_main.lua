function onDrop(x, y, draginfo)
  local sDragType = draginfo.getType();

  if (sDragType == "shortcut") and (y < 90) then
    local sLabel = draginfo.getDescription();
    local sClass, sRecord = draginfo.getShortcutData();
    if sLabel and sClass and sRecord then
      list.addEntry(sLabel, sClass, sRecord);
    end
  end
end