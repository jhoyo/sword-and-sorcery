function onInit()
  local nodeChar = getDatabaseNode().getChild('...');

  for _, sName in pairs(DataCommon.abilities) do
    DB.addHandler(DB.getPath(nodeChar, "abilities."..sName..".base"), "onUpdate", update);
  end
  for _, sName in pairs(DataCommon.abilities_gift) do
    DB.addHandler(DB.getPath(nodeChar, "category_"..sName), "onUpdate", update);
  end
  update();
end

function onClose()
  local nodeChar = getDatabaseNode().getChild('...');

  for _, sName in pairs(DataCommon.abilities) do
    DB.removeHandler(DB.getPath(nodeChar, "abilities."..sName..".base"), "onUpdate", update);
  end
  for _, sName in pairs(DataCommon.abilities_gift) do
    DB.removeHandler(DB.getPath(nodeChar, "category_"..sName), "onUpdate", update);
  end
end


function update()
  local nodeChar = getDatabaseNode().getChild('...');
  local nTotal = 0
  for _, sName in pairs(DataCommon.abilities) do
    local nValue = DB.getValue(nodeChar, "abilities."..sName..".base", 0);
    if nValue < 0 then
      nValue = 0;
    elseif nValue > DataCommon.abilities_max_value then
      nValue = DataCommon.abilities_max_value;
    end

    nTotal = nTotal + DataCommon.abilities_PA_cost[nValue];
    if (nValue > 0) and Utilities.inArray(DataCommon.abilities_gift, sName) then
      local sCat = ActorManager2.getMagicCategoryAndLevel(nodeChar, sName);
      -- local sCat = DB.getValue(nodeChar, "category_"..sName, "");
      nTotal = nTotal + (DataCommon.abilities_PA_magic[sCat] or 0);
    end
  end

  setValue(nTotal);

end
