--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local bFilter = true;
function setFilter(bNewFilter)
	bFilter = bNewFilter;
end
function getFilter()
	return bFilter;
end

function onInit()
	if not windowlist.isReadOnly() then
		registerMenuItem(Interface.getString("list_menu_deleteitem"), "delete", 6);
		registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 6, 7);

		registerMenuItem(Interface.getString("power_menu_addaction"), "pointer", 3);
		registerMenuItem(Interface.getString("power_menu_addspellcast"), "radial_spellcast", 3, 1);
		registerMenuItem(Interface.getString("power_menu_addattack"), "radial_sword", 3, 2);
		registerMenuItem(Interface.getString("power_menu_addsaveroll"), "radial_saveroll", 3, 3);
		registerMenuItem(Interface.getString("power_menu_adddamage"), "radial_damage", 3, 4);
		registerMenuItem(Interface.getString("power_menu_addeffect"), "radial_effect", 3, 5);
		registerMenuItem(Interface.getString("power_menu_addheal"), "radial_heal", 3, 8);
		registerMenuItem(Interface.getString("power_menu_moreactions"), "tokenbagzoom", 3, 6);
		registerMenuItem(Interface.getString("power_menu_addsummon"), "radial_summon", 3, 6, 3);
		registerMenuItem(Interface.getString("power_menu_addroll"), "radial_roll", 3, 6, 4);
		registerMenuItem(Interface.getString("power_menu_addskill"), "radial_skill", 3, 6, 5);
		registerMenuItem(Interface.getString("power_menu_addaffliction"), "hotkeyclear", 3, 6, 6);
		registerMenuItem(Interface.getString("power_menu_addcost"), "radial_cost", 3, 6, 7);

		-- registerMenuItem(Interface.getString("power_menu_reparse"), "textlist", 4);

		DB.addHandler(DB.getPath(getDatabaseNode()), "onChildUpdate", updateActionsView);
		DB.addHandler(DB.getPath(getDatabaseNode()), "onChildUpdate", updateCostsView);

		updateActionsView();
		updateCostsView();
	end

	onDisplayChanged();
	windowlist.onChildWindowAdded(self);
	checkLinkedItem();
end

function onClose()
	DB.removeHandler(DB.getPath(getDatabaseNode()), "onChildUpdate", updateActionsView);
	DB.removeHandler(DB.getPath(getDatabaseNode()), "onChildUpdate", updateCostsView);
end

function onPreparedChanged()
	windowlist.window.updatePrepared()
end

function onDisplayChanged(inCT)
	sDisplayMode = DB.getValue(getDatabaseNode(), "...powerdisplaymode", "");

	if inCT or (sDisplayMode == "action") then
		header.subwindow.group.setVisible(false);
		header.subwindow.shortdescription.setVisible(false);
		header.subwindow.actionsmini.setVisible(true);
	elseif sDisplayMode == "summary" then
		header.subwindow.group.setVisible(false);
		header.subwindow.shortdescription.setVisible(true);
		header.subwindow.actionsmini.setVisible(false);
	else
		header.subwindow.group.setVisible(true);
		header.subwindow.shortdescription.setVisible(false);
		header.subwindow.actionsmini.setVisible(false);
	end
end

function createAction(sType)
	local node = getDatabaseNode();
	-- Debug.chat(node)
	if node then
		local nodeActions = node.createChild("actions");
		-- Debug.chat(nodeActions)
		if nodeActions then
			local nodeAction = nodeActions.createChild();
			-- Debug.chat(nodeAction)
			if nodeAction then
				DB.setValue(nodeAction, "type", "string", sType);
				if sType == "effect" then
					DB.setValue(nodeAction, "initialized_cost", "number", 1);
					DB.setValue(nodeAction, "initialized_duration", "number", 1);
				elseif sType == "cost" then
					DB.setValue(nodeAction, "initialized_cost", "number", 1);
				end
			end
		end
	end
end

function onMenuSelection(selection, subselection, subsub)
	if selection == 6 and subselection == 7 then
		getDatabaseNode().delete();
	elseif selection == 4 then
		Debug.chat('Parse still not implemented.')
		-- PowerManager.parsePCPower(getDatabaseNode());
	elseif selection == 3 then
		if subselection == 1 then
			createAction("cast");
		elseif subselection == 2 then
			createAction("attack");
		elseif subselection == 3 then
			createAction("save");
		elseif subselection == 4 then
			createAction("damage");
		elseif subselection == 5 then
			createAction("effect");
		elseif subselection == 8 then
			createAction("heal");
		elseif subselection == 6 then
			if subsub == 3 then
				createAction("summon");
			elseif subsub == 4 then
				createAction("roll");
			elseif subsub == 5 then
				createAction("skill");
			elseif subsub == 6 then
				createAction("affliction");
			elseif subsub == 7 then
				createAction("cost");
			end
		end
	end
end

function toggleDetail()
	local status = (activatedetail.getValue() == 1);

	actions.setVisible(status);

	for _,v in pairs(actions.getWindows()) do
		v.updateDisplay();
	end
end

function getDescription(bShowFull)
	local node = getDatabaseNode();

	local s = DB.getValue(node, "name", "");

	if bShowFull then
		local sShort = DB.getValue(node, "shortdescription", "");
		if sShort ~= "" then
			s = s .. " - " .. sShort;
		end
	end

	return s;
end

function usePower(bShowFull)
	local node = getDatabaseNode();
	ChatManager.Message(getDescription(bShowFull), true, ActorManager.resolveActor(node.getChild("...")));
end


-----------------------------
-- EXTRA VISUALIZATIONS
-----------------------------

function updateActionsView()
	local node = getDatabaseNode();
	local sActions = DB.getValue(node, "castingtime", ""):lower();
	local nLevel = PowerManager.getSpellLevel(node, true);
	local bRitual = DB.getValue(node, "type_spell", "") == "power_ritual";

	-- Spells may get extra actions
	local nodeChar = node.getChild("...");
	if nodeChar.getPath():find("combattracker") then
		nodeChar = ActorManager2.getNodeFromCT(nodeChar);
	end
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
	local aPowerGroup = PowerManager.getPowerGroupRecord(rActor, node);
	local sType = "";
	if aPowerGroup then
		sType = aPowerGroup.grouptype;
	end
	local nExtra = 0;
	if sType == "power_spell" then
		-- Get the spell cast action for all info
		local nodeCast = PowerManager.getCastAction(node);
		if nodeCast then
			local rAction = PowerManager.getPCPowerAction(nodeCast);
			nExtra = rAction.increased_actions;
			nLevel = math.max(0, math.min(10, rAction.level))
		end
	end

	-- Get actions and tooltip
	local aActions = {"action_ritual"};
	local sTooltip = "";
	if bRitual then
		sTooltip = DataCommon.magic_standard_actions_ritual[nLevel];
		if nExtra and nExtra > 0 then
			sTooltip = sTooltip .. " + " .. tostring(nExtra * DataCommon.magic_extra_actions_ritual) .. " " .. Interface.getString("minute") .. "s";
		end
	else
		aActions, sTooltip = ActionCast.getArrayOfActions(sActions, nExtra, nLevel, bRitual, true);
	end
	header.subwindow.box_actions.updateView(aActions, StringManager.capitalize(sTooltip));

end

function updateCostsView()
	-- Choose between types
	local node = getDatabaseNode();
	local nodeChar = node.getChild("...");
	if nodeChar.getPath():find("combattracker") then
		nodeChar = ActorManager2.getNodeFromCT(nodeChar);
	end
	local nodeChar, rActor = ActorManager2.getActorAndNode(nodeChar);
	local aPowerGroup = PowerManager.getPowerGroupRecord(rActor, node);
	local sType = "";
	if aPowerGroup then
		sType = aPowerGroup.grouptype;
	end
	local nVigor = 0;
	local nEssence = 0;
	local sFrameVigor = "fieldblue";
	local sFrameEssence = "fieldpurple";
	local sVigorTooltip = Interface.getString("tooltip_power_vigor_cost");
	local sEssenceTooltip = Interface.getString("tooltip_power_essence_cost");

	if sType == "power_spell" then
		-- Get the spell cast action for all info
		local nodeCast = PowerManager.getCastAction(node);
		if nodeCast then
			local rAction = PowerManager.getPCPowerAction(nodeCast);
			local bBlood = rAction.keywords:lower():find(DataCommon.magic_keywords["blood_magic"]);
			local nExtra = rAction.increased_vigor;
			if rAction.type_spell ~= "power_ritual" or bBlood then
				
				if bBlood then
					nVigor = math.max(1, rAction.level);
					nVigor = nVigor + math.max(0, nExtra - ActorManager2.getAptitudeCategory(DataCommon.aptitude["energy_absorption"]));
					sFrameVigor = "fieldred";
					sVigorTooltip = Interface.getString("tooltip_power_vitality_cost");
				else
					nVigor = ActorManager2.calculateSpellVigorCost(rActor, rAction.level, rAction.stat) + nExtra;
				end
			end
			nEssence = rAction.essence_cost;
			if rAction.essence_target == Interface.getString("essence_target_channeler") then
				sFrameEssence = "fieldpurple_gold";
				sEssenceTooltip = sEssenceTooltip .. Interface.getString("tooltip_power_essence_cost_channeler");
			elseif rAction.essence_target == Interface.getString("essence_target_both") then
				sFrameEssence = "fieldpurple_red";
				sEssenceTooltip = sEssenceTooltip .. Interface.getString("tooltip_power_essence_cost_both");
			else
				sEssenceTooltip = sEssenceTooltip .. Interface.getString("tooltip_power_essence_cost_target");
			end
		end

	elseif sType == "power_item_spell" then
		-- No Vigor cost but only essence
		nEssence = DB.getValue(node, "essence_cost", 0);

	else
		-- Techniques use direct Vigor cost and no Essence
		nVigor = tonumber(DB.getValue(node, "vigor_cost", "0"));
	end

	-- Set values
	header.subwindow.vigor_cost_box.updateView(nVigor, sFrameVigor, sVigorTooltip);
	header.subwindow.essence_cost_box.updateView(nEssence, sFrameEssence, sEssenceTooltip);
end

function checkLinkedItem()
	local node = getDatabaseNode();
	local sPath = DB.getValue(node, "linked_power", "");
	if sPath ~= "" then
		local nodePower = DB.findNode(sPath);
		if not nodePower then
			node.delete();
		end
	end
end

