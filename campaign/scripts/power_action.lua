--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	registerMenuItem(Interface.getString("power_menu_actiondelete"), "deletepointer", 4);
	registerMenuItem(Interface.getString("list_menu_deleteconfirm"), "delete", 4, 3);

	updateDisplay();

	local node = getDatabaseNode();
	windowlist.setOrder(node);

	local sNode = node.getNodeName();
	DB.addHandler(sNode, "onChildUpdate", onDataChanged);

	local sType = DB.getValue(node, "type", "");
	if sType == "cast" then
		local nodeChar = DB.getChild(node, ".....");
		DB.addHandler(DB.getPath(nodeChar, "listmagic.*.active"), "onUpdate", onDataChanged);
		DB.addHandler(DB.getPath(nodeChar, "listmagic.*.active_cat"), "onUpdate", onDataChanged);
	end
	onDataChanged();
end

function onClose()
	local sNode = getDatabaseNode().getNodeName();
	DB.removeHandler(sNode, "onChildUpdate", onDataChanged);
	local sType = DB.getValue(node, "type", "");
	if sType == "cast" then
		local nodeChar = DB.getChild(node, ".....");
		DB.removeHandler(DB.getPath(nodeChar, "listmagic.*.active"), "onUpdate", onDataChanged);
		DB.removeHandler(DB.getPath(nodeChar, "listmagic.*.active_cat"), "onUpdate", onDataChanged);
	end
end

function onMenuSelection(selection, subselection)
	if selection == 4 and subselection == 3 then
		getDatabaseNode().delete();
	end
end


function highlight(bState)
	if bState then
		setFrame("rowshade");
	else
		setFrame(nil);
	end
end

function updateDisplay()
	local node = getDatabaseNode();

	local sType = DB.getValue(node, "type", "");
	if sType == "attack" then
		actionlabel.setValue(Interface.getString("power_label_attack"));
		actionbutton.setIcons("button_action_attack", "button_action_attack_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_attack_editor");
	elseif sType == "save" then
		actionlabel.setValue(Interface.getString("power_label_save"));
		actionbutton.setIcons("button_action_save", "button_action_save_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_save_editor");
	elseif sType == "damage" then
		actionlabel.setValue(Interface.getString("power_label_damage"));
		actionbutton.setIcons("button_action_damage", "button_action_damage_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_damage_editor");
	elseif sType == "heal" then
		actionlabel.setValue(Interface.getString("power_label_heal"));
		actionbutton.setIcons("button_action_heal", "button_action_heal_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_heal_editor");
	elseif sType == "effect" then
		actionlabel.setValue(Interface.getString("power_label_effect"));
		actionbutton.setIcons("button_action_effect", "button_action_effect_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_effect_editor");
	elseif sType == "summon" then
		actionlabel.setValue(Interface.getString("power_label_summon"));
		actionbutton.setIcons("button_action_summon", "button_action_summon_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_summon_editor");
	elseif sType == "roll" then
		actionlabel.setValue(Interface.getString("power_label_roll"));
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_roll_editor");
	elseif sType == "cast" then
		actionlabel.setValue(Interface.getString("power_label_cast"));
		actionbutton.setIcons("button_action_cast", "button_action_cast_down");
		actionbutton.setTooltipText(Interface.getString("char_tooltip_castspell"));
		actionetail.setEditorWindow("power_cast_editor");
	elseif sType == "skill" then
		actionlabel.setValue(Interface.getString("power_label_skill"));
		actionbutton.setIcons("button_action_skill", "button_action_skill_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_skill_editor");
	elseif sType == "affliction" then
		actionlabel.setValue(Interface.getString("power_label_affliction"));
		actionbutton.setIcons("button_action_affliction", "button_action_affliction_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_affliction_editor");
		parameter.setTooltipText(Interface.getString("tooltip_power_dosage"));
	elseif sType == "cost" then
		actionlabel.setValue(Interface.getString("power_label_cost"));
		actionbutton.setIcons("button_action_cost", "button_action_cost_down");
		actionbutton.setTooltipText("");
		actionetail.setEditorWindow("power_cost_editor");
		parameter.setTooltipText(Interface.getString("tooltip_power_dosage"));
	end



	updateViews();
end

function updateViews()
	onDataChanged();
end

function onDataChanged()
	local sType = DB.getValue(getDatabaseNode(), "type", "");

	if sType == "cast" then
		onCastChanged();
	elseif sType == "damage" then
		onDamageChanged();
	elseif sType == "effect" then
		onEffectChanged();
	elseif sType == "attack" then
		onAttackChanged();
	elseif sType == "save" then
		onSaveChanged();
	elseif sType == "summon" then
		onSummonChanged();
	elseif sType == "roll" then
		onRollChanged();
	elseif sType == "skill" then
		onSkillChanged();
	elseif sType == "heal" then
		onHealChanged();
	elseif sType == "affliction" then
		onAfflictionChanged();
	elseif sType == "cost" then
		onCostChanged();
	end
	setComponents();
end

function onCostChanged()
	local sString = PowerManager.getPCPowerCostActionText(getDatabaseNode());
	actionview.setValue(sString);
end

function onAfflictionChanged()
	local sString = PowerManager.getPCPowerAfflictionActionText(getDatabaseNode());
	actionview.setValue(sString);
end

function onRollChanged()
	local sString = PowerManager.getPCPowerRollActionText(getDatabaseNode());
	actionview.setValue(sString);
end

function onAttackChanged()
	local sAttack = PowerManager.getPCPowerAttackActionText(getDatabaseNode());
	actionview.setValue(sAttack);
end

function onSkillChanged()
	local sSkill = PowerManager.getPCPowerSkillActionText(getDatabaseNode());
	actionview.setValue(sSkill);
end

function onSaveChanged()
	local sSave = PowerManager.getPCPowerSaveActionText(getDatabaseNode());
	actionview.setValue(sSave);
end

function onCastChanged()
	local sCast = PowerManager.getPCPowerCastActionText(getDatabaseNode());
	actionview.setValue(sCast);
	pcall(windowlist.window.updateActionsView);
	pcall(windowlist.window.updateCostsView);
end

function onDamageChanged()
	local sDamage = PowerManager.getPCPowerDamageActionText(getDatabaseNode());
	actionview.setValue(sDamage);
end

function onHealChanged()
	local sHeal = PowerManager.getPCPowerHealActionText(getDatabaseNode());
	actionview.setValue(sHeal);
end

function onEffectChanged()
	local sLabel = PowerManager.getPCPowerEffectActionText(getDatabaseNode());
	local sTooltip = EffectManagerSS.getTooltipFromText(sLabel);
	actionview.setTooltipText(sTooltip);
	actionview.setValue(sLabel);
end

function onSummonChanged()
	local sSummon = PowerManager.getPCPowerSummonActionText(getDatabaseNode());
	actionview.setValue(sSummon);
end

function setComponents()
	local node = getDatabaseNode();
	local sType = DB.getValue(node, "type", "");
	local bVisible = false;

	if sType == "cast" then
		local nodeSpell = node.getChild("...");
		local sComp = DB.getValue(nodeSpell, "components", "");
		local aIcons = {};
		if sComp ~= "" then
			bVisible = true;
			for k,v in pairs(DataCommon.magic_components) do
				local bCond, bMand = ActionCast.hasComponent(sComp, v);
				if bCond then
					local sAdd = "component_" .. k;
					if k == "gem" then
						local nLevel = DB.getValue(nodeSpell, "level", 0) + DB.getValue(node, "parameter", 0);
						sAdd = sAdd .. "_" .. tostring(math.floor(nLevel/3));
					elseif k == "material" then
						local nLevel = DB.getValue(nodeSpell, "level", 0) + DB.getValue(node, "parameter", 0);
						for ind = 1,math.floor(nLevel/3) do
							table.insert(aIcons, sAdd);
						end
					elseif bMand then
						sAdd = sAdd .. "_mandatory";
					end
					table.insert(aIcons, sAdd);
				end
			end
		end
		box_components.updateView(aIcons, sComp)
	end
	box_components.setVisible(bVisible);
end
