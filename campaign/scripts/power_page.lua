--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local aGroups = {};
local aCharSlots = {};

local bCheckingUsage = false;
local bUpdatingGroups = false;

function isInCT()
	return parentcontrol.window.getClass() == "ct_entry";
end

function getCharNode()
	local node;
	local inCT = isInCT();
	if inCT then
		local sType, sChar = getDatabaseNode().createChild("link").getValue();
		if sType and sType == 'npc' then
			node = getDatabaseNode();
		elseif sChar and sChar ~= "" then
			node = DB.findNode(sChar)
		end
	else
		node = getDatabaseNode();
	end
	return node;
end


function getEditMode()
	local inCT = isInCT();
	if inCT then
		return false;
	else
		if parentcontrol.window.parentcontrol.window.actions_iedit then
			return parentcontrol.window.parentcontrol.window.actions_iedit == 1;
		end
	end
	return false;
end

function onInit()
	updatePowerGroups();
	-- updatePrepared();
	local node = getCharNode();

	DB.addHandler(DB.getPath(node, "level"), "onUpdate", onAbilityChanged);
	DB.addHandler(DB.getPath(node, "heightened_list"), "onUpdate", onHeightenedListChanged);
	DB.addHandler(DB.getPath(node, "abilities.*.current"), "onUpdate", onAbilityChanged);
	DB.addHandler(DB.getPath(node, "powergroup.*.groupstat"), "onUpdate", onAbilityChanged);
	DB.addHandler(DB.getPath(node, "powergroup.*.groupskill"), "onUpdate", onAbilityChanged);

	DB.addHandler(DB.getPath(node, "powergroup"), "onChildAdded", onGroupListChanged);
	DB.addHandler(DB.getPath(node, "powergroup"), "onChildDeleted", onGroupListChanged);
	DB.addHandler(DB.getPath(node, "powergroup.*.name"), "onUpdate", onGroupNameChanged);
	DB.addHandler(DB.getPath(node, "powergroup.*.grouptype"), "onUpdate", onGroupTypeChanged);
	DB.addHandler(DB.getPath(node, "magic.methodic"), "onUpdate", onGroupTypeChanged);
	DB.addHandler(DB.getPath(node, "powergroup.*.groupcharges"), "onUpdate", onGroupTypeChanged); 

	DB.addHandler(DB.getPath(node, "powers.*.cast"), "onUpdate", updateGlobalUses);
	DB.addHandler(DB.getPath(node, "powers.*.counter"), "onUpdate", onUsesChanged);
	DB.addHandler(DB.getPath(node, "powers.*.charges"), "onUpdate", onUsesChanged);
	DB.addHandler(DB.getPath(node, "powers.*.prepared"), "onUpdate", onUsesChanged);
	DB.addHandler(DB.getPath(node, "powermeta.*.max"), "onUpdate", onUsesChanged);
	DB.addHandler(DB.getPath(node, "powermeta.*.used"), "onUpdate", onUsesChanged);

	DB.addHandler(DB.getPath(node, "powers.*.group"), "onUpdate", onPowerGroupChanged);
	DB.addHandler(DB.getPath(node, "powers.*.level"), "onUpdate", onPowerGroupChanged);

	DB.addHandler(DB.getPath(node, "inventorylist"), "onUpdate", updatePowerGroups);
end

function onClose()
	local node = getCharNode();

	DB.removeHandler(DB.getPath(node, "level"), "onUpdate", onAbilityChanged);
	DB.removeHandler(DB.getPath(node, "heightened_list"), "onUpdate", onHeightenedListChanged);
	DB.removeHandler(DB.getPath(node, "abilities.*.current"), "onUpdate", onAbilityChanged);
	DB.removeHandler(DB.getPath(node, "powergroup.*.groupstat"), "onUpdate", onAbilityChanged);
	DB.removeHandler(DB.getPath(node, "powergroup.*.groupskill"), "onUpdate", onAbilityChanged);

	DB.removeHandler(DB.getPath(node, "powergroup"), "onChildAdded", onGroupListChanged);
	DB.removeHandler(DB.getPath(node, "powergroup"), "onChildDeleted", onGroupListChanged);
	DB.removeHandler(DB.getPath(node, "powergroup.*.name"), "onUpdate", onGroupNameChanged);
	DB.removeHandler(DB.getPath(node, "powergroup.*.grouptype"), "onUpdate", onGroupTypeChanged);
	DB.removeHandler(DB.getPath(node, "magic.methodic"), "onUpdate", onGroupTypeChanged);
	DB.removeHandler(DB.getPath(node, "powergroup.*.groupcharges"), "onUpdate", onGroupTypeChanged);

	DB.removeHandler(DB.getPath(node, "powers.*.cast"), "onUpdate", updateGlobalUses);
	DB.removeHandler(DB.getPath(node, "powers.*.counter"), "onUpdate", onUsesChanged);
	DB.removeHandler(DB.getPath(node, "powers.*.charges"), "onUpdate", onUsesChanged);
	DB.removeHandler(DB.getPath(node, "powers.*.prepared"), "onUpdate", onUsesChanged);
	DB.removeHandler(DB.getPath(node, "powermeta.*.max"), "onUpdate", onUsesChanged);
	DB.removeHandler(DB.getPath(node, "powermeta.*.used"), "onUpdate", onUsesChanged);

	DB.removeHandler(DB.getPath(node, "powers.*.group"), "onUpdate", onPowerGroupChanged);
	DB.removeHandler(DB.getPath(node, "powers.*.level"), "onUpdate", onPowerGroupChanged);

	DB.removeHandler(DB.getPath(node, "inventorylist"), "onChildUpdate", updatePowerGroups);
end

function onAbilityChanged()
	rebuildGroups();
	for _,v in pairs(powers.getWindows()) do
		if v.getClass() ~= "power_group_header" then
			local sGroup = DB.getValue(v.getDatabaseNode(), "group", "");
			local rGroup = aGroups[sGroup];
			for _,v2 in pairs(v.actions.getWindows()) do
				v2.updateViews();
			end
			if v.header.subwindow then
				for _,v2 in pairs(v.header.subwindow.actionsmini.getWindows()) do
					v2.updateViews();
				end
			end
		end
	end
end

function onModeChanged()
	rebuildGroups();
	updateUses();
end

function onDisplayChanged()
	local inCT = isInCT();
	for _,v in pairs(powers.getWindows()) do
		if v.getClass() ~= "power_group_header" then
			v.onDisplayChanged(inCT);
		end
	end
end

function onUsesChanged()
	rebuildGroups();
	updateUses();
end

function onGroupListChanged()
	updatePowerGroups();
end

function onGroupTypeChanged()
	updatePowerGroups();
end

function onGroupNameChanged(nodeGroupName)
	if bUpdatingGroups then
		return;
	end
	bUpdatingGroups = true;

	local nodeParent = nodeGroupName.getParent();
	local sNode = nodeParent.getNodeName();

	local nodeGroup = nil;
	local sOldValue = "";
	for sGroup, vGroup in pairs(aGroups) do
		if vGroup.nodename == sNode then
			nodeGroup = vGroup.node;
			sOldValue = sGroup;
			break;
		end
	end
	if not nodeGroup or sGroup == "" then
		bUpdatingGroups = false;
		return;
	end

	local sNewValue = DB.getValue(nodeParent, "name", "");
	for _,v in pairs(powers.getWindows()) do
		if v.group.getValue() == sOldValue then
			v.group.setValue(sNewValue);
		end
	end

	bUpdatingGroups = false;

	updatePowerGroups();
end

function onPowerListChanged()
	updatePowerGroups();
	updateDisplay();
end

function onPowerGroupChanged(node)
	updatePowerGroups();
end

function addPower(bFocus)
	local w = powers.createWindow();
	if bFocus then
		w.name.setFocus();
	end
	return w;
end

function addGroupPower(sGroup, nLevel)
	local w = powers.createWindow();
	w.level.setValue(nLevel);
	w.group.setValue(sGroup);
	w.name.setFocus();
	return w;
end

function updateDisplay()
	local inCT = isInCT();
	if not inCT and parentcontrol.window.parentcontrol.window.actions_iedit then
		local bEditMode = (parentcontrol.window.parentcontrol.window.actions_iedit.getValue() == 1);
		for _,w in ipairs(powers.getWindows()) do
			if w.getClass() == "power_group_header" then
				w.iadd.setVisible(bEditMode);
			else
				w.idelete.setVisibility(bEditMode);
			end
		end
	end
end

function updatePowerGroups()
	if bUpdatingGroups then
		return;
	end
	bUpdatingGroups = true;

	rebuildGroups();

	-- Determine all the groups accounted for by current powers
	local aPowerGroups = {};
	for _,nodePower in pairs(DB.getChildren(getCharNode(), "powers")) do
		local sGroup = DB.getValue(nodePower, "group", "");
		if sGroup ~= "" then
			aPowerGroups[sGroup] = true;
		end
	end

	-- Remove the groups that already exist
	for sGroup,_ in pairs(aGroups) do
		if aPowerGroups[sGroup] then
			aPowerGroups[sGroup] = nil;
		end
	end

	-- For the remaining power groups, that aren't named
	local sLowerSpellsLabel = Interface.getString("power_label_groupspells"):lower();
	for k,_ in pairs(aPowerGroups) do
		if not aGroups[k] then
			local nodeGroups = DB.createChild(getCharNode(), "powergroup");
			local nodeNewGroup = nodeGroups.createChild();
			DB.setValue(nodeNewGroup, "name", "string", k);
			if sLowerSpellsLabel == k:lower() then
				DB.setValue(nodeNewGroup, "castertype", "string", "memorization");
			end
		end
	end

	rebuildGroups();

	bUpdatingGroups = false;

	updateHeaders();
	updateUses();
end

function updateHeaders()
	if bUpdatingGroups then
		return;
	end
	bUpdatingGroups = true;

	-- Close all category headings
	for _,v in pairs(powers.getWindows()) do
		if v.getClass() == "power_group_header" then
			v.close();
		end
	end

	-- Create new category headings
	local aCategoryWindows = {};
	local aGroupShown = {};
	for _,nodePower in pairs(DB.getChildren(getCharNode(), "powers")) do
		local sCategory, sGroup, nLevel = getWindowSortByNode(nodePower);

		if not aCategoryWindows[sCategory] then
			local wh = powers.createWindowWithClass("power_group_header");
			if wh then
				local sTooltip = getTooltipString(sGroup, nLevel);
				wh.setHeaderCategory(aGroups[sGroup], sGroup, nLevel, false, sTooltip);
			end
			aCategoryWindows[sCategory] = wh;
			aGroupShown[sGroup] = true;
		end
	end

	-- Create empty category headings
	for k,v in pairs(aGroups) do
		if not aGroupShown[k] then
			local wh = powers.createWindowWithClass("power_group_header");
			if wh then
				wh.setHeaderCategory(v, k, nil, true);
			end
		end
	end

	bUpdatingGroups = false;

	powers.applySort();
end

function getTooltipString(sGroup, nLevel)
	local nIncantationCommon = 0;
	local nIncantationRare = 0;
	local nRitualCommon = 0;
	local nRitualRare = 0;

	for _,v in pairs(powers.getWindows()) do
		if v.getClass() ~= "power_group_header" then
			local node = v.getDatabaseNode();
			local sGroupPower = DB.getValue(node, "group", "");
			local nLevelPower = DB.getValue(node, "level", 0);
			if sGroupPower == sGroup and nLevelPower == nLevel then
				local sType = DB.getValue(node, "type_spell", "");
				local sRarity = DB.getValue(node, "rarity", "");
				if sType == "power_incantation" and sRarity == "spell_common" then
					nIncantationCommon = nIncantationCommon + 1;
				elseif sType == "power_incantation" and sRarity == "spell_rare" then
					nIncantationRare = nIncantationRare + 1;
				elseif sType == "power_ritual" and sRarity == "spell_common" then
					nRitualCommon = nRitualCommon + 1;
				elseif sType == "power_ritual" and sRarity == "spell_rare" then
					nRitualRare = nRitualRare + 1;
				end
			end
		end
	end

	local sTooltip = "";
	local bAddSplit = false;
	if nIncantationCommon + nIncantationRare > 0 then
		sTooltip = Interface.getString("spell_group_tooltip_incantation"):format(nIncantationCommon, nIncantationRare);
		bAddSplit = true;
	end
	if nRitualCommon + nRitualRare > 0 then
		if bAddSplit then
			sTooltip = sTooltip .. "\n";
		end
		sTooltip = sTooltip .. Interface.getString("spell_group_tooltip_ritual"):format(nRitualCommon, nRitualRare);
	end

	return sTooltip;
end

function onPowerWindowAdded(w)
	updatePowerWindowDisplay(w);
	local nodeChar = getCharNode();
	local bMethodic = ActorManager2.isMethodicChanneler(nodeChar);
	local sMode = DB.getValue(nodeChar, "powermode", "");
	updatePowerWindowUses(nodeChar, w, bMethodic);
	-- updateActionsCost(w);
end


function updatePowerWindowDisplay(w)
	local bEditMode = getEditMode();
	w.idelete.setVisibility(bEditMode);
end

function updateActionsCost(w)
	w.header.subwindow.action_cost.saveNode(w.getDatabaseNode());
	w.header.subwindow.vigor_cost_box.saveNode(w.getDatabaseNode());
end

function updatePrepared()
	local nodeList = getDatabaseNode().createChild("powers");
	local nTotal = 0;
	for _,nodeSpell in pairs(nodeList.getChildren()) do
		local sType = DB.getValue(nodeSpell, "type_spell", 0);
		if sType == "power_incantation" or sType == "power_ritual" then
			local sNew = DB.getValue(nodeSpell, "prepared_cycler", "");
			if sNew == "power_label_prepared" then
				nTotal = nTotal + 1;
			end
		end
	end
	DB.setValue(nodeList.getChild(".."), "magic.prepared", "number", nTotal);
end


function updatePowerWindowUses(nodeChar, w, sMode, bMethodic)
	-- Get power information
	local sGroup = w.group.getValue();
	local nLevel = w.level.getValue();
	local nCast = w.cast.getValue();
	local nCharges = w.charges.getValue();
	local bPrepared = w.header.subwindow.prepared_cycler.getValue() ~= "";
	local sPathPower = w.linked_power.getValue();
	local node = w.getDatabaseNode();
	local sType = DB.getValue(node, "reftype", "");
	local sTypeSpell = DB.getValue(node, "type_spell", "");
	local sItemPath = DB.getValue(node, "item_path", "");

	-- Get the power group, and whether it's a caster group
	local rGroup = aGroups[sGroup];
	local bPrepares = bMethodic and (sType == "power_spell") and (sTypeSpell == "power_incantation" or sTypeSpell == "power_ritual");
	
	-- Get group meta usage information
	local nAvailable = 0;
	local nTotalCast = 0;
	local nTotalCharges = 0;
	if rGroup and rGroup.nTotalCharges then
		nTotalCast = rGroup.nTotalCast;
		nTotalCharges = rGroup.nTotalCharges;
	end
	
	-- Check if the whole power must be hidden
	local bShow = true;
	if sMode == "combat" then
		-- Prepared spell not available
		if bPrepares and not bPrepared then
			bShow = false;
		end
		-- Ritual requires too much time
		if sTypeSpell == "power_ritual" then
			bShow = false;
		end
		-- Max number of charges reached
		if nTotalCharges > 0 and nTotalCast >= nTotalCharges then
			Debug.chat("nTotalCharges", nTotalCharges, "nTotalCast", nTotalCast)
			bShow = false;
		end
		if nCharges > 0 and nCast >= nCharges then
			Debug.chat("nCharges", nCharges, "nCast", nCast)
			bShow = false;
		end
		-- Item spell and item not equipped
		if sItemPath ~= "" then
			local nodeItem = DB.findNode(sItemPath);
			if not nodeItem or (DB.getValue(nodeItem, "carried", 0) ~= 2) then
				bShow = false;
			end
		end
	end
	w.setFilter(bShow);

	-- Hide or show individual elements inside the power
	w.header.subwindow.prepared_cycler.setVisible(false);
	w.header.subwindow.charges.setVisible(false);
	w.header.subwindow.usesperiod.setVisible(false);
	w.header.subwindow.charges_recovery.setVisible(false);
	w.header.subwindow.counter.setVisible(false);
	w.header.subwindow.blank.setVisible(false);

	if sMode == "preparation" then
		if  bPrepares then
			w.header.subwindow.prepared_cycler.setVisible(true);
		elseif sType ~= "power_spell" then
			w.header.subwindow.charges.setVisible(true);
			w.header.subwindow.usesperiod.setVisible(true);
			local sValue = w.header.subwindow.usesperiod.getStringValue();
			local bCond = (sValue == "power_label_useperiod_daily") or (sValue == "power_label_useperiod_enc");
			w.header.subwindow.charges_recovery.setVisible(bCond);

			bCond = DB.getValue(w.getDatabaseNode(), "linked_power", "") == "";
			w.header.subwindow.charges.setEnabled(bCond);
			w.header.subwindow.usesperiod.setEnabled(bCond);
			w.header.subwindow.charges_recovery.setEnabled(bCond);

		end
		-- w.header.subwindow.blank.setVisible(true);

	else
		local bCond2 = not bPrepares and nCharges > 0;
		if bCond2 then
			w.header.subwindow.counter.setVisible(true);
			if nTotalCharges > 0 and nTotalCharges == nCharges then
				w.header.subwindow.counter.update(sMode, true, nCharges, nTotalCast, nChares);
			else
				w.header.subwindow.counter.update(sMode, true, nCharges, nCast, nChares);
			end
		end

	end

	return bShow;
end

function updateGlobalUses(nodeCast)
	local nodePower = nodeCast.getParent();
	local nCast = DB.getValue(nodePower, "cast", 0);
	local nCharges = DB.getValue(nodePower, "charges", 0);
	local sGroup = DB.getValue(nodePower, "group", "");
	local rGroup = aGroups[sGroup];

	-- IF using global charges, update the used ones
	if rGroup and rGroup.nTotalCharges > 0 and nCharges == rGroup.nTotalCharges then
		rGroup.nTotalCast = nCast;
	end

	-- Update view
	onUsesChanged();

end


function updateUses()
	if bCheckingUsage then
		return;
	end
	bCheckingUsage = true;

	-- Prepare for lots of crunching
	local nodeChar = getCharNode();
	local bMethodic = ActorManager2.isMethodicChanneler(nodeChar);
	local sMode = DB.getValue(nodeChar, "powermode", "");

	-- Add power counts, total cast and total prepared per group/slot
	for _,v in pairs(DB.getChildren(nodeChar, "powers")) do
		local sGroup = DB.getValue(v, "group", "");
		local rGroup = aGroups[sGroup];
		local nCast = DB.getValue(v, "cast", 0);
		local nCharges = DB.getValue(v, "charges", 0);
		if rGroup then
			if rGroup.grouptype ~= "power_spell" then
				rGroup.nCount = (rGroup.nCount or 0) + 1;
				rGroup.nTotalCast = (rGroup.nTotalCast or 0);
				rGroup.nTotalCharges = (rGroup.nTotalCharges or 0);
			end
		end
	end

	local aCasterGroupSpellsShown = {};

	-- Show/hide powers based on findings
	for _,v in pairs(powers.getWindows()) do
		if v.getClass() ~= "power_group_header" then
			-- Update visible elements
			if updatePowerWindowUses(nodeChar, v, sMode, bMethodic) then
				local sGroup = v.group.getValue();
				local rGroup = aGroups[sGroup];
				local bCaster = (rGroup and rGroup.grouptype == "power_spell");

				if bCaster then
					if not aCasterGroupSpellsShown[sGroup] then
						aCasterGroupSpellsShown[sGroup] = {};
					end
					local nLevel = v.level.getValue();
					aCasterGroupSpellsShown[sGroup][nLevel] = (aCasterGroupSpellsShown[sGroup][nLevel] or 0) + 1;
				elseif rGroup then
					rGroup.nShown = (rGroup.nShown or 0) + 1;
				end
			end
		end
	end

	-- Hide headers with no spells
	for _,v in pairs(powers.getWindows()) do
		if v.getClass() == "power_group_header" then
			local sGroup = v.group.getValue();
			local rGroup = aGroups[sGroup];
			local bCaster = (rGroup and rGroup.grouptype == "power_spell");

			local bShow = true;

			if sMode == "combat" then
				if bCaster then
					local nLevel = v.level.getValue();
					if not aCasterGroupSpellsShown[sGroup] or (aCasterGroupSpellsShown[sGroup][nLevel] or 0) <= 0 then
						bShow = false;
					end
				elseif rGroup then
					bShow = ((rGroup.nShown or 0) > 0);
				end
			end

			v.setFilter(bShow);
		end
	end

	powers.applyFilter();

	bCheckingUsage = false;

end

function onDrop(x, y, draginfo)
	if draginfo.isType("shortcut") then
		local sClass = draginfo.getShortcutData();
		if sClass == "reference_spell" or sClass == "power" then
			local node = draginfo.getDatabaseNode();
			if node then
				bUpdatingGroups = true;
				draginfo.setSlot(2);
				local sGroup = draginfo.getStringData();
				draginfo.setSlot(1);
				if sGroup == "" then
					sGroup = Interface.getString("power_label_groupspells");
				end
				PowerManager.addPower(sClass, draginfo.getDatabaseNode(), getCharNode(), sGroup);
				bUpdatingGroups = false;
				onPowerGroupChanged();
			else
				ChatManager.SystemMessage(Interface.getString("module_message_missinglink_wildcard"));
			end
			return true;
		end
		if sClass == "reference_classfeature" then
			bUpdatingGroups = true;
			PowerManager.addPower(sClass, draginfo.getDatabaseNode(), getCharNode());
			bUpdatingGroups = false;
			onPowerGroupChanged();
			return true;
		end
		if sClass == "reference_racialtrait" then
			bUpdatingGroups = true;
			PowerManager.addPower(sClass, draginfo.getDatabaseNode(), getCharNode());
			bUpdatingGroups = false;
			onPowerGroupChanged();
			return true;
		end
		if sClass == "reference_feat" then
			bUpdatingGroups = true;
			PowerManager.addPower(sClass, draginfo.getDatabaseNode(), ggetCharNode());
			bUpdatingGroups = false;
			onPowerGroupChanged();
			return true;
		end
		if sClass == "ref_ability" then
			bUpdatingGroups = true;
			PowerManager.addPower(sClass, draginfo.getDatabaseNode(), getCharNode());
			bUpdatingGroups = false;
			onPowerGroupChanged();
			return true;
		end
	end
end

--------------------------
-- POWER GROUP DISPLAY
--------------------------

function rebuildGroups()
	aGroups = {};

	local nodeChar = getCharNode();

	for _,v in pairs(DB.getChildren(nodeChar, "powergroup")) do
		local sGroup = DB.getValue(v, "name", "");
		local rGroup = {};

		-- Basic info
		rGroup.node = v;
		rGroup.nodename = v.getNodeName();
		if sGroup == "" then
			rGroup.grouptype = "";
		else
			rGroup.grouptype = DB.getValue(v, "grouptype", "");
		end
		-- Prepares spells
		if rGroup.grouptype == "power_spell" then
			rGroup.bPrepares = DB.getValue(nodeChar, "magic.methodic", 0) == 1;
		else
			rGroup.bPrepares = false;
		end
		-- Max charges
		if rGroup.grouptype ~= "power_spell" then
			rGroup.nTotalCharges = DB.getValue(v, "groupcharges", 0);
		else
			rGroup.nTotalCharges = 0;
		end
		if rGroup.nTotalCharges > 0 then
			rGroup.nTotalCast = calculateGlobalUses(sGroup, rGroup.nTotalCharges);
		else
			rGroup.nTotalCast = 0;
		end
		-- Default stat and skill
		rGroup.groupstat = DB.getValue(v, "groupstat", "");
		rGroup.groupskill = DB.getValue(v, "groupskill", "");
		-- Save
		aGroups[sGroup] = rGroup;
	end

end

function calculateGlobalUses(sGroup, nTotalCharges)
	for _,w in pairs(powers.getWindows()) do
		if w.getClass() ~= "power_group_header" then
			local nCast = w.cast.getValue();
			local nCharges = w.charges.getValue();
			local sCurrentGroup = DB.getValue(w.getDatabaseNode(), "group", "");
			if sGroup == sCurrentGroup and nCharges == nTotalCharges then
				return nCast;
			end
		end
	end
	return 0;
end


function getWindowSortByNode(node)
	local sGroup = DB.getValue(node, "group", "");
	local nLevel = DB.getValue(node, "level", 0);

	sCategory = sGroup;
	if aGroups[sGroup] and aGroups[sGroup].grouptype == "power_spell" then
		sCategory = sCategory .. nLevel;
	else
		nLevel = 0;
	end

	return sCategory, sGroup, nLevel;
end

function getWindowSort(w)
	local sGroup = w.group.getValue();
	local nLevel = w.level.getValue();

	sCategory = sGroup;
	if aGroups[sGroup] and aGroups[sGroup].grouptype == "power_spell" then
		sCategory = sCategory .. nLevel;
	else
		nLevel = 0;
	end

	return sCategory, sGroup, nLevel;
end

function onSortCompare(w1, w2)
	local vCategory1 = getWindowSort(w1);
	local vCategory2 = getWindowSort(w2);
	if vCategory1 ~= vCategory2 then
		return vCategory1 > vCategory2;
	end

	local bIsHeader1 = (w1.getClass() == "power_group_header");
	local bIsHeader2 = (w2.getClass() == "power_group_header");
	if bIsHeader1 then
		return false;
	elseif bIsHeader2 then
		return true;
	end

	local sValue1 = string.lower(w1.name.getValue());
	local sValue2 = string.lower(w2.name.getValue());
	if sValue1 ~= sValue2 then
		return sValue1 > sValue2;
	end
end

function onHeightenedListChanged()
	-- TODO;
end
