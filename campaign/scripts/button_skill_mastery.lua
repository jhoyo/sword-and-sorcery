--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function updateTooltip()
    local nValue = getValue();
    local sTooltip = Interface.getString("char_tooltip_prof_" .. tostring(nValue));

    if nValue < 4 then

        if nValue == 0 then
            sTooltip = sTooltip .. "\n*: ";
        elseif nValue == 1 then
            sTooltip = sTooltip .. "\n* -> **: ";
        elseif nValue == 2 then
            sTooltip = sTooltip .. "\n** -> ***: ";
        elseif nValue == 3 then
            sTooltip = sTooltip .. "\n*** -> ****: ";
        end

        local nodeSkill = getDatabaseNode().getParent();
        local nProf = DB.getValue(nodeSkill, "profession", 0);
        local sPath = getDatabaseNode().getPath();
        -- Debug.chat("sPath", sPath)

        local nCost;
        if sPath:find("skilllist_specific") then
            -- Debug.chat("Specific", nValue)
            -- Debug.chat("Specific", DataCommon.SP_cost_individual["specific"])
            nCost = DataCommon.SP_cost_individual["specific"][nValue+1];
        else
            nCost = DataCommon.SP_cost_individual["default"][nValue+1];
        end
        if nProf < 1 then
            nCost = nCost + DataCommon.SP_cost_individual["no_prof"];
        end
        sTooltip = sTooltip .. tostring(nCost) .. " " .. Interface.getString("SP");

        local nodeChar = nodeSkill.getChild("...");
        local nLevel = DB.getValue(nodeChar, "level", 0);
        local nMinLevel = 0;
        if sPath:find("skills_specific") then
            nMinLevel = DataCommon.SP_min_level["specific"][nValue+1];
        elseif sPath:find("skills_basic") then
            nMinLevel = DataCommon.SP_min_level["basic"][nValue+1];
        else
            nMinLevel = DataCommon.SP_min_level["default"][nValue+1];
        end
        if nLevel < nMinLevel then
            sTooltip = sTooltip .. string.format(Interface.getString("tooltip_stat_requires_level"), tostring(nMinLevel));
        end
    end

	setStateTooltipText(nValue, sTooltip);
end

function onInit()
    if super and super.onInit then
        super.onInit()
    end
    updateTooltip();

    local nodeSkill = getDatabaseNode().getParent();
    local nodeChar = nodeSkill.getChild("...");
    DB.addHandler(DB.getPath(nodeSkill, "profession"), 'onUpdate', updateTooltip);
    DB.addHandler(DB.getPath(nodeChar, "level"), 'onUpdate', updateTooltip);
end

function onClose()
    if super and super.onClose then
        super.onClose()
    end

    local nodeSkill = getDatabaseNode().getParent();
    local nodeChar = nodeSkill.getChild("...");
    DB.removeHandler(DB.getPath(nodeSkill, "profession"), 'onUpdate', updateTooltip);
    DB.removeHandler(DB.getPath(nodeChar, "level"), 'onUpdate', updateTooltip);
end

function onValueChanged()
    if super and super.onValueChanged then
        super.onValueChanged()
    end
    updateTooltip();
end