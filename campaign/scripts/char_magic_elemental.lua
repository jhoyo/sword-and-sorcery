--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
  local sSubpath = "skilllist_magic.elemental_skill";
  local nodeChar = getDatabaseNode();
  nodeChar.createChild(sSubpath);
  local sPathChar = DB.getPath(nodeChar);
  local sPath = sPathChar .. "." .. sSubpath;
  elemental_skill.setValue("main_skill_item", sPath);
  local nodeSkill = DB.findNode(sPath);
  nodeSkill.getChild("stat").setValue(Interface.getString("elemental"));

  sSubpath = "skilllist_magic.fire_skill";
  nodeChar.createChild(sSubpath);
  sPath = sPathChar .. "." .. sSubpath;
  fire_skill.setValue("main_skill_item", sPath);
  nodeSkill = DB.findNode(sPath);
  nodeSkill.getChild("name").setValue(Interface.getString("skill_value_fire"));

  sSubpath = "skilllist_magic.earth_skill";
  nodeChar.createChild(sSubpath);
  sPath = sPathChar .. "." .. sSubpath;
  earth_skill.setValue("main_skill_item", sPath);
  nodeSkill = DB.findNode(sPath);
  nodeSkill.getChild("name").setValue(Interface.getString("skill_value_earth"));

  sSubpath = "skilllist_magic.mind_skill";
  nodeChar.createChild(sSubpath);
  sPath = sPathChar .. "." .. sSubpath;
  mind_skill.setValue("main_skill_item", sPath);
  nodeSkill = DB.findNode(sPath);
  nodeSkill.getChild("name").setValue(Interface.getString("skill_value_mind"));

  sSubpath = "skilllist_magic.air_skill";
  nodeChar.createChild(sSubpath);
  sPath = sPathChar .. "." .. sSubpath;
  air_skill.setValue("main_skill_item", sPath);
  nodeSkill = DB.findNode(sPath);
  nodeSkill.getChild("name").setValue(Interface.getString("skill_value_air"));

  sSubpath = "skilllist_magic.water_skill";
  nodeChar.createChild(sSubpath);
  sPath = sPathChar .. "." .. sSubpath;
  water_skill.setValue("main_skill_item", sPath);
  nodeSkill = DB.findNode(sPath);
  nodeSkill.getChild("name").setValue(Interface.getString("skill_value_water"));

  sSubpath = "skilllist_magic.spirit_skill";
  nodeChar.createChild(sSubpath);
  sPath = sPathChar .. "." .. sSubpath;
  spirit_skill.setValue("main_skill_item", sPath);
  nodeSkill = DB.findNode(sPath);
  nodeSkill.getChild("name").setValue(Interface.getString("skill_value_spirit"));

  fire_skill.subwindow.setCustom(false);
  earth_skill.subwindow.setCustom(false);
  mind_skill.subwindow.setCustom(false);
  air_skill.subwindow.setCustom(false);
  water_skill.subwindow.setCustom(false);
  spirit_skill.subwindow.setCustom(false);
end
