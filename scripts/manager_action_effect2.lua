--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYEFF = "applyeff";

local aEffectVarMap = {};

function onInit()
	-- Interface.onHotkeyDrop = onHotkeyDrop;

	-- Override CoreRPG functions
	aEffectVarMap = EffectManager.aEffectVarMap;
	OOBManager.registerOOBMsgHandler(EffectManager.OOB_MSGTYPE_APPLYEFF, handleApplyEffect);
	ActionEffect.getRoll = getRoll;
	ActionEffect.onEffect = onEffect;
end

function handleApplyEffect(msgOOB)
	-- Get the target combat tracker node
	local nodeCTEntry = DB.findNode(msgOOB.sTargetNode);
	if not nodeCTEntry then
		ChatManager.SystemMessage(Interface.getString("ct_error_effectapplyfail") .. " (" .. msgOOB.sTargetNode .. ")");
		return;
	end

	-- Reconstitute the effect details
	local rEffect = {};
	for k,v in pairs(msgOOB) do
		if aEffectVarMap[k] then
			if aEffectVarMap[k].sDBType == "number" then
				rEffect[k] = tonumber(msgOOB[k]) or 0;
			else
				rEffect[k] = msgOOB[k];
			end
		elseif not rEffect[k] then
			rEffect[k] = v;
		end
	end

	-- Different if concentration
	if msgOOB.nDur and tonumber(msgOOB.nDur) > 0 then
		local rConc = {};
		rConc.nDur = tonumber(msgOOB.nDur);
		rConc.nLevel = tonumber(msgOOB.nLevel);
		rConc.sStat = msgOOB.stat;
		rConc.sSkill = msgOOB.skill;
		rConc.nID = tonumber(msgOOB.nID);
		rConc.nHiddenSpell = tonumber(msgOOB.nHiddenSpell);
		local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
		local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);
		if msgOOB.save_stat then
			rConc.sSaveStatus = ActionSkill.getSaveStatus(rTarget, rSource);
			ActionSkill.clearSaveStatus(rTarget, rSource);
			rConc.save_stat = msgOOB.save_stat;
			rConc.save_skill = msgOOB.save_skill;
			rConc.keywords = msgOOB.keywords;
			rConc.nSaveDif = msgOOB.nSaveDif;
		end

		EffectManagerSS.addConcentrationEffect(rSource, {rTarget}, rEffect, rConc, true)
	else
		-- Apply the effect
		EffectManager.addEffect(msgOOB.user, msgOOB.identity, nodeCTEntry, rEffect, true);
	end
end

function getRoll(draginfo, rActor, rAction)
	local rRoll = EffectManager.encodeEffect(rAction);
	if rRoll.sDesc == "" then
		return nil;
	end

	if draginfo and Input.isShiftPressed() then
		local aTargetNodes = {};
		local aTargets;
		if rRoll.bSelfTarget then
			aTargets = { rActor };
		else
			aTargets = TargetingManager.getFullTargets(rActor);
		end
		for _,v in ipairs(aTargets) do
			local sCTNode = ActorManager.getCTNodeName(v);
			if sCTNode ~= "" then
				table.insert(aTargetNodes, sCTNode);
			end
		end

		if #aTargetNodes > 0 then
			rRoll.aTargets = table.concat(aTargetNodes, "|");
		end
	end

	-- Description
	rRoll.sDescAux = Interface.getString("power_label_effect"):upper();
	if rAction.order and rAction.order > 1 then
		rRoll.sDescAux = rRoll.sDescAux .. " #" .. rAction.order;
	end
	rRoll.sDescAux = rRoll.sDescAux .. "\n";
	if rAction.label then
		rRoll.sDescAux = rRoll.sDescAux .. "\n" .. rAction.label:upper();
	end

	-- Cost
	rRoll.nApplyCost = rAction.nApplyCost or 0;
	rRoll.sCostType = rAction.sCostType;
	rRoll.nCost = rAction.nCost;
	rRoll.sCostResource = rAction.sCostResource;
	rRoll.nCostInitial = rAction.nCostInitial;

	-- Concentration
	if rAction.bConc then
		rRoll.nLevel = rAction.level_cast;
		rRoll.nConc = 1;
		rRoll.nDur = rAction.nDur;
		rRoll.stat = rAction.stat;
		rRoll.skill = rAction.skill;
		rRoll.nID = math.random(DataCommon.conc_ID_max);
		if rAction.bHiddenSpell then
			rRoll.nHiddenSpell = 1;
		else
			rRoll.nHiddenSpell = 0;
		end

		-- Save
		if rAction.save_stat and rAction.save_stat ~= "" then
			rRoll.save_stat = rAction.save_stat;
			rRoll.save_skill = rAction.save_skill;
			rRoll.keywords = rAction.keywords;
			rRoll.nSaveDif = rAction.dif_mod + ActorManager2.getCheck(rActor, rAction.dif_stat, rAction.dif_skill, nil, false, true, rAction.keywords);
		end
		rRoll.nMod = 0;
	end

	return rRoll;
end

function onEffect(rSource, rTarget, rRoll)
	-- Notify intention to apply effect
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	rMessage.mode = "chat_effect";
	rMessage.font = "msgfont";
	rMessage.text = rRoll.sDescAux or rMessage.text;
	if rTarget then
		rMessage.text = rMessage.text .. "\n- " .. Interface.getString("power_label_targeting") .. ": " .. ActorManager.getDisplayName(rTarget);
	end
	-- Comm.deliverChatMessage(rMessage);
	MessageManager.onRollMessage(rMessage);

	local bCriticalFail = false;
	-- Decode effect from roll
	local rEffect = EffectManager.decodeEffect(rRoll);
	rEffect.sDesc = rRoll.sDescAux;
	if not rEffect then
		ChatManager.SystemMessage(Interface.getString("ct_error_effectdecodefail"));
		return;
	end

	-- If no target, then report to chat window and exit
	if not rTarget then
		EffectManager.onUntargetedDrop(rEffect);
		rRoll.sDesc = EffectManager.encodeEffectAsText(rEffect);

		-- Report effect to chat window
		local rMessage = ActionsManager.createActionMessage(nil, rRoll);
		rMessage.icon = "roll_effect";
		rMessage.mode = "chat_unknown";
		-- Comm.deliverChatMessage(rMessage);
		MessageManager.onRollMessage(rMessage);
		return;
	end

	-- If target not in combat tracker, then we're done
	local sTargetCT = ActorManager.getCTNodeName(rTarget);
	if sTargetCT == "" then
		ChatManager.SystemMessage(Interface.getString("ct_error_effectdroptargetnotinct"));
		return;
	end

	-- If source and target, check if the target did a save to modify duration
	if rSource then
		local sSourceCT = ActorManager.getCTNodeName(rSource);
		local sDamageState, _, sSaveEffect = ActionSkill.getSaveStatus(rTarget, rSource);
		if sSaveEffect == 'power_mod_duration' then
			if sDamageState == "critical success" then
				return;
			elseif sDamageState == "success" then
				rEffect.nDuration = 1;
			elseif sDamageState == "critical failure" then
				bCriticalFail = true;
				-- TODO: Hacer que valga para algo
			end
			ActionSkill.clearSaveStatus(rTarget, rSource);
		end
	end

	-- If effect is not a CT effect drag, then figure out source and init
	if rEffect.sSource == "" then
		local sSourceCT = "";
		if rSource then
			sSourceCT = ActorManager.getCTNodeName(rSource);
		end
		if sSourceCT == "" then
			local nodeTempCT = nil;
			if User.isHost() then
				nodeTempCT = CombatManager.getActiveCT();
			else
				nodeTempCT = CombatManager.getCTFromNode("charsheet." .. User.getCurrentIdentity());
			end
			if nodeTempCT then
				sSourceCT = nodeTempCT.getNodeName();
			end
		end
		if sSourceCT ~= "" then
			rEffect.sSource = sSourceCT;
			EffectManager.onEffectSourceChanged(rEffect, DB.findNode(sSourceCT));
		end
	end

	-- If source is same as target, then don't specify a source
	if rEffect.sSource == sTargetCT then
		rEffect.sSource = "";
	end

	-- If source is non-friendly faction and target does not exist or is non-friendly, then effect should be GM only
	if (rSource and ActorManager.getFaction(rSource) ~= "friend") and (not rTarget or ActorManager.getFaction(rTarget) ~= "friend") then
		rEffect.nGMOnly = 1;
	end

	-- Cost
	if tonumber(rRoll.nApplyCost) > 0 and rRoll.nCostInitial then
		rEffect.nCost = rRoll.nCost;

		local rAction = {};
		rAction.label = "Start effect cost";
		rAction.sTargeting = "self";
		rAction.nAvoidNotifyAction = 1;
		rAction.nMod = rRoll.nCost;
		if rRoll.sCostResource == "" then
			local aClause = {dmgtype=DataCommon.dmgtypes_modifier["nonlethal"]};
			rAction.clauses = {aClause};
			rEffect.nCostVitality = 0;
		else
			rAction.clauses = {};
			rEffect.nCostVitality = 1;
		end

		local rRollCost = ActionDamage.getRoll(nil, rAction);
		if rEffect.nGMOnly then
			rRollCost.bSecret = true;
		end
		ActionsManager.actionDirect(nil, "damage", { rRollCost }, { { rSource } });
	end

	-- Concentration
	if rRoll.nDur and tonumber(rRoll.nDur) > 0 then
		rEffect.nLevel = ActionSkill.getEffectiveSpellLevel(rSource);
		rEffect.nConc = rRoll.nConc;
		rEffect.nDur = rRoll.nDur;
		rEffect.stat = rRoll.stat;
		rEffect.skill = rRoll.skill;
		rEffect.nID = rRoll.nID;
		rEffect.nHiddenSpell = rRoll.nHiddenSpell;
		_, rEffect.sSourceNode = ActorManager.getTypeAndNodeName(rSource);
		_, rEffect.sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
		-- Saves on turn start
		if rRoll.save_stat then
			rEffect.save_stat = rRoll.save_stat;
			rEffect.save_skill = rRoll.save_skill;
			rEffect.keywords = rRoll.keywords;
			rEffect.nSaveDif = rRoll.nSaveDif;
		end
	end

	-- Resolve
	-- If shift-dragging, then apply to the source actor targets, then target the effect to the drop target
	if rRoll.aTargets then
		local aTargets = StringManager.split(rRoll.aTargets, "|");
		for _,v in ipairs(aTargets) do
			rEffect.sTarget = sTargetCT;
			EffectManager.notifyApply(rEffect, v);
		end

	-- Otherwise, just apply effect to drop target normally
	else
		EffectManager.notifyApply(rEffect, sTargetCT);
	end
end