--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_TRANSFORM = "summon_npc";
OOB_MSGTYPE_TRANSFORM_BACK = "remove_npc";

--------------------
-- HANDLE TRANSFORMATIONS
--------------------

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_TRANSFORM, handleTransform);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_TRANSFORM_BACK, handleTransformBack);
end

function handleTransform(msgOOB)
	local rActor = ActorManager.resolveActor(msgOOB.sActorNode);
	local _, nodeActor = ActorManager.getTypeAndNode(rActor);
	if not nodeActor then
		return;
	end
	local nodeTransform = DB.findNode(msgOOB.sTransform);
	if not nodeTransform then
		return;
	end

	-- Transform back (just in case it is already transformed)
	transformBackCharacter(nodeActor);
	-- Save the original data
	saveOriginalData(nodeActor);
	-- Transform to new form
	transformCharacter(nodeActor, nodeTransform);
end

function handleTransformBack(msgOOB)
	local rActor = ActorManager.resolveActor(msgOOB.sActorNode);
	local _, nodeActor = ActorManager.getTypeAndNode(rActor);
	if not nodeActor then
		return;
	end
	
	-- Transform to new form
	transformBackCharacter(nodeActor);
end

----------------------------
-- AUXILIAR
----------------------------
function saveOriginalData(nodeActor)
	-- Get a blank new node for storing info
	local nodeSave = nodeActor.createChild("saved_data");
	DB.deleteChildren(nodeSave);
	local sPath = nodeActor.getPath();
	-- Save replace data
	for _,sPathAdd in pairs(DataCommon.transformation_replace) do
		local nodeOriginal = DB.findNode(sPath .. sPathAdd);
		if nodeOriginal then
			local value = nodeOriginal.getValue();
			local sType = nodeOriginal.getType();
			local nodeNew = nodeSave.createChild(sPathAdd, sType);
			nodeNew.setValue(value);
		end
	end
	-- Save skill data
	local nodeList = nodeActor.createChild("skilllist_basic");
	local nodeListNew = nodeSave.createChild("skilllist_basic");
	for _,nodeSkill in pairs(nodeList.getChildren()) do
		local sName = nodeSkill.getName();
		local nValue = DB.getValue(nodeSkill, "prof", 0);
		local nodeNew = nodeListNew.createChild(sName);
	end
	local nodeFight = ActorManager2.getSkillNode(nodeActor, Interface.getString("skill_value_fighting"));
	local nValue = DB.getValue(nodeFight, "prof", 0);
	local nodeNew = nodeSave.createChild("fighting", "number");
	nodeNew.setValue(nValue);
	-- Racial aptitudes
	nodeList = nodeActor.createChild("raciallist");
	nodeListNew = nodeSave.createChild("raciallist");
	for _,nodeApt in pairs(nodeList.getChildren()) do
		local sPath = nodeApt.getPath();
		local nRank = DB.getValue(nodeApt, "category", 1);
		local sName = DB.getValue(nodeApt, "name", "");
		local nodeNew = nodeListNew.createChild();
		DB.setValue(nodeNew, "path", "string", sPath);
		DB.setValue(nodeNew, "category", "number", nodeNew);
		DB.setValue(nodeNew, "name", "string", sName);
	end	
end


function transformCharacter(nodeActor, nodeTransform)
	-- Save health percentages
	local nVitDamage = DB.getValue(nodeActor, "health.vit.damage", 0);
	local nVitMax = DB.getValue(nodeActor, "health.vit.max", 1);
	local nVigorDamage = DB.getValue(nodeActor, "health.vigor.damage", 0);
	local nVigorMax = DB.getValue(nodeActor, "health.vigor.max", 1);
	local nVitPercent = nVitDamage / nVitMax;
	local nVigorPercent = nVigorDamage / nVigorMax
	-- Replace data
	for _,sPath in pairs(DataCommon.transformation_replace) do
		local nodeSave = nodeTransform.createChild(sPath);
		local nodeNew = nodeActor.createChild(sPath);
		if nodeSave and nodeNew then
			local sType = nodeSave.getType();
			local value = nodeSave.getValue();
			local bCond1 = sType == "number" and value ~= 0;
			local bCond2 = sType == "string" and value ~= "";
			local bCond3 = sType == "token" and value ~= "";
			local bCond4 = sType == "number" and sType == "string" and sType == "token";
			if bCond1 or bCond2 or bCond3 or bCond4 then
				nodeNew.setValue(value);
			end
		end
	end
	local sToken = DB.getValue(nodeActor, "token", "");
	local sTokenReal = DB.getValue(nodeActor, "token_real", "");
	if sTokenReal == "" then
		DB.setValue(nodeActor, "token_real", "token", sToken);
	end
	-- Save skill data
	local nodeList = nodeTransform.createChild("skilllist_basic");
	local nodeListNew = nodeActor.createChild("skilllist_basic");
	for _,nodeSkill in pairs(nodeList.getChildren()) do
		local sName = DB.getValue(nodeSkill, "name", "");
		local nodeNew = ActorManager2.getSkillNode(nodeActor, sName);
		if nodeNew then
			local nValue = DB.getValue(nodeSkill, "prof", 0);
			local nValueNew = DB.getValue(nodeNew, "prof", 0);
			if nValueNew < nValue then
				DB.setValue(nodeNew, "prof", "number", nValue);
			end
		end
	end
	local nodeFightNew = ActorManager2.getSkillNode(nodeActor, Interface.getString("skill_value_fighting"));
	if nodeFightNew then
		local nodeFight = ActorManager2.getSkillNode(nodeTransform, Interface.getString("skill_value_fighting"));
		local nValueNew = DB.getValue(nodeFightNew, "prof", 0);
		local nValue = DB.getValue(nodeFight, "prof", 0);
		if nValueNew < nValue then
			DB.setValue(nodeFightNew, "prof", "number", nValue);
		end
	end
	-- Racial aptitudes
	nodeList = nodeTransform.createChild("raciallist");
	nodeListNew = nodeActor.createChild("raciallist");
	for _,nodeApt in pairs(nodeList.getChildren()) do
		local nRank = DB.getValue(nodeApt, "category", 1);
		local sName = DB.getValue(nodeApt, "name", ""):lower();
		local bExists = false;
		for _,nodeAptNew in pairs(nodeListNew.getChildren()) do
			local nRankNew = DB.getValue(nodeAptNew, "category", 0);
			local sNameNew = DB.getValue(nodeAptNew, "name", ""):lower();
			if sNameNew:find(sName) then
				bExists = true;
				if nRank > nRankNew then
					DB.setValue(nodeAptNew, "category", "number", nRank);
					sNameNew = DB.getValue(nodeAptNew, "name", "");
					DB.setValue(nodeAptNew, "name", "string", sNameNew .. DataCommon.transformation_tag)
				end
			end
		end
		if not bExists then
			local nodeAptNew = nodeListNew.createChild();
			DB.copyNode(nodeApt, nodeAptNew);
			local sNameNew = DB.getValue(nodeAptNew, "name", ""):lower();
			DB.setValue(nodeAptNew, "name", "string", sNameNew .. DataCommon.transformation_tag)
		end
	end	
	-- Set health percentages
	nVitMax = DB.getValue(nodeActor, "health.vit.max", 1);
	nVigorMax = DB.getValue(nodeActor, "health.vigor.max", 1);
	nVitDamage = Utilities.round(nVitMax * nVitPercent);
	nVigorDamage = Utilities.round(nVigorMax * nVitPercent);
	DB.setValue(nodeActor, "health.vit.damage", "number", nVitDamage);
	DB.setValue(nodeActor, "health.vigor.damage", "number", nVigorDamage);
	-- Update CT token
	CharManager.setTokenShow(nodeChar)
	-- Save that it is transformed
	DB.setValue(nodeActor, "is_transformed", "number", 1);
end

function transformBackCharacter(nodeActor)
	-- Skip if not transformed
	if not nodeActor or DB.getValue(nodeActor, "is_transformed", 0) == 0 then
		return;
	end
	local nodeSave = nodeActor.getChild("saved_data");
	if not nodeSave then
		return;
	end
	-- Save health percentages
	local nVitDamage = DB.getValue(nodeActor, "health.vit.damage", 0);
	local nVitMax = DB.getValue(nodeActor, "health.vit.max", 1);
	local nVigorDamage = DB.getValue(nodeActor, "health.vigor.damage", 0);
	local nVigorMax = DB.getValue(nodeActor, "health.vigor.max", 1);
	local nVitPercent = nVitDamage / nVitMax;
	local nVigorPercent = nVigorDamage / nVigorMax
	-- Replace data
	for _,sPath in pairs(DataCommon.transformation_replace) do
		local nodeOriginal = nodeTransform.createChild(sPath);
		local nodeNew = nodeActor.createChild(sPath);
		if nodeOriginal and nodeNew then
			nodeNew.setValue(nodeOriginal.getValue());
		end
	end	
	-- Skill data
	local nodeList = nodeSave.createChild("skilllist_basic");
	local nodeListNew = nodeActor.createChild("skilllist_basic");
	for _,nodeSkill in pairs(nodeList.getChildren()) do
		local sName = nodeSkill.getName();
		local nodeNew = nodeListNew.getChild(sName);
		if nodeNew then
			local nValue = nodeSkill.getValue() or 0;
			DB.setValue(nodeNew, "prof", "number", nValue);
		end
	end
	local nodeFight = ActorManager2.getSkillNode(nodeActor, Interface.getString("skill_value_fighting"));
	if nodeFight then
		local nValue = DB.getValue(nodeSave, "fighting", 0);
		DB.setValue(nodeFight, "prof", "number", nValue);
	end
	-- Aptitudes
	nodeList = nodeSave.createChild("raciallist");
	nodeListNew = nodeActor.createChild("raciallist");
	for _,nodeApt in pairs(nodeList.getChildren()) do
		local sPath = DB.getValue(nodeApt, "path", "");
		local nodeNew = DB.findNode(sPath);
		if nodeNew then
			DB.setValue(nodeNew, "category", "number", DB.getValue(nodeApt, "category", 1));
			DB.setValue(nodeNew, "name", "number", DB.getValue(nodeApt, "name", ""));
		end
	end
	for _,nodeApt in pairs(nodeList.nodeListNew()) do
		local sName = DB.getValue(nodeApt, "name", "");
		if sName:find(DataCommon.transformation_tag) then
			nodeApt.delete();
		end
	end
	-- Set health percentages
	nVitMax = DB.getValue(nodeActor, "health.vit.max", 1);
	nVigorMax = DB.getValue(nodeActor, "health.vigor.max", 1);
	nVitDamage = Utilities.round(nVitMax * nVitPercent);
	nVigorDamage = Utilities.round(nVigorMax * nVitPercent);
	DB.setValue(nodeActor, "health.vit.damage", "number", nVitDamage);
	DB.setValue(nodeActor, "health.vigor.damage", "number", nVigorDamage);
	-- Update CT token
	CharManager.setTokenShow(nodeChar)
	-- Save that it is not transformed
	DB.setValue(nodeActor, "is_transformed", "number", 0);
end

