--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

-- Genders
gender_translate = {
	["man"] = "man",
	["boy"] = "man",
	["masculine"] = "man",
	["woman"] = "woman",
	["girl"] = "woman",
	["feminine"] = "woman",
};

gender_opposed = {
	["man"] = "woman",
	["woman"] = "man",
};

-- Abilities (database names)
abilities = {
	"strength",
	"constitution",
	"agility",
	"dexterity",
	"perception",
	"intelligence",
	"willpower",
	"charisma",
	"arcane",
	"sacred",
	"primal",
	"psychic",
	"elemental"
};

abilities_type = {
	["body"] = {
	"strength",
	"constitution",
	"agility",
	"dexterity",
	"perception",
	}, 
	["mind"] = {
	"intelligence",
	"willpower",
	"psychic",
	},
	["spirit"] = {
	"charisma",
	"arcane",
	"sacred",
	"primal",
	"psychic",
	"elemental"
	},
};

abilities_nogift = {
	"strength",
	"constitution",
	"agility",
	"dexterity",
	"perception",
	"intelligence",
	"willpower",
	"charisma",
};

abilities_translated = abilities;
abilities_nogift_translated = abilities_nogift;

abilities_translation = {
	["strength"] = "strength",
	["constitution"] = "constitution",
	["agility"] = "agility",
	["dexterity"] = "dexterity",
	["perception"] = "perception",
	["intelligence"] = "intelligence",
	["willpower"] = "willpower",
	["charisma"] = "charisma",
	["arcane"] = "arcane",
	["sacred"] = "sacred",
	["primal"] = "primal",
	["psychic"] = "psychic",
	["elemental"] = "elemental",
	["base"] = "base",
	["prof"] = "proficiency",
	["proficiency"] = "proficiency"
};

abilities_translation_inv = abilities_translation;

ability_gift = "gift";
ability_magic = "magic";
ability_luck = "luck";
ability_spirit = "spirit";
ability_gift_trans = ability_gift;
ability_magic_trans = ability_magic;
ability_luck_trans = ability_luck;
ability_spirit_trans = ability_spirit_trans;
aGift = {ability_gift, ability_magic};

abilities_gift = {
	"arcane",
	"sacred",
	"primal",
	"psychic",
	"elemental"
};

abilities_gift_translated = abilities_gift;

abilities_gift_translation = {
	["arcane"] = "arcane",
	["sacred"] = "sacred",
	["primal"] = "primal",
	["psychic"] = "psychic",
	["elemental"] = "elemental"
};

abilities_gift_translation_inv = abilities_gift_translation;

ability_ltos = {
	["strength"] = "STR",
	["constitution"] = "CON",
	["dexterity"] = "DEX",
	["agility"] = "AGI",
	["perception"] = "PER",
	["intelligence"] = "INT",
	["willpower"] = "WIL",
	["charisma"] = "CHA",
	["arcane"] = "ARC",
	["sacred"] = "SAC",
	["primal"] = "PRI",
	["psychic"] = "PSI",
	["elemental"] = "ELE",
	["base"] = "Base"
};

ability_stol = {
	["STR"] = "strength",
	["CON"] = "constitution",
	["AGI"] = "agility",
	["DEX"] = "dexterity",
	["PER"] = "perception",
	["INT"] = "intelligence",
	["WIL"] = "willpower",
	["CHA"] = "charisma",
	["ARC"] = "arcane",
	["SAC"] = "sacred",
	["PRI"] = "primal",
	["PSI"] = "psychic",
	["ELE"] = "elemental",
};

ability_stol_translated = ability_stol;

ability_limited = {
	"agility",
	"dexterity",
	"arcane",
	"elemental",
	"perception",
	"psychic",
	"primal",
	"skill",
	"skillmagic",
	"skillnatural",
	"skillhelmet",
};
ability_limited_magicarmor = {
	"arcane",
	"elemental",
};
ability_limited_always = {
	"agility",
	"dexterity",
	"skillmagic",
};
ability_limited_weight = {
	"agility",
	"dexterity",
	"skill",
};
ability_limited_helmet = {
	"perception",
	"psychic",
	"skillhelmet",
};
ability_limited_nonatural = {
	"primal",
	"skillnatural",
};
ability_limited_shield = {
	"arcane",
	"elemental",
	"psychic",
	"skillmagic",
	"skillhelmet",
};
ability_limited_shield_nonatural = {
	"primal",
	"skillnatural",
};
ability_limited_value = {
	[0] = "",
	[1] = "skill",
	[2] = "skillmagic",
	[3] = "skillnatural",
	[4] = "skillhelmet",
}

-- Values for wound comparison
healthstatusfull = "healthy";
healthstatusscratched = "scratched";
healthstatushalf = "wounded";
healthstatuswounded = "critical";

-- Values for size comparison
sizelabels = {
	["tiny"] = "tiny",
	["small"] = "small",
	["medium"] = "medium",
	["large"] = "large",
	["huge"] = "huge",
	["gargantuan"] = "gargantuan",
};

size_default = "medium";


creaturesizespace = {
	["tiny"] = 0.5,
	["small"] = 1,
	["medium"] = 1,
	["large"] = 2,
	["huge"] = 3,
	["gargantuan"] = 4,
};


creaturesize = {
	["tiny"] = 1,
	["small"] = 2,
	["medium"] = 3,
	["large"] = 4,
	["huge"] = 5,
	["gargantuan"] = 6,
};

size_extra_range = {
	["tiny"] = -1,
	["small"] = 0,
	["medium"] = 0,
	["large"] = 1,
	["huge"] = 1,
	["gargantuan"] = 2,
};

size_modifier = {
	["tiny"] = 3,
	["small"] = 1,
	["medium"] = 0,
	["large"] = -1,
	["huge"] = -3,
	["gargantuan"] = -5,
};

size_carry_multiplier = {
	["tiny"] = 0.5,
	["small"] = 0.75,
	["medium"] = 1,
	["large"] = 1.5,
	["huge"] = 2,
	["gargantuan"] = 3,
};

size_sprint_multiplier = {
	["tiny"] = 1.5,
	["small"] = 1.25,
	["medium"] = 1,
	["large"] = 0.75,
	["huge"] = 0,
	["gargantuan"] = 0,
};

size_run_multiplier = {
	["tiny"] = 1.5,
	["small"] = 1.25,
	["medium"] = 1,
	["large"] = 0.75,
	["huge"] = 0,
	["gargantuan"] = 0,
};
quadruped_run_mult = 1.5;
quadruped_sprint_mult = 2;

base_sanity_mult = 5;
base_spirit = 3;

base_sanity_mult = 5;
base_spirit = 3;

-- Values for creature type comparison
creaturedefaulttype = "humanoid";
creaturehalftype = "half-";
creaturehalftypesubrace = "human";
creaturetype = {
	"aberration",
	"beast",
	"celestial",
	"construct",
	"dragon",
	"elemental",
	"fey",
	"fiend",
	"giant",
	"humanoid",
	"ooze",
	"plant",
	"undead",
};
creaturesubtype = {
	"dragonborn",
	"dwarf",
	"elf",
	"gith",
	"gnoll",
	"gnome",
	"goblin",
	"goblinoid",
	"halfling",
	"human",
	"kobold",
	"lizardfolk",
	"orc",
	"shapechanger",
	"half-blood"
};

-- Values supported in effect conditionals
conditionaltags = {
};

conditions_number = {
	["Tired"] = "Tired",
	["Drained"] = "Drained",
	["Wounded"] = "Wounded",
	["Morale"] = "Morale",
};

-- Conditions supported in effect conditionals and for token widgets
conditions = {
	["Entangled"] = "Entangled",
	["Frightened"] = "Frightened",
	["Terrified"] = "Terrified",
	["Stunned"] = "Stunned",
	["Ashamed"] = "Ashamed",
	["Blinded"] = "Blinded",
	["Confused"] = "Confused",
	["Weakened"] = "Weakened",
	["Dazzled"] = "Dazzled",
	["Unaware"] = "Unaware",
	["Distracted"] = "Distracted",
	["Asleep"] = "Asleep",
	["Maddened"] = "Maddened",
	["Muted"] = "Muted",
	["Deafened"] = "Deafened",
	["Stupified"] = "Stupified",
	["Turned"] = "Turned",
	["Fascinated"] = "Fascinated",
	["Unconscious"] = "Unconscious",
	["Helpless"] = "Helpless",
	["Immobilized"] = "Inmobilized",
	["Dizzy"] = "Dizzy",
	["Dying"] = "Dying",
	["Hidden"] = "Hidden",
	["Paralyzed"] = "Paralyzed",
	["Petrified"] = "Petrified",
	["Unwilled"] = "Unwilled",
	["Trance"] = "Trance",
	["Prone"] = "Prone",
	["Dead"] = "Dead",
	["No-sprint"] = "No sprint",
	["No-run"] = "No run",
	["No-walk"] = "No walk",
};

conditions_mental = {
	["Frightened"] = "Frightened",
	["Terrified"] = "Terrified",
	["Stunned"] = "Stunned",
	["Ashamed"] = "Ashamed",
	["Confused"] = "Confused",
	["Distracted"] = "Distracted",
	["Asleep"] = "Asleep",
	["Maddened"] = "Maddened",
	["Stupified"] = "Stupified",
	["Fascinated"] = "Fascinated",
	["Unconscious"] = "Unconscious",
	["Unwilled"] = "Unwilled",
	["Trance"] = "Trance",
	["MORAL"] = "MORAL",
};
conditions_emotional = {
	["Frightened"] = "Frightened",
	["Terrified"] = "Terrified",
	["Ashamed"] = "Ashamed",
	["Maddened"] = "Maddened",
	["Fascinated"] = "Fascinated",
	["Trance"] = "Trance",
	["MORAL"] = "MORAL",
};
condition_mental = "mental";
condition_emotional = "emotional";
conditions_end_concentration = {"Asleep", "Maddened", "Stupified", "Unconscious", "Unwilled", "Dead", "Dying"};
conditions_concentration_start_turn = {"Confused", "Terrified", "Stunned", "Trance"};
conditions_not_in_melee = {"Terrified", "Stunned", "Blinded", "Confused", "Unaware", "Asleep", "Stupified", "Fascinated", "Unconscious", "Helpless", "Dying", "Hidden", "Paralyzed", "Petrified", "Unwilled", "Trance", "Prone", "Dead", "Entangled"};
conditions_not_obstacle = {"Dying", "Hidden", "Prone", "Asleep", "Unconscious", "Dead"};
conditions_not_actions = {"Asleep", "Unconscious", "Dying", "Paralyzed", "Petrified", "Trance", "Dead"};
conditions_not_reactions = {"Stuned", "Confused", "Unaware", "Helpless", "Fascinated"};
conditions_unaware = {"Entangled", "Stunned", "Blided", "Unaware", "Fascinated"};
conditions_unaware_attack = {"Hidden", "Invisible"};
conditions_helpless = {"Helpless", "Unconscious", "Dying", "Paralyzed", "Petrified", "Trance", "Dead"};

concentration_damage_dif = {
	["damage_base"] = 0,
	["damage_mult"] = 2,
	["nonlethal_base"] = 5,
	["nonlethal_mult"] = 1,
	["sanity_base"] = 7,
	["sanity_mult"] = 3,
	["sanity_perm_base"] = 7,
	["sanity_perm_mult"] = 5,
	["spirit_base"] = 7,
	["spirit_mult"] = 5,
	["spirit_perm_base"] = 7,
	["spirit_perm_mult"] = 7,
	["stat_base"] = 7,
	["stat_mult"] = 3,
};
mentall_illness_dif_mult = 3;
condition_maddened_threshold = 5;
condition_reduced_stats = {
	["strength"] = "Inmobilized; Prone",
	["constitution"] = "Unconscious",
	["agility"] = "Inmobilized",
	["dexterity"] = "Unable to use hands",
	["perception"] = "Blinded; Deafened",
	["intelligence"] = "Maddened",
	["willpower"] = "Unwilled",
	["charisma"] = "Unable to comunicate",
	["arcana"] = "Unable to channel",
	["sacred"] = "Unable to channel",
	["primal"] = "Unable to channel",
	["psychic"] = "Unable to channel",
	["elemental"] = "Unable to channel",
};
conditions_zero_stats = {
	["strength"] = "Dead",
	["constitution"] = "Dead",
	["agility"] = "Dead",
	["dexterity"] = "Dead",
	["perception"] = "Dead",
	["intelligence"] = "Dead",
	["willpower"] = "Dead",
	["charisma"] = "Dead",
	["arcana"] = "Unable to channel; No magic senses",
	["sacred"] = "Unable to channel; No magic senses",
	["primal"] = "Unable to channel; No magic senses",
	["psychic"] = "Unable to channel; No magic senses",
	["elemental"] = "Unable to channel; No magic senses",
};

conditions_desc = {
	["Entangled"] = "El personaje se encuentra inmovilizado, desprevenido, y posee desventaja en todas sus pruebas que no sean salvaciones. Tambien recibe desventaja en todas sus salvaciones de Agilidad.",
	["Frightened"] = "El personaje debe pelear a la defensiva contra la fuente de su miedo. No podra acercarse a ella salvo que supere una prueba de Voluntad + Coraje (dificultad 15 por defecto). Si el personaje sufre algun otro efecto que le provoque estar asustado, pasa a estar aterrorizado.",
	["Terrified"] = "El personaje obtiene desventaja en todas sus pruebas de ataque mientras la fuente de su miedo este presente, y no puede acercarse voluntariamente a ella. Debera emplear sus acciones en alejarse de la fuente del miedo todo lo posible (sin llegar a ser suicida, al menos en principio). Ademas, sufre desventaja en las salvaciones de Coraje. El DJ puede permitir al personaje repetir la salvacion al final de cada uno de los turnos que el personaje tenga este rasgo, o bien cuando se den ciertas situaciones.",
	["Stunned"] = "El personaje realiza todas sus acciones de ataque, pruebas de Concentracion y pruebas de iniciativa en desventaja. Obtiene un penalizador de 3 a sus Defensas, y no puede realizar la actividad de Esprintar. Debe realizar una prueba de salvacion para comprobar si mantiene la concentración al inicio de cada uno de sus turnos. Este es un efecto Mental.",
	["Ashamed"] = "El personaje obtiene un penalizador de -1 a sus pruebas de ataque y desventaja en todas sus pruebas sociales.",
	["Blinded"] = "El personaje se encuentra desprevenido. Todas sus pruebas de Percepcion basadas en la vista obtienen un fallo critico automatico, y es inmune a efectos visuales. Recibe desventaja en todas las pruebas que empleen el uso de la vista (como realizar un ataque). Trata a todos los personajes como no detectados, a no ser que supere una prueba de Percepcion + Alerta basada en un sentido diferente a la vista, en cuyo caso contaran como localizadas (desventaja en las pruebas de ataque). Se sobrepone al Estado deslumbrado.",
	["Confused"] = "El personaje no puede emplear reacciones ni concentrarse. Realiza sus pruebas de Iniciativa en desventaja, y otorga ventaja en las pruebas de ataque. Debe usar sus acciones para atacar al ultimo personaje que te ataco el ultimo turno. Si no hay tal enemigo, lanza 1d8. 1: se ataca a si mismo una vez y su turno termina. El daño infligido corresponde al daño del arma mas su Fuerza. 2-4: Ataca al personaje mas cercano. 5-7:  Pierde el turno balbuceando incoherentemente. 8: Puede actuar normalmente.",
	["Weakened"] = "El personaje recibe una penalizacion de -3 a sus Defensas, y desventaja a sus pruebas de ataque y sus pruebas de Fuerza, Constitucion, Destreza y Agilidad, y una reduccion de 1 a su Velocidad.",
	["Dazzled"] = "El personaje obtiene un penalizador de -3 todas sus pruebas de ataque, Defensas y otras pruebas que requieran el uso de la vista.",
	["Unaware"] = "El personaje no puede usar más Defensas que las de desprevenido e indefenso. Además, realiza en desventaja todas sus pruebas de salvación de Reflejos.",
	["Distracted"] = "El personaje obtiene un penalizador de -1 a todas sus pruebas y sus Defensas, y desventaja en sus pruebas de Iniciativa y Alerta.",
	["Asleep"] = "El personaje se encuentra indefenso, cegado y no puede realizar ninguna accion ni concentrarse. Realiza todas las pruebas de Percepcion y salvaciones de Voluntad en desventaja, y obtiene un fallo critico en todas las salvaciones de Agilidad, Fuerza y Destreza. Cuando se adquiere este Estado, tambien se adquiere el Estado tumbado. El personaje pierde el Estado dormido cuando sufre daño, un aliado usa la accion Despertar, o el personaje supera una prueba de Percepcion + Alerta al escuchar un ruido (dificultad 15 + 3/categoria de intensidad por encima de normal).",
	["Maddened"] = "El personaje no es dueño de si mismo. El personaje se vuelve inmune al cansancio y al dolor, pero sera el DJ quien decida su curso de accion.",
	["Muted"] = "El personaje no puede emitir sonidos por su boca (por que no tiene, porque esta amordazado, porque un conjuro se lo impide, etc.). El personaje no puede hablar ni realizar Palabras de poder (ver seccion Magia).",
	["Deafened"] = "El personaje recibe un fallo critico en todas las pruebas de Percepcion relacionadas con el oido, y desventaja en aquellas que requieran su uso (como el realizar un conjuro con componente verbal). Ademas, es inmune a los efectos auditivos y resistente al daño sonico. ",
	["Turned"] = "El personaje debe alejarse todo lo posible de la fuente de su expulsion. Obtiene desventaja en todas sus pruebas de ataque y debe priorizar el alejarse todo lo posible de la fuente de su expulsion. ",
	["Fascinated"] = "El personaje queda Desprevenido y obtiene desventaja a todas las pruebas de Iniciativa y Percepcion no relacionadas con el motivo de su fascinacion, y queda desprevenido frente a otras amenazas. No puede realizar acciones hostiles contra el origen de su fascinacion y aquellos que perciba como sus aliados. El origen de su fascinacion obtiene ventaja en todas las pruebas de Carisma para afectar al personaje fascinado. Si el o sus aliados sufren alguna accion hostil (y se da cuenta), el estado de fascinacion terminara.",
	["Stupified"] = "La mente del personaje no funciona correctamente. El personaje es incapaz de concentrarse y realizar conjuros, tiene desventaja en todas las pruebas de Voluntad, Inteligencia y Carisma, y un penalizador de -3 a sus Defensas.",
	["Unconscious"] = "Cuando adquiere este Estado, el personaje cae al suelo y suelta lo que lleve en las manos. El personaje queda indefenso, no puede realizar acciones ni concentrarse y recibe un fallo critico en todas sus pruebas de Percepcion y sus salvaciones de Fuerza, Destreza y Agilidad, y desventaja en las de Voluntad. ",
	["Helpless"] = "El personaje pierde todas sus Defensas salvo la de indefenso. El personaje falla criticamente todas sus salvaciones de Fuerza, Destreza y Agilidad.",
	["Immobilized"] = "La Velocidad del personaje se reduce a 0, y obtiene desventaja en las pruebas de Agilidad y Destreza.",
	["Dizzy"] = "El personaje recibe desventaja en todas sus pruebas, y un penalizador de -3 a todas sus Defensas.",
	["Dying"] = "El personaje se encuentra a las puertas de la muerte. Mientras posea este Estado, el personaje no puede perder el estado Inconsciente.",
	["Hidden"] = "El personaje se encuentra escondido. Recibe todos los ataques en desventaja.",
	["Paralyzed"] = "El personaje se encuentra indefenso, inmovil y no puede llevar a cabo acciones que requieran movimiento alguno salvo respirar. ",
	["Petrified"] = "El personaje cae inconsciente y adquiere proteccion natural 10 al daño fisico y 5 al resto de tipos de daño. Atacar con armas convencionales a un personaje petrificado puede resultar en daño para las armas.",
	["Unwilled"] = "El personaje obtiene un fallo critico en todas sus pruebas de Voluntad y acatara toda orden que reciba de su amo (de cualquiera si no tiene amo).",
	["Trance"] = "El personaje queda Indefenso, y no puede realizar acciones de ningun tipo. Si se le mueve amablemente, obedece. Este estado termina si el personaje recibe daño o un movimiento brusco (como una sacudida).",
	["Prone"] = "El personaje tiene desventaja en todas sus pruebas de ataque salvo ciertas armas (como una ballesta), otorga ventaja en los ataques cuerpo a cuerpo y obtiene un +3 a sus Defensas contra ataques a distancia.",
	["Dead"] = "El personaje esta muerto.",
	["No-sprint"] = "No puede Esprintar ni dar un Paso rapido.",
	["No-run"] = "No puede Correr, Esprintar ni dar un Paso rapido.",
	["No-walk"] = "No puede Andar, Correr, Esprintar ni dar un Paso rapido.",
};
point_blank_shot = "Point blank shot";

pain = "pain";
tiredness = "tiredness";

-- All states keywords
keyword_states = {
	["INIT"] = "INIT",
	["ADVINIT"] = "ADVINIT",
	["CHECK"] = "CHECK",
	["DEF"] = "DEF",
	["ATK"] = "ATK",
	["DMG"] = "DMG",
	["HEAL"] = "HEAL",
	["STR"] = "STR",
	["CON"] = "CON",
	["AGI"] = "AGI",
	["DEX"] = "DEX",
	["PER"] = "PER",
	["INT"] = "INT",
	["WIL"] = "WIL",
	["CHA"] = "CHA",
	["ARC"] = "ARC",
	["SAC"] = "SAC",
	["PRI"] = "PRI",
	["PSY"] = "PSY",
	["ELE"] = "ELE",
	["LEVEL"] = "LEVEL",
	["ESSENCE"] = "ESSENCE",
	["SAVE"] = "SAVE",
	["ADVSAVE"] = "ADVSAVE",
	["INMUNE"] = "INMUNE",
	["VULN"] = "VULN",
	["RESIST"] = "RESIST",
	["ABSORB"] = "ABSORB",
	["COVER"] = "COVER",
	["REGEN"] = "REGEN",
	["TRUEREGEN"] = "MREGEN",
	["DMGO"] = "DMGO",
	["AURA"] = "AURA",
	["PIERCING"] = "PIERCING",
	["MPIERCING"] = "MPIERCING",
	["PROT"] = "PROT",
	["NATPROT"] = "NATPROT",
	["MPROT"] = "MPROT",
	["APROT"] = "APROT",
	["RANGE"] = "RANGE",
	["ADVATK"] = "ADVATK",
	["ADVCHECK"] = "ADVCHECK",
	["GRANTADVATK"] = "GRANTADVATK",
	["CRITATK"] = "CRITATK",
	["CRITDEF"] = "CRITDEF",
	["MORAL"] = "MORAL",
	["DMGTYPE"] = "DMGTYPE",
	["ACTIONS"] = "ACTIONS",
	["FOCUS"] = "FOCUS",
	["MISSCAST"] = "MISSCAST",
	["ONSTART"] = "ONSTART",
	["TIED"] = "TIED",
	["DEFMULT"] = "DEFMULT",
	["DEFRANGEDMULT"] = "DEFRANGEDMULT",
	["READMAGIC"] = "READMAGIC",
	["DETECTMAGIC"] = "DETECTMAGIC",
	["DMGMINCAT"] = "DMGMINCAT",
	["PEN"] = "PEN",
	["REST"] = "REST",
	["GRANTDMG"] = "GRANTDMG",
	["REROLL"] = "REROLL",
	["AFFLICTION"] = "AFFLICTION",
	["RECHARGE"] = "RECHARGE",
	["CONCENTRATION"] = "CONCENTRATION",
	["CARRYSTR"] = "CARRYSTR",
};

keyword_inactive = {
	["conc"] = "(Conc)",
	["style"] = "(Style)",
	["hidden"] = "(Hidden)",
	["curse"] = "(Curse)",
	["curse"] = "(Curse)",
	["summon"] = "Summon:",
	["save_start"] = "(start_save)",
	["from_aura"] = "(From_aura)",
	["temp_vigor"] = "(Temp. Vigor)",
	["temp_vit"] = "(Temp. Vit.)",
	["temp_vigor"] = "(Temp. Vigor)",
	["temp_vit"] = "(Temp. Vit.)",
};

keyword_states_data = {}; -- TODO in english

max_actions = {
	["action"] = 2,
	["fast"] = 1,
	["reaction"] = 1,
	["tiring"] = 3,
};
type_actions = {
	["action"] = "action",
	["fast"] = "fast",
	["reaction"] = "reaction",
	["turn"] = "turn",
	["variable"] = "variable",
	["tiring"] = "tiring",
};
type_actions_label = {
	["action"] = "action",
	["fast"] = "fast action",
	["reaction"] = "reaction",
	["turn"] = "complete turn action",
	["variable"] = "variable actions",
};
type_actions_label_plural = {
	["action"] = "%s actions",
	["fast"] = "%s fast actions",
	["reaction"] = "%s reactions",
};
type_actions_matcher = {
	["action"] = "(%d+) actions",
	["fast"] = "(%d+) fast actions",
	["reaction"] = "(%d+) reactions",
};

concentration_keywords = {
	["type"] = "type",
	["adv"] = "adv",
	["dur"] = "dur",
	["mod"] = "mod",
	["durmult"] = "durmult",
};


onstart_keywords = {
	["save"] = "save",
	["dif"] = "dif",
	["mod_dur"] = "duration",
	["mod_dano"] = "damage",
};

-- Bonus/penalty effect types for token widgets
bonuscomps = {
	["INIT"] = "INIT",
	["CHECK"] = "CHECK",
	["DEF"] = "DEF",
	["ATK"] = "ATK",
	["DMG"] = "DMG",
	["HEAL"] = "HEAL",
	["STR"] = "STR",
	["CON"] = "CON",
	["AGI"] = "AGI",
	["DEX"] = "DEX",
	["PER"] = "PER",
	["INT"] = "INT",
	["WIL"] = "WIL",
	["CHA"] = "CHA",
	["SAVE"] = "SAVE",
	["PIERCING"] = "PIERCING",
	["MPIERCING"] = "MPIERCING",
	["PROT"] = "PROT",
	["NATPROT"] = "NATPROT",
	["RANGE"] = "RANGE",
	["ADVATK"] = "ADVATK",
	["ADVCHECK"] = "ADVCHECK",
	["GRANTADVATK"] = "GRANTADVATK",
	["CRITATK"] = "CRITATK",
	["CRITDEF"] = "CRITDEF",
};

-- Condition effect types for token widgets
condcomps = {
	["blinded"] = "cond_blinded",
	["charmed"] = "cond_charmed",
	["deafened"] = "cond_deafened",
	["encumbered"] = "cond_encumbered",
	["frightened"] = "cond_frightened",
	["grappled"] = "cond_grappled",
	["incapacitated"] = "cond_paralyzed",
	["incorporeal"] = "cond_incorporeal",
	["invisible"] = "cond_invisible",
	["paralyzed"] = "cond_paralyzed",
	["petrified"] = "cond_paralyzed",
	["prone"] = "cond_prone",
	["restrained"] = "cond_restrained",
	["stunned"] = "cond_stunned",
	["unconscious"] = "cond_unconscious"
}; -- TODO

-- Other visible effect types for token widgets
othercomps = {
	["COVER"] = "cond_cover",
	["SCOVER"] = "cond_cover",
	["IMMUNE"] = "cond_immune",
	["RESIST"] = "cond_resist",
	["VULN"] = "cond_vuln",
	["REGEN"] = "cond_regen",
	["DMGO"] = "cond_ongoing"
};

-- Full scale widgets
condfull_ordered = {
	[1] = "Dead";
	[2] = "Unconscious";
	[3] = "Asleep";
};
condfull = {
	["Dead"] = "cond_full_dead",
	["Asleep"] = "cond_full_asleep",
	["Unconscious"] = "cond_full_asleep",
}

-- Effect components which can be targeted
targetableeffectcomps = {
	"COVER",
	"SCOVER",
	"AC",
	"SAVE",
	"ATK",
	"DMG",
	"IMMUNE",
	"VULN",
	"RESIST",
	"ABSORB"
};

connectors = {
	"and",
	"or"
};

-- Range types supported
rangetypes = {
	["melee"] = "melee",
	["ranged"] = "ranged",
	["thrown"] = "thrown",
	["distance"] = "distance",
};

rangetypes_inv = rangetypes;

opportunity = "opportunity";

-- Damage types supported
dmgtypes = {
	"physical",
	"acid",		-- ENERGY TYPES
	"cold",
	"fire",
	"energy",
	"lightning",
	"necrotic",
	"toxic",
	"organic",
	"psychic",
	"radiant",
	"thunder",
	"essence",    -- SPECIAL DAMAGE TYPES
	"madness",
	"spirit",
	"gift",
	"adamantine", 	-- WEAPON PROPERTY DAMAGE TYPES
	"iron",
	"silver",
	"magic",        -- MODIFIER DAMAGE TYPES
	"nonlethal",
	"precision",
	"unavoidable"
};

dmgtypes_basic = {
	["physical"] = "physical",
	["acid"] = "acid",		-- ENERGY TYPES
	["cold"] = "cold",
	["fire"] = "fire",
	["energy"] = "energy",
	["lightning"] = "lightning",
	["necrotic"] = "necrotic",
	["toxic"] = "toxic",
	["organic"] = "organic",
	["psychic"] = "psychic",
	["radiant"] = "radiant",
	["thunder"] = "thunder",
};

dmgtypes_special = {
	["essence"] = "essence",
	["madness"] = "madness",
	["spirit"] = "spirit",
	-- ["gift"] = "gift",
	["all"] = "all",
};
dmgtypes_special_translated = dmgtypes_special;

dmgtypes_material = {
	"adamantine", 
	"iron",
	"silver",
};

dmgtypes_modifier = {	
	["magic"] = "magic",
	["nonlethal"] = "nonlethal",
	["precision"] = "precision",
	["unavoidable"] = "unavoidable"
};
dmgtypes_modifier_translated = dmgtypes_modifier;

healdamage = {
	"magicheal",
	"trueheal",
	"magictrueheal",
	"recover",
	"temphp",
	"tempvig"
};

damage_vit_to_vigor = 1;
damage_barrier = {
	["nonlethal"] = 0.5,
	["lethal"] = 1,
	["essence"] = 3,
	["stat"] = 5,
	["mental_nonlethal"] = 3,
	["mental"] = 6,
	["spirit_nonlethal"] = 5,
	["spirit"] = 10,
};

damage_save_mult = {
	["critical success"] = 0,
	["success"] = 0.5,
	["failure"] = 1,
	["critical failure"] = 1.5,
};

-- Bonus types supported in power descriptions
bonustypes = {
};
stackablebonustypes = {
};

function onInit()

	-- Skills
	skilldata_basic = {
		[Interface.getString("skill_value_alert")] = { lookup = "alert", stat = 'perception', min=1, lim=1 },
		[Interface.getString("skill_value_concentration")] = { lookup = "concentration", stat = 'willpower', min=1 },
		[Interface.getString("skill_value_courage")] = { lookup = "courage", stat = 'willpower', min=1 },
		[Interface.getString("skill_value_dodge")] = { lookup = "dodge", stat = 'agility', min=1, lim=1 },
		[Interface.getString("skill_value_insight")] = { lookup = "insight", stat = 'charisma', min=1 },
		[Interface.getString("skill_value_knowledge_common")] = { lookup = "knowledge_common", stat = 'intelligence', min=1 },
		[Interface.getString("skill_value_might")] = { lookup = "might", stat = 'strength', min=1 },
		[Interface.getString("skill_value_reflexes")] = { lookup = "reflexes", stat = 'dexterity', min=1, lim=1 },
		[Interface.getString("skill_value_resistence")] = { lookup = "resistence", stat = 'constitution', min=1 },
	};

	skilldata_learned = {
		[Interface.getString("skill_value_acrobatics")] = { lookup = "acrobatics", stat = 'agility', lim=1 },
		[Interface.getString("skill_value_animalhandling")] = { lookup = "animalhandling", stat = 'charisma' },
		[Interface.getString("skill_value_deception")] = { lookup = "deception", stat = 'charisma' },
		[Interface.getString("skill_value_diplomacy")] = { lookup = "diplomacy", stat = 'charisma' },
		[Interface.getString("skill_value_intimidation")] = { lookup = "intimidation", stat = 'charisma' },
		[Interface.getString("skill_value_investigation")] = { lookup = "investigation", stat = 'intelligence' },
		[Interface.getString("skill_value_leadership")] = { lookup = "leadership", stat = 'charisma' },
		[Interface.getString("skill_value_medicine")] = { lookup = "medicine", stat = 'intelligence' },
		[Interface.getString("skill_value_mount")] = { lookup = "mount", stat = 'agility', lim=1 },
		[Interface.getString("skill_value_sleightofhand")] = { lookup = "sleightofhand", stat = 'dexterity' },
		[Interface.getString("skill_value_stealth")] = { lookup = "stealth", stat = 'agility', disarmorstealth = 1, lim=1 },
		[Interface.getString("skill_value_survival")] = { lookup = "survival", stat = 'intelligence' },
		[Interface.getString("skill_value_locksmith")] = { lookup = "locksmith", stat = 'dexterity' },
	};

	skilldata_specific = {
		[Interface.getString("skill_value_performance")] = { lookup = "performance", stat = 'charisma' },
		[Interface.getString("skill_value_disguise")] = { lookup = "disguise", stat = 'intelligence' },
		[Interface.getString("skill_value_traps")] = { lookup = "traps", stat = 'dexterity' },
		[Interface.getString("skill_value_knowledge_arcane")] = { lookup = "knowledge_arcane", stat = 'intelligence' },
		[Interface.getString("skill_value_knowledge_science")] = { lookup = "knowledge_science", stat = 'intelligence' },
		[Interface.getString("skill_value_knowledge_history")] = { lookup = "knowledge_history", stat = 'intelligence' },
		[Interface.getString("skill_value_knowledge_nature")] = { lookup = "knowledge_nature", stat = 'intelligence' },
		[Interface.getString("skill_value_knowledge_military")] = { lookup = "knowledge_military", stat = 'intelligence' },
		[Interface.getString("skill_value_knowledge_religion")] = { lookup = "knowledge_religion", stat = 'intelligence' },
		[Interface.getString("skill_value_knowledge_realmatic")] = { lookup = "knowledge_realmatic", stat = 'intelligence' },
		[Interface.getString("skill_value_poisons")] = { lookup = "poisons", stat = 'intelligence' },
		[Interface.getString("skill_value_gambling")] = { lookup = "gambling", stat = 'intelligence' },
		[Interface.getString("skill_value_trade")] = { lookup = "gambling", stat = 'charisma' },
	};

	skilldata_weaponary = {
		[Interface.getString("skill_value_bows")] = { lookup = "bows", stat = 'dexterity' },
		[Interface.getString("skill_value_firearms")] = { lookup = "firearms", stat = 'dexterity' },
		[Interface.getString("skill_value_crossbows")] = { lookup = "crossbows", stat = 'dexterity' },
		[Interface.getString("skill_value_fencing")] = { lookup = "fencing", stat = 'dexterity' },
		[Interface.getString("skill_value_polearms")] = { lookup = "polearms", stat = 'strength' },
		[Interface.getString("skill_value_shields")] = { lookup = "shields", stat = 'strength' },
		[Interface.getString("skill_value_swords")] = { lookup = "swords", stat = 'strength' },
		[Interface.getString("skill_value_axes")] = { lookup = "axes", stat = 'strength' },
		[Interface.getString("skill_value_fighting")] = { lookup = "fighting", stat = 'strength' },
		[Interface.getString("skill_value_simple")] = { lookup = "simple", stat = 'strength' },
		[Interface.getString("skill_value_light_armor")] = { lookup = "light_armor", stat = '' },
		[Interface.getString("skill_value_medium_armor")] = { lookup = "medium_armor", stat = '' },
		[Interface.getString("skill_value_heavy_armor")] = { lookup = "heavy_armor", stat = '' },
		[Interface.getString("skill_value_lefthanded")] = { lookup = "lefthanded", stat = '' },
	};

	skilldata_magic = {
		[Interface.getString("skill_value_arcaneskill")] = { lookup = "arcaneskill", stat = 'arcane', lim=2 },
		[Interface.getString("skill_value_sacredskill")] = { lookup = "sacredskill", stat = 'sacred' },
		[Interface.getString("skill_value_primalskill")] = { lookup = "primalskill", stat = 'primal', lim=3 },
		[Interface.getString("skill_value_psychicskill")] = { lookup = "psychicskill", stat = 'psychic', lim=4 },
		[Interface.getString("skill_value_air")] = { lookup = "air", stat = 'elemental', lim=2 },
		[Interface.getString("skill_value_water")] = { lookup = "water", stat = 'elemental', lim=2 },
		[Interface.getString("skill_value_fire")] = { lookup = "fire", stat = 'elemental', lim=2 },
		[Interface.getString("skill_value_earth")] = { lookup = "earth", stat = 'elemental', lim=2 },
		[Interface.getString("skill_value_mind")] = { lookup = "mental", stat = 'elemental', lim=2 },
		[Interface.getString("skill_value_spirit")] = { lookup = "spirit", stat = 'elemental', lim=2},
		[Interface.getString("skill_value_writing")] = { lookup = "writing", stat = 'intelligence' },
		[Interface.getString("skill_value_alchemy")] = { lookup = "alchemy", stat = 'intelligence' },
		[Interface.getString("skill_value_runic")] = { lookup = "runic", stat = 'intelligence' },
	};

	aptitudes_saves = {
		[Interface.getString("skill_value_insight"):lower()] = "empathic",
		[Interface.getString("skill_value_alert"):lower()] = "alert",
		[Interface.getString("skill_value_reflexes"):lower()] = "fast_reflexes",
		[Interface.getString("skill_value_resistence"):lower()] = "great_fortitude",
		[Interface.getString("skill_value_concentration"):lower()] = "iron_concentration",
		[Interface.getString("skill_value_knowledge_common"):lower()] = "common_sense",
		[Interface.getString("skill_value_dodge"):lower()] = "evasion",
		[Interface.getString("skill_value_courage"):lower()] = "courage",
		[Interface.getString("skill_value_might"):lower()] = "unstopable",
		[Interface.getString("skill_value_acrobatics"):lower()] = "evasion",
	};

	skilldata_magic_nodenames = {
		[Interface.getString("skill_value_arcaneskill")] = "arcane_skill",
		[Interface.getString("skill_value_sacredskill")] = "sacred_skill",
		[Interface.getString("skill_value_primalskill")] = "primal_skill",
		[Interface.getString("skill_value_psychicskill")] = "psychic_skill",
		[Interface.getString("skill_value_air")] = "air_skill",
		[Interface.getString("skill_value_water")] = "water_skill",
		[Interface.getString("skill_value_fire")] = "fire_skill",
		[Interface.getString("skill_value_earth")] = "earth_skill",
		[Interface.getString("skill_value_mind")] = "mind_skill",
		[Interface.getString("skill_value_spirit")] = "spirit_skill",
	};

	DataCommon.shields = {
        Interface.getString("skill_value_shields"),
        "Exotic shields"
    };
	DataCommon.shields = {
        Interface.getString("skill_value_shields"),
        "Exotic shields"
    };

	armor_speed_pen = {
		["no_armor"] = 0,
		[Interface.getString("skill_value_light_armor"):lower()] = 0;
		[Interface.getString("skill_value_medium_armor"):lower()] = 0;
		[Interface.getString("skill_value_heavy_armor"):lower()] = -1;
	};

	armor_sprint_mult = {
		["no_armor"] = 5,
		[Interface.getString("skill_value_light_armor"):lower()] = 4;
		[Interface.getString("skill_value_medium_armor"):lower()] = 3;
		[Interface.getString("skill_value_heavy_armor"):lower()] = 2;
	};

	element_names = {
		Interface.getString("skill_value_air"),
		Interface.getString("skill_value_water"),
		Interface.getString("skill_value_fire"),
		Interface.getString("skill_value_earth"),
		Interface.getString("skill_value_mind"),
		Interface.getString("skill_value_spirit"),
	};

end

mastery_strings = {
	[0] = " [NO MAST]",
	[1] = " [MAST x1/2]",
	[2] = " [MAST]",
	[3] = " [MAST x3/2]",
	[4] = " [MAST x2]",
}

-- Thresholds
critical_success = 9;
critical_miss = 9;
check_grade = 3;

-- Defenses
defense_base = 10;
defense_base = 10;

defense_mult_base = {
	["dodge"] = 4,
	["dodge"] = 4,
	["main_hand"] = 4,
	["side_hand"] = 4,
};
defense_mult_ranged = {
	["dodge"] = 1,
	["dodge"] = 1,
	["unaware"] = 0.5,
	["main_hand"] = 0,
	["side_hand"] = 0,
};

defenses_names = {
	["dodge"] = "dodge",
	["mount"] = "mount",
	["main_hand"] = "main-hand",
	["side_hand"] = "side-hand",
	["unaware"] = "unaware",
	["helpless"] = "helpless"
};


-- Health
vitality_thresholds = {0.85, 0.6, 0.25};
vigor_thresholds = {0.9, 0.7, 0.5};

vitality_states = {
	"Healty",
	"Scratched",
	"Wounded",
	"Severe",
	"Critical",
	"Dying"
};

vitality_colors = {
	"008000",
	"00A300",
	"E6A300",
	"E66900",
	"CC0000",
	"AA0000"
};

vigor_colors = vitality_colors;

vigor_states = {
	"Rested",
	"Heated-up",
	"Tired",
	"Drained",
	"Extenuated",
	"Unconscious"
};

-- Keywords
type_ranged = " (Ranged)";
type_melee = " (Melee)";
type_thrown = " (Thrown)";

ranged_types_short = {[0] = "M", [1] = "R", [2] = "T"};

ranged_weapons = {"Bows", "Crossbows", "Firearms", "Simple"}

weapon_keywords = {
	["two_hands"] = "2 hands",
	["side_hand"] = "side hand",
	["short"] = "short weapon",
	["light"] = "light",
	["double"] = "double weapon",
	["natural"] = "natural weapon",
	["secundary"] = "secondary attack",
	["incorporeal"] = "incorporeal",
	["no_sprint"] = "no sprint",
	["finesse"] = "finesse",
	["no_parry"] = "no parry",
	["unlimited"] = "unlimited parry",
	["touch"] = "touch",
	["electric"] = "electric",
	["gift"] = "gift",
	["large"] = "large",
};

weapon_keywords_number = {
	["hunting"] = "hunting weapon ([+-]?%d+)",
	["monstruosity"] = "hunt monsters ([+-]?%d+)",
	["attack"] = "attack ([+-]?%d+)",
	["crit_attack"] = "critical attack ([+-]?%d+)",
	["threshold"] = "threshold ([+-]?%d+)",
	["defense_ranged"] = "ranged defense ([+-]?%d+)",
	["defense_melee"] = "parry ([+-]?%d+)",
	["defense_mult"] = "parry multiplier ([+-]?%d+)",
	["length"] = "length ([+-]?%d+)",
	["damage_armor"] = "damages armor ([+-]?%d+)",
	["damage_object"] = "damages objects ([+-]?%d+)",
	["min_str"] = "min strength ([+-]?%d+)",
	["unstable"] = "unstable ([+-]?%d+)",
	["piercing"] = "piercing ([+-]?%d+)",
	["lens"] = "lens ([+-]?%d+)",
	["reroll"] = "reroll ([+-]?%d+)",
	["recharge"] = "recharge ([+-]?%d*%.?%d+)",
	["chamber"] = "chamber ([+-]?%d+)",
};

weapon_keywords_desc = {
	["(%d) acciones"] = "Es necesario emplear %s acciones para realizar una accion de Atacar.",
	["alcance (%d+)-?(%d+?)"] = "Determina las distancias minima (%d) y maxima (%d) a la que puede encontrarse el oponente para poder realizar un ataque contra el en combate cuerpo a cuerpo.",
	["arma corta"] = "Un arma corta puede desenvainarse como accion rapida en lugar de una accion normal. Ademas, puede esconderse ente la ropa. Se pueden realizar ataques rapidos. Tambien tiene las propiedades de Ligera y Longitud -2.",
	["arma de caza (%d+)"] = "El personaje obtiene un bonificador al daño contra animales y bestias magicas de %d.",
	["arma doble"] = "Un arma doble debe usarse a dos manos. Ademas, se utiliza como si se tuviera un arma de ese tipo en cada mano: otorga una defensa de parada con la mano secundaria y permite realizar la accion Ataque rapido.",
	["arma natural"] = "No se puede desarmar al personaje de esta arma. No puede ser dañada. En caso de que fuese a perder Integridad por cualquier motivo, el personaje pierde 3 puntos de Vitalidad en su lugar. Atacar a un oponente hecho de un material solido o vistiendo una armadura de un material solido causa daño al personaje igual a la mitad de su dureza (armadura) o Constitucion (personaje). Si el ataque es parado con un arma no natural, el personaje sufre el daño basico del arma.",
	["ataque critico (-?%d+)"] = "El atacante obtiene los efectos de la Regla del 16 con un resultado natural de 16 - %d en sus pruebas de ataque. Esta propiedad se ve contrarrestada por la propiedad Defensa critica del defensor.",
	["ataque secundario"] = "Las armas de mayor envergadura no pueden emplearse a corta distancia. Cuando un enemigo se encuentre adyacente al personaje, este debe emplear el daño del ataque secundario en lugar del daño normal.",
	["aturdir"] = "El arma puede usarse para aturdir. Antes de realizar la prueba de ataque, el personaje puede escoger entre causar daño normal, daño no letal sin sufrir desventaja en la prueba de ataque, o realizar una maniobra de aturdir sin sufrir un ataque de oportunidad. Si escoge realizar la maniobra, realizara la prueba de ataque del arma, no la descrita en la maniobra.",
	["apresar"] = "Antes de realizar la prueba de ataque, el personaje puede escoger entre causar daño fisico o realizar una maniobra de Atrapar sin sufrir un ataque de oportunidad. Si escoge realizar la maniobra, realizara la prueba de ataque del arma, no la descrita en la maniobra.",
	["carga"] = "Debe emplearse durante una carga. ",
	["carga de caballeria"] = "Debe emplearse durante una carga. El personaje debe ir montado.",
	["dos manos"] = "Esta arma requiere usar ambas manos.",
	["defensa contra proyectiles (-?%d+)"] = "El arma puede ser empleada para detener proyectiles. Ademas, otorga un modificador a dicha Defensa de %d.",
	["rompedor"] = "Si el ataque del arma es parado, trata el arma usada para parar como si tuviera el rasgo quebradizo. Si el ataque es un exito, la armadura del oponente recibe 1 punto de daño de integridad modificado por una prueba de salvacion de Dureza (dificultad 9 + daño sufrido).",
	["desarmar"] = "Antes de realizar la prueba de ataque, el personaje puede escoger entre causar daño fisico o realizar una maniobra de Desarmar. Si escoge realizar la maniobra, realizara la prueba de ataque del arma, no la descrita en la maniobra.",
	["desmontar"] = "Antes de realizar la prueba de ataque, el personaje puede escoger entre causar daño fisico o realizar una maniobra de Desmontar. Si escoge realizar la maniobra, realizara la prueba de ataque del arma, no la descrita en la maniobra.",
	["disparo cuerpo a cuerpo"] = "Disparar el arma en combate cuerpo a cuerpo no provoca ataques de oportunidad (pero si al recargarlas).",
	["disparo multiple"] = "El personaje puede realizar dos ataques al mismo objetivo con esta arma empleando una unica accion. Cada ataque tiene un penalizador de -3.",
	["fuerza minima (d+)"] = "Si la fuerza del personaje es inferior a %d, no podra atacar con dicha arma. Ademas, si no se trata de un arma de proyectiles, el personaje nunca podra usar la Destreza para atacar, independientemente de las Aptitudes que tenga.",
	["incorporea"] = "El arma solo puede parar o ser parada por otras armas incorporeas o magicas, y gana Penetracion igual al numero de dados de daño. El daño del arma solo puede ser reducido por armaduras magicas.",
	["atascarse (d+)"] = "El arma es propensa a no funcionar. Si el resultado de los dados en la prueba de ataque ocurre en los %d valores siguientes a los de la Regla del 2, el arma queda atascada (no podra usarse para atacar). Para desatascarla el personaje debe emplear tantas acciones como el valor de Recarga del arma. ",
	["lentes de aumento (%d+)"] = "El arma de proyectiles tiene dos lentes que le permiten disparar muy lejos con una precision sorprendente. Reduce en X el numero de rangos de alcance al que se encuentra el objetivo para calcular los penalizadores de distancia. Sin embargo, obtiene una desventaja por cada rango de alcance mas proximo que %d.",
	["ligera"] = "Un arma ligera es apropiada para utilizarla en la mano torpe, junto con otra arma en la mano principal. Si se usa esta arma en la mano torpe, no impone penalizadores a las tiradas de ataque y a la defensa. Ademas, puede desenvainarse con una accion rapida. Tambien tiene Longitud -1.",
	["longitud (%d+)"] = "El arma otorga un modificador de %d a las pruebas de ataque y a la Defensa de parada realizados con dicha arma.",
	["no puede esprintar"] = "El personaje no puede esprintar mientras porte el objeto.",
	["penetracion (-?%d+)"] = "El arma reduce la Proteccion del defensor en %d. Esta propiedad se ve contrarrestada por la propiedad de Resistencia de la armadura del defensor.",
	["precisa"] = "El personaje puede emplear la Destreza en lugar de la Fuerza para las pruebas de ataque y la Defensa con dicha arma.",
	["proteccion contra cargas"] = "Cuando un oponente carga contra el personaje, este puede emplear su reaccion para llevar a cabo un Ataque de oportunidad contra el si no emplea un arma con mayor alcance. Si causa daño al oponente en dicho ataque de oportunidad, el oponente pierde su ataque de carga. ",
	["provoca ataques de oportunidad"] = "Atacar con esta arma provoca ataques de oportunidad. Ademas, no se pueden utilizar ataques de oportunidad con esta arma.",
	["quebradizo"] = "Si esta arma para o es parada por otra arma de mayor dureza, sufre un punto de daño de integridad modificado por una prueba de salvacion de Dureza (dificultad dureza pasiva del otro arma). Ocurre lo mismo cuando el arma golpea a un oponente con una armadura de mayor dureza que el arma, o el defensor posee el rasgo Metalico o Solido.",
	["recamara (%d+)"] = "El arma puede ser cargada con hasta %d proyectiles.",
	["recarga (-?%d+)"] = "El arma necesita X acciones para recargar un proyectil.",
	["romper objeto (-?%d+)"] = "Aumenta la dificultad de las pruebas de salvacion de Dureza relacionadas con la maniobra Romper objeto en %d al emplear esta arma.",
	["sin parada"] = "El arma no sirve para realizar paradas, por lo que empuñarla no otorga la Defensa de parada correspondiente.",
	["voluminosa"] = "Realiza la prueba de ataque en desventaja si se emplea en espacios angostos o desde detras de cobertura. Si se trata de un arma de proyectiles, tambien obtiene desventaja en la prueba de ataque al disparar sobre un objetivo a menos de la mitad de su rango de alcance.",
};

armor_keywords_number = {
	["resistence"] = "resistence ([+-]?%d+)",
};
armor_keywords = {
	["helmet"] = "no helmet",
	["noisy"] = "noisy",
	["metallic"] = "metallic",
	["ether"] = "sin bloqueo de eter",
};
armor_keywords_desc = {
	["sin casco"] = "Esta armadura no limita la Percepción ni la Magia psíquica del personaje.",
	["defensa critica (%d+)"] = "El atacante obtiene los efectos de la Regla del 2 con un resultado natural de 2 + %d en sus pruebas de ataque. Esta propiedad se ve contrarrestada por la propiedad Ataque critico del atacante.",
	["metalica"] = "Algunos ataques son mas faciles de realizar contra enemigos que portan armaduras metalicas (o que estan hechos de metal, como un golem de hierro).",
	["proteccion ambiental"] = "Protege de los rigores del clima, de manera que los hace soportables.",
	["quebradizo"] = "Si el personaje recibe un golpe de un arma de mayor Dureza que la armadura, esta ultima recibe un punto de daño de integridad modificado por una prueba de salvacion de Dureza (dificultad 9 + daño sufrido).",
	["resistencia (-?%d+)"] = "Reduce la Penetracion de las ramas en un valor igual a la resistencia de la armadura.",
	["ruidosa"] = "Si se porta una armadura ruidosa, todas las tiradas de Sigilo se realizaran en desventaja.",
	["solida"] = "La armadura es de un material solido, y puede hacer que las armas con la propiedad Quebradizo se rompan al impactar contra ella. ",
	["sin bloqueo de eter"] = "La no bloquea el eter y, por lo tanto, no reduce los Atributos de Don del canalizador. ",
};
armor_clothes = "clothes"
armor_bracelet = "bracelet"
armor_summoned = "summoned armor";
weapon_summoned = "summoned weapon";

save = "save";
all = "all";
any = "any";
any = "any";
base = "base";
metallic = "metallic";

-- Magic
magic_min = {-999, -5, -2, 1, 4, 7, 12, 15, 18, 21, 24};
magic_max = {-6, -3, 0, 3, 6, 11, 14, 17, 20, 23, 999};
magic_increased_effects = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};
magic_increased_misscast = {-10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10};
magic_names = {"Nonexistent", "Frozen", "Weak", "Calm", "Tranquil", "Normal", "Agitated", "Powerful", "Tumultuous", "Swirl", "Maelstrom"};
misscast_table_name = {'Misscast ', "Light Misscast", "Medium Misscast", "Severe Misscast"};
max_spell_level = 11;

dispell_dice_cat_mult = 4;

dispell_dice_cat_mult = 4;

fatal_magic = {
	[1] = {text = "", damage = {"d4"}, damagestat={}, mentaltemp={}, mentalperm={}},
	[2] = {text = "", damage = {"d8"}, damagestat={}, mentaltemp={"1d1"}, mentalperm={}},
	[3] = {text = "", damage = {"d12"}, damagestat={"1d1"}, mentaltemp={"1d1"}, mentalperm={"1d1"}},
	[4] = {text = "", damage = {"d20"}, damagestat={"1d2"}, mentaltemp={"1d2"}, mentalperm={"1d1"}},
	[5] = {text = "", damage = {"d30"}, damagestat={"1d3"}, mentaltemp={"1d2"}, mentalperm={"1d2"}},
	[6] = {text = "", damage = {"d40"}, damagestat={"1d4"}, mentaltemp={"1d3"}, mentalperm={"1d2"}},
	[7] = {text = "", damage = {"d60"}, damagestat={"1d5"}, mentaltemp={"1d3"}, mentalperm={"1d3"}},
	[8] = {text = "fatal_magic_8", damage = {}, damagestat={}, mentaltemp={}, mentalperm={}},
};

misscast_levels = {
	[0] = {indices = {2, 99}, [2]={"Disfuncion menor"}, [99]={}}, 
	[1] = {indices = {5, 98}, [5]={"Disfuncion menor"}, [98]={}}, 
	[2] = {indices = {1, 9, 97}, [1]={"Disfuncion menor", "Disfuncion menor"}, [9]={"Disfuncion menor"}, [97]={}}, 
	[3] = {indices = {2, 13, 96}, [2]={"Disfuncion menor", "Disfuncion menor"}, [13]={"Disfuncion menor"}, [96]={}}, 
	[4] = {indices = {2, 5, 19, 95}, [2]={"Disfuncion intermedia"}, [5]={"Disfuncion menor", "Disfuncion menor"}, [19]={"Disfuncion menor"}, [95]={}}, 
	[5] = {indices = {4, 8, 25, 94}, [4]={"Disfuncion intermedia"}, [8]={"Disfuncion menor", "Disfuncion menor"}, [25]={"Disfuncion menor"}, [94]={}}, 
	[6] = {indices = {1, 7, 12, 32, 93}, [1]={"Disfuncion intermedia", "Disfuncion menor"}, [7]={"Disfuncion intermedia"}, [12]={"Disfuncion menor", "Disfuncion menor"}, [32]={"Disfuncion menor"}, [93]={}}, 
	[7] = {indices = {1, 3, 11, 17, 40, 92}, [1]={"Disfuncion severa"}, [3]={"Disfuncion intermedia", "Disfuncion menor"}, [11]={"Disfuncion intermedia"}, [17]={"Disfuncion menor", "Disfuncion menor"}, [40]={"Disfuncion menor"}, [92]={}}, 
	[8] = {indices = {1, 3, 6, 16, 23, 49, 91}, [1]={"Disfuncion intermedia", "Disfuncion intermedia"}, [3]={"Disfuncion severa"}, [6]={"Disfuncion intermedia", "Disfuncion menor"}, [16]={"Disfuncion intermedia"}, [23]={"Disfuncion menor", "Disfuncion menor"}, [49]={"Disfuncion menor"}, [91]={}}, 
	[9] = {indices = {1, 3, 6, 10, 19, 27, 56, 90}, [1]={"Disfuncion severa", "Disfuncion menor"}, [3]={"Disfuncion intermedia", "Disfuncion intermedia"}, [6]={"Disfuncion severa"}, [10]={"Disfuncion intermedia", "Disfuncion menor"}, [19]={"Disfuncion intermedia"}, [27]={"Disfuncion menor", "Disfuncion menor"}, [56]={"Disfuncion menor"}, [90]={}}, 
	[10] = {indices = {1, 3, 6, 10, 15, 29, 38, 68, 89}, [1]={"Disfuncion severa", "Disfuncion intermedia"}, [3]={"Disfuncion severa", "Disfuncion menor"}, [6]={"Disfuncion intermedia", "Disfuncion intermedia"}, [10]={"Disfuncion severa"}, [15]={"Disfuncion intermedia", "Disfuncion menor"}, [29]={"Disfuncion intermedia"}, [38]={"Disfuncion menor", "Disfuncion menor"}, [68]={"Disfuncion menor"}, [89]={}}, 
	[11] = {indices = {1, 3, 6, 10, 15, 21, 36, 46, 76, 88}, [1]={"Disfuncion severa", "Disfuncion severa"}, [3]={"Disfuncion severa", "Disfuncion intermedia"}, [6]={"Disfuncion severa", "Disfuncion menor"}, [10]={"Disfuncion intermedia", "Disfuncion intermedia"}, [15]={"Disfuncion severa"}, [21]={"Disfuncion intermedia", "Disfuncion menor"}, [36]={"Disfuncion intermedia"}, [46]={"Disfuncion menor", "Disfuncion menor"}, [76]={"Disfuncion menor"}, [88]={}},  
	[12] = {indices = {2, 5, 9, 14, 20, 27, 42, 52, 82, 87}, [2]={"Disfuncion severa", "Disfuncion severa"}, [5]={"Disfuncion severa", "Disfuncion intermedia"}, [9]={"Disfuncion severa", "Disfuncion menor"}, [14]={"Disfuncion intermedia", "Disfuncion intermedia"}, [20]={"Disfuncion severa"}, [27]={"Disfuncion intermedia", "Disfuncion menor"}, [42]={"Disfuncion intermedia"}, [52]={"Disfuncion menor", "Disfuncion menor"}, [82]={"Disfuncion menor"}, [87]={}}, 
};
misscast_max = 12;

magic_categories = {
	"magic_category_weak",
	"magic_category_medium",
	"magic_category_strong",
};
magic_categoriy_default = "magic_category_weak";

spell_level_0_cost = {
	["magic_category_weak"] = 3,
	["magic_category_medium"] = 2,
	["magic_category_strong"] = 1,
};
spell_level_cost = {
	["magic_category_weak"] = 4,
	["magic_category_medium"] = 3,
	["magic_category_strong"] = 2,
};
initial_magic_stat = {
	["magic_category_weak"] = 3,
	["magic_category_medium"] = 4,
	["magic_category_strong"] = 5,
};
max_spell_mult = {
	[""] = 1/3,
	["magic_category_weak"] = 1/3,
	["magic_category_medium"] = 1/2,
	["magic_category_strong"] = 1,
};
stat_emission = {
	[1] = 1,
	[2] = 3,
	[3] = 6,
	[4] = 10,
	[5] = 15,
	[6] = 21,
	[7] = 28,
	[8] = 36,
	[9] = 45,
	[10] = 55,
};
stat_multiplier = {
	[0] = 1,
	[1] = 1,
	[2] = 1,
	[3] = 2,
	[4] = 2,
	[5] = 2,
	[6] = 3,
	[7] = 3,
	[8] = 3,
	[9] = 4,
	[10] = 4,
	[11] = 4,
	[12] = 4,
};

summon_label = "summon";
number_label = "number";

damage_names = {
	["heal"] = "Vitality healing",
	["recover"] = "Vigor healing",
	["tempvit"] = "Temporal vitality",
	["tempvigor"] = "Temporal vigour",
	["stat"] = "Stat healing",
	["renewal"] = "Essence healing",
	["madness_temp"] = "Temp. madness healing",
	["spirit_temp"] = "Temp. spirit healing",
	["madness_perm"] = "Perm. madness healing",
	["spirit_perm"] = "Temp. spirit healing",
	["damage"] = "Damage",
};

magic_components = {
	["words"] = "Words",
	["gestures"] = "Gestures",
	["material"] = "Material",
	["vision"] = "Vision",
	["concentration"] = "Concentration",
	["gem"] = "Gem",
};
magic_components_short = {
	["words"] = "W",
	["gestures"] = "G",
	["material"] = "M",
	["vision"] = "V",
	["concentration"] = "C",
	["gem"] = "G",
};
magic_materials = "Magic material";
magic_gems = {
	[0] = "Tiny gem",
	[1] = "Tiny gem",
	[2] = "Tiny gem",
	[3] = "Small gem",
	[4] = "Small gem",
	[5] = "Small gem",
	[6] = "Medium gem",
	[7] = "Medium gem",
	[8] = "Medium gem",
	[9] = "Large gem",
	[10] = "Large gem",
};
standard = "Standard";
magic_standard_actions = {
	[0] = {"action"},
	[1] = {"action"},
	[2] = {"action"},
	[3] = {"action", "action"},
	[4] = {"action", "action"},
	[5] = {"action", "action"},
	[6] = {"action", "action", "action"},
	[7] = {"action", "action", "action"},
	[8] = {"action", "action", "action"},
	[9] = {"action", "action", "action", "action"},
	[10] = {"action", "action", "action", "action"},
};
magic_standard_actions_ritual = {
	[0] = "15 minutes",
	[1] = "15 minutes",
	[2] = "15 minutes",
	[3] = "30 minutes",
	[4] = "30 minutes",
	[5] = "30 minutes",
	[6] = "1 hour",
	[7] = "1 hour",
	[8] = "1 hour",
	[9] = "2 hours",
	[10] = "2 hours",
};
magic_extra_actions_ritual = 15;
magic_keywords = {
	["tiring"] = "tiring",
	["demanding"] = "demanding",
	["blood_magic"] = "blood magic",
	["hidden"] = "hidden",
	["electric"] = "electric",
	["major_heal"] = "major healing",
	["minor_heal"] = "minor healing",
	["style"] = "style",
};
magic_keywords_number = {
	["unstable"] = "unstable ([+-]?%d+)"
};
magic_keywords_desc = {
	["adivinacion"] = "Estos conjuros se usan para obtener informacion mediante la Magia. Dependiendo de sus efectos, los conjuros con el rasgo Adivinacion se pueden considerar opuestos a algunos del talento de Ilusion. Adicionalmente, un conjuro de Adivinacion con el rasgo de Visual o Auditivo puede emplearse para dispersar otro conjuro del talento de Adivinacion con los mismos rasgos, o viceversa.",
	["agotador"] = "Mientras mantenga el conjuro, el personaje debe volver a pagar el coste en Vigor del conjuro al principio de cada uno de sus turnos o el conjuro desaparecera.",
	["auditivo"] = "El conjuro tiene una componente sonora imprescindible. Los personajes sin el sentido del oido (por ejemplo, por estar ensordecido) son inmunes a los efectos del conjuro.",
	["aumento"] = "Estos conjuros mejoran las caracteristicas de sus objetivos. Si dos efectos de Aumento mejoran la misma caracteristica de un personaje no se suman, sino que se emplea el mayor valor. Adicionalmente, si un conjuro de Aumento afecta a un personaje sujeto a uno o mas conjuros de Debilitamiento que afecten a la misma caracteristica, estos conjuros se consideraran opuestos.",
	["aura"] = "Un conjuro de Aura debe ser realizado sobre un personaje u objeto que sera el portador del Aura. A partir de ese momento, el Aura afectara a todos los objetivos dentro del area de alcance especificada en la descripcion del conjuro, ademas de al propio portador.  Un personaje unicamente puede portar una misma Aura a la vez. En caso de realizarse un conjuro de Aura sobre un personaje que ya porta una, el nuevo conjuro sustituira al antiguo. ",
	["bloqueo de esencia"] = "La Esencia de este conjuro no se puede recuperar hasta que el conjuro finaliza. En caso que se trate de un conjuro de duracion Instantanea, la Esencia no se puede recuperar hasta que finalicen los efectos del conjuro.",
	["calor"] = "Estos conjuros pueden emplearse para prender fuego a objetos Inflamables. Los conjuros con el rasgo Calor son opuestos a los conjuros con el rasgo Frio.",
	["debilitamiento"] = "Este conjuro reduce las caracteristicas de los objetivos. Si dos efectos de Debilitamiento afectan al mismo rasgo, solo se aplica el del penalizador mas alto en lugar de sumarlos. Adicionalmente, si un conjuro de Debilitamiento afecta a un personaje sujeto a uno o mas conjuros de Aumento que afecten a la misma caracteristica, estos conjuros seran considerados como opuestos.",
	["dimensional"] = "El conjuro hace que el objetivo se teletransporte o que cambie de Reino. Este rasgo es comun de los conjuros del Talento de Espacio.",
	["electrico"] = "El conjuro se basa en la electricidad. Las pruebas de ataque realizadas contra objetivos con el rasgo Metalico se consideran pruebas de ataque de toque. Las pruebas de salvacion llevadas a cabo por personajes con el rasgo Metalico deben llevarse a cabo en desventaja. Ademas, los efectos de este conjuro pueden verse afectados por grandes cantidades de metal o agua. El daño grave causado por estos conjuros es Paralisis.",
	["emocional"] = "El conjuro afecta a las emociones del personaje. Algunos personajes (constructos, no muertos) son inmunes a estos efectos.",
	["espiritual"] = "Estos conjuros afectan directamente al Espiritu de sus objetivos. Los personajes sin Espiritu (Espiritu 0) son inmunes a estos conjuros.",
	["exigente"] = "El canalizador siempre debe realizar la prueba magica para realizar un conjuro exigente. Si la falla, debe pagar el coste de Vigor y Esencia del conjuro como si hubiera tenido exito (ademas de otras posibles complicaciones).",
	["fuerza"] = "Cuando un conjuro de fuerza se enfrenta a otro (por ejemplo, un conjuro de Proyectil magico contra otro de Escudarse), se consideran conjuros opuestos.",
	["frio"] = "Un conjuro de frio puede emplearse para congelar liquidos. Por cada punto de daño causado, pueden congelar hasta 100 ml de liquido. Ademas, el daño grave causado por estos conjuros es Congelar. Los conjuros con el rasgo Frio son opuestos a aquellos con el rasgo Calor.",
	["inestable (-?%d+)"] = "El conjuro es mas caotico de lo normal, por lo que aumenta en %d su nivel a la hora de determinar en que tabla lanzar para comprobar si el canalizador sufre una disfuncion magica.",
	["intrincado"] = "El conjuro emplea mas hilos magicos de lo habitual, por lo que interfiere mucho mas con el resto de sentidos. Este conjuro bloquea los sentidos magicos mas alla de su area de efecto.",
	["invocacion"] = "El conjuro invoca a un objeto o personaje de otro mundo u otro Reino. Una vez que el conjuro termina, el objeto o personaje invocado regresa a alli donde se encontraba sin dejar nada atras. Ocurre lo mismo si el objeto es destruido o el personaje muere.",
	["linguistico"] = "Estos conjuros requieren transmitir informacion entre el objetivo y el canalizador. Si el canalizador emplea un dialecto que no conoce el objetivo, pero si conoce otro de la misma familia, realiza cualquier prueba de salvacion en ventaja, o el canalizador realiza cualquier prueba magica en desventaja. Si el dialecto empleado por el canalizador no es comprendido en absoluto por el objetivo, el conjuro no le afectara.",
	["luz"] = "Este conjuro crea luz. Por lo tanto, es opuesto a aquellos con el rasgo Oscuridad.",
	["magia de sangre"] = "El conjuro gana el rasgo Inestable 1 y no tiene coste de Vigor. En su lugar, el personaje debera hacerse un corte, perdiendo una cantidad de Vitalidad igual al nivel del conjuro (minimo 1). Si el coste de Vigor aumenta debido al uso de tecnicas magicas, el coste de Vitalidad del conjuro aumenta en la mitad de dicho incremento (redondeando hacia arriba). La Vitalidad temporal no puede utilizarse para pagar este coste, y los personajes sin sangre (como un liche o un elemental) no pueden realizar estos conjuros. Los conjuros de Magia de sangre son mas dificiles de dispersar, por lo que todas las pruebas para desatarlos o dispersarlos deben realizarse en desventaja.",
	["magia runica"] = "A la hora de realizar este conjuro, debe realizarse una prueba e Inteligencia + Magia runica en lugar de la prueba magica habitual.",
	["maldicion"] = "Los efectos de este conjuro permanecen en el tiempo pese a que el canalizador no mantenga el conjuro o lo ate. La dificultad para dispersarlo es Atributo de Don + Habilidad magica del canalizador + 10 + 3 x (Nivel del conjuro de maldicion). Si se intenta dispersar la Maldicion sin emplear un conjuro especifico para dispersar maldiciones, el canalizador del conjuro dispersivo debe realizar la prueba magica en desventaja. Ademas, si los efectos de un ritual de maldicion tienen una duracion modificada mas larga (ver seccion Duracion de un conjuro)..",
	["mental"] = "Estos conjuros afectan a la Mente de sus objetivos. Los personajes sin Mente (Inteligencia, Voluntad, Carisma o Mente 0) son inmunes a estos conjuros.",
	["oculto"] = "Los conjuros ocultos presentan unas emanaciones magicas muy reducidas una vez realizado, lo que evita que otro canalizador lo detecte instantaneamente. Esto tambien dificulta enormemente su manipulacion, por lo que cualquier prueba magica llevada a cabo para afectar al conjuro (como atarlo o desatarlo) se debe realizar en desventaja. Esta propiedad esta muy extendida entre los conjuros del talento de la Ilusion.",
	["orden"] = "Los conjuros de orden suelen ser menos aleatorios que los demas. Ademas, todos los conjuros de orden se oponen a los conjuros del Talento del Caos, y viceversa.",
	["orden"] = "Los conjuros de orden suelen ser menos aleatorios que los demas. Ademas, todos los conjuros de orden se oponen a los conjuros del Talento del Caos, y viceversa.",
	["oscuridad"] = "Este conjuro crea oscuridad, por lo que es opuesto a los conjuros con el rasgo Luz. Ademas, puede utilizarse para dispersar los efectos de un conjuro con el rasgo Visual.",
	["resurreccion"] = "Este conjuro solo afecta a un personaje con el estado Muerto. Si el personaje no desea volver a la vida o su Espiritu ha sido destruido, el conjuro falla.",
	["runa temporal"] = "Este conjuro crea una runa temporal sobre el objetivo del conjuro. Una vez que el conjuro termine, la runa desaparecera.",
	["sanacion mayor"] = "Los conjuros de Sanacion mayor emplean la energia del eter para sanar a sus objetivos, por lo que requieren que este gaste Esencia.",
	["sanacion menor"] = "Los de Sanacion menor emplean la energia del objetivo, por lo que imponen un penalizador de -1 por cansancio.",
	["silencio"] = "Este conjuro crea silencio, por lo que es opuesto a los conjuros con el rasgo Sonico. Ademas, puede emplearse para dispersar cualquier conjuro con el rasgo Auditivo",
	["sonico"] = "Este conjuro crea sonido, por lo que es opuesto a los conjuros con el rasgo Silencio.",
	["telequinesis"] = "Dos conjuros con el rasgo telequinesis se consideran opuestos si esten afectando a un mismo objetivo.",
	["transformacion"] = "Estos conjuros transforman objetos o personajes. Dos conjuros de Transformacion que afecten a la misma caracteristica de un mismo objetivo se consideran opuestos. En caso de que un personaje sea transformado en otro diferente, comienza con su Vitalidad intacta. Si su Vigor o Vitalidad se ve reducidos a 0, el conjuro finaliza. Una vez el conjuro finaliza, recupera su Vitalidad original. Sin embargo, si el conjuro finaliza debido a que el personaje transformado perdio toda su Vitalidad, el personaje original sufre daño inevitable de un dado de categoria 2 x Nivel del conjuro.",
	["visual"] = "El conjuro tiene una componente visual imprescindible. Por lo tanto, aquellos personajes que no posean el don de la vista (como un personaje cegado) o no este mirando en la direccion del conjuro, es inmune a dicho conjuro. En caso de que haya algun tipo de impedimento en la linea de vision, pero que no lo oculte por completo, el personaje recibira ventaja en cualquier prueba de salvacion que deba realizar, y el canalizador recibira desventaja en sus pruebas magicas.",
};

-- Items
shield_types = {
	"shields",
	"exotic shields"
};
natural_materials = {
	"leather",
	"wood",
	"cloth",
};
armor_mastery_limit = {
	[0] = -2,
	[1] = -1,
	[2] = 0,
	[3] = 0,
	[4] = 1,
};
armor_mastery_init = {
	[0] = -2,
	[1] = -1,
	[2] = 0,
	[3] = 0,
	[4] = 1,
};
armor_mastery_sprint = {
	[0] = -2,
	[1] = -1,
	[2] = 0,
	[3] = 0,
	[4] = 1,
};
armor_mastery_critdefense = {
	[0] = -1,
	[1] = 0,
	[2] = 0,
	[3] = 1,
	[4] = 2,
};
armor_mastery_mindamage = {
	[-1] = .5,
	[0] = .75,
	[1] = .6666667,
	[2] = .5,
	[3] = .33333334,
	[4] = .25,
};

initial_items = {
	{name = "Odre de agua", count = 1},
	{name = "Raciones de viaje", count = 5},
	{name = "Equipo de supervivencia", count = 1},
	{name = "Ropa normal", count = 1},
	{name = "Cuchillo", count = 1},
	{name = "Antorcha", count = 1},
	{name = "Bolsa de dinero", count = 1},
};

coins = {"platinum", "gold", "electrum", "silver", "copper"};
coins_short = {"pc", "gc", "ec", "sc", "cc"};

rarity_min = 1;
rarity_max = 7;
rarities = {"item_rarity_common", "item_rarity_usual", "item_rarity_uncommon", "item_rarity_scarse", "item_rarity_rare", "item_rarity_inexistent", "item_rarity_mythic"};
spell_rarity = {
	[""] = 0,
	["spell_common"] = 0,
	["spell_rare"] = 2,
	["spell_lost"] = 4,
};
rarity = {
	[""] = 1,
	["item_rarity_common"] = 1,
	["item_rarity_usual"] = 2,
	["item_rarity_uncommon"] = 3,
	["item_rarity_scarse"] = 4,
	["item_rarity_rare"] = 5,
	["item_rarity_inexistent"] = 6,
	["item_rarity_mythic"] = 7,
};
rarity_inv = {
	[1] = "item_rarity_common",
	[2] = "item_rarity_usual",
	[3] = "item_rarity_uncommon",
	[4] = "item_rarity_scarse",
	[5] = "item_rarity_rare",
	[6] = "item_rarity_inexistent",
	[7] = "item_rarity_mythic",
};
rarity_quality = {
	[1] = 0,
	[2] = 1,
	[3] = 2,
	[4] = 2,
	[5] = 3,
};

rune_space_per_level = {
	[0] = 1,
	[1] = 1,
	[2] = 1,
	[3] = 2,
	[4] = 2,
	[5] = 2,
	[6] = 3,
	[7] = 3,
	[8] = 3,
	[9] = 4,
	[10] = 4,
	[11] = 5,
	[12] = 5,
};
min_runic_rarity = {
	[0] = 3,
	[1] = 3,
	[2] = 3,
	[3] = 4,
	[4] = 4,
	[5] = 4,
	[6] = 5,
	[7] = 5,
	[8] = 5,
	[9] = 6,
	[10] = 6,
	[11] = 7,
	[12] = 7,
};
rune_price = {
	[0] = "2 gc",
	[1] = "3 gc",
	[2] = "5 gc",
	[3] = "10 gc",
	[4] = "20 gc",
	[5] = "30 gc",
	[6] = "50 gc",
	[7] = "70 gc",
	[8] = "90 gc",
	[9] = "150 gc",
	[10] = "200 gc",
	[11] = "275 gc",
	[12] = "400 gc",
};
rune_keywords = {
	-- ["essence"] = "essence block (%d+)",
	-- ["space"] = "runes space (%d+)",
	["space_level"] = "runes space per level",
	["cheap"] = "cheap rune",
	["multiple"] = "multiple rune",
	["cursed"] = "cursed rune",
	["rare"] = "rare rune",
	["mythic"] = "mythic rune",
	["secret"] = "secret rune",
};
rune_keywords_desc = {
	["efecto oculto"] = "Los efectos de esta runa tienen una emanacion magica muy debil, por lo que su uso normalmente no sera detectado por los sentidos magicos de los canalizadores, similar a los conjuros con el rasgo Oculto.",
	["espacio de runas por nivel"] = ": Esta runa es similar a Espacio de runas X, pero X depende del nivel de la runa: 1 para runas de nivel 0 a 2, 2 para runas de nivel 3 a 5, 3 para runas de nivel 6 a 8, y 4 para runas de nivel 9 o mas.",
	["runa barata"] = "",
	["runa barata"] = "El precio de esta runa es un tercio del precio normal. Reduce la rareza minima a Usual.",
	["runa celosa"] = "No se pueden inscribir dos runas celosas en un mismo objeto. No se pueden vincular dos objetos con la misma runa celosa.",
	["runa enganosa"] = "Es facil confundir esta runa con otra (ver Identificar runas).",
	["runa limitada (%d+)"] = "Un personaje no puede inscribir mas de %d runas limitadas. Si ya ha inscrito todas las posibles, alguna de ellas debe destruirse o hacerse perder su poder antes de inscribir una nueva.",
	["runa maldita"] = "Una vez vinculado, el objeto no puede desvincularse ni retirarse voluntariamente (ver vincular objetos runicos malditos). Esta runa tambien tiene el rasgo Runa engañosa. Esta runa reduce el precio de venta del objeto en lugar de aumentarlo (cuando el comerciante es honesto).",
	["runa mitica"] = "Aprender a grabar estas runas requiere ser Maestro en Magia runica. Multiplica por 5 el precio de la runa y aumenta la rareza del objeto en 2.",
	["runa multiple"] = "Se pueden grabar varias runas de este tipo en el mismo objeto, pero se debe escoger una opcion diferente (hechizo diferente, tipo de daño diferente, atributo diferente, etc.). Solo se aplica el mayor Bloqueo de esencia y Espacio de runas de ellas. Divide a la mitad el precio de todas las runas multiples salvo la mas cara.",
	["runa oculta"] = "La runa es invisible salvo que se usen sentidos magicos aumentados. ",
	["runa personal"] = "Esta runa solo puede inscribirse sobre si mismo. ",
	["runa rara"] = "Aprender a grabar estas runas requiere ser Experto o Maestro en Magia runica. Duplica el precio de la runa y aumenta en 1 la rareza del objeto runico.",
	["runa ritual"] = "Un canalizador puede realizar un ritual del nivel de esta runa. Si lo hace, graba una version temporal de esta runa. La runa temporal desaparece tras el primer descanso largo del canalizador que la inscribe.",
	["runa secreta"] = "Aprender a grabar estas runas requiere una Aptitud especial. ",
};
runes = {
	["wand"] = "magic wand",
	["staff"] = "magic staff",
};
rune_prize_mod = {
	["cheap"] = 1/3,
	["multiple"] = 0.5,
	["rare"] = 2,
	["mythic"] = 5,
	["secret"] = 4,
	-- [0] = 1,
	-- [2] = 2,
	-- [4] = 5,
};
rune_rarity_mod = {
	["cheap"] = -1,
	["multiple"] = 0,
	["rare"] = 1,
	["mythic"] = 2,
	["secret"] = 1,
};
extra_runes_prize_mult = {
	[0] = 1;
	[1] = 1.25;
	[2] = 1.5;
	[3] = 2;
	[4] = 2.5;
	[5] = 3.5;
	[6] = 4.5;
	[7] = 6;
	[8] = 7.5;
};

scroll = "scroll";
potion = "potion";
wand = "wand";
staff = "staff";
forge_charges = {
	["scroll"] = 1,
	["potion"] = 1,
	["wand"] = 5,
	["staff"] = 2,
};
forge_recharge = {
	["scroll"] = "",
	["potion"] = "",
	["wand"] = "item_power_type_charges",
	["staff"] = "item_power_type_rec_charges",
};

prize_scroll = {
	[0] = "5 sc";
	[1] = "1 ec";
	[2] = "2 ec";
	[3] = "4 ec";
	[4] = "6 ec";
	[5] = "8 ec";
	[6] = "1 gc 5 ec";
	[7] = "2 gc";
	[8] = "2 gc 5 ec";
	[9] = "5 gc";
	[10] = "8 gc";
};
prize_potion = {
	[0] = "5 sc";
	[1] = "1 ec";
	[2] = "2 ec";
	[3] = "3 ec";
	[4] = "4 ec";
	[5] = "5 ec";
	[6] = "8 ec";
	[7] = "1 gc";
	[8] = "1 gc 2 ec";
	[9] = "1 gc 5 ec";
	[10] = "2 gc";
};
prize_rare_mod = {
	["scroll"] = 5;
	["potion"] = 2,
};
prize_mythic_mod = {
	["scroll"] = 25;
	["potion"] = 5,
};
rarity_cons = {
	[0] = 2;
	[1] = 2;
	[2] = 2;
	[3] = 3;
	[4] = 3;
	[5] = 3;
	[6] = 4;
	[7] = 4;
	[8] = 4;
	[9] = 5;
	[10] = 5;
};
rarity_rare_mod = 1;
rarity_mythic_mod = 3;
weight_cons = {
	[0] = 0.15;
	[1] = 0.15;
	[2] = 0.15;
	[3] = 0.2;
	[4] = 0.2;
	[5] = 0.2;
	[6] = 0.25;
	[7] = 0.25;
	[8] = 0.25;
	[9] = 0.3;
	[10] = 0.3;
};
initial_prize = {
	["wand"] = "2 sc",
	["staff"] = "5 sc"
};
base_prize_multiplier = {
	[1] = 1,
	[2] = 5,
	[3] = 15,
	[4] = 30,
	[5] = 50,
}

-- Rest
no_rest_char_types = {
	"construct",
	"elemental",
	"ooze",
	"plant",
	"undead",};
turns_per_unit = {
	["minute"] = 6;
	["hour"] = 360;
	["day"] = 360 * 24;
	["month"] = 360 * 24 * 30;
	["year"] = 360 * 24 * 365;
};
endcombat_regen_vitality = 5;
endcombat_minutes = 5;

rest_short = {
	["vigor"] = {[-5]=0, [-4]=0.1, [-3]=0.2, [-2]=0.3, [-1]=0.4, [0]=0.4, [1]=0.4, [2]=0.45, [3]=0.5, [4]=0.55, [5]=0.6},
	["vit"] = {[-5]=0, [-4]=0, [-3]=0, [-2]=0, [-1]=0, [0]=0, [1]=0, [2]=0, [3]=1, [4]=1, [5]=1},
	["save"] = {[-5]=0, [-4]=0, [-3]=1, [-2]=1, [-1]=2, [0]=2, [1]=2, [2]=2, [3]=3, [4]=3, [5]=4},
	-- ["aptitudes"] = {[-5]="0", [-4]="0", [-3]="1", [-2]="1", [-1]="1", [0]="1", [1]="1", [2]="1", [3]="2", [4]="2", [5]="3"},
	["regen"] = {[-5]=0, [-4]=0.25, [-3]=0.5, [-2]=1, [-1]=1.5, [0]=1.5, [1]=1.5, [2]=2, [3]=2.5, [4]=3, [5]=3.5},
	["tired"] = {[-5]=0, [-4]=0, [-3]=0, [-2]=0, [-1]=1, [0]=1, [1]=1, [2]=1, [3]=1, [4]=2, [5]=2},
};
rest_long = {
	["vigor"] = {[-5]=0.3, [-4]=0.4, [-3]=0.5, [-2]=0.6, [-1]=0.8, [0]=0.8, [1]=0.8, [2]=0.9, [3]=1, [4]=1, [5]=1},
	["vit"] = {[-5]=0, [-4]=0, [-3]=0, [-2]=1, [-1]=1, [0]=1, [1]=1, [2]=2, [3]=2, [4]=3, [5]=4},
	["save"] = {[-5]=0, [-4]=0, [-3]=1, [-2]=2, [-1]=3, [0]=3, [1]=3, [2]=4, [3]=5, [4]=6, [5]=7},
	-- ["aptitudes"] = {[-5]="All -2", [-4]="All -2", [-3]="All -1", [-2]="All -1", [-1]="All", [0]="All", [1]="All", [2]="All", [3]="All", [4]="All", [5]="All"},
	["regen"] = {[-5]=0, [-4]=0.25, [-3]=0.5, [-2]=1, [-1]=1.5, [0]=1.5, [1]=1.5, [2]=2, [3]=2.5, [4]=3, [5]=3.5},
	["tired"] = {[-5]=0, [-4]=1, [-3]=1, [-2]=1, [-1]=2, [0]=2, [1]=2, [2]=2, [3]=2, [4]=3, [5]=3},
	["pain"] = {[-5]=0, [-4]=0, [-3]=0, [-2]=0, [-1]=0, [0]=0, [1]=0, [2]=1, [3]=1, [4]=1, [5]=2},
	["mental"] = {[-5]=0, [-4]=0, [-3]=0, [-2]=0, [-1]=0.33, [0]=0.33, [1]=0.33, [2]=0.33, [3]=0.5, [4]=0.5, [5]=1},
};
rest_story = {
	["vigor"] = 1,
	["vit"] = 999,
	["save"] = -1,
	-- ["aptitudes"] = {[-5]="All -2", [-4]="All -2", [-3]="All -1", [-2]="All -1", [-1]="All", [0]="All", [1]="All", [2]="All", [3]="All", [4]="All", [5]="All"},
	["regen"] = 999,
	["tired"] = 99,
	["pain"] = 99,
	["mental"] = 99,
	["mental_perm"] = 1,
	["spiritual"] = 1,
};
rest_limits = {["min"]=-5, ["max"]=5};

rest_conditions = {
	["duration"] = {title = "Rest duration", conditions = {
		{label = "4 hours (long)", mod = -6, hours_long=4},
		{label = "30 minutes (short), 6 hours (long)", mod = -4, hours_long=6, hours_short=0.5},
		{label = "1 hour (short), 8 hours (long)", mod = -2, hours_long=8, hours_short=1},
		{label = "2 hour (short), 10 hours (long)", mod = 0, hours_long=10, hours_short=2},
		{label = "4 hour (short), 16 hours (long)", mod = 1, hours_long=16, hours_short=4},
		{label = "6 hour (short), 1 day (long)", mod = 2, hours_long=24, hours_short=6},
		}
	},
	["accomodation"] = {title = "Accomodation", conditions = {
		{label = "No accomodation", mod = -1},
		{label = "Basic accomodation: cheap room, tent, cave, ...", mod = 0},
		{label = "Nice accomodation: good room", mod = 1},
		}
	},
	["temperature"] = {title = "Temperature", conditions = {
		{label = "Very bad temperature: Frost cold or hellish hot", mod = -3},
		{label = "Bad temperature: cold, hot, ...", mod = -1},
		{label = "Nice temperature", mod = 0},
		}
	},
	["weather"] = {title = "Weather", conditions = {
		{label = "Very bad weather: Tempest, blizzard, inundated soil, ...", mod = -3},
		{label = "Bad weather: light rain, humid soil, arid weather, ...", mod = -1},
		{label = "Nice weather", mod = 0},
		}
	},
	["food"] = {title = "Food and drink", conditions = {
		{label = "No food or drink (may produce starvation effects)", mod = -3},
		{label = "Half ration", mod = -1},
		{label = "Normal ration", mod = 0},
		{label = "Double ration or food and drink of high quality", mod = 1},
		}
	},
	["light"] = {title = "Light and heat", conditions = {
		{label = "Neither light or heat", mod = -2},
		{label = "No light or heat", mod = -1},
		{label = "Light and heat present", mod = 0},
		}
	},
	["activities"] = {title = "Extra activities", conditions = {
		{label = "Taxing physical activities: Look for food, build, hunt, train, forge", mod = -99},
		{label = "Taxing mental activities: Focused read, learn a nuew spell, fabricate a new potion/scroll/rune, work", mod = -2},
		{label = "Medium activities: Watch, casual read, socialising, carry water", mod = -1},
		{label = "Light activities: Personal care, equipment maintenance, light fire, mount a tent, cook", mod = 0},
		}
	},
	["sleep"] = {title = "Sleep", tiring=true, conditions = {
		{label = "Nothing (long)", mod = -6, tiring=-3},
		{label = "25% usual duration (long)", mod = -4, tiring=-2},
		{label = "50% usual duration (long)", mod = -2, tiring=-1},
		{label = "75% usual duration (long)", mod = -1, tiring=0},
		{label = "100% usual duration (long)", mod = 0, tiring=0},
		{label = "125% usual duration (long), 30 minutes (short)", mod = 1, tiring=0},
		{label = "150% usual duration (long), 1 hour (short)", mod = 1, tiring=1},
		}
	},
};
rest_labels = {["short"] = "short", ["long"]="long"}

-- atributes
abilities_base_PA = 12;
abilities_PA_cost = {
	[0] = 0,
	[1] = 0,
	[2] = -1,
	[3] = 0,
	[4] = 1,
	[5] = 2,
	[6] = 4,
	[7] = 7,
	[8] = 11,
	[9] = 16,
	[10] = 22,
	[11] = 29,
	[12] = 37,
};
abilities_PA_individual = {
	[2] = 1,
	[3] = 1,
	[4] = 1,
	[5] = 2,
	[6] = 3,
	[7] = 4,
	[8] = 5,
	[9] = 6,
	[10] = 7,
	[11] = 8,
};
abilities_min_level = {
	[2] = 0,
	[3] = 0,
	[4] = 0,
	[5] = 0,
	[6] = 2,
	[7] = 6,
	[8] = 8,
	[9] = 10,
	[10] = 11,
	[11] = 12,
};
abilities_max_value = 11;
abilities_PA_magic = {
	["magic_category_weak"] = 0,
	["magic_category_medium"] = -1,
	["magic_category_strong"] = -2,
	[""] = 0,
};

-- Skill points
SP_cost = {
	["skilllist_basic"] = {0, 2, 5, 9, 2},
	["skilllist_learned"] = {1, 3, 6, 10, 2},
	["skilllist_specific"] = {0.5, 1.5, 3.5, 6.5, 1},
	["skilllist_weaponary"] = {1, 3, 6, 10, 2},
	["skilllist_magic"] = {1, 3, 6, 10, 2},
	["no_prof"] = {1, 2, 3, 4, 1},
	["no_prof_basic"] = {0, 1, 2, 3, 1},
};
SP_cost_individual = {
	["default"] = {1, 2, 3, 4},
	["specific"] = {0.5, 1, 2, 3},
	["no_prof"] = 1,
};
SP_min_level = {
	["default"] = {0, 0, 4, 8},
	["basic"] = {0, 0, 2, 4},
	["specific"] = {0, 0, 2, 6},
};

SP_per_level = {
	[0] = 0,
	[1] = 0,
	[2] = 0,
	[3] = 2,
	[4] = 2,
	[5] = 2,
	[6] = 2,
	[7] = 4,
	[8] = 4,
	[9] = 4,
	[10] = 4,
};
levels_SP_int = {0, 5, 10};


-- Other
nMaxNPC = 999;
coin_weights = {0.02, 0.015, 0.01, 0.008, 0.005};
DC_base = 7;
passive_base = 7;
DC_base = 7;
passive_base = 7;
spell_cast_DC_base = 10;
spell_conc_DC_base = 10;
spell_tie_DC_base = 10;
conc_multiple_increase_DC = 3;
conc_ID_max = 99999;
mandatory = "(Mandatory)";

transformation_replace = {
	"abilities.strength.base",
	"abilities.constitution.base",
	"abilities.agility.base",
	"abilities.dexterity.base",
	"abilities.perception.base",
	"health.fortitude",
	"health.endurance",
	"speed.base",
	"size",
	"senses",
	"speed_string",
	"portrait",
	"token", 
	"token_mounted", 
	"token_real", 
};
transformation_tag = " (transformation)";

recipe_time = {
	[""] = {[-2]=0, [-1]=0, [0]=12, [1]=8, [2]=6, [3]=4, [4]=3, [5]=2},
	["char_tooltip_prof_1"] = {[-2]=0, [-1]=0, [0]=0, [1]=8, [2]=6, [3]=4, [4]=3, [5]=2},
	["char_tooltip_prof_2"] = {[-2]=0, [-1]=0, [0]=0, [1]=0, [2]=8, [3]=6, [4]=4, [5]=3},
	["char_tooltip_prof_3"] = {[-2]=0, [-1]=0, [0]=0, [1]=0, [2]=0, [3]=8, [4]=6, [5]=4},
	["char_tooltip_prof_4"] = {[-2]=0, [-1]=0, [0]=0, [1]=0, [2]=0, [3]=0, [4]=8, [5]=6},
	["char_tooltip_prof_5"] = {[-2]=0, [-1]=0, [0]=0, [1]=0, [2]=0, [3]=0, [4]=0, [5]=8},
};
research_spell_min_mastery = {
	[0] = 2;
	[1] = 2;
	[2] = 2;
	[3] = 3;
	[4] = 3;
	[5] = 3;
	[6] = 4;
	[7] = 4;
	[0] = 4;
	[9] = 5;
	[10] = 5;
}
recipe_research_keyword = "research";
recipe_requires_magic = "magic gift";
recipe_time_research_mult = 1.5;

XP_level_mult = 3;
XP_level_0 = 5;
levels_PA = {
	[2] = 2,
	[3] = 2,
	[4] = 3,
	[5] = 3,
	[6] = 5,
	[7] = 5,
	[8] = 6,
	[9] = 6,
	[10] = 8,
};
levels_luck = {1, 5, 9};
levels_essence = {3, 7};
levels_fortitude = {4, 8, 10};
XP_int_mult = 3;
XP_int_threshold = 3;

luck_dice = "d8";

result_to_mode = {
	["critical success"] = "chat_success_crit";
	["success"] = "chat_success";
	["critical failure"] = "chat_miss_crit";
	["failure"] = "chat_miss";
};
result_to_stringres = {
	["critical success"] = "success_crit";
	["success"] = "success";
	["critical failure"] = "miss_crit";
	["failure"] = "miss";
};

-- Character level
level_max = 10;
level_max_ascendant = 15;
level_max_god = 20;

-- Afflictions
affliction_half_peligrosity = 3;
affliction_multiple_peligrosity = 3;


-- Aptitude names
aptitude = {
	["character"] = "Player character",
	["blood_power"] = "The power of blood",
	["quadruped"] = "Quadruped",

	["weapon_finesse"] = "Weapon finesse",
	["war_gestures"] = "War gestures",
	["combat_casting"] = "Combat casting",
	["chaotic_magic"] = "Chaotic magic",
	["hefty"] = "Hefty",
	["improved_ini"] = "Improved initiative",
	["light_sleep"] = "Light sleep",
	["prepare_extra_spell"] = "Prepare extra spell",
	["energy_absorption"] = "Energy absorption",
	["eagle_eye"] = "Eagle eye",
	["light_feet"] = "Light feet",
	["move_without_armor"] = "Move without armor",
	["slow"] = "Slow",
	["very_slow"] = "Very slow",
	["less_actions"] = "NOT IMPLEMENTED",
	["more_actions"] = "NOT IMPLEMENTED",
	["less_reactions"] = "NOT IMPLEMENTED",
	["more_reactions"] = "Oportunist",
	["less_fast"] = "NOT IMPLEMENTED",
	["more_fast"] = "Fast action",
	["untiring"] = "Tireless",
	["untiring"] = "Tireless",
	["robust"] = "Robust",
	["vigorous"] = "Vigorous",
	["die_hard"] = "Die hard",
	["indefatigable"] = "Indefatigable",
	["die_hard"] = "Die hard",
	["indefatigable"] = "Indefatigable",
	["essence_reserve"] = "Essence reserve",
	["overchannel"] = "Overchannel",
	["metallic"] = "Metallic",
	["soldier_rest"] = "Soldier rest",
	["stable_mind"] = "Stable mind",
	["strong_spirit"] = "Strong spirit",
	["no_spirit"] = "No spirit",
	["multiattack"] = "Multiattack",
	["tolerate_corruption"] = "Tolerant to corruption",
	["magic_piercing"] = "Magic piercing",
	["ascendant"] = "Ascendant",
	["god"] = "God",

	["empathic"] = "Empathic",
	["alert"] = "Alert",
	["fast_reflexes"] = "Fast reflexes",
	["great_fortitude"] = "Great fortitude",
	["iron_concentration"] = "Iron concentration",
	["common_sense"] = "Common sense",
	["evasion"] = "Evasion",
	["courage"] = "Courage",
	["unstopable"] = "Unstopable",
	["blind_fight"] = "Blind fight",

};

techniques = {
	["overchannel"] = "Overchannel",
	["components"] = "Saved components",
	["conc_advanced"] = "Advanced concentration",
	["conc_master"] = "Master concentration",
	["blood_magic"] = "Blood magic",
	["invisible_spells"] = "Invisible spells",
};



function getRulesetAndTranslatedAbility(sRuleset)
	return sRuleset, sRuleset;
end
