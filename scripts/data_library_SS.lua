--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function getItemIsIdentified(vRecord, vDefault)
	return LibraryData.getIDState("item", vRecord, true) and DB.getValue(vRecord, "modified", 0) == 0;
end

function getRecipeCategory(vNode)
	local sCat = DB.getValue(vNode, "category", "");
	if sCat == "" then
		sCat = "char_tooltip_prof_0";
	end
	return Interface.getString(sCat);
end

function getRecipeRarity(vNode)
	local sCat = DB.getValue(vNode, "rarity", "");
	if sCat == "" then
		sCat = "recipe_common";
	end
	return Interface.getString(sCat);
end

function detAfflictionType(vNode)
	return Interface.getString(DB.getValue(vNode, "cycler", ""));
end
function detAfflictionDelivery(vNode)
	return Interface.getString(DB.getValue(vNode, "delivery", ""));
end


function getRace(vNode)
	local sRaceSubrace = DB.getValue(vNode, "race", "");
	local s, _ = sRaceSubrace:find("%(");
	local result = sRaceSubrace;
	if s then
		result = string.sub(sRaceSubrace, 1, s-2);
	end
	return result;
end

function getNPCTypeValue(vNode)
	local v = StringManager.trim(DB.getValue(vNode, "type", ""));
	local sType = v:match("^[^(%s]+");
	if sType then
		v = StringManager.trim(sType);
	end
	v = StringManager.capitalize(v);
	return v;
end

function getItemRecordDisplayClass(vNode)
	local sRecordDisplayClass = "item";
	if vNode then
		local sBasePath, sSecondPath = UtilityManager.getDataBaseNodePathSplit(vNode);
		if sBasePath == "reference" then
			if sSecondPath == "equipmentdata" then
				local sTypeLower = StringManager.trim(DB.getValue(DB.getPath(vNode, "type"), ""):lower());
				if sTypeLower == "weapon" then
					sRecordDisplayClass = "reference_weapon";
				elseif sTypeLower == "armor" then
					sRecordDisplayClass = "reference_armor";
				elseif sTypeLower == "mounts and other animals" then
					sRecordDisplayClass = "reference_mountsandotheranimals";
				elseif sTypeLower == "waterborne vehicles" then
					sRecordDisplayClass = "reference_waterbornevehicles";
				elseif sTypeLower == "vehicle" then
					sRecordDisplayClass = "reference_vehicle";
				else
					sRecordDisplayClass = "reference_equipment";
				end
			else
				sRecordDisplayClass = "reference_magicitem";
			end
		end
	end
	return sRecordDisplayClass;
end

function isItemIdentifiable(vNode)
	return (getItemRecordDisplayClass(vNode) == "item");
end

function getSpellSourceValue(vNode)
	return StringManager.split(DB.getValue(vNode, "source", ""), ",", true);
end

function getSpellSchool(vNode)
	return StringManager.split(DB.getValue(vNode, "school", ""), ",", true);
end

function getRefType(vNode)
	return Interface.getString(DB.getValue(vNode, "reftype", ""));
end

function getSpellElements(vNode)
	local aArray = StringManager.split(DB.getValue(vNode, "elements", ""), ",", true);
	local aAll = {DataCommon.all, DataCommon.all .. "s", DataCommon.any};
	if aArray[1] and Utilities.inArray(aAll, aArray[1]:lower()) then
		return DataCommon.element_names;
	else
		return aArray;
	end
end

function getSpellKeywords(vNode)
	return StringManager.split(DB.getValue(vNode, "keywords", ""), ",", true);
end

function getSpellRarityValue(vNode)
	local nodeRarity = vNode.getChild("rarity");
	local sRarity = "";
	if nodeRarity then
		sRarity = Interface.getString(nodeRarity.getValue());
	end
	return sRarity;
end

function getSpellSubtypeValue(vNode)
	local nodeRarity = vNode.getChild("type_spell");
	local sRarity = "";
	if nodeRarity then
		sRarity = Interface.getString(nodeRarity.getValue());
	end
	return sRarity;
end

function getSpellIsArcane(vNode)
	local sSource = DB.getValue(vNode, "source", ""):lower();
	local bCond = DB.getValue(vNode, "reftype", "") == "power_spell";
	return bCond and sSource:find(Interface.getString("arcane"):lower());
end
function getSpellIsSacred(vNode)
	local sSource = DB.getValue(vNode, "source", ""):lower();
	local bCond = DB.getValue(vNode, "reftype", "") == "power_spell";
	return bCond and sSource:find(Interface.getString("sacred"):lower());
end
function getSpellIsPrimal(vNode)
	local sSource = DB.getValue(vNode, "source", ""):lower();
	local bCond = DB.getValue(vNode, "reftype", "") == "power_spell";
	return bCond and sSource:find(Interface.getString("primal"):lower());
end
function getSpellIsMental(vNode)
	local sSource = DB.getValue(vNode, "source", ""):lower();
	local bCond = DB.getValue(vNode, "reftype", "") == "power_spell";
	return bCond and sSource:find(Interface.getString("mental"):lower());
end
function getSpellIsElemental(vNode)
	local sSource = DB.getValue(vNode, "source", ""):lower();
	local bCond = DB.getValue(vNode, "reftype", "") == "power_spell";
	return bCond and sSource:find(Interface.getString("elemental"):lower());
end

function getSpellType(nodeRecord, sDefault)
	if nodeRecord then
		local sString = DB.getValue(nodeRecord, "type_spell", "");
		return Interface.getString(sString);
	end
	return sDefault;
end
function getSpellRarity(nodeRecord, sDefault)
	if nodeRecord then
		local sString = DB.getValue(nodeRecord, "rarity", "");
		return Interface.getString(sString);
	end
	return sDefault;
end
function getSpellComponents(nodeRecord, sDefault)
	if nodeRecord then
		local sString = DB.getValue(nodeRecord, "components", ""):lower();
		local sComp = "";
		local bAdd = false;
		for k,v in pairs(DataCommon.magic_components) do
			if sString:find(v:lower()) then
				if bAdd then
					sComp = sComp .. ", ";
				end
				sComp = sComp .. DataCommon.magic_components_short[k];
				bAdd = true;
			end
		end
		return sComp;
	end
	return sDefault;
end
function getSpellEssenceCost(nodeRecord, sDefault)
	if nodeRecord then
		local nEssence = DB.getValue(nodeRecord, "essence_cost", 0);
		local sTarget = DB.getValue(nodeRecord, "essence_target", "");
		if nEssence > 0 then
			return tostring(nEssence) .. " (" .. Interface.getString(sTarget) .. ")";
		else
			return sDefault;
		end
	end
	return sDefault;
end

function getAptitudeSubtypeValue(vNode)
	return AptitudeManager.getAptitudeSubtypeValue(vNode);
end
function getAptitudeLevel(vNode)
	return AptitudeManager.getAptitudeLevel(vNode);
end
function getAptitudeAllLevels(vNode)
	return AptitudeManager.getAptitudeAllLevels(vNode);
end
function getItemRarityValue(vNode)
	local sString = Interface.getString(DB.getValue(vNode, "rarity", ""));
	if not sString or sString == "" then
		sString = Interface.getString("item_rarity_common");
	end
	return sString;
end

function getTechniqueType(vNode)
	return Interface.getString(DB.getValue(vNode, "cycler", ""));
end
function getTechniqueSubtype(vNode)
	return Interface.getString(DB.getValue(vNode, "subcycler", ""));
end
function getTechniqueCombattype(vNode)
	return Interface.getString(DB.getValue(vNode, "combatcycler", ""));
end
function getTechniqueMagictype(vNode)
	return Interface.getString(DB.getValue(vNode, "magiccycler", ""));
end

function getTemplateType(vNode)
	return Interface.getString(DB.getValue(vNode, "type", ""));
end

aRecordOverrides = {
	-- CoreRPG overrides
	["quest"] = {
		aDataMap = { "quest", "reference.questdata" },
	},
	["image"] = {
		aDataMap = { "image", "reference.imagedata" },
		sListDisplayClass = "masterindexitem_image",
	},
	["npc"] = {
		aDataMap = { "npc", "reference.npcdata" },
		sListDisplayClass = "masterindexitem_npc",
		-- aGMListButtons = { "button_npc_letter", "button_npc_cr", "button_npc_type" };
		aCustomFilters = {
			["Type"] = { sField = "type" },
			["Level"] = { sField = "level", sType = "number" },
			["Ancestry"] = { sField = "race", fGetValue = getRace },
			["Family"] = { sField = "race" },
		},
	},
	["item"] = {
		aDataMap = { "item", "reference.equipmentdata", "reference.magicitemdata" },
		sListDisplayClass = "masterindexitem_item",
		fIsIdentifiable = isItemIdentifiable,
		fRecordDisplayClass = getItemRecordDisplayClass,
		aRecordDisplayClasses = { "item", "reference_magicitem", "reference_armor", "reference_weapon", "reference_equipment", "reference_mountsandotheranimals", "reference_waterbornevehicles", "reference_vehicle" },
		aGMListButtons = { "button_item_armor", "button_item_weapons", "button_item_templates" };
		aPlayerListButtons = { "button_item_armor", "button_item_weapons", "button_item_templates", };
		aCustomFilters = {
			["Type"] = { sField = "type" },
			["Subtype"] = { sField = "subtype" },
			["Rarity"] = { sField = "rarity", fGetValue = getItemRarityValue},
		},
		bHidden = false,
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},

	-- New record types
	["itemtemplate"] = {
		bExportNoReadOnly = true,
		sListDisplayClass = "masterindexitem_itemtemplate",
		bHidden = true,
		aDataMap = { "itemtemplate", "reference.magicrefitemdata" },
		aDisplayIcon = { "button_items", "button_items_down" },
		bHidden = false,
		aCustomFilters = {
			["Type"] = { sField = "type", fGetValue = getTemplateType },
		},
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["background"] = {
		bExportNoReadOnly = true,
		aDataMap = { "background", "reference.backgrounddata" },
		aDisplayIcon = { "button_backgrounds", "button_backgrounds_down" },
		sRecordDisplayClass = "reference_background",
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["class"] = {
		bExportNoReadOnly = true,
		aDataMap = { "class", "reference.classdata" },
		aDisplayIcon = { "button_classes", "button_classes_down" },
		sRecordDisplayClass = "reference_class",
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["aptitude"] = {
		bExportNoReadOnly = true,
		sListDisplayClass = "masterindexitem_aptitude",
		aDataMap = { "aptitude", "reference.aptitudedata" },
		aDisplayIcon = { "button_feats", "button_feats_down" },
		sRecordDisplayClass = "reference_aptitude",
		bHidden = false,
		sSidebarCategory = "create",
		aGMListButtons = { "button_aptitude_professional" };
		aPlayerListButtons = { "button_aptitude_professional" };
		aCustomFilters = {
			["Type"] = { sField = "reftype", fGetValue =  getRefType },
			["Subtype"] = { sField = "subtype", fGetValue =  getAptitudeSubtypeValue },
			["Ini. level"] = { sField = "level", sType = "number", fGetValue =  getAptitudeLevel };
			["Has level"] = { sField = "level", sType = "number", fGetValue =  getAptitudeAllLevels };
		},
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["technique"] = { 
		bExportNoReadOnly = true,
		sListDisplayClass = "masterindexitem_technique",
		aDataMap = { "technique", "reference.techniquedata" },
		aDisplayIcon = { "button_techniques", "button_techniques_down" },
		sRecordDisplayClass = "reference_technique",
		bHidden = false,
		sSidebarCategory = "create",
		aCustomFilters = {
			["Type"] = { sField = "cycler", fGetValue =  getTechniqueType },
			["Subtype"] = { sField = "subcycler", fGetValue =  getTechniqueSubtype },
			["Type (combat)"] = { sField = "combatcycler", fGetValue =  getTechniqueCombattype },
			["Type (magic)"] = { sField = "magiccycler", fGetValue =  getTechniqueMagictype },
		},
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["recipe"] = { 
		bExportNoReadOnly = true,
		sListDisplayClass = "masterindexitem_recipe",
		aDataMap = { "recipe", "reference.recipedata" },
		-- aDisplayIcon = { "button_techniques", "button_techniques_down" },
		sRecordDisplayClass = "reference_recipe",
		bHidden = false,
		sSidebarCategory = "create",
		aCustomFilters = {
			["Skill"] = { sField = "skill" },
			["Category"] = { sField = "category", fGetValue = getRecipeCategory },
			["Rarity"] = { sField = "rarity", fGetValue = getRecipeRarity },
		},
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["affliction"] = { 
		bExportNoReadOnly = true,
		sListDisplayClass = "masterindexitem_affliction",
		aDataMap = { "affliction", "reference.afflictiondata" },
		-- aDisplayIcon = { "button_techniques", "button_techniques_down" },
		sRecordDisplayClass = "reference_affliction",
		bHidden = false,
		sSidebarCategory = "create",
		aCustomFilters = {
			["Type"] = { sField = "cycler", fGetValue = detAfflictionType },
			["Rarity"] = { sField = "rarity", fGetValue = getRecipeRarity },
			["Delivery"] = { sField = "delivery", fGetValue = detAfflictionDelivery },
			["Supplies"] = { sField = "category", },
		},
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["race"] = {
		bExportNoReadOnly = true,
		aDataMap = { "race", "reference.racedata" },
		aDisplayIcon = { "button_races", "button_races_down" },
		sRecordDisplayClass = "reference_race",
		bHidden = false,
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["skill"] = {
		bExportNoReadOnly = true,
		bHidden = false,
		aDataMap = { "skill", "reference.skilldata" },
		aDisplayIcon = { "button_skills", "button_skills_down" },
		sRecordDisplayClass = "reference_skill",
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
	["spell"] = {
		bExportNoReadOnly = true,
		sListDisplayClass = "masterindexitem_power",
		bHidden = false,
		aGMListButtons = { "button_spells_arcane", "button_spells_sacred", "button_spells_primal", "button_spells_mental", "button_spells_elemental" };
		aPlayerListButtons = { "button_spells_arcane", "button_spells_sacred", "button_spells_primal", "button_spells_mental", "button_spells_elemental" };
		aDataMap = { "spell", "reference.spelldata" },
		aDisplayIcon = { "button_spells", "button_spells_down" },
		sRecordDisplayClass = "power",
		aCustomFilters = {
			["Type of magic"] = { sField = "source", fGetValue = getSpellSourceValue },
			["Level"] = { sField = "level", sType = "number" };
			["Talent"] = { sField = "school", fGetValue = getSpellSchool };
			["Type"] = { sField = "reftype", fGetValue = getRefType };
			["Rarity"] = { sField = "rarity", fGetValue = getSpellRarityValue };
			["Type of spell"] = { sField = "type_spell", fGetValue = getSpellType};
			["Element"] = { sField = "elements", fGetValue = getSpellElements };
			["Keyword"] = { sField = "keywords", fGetValue = getSpellKeywords };
		},
		aCustom = {
			tWindowMenu = { ["right"] = { "chat_output" } },
		},
	},
};

aListViews = {
	["npc"] = {
		["byletter"] = {
			sTitleRes = "npc_grouped_title_byletter",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "npc_grouped_label_name", nWidth=250 },
				{ sName = "cr", sType = "string", sHeadingRes = "npc_grouped_label_cr", sTooltipRe = "npc_grouped_tooltip_cr", bCentered=true },
			},
			aFilters = { },
			aGroups = { { sDBField = "name", nLength = 1 } },
			aGroupValueOrder = { },
		},
		-- ["bycr"] = {
		-- 	sTitleRes = "npc_grouped_title_bycr",
		-- 	aColumns = {
		-- 		{ sName = "name", sType = "string", sHeadingRes = "npc_grouped_label_name", nWidth=250 },
		-- 		{ sName = "cr", sType = "string", sHeadingRes = "npc_grouped_label_cr", sTooltipRe = "npc_grouped_tooltip_cr", bCentered=true },
		-- 	},
		-- 	aFilters = { },
		-- 	aGroups = { { sDBField = "cr", sPrefix = "CR" } },
		-- 	aGroupValueOrder = { "CR", "CR 0", "CR 1/8", "CR 1/4", "CR 1/2",
		-- 						"CR 1", "CR 2", "CR 3", "CR 4", "CR 5", "CR 6", "CR 7", "CR 8", "CR 9" },
		-- },
		["bytype"] = {
			sTitleRes = "npc_grouped_title_bytype",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "npc_grouped_label_name", nWidth=250 },
				{ sName = "cr", sType = "string", sHeadingRes = "npc_grouped_label_cr", sTooltipRe = "npc_grouped_tooltip_cr", bCentered=true },
			},
			aFilters = { },
			aGroups = { { sDBField = "type" } },
			aGroupValueOrder = { },
		},
	},
	["item"] = {
		["armor"] = {
			sTitleRes = "item_grouped_title_armor",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "item_grouped_label_name", nWidth=150 },
				{ sName = "cost", sType = "string", sHeadingRes = "item_grouped_label_cost", bCentered=true },
				{ sName = "protection", sType = "string", sHeadingRes = "item_grouped_label_ac", sTooltipRes = "item_grouped_tooltip_ac", nWidth=260, bCentered=true, nSortOrder=1 },
				{ sName = "init", sType = "number", sHeadingRes = "item_grouped_label_init", sTooltipRes = "item_grouped_tooltip_init", nWidth=30, bCentered=true, nSortOrder=3 },
				{ sName = "limit", sType = "number", nWidth=30, sHeadingRes = "item_grouped_label_limit", sTooltipRes = "item_grouped_tooltip_limit", bCentered=true, nSortOrder=2 },
				{ sName = "weight", sType = "number", sHeadingRes = "item_grouped_label_weight", sTooltipRes = "item_grouped_tooltip_weight", nWidth=30, bCentered=true },
				{ sName = "properties", sType = "string", sHeadingRes = "item_grouped_label_properties", nWidth=200, bCentered=true }
			},
			aFilters = {
				{ sDBField = "type", vFilterValue = "Armadura" }, -- WARNING: Español
				{ sCustom = "item_isidentified" }
			},
			aGroups = { { sDBField = "subtype" } },
			aGroupValueOrder = {}, -- "Light Armor", "Medium Armor", "Heavy Armor", "Shield" },
		},
		["weapon"] = {
			sTitleRes = "item_grouped_title_weapons",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "item_grouped_label_name", nWidth=150 },
				{ sName = "cost", sType = "string", sHeadingRes = "item_grouped_label_cost", bCentered=true },
				{ sName = "init", sType = "number", sHeadingRes = "item_grouped_label_init", sTooltipRes = "item_grouped_tooltip_init", nWidth=30, bCentered=true, nSortOrder=1 },
				{ sName = "weight", sType = "number", sHeadingRes = "item_grouped_label_weight", sTooltipRes = "item_grouped_tooltip_weight", nWidth=30, bCentered=true },
				{ sName = "attacks.id-00001.damage_attack", sType = "string", sHeadingRes = "item_grouped_label_damage", sTooltipRes = "item_grouped_tooltip_damage", nWidth=70, bCentered=true },
				{ sName = "attacks.id-00001.properties_attack", sType = "string", sHeadingRes = "item_grouped_label_properties", sTooltipRes = "item_grouped_tooltip_properties", nWidth=300, bCentered=true },
				-- { sName = "attacks.id-00002.damage_attack", sType = "string", sHeadingRes = "item_grouped_label_damage", sTooltipRes = "item_grouped_tooltip_damage2", nWidth=70, bCentered=true },
				-- { sName = "attacks.id-00002.properties_attack", sType = "string", sHeadingRes = "item_grouped_label_properties", sTooltipRes = "item_grouped_tooltip_properties2", nWidth=300, bCentered=true },
			},
			aFilters = {
				{ sDBField = "type", vFilterValue = "Arma" },
				{ sCustom = "item_isidentified" }
			},
			aGroups = { { sDBField = "subtype" } },
			aGroupValueOrder = {}, -- "Simple Melee Weapons", "Simple Ranged Weapons", "Martial Weapons", "Martial Melee Weapons", "Martial Ranged Weapons" },
		},
	},
	["spell"] = {
		["arcane"] = {
			sTitleRes = "arcane",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "power_spell", nWidth=150 },
				{ sName = "type_spell", sType = "custom", sHeadingRes = "random_item_label_type", nWidth=70, bCentered=true, sCustom="getstring" },
				{ sName = "rarity", sType = "custom", sHeadingRes = "spell_rarity", nWidth=70, bCentered=true },
				{ sName = "school", sType = "string", sHeadingRes = "ref_label_school", nWidth=90, bCentered=true },
				{ sName = "components", sType = "custom", sHeadingRes = "ref_label_components", nWidth=90, bCentered=true },
				{ sName = "castingtime", sType = "string", sHeadingRes = "ref_label_castingtime", nWidth=90, bCentered=true },
				{ sName = "duration", sType = "string", sHeadingRes = "ref_label_duration", nWidth=90, bCentered=true },
				{ sName = "essence_cost", sType = "custom", sHeadingRes = "ref_label_essence_cost", nWidth=90, bCentered=true },
				{ sName = "keywords", sType = "string", sHeadingRes = "spell_keywords", nWidth=250, bCentered=true },
				-- Otros campos: sTemplate, bWrapped, sTooltipRes, nSortOrder
			},
			aFilters = {
				{ sCustom = "spell_isarcane" }
			},
			aGroups = { { sDBField = "level", sPrefix = "Nivel"} },
			aGroupValueOrder = {"Nivel 0", "Nivel 1", "Nivel 2", "Nivel 3", "Nivel 4", "Nivel 5", "Nivel 6", "Nivel 7", "Nivel 8", "Nivel 9", "Nivel 10", }, 
		},
		
		["sacred"] = {
			sTitleRes = "sacred",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "power_spell", nWidth=150 },
				{ sName = "type_spell", sType = "custom", sHeadingRes = "random_item_label_type", nWidth=70, bCentered=true, sCustom="getstring" },
				{ sName = "rarity", sType = "custom", sHeadingRes = "spell_rarity", nWidth=70, bCentered=true },
				{ sName = "school", sType = "string", sHeadingRes = "ref_label_school", nWidth=90, bCentered=true },
				{ sName = "components", sType = "custom", sHeadingRes = "ref_label_components", nWidth=90, bCentered=true },
				{ sName = "castingtime", sType = "string", sHeadingRes = "ref_label_castingtime", nWidth=90, bCentered=true },
				{ sName = "duration", sType = "string", sHeadingRes = "ref_label_duration", nWidth=90, bCentered=true },
				{ sName = "essence_cost", sType = "custom", sHeadingRes = "ref_label_essence_cost", nWidth=90, bCentered=true },
				{ sName = "keywords", sType = "string", sHeadingRes = "spell_keywords", nWidth=250, bCentered=true },
				-- Otros campos: sTemplate, bWrapped, sTooltipRes, nSortOrder
			},
			aFilters = {
				{ sCustom = "spell_issacred" }
			},
			aGroups = { { sDBField = "level", sPrefix = "Nivel"} },
			aGroupValueOrder = {"Nivel 0", "Nivel 1", "Nivel 2", "Nivel 3", "Nivel 4", "Nivel 5", "Nivel 6", "Nivel 7", "Nivel 8", "Nivel 9", "Nivel 10", }, 
		},
		
		["primal"] = {
			sTitleRes = "primal",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "power_spell", nWidth=150 },
				{ sName = "type_spell", sType = "custom", sHeadingRes = "random_item_label_type", nWidth=70, bCentered=true, sCustom="getstring" },
				{ sName = "rarity", sType = "custom", sHeadingRes = "spell_rarity", nWidth=70, bCentered=true },
				{ sName = "school", sType = "string", sHeadingRes = "ref_label_school", nWidth=90, bCentered=true },
				{ sName = "components", sType = "custom", sHeadingRes = "ref_label_components", nWidth=90, bCentered=true },
				{ sName = "castingtime", sType = "string", sHeadingRes = "ref_label_castingtime", nWidth=90, bCentered=true },
				{ sName = "duration", sType = "string", sHeadingRes = "ref_label_duration", nWidth=90, bCentered=true },
				{ sName = "essence_cost", sType = "custom", sHeadingRes = "ref_label_essence_cost", nWidth=90, bCentered=true },
				{ sName = "keywords", sType = "string", sHeadingRes = "spell_keywords", nWidth=250, bCentered=true },
				-- Otros campos: sTemplate, bWrapped, sTooltipRes, nSortOrder
			},
			aFilters = {
				{ sCustom = "spell_isprimal" }
			},
			aGroups = { { sDBField = "level", sPrefix = "Nivel"} },
			aGroupValueOrder = {"Nivel 0", "Nivel 1", "Nivel 2", "Nivel 3", "Nivel 4", "Nivel 5", "Nivel 6", "Nivel 7", "Nivel 8", "Nivel 9", "Nivel 10", }, 
		},
		
		["mental"] = {
			sTitleRes = "psychic",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "power_spell", nWidth=150 },
				{ sName = "type_spell", sType = "custom", sHeadingRes = "random_item_label_type", nWidth=70, bCentered=true, sCustom="getstring" },
				{ sName = "rarity", sType = "custom", sHeadingRes = "spell_rarity", nWidth=70, bCentered=true },
				{ sName = "school", sType = "string", sHeadingRes = "ref_label_school", nWidth=90, bCentered=true },
				{ sName = "components", sType = "custom", sHeadingRes = "ref_label_components", nWidth=90, bCentered=true },
				{ sName = "castingtime", sType = "string", sHeadingRes = "ref_label_castingtime", nWidth=90, bCentered=true },
				{ sName = "duration", sType = "string", sHeadingRes = "ref_label_duration", nWidth=90, bCentered=true },
				{ sName = "essence_cost", sType = "custom", sHeadingRes = "ref_label_essence_cost", nWidth=90, bCentered=true },
				{ sName = "keywords", sType = "string", sHeadingRes = "spell_keywords", nWidth=250, bCentered=true },
				-- Otros campos: sTemplate, bWrapped, sTooltipRes, nSortOrder
			},
			aFilters = {
				{ sCustom = "spell_ismental" }
			},
			aGroups = { { sDBField = "level", sPrefix = "Nivel"} },
			aGroupValueOrder = {"Nivel 0", "Nivel 1", "Nivel 2", "Nivel 3", "Nivel 4", "Nivel 5", "Nivel 6", "Nivel 7", "Nivel 8", "Nivel 9", "Nivel 10", }, 
		},
		
		["elemental"] = {
			sTitleRes = "elemental",
			aColumns = {
				{ sName = "name", sType = "string", sHeadingRes = "power_spell", nWidth=150 },
				{ sName = "type_spell", sType = "custom", sHeadingRes = "random_item_label_type", nWidth=70, bCentered=true, sCustom="getstring" },
				{ sName = "rarity", sType = "custom", sHeadingRes = "spell_rarity", nWidth=70, bCentered=true },
				{ sName = "school", sType = "string", sHeadingRes = "ref_label_school", nWidth=90, bCentered=true },
				{ sName = "components", sType = "custom", sHeadingRes = "ref_label_components", nWidth=90, bCentered=true },
				{ sName = "castingtime", sType = "string", sHeadingRes = "ref_label_castingtime", nWidth=90, bCentered=true },
				{ sName = "duration", sType = "string", sHeadingRes = "ref_label_duration", nWidth=90, bCentered=true },
				{ sName = "essence_cost", sType = "custom", sHeadingRes = "ref_label_essence_cost", nWidth=90, bCentered=true },
				{ sName = "keywords", sType = "string", sHeadingRes = "spell_keywords", nWidth=250, bCentered=true },
				-- Otros campos: sTemplate, bWrapped, sTooltipRes, nSortOrder
			},
			aFilters = {
				{ sCustom = "spell_iselemental" }
			},
			aGroups = { { sDBField = "level", sPrefix = "Nivel"},
						{sDBField = "elements"}
			},
			aGroupValueOrder = {"Nivel 0", "Nivel 1", "Nivel 2", "Nivel 3", "Nivel 4", "Nivel 5", "Nivel 6", "Nivel 7", "Nivel 8", "Nivel 9", "Nivel 10", }, 
		},
	},
};

-- aExport = {
-- 	{["name"] = "aptitude", ["label"] = "ref_type_feat"},
-- 	{["name"] = "class", ["label"] = "ref_type_class"},
-- 	{["name"] = "background", ["label"] = "ref_type_background"},
-- };

function onInit()
	LibraryData.setCustomFilterHandler("item_isidentified", getItemIsIdentified);

	LibraryData.setCustomFilterHandler("spell_isarcane", getSpellIsArcane);
	LibraryData.setCustomFilterHandler("spell_issacred", getSpellIsSacred);
	LibraryData.setCustomFilterHandler("spell_isprimal", getSpellIsPrimal);
	LibraryData.setCustomFilterHandler("spell_ismental", getSpellIsMental);
	LibraryData.setCustomFilterHandler("spell_iselemental", getSpellIsElemental);
	LibraryData.setCustomGroupOutputHandler("spell_level", getSpellLevelString);
	LibraryData.setCustomColumnHandler("type_spell", getSpellType);
	LibraryData.setCustomColumnHandler("rarity", getSpellRarity);
	LibraryData.setCustomColumnHandler("components", getSpellComponents);
	LibraryData.setCustomColumnHandler("essence_cost", getSpellEssenceCost);
	
	LibraryData.overrideRecordTypes(aRecordOverrides);
	LibraryData.setRecordViews(aListViews);
	LibraryData.setRecordTypeInfo("vehicle", nil);
	
end
