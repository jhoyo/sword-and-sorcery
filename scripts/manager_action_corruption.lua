--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--
function onInit()
	ActionsManager.registerModHandler("corruption", modRoll);
	ActionsManager.registerResultHandler("corruption", onRoll);
end

-- function handleApplyAttack(msgOOB)
-- 	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
-- 	local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);
--
-- 	local nTotal = tonumber(msgOOB.nTotal) or 0;
-- 	applyAttack(rSource, rTarget, (tonumber(msgOOB.nSecret) == 1), msgOOB.sAttackType, msgOOB.sDesc, nTotal, msgOOB.sResults);
-- end
--
-- function notifyApplyAttack(rSource, rTarget, bSecret, sAttackType, sDesc, nTotal, sResults)
-- 	if not rTarget then
-- 		return;
-- 	end
--
-- 	local msgOOB = {};
-- 	msgOOB.type = OOB_MSGTYPE_APPLYATK;
--
-- 	if bSecret then
-- 		msgOOB.nSecret = 1;
-- 	else
-- 		msgOOB.nSecret = 0;
-- 	end
-- 	msgOOB.sAttackType = sAttackType;
-- 	msgOOB.nTotal = nTotal;
-- 	msgOOB.sDesc = sDesc;
-- 	msgOOB.sResults = sResults;
--
-- 	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
-- 	msgOOB.sSourceType = sSourceType;
-- 	msgOOB.sSourceNode = sSourceNode;
--
-- 	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
-- 	msgOOB.sTargetType = sTargetType;
-- 	msgOOB.sTargetNode = sTargetNode;
--
-- 	Comm.deliverOOBMessage(msgOOB, "");
-- end

function performRoll(draginfo, rActor, rAction)
	local rRoll = getRoll(rActor, rAction);

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function getRoll(rActor, rAction)
	local nAdv = rAction.nAdv or 0;

	-- Build basic roll
	local rRoll = {};
	rRoll.sType = "corruption";
	rRoll.aDice = {"d8","d8"};
	rRoll.sTypeCorruption = rAction.type;

	-- Build the description label
	-- rRoll.sDesc = rAction.label or "";
	rRoll.nMod = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["tolerate_corruption"]);
	rRoll.nAdv = 0;
	-- rRoll.sLabel = rAction.sName:lower();


	return rRoll;
end

function modRoll(rSource, rTarget, rRoll)
	-- Proceed to the next step
	ActionsManager2.encodeDesktopMods(rRoll);
end

function onRoll(rSource, rTarget, rRoll)
	-- Make the rolls
	ActionsManager2.decodeAdvantage(rRoll);

	-- Create the print message
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	rMessage.text = string.gsub(rMessage.text, " %[MOD:[^]]*%]", "");
	rMessage.mode = "chat_unknown";

	-- Comm.deliverChatMessage(rMessage);
	MessageManager.onRollMessage(rMessage);

	-- Manage result
	handleCorruption(rSource, rRoll);

end

function handleCorruption(rSource, rRoll)
	local nodeActor = ActorManager2.getActorAndNode(rSource);
	-- Get target DC
	local nTotal = ActionsManager.total(rRoll);
	local nCorruptionAux = DB.getValue(nodeActor, "health.corruption." .. rRoll.sTypeCorruption);
	local nCorruption = nCorruptionAux + DB.getValue(nodeActor, "health.corruption.spirit");
	Debug.chat("nCorruption", nCorruption)

	-- Start message
	local sIcon = "char_body";
	if rRoll.sTypeCorruption == "mind" then
		sIcon = "component_concentration";
	elseif rRoll.sTypeCorruption == "spirit" then
		sIcon = "char_soul";
	end
	local msgShort = {font = "msgfont", icon=sIcon, mode="chat_success", text=Interface.getString("corruption_action_nothing")};
	local msgLong = {font = "msgfont", icon=sIcon, mode="chat_success", text=Interface.getString("corruption_action_nothing")};
	
	-- Handle bad result
	if nCorruption > 0 and nTotal < nCorruption then
		-- Remove corruption
		local nRemove = 0;
		local aStats = DataCommon.abilities_type[rRoll.sTypeCorruption];
		for _,v in pairs(aStats) do
			local nValue = ActorManager2.getCurrentAbilityValue(rSource, v);
			nRemove = math.max(nRemove, nValue);
		end
		nRemove = nRemove + ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["tolerate_corruption"]);
		nRemove = math.min(nCorruptionAux, nRemove);
		DB.setValue(nodeActor, "health.corruption." .. rRoll.sTypeCorruption, "number", nCorruptionAux - nRemove)

		-- Message
		msgShort.mode = "chat_miss";
		msgLong.mode = "chat_miss";
		msgShort.text = Interface.getString("corruption_mutation_" .. rRoll.sTypeCorruption):format(nRemove);
		msgLong.text = Interface.getString("corruption_mutation_" .. rRoll.sTypeCorruption):format(nRemove);
	end

	-- Send message
	ActionsManager.outputResult(false, rSource, nil, msgLong, msgShort);

end

