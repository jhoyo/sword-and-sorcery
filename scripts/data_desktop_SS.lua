--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	-- Add module data
	for k,v in pairs(aDataModuleSet) do
		for _,v2 in ipairs(v) do
			Desktop.addDataModuleSet(k, v2);
		end
	end

	-- Add sidebar small buttons
	-- local tButton = { icon = "sidebar_icon_recordtype_winds_of_magic",
	local tButton = { icon = "sidebar_icon_bang",
					 iconPressed = "sidebar_icon_recordtype_winds_of_magic",
					 tooltipres = "library_recordtype_label_winds_of_magic",
					 class = "winds_of_magic",
					 path = "windsofmagic" };
	-- DesktopManager.registerSidebarStackButton(tButton, true);
	DesktopManager.registerSidebarToolButton(tButton, true);
	DesktopManager.removeSidebarToolButton("modifiers");
end

aDataModuleSet = {};


-- aDataModuleSet =
-- {
-- 	["local"] =
-- 	{
-- 		{
-- 			name = "5E - SRD",
-- 			modules =
-- 			{
-- 				{ name = "DD5E SRD Bestiary" },
-- 				{ name = "DD5E SRD Data" },
-- 				{ name = "DD5E SRD Magic Items" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - Basic Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD Basic Rules - DM" },
-- 				{ name = "DD Basic Rules - Player" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - Core Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD Dungeon Masters Guide", storeid = "WOTC5EDMG" },
-- 				{ name = "DD MM Monster Manual", storeid = "WOTC5EMMDELUXE" },
-- 				{ name = "DD PHB Deluxe", storeid = "WOTC5EPHBDELUXE" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - All Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD Dungeon Masters Guide", storeid = "WOTC5EDMG" },
-- 				{ name = "DD Dungeon Masters Guide - Players", storeid = "WOTC5EDMG" },
-- 				{ name = "DD MM Monster Manual", storeid = "WOTC5EMMDELUXE" },
-- 				{ name = "DD PHB Deluxe", storeid = "WOTC5EPHBDELUXE" },
-- 				{ name = "DD Curse of Strahd Players", storeid = "WOTC5ECOS" },
-- 				{ name = "DD Elemental Evil Players Companion", storeid = "WOTC5EEEPC" },
-- 				{ name = "D&D Sword Coast Adventurer's Guide - Player's Guide", storeid = "WOTC5ESCAG" },
-- 				{ name = "DD Tomb of Annihilation - Players", storeid = "WOTC5ETOA" },
-- 				{ name = "Volos Guide to Monsters Players", storeid = "WOTC5EVGM" },
-- 				{ name = "DD Xanathar's Guide to Everything Players", storeid = "WOTC5EXGTE" },
-- 				{ name = "DD Mordenkainen's Tome of Foes Players", storeid = "WOTC5EMTOF" },
-- 				{ name = "DD Wayfinder's Guide to Eberron", storeid = "WOTC5EWGTE" },
-- 			},
-- 		},
-- 	},
-- 	["client"] =
-- 	{
-- 		{
-- 			name = "5E - SRD",
-- 			modules =
-- 			{
-- 				{ name = "DD5E SRD Data" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - Basic Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD Basic Rules - Player" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - Core Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD PHB Deluxe", storeid = "WOTC5EPHBDELUXE" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - All Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD PHB Deluxe", storeid = "WOTC5EPHBDELUXE" },
-- 				{ name = "DD Dungeon Masters Guide - Players", storeid = "WOTC5EDMG" },
-- 				{ name = "DD Curse of Strahd Players", storeid = "WOTC5ECOS" },
-- 				{ name = "DD Elemental Evil Players Companion", storeid = "WOTC5EEEPC" },
-- 				{ name = "D&D Sword Coast Adventurer's Guide - Player's Guide", storeid = "WOTC5ESCAG" },
-- 				{ name = "DD Tomb of Annihilation - Players", storeid = "WOTC5ETOA" },
-- 				{ name = "Volos Guide to Monsters Players", storeid = "WOTC5EVGM" },
-- 				{ name = "DD Xanathar's Guide to Everything Players", storeid = "WOTC5EXGTE" },
-- 				{ name = "DD Mordenkainen's Tome of Foes Players", storeid = "WOTC5EMTOF" },
-- 				{ name = "DD Wayfinder's Guide to Eberron", storeid = "WOTC5EWGTE" },
-- 			},
-- 		},
-- 	},
-- 	["host"] =
-- 	{
-- 		{
-- 			name = "5E - SRD",
-- 			modules =
-- 			{
-- 				{ name = "DD5E SRD Bestiary" },
-- 				{ name = "DD5E SRD Data" },
-- 				{ name = "DD5E SRD Magic Items" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - Basic Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD Basic Rules - DM" },
-- 				{ name = "DD Basic Rules - Player" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - Core Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD Dungeon Masters Guide", storeid = "WOTC5EDMG" },
-- 				{ name = "DD MM Monster Manual", storeid = "WOTC5EMMDELUXE" },
-- 				{ name = "DD PHB Deluxe", storeid = "WOTC5EPHBDELUXE" },
-- 			},
-- 		},
-- 		{
-- 			name = "5E - All Rules",
-- 			modules =
-- 			{
-- 				{ name = "DD Dungeon Masters Guide", storeid = "WOTC5EDMG" },
-- 				{ name = "DD Dungeon Masters Guide - Players", storeid = "WOTC5EDMG" },
-- 				{ name = "DD MM Monster Manual", storeid = "WOTC5EMMDELUXE" },
-- 				{ name = "DD PHB Deluxe", storeid = "WOTC5EPHBDELUXE" },
-- 				{ name = "DD Curse of Strahd Players", storeid = "WOTC5ECOS" },
-- 				{ name = "DD Elemental Evil Players Companion", storeid = "WOTC5EEEPC" },
-- 				{ name = "D&D Sword Coast Adventurer's Guide - Player's Guide", storeid = "WOTC5ESCAG" },
-- 				{ name = "D&D Sword Coast Adventurer's Guide - Campaign Guide", storeid = "WOTC5ESCAG" },
-- 				{ name = "DD Tomb of Annihilation - Players", storeid = "WOTC5ETOA" },
-- 				{ name = "DD Volos Guide to Monsters", storeid = "WOTC5EVGM" },
-- 				{ name = "Volos Guide to Monsters Players", storeid = "WOTC5EVGM" },
-- 				{ name = "DD Xanathar's Guide to Everything", storeid = "WOTC5EXGTE" },
-- 				{ name = "DD Xanathar's Guide to Everything Players", storeid = "WOTC5EXGTE" },
-- 				{ name = "DD Mordenkainen's Tome of Foes", storeid = "WOTC5EMTOF" },
-- 				{ name = "DD Mordenkainen's Tome of Foes Players", storeid = "WOTC5EMTOF" },
-- 				{ name = "DD Wayfinder's Guide to Eberron", storeid = "WOTC5EWGTE" },
-- 			},
-- 		},
-- 	},
-- };
