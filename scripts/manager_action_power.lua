--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYSAVEVS = "applysavevs";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYSAVEVS, handleApplySaveVs);

	ActionsManager.registerTargetingHandler("powersave", onPowerTargeting);
	ActionsManager.registerModHandler("powersave", modPowerSave);
	ActionsManager.registerResultHandler("powersave", onPowerSave);
end

function handleApplySaveVs(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);

	-- Get the skill nodes correctly
	local node_skill, isSpecialty, node_specialty = ActorManager2.getSaveNode(rTarget, msgOOB.save_skill);
	if isSpecialty then
		node_skill = node_specialty;
	end

	-- Get the action table
	local rAction = {};
	rAction.type = "save";
	rAction.nodeSkill = node_skill;
	rAction.bSpecialty = isSpecialty;
	rAction.sStat = msgOOB.save_stat;
	rAction.nDif = msgOOB.nDif;
	rAction.bSecret = msgOOB.bSecret;
	rAction.sSaveEffect = msgOOB.save_effect;
	rAction.sOrigin = msgOOB.sOrigin;
	rAction.keywords = msgOOB.keywords;
	rAction.school = msgOOB.school;
	rAction.nAdv = msgOOB.nAdv;
	rAction.nForced = 1;

	-- Make the skill roll
	ActionSkill.performRoll(nil, rTarget, rAction);
end

function notifyApplySaveVs(rSource, rTarget, rRoll)
	if not rTarget then
		return;
	end

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYSAVEVS;

	msgOOB.bSecret = rRoll.bSecret;
	msgOOB.sDesc = rRoll.sDesc;
	msgOOB.nDif = rRoll.nDif;
	msgOOB.save_stat = rRoll.save_stat;
	msgOOB.save_skill = rRoll.save_skill;
	msgOOB.save_effect = rRoll.save_effect;
	msgOOB.sOrigin = rRoll.sOrigin;
	msgOOB.keywords = rRoll.keywords;
	msgOOB.school = rRoll.school;
	msgOOB.nAdv = rRoll.nAdv;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	msgOOB.sTargetType = sTargetType;
	msgOOB.sTargetNode = sTargetNode;

	Comm.deliverOOBMessage(msgOOB, "");
end

function onPowerTargeting(rSource, aTargeting, rRolls)
	local bRemoveOnMiss = false;
	local sOptRMMT = OptionsManager.getOption("RMMT");
	if sOptRMMT == "on" then
		bRemoveOnMiss = true;
	elseif sOptRMMT == "multi" then
		local aTargets = {};
		for _,vTargetGroup in ipairs(aTargeting) do
			for _,vTarget in ipairs(vTargetGroup) do
				table.insert(aTargets, vTarget);
			end
		end
		bRemoveOnMiss = (#aTargets > 1);
	end

	if bRemoveOnMiss then
		for _,vRoll in ipairs(rRolls) do
			vRoll.bRemoveOnMiss = "true";
		end
	end

	return aTargeting;
end

function getSaveVsRoll(rActor, rAction)
	local rRoll = {};
	rRoll.sType = "powersave";
	rRoll.aDice = {};

	-- Get modifier
	rRoll.nMod = 0;
	rRoll.sOrigin = ActorManager.getCTNodeName(rActor);

	-- Get difficulty
	rRoll.nDif, _, _, _ = ActorManager2.getCheck(rActor, rAction.dif_stat, rAction.dif_skill, rAction.dif_skill, false, true);
	rRoll.nDif = rRoll.nDif + (rAction.dif_mod or 0) + (rAction.nMod or 0);
	rRoll.nAdv = rAction.nAdv or 0;

	-- Save info
	rRoll.save_stat = rAction.save_stat;
	rRoll.save_skill = rAction.save_skill;
	rRoll.save_effect = rAction.save_effect;
	rRoll.keywords = rAction.keywords;
	rRoll.school = rAction.school;

	-- Message
	rRoll.sDesc = Interface.getString("save"):upper();
	if rAction.order and rAction.order > 1 then
		rRoll.sDesc = rRoll.sDesc .. " #" .. rAction.order;
	end
	rRoll.sDesc = rRoll.sDesc .. "\n";
	if rAction.label then
		rRoll.sDesc = rRoll.sDesc .. "\n" .. rAction.label:upper();
	end

	-- Check
	if rRoll.save_stat or rRoll.save_skill then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("check") .. ": ";
	 	if rRoll.save_stat then
			rRoll.sDesc = rRoll.sDesc .. rRoll.save_stat;
			if rRoll.save_skill then
				rRoll.sDesc = rRoll.sDesc .. " + ";
			end
	 	end
	 	if rRoll.save_skill then
			rRoll.sDesc = rRoll.sDesc .. rRoll.save_skill;
		end
		if rRoll.nDif then
			rRoll.sDesc = rRoll.sDesc .. " (" .. rRoll.nDif .. ")";
		end
	end
	if rAction.save_effect and rAction.save_effect ~= "" then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString(rAction.save_effect);
	end

	-- Keywords
	if rRoll.keywords then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("keywords") .. ": " .. rRoll.keywords;
		if rAction.magic then
			rRoll.sDesc = rRoll.sDesc .. ", " .. Interface.getString("keyword_magic");
		end
	else
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("keywords") .. ": " .. Interface.getString("keyword_magic");
	end


	-- if rAction.save_effect == "power_mod_damage" or rAction.save_effect == "power_mod_durdamage" then
	-- 	rRoll.sDesc =rRoll.sDesc .. " [Damage modified]";
	-- end
	-- if rAction.save_effect == "power_mod_duration" or rAction.save_effect == "power_mod_durdamage" then
	-- 	rRoll.sDesc = rRoll.sDesc .. " [Duration modified]";
	-- end
	--
	-- if rAction.keywords ~= "" then
	-- 	rRoll.sDesc = rRoll.sDesc .. " (" .. rAction.keywords .. ")";
	-- end

	return rRoll;
end

function performSaveVsRoll(draginfo, rActor, rAction)
	local rRoll = getSaveVsRoll(rActor, rAction);

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function modPowerSave(rSource, rTarget, rRoll)
end

-- function onPowerCast(rSource, rTarget, rRoll)
-- 	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
-- 	rMessage.dice = nil;
-- 	rMessage.icon = "roll_cast";
--
-- 	if rTarget then
-- 		rMessage.text = rMessage.text .. " [at " .. ActorManager.getDisplayName(rTarget) .. "]";
-- 	end
--
-- 	Comm.deliverChatMessage(rMessage);
-- end

function onCastSave(rSource, rTarget, rRoll)
	if rTarget then
		notifyApplySaveVs(rSource, rTarget, rRoll);
		return true;
	end

	return false;
end

function onPowerSave(rSource, rTarget, rRoll)
	-- Advantage
	ActionsManager2.encodeAdvantage(rRoll);
	rRoll.aDice = {};

	if rTarget then
		notifyApplySaveVs(rSource, rTarget, rRoll);
		rRoll.sDesc = rRoll.sDesc .. "\n" .. Interface.getString("power_label_targeting") .. ": " .. ActorManager.getDisplayName(rTarget);
	end

	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	rMessage.mode = "chat_save";
	rMessage.font = "chat";
	-- Comm.deliverChatMessage(rMessage);
	MessageManager.onRollMessage(rMessage);
end
