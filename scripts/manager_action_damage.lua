--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYDMG = "applydmg";
OOB_MSGTYPE_APPLYDMGSTATE = "applydmgstate";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYDMG, handleApplyDamage);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYDMGSTATE, handleApplyDamageState);

	ActionsManager.registerModHandler("damage", modDamage);
	ActionsManager.registerResultHandler("damage", onDamage);
end

function handleApplyDamage(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);
	if rTarget then
		rTarget.nOrder = msgOOB.nTargetOrder;
	end

	local rAction  = {};
	rAction.bSecret = tonumber(msgOOB.nSecret) == 1;
	rAction.nTotal = tonumber(msgOOB.nTotal) or 0;
	rAction.sDamageTypes = msgOOB.sDamageTypes;
	rAction.nPiercing = msgOOB.nPiercing;
	rAction.bSelfTarget = msgOOB.nSelfTarget == "1";
	if msgOOB.sNotification and msgOOB.sNotification ~= "" then
		rAction.aNotifications = {msgOOB.sNotification};
	end

	applyDamage(rSource, rTarget, rAction);
end


function notifyApplyDamage(rSource, rTarget, rAction)
	if not rTarget then
		return;
	end
	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	if sTargetType ~= "pc" and sTargetType ~= "ct" then
		return;
	end

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYDMG;

	if rAction.bSecret then
		msgOOB.nSecret = 1;
	else
		msgOOB.nSecret = 0;
	end
	msgOOB.nTotal = rAction.nTotal;
	msgOOB.sDamageTypes = rAction.sDamageTypes;
	msgOOB.nPiercing = rAction.nPiercing or 0;
	msgOOB.sTargetType = sTargetType;
	msgOOB.sTargetNode = sTargetNode;
	msgOOB.nTargetOrder = rTarget.nOrder;
	msgOOB.nSelfTarget = rAction.nSelfTarget or 0;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	Comm.deliverOOBMessage(msgOOB, "");
end

function getRoll(rActor, rAction)
	local rRoll = {};
	rRoll.sType = "damage";
	rRoll.aDice = rAction.aDice or {};
	rRoll.nMod = rAction.nMod or 0;
	rRoll.sDamageTypes = rAction.sDamageTypes or "";
	rRoll.bWeapon = rAction.bWeapon;
	rRoll.nPiercing = rAction.nPiercing or 0;
	rRoll.nReroll = rAction.nReroll or 0;
	rRoll.nAvoidNotifyAction = rAction.nAvoidNotifyAction or 0;
	rRoll.range = rAction.range;
	rRoll.nIncorporeal = 0;
	if rAction.bIncorporeal then
		rRoll.nIncorporeal = 1;
	end

	-- Damage clauses
	if rAction.clauses and #rAction.clauses > 0 then
		for _,vClause in pairs(rAction.clauses) do
			-- Dice and mod
			local aDice, nMod = Utilities.getDiceAndMod(rActor, vClause.roll, vClause.stat, vClause.statmult, vClause.height, rAction.level)
			rRoll.nMod = rRoll.nMod + nMod;
			for _,v in pairs(aDice) do
				table.insert(rRoll.aDice, v);
			end
			-- Damage types
			local sDamages = vClause.dmgtype:lower();
			for _,v in pairs(DataCommon.dmgtypes) do
				if sDamages:find(v) then
					if rRoll.sDamageTypes ~= "" then
						rRoll.sDamageTypes = rRoll.sDamageTypes .. ", ";
					end
					rRoll.sDamageTypes = rRoll.sDamageTypes .. v;
				end
			end
		end
	end

	-- Handle self-targeting
	if rAction.sTargeting and rAction.sTargeting == "self" then
		rRoll.bSelfTarget = true;
		rRoll.nSelfTarget = 1;
	end

	-- Description
	rRoll.sDesc = Interface.getString("modifier_label_damage"):upper();
	if rAction.order and rAction.order > 1 then
		rRoll.sDesc = rRoll.sDesc .. " #" .. rAction.order;
	end
	rRoll.sDesc = rRoll.sDesc .. "\n\n" .. rAction.label:upper();
	if rAction.attackType then
		rRoll.sDesc = rRoll.sDesc .. rAction.attackType;
		rRoll.attackType = rAction.attackType;
	end

	rRoll.sDesc = rRoll.sDesc .. "\n- " .. DiceManager.convertDiceToString(rRoll.aDice, rRoll.nMod, true) .. " " .. rRoll.sDamageTypes;

	return rRoll;
end

function performRoll(draginfo, rActor, rAction)
	local rRoll = getRoll(rActor, rAction);

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function modDamage(rSource, rTarget, rRoll)
	-- Set up
	local aAddDice = {};
	local nAddMod = 0;

	-- Build attack type filter
	local aAttackFilter = StringManager.split(rRoll.sDamageTypes, ",", true);
	if rRoll.attackType then
		table.insert(aAttackFilter, rRoll.attackType);
	else
		table.insert(aAttackFilter, DataCommon.type_melee);
	end

	-- Add attack damage excess
	local nExcess = 0;
	if not rRoll.nSelfTarget then
		nExcess = ActionAttack.getHitExcess(rSource, rTarget);
		ActionAttack.clearHitExcess(rSource, rTarget);
		rRoll.nMod = rRoll.nMod + nExcess;
		if nExcess ~= 0 then
			rRoll.sDesc = rRoll.sDesc .. Interface.getString("damage_chat_lastattack") .. tostring(nExcess);
		end
	end

	-- Apply damage type modifiers
	if not rRoll.nSelfTarget then
		local aEffects = EffectManagerSS.getEffectsBonusByType(rSource, DataCommon.keyword_states["DMGTYPE"], false, {}, rTarget, false, nil, true);

		local aAddTypes = {};
		for _,v in pairs(aEffects) do
			for _,v2 in pairs(v.remainder) do
				local aSplitTypes = StringManager.split(v2, ",", true);
				for _,v3 in pairs(aSplitTypes) do
					v3 = v3:lower();
					if Utilities.inArray(DataCommon.dmgtypes, v3) and not Utilities.inArray(aAttackFilter, v3) then
						table.insert(aAttackFilter, v3);
						table.insert(aAddTypes, v3);
						rRoll.sDamageTypes = rRoll.sDamageTypes .. "," .. v3
					end
				end
			end
		end
		if #aAddTypes > 0 then
			rRoll.sDesc = rRoll.sDesc .. Interface.getString("damage_chat_extratypes");
			for k,v in pairs(aAddTypes) do
				if k > 1 then
					rRoll.sDesc = rRoll.sDesc .. ", ";
				end
				rRoll.sDesc = rRoll.sDesc .. v;
			end
		end
	end

	-- Apply damage bonus
	if not rRoll.nSelfTarget then
		local aEffects = EffectManagerSS.getEffectsBonusByType(rSource, DataCommon.keyword_states["DMG"], true, aAttackFilter, rTarget);
		local aEffectDice = {};
		local nEffectMod = 0;
		for _,v in pairs(aEffects) do
			for _,vDie in ipairs(v.dice) do
				table.insert(aEffectDice, vDie);
				local sCat = vDie:match("d(%d+)");
				if vDie:sub(1,1) == "-" then
					table.insert(rRoll.aDice, "-p" .. sCat);
				else
					table.insert(rRoll.aDice, "p" .. sCat);
				end
			end

			nEffectMod = nEffectMod + v.mod;
			rRoll.nMod = rRoll.nMod + v.mod;
		end

		local sMod = StringManager.convertDiceToString(aEffectDice, nEffectMod, true);
		if sMod ~= "" then
			rRoll.sDesc = rRoll.sDesc .. Interface.getString("damage_chat_extradmg") .. sMod;
		end

	end

	-- Minimum damage category
	if not rRoll.nSelfTarget then
		local nMinCat = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["DMGMINCAT"]}, true, aAttackFilter);
		if nMinCat > 0 then
			local nCat = 0;
			for _,vDie in ipairs(rRoll.aDice) do
				nCat = nCat + vDie:match("d(%d+)");
			end
			if nCat < nMinCat then
				rRoll.aDice = {"d" .. tostring(nMinCat)};
				rRoll.sDesc = rRoll.sDesc .. Interface.getString("damage_chat_mincat") .. tostring(nMinCat);
			end
		end
	end

	-- Reroll
	if not rRoll.nSelfTarget then
		local nExtra= EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["REROLL"]}, true, aAttackFilter);
		rRoll.nReroll = rRoll.nReroll + nExtra;
		if rRoll.nReroll > 0 then
			rRoll.sDesc = rRoll.sDesc .. Interface.getString("damage_chat_reroll") .. tostring(rRoll.nReroll);
		end
	else
		rRoll.nReroll = 0;
	end

	-- Target suffers extra damage
	if not rRoll.nSelfTarget then
		local aEffects = EffectManagerSS.getEffectsBonusByType(rTarget, DataCommon.keyword_states["GRANTDMG"], true, aAttackFilter, aSource);
		local aEffectDice = {};
		local nEffectMod = 0;
		for _,v in pairs(aEffects) do
			for _,vDie in ipairs(v.dice) do
				table.insert(aEffectDice, vDie);
				local nCat = vDie:match("d(%d-)");
				if vDie:sub(1,1) == "-" then
					table.insert(rRoll.aDice, "-p" .. nCat);
				else
					table.insert(rRoll.aDice, "p" .. nCat);
				end
			end

			nEffectMod = nEffectMod + v.mod;
			rRoll.nMod = rRoll.nMod + v.mod;
		end

		local sMod = StringManager.convertDiceToString(aEffectDice, nEffectMod, true);
		if sMod ~= "" then
			rRoll.sDesc = rRoll.sDesc .. Interface.getString("damage_chat_targetdmg") .. sMod;
		end

	end

	-- Apply desktop modifiers
	ActionsManager2.encodeDesktopMods(rRoll);
end


function onDamage(rSource, rTarget, rRoll)
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	-- createDamageMessage(rSource, rTarget, rRoll);
	rMessage.font = "chat";
	rMessage.mode = "chat_damage";
	rMessage.text = rRoll.sDesc;

	-- Send the chat message
	local bShowMsg = not (rRoll.nAvoidNotifyAction == 1);
	if bShowMsg and rTarget and rTarget.nOrder and rTarget.nOrder ~= 1 then
		bShowMsg = false;
	end
	if bShowMsg then
		-- Comm.deliverChatMessage(rMessage);
		MessageManager.onRollMessage(rMessage);
	end

	-- Set the action info
	local rAction = {};
	rAction.bSecret = rRoll.bTower;
	-- rAction.sText = string.gsub(rRoll.sDesc, " %[MOD:[^]]*%]", "");
	rAction.nTotal = ActionsManager.total(rRoll);
	rAction.nIncorporeal = rRoll.nIncorporeal;
	rAction.sDamageTypes = rRoll.sDamageTypes;

	-- Reduce piercing due to distance
	local nRedPierc = 0;
	if rRoll.attackType == DataCommon.type_thrown or rRoll.attackType == DataCommon.type_ranged then
		local nDist = CombatManager2.getTokenDistance(rSource, rTarget);
		local nRanges = math.ceil(nDist / tonumber(rRoll.range));
		nRedPierc = math.max(0, nRanges - 1);
	end
	rAction.nPiercing = math.max(0, (tonumber(rRoll.nPiercing) or "0") - nRedPierc);
	rAction.nSelfTarget = rRoll.nSelfTarget;

	-- Apply damage to the PC or CT entry referenced
	notifyApplyDamage(rSource, rTarget, rAction);
end

--
-- UTILITY FUNCTIONS
--

function encodeDamageTypes(rRoll)
	for _,vClause in ipairs(rRoll.clauses) do
		if vClause.dmgtype and vClause.dmgtype ~= "" then
			local sDice = StringManager.convertDiceToString(vClause.dice, vClause.modifier);
			rRoll.sDesc = rRoll.sDesc .. string.format(" [TYPE: %s (%s)(%s)]", vClause.dmgtype, sDice, table.concat(vClause.reroll or {}, ","));
			-- rRoll.sDesc = rRoll.sDesc .. string.format(" [TYPE: %s (%s)(%s)(%s)(%s)]", vClause.dmgtype, sDice, vClause.stat or "", vClause.statmult or 1, table.concat(vClause.reroll or {}, ","));
		end
	end
end

function decodeDamageTypes(rRoll, bFinal)
	-- Process each type clause in the damage description (INITIAL ROLL)
	local nMainDieIndex = 0;
	local aRerollOutput = {};
	rRoll.clauses = {};
	-- for sDamageType, sDamageDice, sDamageAbility, sDamageAbilityMult, sDamageReroll in string.gmatch(rRoll.sDesc, "%[TYPE: ([^(]*) %(([^)]*)%)%((%w*)%)%((%w*)%)%(([%w,]*)%)%]") do
	for sDamageType, sDamageDice, sDamageReroll in string.gmatch(rRoll.sDesc, "%[TYPE: ([^(]*) %(([^)]*)%)%(([%w,]*)%)%]") do
		local rClause = {};
		rClause.dmgtype = StringManager.trim(sDamageType);
		-- rClause.stat = sDamageAbility;
		-- rClause.statmult = tonumber(sDamageAbilityMult) or 1;
		rClause.dice, rClause.modifier = StringManager.convertStringToDice(sDamageDice);
		rClause.nTotal = rClause.modifier;
		local aReroll = {};
		for sReroll in sDamageReroll:gmatch("%d+") do
			table.insert(aReroll, tonumber(sReroll) or 0);
		end
		if #aReroll > 0 then
			rClause.reroll = aReroll;
		end
		for kDie,vDie in ipairs(rClause.dice) do
			nMainDieIndex = nMainDieIndex + 1;
			if rRoll.aDice[nMainDieIndex] then
				if bFinal and
						rClause.reroll and rClause.reroll[kDie] and
						rRoll.aDice[nMainDieIndex].result and
						(math.abs(rRoll.aDice[nMainDieIndex].result) <= rClause.reroll[kDie]) then
					local nDieSides = tonumber(string.match(rRoll.aDice[nMainDieIndex].type, "[%-%+]?[a-z](%d+)")) or 0;
					if nDieSides > 0 then
						table.insert(aRerollOutput, "D" .. nMainDieIndex .. "=" .. rRoll.aDice[nMainDieIndex].result);
						local nSubtotal = math.random(nDieSides);
						if rRoll.aDice[nMainDieIndex].result < 0 then
							rRoll.aDice[nMainDieIndex].result = -nSubtotal;
						else
							rRoll.aDice[nMainDieIndex].result = nSubtotal;
						end
					end
				end
				rClause.nTotal = rClause.nTotal + (rRoll.aDice[nMainDieIndex].result or 0);
			end
		end

		table.insert(rRoll.clauses, rClause);
	end
	if #aRerollOutput > 0 then
		rRoll.sDesc = rRoll.sDesc .. " [REROLL " .. table.concat(aRerollOutput, ",") .. "]";
	end

	-- Process each type clause in the damage description (DRAG ROLL RESULT)
	local nClauses = #(rRoll.clauses);
	for sDamageType, sDamageDice in string.gmatch(rRoll.sDesc, "%[TYPE: ([^(]*) %(([^)]*)%)]") do
		local sTotal = string.match(sDamageDice, "=(%d+)");
		if sTotal then
			local nTotal = tonumber(sTotal) or 0;

			local rClause = {};
			rClause.dmgtype = StringManager.trim(sDamageType);
			rClause.stat = "";
			rClause.dice = {};
			rClause.modifier = nTotal;
			rClause.nTotal = nTotal;

			table.insert(rRoll.clauses, rClause);
		end
	end

	-- Add untyped clause if no TYPE tag found
	if #(rRoll.clauses) == 0 then
		local rClause = {};
		rClause.dmgtype = "";
		rClause.stat = "";
		rClause.dice = {};
		rClause.modifier = rRoll.nMod;
		rClause.nTotal = rRoll.nMod;
		for _,vDie in ipairs(rRoll.aDice) do
			if type(vDie) == "table" then
				table.insert(rClause.dice, vDie.type);
				rClause.nTotal = rClause.nTotal + (vDie.result or 0);
			else
				table.insert(rClause.dice, vDie);
			end
		end

		table.insert(rRoll.clauses, rClause);
	end

	-- Handle drag results that are halved or doubled
	if #(rRoll.aDice) == 0 then
		local nResultTotal = 0;
		for i = nClauses + 1, #(rRoll.clauses) do
			nResultTotal = rRoll.clauses[i].nTotal;
		end
		if nResultTotal > 0 and nResultTotal ~= rRoll.nMod then
			if math.floor(nResultTotal / 2) == rRoll.nMod then
				for _,vClause in ipairs(rRoll.clauses) do
					vClause.modifier = math.floor(vClause.modifier / 2);
					vClause.nTotal = math.floor(vClause.nTotal / 2);
				end
			elseif nResultTotal * 2 == rRoll.nMod then
				for _,vClause in ipairs(rRoll.clauses) do
					vClause.modifier = 2 * vClause.modifier;
					vClause.nTotal = 2 * vClause.nTotal;
				end
			end
		end
	end

	-- Remove damage type information from roll description
	rRoll.sDesc = string.gsub(rRoll.sDesc, " %[TYPE:[^]]*%]", "");

	if bFinal then
		local nFinalTotal = ActionsManager.total(rRoll);

		-- Handle minimum damage
		if nFinalTotal < 0 and rRoll.aDice and #rRoll.aDice > 0 then
			rRoll.sDesc = rRoll.sDesc .. " [MIN DAMAGE]";
			rRoll.nMod = rRoll.nMod - nFinalTotal;
			nFinalTotal = 0;
		end

		-- Capture any manual modifiers and adjust damage types accordingly
		-- NOTE: Positive values are added to first damage clause, Negative values reduce damage clauses until none remain
		local nClausesTotal = 0;
		for _,vClause in ipairs(rRoll.clauses) do
			nClausesTotal = nClausesTotal + vClause.nTotal;
		end
		if nFinalTotal ~= nClausesTotal then
			local nRemainder = nFinalTotal - nClausesTotal;
			if nRemainder > 0 then
				if #(rRoll.clauses) == 0 then
					table.insert(rRoll.clauses, { dmgtype = "", stat = "", dice = {}, modifier = nRemainder, nTotal = nRemainder})
				else
					rRoll.clauses[1].modifier = rRoll.clauses[1].modifier + nRemainder;
					rRoll.clauses[1].nTotal = rRoll.clauses[1].nTotal + nRemainder;
				end
			else
				for _,vClause in ipairs(rRoll.clauses) do
					if vClause.nTotal >= -nRemainder then
						vClause.modifier = vClause.modifier + nRemainder;
						vClause.nTotal = vClause.nTotal + nRemainder;
						break;
					else
						vClause.modifier = vClause.modifier - vClause.nTotal;
						nRemainder = nRemainder + vClause.nTotal;
						vClause.nTotal = 0;
					end
				end
			end
		end

		-- Collapse damage clauses into smallest set, then add to roll description as text
		local aDamage = getDamageStrings(rRoll.clauses);
		for _, rDamage in ipairs(aDamage) do
			local sDice = StringManager.convertDiceToString(rDamage.aDice, rDamage.nMod);
			local sDmgTypeOutput = rDamage.sType;
			if sDmgTypeOutput == "" then
				sDmgTypeOutput = "untyped";
			end
			rRoll.sDesc = rRoll.sDesc .. string.format(" [TYPE: %s (%s=%d)]", sDmgTypeOutput, sDice, rDamage.nTotal);
		end
	end
end

-- Collapse damage clauses by damage type (in the original order, if possible)
function getDamageStrings(clauses)
	local aOrderedTypes = {};
	local aDmgTypes = {};
	for _,vClause in ipairs(clauses) do
		local rDmgType = aDmgTypes[vClause.dmgtype];
		if not rDmgType then
			rDmgType = {};
			rDmgType.aDice = {};
			rDmgType.nMod = 0;
			rDmgType.nTotal = 0;
			rDmgType.sType = vClause.dmgtype;
			aDmgTypes[vClause.dmgtype] = rDmgType;
			table.insert(aOrderedTypes, rDmgType);
		end

		for _,vDie in ipairs(vClause.dice) do
			table.insert(rDmgType.aDice, vDie);
		end
		rDmgType.nMod = rDmgType.nMod + vClause.modifier;
		rDmgType.nTotal = rDmgType.nTotal + (vClause.nTotal or 0);
	end

	return aOrderedTypes;
end

function getDamageTypesFromString(sDamageTypes)
	local sLower = string.lower(sDamageTypes);
	local aSplit = StringManager.split(sLower, ",", true);

	local aDamageTypes = {};
	for _,v in ipairs(aSplit) do
		if StringManager.contains(DataCommon.dmgtypes, v) then
			table.insert(aDamageTypes, v);
		end
	end

	return aDamageTypes;
end

--
-- DAMAGE APPLICATION
--

function getReductionType(rSource, rTarget, sEffectType)
	local aEffects = EffectManagerSS.getEffectsByType(rTarget, sEffectType, {}, rSource);

	local aFinal = {};
	for _,v in pairs(aEffects) do
		local rReduction = {};

		rReduction.mod = v.mod;
		rReduction.aNegatives = {};
		for _,vType in pairs(v.remainder) do
			if #vType > 1 and ((vType:sub(1,1) == "!") or (vType:sub(1,1) == "~")) then
				if StringManager.contains(DataCommon.dmgtypes, vType:sub(2)) then
					table.insert(rReduction.aNegatives, vType:sub(2));
				end
			end
		end

		for _,vType in pairs(v.remainder) do
			if vType ~= "untyped" and vType ~= "" and vType:sub(1,1) ~= "!" and vType:sub(1,1) ~= "~" then
				if StringManager.contains(DataCommon.dmgtypes, vType) or vType == "all" then
					aFinal[vType] = rReduction;
				end
			end
		end
	end

	return aFinal;
end

function checkReductionTypeHelper(rMatch, aDmgType)
	if not rMatch or (rMatch.mod ~= 0) then
		return false;
	end
	if #(rMatch.aNegatives) > 0 then
		local bMatchNegative = false;
		for _,vNeg in pairs(rMatch.aNegatives) do
			if StringManager.contains(aDmgType, vNeg) then
				bMatchNegative = true;
				break;
			end
		end
		return not bMatchNegative;
	end
	return true;
end

function checkReductionType(aReduction, aDmgType)
	for _,sDmgType in pairs(aDmgType) do
		if checkReductionTypeHelper(aReduction[sDmgType], aDmgType) or checkReductionTypeHelper(aReduction["all"], aDmgType) then
			return true;
		end
	end

	return false;
end

function checkNumericalReductionTypeHelper(rMatch, aDmgType, nLimit)
	if not rMatch or (rMatch.mod == 0) then
		return 0;
	end

	local bMatch = false;
	if #rMatch.aNegatives > 0 then
		local bMatchNegative = false;
		for _,vNeg in pairs(rMatch.aNegatives) do
			if StringManager.contains(aDmgType, vNeg) then
				bMatchNegative = true;
				break;
			end
		end
		if not bMatchNegative then
			bMatch = true;
		end
	else
		bMatch = true;
	end

	local nAdjust = 0;
	if bMatch then
		nAdjust = rMatch.mod - (rMatch.nApplied or 0);
		if nLimit then
			nAdjust = math.min(nAdjust, nLimit);
		end
		rMatch.nApplied = (rMatch.nApplied or 0) + nAdjust;
	end

	return nAdjust;
end

function checkNumericalReductionType(aReduction, aDmgType, nLimit)
	local nAdjust = 0;

	for _,sDmgType in pairs(aDmgType) do
		if nLimit then
			local nSpecificAdjust = checkNumericalReductionTypeHelper(aReduction[sDmgType], aDmgType, nLimit);
			nAdjust = nAdjust + nSpecificAdjust;
			local nGlobalAdjust = checkNumericalReductionTypeHelper(aReduction["all"], aDmgType, nLimit - nSpecificAdjust);
			nAdjust = nAdjust + nGlobalAdjust;
		else
			nAdjust = nAdjust + checkNumericalReductionTypeHelper(aReduction[sDmgType], aDmgType);
			nAdjust = nAdjust + checkNumericalReductionTypeHelper(aReduction["all"], aDmgType);
		end
	end

	return nAdjust;
end

function divideDamageByType(nTotal, sDamages)
	-- Divide string in different damage types
	local aBasic = {};
	local aMod = {};
	local bStat = true;

	for _,v in pairs(DataCommon.dmgtypes_basic) do
		if sDamages:find(v) then
			table.insert(aBasic, v);
		end
	end
	for _,v in pairs(DataCommon.dmgtypes_special) do
		if sDamages:find(v) then
			table.insert(aBasic, v);
		end
	end
	for _,v in pairs(DataCommon.dmgtypes_material) do
		if sDamages:find(v) then
			table.insert(aMod, v);
		end
	end
	for _,v in pairs(DataCommon.dmgtypes_modifier) do
		if sDamages:find(v) then
			table.insert(aMod, v);
		end
	end
	for _,v in pairs(DataCommon.abilities_translation_inv) do
		if sDamages:find(v) then
			table.insert(aBasic, v);
		end
	end
	if sDamages:find(DataCommon.ability_gift_trans) then
		table.insert(aBasic, DataCommon.ability_gift_trans);
	end

	-- Concatenate
	if #aBasic == 0 then
		return {{total = nTotal, types = aMod}};
	else
		nTotal = math.ceil(nTotal / #aBasic)
		local aFinal = {};
		for _,v in pairs(aBasic) do
			local aAdd = {v};
			for _,vv in pairs(aMod) do
				table.insert(aAdd, vv);
			end
			if Utilities.inArray(DataCommon.abilities_translation_inv, v) or v == DataCommon.ability_gift_trans then
				table.insert(aAdd, "STAT");
			end
			table.insert(aFinal, {total = nTotal, types = aAdd});
		end
		return aFinal;
	end
end

function getDamagePiercing(rSource, rTarget, nPiercing)
	local nMagicPiercing = EffectManagerSS.getEffectsBonus(rSource, {DataCommon.keyword_states["MPIERCING"]}, true, rTarget) + ActorManager2.getAptitudeCategory(rSource, DataCommon.aptitude["magic_piercing"]);
	local nStatePiercing = EffectManagerSS.getEffectsBonus(rSource, {DataCommon.keyword_states["PIERCING"]}, true, rTarget);
	nPiercing = nPiercing + nStatePiercing;
	return nPiercing, nMagicPiercing;
end

function getDamageAdjust(rSource, rTarget, rDamage, aNotifications, nPiercing, nAvoidProt)

	local nPiercing, nMagicPiercing = getDamagePiercing(rSource, rTarget, nPiercing);

	local nProtTotal = 0;
	local nResistTotal = 0;
	local nInmuneTotal = 0;
	local nAbsorbTotal = 0;
	local nVulnTotal = 0;
	local bModSave = false;
	local bModDamage = false;

	-- Saves to reduce damage
	local sSaveStatus = ActionSkill.getSaveStatus(rTarget, rSource);
	ActionSkill.clearSaveStatus(rTarget, rSource);

	-- Iterate through damage type entries in order to adjust them
	for _, aDamage in pairs(rDamage) do
		-- Unavoidable damage is not reduced by any means
		if not Utilities.inArray(aDamage.types, DataCommon.dmgtypes_modifier["unavoidable"]) then

			-- Save acts differently depending on the type of damage
			if sSaveStatus ~= "failure" and Utilities.inArray(aDamage.types, DataCommon.dmgtypes_special["madness"]) or Utilities.inArray(aDamage.types, DataCommon.dmgtypes_special["spirit"]) then
				if sSaveStatus ~= "critical failure" then
					aDamage.total = aDamage.total + 1;
					table.insert(aNotifications, Interface.getString("damage_chat_absorb"));
				else
					table.insert(aDamage.types, DataCommon.dmgtypes_modifier["nonlethal"]);
					if sSaveStatus ~= "critical success" then
						aDamage.total = math.floor(0.5 * aDamage.total);
						table.insert(aNotifications, Interface.getString("damage_chat_savecritsuccess"));
					else
						table.insert(aNotifications, Interface.getString("damage_chat_savesuccess"));
					end
				end
			elseif sSaveStatus ~= "failure" then
				local nMultSave = DataCommon.damage_save_mult[sSaveStatus];
				aDamage.total = math.floor(aDamage.total * nMultSave);
				table.insert(aNotifications, string.format(Interface.getString("damage_chat_savemult"), tostring(nMultSave)));
			end


			-- Precision and self-damage is not reduced by protection
			if not nAvoidProt and not Utilities.inArray(aDamage.types,DataCommon.dmgtypes_modifier["precision"]) then
				local nProt, nExtraProt, nMast = ActorManager2.getProtectionValue(rTarget, aDamage.types, nPiercing, nMagicPiercing);
				if OptionsManager.isOption("MinDamage", "yes") then
					
					nProt = math.min(math.ceil(aDamage.total * (1 - DataCommon.armor_mastery_mindamage[nMast])) ,nProt);
				end
				aDamage.total = math.max(0, aDamage.total - nProt - nExtraProt);

				nProtTotal = nProtTotal + nProt + nExtraProt;
			end

			-- Get individual damage types for each damage clause
			local nSens, nInmune, nAbsorb, nMult = 0, 0, 0, 1;
			if not nAvoidProt then
				nSens, nInmune, nAbsorb, nMult, _ = ActorManager2.getSensitivity(rTarget, aDamage.types, rTarget, true);
			end
			-- Notifications info
			if nAbsorb > 0 then
				nAbsorbTotal = nAbsorbTotal - math.floor(aDamage.total * nMult);
			elseif nInmune > 0 then
				nInmuneTotal = nInmuneTotal + aDamage.total;
			elseif nSens > 0 then
				nResistTotal = nResistTotal + aDamage.total - math.floor(aDamage.total * nMult);
			elseif nSens < 0 then
				nVulnTotal = nVulnTotal + math.floor(aDamage.total * nMult) - aDamage.total;
			end
			-- Apply
			aDamage.total = math.floor(aDamage.total * nMult);

			
		end
	end
	-- Notifications
	if nProtTotal > 0 then
		table.insert(aNotifications, string.format(Interface.getString("damage_chat_prot"), nProtTotal));
	end
	if nResistTotal > 0 then
		table.insert(aNotifications, string.format(Interface.getString("damage_chat_resist"), nResistTotal));
	end
	if nInmuneTotal > 0 then
		table.insert(aNotifications, string.format(Interface.getString("damage_chat_inmune"), nInmuneTotal));
	end
	if nAbsorbTotal > 0 then
		table.insert(aNotifications, string.format(Interface.getString("damage_chat_absorb"), nAbsorbTotal));
	end
	if nVulnTotal > 0 then
		table.insert(aNotifications, string.format(Interface.getString("damage_chat_vuln"), nVulnTotal));
	end

	-- Results
	return rDamage, aNotifications
end




function decodeDamageText(nDamage, sDamageDesc, sSubtype, sHealStat)
	local rDamageOutput = {};

	if sSubtype == "stat" or sSubtype == "magicstat" then
		rDamageOutput.sType = sSubtype;
		rDamageOutput.sTypeOutput = Interface.getString("power_label_heal_stat") .. " (" .. sHealStat .. ")";
		rDamageOutput.sVal = string.format("%01d", nDamage);
		rDamageOutput.nVal = nDamage;
		if sSubtype == "magicstat" then
			rDamageOutput.aDamageTypes = {["magic"] = nDamage};
		end
		rDamageOutput.sHealStat = sHealStat;

	elseif ActionHeal.isHealName(sSubtype) then
		rDamageOutput.sType = sSubtype;
		rDamageOutput.sTypeOutput = ActionHeal.getDescriptionName(sSubtype);
		rDamageOutput.sVal = string.format("%01d", nDamage);
		rDamageOutput.nVal = nDamage;
		if ActionHeal.isHealMagicName(sSubtype) then
			rDamageOutput.aDamageTypes = {["magic"] = nDamage};
		else
			rDamageOutput.aDamageTypes = {["unavoidable"] = nDamage};
		end


	elseif nDamage < 0 then
		rDamageOutput.sType = "trueheal";
		rDamageOutput.sTypeOutput = Interface.getString("power_label_heal_trueheal");
		rDamageOutput.sVal = string.format("%01d", (0 - nDamage));
		rDamageOutput.nVal = 0 - nDamage;
		rDamageOutput.aDamageTypes = {["unavoidable"] = nDamage};

	else
		rDamageOutput.sType = "damage";
		rDamageOutput.sTypeOutput = "Damage";
		rDamageOutput.sVal = string.format("%01d", nDamage);
		rDamageOutput.nVal = nDamage;

		-- Determine critical
		rDamageOutput.bCritical = string.match(sDamageDesc, "%[CRITICAL%]");

		-- Determine range
		rDamageOutput.sRange = string.match(sDamageDesc, "%[DAMAGE %((%w)%)%]") or "";
		rDamageOutput.aDamageFilter = {};
		if rDamageOutput.sRange == "M" then
			table.insert(rDamageOutput.aDamageFilter, "melee");
		elseif rDamageOutput.sRange == "R" then
			table.insert(rDamageOutput.aDamageFilter, "ranged");
		end

		-- Determine damage energy types
		local nDamageRemaining = nDamage;
		rDamageOutput.aDamageTypes = {};
		for sDamageType, sDamageDice, sDamageSubTotal in string.gmatch(sDamageDesc, "%[TYPE: ([^(]*) %(([%d%+%-dD]+)%=(%d+)%)%]") do
			local nDamageSubTotal = (tonumber(sDamageSubTotal) or 0);
			rDamageOutput.aDamageTypes[sDamageType] = nDamageSubTotal + (rDamageOutput.aDamageTypes[sDamageType] or 0);
			if not rDamageOutput.sFirstDamageType then
				rDamageOutput.sFirstDamageType = sDamageType;
			end

			nDamageRemaining = nDamageRemaining - nDamageSubTotal;
		end
		if nDamageRemaining > 0 then
			rDamageOutput.aDamageTypes[""] = nDamageRemaining;
		elseif nDamageRemaining < 0 then
			ChatManager.SystemMessage("Total mismatch in damage type totals");
		end
	end

	return rDamageOutput;
end

function applyDamage(rSource, rTarget, rAction)
	local bSecret = rAction.bSecret;

	-- Get characters
	local sTargetType, nodeTarget = ActorManager.getTypeAndNode(rTarget);
	if sTargetType ~= "pc" and sTargetType ~= "ct" then
		return;
	end

	-- Other
	-- local nodeTargetCT = ActorManager.getCTNode(rTarget);

	-- Check if it is auto damage
	local bAutodamage = rAction.bSelfTarget;
	if not bAutodamage and rSource and rTarget then
		bAutodamage = rSource.sCTNode == rTarget.sCTNode;
	end

	-- Create array of damage types
	rDamage = divideDamageByType(rAction.nTotal or 0, rAction.sDamageTypes or "");

	-- Apply damage
	for _, aDamage in pairs(rDamage) do

		local bNonlethal = Utilities.inArray(aDamage.types, DataCommon.dmgtypes_modifier["nonlethal"])

		-- Barrier
		local nBarrier = DB.getValue(nodeTarget, "health.barrier", 0);
		if not bAutodamage and nBarrier > 0 then
			-- Get data
			local nMult = DataCommon.damage_barrier["lethal"];
			local nodeTarget, rTarget = ActorManager2.getActorAndNode(rTarget);
			local nPiercing, nMagicPiercing = getDamagePiercing(rSource, rTarget, rAction.nPiercing or 0);
			nPiercing = nPiercing + nMagicPiercing;
			nBarrier = nBarrier - nPiercing;
			-- Switch damage multiplier
			if Utilities.inArray(aDamage.types, DataCommon.dmgtypes_special["essence"]) then
				nMult = DataCommon.damage_barrier["essence"];
			elseif Utilities.inArray(aDamage.types, DataCommon.dmgtypes_special["madness"]) then
				if bNonlethal then
					nMult = DataCommon.damage_barrier["mental_nonlethal"];
				else
					nMult = DataCommon.damage_barrier["mental"];
				end
			elseif Utilities.inArray(aDamage.types, DataCommon.dmgtypes_special["spirit"]) then
				if bNonlethal then
					nMult = DataCommon.damage_barrier["spirit_nonlethal"];
				else
					nMult = DataCommon.damage_barrier["spirit"];
				end
			elseif Utilities.inArray(aDamage.types, "STAT") then
				nMult = DataCommon.damage_barrier["stat"];
			elseif bNonlethal then
				nMult = DataCommon.damage_barrier["nonlethal"];
			end
			-- Apply reduction
			local nDamage = math.ceil(aDamage.total * nMult);
			local nReduced = math.floor(nBarrier/nMult)
			if nDamage < nBarrier then
				DB.setValue(nodeTarget, "health.barrier", "number", nBarrier - nDamage);
				aDamage.total = 0;
				messageDamage(rSource, rTarget, "roll_barrier", Interface.getString("char_label_barrier"):lower(), nDamage + nPiercing, {});
				do break; end
			else
				DB.setValue(nodeTarget, "health.barrier", "number", 0);
				aDamage.total = aDamage.total - nReduced;
				nDamage = nBarrier;
				messageDamage(rSource, rTarget, "roll_barrier", Interface.getString("char_label_barrier"):lower(), nDamage + nPiercing, {});
			end

		end

		-- Handle damage reduction
		local aNotifications = rAction.aNotifications or {};
		local aDamage, aNotifications = getDamageAdjust(rSource, rTarget, {aDamage}, aNotifications, rAction.nPiercing or 0, bAutodamage);
		aDamage = aDamage[1];

		-- Switch damage type
		if Utilities.inArray(aDamage.types, DataCommon.dmgtypes_special["essence"]) then
			applyDamageEssence(rTarget, aDamage.total, aNotifications);
		elseif Utilities.inArray(aDamage.types, DataCommon.dmgtypes_special["madness"]) then
			applyDamageSanity(rTarget, aDamage.total, aNotifications, bNonlethal, bAutodamage);
		elseif Utilities.inArray(aDamage.types, DataCommon.dmgtypes_special["spirit"]) then
			applyDamageSpirit(rTarget, aDamage.total, aNotifications, bNonlethal, bAutodamage);
		elseif Utilities.inArray(aDamage.types, "STAT") then
			applyDamageStat(rTarget, aDamage.total, aNotifications, bNonlethal, aDamage.types[1]);
		else
			applyDamageRegular(rTarget, aDamage.total, aNotifications, bNonlethal, aDamage.types, bAutodamage);
		end
	end
end


function applyStatEffects(rTarget, nodeTargetCT, sStat)
	sStat = DataCommon.abilities_translation[sStat];
	if sStat == "strength" then
		if not EffectManagerSS.hasEffect(rTarget, "Prone") then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = "Prone", nDuration = 0 }, true);
		end
	elseif (sStat == "constitution") or (sStat == "intelligence") then
		if not EffectManagerSS.hasEffect(rTarget, "Unconscious") then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = "Unconscious", nDuration = 0 }, true);
		end
	elseif sStat == "perception" then
		if not EffectManagerSS.hasEffect(rTarget, "Blinded") then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = "Blinded", nDuration = 0 }, true);
		end
		if not EffectManagerSS.hasEffect(rTarget, "Deafened") then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = "Deafened", nDuration = 0 }, true);
		end
	elseif sStat == "agility" then
		if not EffectManagerSS.hasEffect(rTarget, "Inmovilized") then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = "Inmovilized", nDuration = 0 }, true);
		end
	elseif sStat == "dexterity" then
		EffectManager.addEffect("", "", nodeTargetCT, { sName = "ATK: -99; DEF: -99 parry;", nDuration = 0 }, true);
	elseif sStat == "willpower" then
		if not EffectManagerSS.hasEffect(rTarget, "Unwilled") then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = "Unwilled", nDuration = 0 }, true);
		end
	elseif sStat == "charisma" then
		if not EffectManagerSS.hasEffect(rTarget, "Charmed; Unable to speak or hear;") then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = "Charmed; Unable to speak or hear;", nDuration = 0 }, true);
		end
	end
end

function makeConcentrationChecks(rActor, nDif)
	-- Look for the concentration effects
	local aIDs, aDif = EffectManagerSS.findConcentrationEffects(rActor);
	local nBonus = 0;
	local nAdv = 0;

	if #aIDs == 0 then
		return false;
	end

	-- Roll
	-- local nodeSkill = ActorManager2.getSaveNode(rActor, Interface.getString("skill_value_concentration"));
	-- nBonus, nAdv, _, _ = ActorManager2.getCheck(rActor, DataCommon.abilities_translation["willpower"], nodeSkill);
	nBonus, nAdv, _, _ = ActorManager2.getCheck(rActor, DataCommon.abilities_translation["willpower"], Interface.getString("skill_value_concentration"));
	local nResult = CombatManager2.simpleRoll("dT", nAdv) + nBonus;

	-- Loop effects
	for k, nID in pairs(aIDs) do
		-- Check difficulty
		local nDifThis = math.max(nDif or 0, aDif[k]);
		if nResult < nDifThis then
			EffectManagerSS.removeAllEffectsByNameAndOwner(rActor, nID, true, true);
		end
	end
	return true;
end

function messageDamage(rSource, rTarget, sIcon, sDamage, nDamage, aNotifications)
	local msgShort = {font = "msgfont", icon=sIcon};
	local msgLong = {font = "msgfont", icon=sIcon};
	if ActorManager.getDisplayName(rSource) == ActorManager.getDisplayName(rTarget) then
		msgShort.mode = "chat_unknown";
		msgLong.mode = "chat_unknown";
	else
		msgShort.mode = "chat_miss";
		msgLong.mode = "chat_miss";
	end
	msgShort.text = tostring(nDamage) .. " " .. sDamage;
	msgLong.text = tostring(nDamage) .. " " .. sDamage;

	if rTarget then
		msgShort.text = msgShort.text .. " ->" .. " [to " .. ActorManager.getDisplayName(rTarget) .. "]";
		msgLong.text = msgLong.text .. " ->" .. " [to " .. ActorManager.getDisplayName(rTarget) .. "]";
	end

	if #aNotifications > 0 then
		msgLong.text = msgLong.text .. " " .. table.concat(aNotifications, " ");
	end

	ActionsManager.outputResult(bSecret, rSource, rTarget, msgLong, msgShort);
end


--
-- TRACK DAMAGE STATE
--

local aDamageState = {};

function applyDamageState(rSource, rTarget, sAttack, sState)
	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYDMGSTATE;

	msgOOB.sSourceNode = ActorManager.getCTNodeName(rSource);
	msgOOB.sTargetNode = ActorManager.getCTNodeName(rTarget);

	msgOOB.sAttack = sAttack;
	msgOOB.sState = sState;

	Comm.deliverOOBMessage(msgOOB, "");
end

function handleApplyDamageState(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);

	if User.isHost() then
		setDamageState(rSource, rTarget, msgOOB.sAttack, msgOOB.sState);
	end
end

function setDamageState(rSource, rTarget, sAttack, sState)
	if not User.isHost() then
		applyDamageState(rSource, rTarget, sAttack, sState);
		return;
	end

	local sSourceCT = ActorManager.getCTNodeName(rSource);
	local sTargetCT = ActorManager.getCTNodeName(rTarget);
	if sSourceCT == "" or sTargetCT == "" then
		return;
	end

	if not aDamageState[sSourceCT] then
		aDamageState[sSourceCT] = {};
	end
	if not aDamageState[sSourceCT][sAttack] then
		aDamageState[sSourceCT][sAttack] = {};
	end
	if not aDamageState[sSourceCT][sAttack][sTargetCT] then
		aDamageState[sSourceCT][sAttack][sTargetCT] = {};
	end
	aDamageState[sSourceCT][sAttack][sTargetCT] = sState;
end

function getDamageState(rSource, rTarget, sAttack)
	local sSourceCT = ActorManager.getCTNodeName(rSource);
	local sTargetCT = ActorManager.getCTNodeName(rTarget);
	if sSourceCT == "" or sTargetCT == "" then
		return "";
	end

	if not aDamageState[sSourceCT] then
		return "";
	end
	if not aDamageState[sSourceCT][sAttack] then
		return "";
	end
	if not aDamageState[sSourceCT][sAttack][sTargetCT] then
		return "";
	end

	local sState = aDamageState[sSourceCT][sAttack][sTargetCT];
	aDamageState[sSourceCT][sAttack][sTargetCT] = nil;
	return sState;
end

-------------------------------------
-- APPLY DAMAGE INDIVIDUALLY
-------------------------------------

function applyDamageEssence(rTarget, nDamage, aNotifications)
	-- Get data
	local nodeTarget, rTarget = ActorManager2.getActorAndNode(rTarget);
	local nTotal = DB.getValue(nodeTarget, "health.essence.max", 0);
	local nDamageOld = DB.getValue(nodeTarget, "health.essence.damage", 0);
	local nEquip = DB.getValue(nodeTarget, "encumbrance.essence", 0);
	local nMax = nTotal - nDamageOld - nEquip;

	-- Apply
	if nDamage == nMax then
		table.insert(aNotifications, Interface.getString("damage_chat_maxessence"));
	elseif nDamage > nMax then
		nDamage = nMax;
		table.insert(aNotifications, Interface.getString("damage_chat_notenoughessence"));
	end
	DB.setValue(nodeTarget, "health.essence.damage", "number", nDamageOld + nDamage);

	-- Message
	messageDamage(rSource, rTarget, "roll_damage_special", DataCommon.dmgtypes_special["essence"], nDamage, aNotifications);
end




function applyDamageSanity(rTarget, nDamage, aNotifications, bNonlethal, bIsCost)
	-- Get data
	local nodeTarget, rTarget = ActorManager2.getActorAndNode(rTarget);
	local nodeTargetCT = ActorManager.getCTNode(rTarget);
	local nTotal = DB.getValue(nodeTarget, "health.sanity.max", 0);
	local nDamageTemp = DB.getValue(nodeTarget, "health.sanity.damage", 0);
	local nDamagePerm = DB.getValue(nodeTarget, "health.sanity.damage_perm", 0);
	local nMax = nTotal - nDamageTemp - nDamagePerm;
	local sIcon = "roll_damage_madness";

	-- Apply
	local bDead = false;
	local nDif = 0;
	local sString = "";
	local sMode = "";
	local nDuration = 0;
	if nDamage >= nMax then
		nDamage = nMax;
		bDead = true;
	end
	if bNonlethal then
		DB.setValue(nodeTarget, "health.sanity.damage", "number", nDamageTemp + nDamage);
		nDif = DataCommon.concentration_damage_dif["sanity_base"] + DataCommon.concentration_damage_dif["sanity_mult"] * nDamage;
	else
		DB.setValue(nodeTarget, "health.sanity.damage_perm", "number", nDamagePerm + nDamage);
		nDif = DataCommon.concentration_damage_dif["sanity_perm_base"] + DataCommon.concentration_damage_dif["sanity_perm_mult"] * nDamage;
		sIcon = "roll_damage_perm";

		-- Check mental problems
		local nDifSave = nDamageTemp + nDamagePerm + nDamage + (nDamagePerm - 1) * DataCommon.mentall_illness_dif_mult;
		local nBonus, nAdv = ActorManager2.getCheck(rTarget, DataCommon.abilities_translation_inv["willpower"], Interface.getString("skill_value_concentration"), nil, false, false, "", true);
		nBonus = nBonus + CombatManager2.simpleRoll("dT", nAdv);
		if nBonus < nDifSave - DataCommon.critical_miss then
			sString = Interface.getString("damage_chat_mental_illness_severe");
			sMode = "chat_miss_crit";
			nDuration = math.ceil(nDifSave - DataCommon.critical_miss - nBonus) * DataCommon.turns_per_unit["hour"];
		elseif nBonus < nDifSave then
			sString = Interface.getString("damage_chat_mental_illness_light");
			sMode = "chat_miss";
			nDuration = math.ceil(nDifSave - nBonus) * DataCommon.turns_per_unit["minute"];
		end

	end

	-- Dead status
	if bDead then
		table.insert(aNotifications, Interface.getString("damage_chat_dead"));
		if not EffectManagerSS.hasEffect(rTarget, DataCommon.conditions["Dead"]) then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = DataCommon.conditions["Dead"], nDuration = 0 }, true);
		end
	elseif not bIsCost then
		makeConcentrationChecks(rTarget, nDif);
	end

	-- Message
	messageDamage(rSource, rTarget, sIcon, DataCommon.dmgtypes_special["madness"], nDamage, aNotifications);

	-- Apply mental problems
	if sString ~= "" then
		local msg = {font = "msgfont", icon = "roll_damage_mental_illness", text = sString, mode = sMode};
		Comm.deliverChatMessage(msg);

		table.insert(aNotifications, Interface.getString("damage_chat_maddened"));
		if not EffectManagerSS.hasEffect(rTarget, DataCommon.conditions["Maddened"]) then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = DataCommon.conditions["Maddened"], nDuration = nDuration }, true);
		end
	end
end



function applyDamageSpirit(rTarget, nDamage, aNotifications, bNonlethal, bIsCost)
	-- Get data
	local nodeTarget, rTarget = ActorManager2.getActorAndNode(rTarget);
	local nodeTargetCT = ActorManager.getCTNode(rTarget);
	local nTotal = DB.getValue(nodeTarget, "health.spirit.max", 0);
	local nDamageTemp = DB.getValue(nodeTarget, "health.spirit.damage", 0);
	local nDamagePerm = DB.getValue(nodeTarget, "health.spirit.damage_perm", 0);
	local nMax = nTotal - nDamageTemp - nDamagePerm;
	local sIcon = "roll_damage_spirit";

	-- Apply
	local bDead = false;
	local nDif = 0;
	if nDamage >= nMax then
		nDamage = nMax;
		bDead = true;
	end
	if bNonlethal then
		DB.setValue(nodeTarget, "health.spirit.damage", "number", nDamageTemp + nDamage);
		nDif = DataCommon.concentration_damage_dif["spirit_base"] + DataCommon.concentration_damage_dif["spirit_mult"] * nDamage;
	else
		DB.setValue(nodeTarget, "health.spirit.damage_perm", "number", nDamagePerm + nDamage);
		nDif = DataCommon.concentration_damage_dif["spirit_perm_base"] + DataCommon.concentration_damage_dif["spirit_perm_mult"] * nDamage;
		local sIcon = "roll_damage_spirit_perm";
	end

	-- Dead status
	if bDead then
		table.insert(aNotifications, Interface.getString("damage_chat_dead"));
		if not EffectManagerSS.hasEffect(rTarget, DataCommon.conditions["Dead"]) then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = DataCommon.conditions["Dead"], nDuration = 0 }, true);
		end
	elseif not bIsCost then
		makeConcentrationChecks(rTarget, nDif);
	end

	-- Message
	messageDamage(rSource, rTarget, sIcon, DataCommon.dmgtypes_special["spirit"], nDamage, aNotifications);
end



function applyDamageStat(rTarget, nDamage, aNotifications, bNonlethal, sStat)
	local nodeTarget, rTarget = ActorManager2.getActorAndNode(rTarget);
	-- Separate special cases
	if sStat == "all" then
		-- Damage to all stats
		for _,sStatEach in pairs(DataCommon.abilities) do
			applyDamageStat(rTarget, nDamage, aNotifications, bNonlethal, sStatEach)
		end
	elseif sStat == DataCommon.ability_gift_trans then
		-- Damage to max gift stat
		local nMax = 0;
		local sMax = "";
		for _,sStatEach in pairs(DataCommon.abilities_gift) do
			local nCurrent = ActorManager2.getAbility(rTarget, sStatEach);
			if nCurrent > nMax then
				nMax = nCurrent;
				sMax = sStatEach;
			end
		end
		if sMax ~= "" then
			applyDamageStat(rTarget, nDamage, aNotifications, bNonlethal, sMax)
		else
			local sText = ActorManager.getDisplayName(rTarget) .. Interface.getString("damage_chat_nogiftstat");
			local msgShort = {font = "msgfont", mode="chat_unknown", icon="roll_damage_stat_perm", text=sText};
			ActionsManager.outputResult(false, rSource, rTarget, msgShort, msgShort);
		end
	else
		local sStatTrans = DataCommon.abilities_translation[sStat];
		-- Safety
		if not sStatTrans or not Utilities.inArray(DataCommon.abilities_translation, sStatTrans) then
			Debug.chat(string.format("Stat %s is not a valid ability for damage", sStat or sStatTrans or ""));
		end
		-- Damage to one stat
		local nodeTarget, rTarget = ActorManager2.getActorAndNode(rTarget);
		local nodeTargetCT = ActorManager.getCTNode(rTarget);
		local nCurrent = DB.getValue(nodeTarget, "abilities." .. sStatTrans .. ".damage", 0);
		local nBase = DB.getValue(nodeTarget, "abilities." .. sStatTrans .. ".base", 0);
		local nMax = 0;
		local sIcon = "roll_damage_stat_perm";
		if bNonlethal then
			nMax = 1;
			sIcon = "roll_damage_stat";
		end
		nDamage = math.min(nBase - nMax - nCurrent, nDamage);
		DB.setValue(nodeTarget, "abilities." .. sStatTrans .. ".damage", "number", nCurrent + nDamage);
		local nDif = DataCommon.concentration_damage_dif["stat_base"] + DataCommon.concentration_damage_dif["stat_mult"] * nDamage;

		-- Current value of 1 or 0
		local sEffect = "";
		nCurrent = ActorManager2.getAbility(rTarget, sStat);
		if nCurrent == 1 and nBase > 1 then
			sEffect = DataCommon.condition_reduced_stats[sStatTrans];
		elseif nCurrent <= 0 and nBase > 1 then
			sEffect = DataCommon.conditions_zero_stats[sStatTrans];
		end
		if sEffect ~= "" then
			table.insert(aNotifications, " [" .. sEffect .. "]");
			if not not EffectManagerSS.hasEffect(rTarget, sEffect) then
				EffectManager.addEffect("", "", nodeTargetCT, { sName = sEffect, nDuration = 0 }, true);
			end
		end

		-- Message
		messageDamage(rSource, rTarget, sIcon, sStat, nDamage, aNotifications);

		-- Concentration
		if nDif > 0 then
			makeConcentrationChecks(rTarget, nDif);
		end
	end

end

function applyDamageRegular(rTarget, nDamage, aNotifications, bNonlethal, aTypes, bAutodamage)
	-- Get data
	local nodeTarget, rTarget = ActorManager2.getActorAndNode(rTarget);
	local nodeTargetCT = ActorManager.getCTNode(rTarget);
	local nTotalVit = DB.getValue(nodeTarget, "health.vit.max", 0);
	local nTempVit = DB.getValue(nodeTarget, "health.vit.temp", 0);
	local nWoundsVit = DB.getValue(nodeTarget, "health.vit.damage", 0);
	local nTotalVigor = DB.getValue(nodeTarget, "health.vigor.max", 0);
	local nTempVigor = DB.getValue(nodeTarget, "health.vigor.temp", 0);
	local nWoundsVigor = DB.getValue(nodeTarget, "health.vigor.damage", 0);
	local nCurrentVit = nTotalVit + nTempVit - nWoundsVit;
	local nCurrentVigor = nTotalVigor + nTempVigor - nWoundsVigor;
	local nDif = 0;
	local sDamage = "";
	-- Remember current health status
	local _,sOriginalStatus = ActorHealthManager2.getPercentWounded(rTarget);
	local _,sOriginalStatusVigor = ActorHealthManager2.getPercentTired(rTarget);

	if bAutodamage then
		nTempVit = 0;
		nTempVigor = 0;
	end

	-- Assign damage
	local nDamageVit = 0;
	local nDamageVigor = 0;
	local nDamageVitTemp = 0;
	local nDamageVigorTemp = 0;
	local nDamageTotalVit = 0;
	if bNonlethal then
		nDamageVigor = nDamage;
	else
		nDamageVit = nDamage;
		nDamageTotalVit = nDamage;
	end

	-- Move excess damage
	local bInconscious = false;
	local bDead = false;
	if nDamageVit > nCurrentVit then
		nDamageVigor = nDamageVigor + DataCommon.damage_vit_to_vigor*(nDamageVit - nCurrentVit);
		nDamageVit = nCurrentVit;
		bDead = true;
		if nCurrentVit > 0 then
			applyDamageSanity(rTarget, 1, aNotifications, true, true);
		end
	end
	if nDamageVigor > nCurrentVigor then
		nDamageVit = nDamageVigor - nCurrentVigor;
		nDamageVigor = nCurrentVigor;
		bInconscious = true;
		bDeactivateRegen = true;
		nDamageTotalVit = nDamageVit;
	end
	local bDeactivateRegen = nDamageTotalVit - nTempVit > DB.getValue(nodeTarget, "health.fortitude", 3);
	if nDamageVit > nCurrentVit then
		nDamageVit = nCurrentVit;
		bDead = true;
	end

	-- Apply vit damage
	if nDamageVit > nTempVit then
		nDamageVit = nDamageVit - nTempVit;
		nDamageVitTemp = nTempVit;
		nTempVit = 0;
	else
		nTempVit = nTempVit - nDamageVit;
		nDamageVitTemp  = nDamageVit;
		nDamageVit = 0;
	end
	nWoundsVit = math.max(0, nWoundsVit + nDamageVit);
	nDif = 2 * nDamageVit
	-- Apply vigor damage
	if nDamageVigor > nTempVigor then
		nDamageVigor = nDamageVigor - nTempVigor;
		nDamageVigorTemp = nTempVigor;
		nTempVigor = 0;
	else
		nTempVigor = nTempVigor - nDamageVigor;
		nDamageVigorTemp = nDamageVigor;
		nDamageVigor = 0;
	end
	nWoundsVigor = math.max(0, nWoundsVigor + nDamageVigor);
	nDif = nDif + nDamageVit

	-- Set values
	if not bAutodamage then
		DB.setValue(nodeTarget, "health.vit.max", "number", nTotalVit);
		DB.setValue(nodeTarget, "health.vit.temp", "number", nTempVit);
	end
	DB.setValue(nodeTarget, "health.vit.damage", "number", nWoundsVit);
	DB.setValue(nodeTarget, "health.vigor.max", "number", nTotalVigor);
	DB.setValue(nodeTarget, "health.vigor.temp", "number", nTempVigor);
	DB.setValue(nodeTarget, "health.vigor.damage", "number", nWoundsVigor);

	-- Check for status change
	local bShowStatus = false;
	if ActorManager.getFaction(rTarget) == "friend" then
		bShowStatus = not OptionsManager.isOption("SHPC", "off");
	else
		bShowStatus = not OptionsManager.isOption("SHNPC", "off");
	end
	if bShowStatus then
		local _,sNewStatus = ActorHealthManager2.getPercentWounded(rTarget);
		if sOriginalStatus ~= sNewStatus then
			table.insert(aNotifications, "[" .. Interface.getString("combat_tag_status") .. ": " .. sNewStatus .. "]");
		end		
		local _,sNewStatus = ActorHealthManager2.getPercentTired(rTarget);
		if sOriginalStatusVigor ~= sNewStatus then
			table.insert(aNotifications, "[" .. Interface.getString("combat_tag_status") .. ": " .. sNewStatus .. "]");
		end
	end
	CharManager.updateEncumbrance(nodeTarget);

	-- Messages
	local sTypes = table.concat(aTypes, ", ");
	if bNonlethal then
		-- Start notifying vigor
		if nDamageVigorTemp > 0 then
			messageDamage(rSource, rTarget, "roll_damage_vigor_temp", sTypes, nDamageVigorTemp, aNotifications);
		end
		if nDamageVigor > 0 or (nDamageVitTemp == 0 and nDamageVit == 0 and nDamageVigorTemp== 0) then
			messageDamage(rSource, rTarget, "roll_damage_vigor", sTypes, nDamageVigor, aNotifications);
		end
	end
	-- Vitality
	if nDamageVitTemp > 0 then
		messageDamage(rSource, rTarget, "roll_damage_temp", sTypes, nDamageVitTemp, aNotifications);
	end
	if nDamageVit > 0 or (nDamageVigorTemp == 0 and nDamageVigor == 0 and nDamageVitTemp == 0) then
		messageDamage(rSource, rTarget, "roll_damage", sTypes, nDamageVit, aNotifications);
	end
	if not bNonlethal then
		-- Vigor again
		if nDamageVigorTemp > 0 then
			messageDamage(rSource, rTarget, "roll_damage_vigor_temp", sTypes, nDamageVigorTemp, aNotifications);
		end
		if nDamageVigor > 0 then
			messageDamage(rSource, rTarget, "roll_damage_vigor", sTypes, nDamageVigor, aNotifications);
		end
	end

	-- States and concentration
	if bDead and (bInconscious or not ActorManager2.isPC(nodeTarget)) then
		table.insert(aNotifications, Interface.getString("damage_chat_dead"));
		if not EffectManagerSS.hasEffect(rTarget, DataCommon.conditions["Dead"]) then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = DataCommon.conditions["Dead"], nDuration = 0 }, true);
		end
	elseif bDead then
		table.insert(aNotifications, Interface.getString("damage_chat_unconscious"));
		if not EffectManagerSS.hasEffect(rTarget, DataCommon.conditions["Dying"]) then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = DataCommon.conditions["Dying"], nDuration = 300 }, true);
		end
		
	elseif bInconscious then
		table.insert(aNotifications, Interface.getString("damage_chat_dying"));
		if not EffectManagerSS.hasEffect(rTarget, DataCommon.conditions["Unconscious"]) then
			EffectManager.addEffect("", "", nodeTargetCT, { sName = DataCommon.conditions["Unconscious"], nDuration = 30 }, true);
		end
	elseif nDif > 0 then
		makeConcentrationChecks(rTarget, nDif);
	end

	-- Deactivate REGEN
	if bDeactivateRegen and not bAutodamage then 
		local bCond = EffectManagerSS.deactivateRegenerationEffects(rTarget, aTypes);
		if bCond then
			table.insert(aNotifications, Interface.getString("damage_chat_regen_deactivate"));
		end
	end

end



function nTimesInArray(array, value)
	return Utilities.nTimesInArray(array, value);
end

function inArray(array, value)
	return Utilities.inArray(array, value);
end

function concatenateArrays(array1, array2)
	return Utilities.concatenateArrays(array1, array2);
end
