--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	ActionsManager.registerModHandler("check", modRoll);
	ActionsManager.registerResultHandler("check", onRoll);
end

function performPartySheetRoll(draginfo, rActor, sCheck)
	local rRoll = getRoll(rActor, sCheck);

	local nTargetDC = DB.getValue("partysheet.checkdc", 0);
	if nTargetDC == 0 then
		nTargetDC = nil;
	end
	rRoll.nTarget = nTargetDC;
	if DB.getValue("partysheet.hiderollresults", 0) == 1 then
		rRoll.bSecret = true;
		rRoll.bTower = true;
	end

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function performRoll(draginfo, rActor, sCheck, nTargetDC, bSecretRoll)
	local rRoll = getRoll(rActor, sCheck, nTargetDC, bSecretRoll);

	if User.isHost() and CombatManager.isCTHidden(ActorManager.getCTNode(rActor)) then
		rRoll.bSecret = true;
	end

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function getRoll(rActor, sCheck, nTargetDC, bSecretRoll)
	local nMod = 0;
	local nAdv = 0;
	local nState = 0;
	local rRoll = {};
	rRoll.sType = "check";

	if sCheck == Interface.getString("luck_before") or sCheck == Interface.getString("luck_after") then
		rRoll.aDice = { DataCommon.luck_dice};
	else
		rRoll.aDice = { "d8", "d8" };
		nMod, nAdv, nState, _ = ActorManager2.getCheck(rActor, sCheck:lower());
	end
	rRoll.nMod = nMod;
	rRoll.nAdv = nAdv;

	rRoll.sDesc = "[CHECK]";
	rRoll.sDesc = rRoll.sDesc .. " " .. StringManager.capitalize(sCheck);
	if nState ~= 0 then
		rRoll.sDesc = rRoll.sDesc .. " [STATE: " .. nState .. "]";
	end
	if nAdv == 1 then
		rRoll.sDesc = rRoll.sDesc .. " [ADV]";
	elseif nAdv > 1 then
		rRoll.sDesc = rRoll.sDesc .. " [ADV x" .. nAdv .. "]";
	elseif nAdv == -1 then
		rRoll.sDesc = rRoll.sDesc .. " [DIS]";
	elseif nAdv < -1 then
		rRoll.sDesc = rRoll.sDesc .. " [DIS x" .. -nAdv .. "]";
	end

	rRoll.bSecret = bSecretRoll;

	rRoll.nTarget = nTargetDC;

	return rRoll;
end

function modRoll(rSource, rTarget, rRoll)
	-- local aAddDesc = {};
	-- local aAddDice = {};
	-- local nAddMod = 0;
	--
	-- local bADV = false;
	-- local bDIS = false;
	-- if rRoll.sDesc:match(" %[ADV%]") then
	-- 	bADV = true;
	-- 	rRoll.sDesc = rRoll.sDesc:gsub(" %[ADV%]", "");
	-- end
	-- if rRoll.sDesc:match(" %[DIS%]") then
	-- 	bDIS = true;
	-- 	rRoll.sDesc = rRoll.sDesc:gsub(" %[DIS%]", "");
	-- end
	--
	-- if rSource then
	-- 	local bEffects = false;
	--
	-- 	-- Get ability used
	-- 	local sActionStat = nil;
	-- 	local sAbility = rRoll.sDesc:match("%[CHECK%] (%w+)");
	-- 	if not sAbility then
	-- 		local sSkill = rRoll.sDesc:match("%[SKILL%] (%w+)");
	-- 		if sSkill then
	-- 			sAbility = rRoll.sDesc:match("%[MOD:(%w+)%]");
	-- 			if sAbility then
	-- 				sAbility = DataCommon.ability_stol[sAbility];
	-- 			else
	-- 				for k, v in pairs(DataCommon.skilldata) do
	-- 					if k == sSkill then
	-- 						sAbility = v.stat;
	-- 					end
	-- 				end
	-- 			end
	-- 		end
	-- 	end
	-- 	if sAbility then
	-- 		sAbility = sAbility:lower();
	-- 	end
	--
	-- 	-- Build filter
	-- 	local aCheckFilter = {};
	-- 	if sAbility then
	-- 		table.insert(aCheckFilter, sAbility);
	-- 	end
	--
	-- 	-- Get roll effect modifiers
	-- 	local nEffectCount;
	-- 	aAddDice, nAddMod, nEffectCount = EffectManagerSS.getEffectsBonus(rSource, {"CHECK"}, false, aCheckFilter);
	-- 	if (nEffectCount > 0) then
	-- 		bEffects = true;
	-- 	end
	--
	-- 	-- Get condition modifiers
	-- 	if EffectManagerSS.hasEffectCondition(rSource, "ADVCHK") then
	-- 		bADV = true;
	-- 		bEffects = true;
	-- 	elseif #(EffectManagerSS.getEffectsByType(rSource, "ADVCHK", aCheckFilter)) > 0 then
	-- 		bADV = true;
	-- 		bEffects = true;
	-- 	end
	-- 	if EffectManagerSS.hasEffectCondition(rSource, "DISCHK") then
	-- 		bDIS = true;
	-- 		bEffects = true;
	-- 	elseif #(EffectManagerSS.getEffectsByType(rSource, "DISCHK", aCheckFilter)) > 0 then
	-- 		bDIS = true;
	-- 		bEffects = true;
	-- 	end
	-- 	if EffectManagerSS.hasEffectCondition(rSource, "Frightened") then
	-- 		bDIS = true;
	-- 		bEffects = true;
	-- 	end
	-- 	if EffectManagerSS.hasEffectCondition(rSource, "Intoxicated") then
	-- 		bDIS = true;
	-- 		bEffects = true;
	-- 	end
	-- 	if EffectManagerSS.hasEffectCondition(rSource, "Poisoned") then
	-- 		bDIS = true;
	-- 		bEffects = true;
	-- 	end
	-- 	if StringManager.contains({ "strength", "dexterity", "constitution" }, sAbility) then
	-- 		if EffectManagerSS.hasEffectCondition(rSource, "Encumbered") then
	-- 			bEffects = true;
	-- 			bDIS = true;
	-- 		end
	-- 	end
	--
	-- 	-- Get ability modifiers
	--
	-- 	local nBonusStat, nBonusEffects = ActorManager2.getAbility(rSource, sAbility, true);
	-- 	if nBonusEffects > 0 then
	-- 		bEffects = true;
	-- 		nAddMod = nAddMod + nBonusStat;
	-- 	end
	--
	-- 	-- Get exhaustion modifiers
	-- 	local nExhaustMod, nExhaustCount = EffectManagerSS.getEffectsBonus(rSource, {"EXHAUSTION"}, true);
	-- 	if nExhaustCount > 0 then
	-- 		bEffects = true;
	-- 		if nExhaustMod >= 1 then
	-- 			bDIS = true;
	-- 		end
	-- 	end
	--
	-- 	-- If effects happened, then add note
	-- 	if bEffects then
	-- 		local sEffects = "";
	-- 		local sMod = StringManager.convertDiceToString(aAddDice, nAddMod, true);
	-- 		if sMod ~= "" then
	-- 			sEffects = "[" .. Interface.getString("effects_tag") .. " " .. sMod .. "]";
	-- 		else
	-- 			sEffects = "[" .. Interface.getString("effects_tag") .. "]";
	-- 		end
	-- 		table.insert(aAddDesc, sEffects);
	-- 	end
	-- end
	--
	-- if #aAddDesc > 0 then
	-- 	rRoll.sDesc = rRoll.sDesc .. " " .. table.concat(aAddDesc, " ");
	-- end
	-- ActionsManager2.encodeDesktopMods(rRoll);
	-- for _,vDie in ipairs(aAddDice) do
	-- 	if vDie:sub(1,1) == "-" then
	-- 		table.insert(rRoll.aDice, "-p" .. vDie:sub(3));
	-- 	else
	-- 		table.insert(rRoll.aDice, "p" .. vDie:sub(2));
	-- 	end
	-- end
	-- rRoll.nMod = rRoll.nMod + nAddMod;
	--
	-- ActionsManager2.encodeAdvantage(rRoll, bADV, bDIS);
end

function onRoll(rSource, rTarget, rRoll)
	ActionsManager2.decodeAdvantage(rRoll);

	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);

	if rRoll.nTarget then
		local nTotal = ActionsManager.total(rRoll);
		local nTargetDC = tonumber(rRoll.nTarget) or 0;

		rMessage.text = rMessage.text .. " (vs. DC " .. nTargetDC .. ")";
		if nTotal >= nTargetDC then
			rMessage.text = rMessage.text .. " [SUCCESS]";
		else
			rMessage.text = rMessage.text .. " [FAILURE]";
		end
	end

	Comm.deliverChatMessage(rMessage);
end
