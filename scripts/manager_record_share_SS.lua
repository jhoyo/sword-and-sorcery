-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sExtraLabel = "ref_label_"; 
local tInfo = { ["spell"] = {
    ["header"] = {name = "name", label = "reftype", getLabel = true,  node_label = true},
    [1] = {name = "level", label = "level",  getLabel = true},
    [2] = {name = "type_spell", label = "type", getName = true, getLabel = true},
    [3] = {name = "spell_rarity", label = "rarity", getName = true, getLabel = true} ,
    [4] = {name = "school",  getLabel = true},
    [5] = {name = "source",  getLabel = true},
    [6] = {name = "elements", space_after = true,  getLabel = true},
    [7] = {name = "components",  getLabel = true},
    [8] = {name = "castingtime",  getLabel = true},
    [9] = {name = "range",  getLabel = true},
    [10] = {name = "objective",  getLabel = true},
    [11] = {name = "duration",  getLabel = true},
    [12] = {name = "essence_cost", auxiliar = "essence_target", getAuxiliar = true, getLabel = true},
    [13] = {name = "keywords", label = "spell_keywords",  getLabel = true},
    ["text"] = "description";
    ["afterlist_1"] = {node = "heightenings", name = "description", label = "leveltag", node_label = true, separator = ":", title = "spell_heightening"},
    },

    ["aptitude"] = {
        ["header"] = {name = "name", label = "ref_type_feat", getLabel = true },
        [1] = {name = "reftype", getName = true, label = "type",  getLabel = true,  auxiliar = "subtype"},
        [2] = {name = "level", label="label_level",  getLabel = true},
        [3] = {name = "extra_exp", label="label_extra_exp",  getLabel = true},
        [4] = {name = "requirements", label="label_requirements",  getLabel = true},
        ["text"] = "text";
    },

    ["item"] = {
        ["header"] = {name = "name", label = "item", getLabel = true},
        [1] = {name = "type", label = "item_label_type", getLabel = true},
        [2] = {name = "subtype", label = "item_label_subtype", getLabel = true},
        [3] = {name = "rarity", getName = true, label = "item_label_rarity", getLabel = true},
        [4] = {name = "cost" , label = "item_label_cost", getLabel = true},
        [5] = {name = "weight", label = "item_label_weight", getLabel = true},
        [6] = {name = "hardness" , label = "item_label_hardness", getLabel = true},
        [7] = {name = "dents" , label = "item_label_dents", getLabel = true, auxiliar = "dents_aux",},
        [8] = {name = "material" , label = "item_label_material", getLabel = true, space_after = true},
        [9] = {name = "projectiles" , label = "item_label_projectiles", getLabel = true},
        [10] = {name = "init" , label = "char_label_initiative", getLabel = true},
        [11] = {name = "bonus" , label = "item_label_bonus", getLabel = true, auxiliar = "bonus_aux",},
        [12] = {name = "protection" , label = "item_label_protection", getLabel = true},
        [13] = {name = "limit" , label = "item_label_limit", getLabel = true, ignore="99"},
        [14] = {name = "properties" , label = "item_label_properties", getLabel = true, space_after = true},
        [15] = {name = "essence", label = "item_label_essence", getLabel = true},
        [16] = {name = "rune_space", label = "item_label_rune_space", getLabel = true, auxiliar = "rune_space_aux" },
        ["text"] = "description",
        ["attacks"] = true,
        ["powers"] = true,
        ["afterlist_1"] = {node = "modifications", name = "name", label = "- ", separator = "", title = "item_label_modifications"},
        ["afterlist_2"] = {node = "runes", name = "name", label = "level", node_label = true, separator = ":", title = "item_label_runes"},
        ["afterlist_3"] = {node = "effectlist", name = "name", label = "- ", separator = "", title = "item_label_effects"},
    },

    ["technique"] = {
        ["header"] = {name = "name", label = "ref_type_technique", getLabel = true },
        [1] = {name = "cycler", getName = true,  label = "type", getLabel = true},
        [2] = {name = "subcycler", getName = true,  label = "technique_mastery", getLabel = true, space_after = true},
        [3] = {name = "combatcycler", getName = true,  label = "technique_style", getLabel = true},
        [4] = {name = "actions", label = "spell_actions", getLabel = true},
        [5] = {name = "vigor_cost", label = "ref_label_vigor_cost", getLabel = true},
        [6] = {name = "magiccycler", getName = true,  label = "technique_style", getLabel = true},
        [7] = {name = "vigor_mod", label = "technique_extra_vigor", getLabel = true, auxiliar = "vigor_mod_aux", auxiliar_sub = "technique_tooltip_perlevel", getAuxiliar = true},
        [8] = {name = "dif_mod", label = "technique_extra_dif", getLabel = true, auxiliar = "dif_mod_aux", auxiliar_sub = "technique_tooltip_perlevel", getAuxiliar = true},
        [9] = {name = "unstable_mod", label = "technique_unstable_mod", getLabel = true, auxiliar = "unstable_mod_aux", auxiliar_sub = "technique_tooltip_perlevel", getAuxiliar = true},
        [10] = {name = "actions_mod", label = "technique_actions_mod", getLabel = true}, 
        [11] = {name = "disadvantage", name_check = true, label = "technique_disadvantage", getLabel = true},
        ["text"] = "text",
    },

    ["recipe"] = {
        ["header"] = {name = "name", label = "recipe_label", getLabel = true },
        [1] = {name = "skill", label = "recipe_skill", getLabel = true},
        [2] = {name = "category", getName = true, label = "recipe_category", getLabel = true},
        [3] = {name = "rarity", getName = true, label = "recipe_rarity", getLabel = true},
        [4] = {name = "equipment", label = "equipment_equipment", getLabel = true},
        [5] = {name = "requirements", label = "recipe_requirements", getLabel = true},
        [6] = {name = "supplies", label = "recipe_supplies", getLabel = true},
        [7] = {name = "cost", label = "recipe_cost", getLabel = true},
        [8] = {name = "keywords", label = "recipe_keywords", getLabel = true},
        [9] = {name = "item_name", label = "recipe_item", getLabel = true},
        ["text"] = "text",
    },

    ["itemtemplate"] = {
        ["header"] = {name = "name", label = "recipe_label", getLabel = true },
        [1] = {name = "type", getName = true,  label = "type", getLabel = true},
        [2] = {name = "subtype", getName = true,  label = "template_item_type", getLabel = true},
        [3] = {name = "material_base", label = "template_material_base", getLabel = true},
        [4] = {name = "natural", label = "template_natural", getLabel = true},
        [5] = {name = "level", label = "level", getLabel = true},
        [6] = {name = "spell", label = "template_spell", getLabel = true},
        [7] = {name = "features", label = "power_label_keywords", getLabel = true, space_after = true},
        [8] = {name = "cost_mult", label = "template_cost_mult", getLabel = true},
        [9] = {name = "cost_add", label = "item_grouped_label_cost", getLabel = true},
        [10] = {name = "rarity", name_sign = true, label = "item_label_rarity", getLabel = true},
        [11] = {name = "weight", label = "template_weight_mult", getLabel = true, auxiliar = "weight_aux", auxiliar_extra = "chat_perheightening", auxiliar_sign = true}, 
        [12] = {name = "hardness", name_sign = true, label = "item_label_hardness", getLabel = true, auxiliar = "hardness_aux", auxiliar_extra = "chat_perheightening", auxiliar_sign = true},
        [13] = {name = "dents", name_sign = true, label = "item_label_dents", getLabel = true, space_after = true },
        [14] = {name = "properties", label = "item_label_properties", getLabel = true},
        [15] = {name = "properties_perheight", label = "template_prop_perheight", getLabel = true},
        [16] = {name = "init", name_sign = true, label = "char_label_initiative", getLabel = true, auxiliar = "init_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening"},
        [17] = {name = "bonus", name_sign = true, label = "item_label_bonus", getLabel = true, auxiliar = "bonus_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening"},
        [18] = {name = "bonus_def", name_sign = true, label = "template_bonus_def", getLabel = true, auxiliar = "bonus_def_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening"},
        [19] = {name = "range_attack", name_sign = true, label = "item_label_attack_range", getLabel = true, auxiliar = "range_attack_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening"},
        [20] = {name = "damage_cat", name_sign = true, label = "template_damage_cat", getLabel = true, auxiliar = "damage_cat_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening"},
        [21] = {name = "damage_dice", name_sign = true, label = "template_damage_dice", getLabel = true, auxiliar = "damage_dice_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening"},
        [22] = {name = "damagetype", label = "template_damage_type", getLabel = true},
        [23] = {name = "protection", label = "item_label_protection", getLabel = true},
        [24] = {name = "protection_perheight", label = "template_prot_perheight", getLabel = true},
        [25] = {name = "limit", name_sign = true, label = "item_label_limit", getLabel = true, auxiliar = "limit_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening", space_after = true},
        [26] = {name = "essence", name_sign = true, label = "item_label_essence", getLabel = true, auxiliar = "essence_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening"},
        [27] = {name = "rune_space", name_sign = true, label = "template_rune_space", getLabel = true, auxiliar = "rune_space_aux", auxiliar_sign = true, auxiliar_extra = "chat_perheightening"},
        ["text"] = "description",
        
        ["afterlist_1"] = {node = "heightenings", name = "description", label = "leveltag", node_label = true, separator = ":", title = "spell_heightening"},
    },
    
};

function onTabletopInit()
	-- RecordShareManager.setWindowClassCallback("ref_ability", handleAbilityShare);
	RecordShareManager.setRecordTypeCallback("spell", handleSpellShare);
	RecordShareManager.setRecordTypeCallback("aptitude", handleAptitudeShare);
	RecordShareManager.setRecordTypeCallback("item", handleItemeShare);
	RecordShareManager.setRecordTypeCallback("technique", handleTechniqueShare);
	RecordShareManager.setRecordTypeCallback("recipe", handleRecipeShare);
	RecordShareManager.setRecordTypeCallback("itemtemplate", handleItemtemplateShare);
end


function handleAptitudeShare(node, tOutput)
    handleCommonShare(node, tOutput, "aptitude");
end
function handleSpellShare(node, tOutput)
    handleCommonShare(node, tOutput, "spell");
end
function handleItemeShare(node, tOutput)
    local bID = LibraryData.getIDState("item", nodeRecord);
    if Session.isHost or bID then
        handleCommonShare(node, tOutput, "item");
    else
        Debug.chat("No permission to share item");
    end
end
function handleTechniqueShare(node, tOutput)
    handleCommonShare(node, tOutput, "technique");
end
function handleRecipeShare(node, tOutput, sExtra)
    handleCommonShare(node, tOutput, "recipe");
end
function handleItemtemplateShare(node, tOutput, sExtra)
    handleCommonShare(node, tOutput, "itemtemplate");
end

function handleCommonShare(node, tOutput, sType)
    local tCommon = tInfo[sType];
    if not tCommon then
        return;
    end

    -- Header
    local tHeader = tCommon["header"];
    if tHeader then
        local sName, sLabel, sAuxiliar = addInfoToTable(node, tHeader, tOutput, " -");
    end
    -- Body
    table.insert(tOutput, "");
    for _,tLine in ipairs(tCommon) do
        local sName, sLabel, sAuxiliar = addInfoToTable(node, tLine, tOutput, ":");
    end
    -- Description
    if tCommon["text"] then
        local sText = DB.getText(node, tCommon["text"], "");
        if sText ~= "" then
            table.insert(tOutput, "");
            table.insert(tOutput, sText);
        end
    end
    -- Attacks
    if tCommon["attacks"] then
        addAttacksInfo(node, tOutput);
    end
    -- Afterlists
    local ind = 1;
    while tCommon["afterlist_"..ind] and ind < 10 do
        local tList = tCommon["afterlist_"..ind];
        local nodeList = DB.getChild(node, tList["node"]);
        if nodeList and nodeList.getChildCount() > 0 then
            -- Separation and title
            table.insert(tOutput, "");
            if tList["title"] then
                table.insert(tOutput, Interface.getString(tList["title"]));
            end
            -- Elements
            for _,node in pairs(nodeList.getChildren()) do
                local sName, sLabel, sAuxiliar = addInfoToTable(node, tList, tOutput, tList["separator"]);
            end
        end
        ind = ind + 1;
    end
    -- Powers
    if tCommon["powers"] then
        addPowersInfo(node, tOutput);
    end
end

function getInfoFromTable(node, tLine)
    -- Get node path
    local sName = tLine["name"];
    local sLabel = tLine["label"];
    if not sLabel then
        sLabel = sExtraLabel .. sName;
    end
    local sAuxiliar = tLine["auxiliar"];

    sName = DB.getText(node, sName, "");
    if tLine["name_check"] and sName == "1" then
        sName = Interface.getString("yes");
    end
    if tLine["node_label"] then
        sLabel = DB.getText(node, sLabel, "");
    end
    if sAuxiliar then
        sAuxiliar = DB.getText(node, sAuxiliar, "");
    end
    if tLine["auxiliar_sub"] and sAuxiliar ~= "" and sAuxiliar ~= "0" then
        sAuxiliar = tLine["auxiliar_sub"];
    elseif tLine["auxiliar_check"] and sAuxiliar == "1" then
        sAuxiliar = Interface.getString("yes");
    end
    
    -- Get data
    if tLine["getName"] then
        sName = Interface.getString(sName);
    end
    if tLine["getLabel"] then
        sLabel = Interface.getString(sLabel);
    end
    if tLine["getAuxiliar"] and sAuxiliar then
        sAuxiliar = Interface.getString(sAuxiliar);
    end

    -- Extras
    if tLine["name_sign"] and sName ~= "" and tonumber(sName) > 0 then
        sName = "+" .. sName;
    end
    if tLine["auxiliar_sign"] and sAuxiliar ~= "" and tonumber(sAuxiliar) > 0 then
        sAuxiliar = "+" .. sAuxiliar;
    end
    if tLine["auxiliar_extra"] and sAuxiliar ~= "" and sAuxiliar ~= "0" then
        sAuxiliar = sAuxiliar .. " " .. Interface.getString(tLine["auxiliar_extra"]);
    end
    
    return sName, sLabel, sAuxiliar;
end


function addInfoToTable(node, tLine, tOutput, sSep)
    local sName, sLabel, sAuxiliar = getInfoFromTable(node, tLine);
    if tLine["force_rep"] or (sName ~= "" and sName ~= (tLine["ignore" ]or "0")) then
        sName = string.format("%s%s %s", sLabel, sSep, sName);
        if sAuxiliar and sAuxiliar ~= "" and sAuxiliar ~= "0" then
            sName = sName .. " (" .. sAuxiliar .. ")";
        end
        table.insert(tOutput, sName);
        if tLine["space_after"] then
            table.insert(tOutput, "");
        end
    end
end

function addAttacksInfo(node, tOutput)
    local nodeList = DB.getChild(node, "attacks");
    if nodeList and nodeList.getChildCount() > 0 then
        table.insert(tOutput, "");
        table.insert(tOutput, Interface.getString("item_label_attacks"));
        for _,nodeAt in pairs(nodeList.getChildren()) do
            -- Print only if there is damage
            local sDamage = DB.getText(nodeAt, "damage_attack", "");
            if sDamage ~= "" then                
                local sString = "";
                -- Type and reach
                local nTypeAttack = DB.getValue(nodeAt, "type_attack", 0);
                sString = "(" .. DataCommon.ranged_types_short[nTypeAttack];
                local nRange = DB.getValue(nodeAt, "range_attack", 0);
                if nRange > 1 or (nTypeAttack > 0 and nRange > 0) then
                    sString = sString .. " " .. nRange;
                end
                -- Damage
                sString = sString .. ") " .. sDamage;
                -- Properties
                local sProperties = DB.getText(nodeAt, "properties_attack", "");
                if sProperties ~= "" then
                    sString = sString .. " (" .. sProperties .. ")";
                end
                -- Add it
                table.insert(tOutput, sString);
            end
                
        end
    end
end

function addPowersInfo(node, tOutput)
    local nodeList = DB.getChild(node, "powers");
    if nodeList and nodeList.getChildCount() > 0 then
        table.insert(tOutput, "");
        table.insert(tOutput, Interface.getString("item_label_powers"));
        for _,nodePo in pairs(nodeList.getChildren()) do
            -- Print only if there is a power name
            local sName = DB.getText(nodePo, "name", "");
            if sName ~= "" then                
                local sType = DB.getText(nodePo, "type", "");
                -- Level and name
                local sString = string.format("(%d) %s", DB.getValue(nodePo, "level", 0), sName);
                -- Consumable
                if sType == "" or sType == "item_power_type_consumed" then
                    sString = sString .. " (" .. Interface.getString("item_power_type_consumed");
                else
                    -- Charges
                    local nCharges = DB.getValue(nodePo, "charges", 0);
                    sString = sString .. string.format(" (%d %s", nCharges, Interface.getString("item_power_type_charges"):lower());
                    -- Recharge
                    if sType == "item_power_type_rec_charges" then
                        local nRec = DB.getValue(nodePo, "recovery", 0);
                        local sRec = DB.getValue(nodePo, "recovery_period", "");
                        if sRec == "" then
                            sRec = Interface.getString("power_recovery_long");
                        else
                            sRec = Interface.getString(sRec);
                        end
                        sString = sString .. ", rec. " .. tostring(nRec) .. " " .. sRec;
                    end
                end
                sString = sString .. ")";

                -- Add it
                table.insert(tOutput, sString);
            end
                
        end
    end
end