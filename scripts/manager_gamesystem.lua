--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

-- Ruleset action types
actions = {
	["dice"] = { bUseModStack = true },
	["table"] = { },
	["cast"] = { sTargeting = "self" },
	["affliction"] = { sTargeting = "all" },
	["castsave"] = { sTargeting = "each" },
	["death"] = { },
	["concentration"] = { },
	["powersave"] = { sTargeting = "each" },
	["attack"] = { sIcon = "action_attack", sTargeting = "each", bUseModStack = true },
	["damage"] = { sIcon = "action_damage", sTargeting = "all", bUseModStack = true },
	["heal"] = { sIcon = "action_heal", sTargeting = "all", bUseModStack = true },
	["effect"] = { sIcon = "action_effect", sTargeting = "all" },
	["init"] = { bUseModStack = true },
	["save"] = { bUseModStack = true },
	["check"] = { bUseModStack = true },
	-- ["recharge"] = { },
	["recovery"] = { bUseModStack = true },
	["skill"] = { bUseModStack = true },
	["summon"] = { },
};

targetactions = {
	"castsave",
	"powersave",
	"attack",
	"damage",
	"heal",
	"effect"
};



function onInit()
	-- Death markers
	registerStandardDeathMarkersSS();

	-- Currencies
	currencies = {
		{ name = Interface.getString("coin_platinum"), weight = 0.02, value = 1000 },
		{ name = Interface.getString("coin_gold"), weight = 0.015, value = 100 },
		{ name = Interface.getString("coin_electrum"), weight = 0.01, value = 10 },
		{ name = Interface.getString("coin_silver"), weight = 0.008, value = 1 },
		{ name = Interface.getString("coin_copper"), weight = 0.005, value = 0.1 },
	};
	currencyDefault = Interface.getString("coin_silver");
	-- Vision
	VisionManager.addVisionType(Interface.getString("vision_infrared"), "darkvision");
	VisionManager.addVisionType(Interface.getString("vision_spirit"), "truesight");

	-- Languages
	languages = {
		-- Ancient dialects
		[Interface.getString("language_value_ancient")] = "Semielven",

		[Interface.getString("language_value_human")] = "",
		[Interface.getString("language_value_celestial")] = "",
		[Interface.getString("language_value_halfling")] = "",
		[Interface.getString("language_value_giant")] = "",

		[Interface.getString("language_value_elvish")] = "Elven",
		[Interface.getString("language_value_fey")] = "Elven",
		[Interface.getString("language_value_aquan")] = "Elven",

		[Interface.getString("language_value_dwarvish")] = "Dwarven",
		[Interface.getString("language_value_gnomish")] = "Dwarven",
		[Interface.getString("language_value_draconic")] = "Dwarven",
		[Interface.getString("language_value_minotaur")] = "Dwarven",

		[Interface.getString("language_value_goblin")] = "Draconic",
		[Interface.getString("language_value_orc")] = "Draconic",
		[Interface.getString("language_value_deepspeech")] = "Draconic",
		[Interface.getString("language_value_shadow")] = "Draconic",

		-- Magic dialects
		[Interface.getString("language_value_magic")] = "Primordial",

		[Interface.getString("language_value_arcane")] = "Celestial",
		[Interface.getString("language_value_divine")] = "Celestial",
		[Interface.getString("language_value_primordial")] = "Celestial",
		[Interface.getString("language_value_elemental")] = "Celestial",
		[Interface.getString("language_value_zen")] = "Celestial",

		[Interface.getString("language_value_druidic")] = "Druidic",

		[Interface.getString("language_value_mortis")] = "Darken",
		[Interface.getString("language_value_aberrant")] = "Darken",

		[Interface.getString("language_value_abyssal")] = "Infernal",
		[Interface.getString("language_value_ashen")] = "Infernal",
		[Interface.getString("language_value_fomorian")] = "Infernal",
		[Interface.getString("language_value_demonic")] = "Infernal",
		[Interface.getString("language_value_diabolic")] = "Infernal",
		[Interface.getString("language_value_putrid")] = "Infernal",
		[Interface.getString("language_value_bestial")] = "Infernal",

		[Interface.getString("language_value_gestual")] = "Gestual",
		[Interface.getString("language_value_militar")] = "Gestual",
		[Interface.getString("language_value_criminal")] = "Gestual",
	};
	languagefonts = {
		[Interface.getString("language_value_ancient")] = "Ancient",
		[Interface.getString("language_value_celestial")] = "",
		[Interface.getString("language_value_fey")] = "Elven",
		[Interface.getString("language_value_draconic")] = "Dwarven",
		[Interface.getString("language_value_shadow")] = "Draconic",
		[Interface.getString("language_value_magic")] = "Primordial",
		[Interface.getString("language_value_runic")] = "Celestial",
		[Interface.getString("language_value_druidic")] = "Druidic",
		[Interface.getString("language_value_infernal")] = "Infernal",
		[Interface.getString("language_value_darken")] = "Darken",
	};
	languagefonts_inv = {};
	for k,v in pairs(languagefonts) do
		languagefonts_inv[v] = k;
	end
	-- To do only when languages are changed
	-- opulateCampaignLanguages();
end

function getCharSelectDetailHost(nodeChar)
	local sValue = "";
	local nLevel = DB.getValue(nodeChar, "level", 0);
	if nLevel > 0 then
		sValue = "Level " .. math.floor(nLevel*100)*0.01;
	end
	return sValue;
end

function requestCharSelectDetailClient()
	return "name,#level";
end

function receiveCharSelectDetailClient(vDetails)
	return vDetails[1], "Level " .. math.floor(vDetails[2]*100)*0.01;
end

function getCharSelectDetailLocal(nodeLocal)
	local vDetails = {};
	table.insert(vDetails, DB.getValue(nodeLocal, "name", ""));
	table.insert(vDetails, DB.getValue(nodeLocal, "level", 0));
	return receiveCharSelectDetailClient(vDetails);
end

function getPregenCharSelectDetail(nodePregenChar)
	return CharManager.getClassLevelSummary(nodePregenChar);
end

function getDistanceUnitsPerGrid()
	return 1;
end

-- OVERRIDEN FROM CORERPG
function populateCampaignLanguages()
	if not languages then
		return;
	end

	DB.deleteChildren(LanguageManager.CAMPAIGN_LANGUAGE_LIST);

	for kLang,vLang in pairs(languages) do
		local nodeLang = DB.createChild(LanguageManager.CAMPAIGN_LANGUAGE_LIST);
		DB.setValue(nodeLang, "name", "string", kLang);
		DB.setValue(nodeLang, "font", "string", vLang);
	end
end


function populateCampaignCurrencies()
	if not currencies then
		return;
	end

	DB.deleteChildren(LanguageManager.CAMPAIGN_CURRENCY_LIST);

	for _,vCurrency in ipairs(currencies) do
		local nodeCurrency = DB.createChild(CurrencyManager.CAMPAIGN_CURRENCY_LIST);
		DB.setValue(nodeCurrency, CurrencyManager.CAMPAIGN_CURRENCY_LIST_NAME, "string", vCurrency["name"] or vCurrency);
		DB.setValue(nodeCurrency, CurrencyManager.CAMPAIGN_CURRENCY_LIST_WEIGHT, "number", vCurrency["weight"] or 0);
		DB.setValue(nodeCurrency, CurrencyManager.CAMPAIGN_CURRENCY_LIST_VALUE, "number", vCurrency["value"] or 0);
	end
end

function registerStandardDeathMarkersSS()
	ImageDeathMarkerManager.setEnabled(true);
	
	ImageDeathMarkerManager.registerGetCreatureTypeFunction(ActorManager2.getCharType);

	ImageDeathMarkerManager.registerCreatureTypes(DataCommon.creaturetype);
	ImageDeathMarkerManager.setCreatureTypeDefault("construct", "blood_black");
	ImageDeathMarkerManager.setCreatureTypeDefault("plant", "blood_green");
	ImageDeathMarkerManager.setCreatureTypeDefault("undead", "blood_violet");
end
