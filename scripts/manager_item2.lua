--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local aRomans = {[0] = "", [1]="I", [2]="II", [3]="III", [4]="IV", [5]="V", [6]="VI", [7]="VII", [8]="VIII", [9]="IX", [10]="X", [11]="XI" };

function onInit()
	-- Override functions
	ItemManager.handleAnyDrop = handleAnyDrop;
	ItemManager.handleItem = handleItem;
	ItemManager.getItemSourceType = getItemSourceType;
	ItemManager.addItemToList = addItemToList;
	ItemManager.helperAddItemPostEvents = helperAddItemPostEvents;

	-- Node to store original nodes
	createSavedItemsNode();
end

function createSavedItemsNode()
	if User.isHost() then
		local node = DB.findNode("items_saved");
		if not node then
			local root = DB.getRoot();
			node = root.createChild("items_saved")
			node.setPublic(true);
		end
		
		local nodeCharsheet = DB.findNode("charsheet");
		if nodeCharsheet then
			for k, v in pairs(nodeCharsheet.getChildren()) do
				local username = v.getOwner();
				if username ~= "" then
					local newNode = node.createChild(username);
					newNode.setPublic(true);
					DB.setOwner(newNode, username);
				end
			end
		end
	end
end

function makeMagicAttacks(nodeItem)
	if isWeapon(nodeItem) then
		for _,v in pairs(DB.getChildren(nodeItem, "attacks")) do
			local sDamage = DB.getValue(v, "damage_attack", ""):lower();
			if not sDamage:find(DataCommon.dmgtypes_modifier["magic"]) then
				DB.setValue(v, "damage_attack", "string", sDamage .. ", " .. DataCommon.dmgtypes_modifier["magic"]);
			end
		end
	end
end

function isArmor(vRecord)
	local nodeItem;
	if type(vRecord) == "string" then
		nodeItem = DB.findNode(vRecord);
	elseif type(vRecord) == "databasenode" then
		nodeItem = vRecord;
	end
	if not nodeItem then
		return false, "", "", false;
	end

	local sTypeLower = StringManager.trim(DB.getValue(nodeItem, "type", "")):lower();
	local sSubtypeLower = StringManager.trim(DB.getValue(nodeItem, "subtype", "")):lower();
	local sArmor = Interface.getString("item_armor");
	local sSummonedArmor = Interface.getString("item_summoned_armor");

	local bIsArmor = (sTypeLower == sArmor) or (sTypeLower == sSummonedArmor) or (sSubtypeLower == sArmor);
	local bIsSummoned = sTypeLower == sSummonedArmor;

	return bIsArmor, sTypeLower, sSubtypeLower, bIsSummoned;
end

function isWeapon(vRecord)
	local bIsWeapon = false;
	local bIsShield = false;

	local nodeItem;
	if type(vRecord) == "string" then
		nodeItem = DB.findNode(vRecord);
	elseif type(vRecord) == "databasenode" then
		nodeItem = vRecord;
	end
	if not nodeItem then
		return false, "", "";
	end

	local sTypeLower = StringManager.trim(DB.getValue(nodeItem, "type", "")):lower();
	local sSubtypeLower = StringManager.trim(DB.getValue(nodeItem, "subtype", "")):lower();
	local sWeapon = Interface.getString("item_weapon");
	local sAmmunition = Interface.getString("item_ammunition");

	if (sTypeLower == sWeapon) or (sSubtypeLower == sWeapon) then
		bIsWeapon = true;
	end
	if sSubtypeLower == sAmmunition then
		bIsWeapon = false;
	end
	-- Debug.chat(vRecord, bIsWeapon, sTypeLower, sSubtypeLower, sWeapon, sAmmunition)

	return bIsWeapon, sTypeLower, sSubtypeLower;
end

function isShield(vRecord)
	local bIsWeapon, _, sSubtypeLower = isWeapon(vRecord);
	if bIsWeapon and Utilities.inArray(DataCommon.shield_types, sSubtypeLower) then
		return true;
	end
	return false;
end


function isRanged(vRecord)
	local bIsRanged = false;
	local bIsWeapon, sTypeLower, sSubtypeLower = isWeapon(vRecord);
	if bIsWeapon then
		local nodeAttacklist = vRecord.getChild("attacks");
		for _, att in pairs(nodeAttacklist.getChildren()) do
			local nValue = DB.getValue(att, "type_attack", 0);
			bIsRanged = bIsRanged or nValue > 0;
		end
	end

	return bIsRanged, sTypeLower, sSubtypeLower;
end

function isAmmunition(vRecord)
	local bIsAmmunition = false;

	local nodeItem;
	if type(vRecord) == "string" then
		nodeItem = DB.findNode(vRecord);
	elseif type(vRecord) == "databasenode" then
		nodeItem = vRecord;
	end
	if not nodeItem then
		return false, "", "";
	end

	local sTypeLower = StringManager.trim(DB.getValue(nodeItem, "type", "")):lower();
	local sSubtypeLower = StringManager.trim(DB.getValue(nodeItem, "subtype", "")):lower();
	local sAmmunition = Interface.getString("item_ammunition");

	if (sTypeLower == sAmmunition) or (sSubtypeLower == sAmmunition) then
		bIsAmmunition = true;
	end

	return bIsAmmunition, sTypeLower, sSubtypeLower;
end

function isNatural(vRecord)
	-- Summoned armor
	local sType = StringManager.trim(DB.getValue(vRecord, "type", ""):lower());
	if DataCommon.armor_summoned == sType then
		return true;
	end
	local sProp = DB.getValue(vRecord, "properties", ""):lower();
	if sProp:find(DataCommon.armor_keywords["ether"]) then
		return true;
	end

	-- Natural material
	local sMat = DB.getValue(vRecord, "material", ""):lower();
	local bCond = Utilities.inArray(DataCommon.natural_materials, sMat);
	if not bCond then
		local sPath = DB.getValue(vRecord, "material_path", "");
		local nodeMat;
		if sPath and sPath ~= "" then
			nodeMat = DB.findNode(sPath);
		end
		bCond = DB.getValue(nodeMat, "natural", 0) > 0;
	end
	return bCond;
end

function isService(vRecord)
	local bCond = false;

	local nodeItem;
	if type(vRecord) == "string" then
		nodeItem = DB.findNode(vRecord);
	elseif type(vRecord) == "databasenode" then
		nodeItem = vRecord;
	end
	if not nodeItem then
		return false, "", "";
	end

	local sTypeLower = StringManager.trim(DB.getValue(nodeItem, "type", "")):lower();
	local sSubtypeLower = StringManager.trim(DB.getValue(nodeItem, "subtype", "")):lower();
	local bCond = sTypeLower == Interface.getString("item_service");

	return bCond, sSubtypeLower;
end


function clearWeapon(nodeChar, sHand)
	DB.setValue(nodeChar, sHand .. ".name", "string", "");
	DB.setValue(nodeChar, sHand .. ".skill", "string", "");
	DB.setValue(nodeChar, sHand .. ".ability", "string", "");
	DB.setValue(nodeChar, sHand .. ".bonus_deffense", "number", 0);
	DB.setValue(nodeChar, sHand .. ".bonus_deffense_ranged", "number", 0);
	DB.setValue(nodeChar, sHand .. ".infused", "number", 0);
	DB.setValue(nodeChar, sHand .. ".unlimited", "number", 0);
	DB.setValue(nodeChar, sHand .. ".defense_mult", "number", 4);

end

function isRefBaseItemClass(sClass)
	return StringManager.contains({"reference_armor", "reference_weapon", "reference_equipment", "reference_mountsandotheranimals", "reference_waterbornevehicles", "reference_vehicle"}, sClass);
end

function addItemToList2(sClass, nodeSource, nodeTarget, nodeTargetList)
	if LibraryData.isRecordDisplayClass("item", sClass) then
		if sClass == "reference_equipment" and DB.getChildCount(nodeSource, "subitems") > 0 then
			local bFound = false;
			for _,v in pairs(DB.getChildren(nodeSource, "subitems")) do
				local sSubClass, sSubRecord = DB.getValue(v, "link", "", "");
				local nSubCount = DB.getValue(v, "count", 1);
				if LibraryData.isRecordDisplayClass("item", sSubClass) then
					local nodeNew = ItemManager.addItemToList(nodeTargetList, sSubClass, sSubRecord);
					if nodeNew then
						bFound = true;
						if nSubCount > 1 then
							DB.setValue(nodeNew, "count", "number", DB.getValue(nodeNew, "count", 1) + nSubCount - 1);
						end
					end
				end
			end
			if bFound then
				return false;
			end
		end

		DB.copyNode(nodeSource, nodeTarget);
		DB.setValue(nodeTarget, "locked", "number", 1);

		-- Set the identified field
		if ((sClass == "reference_magicitem") and (not User.isLocal())) then
			DB.setValue(nodeTarget, "isidentified", "number", 0);
		else
			DB.setValue(nodeTarget, "isidentified", "number", 1);
		end

		return true;
	end

	return false;
end

function addItemToChar(sName, nodeChar, nNew)
	if not sName or not nodeChar then
		return false;
	end
	if not nNew then
		nNew = 1;
	end
	sNameLower = sName:lower();

	local nodeList = DB.getChild(nodeChar, "inventorylist");
	if not nodeList then
		return false;
	end

	-- Look for item
	for _, v in pairs(DB.getChildren(nodeList)) do
		if sNameLower == DB.getValue(v, "name", ""):lower() then
			local nCount = DB.getValue(v, "count", 0);
			DB.setValue(v, "count", "number", nCount + nNew);
			return true;
		end
	end

	-- If item is not found, add it. First, look for the global node
	nodeNew = nodeList.createChild();
	local nodeList = DB.findNode("item");
	for _, nodeItem in pairs(DB.getChildren(nodeList)) do
		if sNameLower == DB.getValue(nodeItem, "name", ""):lower() then
			DB.copyNode(nodeItem, nodeNew);
			DB.setValue(nodeNew, "locked", "number", 1);
			DB.setValue(nodeNew, "count", "number", nNew);
			return true;
		end
	end

	-- If no global item exists create an empty one
	DB.setValue(nodeNew, "locked", "number", 1);
	DB.setValue(nodeNew, "count", "number", nNew);
	DB.setValue(nodeNew, "name", "string", sName);

	-- Set the identified field TODO
	-- if ((sClass == "reference_magicitem") and (not User.isLocal())) then
	-- 	DB.setValue(nodeTarget, "isidentified", "number", 0);
	-- else
	-- 	DB.setValue(nodeTarget, "isidentified", "number", 1);

	return true;
end

function getLinkedPowers(nodeItem)
	local rPowers = {};

	for k, v in pairs(nodeItem.createChild("powers").getChildren()) do
		local rPower = {
			["charges"] = DB.getValue(v, "charges", 0),
			["type"] = DB.getValue(v, "type", ""),
			["recovery_period"] = DB.getValue(v, "recovery_period", ""),
			["recovery"] = DB.getValue(v, "recovery", 0),
			["link_identifier"] = DB.getValue(v, "link_identifier", ""),
			["item_power_path"] = v.getPath(),
			["power_path"] = DB.getValue(v, "link_power", ""),
			["name"] = DB.getValue(v, "name", ""),
		};
		table.insert(rPowers, rPower);
	end

	return rPowers;
end

function scanProperties(sProp)
	if not sProp then
		sProp = "";
	end
	sProp = sProp:lower();
	local aProp = {};

	for key, string in pairs(DataCommon.weapon_keywords) do
		if sProp:find(string) then
			aProp[key] = true;
		else
			aProp[key] = false;
		end
	end
	for key, string in pairs(DataCommon.weapon_keywords_number) do
		local sDefault = "0";
		if key == "defense_mult" then
			sDefault = "4";
		elseif key == "defense_ranged" then
			sDefault = "-99";
		elseif (key == "recharge") or (key == "chamber")  then
			sDefault = "1";
		end
		
		aProp[key] = tonumber(sProp:match(string) or sDefault);
	end

	-- Stat
	aProp["stat"] = "";
	for _,string in pairs(DataCommon.abilities_translated) do
		if sProp:find(string) then
			aProp["stat"] = string;
		end
	end
	
	-- Particlar
	if aProp["short"] then
		aProp["length"] = aProp["length"] - 2;
		aProp["finesse"] = true;
	elseif aProp["light"] then
		aProp["length"] = aProp["length"] - 1;
	end

	return aProp
end

function addPropertiesToAction(rAction, sProp)
	local aProp = ItemManager2.scanProperties(sProp);
	rAction.nCrit = aProp["crit_attack"];
	rAction.nMonsterHunt = aProp["monstruosity"];
	rAction.nAnimalHunt = aProp["hunting"];
	rAction.bIncorporeal = aProp["incorporeal"];
	rAction.nLens = aProp["lens"];
	rAction.nPiercing = aProp["piercing"];
	rAction.nReach = aProp["length"] + aProp["attack"];
	rAction.nThreshold = aProp["threshold"];
	rAction.nDamageArmor = aProp["damage_armor"];
	rAction.bTouch = aProp["touch"];
	rAction.bElectric = aProp["electric"];
	rAction.bLarge = aProp["large"];
	rAction.nAdv = 0;
	return rAction, aProp;
end


function isDoubleWeapon(nodeItem)
	if isWeapon(nodeItem) then
		local nodeList = DB.getChild("attacks");
		if nodeList then
			for _, v in pairs(nodeList.getChildren()) do
				local sProps = DB.getValue(v, "properties", "");
				local aProps = scanProperties(sProp);
				if aProps["double"] then
					return true;
				end
			end
		end
	end
	return false;
end

function isMagicWeapon(nodeItem)
	if isWeapon(nodeItem) then
		return DB.getValue(nodeItem, "number_runes", 0) > 0;
	end
	return false;
end

-------------------------
-- OVERRIDE
-------------------------

function handleAnyDrop(vTarget, draginfo, sListTarget)
	local sDragType = draginfo.getType();

	if not Session.IsHost then
		local sTargetType = ItemManager.getItemSourceType(vTarget);
		if sTargetType == "item" then
			return false;
		elseif sTargetType == "treasureparcel" then
			return false;
		elseif sTargetType == "partysheet" then
			if sDragType ~= "shortcut" then
				return false;
			end
			local sClass, sRecord = draginfo.getShortcutData();
			if not LibraryData.isRecordDisplayClass("item", sClass) then
				return false;
			end
			local sSourceType = ItemManager.getItemSourceType(sRecord);
			if sSourceType ~= "charsheet" then
				return false;
			end
		elseif sTargetType == "charsheet" then
			if not DB.isOwner(vTarget) then
				return false;
			end
		end
	end

	if sDragType == "number" then
		ItemManager.handleString(vTarget, draginfo.getDescription(), draginfo.getNumberData());
		return true;

	elseif sDragType == "string" then
		ItemManager.handleString(vTarget, draginfo.getStringData());
		return true;

	elseif sDragType == "shortcut" then
		local sClass,sRecord = draginfo.getShortcutData();
		if LibraryData.isRecordDisplayClass("item", sClass) then
			local bTransferAll = false;
			local sSourceType = ItemManager.getItemSourceType(sRecord);
			local sTargetType = ItemManager.getItemSourceType(vTarget);
			if StringManager.contains({"charsheet", "partysheet"}, sSourceType) and StringManager.contains({"charsheet", "partysheet"}, sTargetType) then
				bTransferAll = Input.isShiftPressed();
			end

			ItemManager.handleItem(vTarget, sListTarget, sClass, sRecord, bTransferAll);
			return true;
		elseif sClass == "treasureparcel" then
			ItemManager.handleParcel(vTarget, sRecord);
			return true;
		end
	end

	return false;
end

function handleItem(vTargetRecord, sTargetList, sClass, sRecord, bTransferAll)
	local nodeTargetRecord = nil;
	if type(vTargetRecord) == "databasenode" then
		nodeTargetRecord = vTargetRecord;
	elseif type(vTargetRecord) == "string" then
		nodeTargetRecord = DB.findNode(vTargetRecord);
	end
	if not nodeTargetRecord then
		return;
	end

	if not sTargetList then
		local sTargetRecordType = ItemManager.getItemSourceType(nodeTargetRecord);
		if Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType) then
			sTargetList = sTargetList or "inventorylist";
			if ItemManager2 and ItemManager2.getCharItemListPath then
				sTargetList = ItemManager2.getCharItemListPath(vTargetRecord, sClass);
			end
		elseif sTargetRecordType == "treasureparcel" then
			sTargetList = "itemlist";
		elseif sTargetRecordType == "partysheet" then
			sTargetList = "treasureparcelitemlist";
		elseif sTargetRecordType == "item" then
			sTargetList = "";
		end

		if not sTargetList then
			return;
		end
	end

	ItemManager.sendItemTransfer(nodeTargetRecord.getPath(), sTargetList, sClass, sRecord, bTransferAll);
end


function getItemSourceType(vNode)
	local sNodePath = nil;
	if type(vNode) == "databasenode" then
		sNodePath = vNode.getPath();
	elseif type(vNode) == "string" then
		sNodePath = vNode;
	end
	if not sNodePath then
		return "";
	end

	for _,vMapping in ipairs(LibraryData.getMappings("charsheet")) do
		if StringManager.startsWith(sNodePath, vMapping) then
			return "charsheet";
		end
	end

	for _,vMapping in ipairs(LibraryData.getMappings("item")) do
		if StringManager.startsWith(sNodePath, vMapping) then
			return "item";
		end
	end

	for _,vMapping in ipairs(LibraryData.getMappings("treasureparcel")) do
		if StringManager.startsWith(sNodePath, vMapping) then
			return "treasureparcel";
		end
	end

	if StringManager.startsWith(sNodePath, "partysheet") then
		return "partysheet";
	end

	if StringManager.startsWith(sNodePath, "npc") then
		return "npc";
	end

	if StringManager.startsWith(sNodePath, "combattracker") then
		return "ct";
	end

	if StringManager.startsWith(sNodePath, "temp") then
		return "temp";
	end

	return "";
end

-- NOTE: Assumed target and source base nodes
-- (item = campaign, charsheet = char inventory, partysheet = party inventory, treasureparcels = parcel inventory)
function addItemToList(vList, sClass, vSource, bTransferAll, nTransferCount)
	-- Get the source item database node object
	local nodeSource = nil;
	if type(vSource) == "databasenode" then
		nodeSource = vSource;
	elseif type(vSource) == "string" then
		nodeSource = DB.findNode(vSource);
	end
	local nodeList = nil;
	if type(vList) == "databasenode" then
		nodeList = vList;
	elseif type(vList) == "string" then
		nodeList = DB.createNode(vList);
	end
	if not nodeSource or not nodeList then
		return nil;
	end

	-- Determine the source and target item location type
	local sSourceRecordType = ItemManager.getItemSourceType(nodeSource);
	local sTargetRecordType = ItemManager.getItemSourceType(nodeList);

	-- Make sure that the source and target locations are not the same character
	if Utilities.inArray({"charsheet", "ct", "npc"}, sSourceRecordType) and Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType) then
		if nodeSource.getParent().getPath() == nodeList.getPath() then
			return nil;
		end
	end

	-- Use a temporary location to create an item copy for manipulation, if the item type is supported
	local sTempPath;
	if nodeList.getParent() then
		sTempPath = nodeList.getParent().getPath("temp.item");
	else
		sTempPath = "temp.item";
	end
	DB.deleteNode(sTempPath);
	local nodeTemp = DB.createNode(sTempPath);
	local bCopy = false;
	if sClass == "item" then
		DB.copyNode(nodeSource, nodeTemp);
		bCopy = true;
	elseif ItemManager2 and ItemManager2.addItemToList2 then
		bCopy = ItemManager2.addItemToList2(sClass, nodeSource, nodeTemp, nodeList);
	end

	local nodeNew = nil;
	if bCopy then
		-- Remove fields that shouldn't be transferred
		if aDeleteCopyFields then
			for _,sField in ipairs(aDeleteCopyFields) do
				DB.deleteChild(nodeTemp, sField);
			end
		end

		-- Determine target node for source item data.
		-- If we already have an item with the same fields, then just append the item count.
		-- Otherwise, create a new item and copy from the source item.
		local bAppend = false;
		if sTargetRecordType ~= "item" then
			for _,vItem in pairs(DB.getChildren(nodeList, "")) do
				if ItemManager.compareFields(vItem, nodeTemp, true) then
					nodeNew = vItem;
					bAppend = true;
					break;
				end
			end
		end
		if not nodeNew then
			nodeNew = DB.createChild(nodeList);
			if Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType) and Session.IsLocal then
				DB.setValue(nodeTemp, "isidentified", "number", 1);
			end
			DB.copyNode(nodeTemp, nodeNew);
		end

		-- Determine the source, target and item names
		local sSrcName, sTrgtName;
		if Utilities.inArray({"charsheet", "ct", "npc"}, sSourceRecordType) then
			sSrcName = DB.getValue(nodeSource, "...name", "");
		elseif sSourceRecordType == "partysheet" then
			sSrcName = "PARTY";
		else
			sSrcName = "";
		end
		if Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType) then
			sTrgtName = DB.getValue(nodeNew, "...name", "");
		elseif sTargetRecordType == "partysheet" then
			sTrgtName = "PARTY";
		else
			sTrgtName = "";
		end
		local sItemName = ItemManager.getDisplayName(nodeNew, true);

		-- Determine whether to copy all items at once or just one item at a time (based on source and target)
		local bCountN = false;
		if (sSourceRecordType == "treasureparcel" and sTargetRecordType == "partysheet") or
				(sSourceRecordType == "treasureparcel" and Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType)) or
				(sSourceRecordType == "partysheet" and sTargetRecordType == "treasureparcel") or
				(sSourceRecordType == "treasureparcel" and sTargetRecordType == "treasureparcel") then
			bCountN = true;
		elseif (sSourceRecordType == "partysheet" and Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType)) or
				(Utilities.inArray({"charsheet", "ct", "npc"}, sSourceRecordType) and Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType)) or
				(Utilities.inArray({"charsheet", "ct", "npc"}, sSourceRecordType) and sTargetRecordType == "partysheet") then
			if bTransferAll then
				bCountN = true;
			end
		elseif (sSourceRecordType == "temp" and Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType)) or
				(sSourceRecordType == "temp" and sTargetRecordType == "treasureparcel") or
				(sSourceRecordType == "temp" and sTargetRecordType == "partysheet") then
			bCountN = true;
		end
		local nCount = 1;
		if bCountN or sTargetRecordType ~= "item" then
			if bCountN then
				nCount = DB.getValue(nodeSource, "count", 1);
			elseif nTransferCount then
				nCount = math.min(DB.getValue(nodeSource, "count", 1), nTransferCount);
			end
			if bAppend then
				local nAppendCount = math.max(DB.getValue(nodeNew, "count", 1), 1);
				DB.setValue(nodeNew, "count", "number", nCount + nAppendCount);
			else
				DB.setValue(nodeNew, "count", "number", nCount);
			end
		end

		-- If not adding to an existing record, then lock the new record and generate events
		if not bAppend then
			DB.setValue(nodeNew, "locked", "number", 1);
			if Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType) then
				ItemManager.onCharAddEvent(nodeNew);
			end
		end

		-- Generate output message if transferring between characters or between party sheet and character
		if Utilities.inArray({"charsheet", "ct", "npc"}, sSourceRecordType) and (sTargetRecordType == "partysheet" or Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType)) then
			local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
			local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
			msg.text = "[" .. sSrcName .. "] -> [" .. sTrgtName .. "] : " .. sItemName;
			if nCount > 1 then
				msg.text = msg.text .. " (" .. nCount .. "x)";
			end
			Comm.deliverChatMessage(msg);

			local nCharCount = DB.getValue(nodeSource, "count", 0);
			if nCharCount <= nCount then
				ItemManager.onCharRemoveEvent(nodeSource);
				nodeSource.delete();
			else
				DB.setValue(nodeSource, "count", "number", nCharCount - nCount);
			end
		elseif sSourceRecordType == "partysheet" and Utilities.inArray({"charsheet", "ct", "npc"}, sTargetRecordType) then
			local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
			local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
			msg.text = "[" .. sSrcName .. "] -> [" .. sTrgtName .. "] : " .. sItemName;
			if nCount > 1 then
				msg.text = msg.text .. " (" .. nCount .. "x)";
			end
			Comm.deliverChatMessage(msg);

			local nPartyCount = DB.getValue(nodeSource, "count", 0);
			if nPartyCount <= nCount then
				nodeSource.delete();
			else
				DB.setValue(nodeSource, "count", "number", nPartyCount - nCount);
			end
		end
	end

	-- Clean up
	DB.deleteNode(sTempPath);

	return nodeNew;
end

function helperAddItemPostEvents(rSourceItem, rTargetItem)
	Debug.chat("Estoy aqui")
	if not rTargetItem.bAppend then
		DB.setValue(rTargetItem.node, "locked", "number", 1);
		if rTargetItem.sType == "charsheet" then
			ItemManager.onCharAddEvent(rTargetItem.node);
		end
	end

	-- Handle adding suffix to campaign item list copies
	if (rSourceItem.sType == "item") and (rTargetItem.sType == "item") then
		local sName = DB.getValue(rSourceItem.node, "name", "");
		if (sName ~= "") and (UtilityManager.getNodeCategory(rSourceItem.node) == UtilityManager.getNodeCategory(rTargetItem.node)) then
			DB.setValue(rTargetItem.node, "name", "string", sName .. " " .. Interface.getString("masterindex_suffix_duplicate"));
		end
	end

	-- Generate output message if transferring between characters or between party sheet and character
	if rSourceItem.sType == "charsheet" and (rTargetItem.sType == "partysheet" or rTargetItem.sType == "charsheet") then
		-- Avoid notifying between character and mount
		local sPath1 = rSourceItem.node.getPath();
		local sPath2 = rTargetItem.node.getPath();
		-- Debug.chat("sPath1",sPath1)
		-- Debug.chat("sPath2",sPath2)
		if sPath1:find(sPath2) or sPath2:find(sPath1) then
			return;
		end

		local sSrcName = DB.getValue(rSourceItem.node, "...name", "");
		local sTrgtName;
		if rTargetItem.sType == "charsheet" then
			sTrgtName = DB.getValue(rTargetItem.node, "...name", "");
		else
			sTrgtName = "PARTY";
		end
		local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
		msg.text = "[" .. sSrcName .. "] -> [" .. sTrgtName .. "] : " .. ItemManager.getDisplayName(rTargetItem.node, true);
		if rSourceItem.nFinalTransferCount > 1 then
			msg.text = msg.text .. " (" .. rSourceItem.nFinalTransferCount .. "x)";
		end
		Comm.deliverChatMessage(msg);

		local nCharCount = DB.getValue(rSourceItem.node, "count", 0);
		if nCharCount <= rSourceItem.nFinalTransferCount then
			ItemManager.onCharRemoveEvent(rSourceItem.node);
			rSourceItem.node.delete();
		else
			DB.setValue(rSourceItem.node, "count", "number", nCharCount - rSourceItem.nFinalTransferCount);
		end
	elseif rSourceItem.sType == "partysheet" and rTargetItem.sType == "charsheet" then
		local sSrcName = "PARTY";
		local sTrgtName = DB.getValue(rTargetItem.node, "...name", "");
		local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
		msg.text = "[" .. sSrcName .. "] -> [" .. sTrgtName .. "] : " .. ItemManager.getDisplayName(rTargetItem.node, true);
		if rSourceItem.nFinalTransferCount > 1 then
			msg.text = msg.text .. " (" .. rSourceItem.nFinalTransferCount .. "x)";
		end
		Comm.deliverChatMessage(msg);

		local nPartyCount = DB.getValue(rSourceItem.node, "count", 0);
		if nPartyCount <= rSourceItem.nFinalTransferCount then
			rSourceItem.node.delete();
		else
			DB.setValue(rSourceItem.node, "count", "number", nPartyCount - rSourceItem.nFinalTransferCount);
		end
	end
end

-----------------------
-- COSTS
-----------------------

function cost_2_array(sString)
	if not sString then
		sString = "";
	end
	local rCoins = {};
	for k,v in ipairs(DataCommon.coins_short) do
		rCoins[k] = tonumber(sString:match("(%d+) " .. v) or "0");
	end
	return rCoins;
end

function multiply_cost(rCost, nValue, bString)
	if type(rCost) == "string" then
		rCost = cost_2_array(rCost);
	end

	for k,v in ipairs(rCost) do
		rCost[k] = v * nValue;
	end
	rCost = round_cost(rCost);

	if bString then
		return array_2_cost(rCost);
	else
		return rCost;
	end
end

function add_costs(rCost, rAdd, bString)
	if type(rCost) == "string" then
		rCost = cost_2_array(rCost);
	end
	if type(rAdd) == "string" then
		rAdd = cost_2_array(rAdd);
	end

	for k,v in ipairs(rCost) do
		rCost[k] = v + rAdd[k];
	end

	if bString then
		return array_2_cost(rCost);
	else
		return rCost;
	end
end

function rest_costs(aCost1, aCost2)
	aCost1 = round_cost(aCost1);
	aCost2 = round_cost(aCost2);
	if isHigherCost(aCost2, aCost1) then
		return false;
	end
	local nNum1 = array_2_copper(aCost1);
	local nNum2 = array_2_copper(aCost2);
	return copper_2_array(nNum1 - nNum2);
end

function array_2_copper(aCost)
	local nNum = 0;
	for k,v in pairs(aCost) do
		nNum = nNum + 10^(5-k) * v;
	end
	return nNum;
end

function copper_2_array(nNum)
	local aCost = {};
	for ind = 1,5 do
		local nDiv = 10^(5-ind);
		table.insert(aCost, math.floor(nNum / nDiv));
		nNum = nNum % nDiv;
	end
	return aCost
end

function round_cost(rCost)
	for k,v in ipairs(rCost) do
		if k > 1 and v >= 10 then
			local nDiv = math.floor(v/10);
			local nRest = v % 10;
			rCost[k] = nRest;
			rCost[k-1] = rCost[k-1] + nDiv;
		end
		if k < 5 then
			local nRest = rCost[k] % 1;
			rCost[k+1] = rCost[k+1] + nRest * 10;
		end
		rCost[k] = math.floor(rCost[k]);
	end
	return rCost
end

function array_2_cost(rCost)
	rCost = round_cost(rCost);
	sString = "";
	coma = false;
	for k, v in ipairs(rCost) do
		if v > 0 then
			if coma then
				sString = sString .. ", ";
			end
			sString = sString .. tostring(v) .. " " .. DataCommon.coins_short[k];
			coma = true;
		end
	end
	return sString;
end

function isHigherCost(aCost1, aCost2)
	aCost1 = round_cost(aCost1);
	aCost2 = round_cost(aCost2);
	for k,v in pairs(aCost1) do
		if v > aCost2[k] then
			return true;
		elseif v < aCost2[k] then
			return false;
		end
	end
	return true;
end


function buyItem(nodeItem, nNum, nodeChar, bFromInventory)
	-- Get char money
	local aCurrent = {};
	table.insert(aCurrent, DB.getValue(nodeChar, "coins.platinum", 0));
	table.insert(aCurrent, DB.getValue(nodeChar, "coins.gold", 0));
	table.insert(aCurrent, DB.getValue(nodeChar, "coins.electrum", 0));
	table.insert(aCurrent, DB.getValue(nodeChar, "coins.silver", 0));
	table.insert(aCurrent, DB.getValue(nodeChar, "coins.copper", 0));

	-- Get items prize
	local sCost = DB.getValue(nodeItem, "cost", "");
	local aCost = cost_2_array(sCost);
	aCost = multiply_cost(aCost, nNum or 1);
	sCost = array_2_cost(aCost);

	-- Check if there is enough money available
	local bCond = isHigherCost(aCurrent, aCost);

	-- Proceed
	if bCond then
		-- Substract money
		aCurrent = rest_costs(aCurrent, aCost);
		DB.setValue(nodeChar, "coins.platinum", "number", aCurrent[1]);
		DB.setValue(nodeChar, "coins.gold", "number", aCurrent[2]);
		DB.setValue(nodeChar, "coins.electrum", "number", aCurrent[3]);
		DB.setValue(nodeChar, "coins.silver", "number", aCurrent[4]);
		DB.setValue(nodeChar, "coins.copper", "number", aCurrent[5]);

		-- Add item
		if bFromInventory then
			local nCount = DB.getValue(nodeItem, "count", 0) + (nNum or 1);
			DB.setValue(nodeItem, "count", "number", nCount);
		else
			local nodeList = nodeChar.createChild("inventorylist");
			addItemToList(nodeList, "item", nodeItem, false, nNum)
		end

		-- Notify
		local rMessage = {mode="chat_success", font="chatfont", icon = "coins" };
		local sName = DB.getValue(nodeItem, "name", "");
		rMessage.text = Interface.getString("char_buy_bought") .. tostring(nNum) .. " " .. sName .. " " .. Interface.getString("for") .. " " .. sCost;
		local sOwner = nodeChar.getOwner();
		if sOwner then
			Comm.deliverChatMessage(rMessage, {sOwner, ""});
		else
			Comm.deliverChatMessage(rMessage, "");
		end
		
	else
		local rMessage = {mode="chat_miss", font="chatfont", icon = "coins", text=Interface.getString("char_buy_notenoughmoney") .. sCost};
		local sOwner = nodeChar.getOwner();
		if sOwner and sOwner ~= "" then
			Comm.deliverChatMessage(rMessage, {sOwner, ""});
		else
			Comm.deliverChatMessage(rMessage);
		end
	end

end

function sellItem(nodeItem, nNum, nodeChar)
	-- Get the current number
	nNum = nNum or 1;
	local nCount = DB.getValue(nodeItem, "count", 0);
	nNum = math.min(nCount, nNum);

	if nNum > 0 then
		-- Get char money
		local aCurrent = {};
		table.insert(aCurrent, DB.getValue(nodeChar, "coins.platinum", 0));
		table.insert(aCurrent, DB.getValue(nodeChar, "coins.gold", 0));
		table.insert(aCurrent, DB.getValue(nodeChar, "coins.electrum", 0));
		table.insert(aCurrent, DB.getValue(nodeChar, "coins.silver", 0));
		table.insert(aCurrent, DB.getValue(nodeChar, "coins.copper", 0));

		-- Get items prize
		local sCost = DB.getValue(nodeItem, "cost", "");
		local aCost = cost_2_array(sCost);
		local nodePS = DB.findNode("partysheet");
		local nMult = DB.getValue(nodePS, "sellpercentage", 33) / 100;
		aCost = multiply_cost(aCost, nNum * nMult);
		sCost = array_2_cost(aCost);

		-- Sell
		DB.setValue(nodeItem, "count", "number", nCount - nNum);
		aCurrent = add_costs(aCurrent, aCost);
		DB.setValue(nodeChar, "coins.platinum", "number", aCurrent[1]);
		DB.setValue(nodeChar, "coins.gold", "number", aCurrent[2]);
		DB.setValue(nodeChar, "coins.electrum", "number", aCurrent[3]);
		DB.setValue(nodeChar, "coins.silver", "number", aCurrent[4]);
		DB.setValue(nodeChar, "coins.copper", "number", aCurrent[5]);

		-- Notify		
		local rMessage = {mode="chat_success", font="chatfont", icon = "coins" };
		local sName = DB.getValue(nodeItem, "name", "");
		rMessage.text = Interface.getString("char_buy_selled") .. tostring(nNum) .. " " .. sName .. " " .. Interface.getString("for") .. " " .. sCost;
		local sOwner = nodeChar.getOwner();
		if sOwner then
			Comm.deliverChatMessage(rMessage, {sOwner, ""});
		else
			Comm.deliverChatMessage(rMessage);
		end
		
	end

end

function isPotion(nodeItem)
	if not nodeItem then
		return false;
	end
	local sName = DB.getValue(nodeItem, "name", ""):lower();
	return sName:find(DataCommon.potion);
end
function isScroll(nodeItem)
	local sName = DB.getValue(nodeItem, "name", ""):lower();
	return sName:find(DataCommon.scroll);
end
function isWand(nodeItem)
	local sName = DB.getValue(nodeItem, "name", ""):lower();
	local cond1 = sName:find(DataCommon.wand);
	local cond2 = hasRune(nodeItem, DataCommon.runes["wand"]);
	return cond1 or cond2;
end
function isStaff(nodeItem)
	local sName = DB.getValue(nodeItem, "name", ""):lower();
	local cond1 =  sName:find(DataCommon.staff);
	local cond2 = hasRune(nodeItem, DataCommon.runes["staff"]);
	return cond1 or cond2;
end

function hasRune(nodeItem, sRune)
	if not sRune or sRune == "" then
		return false;
	end
	sRune = sRune:lower();
	local cond = false;
	local nodeRunes = DB.createChild(nodeItem, "runes");
	for _,rune in pairs(nodeRunes.getChildren()) do
		local sName = DB.getValue(rune, "name", "");
		if sName == sRune then
			return true;
		end
	end
	return false;
end

function hasModification(nodeItem, sMod)
	if not sMod or sMod == "" then
		return false;
	end
	sMod = sMod:lower();
	local cond = false;
	local nodeMod = DB.createChild(nodeItem, "runes");
	for _,mod in pairs(nodeMod.getChildren()) do
		local sName = DB.getValue(mod, "name", "");
		if sName == sMod then
			return true;
		end
	end
	return false;
end

function getOriginalItem(nodeItem, bCreateOriginal)
	local sPath = DB.getValue(nodeItem, "original_path", "");
	local sPathItem = DB.getPath(nodeItem);
	local nodeOriginal;
	if sPathItem:find("items_saved") then
		nodeOriginal = nodeItem

	elseif sPath ~= "" then
		nodeOriginal = DB.findNode(sPath);

	elseif bCreateOriginal then
		-- There was no original item, create it
		local sUsername = nodeItem.getOwner();
		local nodeList = DB.findNode("items_saved");
		if sUsername == "" then
			nodeList = nodeList.createChild("GM");
		else
			nodeList = nodeList.createChild(sUsername);
		end

		if nodeList then
			nodeOriginal = nodeList.createChild();
			sPath = nodeOriginal.getPath();
			DB.copyNode(nodeItem, nodeOriginal);
			DB.setValue(nodeItem, "original_path", "string", sPath);
			DB.setValue(nodeItem, "link_original_item", "windowreference", "item", sPath);

			-- Remove all unnecesary info
			DB.deleteChildren(nodeOriginal.getChild("modifications"));
			DB.deleteChildren(nodeOriginal.getChild("runes"));
			DB.deleteChildren(nodeOriginal.getChild("powers"));
			DB.deleteChildren(nodeOriginal.getChild("effects"));
			local nodeMat = DB.findNode(DB.getValue(nodeOriginal, "material_path", ""));
			if nodeMat then
				local sBase = DB.getValue(nodeMat, "material_base", "");
				if sBase ~= "" then
					nodeOriginal.createChild("material_path").delete();
					nodeOriginal.createChild("material_aux").delete();
					DB.setValue(nodeOriginal, "material", "string", sBase);
				end
			end
		else
			Debug.chat("Node items_saved." .. sUsername .. "not found");
		end
	end
	return nodeOriginal;
end

function updateModifiedItem(nodeItem, bCreateOriginal)
	-- Get if it can be a consumable
	local nSpells = DB.getChildCount(nodeItem, "powers");
	local bPotion = isPotion(nodeItem);
	local bScroll = isScroll(nodeItem);
	local nodeOriginal = getOriginalItem(nodeItem, bCreateOriginal);

	-- All except potions and scrolls
	if nodeOriginal then
		DB.setValue(nodeItem, "modified", "number", 1);
		-- Get original item and data
		local aProps = getOriginalData(nodeOriginal, sPathMaterial ~= "");

		-- Loop in runes
		for _,v in pairs(DB.getChild(nodeItem, "runes").getChildren()) do
			-- Get rune node
			local nLevel = DB.getValue(v, "level", 0);
			local sPath = DB.getValue(v, "link_identifier", "");
			local nodeRune;
			if sPath ~= "" then
				nodeRune = DB.findNode(sPath);
			end

			if nodeRune then
				-- Get heightening level
				local nLevelRune = DB.getValue(nodeRune, "level", 0);
				local sHeight = "";
				local nHeight = 0;
				local nodeHeight = nodeRune.createChild("heightenings");
				for _,v in pairs(nodeHeight.getChildren()) do
					sHeight = DB.getValue(v, "leveltag", "");
				end
				if (sHeight == "+") or (sdHeight == "+1") then
					nHeight = nLevel - nLevelRune;
				elseif (sHeight == "++") or (sHeight == "+2") then
					nHeight = math.floor((nLevel - nLevelRune)/2);
				elseif sHeight == "+3" then
					nHeight = math.floor((nLevel - nLevelRune)/3);
				elseif sHeight == "+4" then
					nHeight = math.floor((nLevel - nLevelRune)/4);
				end

				-- Add rune info to props
				aProps = addModInfo(aProps, nodeRune, nHeight, nLevel);
			end
		end

		-- Loop in modifications
		for _,v in pairs(DB.getChild(nodeItem, "modifications").getChildren()) do
			-- Get modification node
			local sPath = DB.getValue(v, "link_identifier", "");
			if sPath ~= "" then
				local nodeMod = DB.findNode(sPath);
				aProps = addModInfo(aProps, nodeMod);
			end
		end

		-- Material
		local sPath = DB.getValue(nodeItem, "material_path", "");
		local nodeMat;
		if sPath and sPath ~= "" then
			nodeMat = DB.findNode(sPath);
			aProps = addModInfo(aProps, nodeMat);
		end

		-- Transform properties to the item values
		setOriginalData(nodeItem, aProps);

		-- Update weapon
		updateWeapon(nodeItem);

		-- Update encumbrance
		local sPath = nodeItem.getPath();
		if sPath:find("npc") or sPath:find("charsheet") then
			local nodeChar = nodeItem.getChild("...");
			CharManager.updateEncumbrance(nodeChar);
		end

		-- Update encumbrance
		local sPath = nodeItem.getPath();
		if sPath:find("npc") or sPath:find("charsheet") then
			local nodeChar = nodeItem.getChild("...");
			CharManager.updateEncumbrance(nodeChar);
		end

	-- Potions and scrolls
	elseif (nSpells == 1) and (bScroll or bPotion) then
		constructPotionOrScroll(nodeItem, bPotion);
	end
end

function getOriginalData(nodeItem, bMat)
	local aProps = {attacks = {}, powers = {}, effects = {}};
	local aList = {"attacks"}--, "powers", "effects" };
	local aAvoid = {"modified", "original_path", "modifications", "essence", "rune_space", "rune_space_aux", "runes", "name", "link_original_item", "material", "material_aux", "material_path", "powers", "effects", "carried" };
	local aAvoid = {"modified", "original_path", "modifications", "essence", "rune_space", "rune_space_aux", "runes", "name", "link_original_item", "material", "material_aux", "material_path", "powers", "effects", "carried" };
	aProps["rune_space"] = 1;
	for _,v in pairs(nodeItem.getChildren()) do
		local sName = v.getName();
		if Utilities.inArray(aList, sName) then
			getListInfo(v, aProps[sName])

		elseif sName == "hardness" and not bMat then
			aProps[sName] = v.getValue();

		elseif not Utilities.inArray(aAvoid, sName) then
			aProps[sName] = v.getValue();
		end
	end
	aProps["cost_add"] = {};
	aProps["multiple_cost"] = {};
	aProps["cost_rest"] = {};
	aProps["cost_mult"] = 0;
	aProps["rarity_min"] = 0;
	aProps["number_runes"] = 0;
	aProps["init_rune"] = 0;
	aProps["no_rune_space"] = false;
	aProps["warnings"] = {};

	return aProps;
end


function setOriginalData(nodeItem, aProps)
	local aList = {"attacks"}; --, "powers", "effects" };
	local aAvoid = {"cost_add", "cost_mult", "rarity", "rarity_min", "warnings", "link", "init", "init_rune", "link_original_item", "original_path", "modified", "modifications", "essence", "rune_space", "rune_space_aux", "runes", "name", "material", "material_aux", "material_path", "powers", "effects"};
	for sProp,vProp in pairs(aProps) do
		local nodeList = DB.getChild(nodeItem, sProp);
		if Utilities.inArray(aList, sProp) then
			DB.deleteChildren(nodeList);
			for _,item in pairs(vProp) do
				local nodeSubitem = DB.createChild(nodeList);
				for k,v in pairs(item) do
					local nodePart = DB.createChild(nodeSubitem, k, type(v));
					nodePart.setValue(v);
				end
			end
		elseif not Utilities.inArray(aAvoid, sProp) then
			local nodePart = DB.createChild(nodeItem, sProp);
			nodePart.setValue(vProp);
		end
	end

	-- Set init
	local nInit = math.min(0, aProps["init"]) + aProps["init_rune"];
	DB.setValue(nodeItem, "init", "number", nInit);

	-- Set rune space in special cases
	if aProps["no_rune_space"] then
		DB.setValue(nodeItem, "rune_space", "number", 0)
	end

	-- Set cost
	local nCosts = #aProps["cost_add"] + #aProps["multiple_cost"] + #aProps["cost_rest"];
	if (aProps["cost_mult"] ~= 0) or (nCosts > 0) then
		local rCost = cost_2_array(aProps["cost"]);
		-- First, multiply
		if aProps["cost_mult"] ~= 0 then
			rCost = multiply_cost(rCost, aProps["cost_mult"]);
		end
		-- Then, add
		for _,sAdd in pairs(aProps["cost_add"]) do
			local rAdd = cost_2_array(sAdd);
			rCost = add_costs(rCost, rAdd);
		end
		-- Multiple runes work a bit differently
		local rCostMax;
		local kMax = 0;
		for k,sAdd in pairs(aProps["multiple_cost"]) do
			local rAdd = cost_2_array(sAdd);
			if not rCostMax or kMax == 0 or isHigherCost(rAdd, rCostMax) then
				rCostMax = rAdd;
				kMax = k;
			end
		end
		for k,sAdd in pairs(aProps["multiple_cost"]) do
			local rAdd = cost_2_array(sAdd);
			if k ~= kMax then
				rAdd = multiply_cost(rAdd, 0.5)
			end
			rCost = add_costs(rCost, rAdd);
		end
		-- Then, substract
		for _,sAdd in pairs(aProps["cost_rest"]) do
			local rAdd = cost_2_array(sAdd);
			rCost = rests_costs(rCost, rAdd);
		end

		aProps["cost"] = array_2_cost(rCost);
	end
	DB.setValue(nodeItem, "cost", "string", aProps["cost"]);

	-- Set rarity
	local sRarity = DB.getValue(nodeOriginal, "rarity", "");
	local nRarity = DataCommon.rarity[sRarity] + (aProps["rarity_add"] or 0);
	nRarity = math.min(7, math.max(nRarity, aProps["rarity_min"]));
	sRarity = DataCommon.rarity_inv[nRarity];
	DB.setValue(nodeItem, "rarity", "string", sRarity);
end

function getListInfo(node, array)
	for _,item in pairs(node.getChildren()) do
		local bAvoid = false;
		local aItem = {};
		for _,v in pairs(item.getChildren()) do
			local sName = v.getName();
			aItem[sName] = v.getValue();
			bAvoid = bAvoid or ((sName == "damage_attack") and (v.getValue() == "") );
		end
		if not bAvoid then
			table.insert(array, aItem);
		end
	end
end

function addModInfo(aProps, nodeMod, nHeight, nLevel)
	if not nodeMod then
		return aProps;
	end

	local aAvoid = {"type", "material_base", "level", "spell", "heightenings", "features", "description", "subtype", "name", "type_lista", "level_lista", "link", "cost_add", "cost", "namelabel",  "effectlist", "protection_perheight"};
	local aDamage = {"damage_cat", "damage_cat_aux", "damage_dice", "damage_dice_aux",  "damagetype", "properties", "properties_perheight", "range_attack"};
	local bAdjustDamage = true;
	local aList = {}--;"powers", "effects" };
	local sType = DB.getValue(nodeMod, "type", "");
	local sSubtype = DB.getValue(nodeMod, "subtype", ""):lower();
	local bRune = sType == "template_rune";
	local bMaterial = sType == "template_material";
	local bUpgrade = sType == "template_upgrade";
	local bWeapon = (sSubtype == Interface.getString("item_weapon")) or (sSubtype == Interface.getString("item_ammunition"));
	
	for _,v in pairs(nodeMod.getChildren()) do
		local sName = v.getName();
		local value = v.getValue()
		if Utilities.inArray(aList, sName) then
			getListInfo(v, aProps[sName])

		elseif Utilities.inArray(aList, sName) and nHeight then
			sName = sName:sub(1,sName:len()-4);
			aProps[sName] = (aProps[sName] or 0) + value * nHeight;

		elseif sName == "cost_mult" and value ~= 1 then
			aProps[sName] = (aProps[sName] or 0) + value;

		elseif sName == "weight" and value > 0 then
			aProps[sName] = (aProps[sName] or 0) * value;

		elseif sName == "hardness" then
			if bMaterial then
				aProps[sName] = value;
			else
				aProps[sName] = (aProps[sName] or 0) + value;
			end

		elseif sName == "properties" and value ~= "" and not bWeapon then
			aProps[sName] = mergeProperties(aProps[sName] or "", value)

		elseif sName == "properties_perheight" and nHeight and value ~= "" and not bWeapon then
			for ind=1,nHeight do
				aProps["properties"] = addProtections(aProps["properties"] or "", value);
			end

		elseif sName == "limit_aux" and nHeight then
			aProps["limit"] = (aProps["limit"] or 0) + value * nHeight;

		elseif sName == "init_aux" and nHeight then
			aProps["init_rune"] = (aProps["init_rune"] or 0) + value * nHeight;

		elseif sName == "init" then
			if bRune then
				aProps["init_rune"] = (aProps["init_rune"] or 0) + value;
			else
				aProps["init"] = (aProps["init"] or 0) + value;
			end

		elseif sName == "bonus_aux" and nHeight then
			aProps["bonus"] = (aProps["bonus"] or 0) + value * nHeight;

		elseif sName == "bonus_def" then
			aProps["bonus_aux"] = (aProps["bonus_aux"] or 0) + value;

		elseif sName == "bonus_def_aux" and nHeight then
			aProps["bonus_aux"] = (aProps["bonus_aux"] or 0) + value * nHeight;

		elseif sName == "name" and bMaterial then
			aProps["material_path"] = nodeMod.getPath();
			aProps["material"] = value;

		elseif sName == "protection" then
			aProps[sName] = addProtections(aProps[sName] or "", value);

		elseif sName == "protection_perheight" and nHeight then
			for ind=1,nHeight do
				aProps["protection"] = addProtections(aProps["protection"] or "", value);
			end

		elseif sName == "rune_space" and bMaterial then
			if value == 0 then
				aProps["no_rune_space"] = true;
			else
				aProps["rune_space"] = (aProps["rune_space"] or 0) + value;
			end

		elseif sName == "rune_space" and bRune then
			aProps["rune_space_aux"] = (aProps["rune_space_aux"] or 0) + value;

		elseif sName == "rune_space_aux" and bRune and nHeight then
				aProps["rune_space_aux"] = (aProps["rune_space_aux"] or 0) + value * nHeight;

		elseif sName == "essence_aux" and bRune and nHeight then
			aProps["essence"] = (aProps["essence"] or 0) + value * nHeight;

		elseif sName == "hardness_aux" and bRune and nHeight then
			aProps["hardness"] = (aProps["hardness"] or 0) + value * nHeight;

		elseif sName == "weight_aux" and bRune and nHeight then
			aProps["weight"] = (aProps["weight"] or 0) + value * nHeight;

		elseif Utilities.inArray(aDamage, sName) and bAdjustDamage then
			local nCat = DB.getValue(nodeMod, "damage_cat", 0) + (nHeight or 0) * DB.getValue(nodeMod, "damage_cat_aux", 0);
			local nDice = DB.getValue(nodeMod, "damage_dice", 0) + (nHeight or 0) * DB.getValue(nodeMod, "damage_dice_aux", 0);
			local nRange = DB.getValue(nodeMod, "range_attack", 0) + (nHeight or 0) * DB.getValue(nodeMod, "range_attack_aux", 0);
			local sTypes = DB.getValue(nodeMod, "damagetype", "");
			local sProps = DB.getValue(nodeMod, "properties", "");
			local sPropsHeight = DB.getValue(nodeMod, "properties_perheight", "");
			if nHeight and sPropsHeight ~= "" then
				for ind=1,nHeight do
					sProps = mergeProperties(sProps, sPropsHeight);
				end
			end

			for _,attack in pairs(aProps["attacks"]) do
				attack["damage_attack"] = modifyDamageString(attack["damage_attack"] or "", nCat, nDice, sTypes, bRune);
				attack["properties_attack"] = mergeProperties(attack["properties_attack"] or "", sProps);
				attack["range_attack"] = attack["range_attack"] + nRange;
			end
			bAdjustDamage = false;

		elseif sName == "rarity" then
			aProps["rarity_add"] = (aProps["rarity_add"] or 0) + value;

		elseif not Utilities.inArray(aAvoid, sName) and not Utilities.inArray(aDamage, sName) then
			if not value or (type(value) == "string") or (type(aProps[sName]) == "string") then
				-- Debug.chat("Problematic sName", sName)
			else
				-- Debug.chat(sName, value, aProps[sName])
				aProps[sName] = (aProps[sName] or 0) + value;
			end
		end
	end

	-- Other properties of runes
	if bRune then
		local sKey = DB.getValue(nodeMod, "features", "");
		-- Cost and rarity
		local sAdd, nRarity = calculateRuneCost(nLevel, sKey);
		aProps["number_runes"] = (aProps["number_runes"] or 0) + 1;

		if sKey:find(DataCommon.rune_keywords["multiple"]) then
			table.insert(aProps["multiple_cost"], sAdd);
		elseif sKey:find(DataCommon.rune_keywords["cursed"]) then
			table.insert(aProps["cost_rest"], sAdd);
		else
			table.insert(aProps["cost_add"], sAdd);
		end

		-- Space depending on level
		if sKey:find(DataCommon.rune_keywords["space_level"]) then
			aProps["space_add"] = (aProps["space_add"] or 0) + DataCommon.rune_space_per_level[nLevel];
		end

		-- Rarity
		aProps["rarity_min"] = math.max(aProps["rarity_min"], nRarity);
		
	end

	return aProps;

end

function calculateRuneCost(nLevel, sProp)
	sProp = sProp:lower();
	local sAdd = DataCommon.rune_price[nLevel];
	local rAdd = cost_2_array(sAdd);
	local nRarity = DataCommon.min_runic_rarity[nLevel];
	if sProp:find(DataCommon.rune_keywords["cheap"]) then
		rAdd = multiply_cost(rAdd, DataCommon.rune_prize_mod["cheap"]);
		nRarity = nRarity + DataCommon.rune_rarity_mod["cheap"];
	elseif sProp:find(DataCommon.rune_keywords["rare"]) then
		rAdd = multiply_cost(rAdd, DataCommon.rune_prize_mod["rare"]);
		nRarity = nRarity + DataCommon.rune_rarity_mod["rare"];
	elseif sProp:find(DataCommon.rune_keywords["secret"]) then
		rAdd = multiply_cost(rAdd, DataCommon.rune_prize_mod["secret"]);
		nRarity = nRarity + DataCommon.rune_rarity_mod["secret"];
	elseif sProp:find(DataCommon.rune_keywords["mythic"]) then
		rAdd = multiply_cost(rAdd, DataCommon.rune_prize_mod["mythic"]);
		nRarity = nRarity + DataCommon.rune_rarity_mod["mythic"];
	end
	
	return array_2_cost(rAdd), nRarity;
end

function constructPotionOrScroll(nodeItem, bPotion)
	
	-- Get all nodes
	local aList = DB.getChildren(nodeItem, "powers");
	local nodeItemPower;
	for _,v in pairs(aList) do
		nodeItemPower = v;
	end
	local sPath = DB.getValue(nodeItemPower, "link_identifier", "");
	local nodeSpell = DB.findNode(sPath);

	-- Get info
	local nLevel = DB.getValue(nodeItemPower, "level", 0);
	nLevel = math.max(0, math.min(10, nLevel));
	local sRarity = DB.getValue(nodeSpell, "rarity", ""); -- spell_rare, spell_lost
	local sSpellDesc = DB.getValue(nodeSpell, "description", "");
	local sNameSpell = DB.getValue(nodeSpell, "name", "");

	-- Act
	local sType;
	local sTypeInternal;
	if bPotion then
		sType = StringManager.capitalize(DataCommon.potion);
		sTypeInternal = "potion";
	else
		sType = StringManager.capitalize(DataCommon.scroll);
		sTypeInternal = "scroll";
	end
	local sName = sType .. Interface.getString("of") .. sNameSpell;
	if bPotion and nLevel > 0 then
		local sLevel = aRomans[nLevel];
		local nLength = string.len(sLevel);
		local nLengthOrig = string.len(sName);
		if string.sub(sName, nLengthOrig - nLength + 1, nLengthOrig) ~= sLevel then
			sName = sName .. " " .. sLevel;
		end
	end
	DB.setValue(nodeItem, "name", "string", sName);
	DB.setValue(nodeItem, "type", "string", Interface.getString("forge_magicitem_expendable"));
	DB.setValue(nodeItem, "subtype", "string", sType);		
	DB.setValue(nodeItem, "dents", "number", 1);
	DB.setValue(nodeItem, "modified", "number", 1);
	if bPotion then
		DB.setValue(nodeItem, "hardness", "number", 2);
		DB.setValue(nodeItem, "material", "string", Interface.getString("material_ceramic"));
	else
		DB.setValue(nodeItem, "hardness", "number", 1);
		DB.setValue(nodeItem, "material", "string", Interface.getString("material_scroll"));
	end

	local sPrize;
	if bPotion then
		sPrize = DataCommon.prize_potion[nLevel];
	else
		sPrize = DataCommon.prize_scroll[nLevel];
	end
	local nRarity = DataCommon.rarity_cons[nLevel];
	local nWeight = DataCommon.weight_cons[nLevel];
	if sRarity == "spell_rare" then
		local rCost = ItemManager2.cost_2_array(sPrize);
		rCost = ItemManager2.multiply_cost(rCost, DataCommon.prize_rare_mod[sTypeInternal]);
		sPrize = ItemManager2.array_2_cost(rCost);
		nRarity = nRarity + DataCommon.rarity_rare_mod;
	elseif sRarity == "spell_lost" then
		local rCost = ItemManager2.cost_2_array(sPrize);
		rCost = ItemManager2.multiply_cost(rCost, DataCommon.prize_mythic_mod[sTypeInternal]);
		sPrize = ItemManager2.array_2_cost(rCost);
		nRarity = nRarity + DataCommon.rarity_mythic_mod;
	end		
	nRarity = math.min(DataCommon.rarity_max, math.max(DataCommon.rarity_min, nRarity));
	DB.setValue(nodeItem, "cost", "string", sPrize);
	DB.setValue(nodeItem, "rarity", "string", DataCommon.rarity_inv[nRarity]);
	DB.setValue(nodeItem, "weight", "number", nWeight);

	sDesc = "<h>" .. sNameSpell .. "</h>" .. sSpellDesc;
	DB.setValue(nodeItem, "description", "formattedtext", sDesc);
end

function modifyDamageString(sDamage, nCatPlus, nDicePlus, sTypesPlus, bRune)
	if sDamage == "" then
		return "";
	end

	-- Extract damage parts
	local nDice, nCat, sTypes = sDamage:match("(%d+)d(%d+) (%D+)");
	if not sTypes then
		sTypes = "";
	end

	-- Update
	if bRune and not sDamage:find(DataCommon.dmgtypes_modifier["magic"]) and not sTypesPlus:find(DataCommon.dmgtypes_modifier["magic"]) then
		if sTypesPlus == "" then
			sTypesPlus = DataCommon.dmgtypes_modifier["magic"];
		else
			sTypesPlus = sTypesPlus .. ", " .. DataCommon.dmgtypes_modifier["magic"];
		end
	end
	if sTypes == "" then
		sTypes = sTypesPlus;
	elseif sTypesPlus ~= "" then
		sTypes = sTypes .. ", " .. sTypesPlus;
	end

	-- Create new string
	sDamage = tostring(tonumber(nDice or "1") + nDicePlus) .. "d" .. tostring(tonumber(nCat or "1") + nCatPlus) .. " " .. sTypes;
	return sDamage;
end

function addProtections(sProtItem, sProtMod)
	-- Easy parts
	if sProtItem == "" then
		return sProtMod;
	elseif sProtMod == "" then
		return sProtItem;
	end
	-- Find add protection to all
	local nValueAll = tonumber(sProtMod:match("([+-]?%d+) " .. DataCommon.dmgtypes_special["all"]) or "0");
	
	-- Extract modify prot
	sProt = "";
	coma = false
	for _, v in pairs(DataCommon.dmgtypes) do
		local nValueMod = tonumber(sProtMod:match("([+-]?%d+) " .. v) or "0");
		local nValueItem = tonumber(sProtItem:match("([+-]?%d+) " .. v) or "0");
		if nValueMod + nValueItem > 0 then
			if coma then
				sProt = sProt .. ", ";
			end
			sProt = sProt .. tostring(nValueMod + nValueItem + nValueAll) .. " " .. v;
			coma = true;
		end
	end
	return sProt;
end

function mergeProperties(sItem, sMod)
	aStrings = StringManager.split(sMod, ",");
	sLower = sItem:lower();
	local snItem, nCount;
	for _, s in pairs(aStrings) do
		local str, sMod = StringManager.trim(s:lower()):match("(%a+) ([+-]?%d+%.?%d*)");
		if str then
			snItem = sLower:match(str .. " ([+-]?%d+)");
		end
		if sMod ~= nil and snItem ~= nil then
			local sOrig = str .. " " .. snItem;
			local sNew = str .. " " .. tostring(tonumber(sMod) + tonumber(snItem));
			sItem, nCount = sItem:gsub(sOrig, sNew);
			if not nCount then
				sItem, nCount = sItem:gsub(StringManager.capitalize(sOrig), sNew);
			end
			if not nCount then
				sItem, nCount = sItem:gsub(sOrig:upper(), sNew);
			end
			if not nCount then
				sItem = sLower:gsub(sOrig, sNew);
			end
		else
			if sItem ~= "" then
				sItem = sItem .. ", ";
			end
			sItem = sItem .. s
		end
	end
	return sItem;
end

function updateWeapon(nodeItem)
	-- Only for weapons
	if not isWeapon(nodeItem) then
		return;
	end

	-- Get the weapon
	local sPath = DB.getValue(nodeItem, "weapon_path", "");
	if sPath == "" then
		return;
	end
	local nodeWeapon = DB.findNode(sPath);
	if not nodeWeapon then
		Debug.chat("Not weapon for path", sPath)
		return;
	end

	-- Determine identification
	local nItemID = 0;
	if LibraryData.getIDState("item", nodeItem, true) then
		nItemID = 1;
	end

	-- Check if the attacks are linked
	-- local nodeListWeapon = nodeWeapon.createChild("attacks");
	local nodeListItem = nodeItem.createChild("attacks");
	local bLinked = true;
	for _,nodeAttack in pairs(nodeListItem.getChildren()) do
		local sDamage = DB.getValue(nodeAttack, "damage_attack", "");
		local sName = DB.getValue(nodeAttack, "attack_path", "");
		bLinked = bLinked and (sName ~= "" or sDamage == "");
	end

	-- If attacks are linked, update relevant fields
	if bLinked then
		local sName = "";
		if nItemID == 1 then
			sName = DB.getValue(nodeItem, "name", "");
		else
			sName = DB.getValue(nodeItem, "nonid_name", "");
			if sName == "" then
				sName = Interface.getString("item_unidentified");
			end
			sName = "** " .. sName .. " **";
	
			local sPathOriginal = DB.getValue(nodeItem, "original_path", "");
			if sPathOriginal ~= "" then
				nodeItem = DB.findNode(sPathOriginal);
			end
		end
		DB.setValue(nodeWeapon, "name", "string", sName);


		for _,nodeAttack in pairs(nodeListItem.getChildren()) do
			local sDamage = DB.getValue(nodeAttack, "damage_attack", "");
			if sDamage ~= "" then
				local sPathAttack = sPath .. ".attacks." .. DB.getValue(nodeAttack, "attack_path", "");
				local nodeAttackWeapon = DB.findNode(sPathAttack);
				updateAttack(nodeAttack, nodeAttackWeapon);
			end
		end

	-- If attacks are not linked, delete the weapon and add it again
	else
		-- nodeWeapon.delete();
		-- CharManager.addToWeaponDB(nodeItem);
	end
end

function updateAttack(nodeAttackItem, nodeAttackWeapon)
	if not nodeAttackItem or not nodeAttackWeapon then
		return;
	end

	local sProp = DB.getValue(nodeAttackItem, "damage_attack", "");
	DB.setValue(nodeAttackWeapon, "damageview", "string", sProp);

	sProp = DB.getValue(nodeAttackItem, "properties_attack", "");
	DB.setValue(nodeAttackWeapon, "properties", "string", sProp);
	local aProp = ItemManager2.scanProperties(sProp);
	DB.setValue(nodeAttackWeapon, "attack_props", "number", aProp["attack"] + aProp["length"]);
	
	local nProp = DB.getValue(nodeAttackItem, "type_attack", 0);
	DB.setValue(nodeAttackWeapon, "type_attack", "number", nProp);
	
	nProp = DB.getValue(nodeAttackItem, "range_attack", 0);
	DB.setValue(nodeAttackWeapon, "range_attack", "number", nProp);
	
	local nodeItem = nodeAttackItem.getChild("...");
	nProp = DB.getValue(nodeItem, "dents", 0);
	DB.setValue(nodeAttackWeapon, "weapon_damage", "number", nProp);
	
	nProp = DB.getValue(nodeItem, "bonus", 0);
	DB.setValue(nodeAttackWeapon, "attack_bonus", "number", nProp);
end


function createItemFromRecipe(nodeRecipe, bRestSupplies)
	-- If item, create it. If not, just notify 
	-- If item, create it. If not, just notify 
	local _, sPath = DB.getValue(nodeRecipe, "item_link");
	if not sPath or sPath == "" then
		notifyEndRecipe(nodeRecipe);
		notifyEndRecipe(nodeRecipe);
		return;
	end
	local nodeItemRecipe = DB.findNode(sPath);
	if not nodeItemRecipe then
		notifyEndRecipe(nodeRecipe);
		notifyEndRecipe(nodeRecipe);
		return;
	end
	local sNameItem = DB.getValue(nodeItemRecipe, "name", "");
	if sNameItem == "" then
		notifyEndRecipe(nodeRecipe);
		notifyEndRecipe(nodeRecipe);
		return;
	end
	-- TODO: Check tools

	-- Info
	local sSupplies = DB.getValue(nodeRecipe, "supplies", "");
	local nSupplies, sNameSupplies = sSupplies:match("(%d+) (.*)");
	if nSupplies and nSupplies ~= "" then
		nSupplies = tonumber(nSupplies);
	end
	local nodeChar = nodeRecipe.getChild("...");
	local nodeInventory = nodeChar.createChild("inventorylist");
	local nodeMountInv = DB.getChild(nodeChar, "mount_npc.inventorylist");
	local nodeMountSup, nodeSup, nodeItemNew;
	local nItems = 0;
	local nCurrent = 0;
	local nMountCurrent = 0;
	local bSupplies = bRestSupplies and (sSupplies ~= "") and sNameSupplies and (sNameSupplies ~= "") and (nSupplies > 0);

	-- Loop in mount inventory
	if  nodeMountInv then
		for _,nodeItem in pairs(nodeMountInv.getChildren()) do
			local sName = DB.getValue(nodeItem, "name", "");
			if sName == sNameItem then
				nodeItemNew = nodeItem;
				nItems = DB.getValue(nodeItem, "count", 0);
			end
			if bSupplies and sName == sNameSupplies then
				nodeMountSup = nodeItem;
				nMountCurrent = DB.getValue(nodeItem, "count", 0);
			end
		end
	end

	-- Loop in char inventory
	for _,nodeItem in pairs(nodeInventory.getChildren()) do
		local sName = DB.getValue(nodeItem, "name", "");
		if sName == sNameItem then
			nodeItemNew = nodeItem;
			nItems = DB.getValue(nodeItem, "count", 0);
		end
		if bSupplies and sName == sNameSupplies then
			nodeSup = nodeItem;
			nCurrent =  DB.getValue(nodeItem, "count", 0);
		end
	end

	-- Rest supplies
	local sExtra = "";
	if bSupplies and (nCurrent + nMountCurrent < nSupplies) then
		sExtra = Interface.getString("recipe_error_supplies");
	elseif bSupplies then
		if nodeMountSup then
			DB.setValue(nodeMountSup, "count", "number", math.max(0, nMountCurrent - nSupplies));
		end
		if nMountCurrent < nSupplies then
			DB.setValue(nodeSup, "count", "number", math.max(0, nMountCurrent + nMountCurrent - nSupplies));
		end
	end

	-- Create item
	if nodeItemNew then
		DB.setValue(nodeItemNew, "count", "number", nItems+1);
	else
		-- addItemToList(nodeInventory, "item", nodeItemRecipe, false, 1);
		nodeItemNew = nodeInventory.createChild();
		DB.copyNode(nodeItemRecipe, nodeItemNew);
		DB.setValue(nodeItemNew, "count", "number", 1);
		DB.setValue(nodeItemNew, "carried", "number", 1);
	end

	-- Message
	local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
	local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
	msg.text = "[" .. DB.getValue(nodeChar, "name", "Character") .. "] " .. Interface.getString("recipe_tooltip_fabricate") .. ": " .. sNameItem .. sExtra;
	Comm.deliverChatMessage(msg);
end

function notifyEndRecipe(nodeRecipe)
	local nodeChar = nodeRecipe.getChild("...");
	-- Message
	local msg = {font = "msgfont", icon = "coinbag", mode="chat_unknown"};
	msg.text = "[" .. DB.getValue(nodeChar, "name", "Character") .. "] " .. Interface.getString("recipe_tooltip_end") .. ": " .. DB.getValue(nodeRecipe, "name", "");
	Comm.deliverChatMessage(msg);
end

local aItemLists = { "item", "reference.itemdata" };
function findRefItem(sName)
    -- Add modules
	local aLists = Utilities.addModules(aItemLists);

    -- Find the correct one
    for _,sPath in pairs(aLists) do
		local nodeList = DB.findNode(sPath);
        if nodeList then
			for _, nodeNew in pairs(nodeList.getChildren()) do
				local sNameNew = DB.getValue(nodeNew, "name", "");
				if sName == sNameNew then
                    return nodeNew
                end
            end
        end
    end
    return nil;
end


