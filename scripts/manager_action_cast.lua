--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_MISSCAST = "misscast";
OOB_MSGTYPE_ABORTSPELL = "abortspell";
OOB_MSGTYPE_IDENTIFYSPELL = "identify_spell";

ACTION_CASTSPELL = "castspell";
ACTION_ABORTSPELL = "abortspell";

nEffectsLastSpell = 0;

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_MISSCAST, handleMisscast);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_ABORTSPELL, handleAbortSpell);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_IDENTIFYSPELL, handleIdentifySpell);

	ActionsManager.registerModHandler(ACTION_CASTSPELL, modCastSpell);
	ActionsManager.registerResultHandler(ACTION_CASTSPELL, onCastSpell);

	ActionsManager.registerModHandler(ACTION_ABORTSPELL, modAbortSpell);
	ActionsManager.registerResultHandler(ACTION_ABORTSPELL, onAbortSpell);
end

function handleIdentifySpell(msgOOB)
	-- Information of the channeler
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local nodeSource = ActorManager2.getActorAndNode(rSource);
	local sActorName = DB.getValue(nodeSource, "name", "");
	local sStatSource = msgOOB.sStat;
	local sSkill = Interface.getString("skill_value_knowledge_arcane");
	local nMethodicSource = DB.getValue(nodeSource, "magic.methodic", 0);
	local sGenderSource = DB.getValue(nodeSource, "gender", ""):lower();
	sGenderSource = DataCommon.gender_translate[gender_translate];
	local sGenderOpposed = DataCommon.gender_opposed[sGenderSource];
	local bElementalSource = DB.getValue(nodeSource, "abilities.elemental.current", 0) > 0;
	local nStrengthSource = ActorManager2.getCurrentAbilityValue(rSource, sStatSource);
	nStrengthSource = (DataCommon.stat_emission[nStrengthSource] or 1) * (DataCommon.stat_multiplier[msgOOB.sLevel] or 1)

	-- Loop in CT elements
	local nodeList = DB.findNode(CombatManager.CT_LIST);
	local aResults = {};
	for _, nodeCT in pairs(nodeList.getChildren()) do
		-- Get general info
		local node, rActor = ActorManager2.getNodeFromCT(nodeCT);
		local sName = DB.getValue(node, "name", "");
		local sStat, sSkillMagic = ActorManager2.getBestGift(node);
		local bIsPC = ActorManager2.isPC(node);

		-- Identify only if it is a channeler
		if sStat ~= "" and sName ~= sActorName then
			-- Test (no rolling so there are not too many rolls)
			local bAutofail = false;
			local nBonus = 0;
			local nAdv = 0;
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.keyword_states["READMAGIC"]) then
				nBonus, nAdv = ActorManager2.getCheck(node, sStat, sSkillMagic);
				local aAddDice, _ = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["READMAGIC"], false, {});
				for _, sDice in pairs(aAddDice) do
					nBonus = nBonus + CombatManager2.simpleRoll(sDice, 0);
				end
			else
				nBonus, nAdv = ActorManager2.getCheck(node, "intelligence", sSkill);
			end

			if sStat ~= sStatSource then
				nAdv = nAdv - 1;
			end
			local nMethodic = DB.getValue(node, "magic.methodic", 0);
			if nMethodic ~= nMethodicSource then
				nAdv = nAdv - 1;
			end
			local bKnows = ActorManager2.knowsSpell(node, msgOOB.sName);
			if bKnows then
				nAdv = nAdv + 1;
			end
			local bElemental = DB.getValue(node, "abilities.elemental.current", 0) > 0;
			if bElemental or bElementalSource then
				local sGender = DB.getValue(node, "gender", ""):lower();
				sGender = DataCommon.gender_translate[gender_translate];
				if sGender == sGenderOpposed then
					bAutofail = true;
				elseif sGender ~= sGenderSource then
					nAdv = nAdv - 1;
				end
			end

			if EffectManagerSS.hasAnyEffectCondition(rActor, {DataCommon.conditions["Asleep"], DataCommon.conditions["Unconscious"], DataCommon.conditions["Petrified"], DataCommon.conditions["Dead"] }) then
				bAutofail = true;
			elseif EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Stunned"])then
				nAdv = nAdv - 1;
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Distracted"]) then
				nAdv = nAdv - 1;
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Blinded"]) then
				nAdv = nAdv - 1;
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Confused"]) then
				nAdv = nAdv - 1;
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Maddened"])  then
				nAdv = nAdv - 1;
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Fascinated"]) then
				nAdv = nAdv - 1;
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Trance"]) then
				nAdv = nAdv - 1;
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Deafened"]) then
				nBonus = nBonus - 3;
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Dazzled"]) then
				nBonus = nBonus - 3;
			end

			nAdv = nAdv + ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["eagle_eye"])

			local nDist = CombatManager2.getTokenDistance(rSource, rActor);
			if nDist and nStrengthSource and nDist > nStrengthSource then
				bAutofail = true;
			end

			local nResult = CombatManager2.simpleRoll("dT", nAdv) + nBonus;
			nResult = (nResult - msgOOB.nDif) / DataCommon.check_grade;
			
			-- Save info
			if nResult > 0 then
				aResults[sName] = math.floor(nResult) + 1;
			end
			
			-- Build the message only for PCs
			if bIsPC and nResult >= 0 then
				local rMessage = {mode="chat_skill", font="chatfont", icon="char_abilities_blue"};
				rMessage.text = Interface.getString("identifyspell"):upper() ..  "\n";

				if nResult >= 3 and not bKnows then
					addResearchSpell(node, msgOOB.sName);
				end

				if msgOOB.bCritSuccess then
					rMessage.text = rMessage.text .. "\n-" .. Interface.getString("undispersable");
				end

				if nResult >= 3 or (nResult >= 2 and bKnows) then
					rMessage.text = rMessage.text .. "\n-" .. Interface.getString("power_spell") .. ": " .. msgOOB.sName;
				end

				if nResult >= 2 then
					if msgOOB.sTechniques ~= "" then
						rMessage.text = rMessage.text .. "\n-" .. Interface.getString("library_recordtype_label_technique") .. ": " .. msgOOB.sTechniques;
					else
						rMessage.text = rMessage.text .. "\n-" .. Interface.getString("library_recordtype_label_technique") .. ": " .. Interface.getString("none") ;
					end
				end

				if nResult >= 1 then
					rMessage.text = rMessage.text .. "\n-" .. Interface.getString("level") .. ": " .. msgOOB.sLevel;
					rMessage.text = rMessage.text .. "\n-" .. Interface.getString("ref_label_school") .. ": " .. msgOOB.sSchool;
				end

				rMessage.text = rMessage.text .. "\n-" .. Interface.getString("random_item_label_type_magic") .. ": " .. msgOOB.sMagic;
				rMessage.text = rMessage.text .. "\n-" .. Interface.getString("random_item_label_type_spell") .. ": " .. msgOOB.sType;
				if bElem then
					rMessage.text = rMessage.text .. "\n-" .. Interface.getString("ref_label_elements") .. ": " .. msgOOB.sElement;
				end

					-- Send info
				local sOwner = node.getOwner();
				if sOwner and sOwner ~= "" then
					Comm.deliverChatMessage(rMessage, sOwner);
				end

			-- TODO: Critical failure gives wrong info

			end	
		end
	end
	-- Notify results to GM
	local bPrint=false;
	local rMessage = {mode="chat_skill", font="chatfont", icon="char_abilities_blue"};
	rMessage.text = Interface.getString("identifyspell"):upper() ..  "\n";
	for sName, nRes in pairs(aResults) do
		rMessage.text = rMessage.text .. "\n-" .. sName .. ": " .. tostring(nRes);
		bPrint = true;
	end
	if bPrint then
		Comm.deliverChatMessage(rMessage, "");
	end
end

function addResearchSpell(nodeChar, sNameSpell)
	-- Get spell node and info
	local nodeSpell = PowerManager.findRefPower(sNameSpell);
	if not nodeSpell then
		return;
	end

	local nLevel = DB.getValue(nodeSpell, "level", 0);
	local nMinMast = DataCommon.research_spell_min_mastery[nLevel];
	local sSource = DB.getValue(nodeSpell, "source", ""):lower();

	-- Check if the char can research spells of those level
	local rActor = ActorManager.resolveActor(nodeChar);
	local sSkill = Interface.getString("skill_value_investigation");
	local nodeSkill = getSkillNode(rActor, sSkill);
	local nMast = DB.getValue(nodeSkill, "prof", 0);
	local sMast = "char_tooltip_prof_" .. tostring(nMast);
	if nMinMast == 5 then
		if nMast < 4 then
			return;
		end
		if DB.getValue(nodeChar, "level", 0) < 10 then
			return;
		end
		sMast = "char_tooltip_prof_5"
	elseif nMinMast > nMast then
		return;
	end
	
	-- Check if the character can cast the spell
	local nodeProfs = nodeChar.createChild("classes");
	local bAdd = false;
	for _,v in pairs(nodeProfs.getChildren()) do
		local sMagic = DB.getValue(v, "magic_type", "");
		local bCond1 = sSource:find(sMagic);
		local bCond2 = DB.getValue(nodeChar, "abilities." .. sMagic .. ".current", 0) > 0
		if bCond1 and bCond2 then
			bCond = true;
			local sCat = DB.getValue(v, "magic_category", "");
			local nProfLevel = DB.getValue(v, "level", 0);
			local nMult = DataCommon.max_spell_mult[sCat] or 1;
			local nLevelMagic = nProfLevel * nMult;
			if nLevelMagic > nLevel then
				bAdd = true;
				break;
			end
		end
	end

	-- Add research
	if bAdd then
		local sRarity = DB.getValue(nodeSpell, "rarity", "");
		local nodeRecipes = nodeChar.createChild("recipeslist");
		local nodeRecipe = nodeResearch.createChild();
		local nDays = DataCommon.recipe_time[sMast][nMinMast] * DataCommon.recipe_time_research_mult;
		local sNameSpell = StringManager.capitalize(DataCommon.recipe_research_name)  .. " " .. sNameSpell;
		DB.setValue(nodeRecipe, "days", "number", nDays);
		DB.setValue(nodeRecipe, "name", "string", sNameSpell);
		DB.setValue(nodeRecipe, "skill", "string", sSkill);
		DB.setValue(nodeRecipe, "category", "string", sMast);
		DB.setValue(nodeRecipe, "category_show", "string", Interface.getString(sMast));
		if sRarity == "spell_rare" then
			DB.setValue(nodeRecipe, "rarity", "string", "recipe_rare");
		elseif sRarity == "spell_lost" then
			DB.setValue(nodeRecipe, "rarity", "string", "recipe_lost");
		end
	end

end

function getMisscastTableIndex(nRand, aIndices)
	local nIndex = 0;
	for _,v in pairs(aIndices) do
		if nRand <= v then
			return v;
		end
	end
	return nIndex;
end


function handleMisscast(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local aTable = DataCommon.misscast_levels[tonumber(msgOOB.nLevel)];
	local nRand = math.random(100);
	local nIndex = getMisscastTableIndex(nRand, aTable["indices"]);

	-- Irresistible
	msg = {font = "msgfont", icon = "roll_misscast", mode="chat_miss"}
	if nIndex == 0 then
		msg.mode = "chat_success";
		msg.text = Interface.getString("cast_chat_irresistible");
		Comm.deliverChatMessage(msg);

	-- Misscast tables
	elseif #aTable[nIndex] > 0 then
		aTable = aTable[nIndex];
		msg.text = table.concat(aTable, ", ");
		if ActorManager2.hasAptitude(rSource, DataCommon.aptitude["chaotic_magic"]) and math.random(2) == 2 then
			msg.mode = "chat_unknown";
			msg.text = msg.text .. Interface.getString("");
		end
		Comm.deliverChatMessage(msg);

		local tableRoll={};
		tableRoll.bSecret = true;
		for _,sTable in pairs(aTable) do
			tableRoll.nodeTable = TableManager.findTable(sTable);
			TableManager.performRoll(nil, rSource, tableRoll);
		end
	end
end

function notifyMisscast(nLevel, rSource)
	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_MISSCAST;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;
	msgOOB.nLevel = nLevel;

	Comm.deliverOOBMessage(msgOOB, "");
end

--------------------------
-- CAST SPELL
--------------------------

function perormCastSpell(draginfo, rActor, rAction)

	if Input.isControlPressed() then

		local rRoll = getAbortSpellRoll(rActor, rAction);

		ActionsManager.performAction(draginfo, rActor, rRoll);

	else

		local rRoll = getCastSpellRoll(rActor, rAction);

		ActionsManager.performAction(draginfo, rActor, rRoll);
	end
end


function getCastSpellRoll(rActor, rAction)
	local rRoll = {};
	local aProps = scanKeywords(rAction.keywords);
	local sReject = "";

	rRoll.sType = ACTION_CASTSPELL;
	rRoll.aDice = {};
	rRoll.nMod = 0;

	-- Calculate focus effects
	local aFilter = {rAction.label:lower()}
	if rAction.school and rAction.school ~= "" then
		table.insert(aFilter, rAction.school:lower())
	end
	if rAction.stat and rAction.stat ~= "" then
		table.insert(aFilter, rAction.stat:lower());
	end
	local nFocus, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["FOCUS"]}, true, aFilter);

	-- TODO: In progress: check avoided components
	local listExtra = {};
	local nSpellComp = scanComponents(rAction.sComp);
	-- Concentration
	local nMaxConc = ActorManager2.getConcentrationMax(rActor);
	local _, nConc = ActorManager2.hasConcentrationEffects(rActor);
	local bComp, bMandatory = hasComponent(rAction.sComp, DataCommon.magic_components["concentration"]);
	if bComp and nConc >= nMaxConc and rAction.nConc == 0 then
		if bMandatory then
			sReject = "roll_cast_autofail_conc";
		else
			table.insert(listExtra, DataCommon.magic_components["concentration"]);
		end
	end
	-- Is muted and words
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Muted"]) then
		bComp, bMandatory = hasComponent(rAction.sComp, DataCommon.magic_components["words"]);
		if bComp and bMandatory then
			sReject = "roll_cast_autofail_words";
		elseif bComp then
			table.insert(listExtra, DataCommon.magic_components["words"]);
		end
	end
	-- If is blinded and requires vision
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Blinded"]) then
		bComp, bMandatory = hasComponent(rAction.sComp, DataCommon.magic_components["vision"]);
		if bComp and bMandatory then
			sReject = "roll_cast_autofail_vision";
		elseif bComp then
			table.insert(listExtra, DataCommon.magic_components["vision"]);
		end
	end
	-- Is in melee and gestures
	local bCond1 = EffectManagerSS.hasAnyEffectCondition(rActor, {DataCommon.conditions["Entangled"], DataCommon.conditions["Inmovilized"], DataCommon.conditions["Paralized"]});
	local bCond2 = CombatManager2.isInMelee(rActor);
	local nCat = ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["war_gestures"]);
	local bApt = ActorManager2.hasAptitude(rActor, DataCommon.aptitude["combat_casting"]);
	local nWeapons = ActorManager2.getNumberEquipedWeapons(rActor);
	local bCond3 = nWeapons > 1;
	local bOpportunityAtt = false;
	if bCond1 or bCond2 or bCond3 then
		bComp, bMandatory = hasComponent(rAction.sComp, DataCommon.magic_components["gestures"]);
		if bComp then
			if bMandatory and (bCond1 or (bCond3 and nCat == 0)) then
				sReject = "roll_cast_autofail_gestures";
			elseif bMandatory and bCond2 and not (bApt or (nWeapons > 0 and nCat > 1)) then
				bOpportunityAtt = true;
			elseif not (bCond3 and (nCat > 0)) then
				table.insert(listExtra, DataCommon.magic_components["gestures"]);
			end
		end
	end
	-- Material and gem
	local bCompM, bMandatoryM = hasComponent(rAction.sComp, DataCommon.magic_components["material"]);
	local bCompG = hasComponent(rAction.sComp, DataCommon.magic_components["gem"]);
	local sNameGem = DataCommon.magic_gems[rAction.level]:lower();
	if bCompM or bCompG then
		local nodeActor = ActorManager2.getActorAndNode(rActor);
		local nodeList = DB.createChild(nodeActor, "inventorylist");
		local nodeMat, nodeGem;
		for _,nodeItem in pairs(nodeList.getChildren()) do
			local sName = DB.getValue(nodeItem, "name", ""):lower();
			if sName == DataCommon.magic_materials:lower() then
				nodeMat = nodeItem;
			elseif sName == sNameGem then
				nodeGem = nodeItem;
			end
		end
		if bCompM then
			local nMat = DB.getValue(nodeMat, "count", 0);
			if nMat > 0 then
				DB.setValue(nodeMat, "count", "number", nMat - 1);
			elseif bMandatoryM then
				sReject = "roll_cast_autofail_material";
			else
				table.insert(listExtra, DataCommon.magic_components["material"]);
			end
		end
		if bCompG then
			local nGem = DB.getValue(nodeGem, "count", 0);
			if nGem > 0 then
				DB.setValue(nodeGem, "count", "number", nGem - 1);
			else
				sReject = "roll_cast_autofail_gem";
			end
		end
	end

	
	-- Don't save unnecesary components
	local nExtraComp = #listExtra;
	if rAction.saved_comp + nExtraComp > nSpellComp then
		nExtraComp = math.max(nSpellComp - rAction.saved_comp, 0);
	end
	if nExtraComp > 0 then
		rAction.increased_unstable = rAction.increased_unstable + nExtraComp;
		rAction.increased_dif = rAction.increased_dif + 3 * nExtraComp;
	end
	-- Redo list of techniques
	if nExtraComp > 0 then
		local sTechniques = DataCommon.techniques["components"];
		for _,v in pairs(listExtra) do
			sTechniques = sTechniques .. " " .. v;
		end
		if rAction.saved_comp > 0 then
			sTechniques = sTechniques .. " + " .. tostring(rAction.saved_comp);
		end
		local nKey = 0;
		for k,v in pairs(rAction.list_techniques) do
			if v:find(DataCommon.techniques["components"]) then
				nKey = k;
				rAction.list_techniques[k] = sTechniques;
			end
		end
		if nKey == 0 then
			table.insert(rAction.list_techniques, sTechniques);
		end
	end

	-- Calculate increased / decreased effects
	rRoll.level = rAction.level;
	rRoll.nEffects, rRoll.nMisscast = WindsManager.getCurrentWinds();
	rRoll.nEffects = rRoll.nEffects + rAction.overchannel + nFocus;
	rRoll.nOverchannel = rAction.overchannel;
	nEffectsLastSpell = rRoll.nEffects;


	-- Calculate misscast level
	rRoll.nMisscast = rRoll.nMisscast + rAction.level +  rAction.increased_unstable + aProps["unstable"];
	rRoll.blood_magic = 0;
	if aProps["blood_magic"] then
		rRoll.nMisscast = rRoll.nMisscast + 1;
		rRoll.blood_magic = 1;
	end
	rRoll.nMisscast = math.max(rRoll.nMisscast, 0);
	rRoll.nMisscast = math.min(rRoll.nMisscast, DataCommon.max_spell_level);

	-- Calculate difficulty
	if aProps["demanding"] then
		rRoll.nDemanding = 1;
	else
		rRoll.nDemanding = 0;
	end
	local cond1 = ActorManager2.getPenalizer(rActor) < 0;
	local cond2 = aProps["demanding"];
	local cond3 = rAction.increased_dif > 0 or rAction.overchannel > 0 or rAction.saved_comp > 0;
	local cond4 = rAction.type_spell == "power_ritual";
	if rAction.level + rRoll.nEffects < rAction.min_level or sReject ~= "" then
		rRoll.nDif = 99;
		sReject = "roll_cast_autofail";
	elseif cond1 or cond2 or cond3 or cond4 then
		rRoll.nDif = DataCommon.spell_cast_DC_base + rAction.level + rAction.increased_dif;
	else
		rRoll.nDif = -99;
	end

	-- Calculate if performed spell level is higher than maximum for character
	local nMaxLevelChar = ActorManager2.calculateMaxSpellLevel(rActor, rAction.stat, rAction.type_spell == "power_ritual");
	if nMaxLevelChar < rAction.level then
		rRoll.nDif = 99;
		sReject = "roll_cast_autofail_level";
	end

	-- Actions
	rRoll.sActions = rAction.actions;
	rRoll.nExtraActions = rAction.increased_actions;

	-- Concentration info
	rRoll.nConc = rAction.nConc;
	rRoll.nDur = rAction.nDur;
	rRoll.nActEffects = rAction.nActEffects;
	rRoll.spellName = rAction.label;
	rRoll.nHiddenSpell = 0;
	if aProps["hidden"] then
		rRoll.nHiddenSpell = 1;
	end

	-- Save other info
	rRoll.stat = rAction.stat;
	rRoll.skill = rAction.skill;
	rRoll.nAdv = 0;
	
	if rAction.disadv_test then
		rRoll.nAdv = -rAction.disadv_test;
	end
	rRoll.sVigor = rAction.sVigor;
	rRoll.nIncreasedVigor = rAction.increased_vigor;
	rRoll.essence_cost = rAction.essence_cost;
	rRoll.essence_target = rAction.essence_target;
	rRoll.sSchool = rAction.school;
	rRoll.sTypeSpell = rAction.type_spell;
	rRoll.sElements = rAction.elements;
	rRoll.nIncreasedDif = rAction.increased_dif;
	rRoll.nFinalLevel = rAction.level + rRoll.nEffects;

	-- Message
	rRoll.sDesc = Interface.getString("roll_cast_cast") .. rAction.label .. Interface.getString("roll_cast_level") .. rRoll.nFinalLevel;
	if rRoll.nEffects ~= 0 then
		rRoll.sDesc = rRoll.sDesc .. Interface.getString("roll_cast_orig_level") .. rAction.level .. ")";
	end
	if nFocus > 0 then
		rRoll.sDesc = rRoll.sDesc .. Interface.getString("roll_cast_focus") .. nFocus .. "]";
	end

	if rRoll.nDif > 60 then
		if sReject ~= "" then
			rRoll.sDesc = rRoll.sDesc ..  string.format(Interface.getString(sReject), sNameGem);
		else
			rRoll.sDesc = rRoll.sDesc ..  Interface.getString("roll_cast_autofail");
		end
	elseif rRoll.nDif < -60 then
		rRoll.sDesc = rRoll.sDesc .. " [" .. Interface.getString("autosuccess") .. "]";
	else
		rRoll.sDesc = rRoll.sDesc .. Interface.getString("roll_cast_dif") .. rRoll.nDif .. "]";
	end

	if bOpportunityAtt then
		rRoll.sDesc = rRoll.sDesc ..  Interface.getString("roll_cast_op_attack");
	end

	local sTechniques = "";
	local bAdd = false;
	if #rAction.list_techniques > 0 then
		for _,v in pairs(rAction.list_techniques) do
			rRoll.sDesc = rRoll.sDesc .. "[" .. v .. "]";
			if bAdd then
				sTechniques = sTechniques .. ", ";
			end
			bAdd = true;
			sTechniques = sTechniques .. v;
		end
	end
	rRoll.sTechniques = sTechniques;

	return rRoll;
end

function modCastSpell(rSource, rTarget, rRoll)
	--TODO
end

function onCastSpell(rSource, rTarget, rRoll)
	-- Message Start cast
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	rMessage.mode = "chat_unknown";
	-- Comm.deliverChatMessage(rMessage);
	MessageManager.onRollMessage(rMessage);

	-- Spend actions
	if CombatManager2.inCombat() and rRoll.sActions and rRoll.sActions ~= "" then
		local aActions, sActions = getArrayOfActions(rRoll.sActions, rRoll.nExtraActions or 0, rRoll.level, rRoll.sTypeSpell == "power_ritual");
		local nodeCT = ActorManager2.getCTNode(rSource);
		bSuccess = CombatManager2.useAction(nodeCT, aActions);
		if not bSuccess then
			return;
		end
		if sActions and sActions ~= "" then
			rRoll.sActions = sActions;
		end
	end

	-- Make the misscast roll
	local nExtraMiss, _ = EffectManagerSS.getEffectsBonus(rSource, {DataCommon.keyword_states["MISSCAST"]}, true, {rRoll.label, rRoll.school});
	rRoll.nMisscast = math.max(0, math.min(rRoll.nMisscast + nExtraMiss, DataCommon.misscast_max));
	notifyMisscast(rRoll.nMisscast, rSource);

  -- Get the skill
	local node_skill, isSpecialty, node_specialty = ActorManager2.getSkillNode(rSource, rRoll.skill);
	if isSpecialty then
		node_skill = node_specialty;
	end

	-- Get the action table
	local rAction = {};
	rAction.type = "cast";
	if isSpecialty then
		rAction.nodeSkill = node_specialty;
	else
		rAction.nodeSkill = node_skill;
	end
	rAction.bSpecialty = isSpecialty;
	rAction.sStat = rRoll.stat;
	rAction.nDif = rRoll.nDif;
	rAction.nAdv = rRoll.nAdv;
	rAction.bSecret = rRoll.bSecret;
	rAction.nEffects = rRoll.nEffects;
	rAction.nMisscast = rRoll.nMisscast;
	rAction.nLevel = rRoll.level;
	rAction.nFinalLevel = rRoll.nFinalLevel;
	rAction.nOverchannel = rRoll.nOverchannel
	rAction.nConc = rRoll.nConc;
	rAction.nDur = rRoll.nDur;
	rAction.nActEffects = rRoll.nActEffects;
	rAction.spellName = rRoll.spellName;
	rAction.nHiddenSpell = rRoll.nHiddenSpell;
	rAction.nDemanding = rRoll.nDemanding;
	
	rAction.sVigor = rRoll.sVigor;
	rAction.increased_vigor = rRoll.nIncreasedVigor;
	rAction.essence_cost = rRoll.essence_cost;
	rAction.essence_target = rRoll.essence_target;
	rAction.cost_vitality = rRoll.blood_magic;	
	rAction.school = rRoll.sSchool;
	rAction.type_spell = rRoll.sTypeSpell;
	rAction.elements = rRoll.sElements;
	rAction.sTechniques = rRoll.sTechniques;
	rAction.increased_dif = rRoll.nIncreasedDif;
	rAction.sActions = rRoll.sActions;

	-- Make the skill roll
	ActionSkill.performRoll(nil, rSource, rAction);
end

----------------------------------
-- ABORT SPELL
----------------------------------

function getAbortSpellRoll(rActor, rAction)
	local rRoll = {};

	rRoll.sType = ACTION_ABORTSPELL;
	rRoll.aDice = {};
	rRoll.nMod = 0;

	for k,v in pairs(rAction) do
		rRoll[k] = v;
	end

	-- Message
	rRoll.sDesc = Interface.getString("roll_abort_abort") .. rAction.label .. Interface.getString("roll_cast_level") .. rAction.level;

	return rRoll;
	
end

function modAbortSpell(rSource, rTarget, rRoll)
	--TODO
end

function onAbortSpell(rSource, rTarget, rRoll)
	-- Demanding spells cannot be aborted
	local aProps = scanKeywords(rRoll.keywords);
	if aProps["demanding"] then
		local msg = {font = "msgfont", icon = "roll_misscast", mode="chat_miss", text=Interface.getString("cast_chat_abort_demanding_spell")};
		Comm.deliverChatMessage(msg);
		return;
	end


	-- Create heal action	
	local rAction = {};
	-- rAction.sText = "Testeando";
	-- rAction.label = "Intentando";
	rAction.sSubtype = "power_label_heal_vigor";
	rAction.nPiercing = 0;
	rAction.nMajorHealing = 1;
	rAction.nMagic = false;
	rAction.nAvoidNotifyAction = 1;
	rAction.sTargeting = "self";
	rAction.nTemporal = 0;
	rAction.sTypeHealing = DataCommon.magic_keywords["major_heal"];

	-- Calculate vigor/vitality recovery
	local nCost = 0;
	if rRoll.sVigor == DataCommon.standard then
		if aProps["blood_magic"] then
			nCost = math.max(nLevel, 1);
			rAction.sSubtype = "power_label_heal_vit";
		else
			nCost = ActorManager2.calculateSpellVigorCost(rSource, rRoll.level, rRoll.stat);
		end
	else
		local sTypeCost
		nCost, sTypeCost = PowerManager.getActivityCost(rRoll.sVigor);
		if sTypeCost == "vitality" then
			rAction.sSubtype = "power_label_heal_vit";
		elseif sTypeCost ~= "vigor" then
			nCost = 0;
		end
	end
	rAction.nTotal = nCost + rRoll.increased_vigor;

	ActionHeal.notifyApplyHeal(rSource, rSource, rAction);

	-- Recover essence
	if rRoll.essence_cost > 0 and rAction.essence_target ~= "" then
		rAction.sSubtype = "power_label_heal_essence";
		rAction.nTotal = rRoll.essence_cost;

		if rRoll.essence_target ~= Interface.getString("essence_target_target") then
			ActionHeal.notifyApplyHeal(nil, rSource, rAction);
		end
		if rRoll.essence_target ~= Interface.getString("essence_target_channeler") then
			local aTargets = TargetingManager.getFullTargets(rSource);
			for _,rTarget in pairs(aTargets) do
				ActionHeal.notifyApplyHeal(nil, rTarget, rAction);
			end
		end
	end

end


----------------------------------
-- UTILITY
----------------------------------

function scanKeywords(sProp)
	if not sProp then
		sProp = "";
	end
	sProp = sProp:lower();
	local aProp = {};

	for key, string in pairs(DataCommon.magic_keywords) do
		if sProp:find(string) then
			aProp[key] = true;
		else
			aProp[key] = false;
		end
	end
	for key, string in pairs(DataCommon.magic_keywords_number) do
		local sDefault = "0";
		aProp[key] = tonumber(sProp:match(string) or sDefault);
	end

	return aProp

end

function scanComponents(sProp)
	local aProp = StringManager.split(sProp, ",")
	local nComp = 0;
	for _,v in pairs(aProp) do
		if not v:find(DataCommon.mandatory) then
			nComp = nComp + 1;
		end
	end
	return nComp;
end

function hasComponent(sProp, sComp)
	local aProp = StringManager.split(sProp, ",");
	for _,v in pairs(aProp) do
		if v:find(sComp) then
			if v:find(DataCommon.mandatory) then
				return true, true;
			else
				return true, false;
			end
		end
	end
	return false, false;
end

function getArrayOfActions(sActions, nExtraActions, nLevel, bRitual, bIcons)
	local sOut;
	local sIcons = "";
	if bIcons then
		sIcons = "action_";
	end
	local aActions = {};
	-- Standard case, easy
	if sActions == DataCommon.standard:lower() then
		if bRitual then
			sOut = DataCommon.magic_standard_actions_ritual[nLevel];
			aActions = {};
		else
			-- aActions = DataCommon.magic_standard_actions[nLevel];
			aActions = {};
			local nTotalActions = math.floor(nLevel/3) + 1;
			for ind = 1,nTotalActions do
				table.insert(aActions, sIcons .. "action");
			end
			if #aActions == 1 then
				sOut = StringManager.capitalize(DataCommon.type_actions_label["action"]);
			else
				sOut = string.format(DataCommon.type_actions_label_plural["action"], #aActions);
			end
		end
	else
		-- This is more dificult
		sOut = sActions;
		aActions = {};
		for _,sAction in pairs(StringManager.split(sActions, ",")) do
			sAction = StringManager.trim(sAction);
			-- First, multiple actions. In principle, only standard actions are multiple
			nActions = sAction:match(DataCommon.type_actions_matcher["action"]);
			if nActions then
				for ind = 1,nActions do
					table.insert(aActions, sIcons .. "action");
				end
			end
			-- Now, individual. Start by reactions and complex actions to avoid matching with standard actions when unnecessary
			for sKeyAction,sMatcher in pairs(DataCommon.type_actions_label) do
				if sActions == sMatcher then
					table.insert(aActions, sIcons .. sKeyAction);
				end
			end
		end
	end

	-- Extra actions
	if nExtraActions > 0 then
		if nExtraActions > math.floor(nExtraActions) then
			table.insert(aActions, sIcons .. "fast");
			nExtraActions = math.floor(nExtraActions);
		end
		for ind = 1,nExtraActions do
			table.insert(aActions, sIcons .. "action");
		end
	end
	
	return aActions, sOut;
end