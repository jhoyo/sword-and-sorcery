--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_MESSAGE2GM = "message_gm";

function onInit()
    OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_MESSAGE2GM, handleMessage2GM);

    -- Override CoreRPG
    ActionsManager.messageResult = messageResult;
end



function onRollMessage(rMessage, rSource)
    if rMessage.secret then
        Comm.addChatMessage(rMessage);
    elseif Session.IsHost then
        -- Send it to GM
        Comm.addChatMessage(rMessage);
        -- The owning the character should receive it fully
        local sOwner;
        if rSource then
            local _, nodeActor = ActorManager.getTypeAndNode(rSource);
            sOwner = nodeActor.getOwner();
        end
        -- Send it (transformed) to the rest of players
        sendMessageToUsers(rMessage, sOwner);
    else
        -- Comm.addChatMessage(rMessage);
        rMessage.sOwner = Session.UserName;
        sendMessageToGM(rMessage);
        -- Comm.deliverChatMessage(rMessage, "");
    end
end

function sendMessageToUsers(rMessage, sOwner)
    local sOriginal, sTransformed = transformText(rMessage.text);
    local sMode = rMessage.mode;
    local aUsers = User.getActiveUsers();
    for _, sUser in pairs(aUsers) do
        if sUser and sUser == sOwner then
            rMessage.text = sOriginal;
            rMessage.mode = sMode;
        else
            rMessage.text = sTransformed;
            rMessage.mode = "chat_unknown";
        end
        Comm.deliverChatMessage(rMessage, sUser);
    end
end

function sendMessageToGM(rMessage)
    -- Info
    local msgOOB = {};

    -- Transform message into OOB message
    for k, v in pairs(rMessage) do
        msgOOB[k] = v;
    end
    -- Transform some fields
    msgOOB.type_original = rMessage.type;
    if rMessage.dice then
        for i, v in ipairs(rMessage.dice) do
            msgOOB["dice_" .. i .. "_value"] = v.value;
            msgOOB["dice_" .. i .. "_type"] = v.type;
            msgOOB["dice_" .. i .. "_result"] = v.result;
        end
        msgOOB.dice_expr = rMessage.dice.expr;
    end
    
    -- Deliver to GM client
    msgOOB.type = OOB_MSGTYPE_MESSAGE2GM;
    Comm.deliverOOBMessage(msgOOB, "");
end

function handleMessage2GM(msgOOB)
    -- Transform OOB message into chat message
    -- local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);

    -- Recover all info
    local rMessage = {};
    for k, v in pairs(msgOOB) do
        rMessage[k] = v;
    end

    -- Recover other info
    if msgOOB.type_original then
        rMessage.type = msgOOB.type_original;
    end
    rMessage.dice = {};
    local bAddExpr = false;
    for i = 1, 100 do
        if msgOOB["dice_" .. i .. "_value"] then
            local rDice = {};
            rDice.value = msgOOB["dice_" .. i .. "_value"];
            rDice.type = msgOOB["dice_" .. i .. "_type"];
            rDice.result = msgOOB["dice_" .. i .. "_result"];
            table.insert(rMessage.dice, rDice);
        else
            bAddExpr = i > 1;
            break;
        end
    end
    if bAddExpr and msgOOB.dice_expr then
        rMessage.dice.expr = msgOOB.expr_original;
    end

    -- Deliver to GM
    Comm.addChatMessage(rMessage);
    -- Deliver to the rest of players
    sendMessageToUsers(rMessage, rMessage.sOwner);
    -- Comm.deliverChatMessage(rMessage, "");
end

function transformText(sText)
    local nIndex = sText:find("\n", 1, true);
    local sTransformed = sText;
    if nIndex then
        sTransformed = sText:sub(1, nIndex - 1);
    end
    return sText, sTransformed;
end


--------------------------------------
-- Override CoreRPG
--------------------------------------


-- From ActionsManager
function messageResult(bSecret, rSource, rTarget, rMessageGM, rMessagePlayer)
	local bInvolvesFriendlyUnit = (not rSource or ActorManager.isFaction(rSource, "friend"));

	local bShowResultsToPlayer;
	local sOptSHRR = OptionsManager.getOption("SHRR");
	if sOptSHRR == "off" then
		bShowResultsToPlayer = false;
	elseif sOptSHRR == "pc" then
		bShowResultsToPlayer = bInvolvesFriendlyUnit;
	else
		bShowResultsToPlayer = true;
	end
	
	if bShowResultsToPlayer then
		local nodeCT = ActorManager.getCTNode(rTarget);
		if nodeCT and CombatManager.isCTHidden(nodeCT) then
			rMessageGM.secret = true;
			Comm.deliverChatMessage(rMessageGM, "");
		else
			rMessageGM.secret = false;
			Comm.deliverChatMessage(rMessageGM);
		end
	else
		rMessageGM.secret = true;
		Comm.deliverChatMessage(rMessageGM, "");
        if Session.IsHost then
            local aUsers = User.getActiveUsers();
            if #aUsers > 0 then
                Comm.deliverChatMessage(rMessagePlayer, aUsers);
            end
        else
            Comm.addChatMessage(rMessagePlayer);
        end
	end
end