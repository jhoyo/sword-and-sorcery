--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	EffectManager.registerEffectVar("sUnits", { sDBType = "string", sDBField = "unit", bSkipAdd = true });
	EffectManager.registerEffectVar("sApply", { sDBType = "string", sDBField = "apply", sDisplay = "[%s]" });
	EffectManager.registerEffectVar("sTargeting", { sDBType = "string", bClearOnUntargetedDrop = true });

	EffectManager.setCustomOnEffectAddStart(onEffectAddStart);
	EffectManager.setCustomOnEffectAddIgnoreCheck(onEffectAddIgnoreCheck);

	EffectManager.setCustomOnEffectExpire(onEffectExpire);

	EffectManager.setCustomOnEffectRollEncode(onEffectRollEncode);
	EffectManager.setCustomOnEffectTextEncode(onEffectTextEncode);
	EffectManager.setCustomOnEffectTextDecode(onEffectTextDecode);

	-- EffectManager.setCustomOnEffectActorStartTurn(onEffectActorStartTurn);
end

--
-- EFFECT MANAGER OVERRIDES
--

function onEffectExpire(nodeEffect, nExpireComp)
	-- -- Expire similar effects
	-- local nID = DB.getValue(nodeEffect, "ID", -1);
	-- Remove summoned NPCs or items
	local nID = DB.getValue(nodeEffect, "ID", 0);
	local sLabel = DB.getValue(nodeEffect, "label", "");
	local nodeCT = nodeEffect.getChild("...")
	local nodeChar = ActorManager2.getNodeFromCT(nodeCT);
	if nID > 0 then
	-- Check if something was summoned
		if sLabel:find(DataCommon.keyword_inactive["summon"] .. " " .. Interface.getString("npc")) then
			removeNPCbyID(nID);
		elseif sLabel:find(DataCommon.keyword_inactive["summon"] .. " " .. Interface.getString("item")) then
			removeItemByID(nID);
		elseif sLabel:find(DataCommon.keyword_states["DEFRANGEDMULT"]) or sLabel:find(DataCommon.keyword_states["DEFMULT"]) then
			ActorManager2.updateDefenses(nodeChar);
		end
	end
	if sLabel:find(DataCommon.ability_ltos["constitution"]) then		
		CharManager.calculateMaxVit(nodeChar);
		CharManager.calculateMaxVigor(nodeChar);
	end
	if sLabel:find(DataCommon.ability_ltos["willpower"]) then
		CharManager.calculateMaxVigor(nodeChar);
	end
	if sLabel:find(DataCommon.keyword_states["LEVEL"]) or sLabel:find(DataCommon.keyword_states["ESSENCE"]) then
		CharManager.calculateMaxEssence(nodeChar);
	end
	if sLabel:find(DataCommon.keyword_inactive["temp_vigor"]) then
		DB.setValue(nodeChar, "health.vigor.temp", "number", 0);
	end
	if sLabel:find(DataCommon.keyword_inactive["temp_vit"]) then
		DB.setValue(nodeChar, "health.vit.temp", "number", 0);
	end

	-- If it is an initiative modifier, modify its current initiative up to the current initiative
	local aEffectComps = EffectManager.parseEffect(sLabel);
	for kEffectComp,sEffectComp in ipairs(aEffectComps) do
		local rEffectComp = parseEffectComp(sEffectComp);
		if rEffectComp.type == DataCommon.keyword_states["INIT"] then
			-- Get initiative modifier
			local nMod = rEffectComp.mod;
			for _,sDice in pairs(rEffectComp.dice) do
				nMod = nMod + CombatManager2.simpleRoll(sDice);
			end
			-- Get current initiatives
			local nInitChar = DB.getValue(nodeCT, "initresult", 0) - nMod;
			if nMod < 0 then
				local nodeActive = CombatManager.getActiveCT();
				if nodeActive then
					local nInitCurrent = DB.getValue(nodeActive, "initresult", 0);
					nInitChar = math.min(nInitChar, nInitCurrent - 1);
				end
			end
			DB.setValue(nodeCT, "initresult", "number", nInitChar);
		elseif Utilities.inArray(DataCommon.conditions_not_reactions) or Utilities.inArray(DataCommon.conditions_not_actions) then
			-- TODO: Not useful because effect didnt expired yet
			CombatManager2.resetActions(nodeCT);
		end
	end

	-- Update token wide widgets associated with states
	TokenManager2.updateFullWidgets(nodeCT);
end

function onEffectAddStart(rEffect)
	rEffect.nDuration = rEffect.nDuration or 1;
	local nMult = DataCommon.turns_per_unit[rEffect.sUnits];
	if nMult then
		rEffect.nDuration = rEffect.nDuration * nMult;
	end
	rEffect.sUnits = "";
	-- Safety
	if rEffect.sTargetNode then
		local nodeCT = DB.findNode(rEffect.sTargetNode);
		local nodeChar = ActorManager2.getNodeFromCT(nodeCT);
		if rEffect.sName:find(DataCommon.keyword_states["DEFRANGEDMULT"]) or rEffect.sName:find(DataCommon.keyword_states["DEFMULT"]) then
			if rEffect.sTargetNode then
				ActorManager2.updateDefenses(nodeChar);
			end
		elseif rEffect.sName:find(DataCommon.ability_ltos["constitution"] .. ":") then
			if rEffect.sTargetNode then
				CharManager.calculateMaxVit(nodeChar);
				CharManager.calculateMaxVigor(nodeChar);
			end
		elseif rEffect.sName:find(DataCommon.ability_ltos["willpower"] .. ":") then
			if rEffect.sTargetNode then
				CharManager.calculateMaxVigor(nodeChar);
			end
		elseif rEffect.sName:find(DataCommon.keyword_states["LEVEL"] .. ":") then
			if rEffect.sTargetNode then
				CharManager.calculateMaxEssence(nodeChar);
			end
		elseif rEffect.sName:find(DataCommon.keyword_states["ESSENCE"] .. ":") then
			if rEffect.sTargetNode then
				CharManager.calculateMaxEssence(nodeChar);
			end
		end
	end
end

-- Improve to check inmunity and resistence
function onEffectAddIgnoreCheck(nodeCT, rEffect)
	local rSource = ActorManager.resolveActor(rEffect.sSource);
	local rTarget = ActorManager.resolveActor(nodeCT);
	local sOriginal = rEffect.sName;
	local bEnd = false;

	-- Remove other styles
	local cond1 = EffectManagerSS.hasEffectCondition(nodeCT, DataCommon.keyword_inactive["style"]);
	local cond2 = sOriginal:find(DataCommon.keyword_inactive["style"]);
	if cond1 and cond2 then
		EffectManager.removeEffect(nodeCT, DataCommon.keyword_inactive["style"]);
		EffectManager.message(Interface.getString("chat_removing_styles"), nodeCT, false, sUser);
	end

	-- Check inmunities
	aEffects = StringManager.split(sOriginal, ";");
	local nSens = 0;
	local aCancelled = {};
	for k, v in pairs(aEffects) do
		local sCheck = StringManager.trim(v):lower();
		local nSens2, nInmune, nAbsorb, _, _ = ActorManager2.getSensitivity(rTarget, sCheck, sOrigin);
		if nInmune > 0 or nAbsorb > 0 then
			table.insert(aCancelled, table.remove(aEffects, k))
		else
			nSens = nSens + nSens2;
		end
		if Utilities.inArray(DataCommon.conditions_end_concentration, StringManager.capitalize(v)) then
			bEnd = true;
		end
	end
	if #aCancelled > 0 then
		if #aEffects == 0 then
			return Interface.getString("chat_inmune_to_effect") .. table.concat(aCancelled, ", ");
		else
			rEffect.sName = table.concat(aEffects, "; ");
			local sMsg = Interface.getString("chat_inmune_partially") .. table.concat(aCancelled, ",");
			-- local sUser == ???
			EffectManager.message(sMsg, nodeCT, false, sUser);
		end
	elseif nSens > 0 then
		EffectManager.message(Interface.getString("chat_resist_effect"), nodeCT, false, sUser);
	elseif nSens < 0 then
		EffectManager.message(Interface.getString("chat_vulnerable_effect"), nodeCT, false, sUser);
	end

	-- Check if other concentration effects must be removed
	if bEnd then
		removeAllEffectsByNameAndOwner(rTarget, nil, true, true);
	end

	-- If an effect that gives temporal vitality or vigor is received, remove other
	local bVit = sOriginal:find(DataCommon.keyword_inactive["temp_vit"]);
	local bVigor = sOriginal:find(DataCommon.keyword_inactive["temp_vigor"]);
	if bVigor or bVit then
		for _, effect in pairs(nodeCT.getChild('effects').getChildren()) do
			local sLabel = DB.getValue(effect, "label", "");
			local aEffectComps = EffectManager.parseEffect(sLabel);
			local nMatchVit;
			local nMatchVigor;
			for k,sComp in pairs(aEffectComps) do
				if bVit and sComp == DataCommon.keyword_inactive["temp_vit"] then
					nMatchVit = k;
				end
				if bVigor and sComp == DataCommon.keyword_inactive["temp_vit"] then
					nMatchVigor = k;
				end
			end
			if nMatchVit then
				EffectManager.notifyExpire(effect, nMatchVit, true);
			elseif nMatchVigor then
				EffectManager.notifyExpire(effect, nMatchVigor, true);
			end
		end	
	end

	-- Some effects may force to recalculate defenses


	-- If it is an initiative modifier, modify its current initiative up to the current initiative
	local aEffectComps = EffectManager.parseEffect(sOriginal);
	for kEffectComp,sEffectComp in ipairs(aEffectComps) do
		local rEffectComp = parseEffectComp(sEffectComp);
		if rEffectComp.type == DataCommon.keyword_states["INIT"] then
			-- Get initiative modifier
			local nMod = rEffectComp.mod;
			for _,sDice in pairs(rEffectComp.dice) do
				nMod = nMod + CombatManager2.simpleRoll(sDice);
			end
			-- Get current initiatives
			local nInitChar = DB.getValue(nodeCT, "initresult", 0) + nMod;
			if nMod > 0 then
				local nodeActive = CombatManager.getActiveCT();
				if nodeActive then
					local nInitCurrent = DB.getValue(nodeActive, "initresult", 0);
					nInitChar = math.min(nInitChar, nInitCurrent - 1);
				end
			end
			DB.setValue(nodeCT, "initresult", "number", nInitChar);
		elseif Utilities.inArray(DataCommon.conditions_not_reactions) or Utilities.inArray(DataCommon.conditions_not_actions) then
			CombatManager2.resetActions(nodeCT);
		end
	end

	-- Update token wide widgets associated with states
	TokenManager2.updateFullWidgets(nodeCT);

	return nil;
end

function onEffectRollEncode(rRoll, rEffect)
	if rEffect.sTargeting and rEffect.sTargeting == "self" then
		rRoll.bSelfTarget = true;
	end
end

function onEffectTextEncode(rEffect)
	local aMessage = {};

	if rEffect.sUnits and rEffect.sUnits ~= "" then
		local sOutputUnits = nil;
		if rEffect.sUnits == "minute" then
			sOutputUnits = "MIN";
		elseif rEffect.sUnits == "hour" then
			sOutputUnits = "HR";
		elseif rEffect.sUnits == "day" then
			sOutputUnits = "DAY";
		end

		if sOutputUnits then
			table.insert(aMessage, "[UNITS " .. sOutputUnits .. "]");
		end
	end
	if rEffect.sTargeting and rEffect.sTargeting ~= "" then
		table.insert(aMessage, "[" .. rEffect.sTargeting:upper() .. "]");
	end
	if rEffect.sApply and rEffect.sApply ~= "" then
		table.insert(aMessage, "[" .. rEffect.sApply:upper() .. "]");
	end

	return table.concat(aMessage, " ");
end

function onEffectTextDecode(sEffect, rEffect)
	local s = sEffect;

	local sUnits = s:match("%[UNITS ([^]]+)]");
	if sUnits then
		s = s:gsub("%[UNITS ([^]]+)]", "");
		if sUnits == "MIN" then
			rEffect.sUnits = "minute";
		elseif sUnits == "HR" then
			rEffect.sUnits = "hour";
		elseif sUnits == "DAY" then
			rEffect.sUnits = "day";
		end
	end
	if s:match("%[SELF%]") then
		s = s:gsub("%[SELF%]", "");
		rEffect.sTargeting = "self";
	end
	if s:match("%[ACTION%]") then
		s = s:gsub("%[ACTION%]", "");
		rEffect.sApply = "action";
	elseif s:match("%[ROLL%]") then
		s = s:gsub("%[ROLL%]", "");
		rEffect.sApply = "roll";
	elseif s:match("%[SINGLE%]") then
		s = s:gsub("%[SINGLE%]", "");
		rEffect.sApply = "single";
	end

	return s;
end

function passTimeReduceEffectDuration(nTurns)
	local nodeList = DB.findNode(CombatManager.CT_LIST);
	for _,nodeCT in pairs(nodeList.getChildren()) do
		for _,nodeEffect in pairs(DB.getChildren(nodeCT, "effects")) do
			local nDuration = DB.getValue(nodeEffect, "duration", 0);
			-- TODO: Some effects may have activity at the start/end of each turn
			if nDuration > 0 then
				nDuration = nDuration - nTurns;
				if nDuration <= 0 then
					EffectManager.expireEffect(nodeCT, nodeEffect, 0);
				else
					DB.setValue(nodeEffect, "duration", "number", nDuration);
				end
			end
		end
	end
end

function onEffectActorStartTurn(nodeActor, nodeEffect)
	local sEffName = DB.getValue(nodeEffect, "label", "");
	local aEffectComps = EffectManager.parseEffect(sEffName);
	local nActive = DB.getValue(nodeEffect, "isactive", 0);

	-- Cost per turn
	local nCost = DB.getValue(nodeEffect, "cost", 0);
	if nCost > 0 and nActive == 1 then
		local sPath = DB.getValue(nodeEffect, "source_name", "");
		local nodeSource = nodeActor;
		if sPath ~= "" then
			nodeSource = DB.findNode(sPath);
		end

		if nodeSource then
			local nVitality = DB.getValue(nodeEffect, "cost_vitality", 0);
			local rAction = {};
			rAction.label = "Start turn cost";
			rAction.sTargeting = "self";
			rAction.nAvoidNotifyAction = 1;
			rAction.nMod = nCost;
			rAction.clauses = {};
			if nVitality == 0 then
				local aClause = {dmgtype=DataCommon.dmgtypes_modifier["nonlethal"]};
				rAction.clauses = {aClause};
			end

			local rRoll = ActionDamage.getRoll(nil, rAction);
			if EffectManager.isGMEffect(nodeSource, nodeEffect) then
				rRoll.bSecret = true;
			end
			ActionsManager.actionDirect(nil, "damage", { rRoll }, { { ActorManager.resolveActor(nodeSource) } });
		end
	end

	for _,sEffectComp in ipairs(aEffectComps) do
		local rEffectComp = parseEffectComp(sEffectComp);
		-- Conditionals
		if rEffectComp.type == "IFT" then
			break;
		elseif rEffectComp.type == "IF" then
			local rActor = ActorManager.resolveActor(nodeActor);
			if not checkConditional(rActor, nodeEffect, rEffectComp.remainder) then
				break;
			end

		-- Ongoing damage and regeneration
		elseif rEffectComp.type == DataCommon.keyword_states["DMGO"] or rEffectComp.type == DataCommon.keyword_states["REGEN"] or rEffectComp.type == DataCommon.keyword_states["TRUEREGEN"] then
			if nActive == 1 and CombatManager2.inCombat(nodeActor) then
				applyOngoingDamageAdjustment(nodeActor, nodeEffect, rEffectComp);
			-- elseif nActive == 2 then
			-- 	DB.setValue(nodeEffect, "isactive", "number", 1);
			end
		end
	end
	
	-- Activate skip effects
	if nActive == 2 then
		DB.setValue(nodeEffect, "isactive", "number", 1);
	end

	-- On start save to modify duration
	if DB.getValue(nodeEffect, "save_stat", "") ~= "" then
		-- processONSTARTeffect(nodeActor, nodeEffect, rEffectComp);
		processSAVESTARTeffect(nodeActor, nodeEffect, rEffectComp);
	end

	-- Reduce the duration of the effect
	if OptionsManager.isOption("EffectDuration", "start") then
		local nDuration = DB.getValue(nodeEffect, "duration", 0) - 1;
		if nDuration > 0 then
			DB.setValue(nodeEffect, "duration", "number", nDuration);
		elseif nDuration == 0 then
			EffectManager.expireEffect(nodeActor, nodeEffect, 0);
		end
	end

	-- TODO: Concentration checks if needed

end

function onEffectActorEndTurn(nodeEntry, nodeEffect)
	if OptionsManager.isOption("EffectDuration", "end") then
		local nDuration = DB.getValue(nodeEffect, "duration", 0) - 1;
		if nDuration > 0 then
			DB.setValue(nodeEffect, "duration", "number", nDuration);
		elseif nDuration == 0 then
			EffectManager.expireEffect(nodeEntry, nodeEffect, 0);
		end
	end
end





function removeNPCbyID(nID)
	for _,actor in pairs(CombatManager.getCombatantNodes()) do
		if nID == DB.getValue(actor, "nID", 0) then
			DB.deleteNode(actor);
		end
	end
end


function removeItemByID(nID)
	for _,actor in pairs(CombatManager.getCombatantNodes()) do
		-- PCs
		if ActorManager.isPC(actor) then
			local nodeActor = ActorManager2.getNodeFromCT(actor);
			local nodeInvlist = nodeActor.createChild("inventorylist");
			for _,item in pairs(nodeInvlist.getChildren()) do
				if nID == DB.getValue(item, "nID", 0) then
					CharManager.removeFromWeaponDB(item);
					-- DB.deleteNode(item); Not necessary
				end
			end
		-- NPCs
		else
			local nodeInvlist = actor.createChild("inventorylist");
			for _,item in pairs(nodeInvlist.getChildren()) do
				if nID == DB.getValue(item, "nID", 0) then
					DB.deleteNode(item);
				end
			end
		end
	end
end

-- Function that looks the number of effects that appear with a certain descriptor. If no descriptor, every correct effect is valid
function getNumberOfEffectsByType(rSource, sEffectType, aEffectDescriptor, rTarget, aOtherTypes, bDamageOther)
	-- Safety
	if not rSource or not sEffectType then
		return 0;
	end

	if not aEffectDescriptor then
		aEffectDescriptor = {};
	elseif type(aEffectDescriptor) == "string" then
		aEffectDescriptor = {aEffectDescriptor};
	end

	-- Look for the list of correct effects
	local aEffects = EffectManagerSS.getEffectsByType(rSource, sEffectType, aEffectDescriptor, rTarget, rTarget, aOtherTypes, bDamageOther);

	return #aEffects
end

--
-- CUSTOM FUNCTIONS
--

function parseEffectComp(s)
	local sType = nil;
	local aDice = {};
	local nMod = 0;
	local aRemainder = {};
	local nRemainderIndex = 1;

	local aWords, aWordStats = StringManager.parseWords(s, "%[%]%(%):!");
	if #aWords > 0 then
		sType = aWords[1]:match("^([^:]+):");
		if sType then
			nRemainderIndex = 2;

			local sValueCheck = aWords[1]:sub(#sType + 2);
			if sValueCheck ~= "" then
				table.insert(aWords, 2, sValueCheck);
				table.insert(aWordStats, 2, { startpos = aWordStats[1].startpos + #sType + 1, endpos = aWordStats[1].endpos });
				aWords[1] = aWords[1]:sub(1, #sType + 1);
				aWordStats[1].endpos = #sType + 1;
			end

			if #aWords > 1 then
				if StringManager.isDiceString(aWords[2]) then
					aDice, nMod = StringManager.convertStringToDice(aWords[2]);
					nRemainderIndex = 3;
				end
			end
		end

		if nRemainderIndex <= #aWords then
			while nRemainderIndex <= #aWords and aWords[nRemainderIndex]:match("^%[[%+%-]?%w+%]$") do
				table.insert(aRemainder, aWords[nRemainderIndex]);
				nRemainderIndex = nRemainderIndex + 1;
			end
		end

		if nRemainderIndex <= #aWords then
			local sRemainder = s:sub(aWordStats[nRemainderIndex].startpos);
			local nStartRemainderPhrase = 1;
			local i = 1;
			while i < #sRemainder do
				local sCheck = sRemainder:sub(i, i);
				if sCheck == "," then
					local sRemainderPhrase = sRemainder:sub(nStartRemainderPhrase, i - 1);
					if sRemainderPhrase and sRemainderPhrase ~= "" then
						sRemainderPhrase = StringManager.trim(sRemainderPhrase);
						table.insert(aRemainder, sRemainderPhrase);
					end
					nStartRemainderPhrase = i + 1;
				elseif sCheck == "(" then
					while i < #sRemainder do
						if sRemainder:sub(i, i) == ")" then
							break;
						end
						i = i + 1;
					end
				elseif sCheck == "[" then
					while i < #sRemainder do
						if sRemainder:sub(i, i) == "]" then
							break;
						end
						i = i + 1;
					end
				end
				i = i + 1;
			end
			local sRemainderPhrase = sRemainder:sub(nStartRemainderPhrase, #sRemainder);
			if sRemainderPhrase and sRemainderPhrase ~= "" then
				sRemainderPhrase = StringManager.trim(sRemainderPhrase);
				table.insert(aRemainder, sRemainderPhrase);
			end
		end
	end

	return  {
		type = sType or "",
		mod = nMod,
		dice = aDice,
		remainder = aRemainder,
		original = StringManager.trim(s)
	};
end

function rebuildParsedEffectComp(rComp)
	if not rComp then
		return "";
	end

	local aComp = {};
	if rComp.type ~= "" then
		table.insert(aComp, rComp.type .. ":");
	end
	local sDiceString = StringManager.convertDiceToString(rComp.dice, rComp.mod);
	if sDiceString ~= "" then
		table.insert(aComp, sDiceString);
	end
	if #(rComp.remainder) > 0 then
		table.insert(aComp, table.concat(rComp.remainder, ","));
	end
	return table.concat(aComp, " ");
end

function removeEffectByType(nodeCT, sEffectType)
	if not sEffectType then
		return;
	end
	local aEffectsToDelete = {};

	for _,nodeEffect in pairs(DB.getChildren(nodeCT, "effects")) do
		local nActive = DB.getValue(nodeEffect, "isactive", 0);
		if (nActive ~= 0) then
			local s = DB.getValue(nodeEffect, "label", "");

			local aCompsToDelete = {};

			local aEffectComps = EffectManager.parseEffect(s);
			local nComp = 1;
			for _,sEffectComp in ipairs(aEffectComps) do
				local rEffectComp = parseEffectComp(sEffectComp);
				-- Check conditionals
				if rEffectComp.type == "IFT" then
					break;
				elseif rEffectComp.type == "IF" then
					local rActor = ActorManager.resolveActor(nodeActor);
					if not checkConditional(rActor, nodeEffect, rEffectComp.remainder) then
						break;
					end

				-- Check for effect match
				elseif rEffectComp.type == sEffectType then
					table.insert(aCompsToDelete, nComp);
				end

				nComp = nComp + 1;
			end

			-- Delete portion of effect that matches (or register for full deletion)
			if #aCompsToDelete >= #aEffectComps then
				table.insert(aEffectsToDelete, nodeEffect);
			elseif #aCompsToDelete > 0 then
				local aNewEffectComps = {};
				local nEffectComps = #aEffectComps;
				for i = 1,nEffectComps do
					if not StringManager.contains(aCompsToDelete, i) then
						table.insert(aNewEffectComps, aEffectComps[i]);
					end
				end

				local sNewEffect = EffectManager.rebuildParsedEffect(aNewEffectComps);
				DB.setValue(nodeEffect, "label", "string", sNewEffect);
			end
		end
	end

	for _,v in ipairs(aEffectsToDelete) do
		v.delete();
	end
end

function checkImmunities(rSource, rTarget, rEffect, aOtherTypes)
	local aImmune = getEffectsByType(rTarget, "IMMUNE", {}, rSource, aOtherTypes);

	local aImmuneConditions = {};
	for _,v in pairs(aImmune) do
		for _,vType in pairs(v.remainder) do
			if vType ~= "" and vType:sub(1,1) ~= "!" and vType:sub(1,1) ~= "~" then
				if StringManager.contains(DataCommon.conditions, vType) then
					table.insert(aImmuneConditions, vType:lower());
				end
			end
		end
	end
	if #aImmuneConditions == 0 then
		return {};
	end

	local aNewEffectComps = {};
	local aCancelled = {};

	local aEffectComps = EffectManager.parseEffect(rEffect.sName);
	for _,sEffectComp in ipairs(aEffectComps) do
		local rEffectComp = parseEffectComp(sEffectComp);
		if StringManager.contains(aImmuneConditions, rEffectComp.original:lower()) then
			table.insert(aCancelled, rEffectComp.original);
		else
			table.insert(aNewEffectComps, sEffectComp);
		end
	end
	if #aCancelled == 0 then
		return {};
	end

	rEffect.sName = EffectManager.rebuildParsedEffect(aNewEffectComps);
	if rEffect.sName == "(C)" then
		rEffect.sName = "";
	end
	return aCancelled;
end

function applyOngoingDamageAdjustment(nodeActor, nodeEffect, rEffectComp, aDamageTypes)
	if #(rEffectComp.dice) == 0 and rEffectComp.mod == 0 then
		return;
	end

	local rTarget = ActorManager.resolveActor(nodeActor);

	if rEffectComp.type == DataCommon.keyword_states["REGEN"] then

		ActionHeal.applyRegeneration(rTarget, rEffectComp.dice, rEffectComp.mod)

	elseif rEffectComp.type == DataCommon.keyword_states["TRUEREGEN"] then

		ActionHeal.applyRegeneration(rTarget, rEffectComp.dice, rEffectComp.mod, true)

	else
		local rAction = {};
		rAction.label = "Ongoing damage";
		rAction.clauses = {};
		rAction.sTargeting = "self";
		rAction.nAvoidNotifyAction = 1;
		
		local aClause = {};
		aClause.dice = rEffectComp.dice;
		aClause.modifier = rEffectComp.mod;
		if aDamageTypes then
			aClause.dmgtype = aDamageTypes;
		else
			aClause.dmgtype = string.lower(table.concat(rEffectComp.remainder, ","));
		end
		table.insert(rAction.clauses, aClause);

		local rRoll = ActionDamage.getRoll(nil, rAction);
		if EffectManager.isGMEffect(nodeActor, nodeEffect) then
			rRoll.bSecret = true;
		end
		ActionsManager.actionDirect(nil, "damage", { rRoll }, { { rTarget } });
	end
end

function extractStatSkillMod(sString, sInitial)
		-- Look for just a number
		local nMod = sString:match(sInitial ..' %d+');
		local sStat, sSkill;

		-- Simple number
		if nMod then
			nMod = tonumber(nMod:sub(5));

		-- Stat + Skill
		else
			--Stat
			local sMatch = sString:match(sInitial .. ' %u+');
			local sMatch2 = sString:match(sInitial .. ' %a+');
			if sMatch then
				sStat = DataCommon.ability_stol[sMatch:sub(-3)];
			elseif sMatch2 then
				sStat = sMatch2:sub(sInitial:len()+1):lower();
				if not StringManager.contains(DataCommon.abilities, sStat) then
					sStat = "";
				end

			end
			--  Skill
			sSkill = sString:match('+ %a+');
			if sSkill then
				sSkill = sSkill:sub(3);
			end
			-- Modifier
			nMod = sString:match('+ %d+');
			if nMod then
				nMod = tonumber(nMod:sub(3));
			else
				nMod = 0;
			end
		end
		return sStat, sSkill, nMod;
end

function processSAVESTARTeffect(nodeCTActor, nodeEffect, rEffectComp, bForcedSave, bInitial)
	-- Check if it is from an affliction. In that case, may be skipped
	local sPath = DB.getValue(nodeEffect, "path", "");
	local nodeAffliction = DB.findNode(sPath);
	local nCat = 0;
	local nExtraAdv = 0;
	local sEffName = DB.getValue(nodeEffect, "label", "");
	local aEffectComps = EffectManager.parseEffect(sEffName);

	if nodeAffliction then
		-- Get data
		local nIncubation = DB.getValue(nodeEffect, "incubation", 0);
		local nAdvance = DB.getValue(nodeEffect, "advance", 0);
		local nRecovery = DB.getValue(nodeEffect, "recovery", 0);
		local nTime = DB.getValue(nodeEffect, "time", 0);
		
		for _,sEffectComp in ipairs(aEffectComps) do
			local rEffectComp = parseEffectComp(sEffectComp);
			if rEffectComp.type == DataCommon.keyword_states["AFFLICTION"] then
				nCat = rEffectComp.mod;
			end
		end
		
		-- Process if a save is necessary
		if not bForcedSave then
			nTime = nTime + 1;
			DB.setValue(nodeEffect, "time", "number", nTime);
		end
		if nTime >= nRecovery then
			nExtraAdv = 1;
		end
		bInitial = bInitial or nTime == nIncubation;
		local bPerform = bForcedSave or bInitial or math.fmod(nTime - nIncubation, nAdvance) == 0;
		if not bPerform then
			return;
		end
	elseif bForcedSave then
		Debug.chat("No affliction found")
		return;
	end

	-- Extract relevant information
	local sStat = DB.getValue(nodeEffect, "save_stat", "");
	local sSkill = DB.getValue(nodeEffect, "save_skill", "");
	local sKeywords = DB.getValue(nodeEffect, "keywords", "");
	local nDif = DB.getValue(nodeEffect, "save_dif", "");

	-- Get nodes
	local rActor = ActorManager.resolveActor(nodeCTActor);
	local sNameActor = DB.getValue(nodeCTActor, "name", "");
	local sCTowner = DB.getValue(nodeEffect, "source_name", "");
	if sNameActor == sCTowner then
		return;
	end
	local nodeCTowner, rOwner;
	if sCTowner ~= "" then
		nodeCTowner = DB.findNode(sCTowner);
		rOwner = ActorManager.resolveActor(nodeCTowner);
	end

	local node_skill, isSpecialty, node_specialty = ActorManager2.getSaveNode(rActor, sSkill);

	-- Create the action
	local nBonus, nAdv, _, _, _, _, aDice = ActorManager2.getCheck(rActor, sStat, node_skill, node_specialty, nil, nil, sKeywords, true);

	-- Perform the action
	local nResult = CombatManager2.simpleRoll("dT", nAdv + nExtraAdv);
	local nDiceResult = 0;
	for _,v in pairs(aDice) do
		nDiceResult = nDiceResult + CombatManager2.simpleRoll(v);
	end
	local nTotal = nResult + nDiceResult + nBonus;

	-- Retrieve result
	local sResult = "failure";
	if nTotal >= nDif + DataCommon.critical_success then
		sResult = "critical success";
	elseif nTotal >= nDif then
		sResult = "success";
	elseif nTotal < nDif - DataCommon.critical_miss then
		sResult = "critical failure";
	end

	-- Check result effects
	if nodeAffliction then
		-- Affliction
		if bInitial then
			if sResult == "critical success" then
				nCat = 0;
			elseif sResult == "success" then
				nGrade = math.floor((nTotal - nDif)/DataCommon.check_grade) + 1;
				nCat = math.max(1, nCat - nGrade);
			elseif sResult == "critical failure" then
				nCat = nCat + 1;
			end
		else			
			if sResult == "critical success" then
				nCat = nCat - 2;
			elseif sResult == "success" and nTotal > nDif + 2 then
				nCat = nCat - 1;
			elseif sResult == "failure" and nTotal < nDif -3 then
				nCat = nCat + 1;
			else
				nCat = nCat + 2;
			end
		end
		if nCat <= 0 then
			DB.setValue(nodeEffect, "duration", "number", 1);
		else
			-- Get affliction array
			local rCat, nMax = ActionAffliction.getAfflictionCategories(sPath);
			nCat = math.min(nCat, nMax);

			-- Build new effect
			local sEffect = aEffectComps[1] .. "; " .. DataCommon.keyword_states["AFFLICTION"] .. ": " .. tonumber(nCat);
			for ind=1,nCat do
				local rLevel = rCat[ind];
				if rLevel["effect"] ~= "" then
					sEffect = sEffect .. "; " .. rLevel["effect"];
				end
				if ind == nCat and rLevel["damage"] ~= "" then
					-- DAMAGE
					local rAction = {nSelfTarget = 1, label = aEffectComps[1], sTargeting = "self"};
					rAction.aDice, rAction.nMod, rAction.sDamageTypes = Utilities.analyzeDiceString(rLevel["damage"], true);
					ActionDamage.performRoll(nil, rActor, rAction);
				end
			end
			DB.setValue(nodeEffect, "label", "string", sEffect);
		end

	else
		-- Duration save
		if sResult == "critical success" then
			DB.setValue(nodeEffect, "duration", "number", 1);
		elseif sResult == "success" then
			DB.setValue(nodeEffect, "duration", "number", 2);
			DB.deleteChild(nodeEffect, "save_stat");
			DB.deleteChild(nodeEffect, "save_skill");
			DB.deleteChild(nodeEffect, "keywords");
			DB.deleteChild(nodeEffect, "save_dif");
		elseif sResult == "critical failure" then
			DB.deleteChild(nodeEffect, "save_stat");
			DB.deleteChild(nodeEffect, "save_skill");
			DB.deleteChild(nodeEffect, "keywords");
			DB.deleteChild(nodeEffect, "save_dif");
		end
	end

	-- Message
	local msgShort = {font = "msgfont", mode=DataCommon.result_to_mode[sResult]};
	local msgLong = {font = "msgfont", mode=DataCommon.result_to_mode[sResult]};
	msgShort.text = Interface.getString("save") .. ": "
	msgShort.text = msgShort.text .. ActorManager.getDisplayName(rActor) .. " -> ";
	if rOwner then
		msgShort.text = msgShort.text .. ActorManager.getDisplayName(rOwner);
	end
	msgLong.text = msgShort.text .. " [" .. Utilities.roll_result_2_string(nResult, nDice, nBonus, nAdv) .. " VS Dif. " .. nDif .. "]";

	local sAux = " [" .. Interface.getString(DataCommon.result_to_stringres[sResult]) .. "]";
	msgLong.text = msgLong.text .. sAux;
	msgShort.text = msgShort.text .. sAux;
	if nodeAffliction then
		msgShort.text = msgShort.text .. " [" .. aEffectComps[1] .. "]";
		msgLong.text = msgLong.text .. " [" .. aEffectComps[1] .. "]";
	else
		msgShort.text = msgShort.text .. " [" .. Interface.getString("power_mod_duration") .. "]";
		msgLong.text = msgLong.text .. " [" .. Interface.getString("power_mod_duration") .. "]";
	end

	ActionsManager.outputResult(false, rOwner, rActor, msgLong, msgShort);


end


function applyRecharge(nodeActor, nodeEffect, rEffectComp)
	local rActor = ActorManager.resolveActor(nodeActor);
	local sRecharge = table.concat(rEffectComp.remainder, " ");
	ActionRecharge.performRoll(nil, rActor, sRecharge, rEffectComp.mod, EffectManager.isGMEffect(nodeActor, nodeEffect), nodeEffect);
end

function evalAbilityHelper(rActor, sEffectAbility)
	local sSign, sModifier, sTag = sEffectAbility:match("^%[([%+%-]?)([H%d]?)([A-Z]+)%]$");

	local nAbility = nil;
	if sTag == "STR" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "strength");
	elseif sTag == "DEX" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "dexterity");
	elseif sTag == "CON" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "constitution");
	elseif sTag == "AGI" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "agility");
	elseif sTag == "PER" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "perception");
	elseif sTag == "INT" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "intelligence");
	elseif sTag == "WIL" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "willpower");
	elseif sTag == "CHA" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "charisma");
	elseif sTag == "LVL" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "level");
	elseif sTag == "PRF" then
		nAbility = ActorManager2.getCurrentAbilityValue(rActor, "prf");
	else
		nAbility = ActorManager2.getAbilityScore(rActor, sTag:lower());
	end

	if nAbility then
		if sSign == "-" then
			nAbility = 0 - nAbility;
		end
		if sModifier == "H" then
			if nAbility > 0 then
				nAbility = math.floor(nAbility / 2);
			else
				nAbility = math.ceil(nAbility / 2);
			end
		elseif sModifier then
			nAbility = nAbility * (tonumber(sModifier) or 1);
		end
	end

	return nAbility;
end

function evalEffect(rActor, s)
	if not s then
		return "";
	end
	if not rActor then
		return s;
	end

	local aNewEffectComps = {};
	local aEffectComps = EffectManager.parseEffect(s);
	for _,sEffectComp in ipairs(aEffectComps) do
		local vComp = parseEffectComp(sEffectComp);
		for i = #(vComp.remainder), 1, -1 do
			if vComp.remainder[i]:match("^%[([%+%-]?)([H%d]?)([A-Z]+)%]$") then
				local nAbility = evalAbilityHelper(rActor, vComp.remainder[i]);
				if nAbility then
					vComp.mod = vComp.mod + nAbility;
					table.remove(vComp.remainder, i);
				end
			end
		end
		table.insert(aNewEffectComps, rebuildParsedEffectComp(vComp));
	end
	local sOutput = EffectManager.rebuildParsedEffect(aNewEffectComps);

	return sOutput;
end

function getEffectsByType(rActor, sEffectType, aFilter, rFilterActor, bTargetedOnly, aOtherTypes, bDamageOther)
	if not rActor then
		return {};
	end
	local results = {};

	-- Set up filters
	local aRangeFilter = {};
	local aOtherFilter = {};
	if aFilter then
		for _,v in pairs(aFilter) do
			if type(v) ~= "string" then
				table.insert(aOtherFilter, v);
			elseif StringManager.contains(DataCommon.rangetypes_inv, v) then
				table.insert(aRangeFilter, v);
			else
				table.insert(aOtherFilter, v);
			end
		end
	end

	-- Iterate through effects
	for _,v in pairs(DB.getChildren(ActorManager.getCTNode(rActor), "effects")) do
		-- Check active
		local nActive = DB.getValue(v, "isactive", 0);
		if (nActive ~= 0) then
			local sLabel = DB.getValue(v, "label", "");
			local sApply = DB.getValue(v, "apply", "");

			-- IF COMPONENT WE ARE LOOKING FOR SUPPORTS TARGETS, THEN CHECK AGAINST OUR TARGET
			local bTargeted = EffectManager.isTargetedEffect(v);
			if not bTargeted or EffectManager.isEffectTarget(v, rFilterActor) then
				local aEffectComps = EffectManager.parseEffect(sLabel);

				-- Look for type/subtype match
				local nMatch = 0;
				for kEffectComp,sEffectComp in ipairs(aEffectComps) do
					local rEffectComp = parseEffectComp(sEffectComp);
					-- Handle conditionals
					if rEffectComp.type == "IF" then
						if not checkConditional(rActor, v, rEffectComp.remainder) then
							break;
						end
					elseif rEffectComp.type == "IFT" then
						if not rFilterActor then
							break;
						end
						if not checkConditional(rFilterActor, v, rEffectComp.remainder, rActor) then
							break;
						end
						bTargeted = true;

					-- Compare other attributes
					else
						-- Strip energy/bonus types for subtype comparison
						local aEffectRangeFilter = {};
						local aEffectOtherFilter = {};
						local j = 1;
						local bSkip = false;
						while rEffectComp.remainder[j] do
							local s = rEffectComp.remainder[j];
							local bCond = #s > 0 and ((s:sub(1,1) == "!") or (s:sub(1,1) == "~"));
							-- ORIGINAL
							-- if bCond then
							-- 	s = s:sub(2);
							-- end
							-- if StringManager.contains(DataCommon.dmgtypes, s) or s == DataCommon.all or
							-- 		StringManager.contains(DataCommon.bonustypes, s) or
							-- 		StringManager.contains(DataCommon.conditions, s) or
							-- 		StringManager.contains(DataCommon.connectors, s) then
							-- 	-- SKIP
							if bCond or
								StringManager.contains(DataCommon.bonustypes, s) or
								StringManager.contains(DataCommon.conditions, s) or
								StringManager.contains(DataCommon.connectors, s) then
								-- SKIP
								bSkip = true;
							elseif not bDamageOther and (StringManager.contains(DataCommon.dmgtypes, s) or s == DataCommon.dmgtypes_special["all"]) then
								local bInclude = true;
								if aOtherTypes and #aOtherTypes>1 then
									local jj = 1;
									while rEffectComp.remainder[jj] do
										local ss = rEffectComp.remainder[jj];
										local bCond2 = #ss > 0 and ((ss:sub(1,1) == "!") or (ss:sub(1,1) == "~"));
										if bCond2 then
											ss = ss:sub(2);
											if StringManager.contains(aOtherTypes, ss) then
												bInclude = false;
												break;
											end
										end
										jj = jj + 1;
									end
								end
								if bInclude then
									table.insert(aEffectOtherFilter, s);
								else
									bSkip = true;
								end
							elseif StringManager.contains(DataCommon.rangetypes_inv, s) then
								table.insert(aEffectRangeFilter, s);
							else
								table.insert(aEffectOtherFilter, s);
							end

							j = j + 1;
						end
						
						-- Check for match
						local comp_match = false;
						if rEffectComp.type == sEffectType then

							-- Check effect targeting
							if bTargetedOnly and not bTargeted then
								comp_match = false;
							else
								comp_match = true;
							end

							-- Check filters
							if not bDamageOther and #aEffectRangeFilter > 0 then
								local bRangeMatch = false;
								for _,v2 in pairs(aRangeFilter) do
									if StringManager.contains(aEffectRangeFilter, v2) then
										bRangeMatch = true;
										break;
									end
								end
								if not bRangeMatch then
									comp_match = false;
								end
							end
							if not bDamageOther and #aEffectOtherFilter > 0 then
								local bOtherMatch = false;
								for _,v2 in pairs(aOtherFilter) do
									if type(v2) == "table" then
										local bOtherTableMatch = true;
										for k3, v3 in pairs(v2) do
											if not StringManager.contains(aEffectOtherFilter, v3) then
												bOtherTableMatch = false;
												break;
											end
										end
										if bOtherTableMatch then
											bOtherMatch = true;
											break;
										end
									elseif StringManager.contains(aEffectOtherFilter, v2) then
										bOtherMatch = true;
										break;
									elseif StringManager.contains(DataCommon.dmgtypes_basic, v2) and StringManager.contains(aEffectOtherFilter, DataCommon.dmgtypes_special["all"]) then
										bOtherMatch = true;
										break;
									end
								end
								if not bOtherMatch then
									comp_match = false;
								end
							elseif bSkip then
								comp_match = false;
							end
						end

						-- Match!
						if comp_match then
							nMatch = kEffectComp;
							if nActive == 1 then
								table.insert(results, rEffectComp);
							end
						end
					end
				end -- END EFFECT COMPONENT LOOP

				-- Remove one shot effects
				if nMatch > 0 then
					if nActive == 2 then
						DB.setValue(v, "isactive", "number", 1);
					else
						if sApply == "action" then
							EffectManager.notifyExpire(v, 0);
						elseif sApply == "roll" then
							EffectManager.notifyExpire(v, 0, true);
						elseif sApply == "single" then
							EffectManager.notifyExpire(v, nMatch, true);
						end
					end
				end
			end -- END TARGET CHECK
		end  -- END ACTIVE CHECK
	end  -- END EFFECT LOOP

	-- RESULTS
	return results;
end

function getEffectsBonusByType(rActor, aEffectType, bAddEmptyBonus, aFilter, rFilterActor, bTargetedOnly, aOtherTypes, bDamageOther)
	if not rActor or not aEffectType then
		return {}, 0;
	end

	-- MAKE BONUS TYPE INTO TABLE, IF NEEDED
	if type(aEffectType) ~= "table" then
		aEffectType = { aEffectType };
	end

	-- PER EFFECT TYPE VARIABLES
	local results = {};
	local bonuses = {};
	local penalties = {};
	local nEffectCount = 0;

	for k, v in pairs(aEffectType) do
		-- LOOK FOR EFFECTS THAT MATCH BONUSTYPE
		local aEffectsByType = getEffectsByType(rActor, v, aFilter, rFilterActor, bTargetedOnly, aOtherTypes, bDamageOther);

		-- ITERATE THROUGH EFFECTS THAT MATCHED
		for k2,v2 in pairs(aEffectsByType) do
			-- LOOK FOR ENERGY OR BONUS TYPES
			local dmg_type = nil;
			local mod_type = nil;
			for _,v3 in pairs(v2.remainder) do
				if StringManager.contains(DataCommon.dmgtypes, v3) or StringManager.contains(DataCommon.conditions, v3) or v3 == DataCommon.all then
					dmg_type = v3;
					break;
				elseif StringManager.contains(DataCommon.bonustypes, v3) then
					mod_type = v3;
					break;
				end
			end

			-- IF MODIFIER TYPE IS UNTYPED, THEN APPEND MODIFIERS
			-- (SUPPORTS DICE)
			if dmg_type or not mod_type then
				-- ADD EFFECT RESULTS
				local new_key = dmg_type or "";
				local new_results = results[new_key] or {dice = {}, mod = 0, remainder = {}};

				-- BUILD THE NEW RESULT
				for _,v3 in pairs(v2.dice) do
					table.insert(new_results.dice, v3);
				end
				if bAddEmptyBonus then
					new_results.mod = new_results.mod + v2.mod;
				else
					new_results.mod = math.max(new_results.mod, v2.mod);
				end
				for _,v3 in pairs(v2.remainder) do
					table.insert(new_results.remainder, v3);
				end

				-- SET THE NEW DICE RESULTS BASED ON ENERGY TYPE
				results[new_key] = new_results;

			-- OTHERWISE, TRACK BONUSES AND PENALTIES BY MODIFIER TYPE
			-- (IGNORE DICE, ONLY TAKE BIGGEST BONUS AND/OR PENALTY FOR EACH MODIFIER TYPE)
			else
				local bStackable = StringManager.contains(DataCommon.stackablebonustypes, mod_type);
				if v2.mod >= 0 then
					if bStackable then
						bonuses[mod_type] = (bonuses[mod_type] or 0) + v2.mod;
					else
						bonuses[mod_type] = math.max(v2.mod, bonuses[mod_type] or 0);
					end
				elseif v2.mod < 0 then
					if bStackable then
						penalties[mod_type] = (penalties[mod_type] or 0) + v2.mod;
					else
						penalties[mod_type] = math.min(v2.mod, penalties[mod_type] or 0);
					end
				end

			end

			-- INCREMENT EFFECT COUNT
			nEffectCount = nEffectCount + 1;
		end
	end

	-- COMBINE BONUSES AND PENALTIES FOR NON-ENERGY TYPED MODIFIERS
	for k2,v2 in pairs(bonuses) do
		if results[k2] then
			results[k2].mod = results[k2].mod + v2;
		else
			results[k2] = {dice = {}, mod = v2, remainder = {}};
		end
	end
	for k2,v2 in pairs(penalties) do
		if results[k2] then
			results[k2].mod = results[k2].mod + v2;
		else
			results[k2] = {dice = {}, mod = v2, remainder = {}};
		end
	end

	return results, nEffectCount;
end

function getEffectsBonus(rActor, aEffectType, bModOnly, aFilter, rFilterActor, bTargetedOnly, aOtherTypes, bDamageOther)
	if not rActor or not aEffectType then
		if bModOnly then
			return 0, 0;
		end
		return {}, 0, 0;
	end

	-- MAKE BONUS TYPE INTO TABLE, IF NEEDED
	if type(aEffectType) ~= "table" then
		aEffectType = { aEffectType };
	end

	-- START WITH AN EMPTY MODIFIER TOTAL
	local aTotalDice = {};
	local nTotalMod = 0;
	local nEffectCount = 0;

	-- ITERATE THROUGH EACH BONUS TYPE
	local masterbonuses = {};
	local masterpenalties = {};
	for k, v in pairs(aEffectType) do
		-- GET THE MODIFIERS FOR THIS MODIFIER TYPE
		local effbonusbytype, nEffectSubCount = getEffectsBonusByType(rActor, v, true, aFilter, rFilterActor, bTargetedOnly, aOtherTypes, bDamageOther);

		-- ITERATE THROUGH THE MODIFIERS
		for k2, v2 in pairs(effbonusbytype) do
			-- IF MODIFIER TYPE IS UNTYPED, THEN APPEND TO TOTAL MODIFIER
			-- (SUPPORTS DICE)
			if k2 == "" or StringManager.contains(DataCommon.dmgtypes, k2) then
				for k3, v3 in pairs(v2.dice) do
					table.insert(aTotalDice, v3);
				end
				nTotalMod = nTotalMod + v2.mod;

			-- OTHERWISE, WE HAVE A NON-ENERGY MODIFIER TYPE, WHICH MEANS WE NEED TO INTEGRATE
			-- (IGNORE DICE, ONLY TAKE BIGGEST BONUS AND/OR PENALTY FOR EACH MODIFIER TYPE)
			else
				if v2.mod >= 0 then
					masterbonuses[k2] = math.max(v2.mod, masterbonuses[k2] or 0);
				elseif v2.mod < 0 then
					masterpenalties[k2] = math.min(v2.mod, masterpenalties[k2] or 0);
				end
			end
		end

		-- ADD TO EFFECT COUNT
		nEffectCount = nEffectCount + nEffectSubCount;
	end

	-- ADD INTEGRATED BONUSES AND PENALTIES FOR NON-ENERGY TYPED MODIFIERS
	for k,v in pairs(masterbonuses) do
		nTotalMod = nTotalMod + v;
	end
	for k,v in pairs(masterpenalties) do
		nTotalMod = nTotalMod + v;
	end

	if bModOnly then
		return nTotalMod, nEffectCount;
	end
	return aTotalDice, nTotalMod, nEffectCount;
end

function hasAnyEffectCondition(rActor, aEffects)
	for _, sEffect in pairs(aEffects) do
		if hasEffectCondition(rActor, sEffect) then
			return true;
		end
	end
	return false;
end

function hasEffectCondition(rActor, sEffect)
	return hasEffect(rActor, sEffect, nil, false, true);
end

function numberOfEffectConditions(rActor, tEffects)
	local n = 0;
	for _, s in pairs(tEffects) do
		if hasEffectCondition(rActor, s) then
			n = n + 1;
		end
	end
	return n;
end

function hasEffect(rActor, sEffect, rTarget, bTargetedOnly, bIgnoreEffectTargets)
	if not sEffect or not rActor then
		return false;
	end
	local sLowerEffect = sEffect:lower();

	-- Iterate through each effect
	local aMatch = {};
	for _,v in pairs(DB.getChildren(ActorManager.getCTNode(rActor), "effects")) do
		local nActive = DB.getValue(v, "isactive", 0);
		if nActive ~= 0 then
			-- Parse each effect label
			local sLabel = DB.getValue(v, "label", "");
			local bTargeted = EffectManager.isTargetedEffect(v);
			local aEffectComps = EffectManager.parseEffect(sLabel);

			-- Iterate through each effect component looking for a type match
			local nMatch = 0;
			for kEffectComp,sEffectComp in ipairs(aEffectComps) do
				local rEffectComp = parseEffectComp(sEffectComp);
				-- Handle conditionals
				if rEffectComp.type == "IF" then
					if not checkConditional(rActor, v, rEffectComp.remainder) then
						break;
					end
				elseif rEffectComp.type == "IFT" then
					if not rTarget then
						break;
					end
					if not checkConditional(rTarget, v, rEffectComp.remainder, rActor) then
						break;
					end

				-- Check for match
				elseif rEffectComp.original:lower() == sLowerEffect then
					if bTargeted and not bIgnoreEffectTargets then
						if EffectManager.isEffectTarget(v, rTarget) then
							nMatch = kEffectComp;
						end
					elseif not bTargetedOnly then
						nMatch = kEffectComp;
					end
				end

			end

			-- If matched, then remove one-off effects
			if nMatch > 0 then
				if nActive == 2 then
					DB.setValue(v, "isactive", "number", 1);
				else
					table.insert(aMatch, v);
					local sApply = DB.getValue(v, "apply", "");
					if sApply == "action" then
						EffectManager.notifyExpire(v, 0);
					elseif sApply == "roll" then
						EffectManager.notifyExpire(v, 0, true);
					elseif sApply == "single" then
						EffectManager.notifyExpire(v, nMatch, true);
					end
				end
			end
		end
	end

	if #aMatch > 0 then
		return true;
	end
	return false;
end

function checkConditional(rActor, nodeEffect, aConditions, rTarget, aIgnore)
	local bReturn = true;

	if not aIgnore then
		aIgnore = {};
	end
	table.insert(aIgnore, nodeEffect.getNodeName());

	for _,v in ipairs(aConditions) do
		local sLower = v:lower();
		if sLower == DataCommon.healthstatusfull then
			local nPercentWounded = ActorHealthManager2.getPercentWounded(rActor);
			if nPercentWounded > 0 then
				bReturn = false;
				break;
			end
		elseif sLower == DataCommon.healthstatushalf then
			local nPercentWounded = ActorHealthManager2.getPercentWounded(rActor);
			if nPercentWounded < .5 then
				bReturn = false;
				break;
			end
		elseif sLower == DataCommon.healthstatuswounded then
			local nPercentWounded = ActorHealthManager2.getPercentWounded(rActor);
			if nPercentWounded == 0 then
				bReturn = false;
				break;
			end
		elseif StringManager.contains(DataCommon.conditions, sLower) then
			if not checkConditionalHelper(rActor, sLower, rTarget, aIgnore) then
				bReturn = false;
				break;
			end
		elseif StringManager.contains(DataCommon.conditionaltags, sLower) then
			if not checkConditionalHelper(rActor, sLower, rTarget, aIgnore) then
				bReturn = false;
				break;
			end
		else
			-- local sAlignCheck = sLower:match("^align%s*%(([^)]+)%)$");
			local sSizeCheck = sLower:match("^size%s*%(([^)]+)%)$");
			local sTypeCheck = sLower:match("^type%s*%(([^)]+)%)$");
			local sCustomCheck = sLower:match("^custom%s*%(([^)]+)%)$");
			-- if sAlignCheck then
			-- 	if not ActorManager2.isAlignment(rActor, sAlignCheck) then
			-- 		bReturn = false;
			-- 		break;
			-- 	end
			if sSizeCheck then
				if not ActorManager2.isSize(rActor, sSizeCheck) then
					bReturn = false;
					break;
				end
			elseif sTypeCheck then
				if not ActorManager2.isCreatureType(rActor, sTypeCheck) then
					bReturn = false;
					break;
				end
			elseif sCustomCheck then
				if not checkConditionalHelper(rActor, sCustomCheck, rTarget, aIgnore) then
					bReturn = false;
					break;
				end
			end
		end
	end

	table.remove(aIgnore);

	return bReturn;
end

function checkConditionalHelper(rActor, sEffect, rTarget, aIgnore)
	if not rActor then
		return false;
	end

	for _,v in pairs(DB.getChildren(ActorManager.getCTNode(rActor), "effects")) do
		local nActive = DB.getValue(v, "isactive", 0);
		if nActive ~= 0 and not StringManager.contains(aIgnore, v.getNodeName()) then
			-- Parse each effect label
			local sLabel = DB.getValue(v, "label", "");
			local aEffectComps = EffectManager.parseEffect(sLabel);

			-- Iterate through each effect component looking for a type match
			for _,sEffectComp in ipairs(aEffectComps) do
				local rEffectComp = parseEffectComp(sEffectComp);

				-- CHECK CONDITIONALS
				if rEffectComp.type == "IF" then
					if not checkConditional(rActor, v, rEffectComp.remainder, nil, aIgnore) then
						break;
					end
				elseif rEffectComp.type == "IFT" then
					if not rTarget then
						break;
					end
					if not checkConditional(rTarget, v, rEffectComp.remainder, rActor, aIgnore) then
						break;
					end

				-- CHECK FOR AN ACTUAL EFFECT MATCH
				elseif rEffectComp.original:lower() == sEffect then
					if EffectManager.isTargetedEffect(v) then
						if EffectManager.isEffectTarget(v, rTarget) then
							return true;
						end
					else
						return true;
					end
				end
			end
		end
	end

	return false;
end

function encodeEffectForCT(rEffect)
	local aMessage = {};

	if rEffect then
		table.insert(aMessage, "EFF:");
		table.insert(aMessage, rEffect.sName);

		local sDurDice = StringManager.convertDiceToString(rEffect.aDice, rEffect.nDuration);
		if sDurDice ~= "" then
			local sOutputUnits = nil;
			if rEffect.sUnits and rEffect.sUnits ~= "" then
				if rEffect.sUnits == "minute" then
					sOutputUnits = "MIN";
				elseif rEffect.sUnits == "hour" then
					sOutputUnits = "HR";
				elseif rEffect.sUnits == "day" then
					sOutputUnits = "DAY";
				end
			end

			if sOutputUnits then
				table.insert(aMessage, "(D:" .. sDurDice .. " " .. sOutputUnits .. ")");
			else
				table.insert(aMessage, "(D:" .. sDurDice .. ")");
			end
		end

		if rEffect.sTargeting and rEffect.sTargeting ~= "" then
			table.insert(aMessage, "(T:" .. rEffect.sTargeting:upper() .. ")");
		end

		if rEffect.sApply and rEffect.sApply ~= "" then
			table.insert(aMessage, "(A:" .. rEffect.sApply:upper() .. ")");
		end
	end

	return "[" .. table.concat(aMessage, " ") .. "]";
end

function decodeEffectFromCT(sEffect)
	local rEffect = nil;

	local sEffectName = sEffect:match("EFF: ?(.+)");
	if sEffectName then
		rEffect = {};

		rEffect.sType = "effect";

		rEffect.nDuration = 0;
		rEffect.sUnits = "";
		local sDurDice, sUnits = sEffect:match("%(D:([d%dF%+%-]+) ?([^)]*)%)");
		if sDurDice then
			rEffect.aDice, rEffect.nDuration = StringManager.convertStringToDice(sDurDice);
			if sUnits then
				if sUnits == "MIN" then
					rEffect.sUnits = "minute";
				elseif sUnits == "HR" then
					rEffect.sUnits = "hour";
				elseif sUnits == "DAY" then
					rEffect.sUnits = "day";
				end
			end
		end
		sEffectName = sEffectName:gsub("%(D:[^)]*%)", "");

		rEffect.sTargeting = "";
		if sEffect:match("%(T:SELF%)") then
			rEffect.sTargeting = "self";
		end
		sEffectName = sEffectName:gsub("%(T:[^)]*%)", "");

		rEffect.sApply = "";
		if sEffect:match("%(A:ACTION%)") then
			rEffect.sApply = "action";
		elseif sEffect:match("%(A:ROLL%)") then
			rEffect.sApply = "roll";
		elseif sEffect:match("%(A:SINGLE%)") then
			rEffect.sApply = "single";
		end
		sEffectName = sEffectName:gsub("%(A:[^)]*%)", "");

		rEffect.sName = StringManager.trim(sEffectName);
	end

	return rEffect;
end

function setEffectsInitiative(nodeCT, nInit)
	-- Function that sets the initiative of all character effects to its starting initiative, so the duration is reduced in 1 when it is their turn.
	if not nodeCT or not nodeCT.getChild('effects') then
		return;
	end

	for _, effect in pairs(nodeCT.getChild('effects').getChildren()) do
		DB.setValue(effect, "init", "number", nInit);
	end

end

function isConcentrationEffect(nodeEffect, rActor, rIdentities)
	-- Check if it is concentration
	local nLevel = DB.getValue(nodeEffect, "level", -1);
	local nID = DB.getValue(nodeEffect, "ID", -1);
	if (nLevel < 0) or (nID < 0) then
		return false, -99, -1;
	end
	-- Check if the actor is the owner of the spell
	local bCon = true
	if rActor or rIdentities then
		local sName = DB.getValue(nodeEffect, 'source_name',"");
		if sName == "" then
			sName = DB.getPath(nodeEffect.getParent().getParent());
		end
		local bCon1 = false;
		if rActor then
			bCon1 = sName == rActor['sCTNode'];
		end
		local bCon2 = false;
		if rIdentities then
			for _,sId in pairs(rIdentities) do
				local nodeActor = DB.findNode("charsheet." .. sId);
				local rNewActor = ActorManager.resolveActor(nodeActor);
				bCon2 = bCon2 or (rNewActor and sName == rNewActor['sCTNode']);
			end
		end
		bCon = bCon1 or bCon2;
	end

	-- Calculate base difficulty (multiple concentration effects not taken into account)
	local nDif = DataCommon.spell_conc_DC_base + nLevel;
	if not bCon then
		nDif = -99;
	end

	return bCon, nDif, nID;
end

function removeAllEffectsOfOwner(nodeCT)
	local sActorPath = nodeCT.getPath();
	for _,actor in pairs(CombatManager.getCombatantNodes()) do
		if type(actor) == "databasenode" then
			local bSame = actor == nodeCT;
			for _, effect in pairs(actor.createChild('effects').getChildren()) do
				if bSame or bSame == DB.getValue(effect, 'source_name',"") then
					onEffectExpire(effect)
					effect.delete();
				end
			end
		end
	end
end


function isConcentrationEffect_old(nodeEffect, rActor)
	-- Function to determine if the effect requires concentration
	local bCond = false;
	local nDif = 0;
	local sEffName = DB.getValue(nodeEffect, "label", "");
	local aEffectComps = EffectManager.parseEffect(sEffName);
	-- local nActive = DB.getValue(nodeEffect, "isactive", 0);

	for _,sEffectComp in ipairs(aEffectComps) do
		local rEffectComp = parseEffectComp(sEffectComp);
		-- Conditionals
		if rEffectComp.type == DataCommon.keyword_inactive["conc"] then
			if rActor then
				local bCondAux = false;
				local sName = DB.getValue(nodeEffect, 'source_name',"");
				if sName == "" then
					local sPath = DB.getPath(nodeEffect.getParent().getParent());
					bCondAux = sPath == rActor['sCTNode'];
				else
					bCondAux = sName == rActor['sCTNode'];
				end
				if bCondAux then
					bCond = true;
					nDif = rEffectComp.mod + DataCommon.spell_conc_DC_base;
					break;
				end
			else
				bCond = true;
				nDif = rEffectComp.mod + DataCommon.spell_conc_DC_base;
				break;
			end
		end
	end
	return bCond, nDif;
end


function isTiableEffect(nodeEffect)
	local bCon;
	if Session.IsHost then
		bCon = isConcentrationEffect(nodeEffect);
	else
		local aIdentities = User.getAllActiveIdentities();
		bCon = isConcentrationEffect(nodeEffect, nil, aIdentities);
	end
	local bStat = DB.getValue(nodeEffect, "stat", "") ~= "";
	local bSkill = DB.getValue(nodeEffect, "skill", "") ~= "";
	local bDur = DB.getValue(nodeEffect, "period", 0) > 0;
	return bCon and bStat and bSkill and bDur;
end

function isTiableEffect_old(nodeEffect)
	-- Function to determine if the effect is tiable
	local bCond = false;
	local sStat = ""
	local sEffName = DB.getValue(nodeEffect, "label", "");
	local aEffectComps = EffectManager.parseEffect(sEffName);

	for _,sEffectComp in ipairs(aEffectComps) do
		local rEffectComp = parseEffectComp(sEffectComp);
		-- Conditionals
		if rEffectComp.type == DataCommon.keyword_inactive["conc"] then
			for _, v in pairs(rEffectComp.remainder) do
				sStat = string.match(v:lower(), DataCommon.concentration_keywords["type"] .. " %a+")
				if sStat then
					bCond = true;
					break;
				end
			end
		end
	end
	return bCond
end

function getEffectName(nodeEffect)
	-- Function to get the name of the effect (first part of the composition)
	local sEffName = DB.getValue(nodeEffect, "label", "");
	local aEffectComps = EffectManager.parseEffect(sEffName);
	local rEffectComp = parseEffectComp(aEffectComps[1]);
	return rEffectComp.remainder[1];
end



function getTiespellAction(nodeEffect)
	-- Get info
	local rAction = {};
	rAction.type = "tiespell";
	rAction.nMod = 0;
	rAction.nAdv = 0;
	rAction.nDurationMult = 1;
	rAction.spellName = getEffectName(nodeEffect);
	rAction.nLevel = DB.getValue(nodeEffect, "level", 0);
	rAction.nDif = DataCommon.spell_tie_DC_base + rAction.nLevel;
	rAction.nDuration = DB.getValue(nodeEffect, "period", 0);
	rAction.sStat = DB.getValue(nodeEffect, "stat", "");
	rAction.sSkill = DB.getValue(nodeEffect, "skill", "");
	rAction.nID = DB.getValue(nodeEffect, "ID", -1)
	if Input.isShiftPressed() then
		rAction.nDurationMult = 2;
	elseif Input.isControlPressed() then
		rAction.nDurationMult = 5;
	end

	-- Get hidden
	local sLabel = DB.getValue(nodeEffect, "label", "");
	if Input.isAltPressed() or sLabel:find(DataCommon.keyword_inactive["hidden"]) then
		rAction.nAdv = -1;
	end

	-- Get actor and skill node
	local sName = DB.getValue(nodeEffect, 'source_name',"");
	if sName == "" then
		sName = DB.getPath(nodeEffect.getChild("..."));
	end
	local nodeCT = DB.findNode(sName);
	local rActor = ActorManager.resolveActor(nodeCT);
	rAction.nodeSkill = ActorManager2.getSkillNode(rActor, rAction.sSkill);

	-- Spend actions
	if CombatManager2.inCombat() then
		CombatManager2.useAction(nodeCT, "action");
	end

	return rActor, rAction
end



function getUntiespellAction(nodeEffect, nDif, nAdv)
	-- Get actor
	local _, rActor = ActorManager2.getCurrentCharacter(true);

	-- Get info
	local rAction = {};
	rAction.type = "untiespell";
	rAction.nMod = 0;
	rAction.nAdv = nAdv;
	rAction.nDif = nDif;
	rAction.sStat, rAction.sSkill = ActorManager2.getBestGift(rActor);
	rAction.nodeSkill = ActorManager2.getSkillNode(rActor, rAction.sSkill);

	-- Spend actions
	if CombatManager2.inCombat() then
		CombatManager2.useAction(nodeCT, "action");
	end

	return rActor, rAction
end

function getDispellAction(nodeEffect, nAdv, nDif)
	-- Get actor
	local _, rActor = ActorManager2.getCurrentCharacter(true);

	-- Get info
	local rAction = {};
	rAction.type = "dispell";
	rAction.nMod = 0;
	rAction.nAdv = nAdv;
	rAction.sStat, rAction.sSkill = ActorManager2.getBestGift(rActor);
	rAction.nodeSkill = ActorManager2.getSkillNode(rActor, rAction.sSkill);

	-- Different if we are dispelling a maintained effect or not
	if nDif then
		rAction.nDif = nDif;
		rAction.nLevelDif = nLevelDispell;
	else
		local sName = DB.getValue(nodeEffect, 'source_name',"");
		if sName == "" then
			sName = DB.getPath(nodeEffect.getChild("..."));
		end
		rAction.sNodeCaster = sName;

		local nLevelDispelled = DB.getValue(nodeEffect, "level", 0);
		local nLevelDispell = ActionSkill.getCastStatus(rActor);
		rAction.nLevelDif = nLevelDispell - nLevelDispelled;
	end
	
	return rActor, rAction
end



function tieSpellEffects(rActor, nID, nDuration, nDif, bHidden)
	-- Function to remove all concentration effects with the same name and owner
	for _,actor in pairs(CombatManager.getCombatantNodes()) do
		for _, effect in pairs(actor.createChild('effects').getChildren()) do
			local cond = DB.getValue(effect, "ID", 0) == nID;
			if cond then
				-- Tie the spell
				DB.setValue(effect, 'duration',"number", nDuration);
				local sEffName = DB.getValue(effect, "label", "");
				local aEffectComps = EffectManager.parseEffect(sEffName);
				local bHasHidden = false;

				-- Change concentration for tied
				for k,sEffectComp in ipairs(aEffectComps) do
					if sEffectComp and sEffectComp ~= "" and sEffectComp:find(DataCommon.keyword_inactive["conc"]) then
						aEffectComps[k] = DataCommon.keyword_states["TIED"] .. ': ' .. tostring(nDif);
					elseif sEffectComp and sEffectComp ~= "" and sEffectComp:find(DataCommon.keyword_inactive["hidden"]) then
						bHasHidden = true;
					end
				end
				if not bHasHidden and bHidden then
					table.insert(aEffectComps, DataCommon.keyword_inactive["hidden"])
				end
				DB.setValue(effect, 'label', 'string', EffectManager.rebuildParsedEffect(aEffectComps));
				DB.deleteChild(effect, "level")
				DB.deleteChild(effect, "stat")
				DB.deleteChild(effect, "skill")
			end
		end
	end
end

function findConcentrationEffects(rActor)
	-- Function that looks for all concentration effects owned by an actor. They arrange it so effects with the same name are grouped together.
	local nEffects = 0;
	local aDif = {};
	local aIDs = {};

	for _,actor in pairs(CombatManager.getCombatantNodes()) do
		for _, effect in pairs(actor.createChild('effects').getChildren()) do
			local bCond, nDif, nID = isConcentrationEffect(effect, rActor);
			if bCond and not Utilities.inArray(aIDs, nID) then
				table.insert(aIDs, nID);
				table.insert(aDif, nDif);
			end
		end
	end

	return aIDs, aDif;
end


function removeAllEffectsByNameAndOwner(rActor, nID, bOnlyConc, bMisscast)
	-- Function to remove all concentration effects with the same ID and owner. If no ID is provided, remove all of them.
	for _,actor in pairs(CombatManager.getCombatantNodes()) do
		if type(actor) == "databasenode" then
			for _, effect in pairs(actor.getChild('effects').getChildren()) do
				local bCon, nDif, nIDaux = isConcentrationEffect(effect, rActor);
				if (bCon or not bOnlyConc) and (not nID or nIDaux == nID) then
					EffectManager.expireEffect(ActorManager2.getNodeFromCT(rActor), effect);
					if bCon and bMisscast and nDif then
						ActionCast.notifyMisscast(nDif - DataCommon.spell_conc_DC_base, rActor)
					end
				end
			end
		end
	end
end



function valueInTable(value, table)
	-- Auxiliar function to check if a value is in a table or not
	local bCond = false;
	for k, v in pairs(table) do
		bCond = value == v;
		if bCond then
			return bCond, k;
		end
	end
	return false, nil;
end

function addAfflictionEffect(rActor, rTarget, msgOOB)
	local nodeCT = ActorManager.getCTNode(rTarget)

	-- User and identity
	local sUser = ""
	if not Session.IsHost then
		sUser = User.getUsername();
	end
	local sIdentity = User.getIdentityLabel();

	-- Effect array
	local rNewEffect = {};
	rNewEffect.nDuration = 0;
	rNewEffect.sSource = DB.getValue(ActorManager.getCTNode(rActor), "name", "")
	-- rNewEffect.nInit = DB.getValue(nodeCT, "init", 0);
	rNewEffect.sName = msgOOB.sNameAffliction .. "; " .. DataCommon.keyword_states["AFFLICTION"] .. ": " .. tostring(msgOOB.nIniCat) .. " " .. msgOOB.sTypeAffliction .. ";";

	-- Apply
	local nodeEffect = EffectManager.addEffect(sUser, sIdentity, nodeCT, rNewEffect, true);

	-- Modify fields depending on dossage
	if msgOOB.nDosage == 0.5 then
		msgOOB.nDif = msgOOB.nDif + DataCommon.affliction_half_peligrosity;
		sgOOB.nIncubation = sgOOB.nIncubation * 2;
		sgOOB.nAdvance = sgOOB.nAdvance * 2;
	elseif msgOOB.nDosage > 1 then
		msgOOB.nDif = msgOOB.nDif + msgOOB.nDosage * DataCommon.affliction_multiple_peligrosity;
		sgOOB.nIncubation = math.max(1, math.ceil(sgOOB.nIncubation / msgOOB.nDosage));
		sgOOB.nAdvance = math.max(1, math.ceil(sgOOB.nAdvance / msgOOB.nDosage));
	end

	-- Extra fields
	DB.setValue(nodeEffect, "save_dif", "number", msgOOB.nDif);
	DB.setValue(nodeEffect, "save_stat", "string", DataCommon.abilities_translation_inv["constitution"]);
	DB.setValue(nodeEffect, "save_skill", "string", Interface.getString("skill_value_concentration"));
	local sKeywords = msgOOB.sTypeAffliction;
	if msgOOB.sKeywords ~= "" then
		if sKeywords ~= "" then
			sKeywords = sKeywords .. ", ";
		end
		sKeywords = sKeywords .. msgOOB.sKeywords;
	end
	DB.setValue(nodeEffect, "keywords", "string",  sKeywords);
	DB.setValue(nodeEffect, "incubation", "number",  msgOOB.nIncubation);
	DB.setValue(nodeEffect, "advance", "number",  msgOOB.nAdvance);
	DB.setValue(nodeEffect, "recovery", "number",  msgOOB.nRecovery);
	DB.setValue(nodeEffect, "dosage", "number",  msgOOB.nDosage);
	DB.setValue(nodeEffect, "time", "number",  0);
	DB.setValue(nodeEffect, "path", "string",  msgOOB.sPath);
	DB.setValue(nodeEffect, "link_affliction", "windowreference", "reference_affliction", msgOOB.sPath);

	-- Message
	local sMessage = string.format("%s ['%s'] -> ", Interface.getString("effect_label"), rNewEffect.sName );
	EffectManager.message(sMessage, nodeCT, false, sUser)
end

function addConcentrationEffect(rActor, aTargets, rNewEffect, rConc, bTargeted)
	-- Get some info
	local nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
	local nodeCT;
	if bTargeted then
		nodeCT = ActorManager.getCTNode(rActor);
	end
	local sUser = ""
	if not Session.IsHost then
		sUser = User.getUsername();
	end
	local sIdentity = User.getIdentityLabel();

	-- Check that the user is not unconscious or asleep

	-- Get current concentration effects
	local aIDs, _ = EffectManagerSS.findConcentrationEffects(rActor);
	local nMaxConc = ActorManager2.getConcentrationMax(rActor);
	local bCond1 = #aIDs > nMaxConc;
	local bCond2 = (#aIDs == nMaxConc) and not Utilities.inArray(aIDs, rConc.nID);
	if bCond1 or bCond2 then
		local nRand = math.random(#aIDs);
		removeAllEffectsByNameAndOwner(rActor, aIDs[nRand], true)
	end

	-- Add conc label for flavour if missing
	local sName = rNewEffect.sName;
	if not rNewEffect.sName:find(DataCommon.keyword_inactive["conc"]) then
		rNewEffect.sName = rNewEffect.sName .. "; " .. DataCommon.keyword_inactive["conc"];
		if rConc.sSaveStatus and rConc.save_stat and (rConc.sSaveStatus == "failure") then
			rNewEffect.sName = rNewEffect.sName .. " ;" .. DataCommon.keyword_inactive["save_start"];
		end
		if rConc.nHiddenSpell == 1 then
			rNewEffect.sName = rNewEffect.sName .. "; " .. DataCommon.keyword_inactive["hidden"];
		end
	end

	-- Apply
	for _,rTarget in pairs(aTargets) do
		if not bTargeted then
			nodeCT = ActorManager.getCTNode(rTarget);
		end

		if not rConc.sSaveStatus or (rConc.sSaveStatus ~= "critical_success" )then
			local nodeEffect = EffectManager.addEffect(sUser, sIdentity, nodeCT, rNewEffect, true);

			-- Concentration data
			DB.setValue(nodeEffect, "period", "number", rConc.nDur);
			DB.setValue(nodeEffect, "level", "number", rConc.nLevel);
			DB.setValue(nodeEffect, "stat", "string", rConc.sStat);
			DB.setValue(nodeEffect, "skill", "string", rConc.sSkill);
			DB.setValue(nodeEffect, "ID", "number", rConc.nID);

			-- Change effect if there is a save
			if rConc.sSaveStatus and (rConc.sSaveStatus == "success") then
				DB.setValue(nodeEffect, "duration", "number", 1);
			elseif rConc.sSaveStatus and rConc.save_stat and (rConc.sSaveStatus == "failure") then
				DB.setValue(nodeEffect, "save_dif", "number", rConc.nSaveDif);
				DB.setValue(nodeEffect, "save_stat", "string", rConc.save_stat);
				DB.setValue(nodeEffect, "save_skill", "string", rConc.save_skill);
				DB.setValue(nodeEffect, "keywords", "string", rConc.keywords);
			end

			-- Cost
			if rNewEffect.nCost and rNewEffect.nCost > 0 then
				DB.setValue(nodeEffect, "cost", "number", rNewEffect.nCost);
				if rNewEffect.sCostResource == "vitality" then
					DB.setValue(nodeEffect, "cost_vitality", "number", 1);
				end
			end
		end

	end

end

function reactivateRegenerationEffects(rActor)
	local bCond = false;
	for _,v in pairs(DB.getChildren(ActorManager.getCTNode(rActor), "effects")) do
		local nActive = DB.getValue(v, "isactive", 0);
		if nActive ~= 1 then
			local sLabel = DB.getValue(v, "label", "");
			local aEffectComps = EffectManager.parseEffect(sLabel);

			-- Iterate through each effect component looking for a type match
			for _,sEffectComp in ipairs(aEffectComps) do
				local rEffectComp = parseEffectComp(sEffectComp)
				if rEffectComp.type == DataCommon.keyword_states["REGEN"] or rEffectComp.type == DataCommon.keyword_states["TRUEREGEN"] then
					DB.setValue(v, "isactive", "number", 1);
					bCond = true;
				end
			end
		end
	end
	return bCond;
end

function damagesHelper(aRemainder)
	local aYes = {};
	local aNo = {};
	for _,v in pairs(aRemainder) do
		if Utilities.inArray(DataCommon.dmgtypes, v) then
			table.insert(aYes, v);
		else 
			local bCond1 = v:sub(1, 1) == "!" or v:sub(1, 1) == "~";
			local bCond2 = Utilities.inArray(DataCommon.dmgtypes, v:sub(2, v:len()));
			if bCond1 and bCond2 then
				table.insert(aNo, v:sub(2, v:len()));
			end
		end
	end
	return aYes, aNo;
end

function deactivateRegenerationEffects(rActor, aDamage)
	local bCond = false;
	local bBasic = false;
	for _,v in pairs(aDamage) do
		if Utilities.inArray(DataCommon.dmgtypes_basic, v) then
			bBasic = true;
			break;
		end
	end
	for _,v in pairs(DB.getChildren(ActorManager.getCTNode(rActor), "effects")) do
		local nActive = DB.getValue(v, "isactive", 0);
		if nActive ~= 0 then
			local sLabel = DB.getValue(v, "label", "");
			local aEffectComps = EffectManager.parseEffect(sLabel);
			local bCondEff = false;

			-- Iterate through each effect component looking for a type match
			for _,sEffectComp in ipairs(aEffectComps) do
				local rEffectComp = parseEffectComp(sEffectComp)
				if rEffectComp.type == DataCommon.keyword_states["REGEN"] or rEffectComp.type == DataCommon.keyword_states["TRUEREGEN"] then
					local aYes, aNo = damagesHelper(rEffectComp.remainder);
					local bCondYes = false;
					local bCondNo = false;
					for _,v in pairs(aYes) do
						bCondYes = bCondYes or Utilities.inArray(aDamage, v);
					end
					for _,v in pairs(aNo) do
						bCondNo = bCondNo or Utilities.inArray(aDamage, v);
					end
					bCondEff = bCondEff or (#aYes == 0 and bBasic and not bCondNo) or (bCondYes and not bCondNo);
				end
			end
			if bCondEff then
				bCond = true;
				DB.setValue(v, "isactive", "number", 0);
			end
		end
	end
	return bCond;
end


function getTooltipFromText(sString)
	local sTooltip = "";
	local aSplit = StringManager.split(sString, ";", true);
	local bNewLine = false;
	local aAdded = {};
	for _,v in pairs(aSplit) do
		local sMatch = v:match("(%a+):");
		local sDesc;
		if sMatch and not Utilities.inArray(aAdded, sMatch) then
			local aData = DataCommon.keyword_states_data[sMatch];
			if aData then
				sDesc = aData.desc;
				table.insert(aAdded, sMatch);
			end
		elseif Utilities.inArray(DataCommon.conditions, v) then
			sDesc = DataCommon.conditions_desc[v];
		end
		if sDesc then
			if bNewLine then
				sTooltip = sTooltip .. "\n";
			else
				bNewLine = true;
			end
			if sMatch then
				sTooltip = sTooltip .. sMatch .. ": " .. sDesc;
			else
				sTooltip = sTooltip .. v .. ": " .. sDesc;
			end
		end
	end
	return sTooltip;
end

-------------------------
-- Effect roll build step by step
-------------------------

function provisionalEffectLabel(window)
	if not window then
		Debug.chat("No window")
		window.provisional.setValue("");
		return;
	end

	-- aConfig = {desc="", value=0, mod=false, tooltip=""};

	-- Get the data
	local sKeys = {"effect_checks", "effect_combat", "effect_sensitivity", "effect_magic", "effect_vision"};
	local sValue = window.effect_type.getStringValue();
	local sMaster = "";
	local control = window[sValue];
	if control then
		sMaster = Interface.getString(control.getStringValue());
	end
	local aConfig = DataCommon.keyword_states_data[sMaster];
	if sMaster == "" or not aConfig then
		window.provisional.setValue("");
		return;
	end
	local sRoll = window.effect_roll.getValue() or "";
	local sMod = tostring(window.effect_number.getValue()) or "";
	local sExtra = window.effect_extra.getValue() or "";

	-- Build the effect
	local sString = sMaster;
	if aConfig.mod or aConfig.value > 0 then
		sString = sString .. ":";
	end
	if aConfig.value == 1 then
		sString = sString .. " " .. sMod;
	elseif aConfig.value == 2 then
		sString = sString .. " " .. sRoll;
	end
	if aConfig.mod then
		sString = sString .. " " .. sExtra;
	end

	-- Save it
	window.provisional.setValue(sString);
end

function addProvisionalEffect(nodeEffect)
	local sProv = DB.getValue(nodeEffect, "provisional", "");
	local sCurrent = DB.getValue(nodeEffect, "label", "");
	if sCurrent ~= "" and sProv ~= "" then
		sCurrent = sCurrent .. "; ";
	end
	sCurrent = sCurrent .. sProv;
	DB.setValue(nodeEffect, "label", "string", sCurrent);
end




