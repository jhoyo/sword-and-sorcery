--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--


function onInit()
	ActorHealthManager.isDyingOrDead = isDyingOrDead;
    ActorHealthManager.getWoundPercent = getPercentWounded;
    ActorHealthManager.getHealthInfo = getWoundColor;
    ActorHealthManager.getTokenHealthInfo = getWoundBarColor;
end


function isDyingOrDead(nodeCT)
	local nDamage = DB.getValue(nodeCT, "health.vit.damage", 0);
	local nMax = DB.getValue(nodeCT, "health.vit.max", 0);
	return nDamage >= nMax;
end


function getPercentWounded(rActor, bFromCT, nMax, nDamage, nTemp)
	if rActor then
		if bFromCT then
			rActor = ActorManager.resolveActor(rActor);
		end
		nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
		nDamage = math.max(DB.getValue(nodeActor, "health.vit.damage", 0), 0);
		nMax = math.max(DB.getValue(nodeActor, "health.vit.max", 0), 0);
		nTemp = math.max(DB.getValue(nodeActor, "health.vit.temp", 0), 0);
	else
		if not nMax or not nDamage then
			return 0, "", "000000";
		end
		if not nTemp then
			nTemp = 0;
		end
	end

	nDamage = math.max(nDamage - nTemp, 0);
	local sState = DataCommon.vitality_states[1];
	local sColor = DataCommon.vitality_colors[1];

	if nMax <= 0 then
		return 0, "", "000000";
	end

	local nPercent = nDamage / nMax;
	if nPercent == 1 then
		sState = DataCommon.vitality_states[6];
		sColor = DataCommon.vitality_colors[6];
	elseif nPercent < 1 and nPercent >= DataCommon.vitality_thresholds[1] then
		sState = DataCommon.vitality_states[5];
		sColor = DataCommon.vitality_colors[5];
	elseif nPercent < DataCommon.vitality_thresholds[1] and nPercent >= DataCommon.vitality_thresholds[2] then
		sState = DataCommon.vitality_states[4];
		sColor = DataCommon.vitality_colors[4];
	elseif nPercent < DataCommon.vitality_thresholds[2] and nPercent >= DataCommon.vitality_thresholds[3] then
		sState = DataCommon.vitality_states[3];
		sColor = DataCommon.vitality_colors[3];
	elseif nPercent < DataCommon.vitality_thresholds[3] and nPercent > 0 then
		sState = DataCommon.vitality_states[2];
		sColor = DataCommon.vitality_colors[2];
	end

	return nPercent, sState, sColor;
end

function getPercentTired(rActor, bFromCT, nMax, nDamage, nTemp)
	if rActor then
		if bFromCT then
			rActor = ActorManager.resolveActor(rActor);
		end
		nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
		nDamage = math.max(DB.getValue(nodeActor, "health.vigor.damage", 0), 0);
		nMax = math.max(DB.getValue(nodeActor, "health.vigor.max", 0), 0);
		nTemp = math.max(DB.getValue(nodeActor, "health.vigor.temp", 0), 0);
	else
		if not nMax or not nDamage then
			return 0, "", "000000";
		end
		if not nTemp then
			nTemp = 0;
		end
	end

	nDamage = math.max(nDamage - nTemp, 0);
	local sState = DataCommon.vigor_states[1];
	local sColor = DataCommon.vigor_colors[1];

	if nMax <= 0 then
		return 0, "", "000000";
	end

	local nPercent = nDamage / nMax;
	if nPercent == 1 then
		sState = DataCommon.vigor_states[6];
		sColor = DataCommon.vigor_colors[6];
	elseif nPercent < 1 and nPercent >= DataCommon.vigor_thresholds[1] then
		sState = DataCommon.vigor_states[5];
		sColor = DataCommon.vigor_colors[5];
	elseif nPercent < DataCommon.vigor_thresholds[1] and nPercent >= DataCommon.vigor_thresholds[2] then
		sState = DataCommon.vigor_states[4];
		sColor = DataCommon.vigor_colors[4];
	elseif nPercent < DataCommon.vigor_thresholds[2] and nPercent >= DataCommon.vigor_thresholds[3] then
		sState = DataCommon.vigor_states[3];
		sColor = DataCommon.vigor_colors[3];
	elseif nPercent < DataCommon.vigor_thresholds[3] and nPercent > 0 then
		sState = DataCommon.vigor_states[2];
		sColor = DataCommon.vigor_colors[2];
	end

	return nPercent, sState, sColor;
end

-- function onHealthChanged()
-- 	local sColor = ActorHealthManager2.getWoundColor(getDatabaseNode());
-- 	vit_wounds.setColor(sColor);
-- end

-- Based on the percent wounded, change the font color for the Wounds field
function getWoundColor(rActor)
	nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
	local nPercent, sStatus = getPercentWounded(nodeActor);
	local sColor = ColorManager.getHealthColor(nPercent, false);
	return nPercent, sStatus, sColor;
end

-- Based on the percent wounded, change the token health bar color
function getWoundBarColor(rActor)
	nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
	local nPercentWounded, sStatus = getPercentWounded(nodeActor);
	local sColor = ColorManager.getTokenHealthColor(nPercentWounded, true);
	return nPercentWounded, sStatus, sColor;
end