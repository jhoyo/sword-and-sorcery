--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--


local sTroncal = "";
local sFree = "";
local sBackground = "";
local aAll = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

function onInit()
    sTroncal = Interface.getString("type_aptitude_troncal"):lower();
    sFree = Interface.getString("type_aptitude_free"):lower();
    sBackground = Interface.getString("type_aptitude_background"):lower();
end

function getAptitudeSubtypeValue(vNode)
	return StringManager.split(DB.getValue(vNode, "subtype", ""), ",", true);
end

function getAptitudeLevel(vNode)
	local sLevel = DB.getValue(vNode, "level", "");
	if sLevel == "X" or sLevel == "10" then
		return sLevel
	else
		return sLevel:sub(1,1);
	end
end

function getAptitudeAllLevels(vNode)
    local sLevel = DB.getValue(vNode, "level", "");
    if sLevel == "" then
        return {0};
    elseif sLevel == "X" or sLevel == "x" then
        return aAll;
    else
        local aLevels = StringManager.split(sLevel, ",", true);
        local aLevelsGood = {};
        for _,v in pairs(aLevels) do
            table.insert(aLevelsGood, tonumber(v));
        end
        return aLevelsGood;
    end
end

function isFree(vNode)
    local sReq = DB.getValue(vNode, "requirements", ""):lower();
    local bCond = sReq == sTroncal or sReq == sFree or sReq == sBackground;
    return bCond
end

function calculateXPcost(vNode, nCat, sList, bIndividual)
    if not vNode or not nCat or nCat <= 0 or isFree(vNode) then
        return 0
    end

    local nCat = DB.getValue(nodeApt, "category", 1);
    local nExtra = DB.getValue(nodeApt, "extra_exp", 0);
    local sType = DB.getValue(nodeApt, "type", "");
    local sLevels = DB.getValue(nodeApt, "level", "0");
    local aLevels = getAptitudeAllLevels(sLevels);

    local nXP = 0;
    if sType == Interface.getString("type_aptitude_flaw") or (sList and sList == "disadvantagelist") then
        nXP = -nExtra;
      else
        if bIndividual then
            nXP = nXP + DataCommon.XP_level_0 + DataCommon.XP_level_mult * aLevels[nCat] + nExtra;
        else
            for ind = 1,nCat do
                nXP = nXP + DataCommon.XP_level_0 + DataCommon.XP_level_mult * aLevels[ind] + nExtra;
            end
        end
    end

    return nXP;
end

function getSources(vNode)
    return StringManager.split(DB.getValue(vNode, "subtype", ""):lower(), ",", true);
end

function isComplete(vNode, nCurrent)
    return nCurrent >= #getAptitudeAllLevels(vNode);
end

function getNextXP(vNode, nCurrent)
    local aLevels = getAptitudeAllLevels(vNode);
    local nExtra = DB.getValue(vNode, "extra_exp", 0);
    if nCurrent < #aLevels then
        return nExtra + DataCommon.XP_level_0 + DataCommon.XP_level_mult * aLevels[nCurrent + 1]
    end
    return -1;
end

function getNextLevel(vNode, nCurrent)
    local aLevels = getAptitudeAllLevels(vNode);
    if nCurrent < #aLevels then
        return aLevels[nCurrent + 1]
    end
    return 99;
end

local aAptitudeLists = { "aptitude", "reference.aptitudedata" };

function getAptitudeType(nodeApt)
    local sType = Interface.getString(DB.getValue(nodeApt, "reftype", ""));
    return sType;
end

function findRefAptitude(sName, sType)
    -- Add modules
	local aAptListsMod = Utilities.addModules(aAptitudeLists);

    -- TODO: Remove modifications in the name

    -- Find the correct one
    for _,sPath in pairs(aAptListsMod) do
        local nodeList = DB.findNode(sPath);
        if nodeList then
            for _, nodeApt in pairs(nodeList.getChildren()) do
                local sNameApt = DB.getValue(nodeApt, "name", "");
                local sTypeApt = getAptitudeType(nodeApt);
                -- Debug.chat("sNameApt",sNameApt)
                -- Debug.chat("sTypeApt",sTypeApt)
                if sType == sTypeApt and sNameApt:find(sName) then
                    return nodeApt
                end
            end
        end
    end
    return nil;
end