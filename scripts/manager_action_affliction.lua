--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_AFFLICTION = "applyaff";

--------------------
-- SUMMON NPC
--------------------

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_AFFLICTION, handleAffliction);

	ActionsManager.registerTargetingHandler("affliction", onTargeting);
	ActionsManager.registerResultHandler("affliction", onAffliction);
end

function notifyApplyAffliction(rSource, rTarget, rRoll)
	if not rTarget then
		return;
	end
	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	if sTargetType ~= "pc" and sTargetType ~= "ct" then
		return;
	end

	local msgOOB = {};
	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;
	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	msgOOB.sTargetType = sTargetType;
	msgOOB.sTargetNode = sTargetNode;

	msgOOB.type = OOB_MSGTYPE_AFFLICTION;
	msgOOB.sNameAffliction = rRoll.sNameAffliction;
	msgOOB.sTypeAffliction = rRoll.sTypeAffliction;
	msgOOB.nDif = rRoll.nDif;
	msgOOB.nIncubation = rRoll.nIncubation;
	msgOOB.nAdvance = rRoll.nAdvance;
	msgOOB.nRecovery = rRoll.nRecovery;
	msgOOB.sKeywords = rRoll.sKeywords;
	msgOOB.nIniCat = rRoll.nIniCat;
	msgOOB.sPath = rRoll.sPath;
	msgOOB.sEndEffect = rRoll.sEndEffect;
	msgOOB.sInmunity = rRoll.sInmunity;
	msgOOB.nDosage = rRoll.nDosage;

	Comm.deliverOOBMessage(msgOOB, "");
end


function handleAffliction(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);
	msgOOB.nDif = tonumber(msgOOB.nDif);
	msgOOB.nIncubation = tonumber(msgOOB.nIncubation);
	msgOOB.nAdvance = tonumber(msgOOB.nAdvance);
	msgOOB.nRecovery = tonumber(msgOOB.nRecovery);
	msgOOB.nIniCat = tonumber(msgOOB.nIniCat);
	msgOOB.nDosage = tonumber(msgOOB.nDosage);

	-- Check if the character currently has the same affliction
	local bAdd = true;
	for _,v in pairs(DB.getChildren(ActorManager.getCTNode(rTarget), "effects")) do
		local nActive = DB.getValue(v, "isactive", 0);
		if nActive ~= 0 then
			if DB.getValue(v, "path", "") == msgOOB.sPath then
				bAdd = false;
				local nDosage = DB.getValue(v, "dosage", 0);
				local nDosageNew = nDosage + msgOOB.nDosage;
				-- New dossage effects
				if nDosageNew > nDosage then
					local nDif = DB.getValue(v, "save_dif", 0);
					local nIncubation = DB.getValue(v, "incubation", 0);
					local nAdvance = DB.getValue(v, "advance", 0);
					local nTime = DB.getValue(v, "time", 0);

					local nDifNew = nDif;
					local nIncubationNew = nIncubation;
					local nAdvanceNew = nAdvance;
					if nDosage == 0.5 then
						nDifNew = nDif + DataCommon.affliction_half_peligrosity;
						nIncubationNew = nIncubation / 2;
						nAdvanceNew = nAdvance / 2;
						nDosage = 1;
					end
					local nDosageRound = math.floor(nDosageNew);
					nDosage = math.floor(nDosage);
					nDifNew = nDifNew + (nDosageRound - nDosage) * DataCommon.affliction_multiple_peligrosity;
					nIncubationNew = math.max(1, math.ceil(nIncubationNew * nDosage / nDosageRound));
					nAdvanceNew = math.max(1, math.ceil(nAdvanceNew * nDosage / nDosageRound));

					-- Message
					local sUser = ""
					if not Session.IsHost then
						sUser = User.getUsername();
					end
					local nodeCT = ActorManager.getCTNode(rTarget)
					local sMessage = string.format("%s: [%s%d] -> ", msgOOB.sNameAffliction, Interface.getString("affliction_action_dosage_increase"), nDosageNew);
					EffectManager.message(sMessage, nodeCT, false, sUser)

					-- Check if a save must be triggered
					if nTime < nIncubation then
						if nTime >= nIncubationNew then
							-- TRIGGER
						end
					else
						nIncubationNew = nIncubation;
						local nRemaining = math.fmod(nTime - nIncubation, nAdvance);
						if nRemaining >= nAdvanceNew then
							-- TRIGGER
						end
					end

					DB.setValue(v, "advance", "number",  nAdvanceNew);
					DB.setValue(v, "incubation", "number",  nIncubationNew);
					DB.setValue(v, "save_dif", "number", nDifNew);
					DB.setValue(v, "dosage", "number", nDosageNew);
				end
				break;
			end
			
		end
	end

	-- Apply effect
	if bAdd then
		EffectManagerSS.addAfflictionEffect(rActor, rTarget, msgOOB)
	end

end


function performAffliction(rActor, rAction)
	-- Get the summoned object node
	local _, nodeActor = ActorManager.getTypeAndNode(rActor);

	-- Message
	local msgShort = {font = "msgfont", text=Interface.getString("summoned"), mode="chat_unknown"};
	local msgLong = {font = "msgfont", text=Interface.getString("summoned"), mode="chat_unknown"};

	-- Send message
	local bSecret = false;
	ActionsManager.outputResult(bSecret, rActor, rActor, msgLong, msgShort);
end

----------------------------------
-- ROLL
----------------------------------

function performRoll(draginfo, rActor, rAction)
	local rRoll = getRoll(rActor, rAction);

	ActionsManager.performAction(draginfo, rActor, rRoll);
end


function getRoll(rActor, rAction)
	if not rAction.data then
		Debug.chat("No valid affliction linked to power");
		return;
	end

	-- Get info
	local rRoll = {};
	rRoll.sType = "affliction";
	rRoll.sNameAffliction = rAction.nameAffliction;
	rRoll.sTypeAffliction = Interface.getString(rAction.typeAffliction);
	rRoll.nDif = rAction.peligrosity;
	rRoll.nIncubation = Utilities.getDurationFromString(rAction.incubation);
	rRoll.nAdvance = Utilities.getDurationFromString(rAction.advance);
	rRoll.nRecovery = Utilities.getDurationFromString(rAction.recovery);
	rRoll.sKeywords = rAction.keywords;
	rRoll.nIniCat = rAction.initial_cat;
	rRoll.sPath = rAction.path;
	rRoll.nDosage = rAction.level;
	if rRoll.nDosage > 0 and rRoll.nDosage < 1 then
		rRoll.nDosage = 0.5;
	elseif rRoll.nDosage > 1 then
		rRoll.nDosage = math.floor(rRoll.nDosage);
	end
	-- rRoll.nMod = rRoll.nDosage;
	rRoll.nMod = 0;

	-- Description
	rRoll.sDesc = rRoll.sTypeAffliction:upper() .. "\n\n" .. rRoll.sNameAffliction:upper();
	if rAction.delivery ~= "" then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString(rAction.delivery);
	end
	if rRoll.nDosage == 0.5 then
		rRoll.sDesc = rRoll.sDesc .. Interface.getString("affliction_action_dosage_half");
	elseif rRoll.nDosage > 1 then
		rRoll.sDesc = rRoll.sDesc .. Interface.getString("affliction_action_dosage"):format(rRoll.nDosage);
	end
	if rRoll.sKeywords ~= "" then
		rRoll.sDesc = rRoll.sDesc .. Interface.getString("affliction_action_keywords") .. rRoll.sKeywords;
	end

	return rRoll;
end


function onAffliction(rSource, rTarget, rRoll)
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	rMessage.font = "chat";
	rMessage.mode = "chat_affliction";
	rMessage.text = rRoll.sDesc;

	-- Send the chat message
	local bShowMsg = not (rRoll.nAvoidNotifyAction == 1);
	if bShowMsg and rTarget and rTarget.nOrder and rTarget.nOrder ~= 1 then
		bShowMsg = false;
	end
	if bShowMsg then
		-- Comm.deliverChatMessage(rMessage);
		MessageManager.onRollMessage(rMessage);
	end

	notifyApplyAffliction(rSource, rTarget, rRoll);
end


----------------------------------
-- AUXILIAR
----------------------------------

function getAfflictionCategories(sPath)
	local nodeAf = DB.findNode(sPath);
	if not nodeAf then
		return {}, 0;
	end

	local rCategories = {};
	local nMax = 0;
	local nMin = 99;
	for _,v in pairs(nodeAf.createChild("categories").getChildren()) do
		local nCat = DB.getValue(v, "category", 0);
		if nCat > 0 then
			local rAux = {
				["damage"] = DB.getValue(v, "damage", ""),
				["effect"] = DB.getValue(v, "effect", 0),
			};
			rCategories[nCat] = rAux;
			nMax = math.max(nCat, nMax);
			nMin = math.min(nMin, nCat);
		end
	end
	if nMin > 1 then
		for ind = 1,nMin-1 do
			rCategories[nCat] = {["effect"] = "", ["damage"] = ""};
		end
	end

	return rCategories, nMax;
end

function performAfflictionSave()
	

	-- Initial save (done internally as rolling incurs in desync)
	local nTargetDC = msgOOB.nDif;
	if msgOOB.nDosage > 0 and msgOOB.nDosage < 1 then
		nTargetDC = nTargetDC - DataCommon.affliction_half_peligrosity;
	elseif msgOOB.nDosage > 1 then
		nTargetDC = nTargetDC + DataCommon.affliction_multiple_peligrosity * (msgOOB.nDosage - 1);
	end

	local sKeywords = msgOOB.sTypeAffliction .. ", " .. msgOOB.sKeywords;
	local nValue, nAdv, _, _, _, _, aDice = getCheck(rActor, "constitution", nodeSkill, nil, nil, nil, sKeywords, true);
	local nTotal = nValue + CombatManager2.simpleRoll("dT", nAdv);
	for _,v in pairs(aDice) do
		nTotal = nTotal + CombatManager2.simpleRoll(v);
	end

	local sResult = "";
	local sResultMessage = "";
	local nGrade = 0;
	if (nTotal >= nTargetDC) and (nTotal < nTargetDC + DataCommon.critical_success) then
		nGrade = math.floor((nTotal - nTargetDC)/DataCommon.check_grade) + 1;
		sResult = "success";
		sResultMessage = "[" .. Interface.getString("success") .. Interface.getString("grade").. nGrade .. ")]";
		if nGrade > 1 then
			msgOOB.nIniCat = msgOOB.nIniCat - 1;
		end

	elseif nTotal >= nTargetDC + DataCommon.critical_success then
		nGrade = math.floor((nTotal - nTargetDC - DataCommon.critical_success)/DataCommon.check_grade) + 1;
		sResult = "critical success";
		sResultMessage = "[" .. Interface.getString("success_crit") .. Interface.getString("grade").. nGrade .. ")]";

	elseif (nTotal < nTargetDC) and (nTotal >= nTargetDC - DataCommon.critical_miss) then
		nGrade = math.floor((nTargetDC - nTotal -1)/DataCommon.check_grade) + 1;
		sResult = "failure";
		sResultMessage = "[" .. Interface.getString("miss_crit") .. Interface.getString("grade").. nGrade .. ")]";

	else
		nGrade = math.floor((nTargetDC - nTotal - DataCommon.critical_miss)/DataCommon.check_grade) + 1;
		sResult = "critical failure";
		sResultMessage = "[" .. Interface.getString("miss") .. Interface.getString("miss_crit").. nGrade .. ")]";
	end
end
