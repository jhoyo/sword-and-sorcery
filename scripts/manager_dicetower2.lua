-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
    -- Override CoreRPG
    DiceTowerManager.handleDiceTower = handleDiceTower;
    DiceTowerManager.sendRoll = sendRoll;
	DiceTowerManager.decodeRollFromOOB = decodeRollFromOOB;
end


--------------------------------------
-- Override CoreRPG
--------------------------------------

function decodeRollFromOOB(msgOOB)
	msgOOB.type = nil;
	msgOOB.sender = nil;
	msgOOB.bTower = true;
	msgOOB.bSecret = true;

	msgOOB.nMod = tonumber(msgOOB.nMod) or 0;	
	UtilityManager.simplifyDecode(msgOOB, "aDice");

	-- Lua problem: In some puntual cases, in UtilityManager.simplifyDecode the function pairs don't run over all msgOOB keys and some dice is mising. This is solved here
	if msgOOB.aDice and #msgOOB.aDice == 1 and msgOOB.aDice[1].type == "d8" and msgOOB.aDice.expr == "2d8" then
		table.insert(msgOOB.aDice, {type = "d8"})
	end

	return msgOOB;
end


function sendRoll(rRoll, rSource)
	local msgOOB = DiceTowerManager.encodeOOBFromRoll(rRoll, rSource);

	-- if not Session.IsHost then
	-- 	local msg = { font = "chatfont", icon = "dicetower_icon" };
	-- 	if rSource then
	-- 		msg.sender = ActorManager.getDisplayName(rSource);
	-- 	end
	-- 	msg.text = string.format("%s [%s]", msgOOB.sDesc or "", msgOOB.sDice);
		
	-- 	Comm.addChatMessage(msg);
	-- end

	msgOOB.sDice = nil;
	Comm.deliverOOBMessage(msgOOB, "");
end

function handleDiceTower(msgOOB)
	local rActor = nil;
	if msgOOB.sender and msgOOB.sender ~= "" then
		rActor = ActorManager.resolveActor(msgOOB.sender);
	end

	local rRoll = DiceTowerManager.decodeRollFromOOB(msgOOB);
	rRoll.sDesc = (rRoll.sDesc or "") .. Interface.getString("dicetower_tag");
	
	ActionsManager.roll(rActor, nil, rRoll);
end
