--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--
function onInit()
	ActionsManager.registerModHandler("roll", modRoll);
	ActionsManager.registerResultHandler("roll", onRoll);
end

-- function handleApplyAttack(msgOOB)
-- 	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
-- 	local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);
--
-- 	local nTotal = tonumber(msgOOB.nTotal) or 0;
-- 	applyAttack(rSource, rTarget, (tonumber(msgOOB.nSecret) == 1), msgOOB.sAttackType, msgOOB.sDesc, nTotal, msgOOB.sResults);
-- end
--
-- function notifyApplyAttack(rSource, rTarget, bSecret, sAttackType, sDesc, nTotal, sResults)
-- 	if not rTarget then
-- 		return;
-- 	end
--
-- 	local msgOOB = {};
-- 	msgOOB.type = OOB_MSGTYPE_APPLYATK;
--
-- 	if bSecret then
-- 		msgOOB.nSecret = 1;
-- 	else
-- 		msgOOB.nSecret = 0;
-- 	end
-- 	msgOOB.sAttackType = sAttackType;
-- 	msgOOB.nTotal = nTotal;
-- 	msgOOB.sDesc = sDesc;
-- 	msgOOB.sResults = sResults;
--
-- 	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
-- 	msgOOB.sSourceType = sSourceType;
-- 	msgOOB.sSourceNode = sSourceNode;
--
-- 	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
-- 	msgOOB.sTargetType = sTargetType;
-- 	msgOOB.sTargetNode = sTargetNode;
--
-- 	Comm.deliverOOBMessage(msgOOB, "");
-- end

function performRoll(draginfo, rActor, rAction)
	local rRoll = getRoll(rActor, rAction);

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function getRoll(rActor, rAction)
	local nAdv = rAction.nAdv or 0;

	-- Build basic roll
	local rRoll = {};
	rRoll.sType = "roll";
	rRoll.aDice = rAction.aDice or {};

	-- Build the description label
	rRoll.sDesc = rAction.label or "";
	rRoll.nMod = rAction.mod or 0;
	rRoll.nAdv = 0;
	rRoll.sLabel = rAction.sName:lower();

	-- Bild clauses
	if rAction.clauses then
		rRoll.clauses = rAction.clauses;
		for _,vClause in pairs(rRoll.clauses) do
			for _,vDie in ipairs(vClause.dice) do
				table.insert(rRoll.aDice, vDie);
			end
			rRoll.nMod = rRoll.nMod + vClause.modifier;
			local sAbility = DataCommon.ability_ltos[vClause.stat];
			if sAbility then
				rRoll.sDesc = rRoll.sDesc .. string.format(" [MOD: %s (%s)]", sAbility, vClause.statmult or 1);
			end
		end
	end

	return rRoll;
end

function modRoll(rSource, rTarget, rRoll)
	-- Proceed to the next step
	ActionsManager2.encodeDesktopMods(rRoll);
end

function onRoll(rSource, rTarget, rRoll)
	-- Make the rolls
	ActionsManager2.decodeAdvantage(rRoll);

	-- Create the print message
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	rMessage.text = string.gsub(rMessage.text, " %[MOD:[^]]*%]", "");
	rMessage.mode = "chat_unknown";

	-- Comm.deliverChatMessage(rMessage);
	MessageManager.onRollMessage(rMessage);

	-- Save summon
	if rRoll.sLabel then
		setRollResult(rSource, rRoll.sLabel:lower(), ActionsManager.total(rRoll));
	end

end

---------------
-- Save Roll info
---------------

aRollResults = {};

function setRollResult(rSource, sLabel, nTotal)
	if rSource then
		sSourceCT = ActorManager.getCTNodeName(rSource);
		if not aRollResults[sSourceCT] then
			aRollResults[sSourceCT] = {};
		else
			for k, v in pairs(aRollResults[sSourceCT]) do
				if v.label == sLabel then
					table.remove(aRollResults[sSourceCT], k);
				end
			end
		end
		table.insert(aRollResults[sSourceCT], {label=sLabel, result=nTotal});
	end
end

function getRollResult(rSource, sLabel)
	local nLevel = 0;
	if rSource then
		sSourceCT = ActorManager.getCTNodeName(rSource);
		if aRollResults[sSourceCT] then
			for k, v in pairs(aRollResults[sSourceCT]) do
				if v.label == sLabel then
					nLevel = v.result;
				end
			end
		end
	end
	return nLevel
end

function clearRollResult(rSource, sLabel)
	if rSource then
		sSourceCT = ActorManager.getCTNodeName(rSource);
		if aRollResults[sSourceCT] then
			for k, v in pairs(aRollResults[sSourceCT]) do
				if v.label == sLabel then
					table.remove(aRollResults[sSourceCT], k);
				end
			end
		end
	end
end

function getRollLabels(rSource)
	local aLabels = {};
	if rSource then
		sSourceCT = ActorManager.getCTNodeName(rSource);
		if aRollResults[sSourceCT] then
			for k, v in pairs(aRollResults[sSourceCT]) do
				table.insert(aLabels, v.label);
			end
		end
	end
	return aLabels;
end


-- function applyAttack(rSource, rTarget, bSecret, sAttackType, sDesc, nTotal, sResults)
-- 	local msgShort = {font = "msgfont"};
-- 	local msgLong = {font = "msgfont"};
--
-- 	msgShort.text = "Attack ->";
-- 	msgLong.text = "Attack [" .. nTotal .. "] ->";
-- 	if rTarget then
-- 		msgShort.text = msgShort.text .. " [at " .. ActorManager.getDisplayName(rTarget) .. "]";
-- 		msgLong.text = msgLong.text .. " [at " .. ActorManager.getDisplayName(rTarget) .. "]";
-- 	end
-- 	if sResults ~= "" then
-- 		msgLong.text = msgLong.text .. " " .. sResults;
-- 	end
--
-- 	msgShort.icon = "roll_attack";
-- 	if string.match(sResults, "%[CRITICAL HIT%]") then
-- 		msgLong.icon = "roll_attack_crit";
-- 	elseif string.match(sResults, "HIT%]") then
-- 		msgLong.icon = "roll_attack_hit";
-- 	elseif string.match(sResults, "MISS%]") then
-- 		msgLong.icon = "roll_attack_miss";
-- 	else
-- 		msgLong.icon = "roll_attack";
-- 	end
--
-- 	ActionsManager.outputResult(bSecret, rSource, rTarget, msgLong, msgShort);
-- end
