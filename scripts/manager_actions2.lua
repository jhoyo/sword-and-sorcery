--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function encodeDesktopMods(rRoll)
	local nMod = 0;

	if nMod == 0 then
		return;
	end

	rRoll.nMod = rRoll.nMod + nMod;
	rRoll.sDesc = rRoll.sDesc .. string.format(" [%+d]", nMod);
end

function encodeAdvantage(rRoll, bAvoidButtons)
	if not rRoll.nNoAdv then
		-- Safety
		local nAdv = 0;
		if rRoll.nAdv then
			nAdv = tonumber(rRoll.nAdv);
		end

		-- Add desktop button advantages
		if not bAvoidButtons then
			nAdv = nAdv + getDescktopAdvantage();
			rRoll.nAdv = nAdv;
		end

		-- Add dice and description
		local Ndice = #(rRoll.aDice);
		if nAdv == 1 then
			rRoll.sDesc = rRoll.sDesc .. " [ADV]";
			table.insert(rRoll.aDice, Ndice+1, "d8");
			table.insert(rRoll.aDice, Ndice+2, "d8");
		elseif nAdv > 1 then
			rRoll.sDesc = rRoll.sDesc .. " [ADV x" .. nAdv .. "]";
			for ind=1,nAdv do
				table.insert(rRoll.aDice, Ndice+2*ind-1, "d8");
				table.insert(rRoll.aDice, Ndice+2*ind, "d8");
			end
		elseif nAdv == -1 then
			rRoll.sDesc = rRoll.sDesc .. " [DIS]";
			table.insert(rRoll.aDice, Ndice+1, "d8");
			table.insert(rRoll.aDice, Ndice+2, "d8");
		elseif nAdv < -1 then
			rRoll.sDesc = rRoll.sDesc .. " [DIS x" .. math.abs(nAdv) .. "]";
			for ind=1,math.abs(nAdv) do
				table.insert(rRoll.aDice, Ndice+2*ind-1, "d8");
				table.insert(rRoll.aDice, Ndice+2*ind, "d8");
			end
		end
	end
end

function getDescktopAdvantage()
	local nAdv = 0;
	local bButtonADV1 = ModifierStack.getModifierKey("ADV1");
	local bButtonADV2 = ModifierStack.getModifierKey("ADV2");
	local bButtonADV3 = ModifierStack.getModifierKey("ADV3");
	local bButtonDIS1 = ModifierStack.getModifierKey("DIS1");
	local bButtonDIS2 = ModifierStack.getModifierKey("DIS2");
	local bButtonDIS3 = ModifierStack.getModifierKey("DIS3");
	if bButtonADV1 then
		nAdv = nAdv + 1;
	end
	if bButtonADV2 then
		nAdv = nAdv + 2;
	end
	if bButtonADV3 then
		nAdv = nAdv + 3;
	end
	if bButtonDIS1 then
		nAdv = nAdv - 1;
	end
	if bButtonDIS2 then
		nAdv = nAdv - 2;
	end
	if bButtonDIS3 then
		nAdv = nAdv - 3;
	end
	return nAdv;
end

function decodeAdvantage(rRoll)

	-- Safety
	local nAdv = 0;
	if rRoll.nAdv then
		nAdv = tonumber(rRoll.nAdv);
	end

	-- Extract critical attack and deffense (if applicable)
	local nInfThres = 2;
	local nSupThres = 16;
	local nCrit = tonumber(rRoll.nCrit) or 0;
	if nCrit > 0 then
		nSupThres = nSupThres - nCrit;
	elseif nCrit < 0 then
		nInfThres = nInfThres - nCrit;
	end

	-- Make the roll
	local Ndice = #(rRoll.aDice);
	if Ndice > 1 then
		-- Extract raw the results
		local aResult = {};
		local aDicesType = {};
		local aDicesResult = {};
		local ind_res = 0;
		local ind_other = 0;
		local ind_count = 0;
		local nAddDice = 2 * math.abs(nAdv) + 2;
		for ind = 1, Ndice do
			if (ind_count < nAddDice) and (string.sub(rRoll.aDice[ind].type, 2) == "8") then
				ind_res = ind_res + 1;
				aResult[ind_res] = rRoll.aDice[ind].result;
				ind_count = ind_count + 1;
			else
				ind_other = ind_other + 1;
				aDicesType[ind_other] = rRoll.aDice[ind].type
				aDicesResult[ind_other] = rRoll.aDice[ind].result
			end
		end
		-- Transform them into merged results
		local aResultM = {};
		local ind_res_merged = 0;
		local sign = 0;
		for ind = 1, math.floor(ind_res/2) do
			ind_res_merged = ind_res_merged + 1;
			aResultM[ind_res_merged] = aResult[ind*2-1] + aResult[ind*2];
			-- Add or substract the extra die for the rule of 2 and rule of 16
			if aResultM[ind_res_merged] <= nInfThres or aResultM[ind_res_merged] >= nSupThres then
				local nExtra = math.random(1,8)
				if aResultM[ind_res_merged] <= nInfThres then
					sign = -1;
				else --elseif aResultM[ind_res_merged] >= nSupThres then
					sign = 1;
				end
				aResultM[ind_res_merged] = aResultM[ind_res_merged] + sign * nExtra;
				-- That die explodes
				while nExtra == 8 do
					nExtra = math.random(1, 8)
					aResultM[ind_res_merged] = aResultM[ind_res_merged] + sign * nExtra;
				end
			end
		end

		-- Taylor the dice throwed chat prints. Only if needed
		if ind_res_merged > 0 then
			-- Print
			if nAdv ~= 0 then
				nFinal = aResultM[#aResultM]
				rRoll.sDesc = rRoll.sDesc .. "\n[ORIGINAL ";
				for ind = 1,#aResultM do
					if ind > 1 then
						rRoll.sDesc = rRoll.sDesc .. ", ";
					end
					rRoll.sDesc = rRoll.sDesc .. aResultM[ind]
				end
				rRoll.sDesc = rRoll.sDesc .. "]";
				table.sort(aResultM)
			end

			-- Result
			local nFinal;
			if nAdv > 0 then
				nFinal = aResultM[#aResultM]
				rRoll.aDice[1].type = "gT";
			elseif nAdv < 0 then
				nFinal = aResultM[1]
				rRoll.aDice[1].type = "rT";
			else
				nFinal = aResultM[1]
				rRoll.aDice[1].type = "dT";
			end
			rRoll.aDice[1].result = nFinal;
			rRoll.aDice[1].value = nFinal;
			-- Reshape the rest of the dies
			if ind_other > 0 then
				for ind = 1,ind_other do
					rRoll.aDice[1+ind].result = aDicesResult[ind];
					rRoll.aDice[1+ind].value = aDicesResult[ind];
					rRoll.aDice[1+ind].type = aDicesType[ind];
				end
			end
			-- Remove the rest of the dies
			while #(rRoll.aDice) > 1+ind_other do
				table.remove(rRoll.aDice, 2+ind_other);
			end
		end
	end

end

-- ExtractAdvantage
function extractAdvantage(str)
	-- See if there is advantage or disadvantage
	local bADV = string.match(str, "%[ADV");
	local bDIS = string.match(str, "%[DIS");
	-- Calculate the advantage degree
	local nAdv = 0;
	local nDis = 0;
	if bADV then
		local sAdv = string.match(str, "%[ADV x%d%]");
		if sAdv then
			nAdv = tonumber(string.match(sAdv, "%d"));
		else
			nAdv = 1;
		end
	end
	if bDIS then
		local sDis = string.match(str, "%[DIS x%d%]");
		if sDis then
			nDis = tonumber(string.match(sDis, "%d"));
		else
			nDis = 1;
		end
	end
	return nAdv - nDis;
end


-------------------------
-- OVERRIDE CORERPG
-------------------------

function onInit()
	ActionsManager.encodeRollForDrag = encodeRollForDrag;
	ActionsManager.decodeRollFromDrag = decodeRollFromDrag;
end


function encodeRollForDrag(draginfo, i, vRoll)
	draginfo.setSlot(i);

	for k,v in pairs(vRoll) do
		if k == "sType" then
			draginfo.setSlotType(v);
		elseif k == "sDesc" then
			draginfo.setStringData(v);
		elseif k == "aDice" then
			draginfo.setDieList(v);
		elseif k == "nMod" then
			draginfo.setNumberData(v);
		elseif type(k) ~= "table" then
			local sk = tostring(k) or "";
			if sk ~= "" then
				if type(v) ~= "table" then
					draginfo.setMetaData(sk, tostring(v) or "");
				else
					draginfo.setMetaData(sk, table2string(v) or "");
				end
			end
		end
	end
end

function decodeRollFromDrag(draginfo, i, bFinal)
	draginfo.setSlot(i);

	local vRoll = draginfo.getMetaDataList();
	if vRoll.rAction then
		vRoll.rAction = string2table(vRoll.rAction);
	end

	vRoll.sType = draginfo.getSlotType();
	if vRoll.sType == "" then
		vRoll.sType = draginfo.getType();
	end

	vRoll.sDesc = draginfo.getStringData();
	if bFinal and vRoll.sDesc == "" then
		vRoll.sDesc = draginfo.getDescription();
	end
	vRoll.nMod = draginfo.getNumberData();

	vRoll.aDice = draginfo.getDieList() or {};

	vRoll.bSecret = draginfo.getSecret();

	if vRoll.aAddDice then
		vRoll.aAddDice = string2table(vRoll.aAddDice);
	end
	return vRoll;
end

function encodeActionForDrag(draginfo, rSource, sType, rRolls)
	ActionsManager.encodeActors(draginfo, rSource);
	
	draginfo.setType(sType);
	
	if GameSystem.actions[sType] and GameSystem.actions[sType].sIcon then
		draginfo.setIcon(GameSystem.actions[sType].sIcon);
	elseif sType ~= "dice" then
		draginfo.setIcon("action_roll");
	end
	if #rRolls == 1 then
		draginfo.setDescription(rRolls[1].sDesc);
	end
	
	for kRoll, vRoll in ipairs(rRolls) do
		ActionsManager.encodeRollForDrag(draginfo, kRoll, vRoll);
	end
	
	local bSecret = (#rRolls > 0 and rRolls[1].bSecret);
	draginfo.setSecret(bSecret);
end

function decodeActionFromDrag(draginfo, bFinal)
	local rSource, aTargets = ActionsManager.decodeActors(draginfo);
	
	local rRolls = {};
	for i = 1, draginfo.getSlotCount() do
		table.insert(rRolls, ActionsManager.decodeRollFromDrag(draginfo, i, bFinal));
	end
	
	return rSource, rRolls, aTargets;
end


-------------
-- AUX
-------------

function table2string(t)
	s = "";
	for k, v in pairs(t) do
		local sType = type(v);
		s = s .. tostring(k) .. "_" .. sType .. "_" .. tostring(v) .. "/";
	end
	return s;
end

function string2table(s)
	t = {};
	for _, v in pairs(StringManager.split(s, "/")) do
		local aValues = StringManager.split(v, "_");
		if aValues[2] == "number" then
			t[aValues[1]] = tonumber(aValues[3]);
		elseif aValues[2] == "boolean" then
			t[aValues[1]] = aValues[3] == "true";
		else
			t[aValues[1]] = aValues[3];
		end
	end
	return t;
end