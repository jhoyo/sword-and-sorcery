--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYATK = "applyatk";
OOB_MSGTYPE_APPLYHRFC = "applyhrfc";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYATK, handleApplyAttack);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYHRFC, handleApplyHRFC);

	ActionsManager.registerTargetingHandler("attack", onTargeting);
	ActionsManager.registerModHandler("attack", modAttack);
	ActionsManager.registerResultHandler("attack", onAttack);
end

function handleApplyAttack(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);

	local nTotal = tonumber(msgOOB.nTotal) or 0;
	applyAttack(rSource, rTarget, (tonumber(msgOOB.nSecret) == 1), msgOOB.sAttackType, msgOOB.sResult, nTotal, msgOOB.sMessage, msgOOB.sMessageShort);
end

function notifyApplyAttack(rSource, rTarget, bSecret, sAttackType, sResult, nTotal, sMessage, sMessageShort)
	if not rTarget then
		return;
	end

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYATK;

	if bSecret then
		msgOOB.nSecret = 1;
	else
		msgOOB.nSecret = 0;
	end
	msgOOB.sAttackType = sAttackType;
	msgOOB.nTotal = nTotal;
	msgOOB.sResult = sResult;
	msgOOB.sMessage = sMessage;
	msgOOB.sMessageShort = sMessageShort;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	msgOOB.sTargetType = sTargetType;
	msgOOB.sTargetNode = sTargetNode;

	Comm.deliverOOBMessage(msgOOB, "");
end

-- function handleApplyHRFC(msgOOB)
-- 	TableManager.processTableRoll("", msgOOB.sTable);
-- end

-- function notifyApplyHRFC(sTable)
-- 	local msgOOB = {};
-- 	msgOOB.type = OOB_MSGTYPE_APPLYHRFC;

-- 	msgOOB.sTable = sTable;

-- 	Comm.deliverOOBMessage(msgOOB, "");
-- end

function onTargeting(rSource, aTargeting, rRolls)
	local bRemoveOnMiss = false;
	local sOptRMMT = OptionsManager.getOption("RMMT");
	if sOptRMMT == "on" then
		bRemoveOnMiss = true;
	elseif sOptRMMT == "multi" then
		local aTargets = {};
		for _,vTargetGroup in ipairs(aTargeting) do
			for _,vTarget in ipairs(vTargetGroup) do
				table.insert(aTargets, vTarget);
			end
		end
		bRemoveOnMiss = (#aTargets > 1);
	end

	if bRemoveOnMiss then
		for _,vRoll in ipairs(rRolls) do
			vRoll.bRemoveOnMiss = "true";
		end
	end

	return aTargeting;
end

-- function performPartySheetVsRoll(draginfo, rActor, rAction)
-- 	local rRoll = getRoll(nil, rAction);

-- 	if DB.getValue("partysheet.hiderollresults", 0) == 1 then
-- 		rRoll.bSecret = true;
-- 		rRoll.bTower = true;
-- 	end

-- 	ActionsManager.actionDirect(nil, "attack", { rRoll }, { { rActor } });
-- end

function performRoll(draginfo, rActor, rAction)
	local rRoll = getRoll(rActor, rAction);

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function getRoll(rActor, rAction)

	-- Build basic roll
	local rRoll = {};
	rRoll.sType = "attack";
	rRoll.aDice = Utilities.addEffectDice({ "d8", "d8" }, rAction.aDice or {}, "b");

	rRoll.nAdv = rAction.nAdv or 0;
	if rAction.bElectric then
		rRoll.nElectric =  1;
	else
		rRoll.nElectric =  0;
	end

	-- Build the description label
	if rAction.bTouch then
		rRoll.sDesc = Interface.getString("modifier_label_touch_attack"):upper();
		rRoll.nTouch = 1;
	else
		rRoll.nTouch = 0;
		rRoll.sDesc = Interface.getString("modifier_label_attack"):upper();
	end
	if rAction.order and rAction.order > 1 then
		rRoll.sDesc = rRoll.sDesc .. " #" .. rAction.order;
	end
	rRoll.sDesc = rRoll.sDesc .. "\n\n" .. rAction.label:upper();

	-- Add ability label
	if rAction.ability then
		local sAbilityEffect = rAction.ability;
		if sAbilityEffect then
			rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_groupstat") .. ": " .. StringManager.capitalize(sAbilityEffect);
		end
		rRoll.sAbility = sAbilityEffect;
	end

	-- Add skill label
	if rAction.skill then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_groupskill") .. ": " .. rAction.skill;
		rRoll.sSkill = rAction.skill;
	end

	-- Add built-in bonus
	rRoll.modifier = rAction.modifier or rAction.mod or 0;
	if rRoll.modifier ~= 0 or (rAction.aDice and #rAction.aDice > 0) then
		local sString = StringManager.convertDiceToString(rRoll.aDice, rRoll.modifier);
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_groupmod") .. ": " .. sString;
	end

	-- Add range
	if rAction.type then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("ct_tooltip_reach") .. ": "
		if rAction.type == 1 then
			rRoll.sAttackType = Interface.getString("ranged")
		elseif rAction.type == 2 then
			rRoll.sAttackType = Interface.getString("thrown")
		else
			rRoll.sAttackType = Interface.getString("melee")
		end
		rRoll.sDesc = rRoll.sDesc .. rRoll.sAttackType;
	end

	-- Add action array
	rRoll.rAction = rAction;

	return rRoll;
end

function modAttack(rSource, rTarget, rRoll)
	-- Start
	clearHitExcess(rSource);
	local rAction = rRoll.rAction;

	-- Build attack filter
	local aAttackFilter = {};
	if rAction.type == 2 then
		table.insert(aAttackFilter, DataCommon.rangetypes_inv["distance"]);
		table.insert(aAttackFilter, DataCommon.rangetypes_inv["thrown"]);
	elseif rAction.type == 1 then
		table.insert(aAttackFilter, DataCommon.rangetypes_inv["distance"]);
		table.insert(aAttackFilter, DataCommon.rangetypes_inv["ranged"]);
	else
		table.insert(aAttackFilter, DataCommon.rangetypes_inv["melee"]);
	end
	if rAction.bOpportunity then
		table.insert(aAttackFilter, DataCommon.opportunity);
	end

	-- Get attack modifier
	local nMod, nAdv, nEffect, bAutofail, nRanges, aAddDice = ActorManager2.getAttack(rSource, rTarget, rRoll.sAbility, rRoll.sSkill, rAction);
	rRoll.nMod = nMod;
	rRoll.nAdv = rRoll.nAdv + nAdv;
	nEffect = nEffect - rRoll.modifier;
	rRoll.aDice = Utilities.addEffectDice(rRoll.aDice, aAddDice, "p");

	-- Add remaining description pieces
	if rAction.type > 0 and nRanges then
		rRoll.sDesc = rRoll.sDesc .. " (" .. Interface.getString("ct_tooltip_reach") .. " " .. nRanges .. ")";
	end
	if bOpportunity then
		rRoll.sDesc = rRoll.sDesc .. " (" .. Interface.getString("modifier_label_atkopp") .. ")";
	end
	local nPen = ActorManager2.getPenalizer(rSource);
	if nPen and nPen ~= 0 then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("penal") .. ": " .. nPen;
	end
	if nEffect ~= 0 or #aAddDice > 0 then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_effect") .. ": " .. StringManager.convertDiceToString(aAddDice, nEffect);
	end
	if bSuperiorCover then
		rRoll.sDesc = rRoll.sDesc .. " (" .. Interface.getString("modifier_label_atkcover") .. ")";
	elseif bCover then
		rRoll.sDesc = rRoll.sDesc .. " (" .. Interface.getString("modifier_label_atkscover") .. ")";
	end
	local nCrit = ActorManager2.getCriticalAttack(rActor, rTarget, rAction.nCrit or 0, aAttackFilter);
	if nCrit > 0 then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("critical_attack") .. ": " .. nCrit;
	elseif nCrit < 0 then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("critical_def") .. ": " .. -nCrit;
	end
	if bAutofail then
		rRoll.sDesc = rRoll.sDesc ..  "\n- " .. Interface.getString("autofail");
	end

	-- Save info
	rRoll.nAutofail = 0;
	if bAutofail then
		rRoll.nAutofail = 1;
	end
	rRoll.attackFilter = aAttackFilter;
	rRoll.type = rAction.type;
	rRoll.opportunity = bOpportunity;
	rRoll.nCrit = nCrit;

	-- Proceed to the next step
	ActionsManager2.encodeDesktopMods(rRoll);
	ActionsManager2.encodeAdvantage(rRoll);
end

function onAttack(rSource, rTarget, rRoll)
	-- Make the rolls
	ActionsManager2.decodeAdvantage(rRoll);

	local rAction = {};
	rAction.nTotal = ActionsManager.total(rRoll);
	local sMessage = "";
	local sMessageShort = "";

	-- Get the deffense of the defender
	local nDefenseVal, nodeUseDef, sTypeDefense = CombatManager2.getDefenseValue(rSource, rTarget, rRoll);

	-- Manage the result
	local nExcess = 0;
	local bElectric = rRoll.nElectric == "1" and ActorManager2.isMetallic(rTarget);
	local bTouch = (rRoll.nTouch == "1" or bElectric) and rAction.nTotal >= nDefenseVal - DataCommon.critical_miss;
	local bAutofail = rRoll.nAutofail == "1";
	if nDefenseVal then
		if not bAutofail and (rAction.nTotal >= nDefenseVal or bTouch) then
			nExcess = rAction.nTotal - nDefenseVal
			local sSign = "+";
			if bTouch then
				sSign = "";
			end
			sMessage = "[" .. Interface.getString("hit") .. Interface.getString("by") .. nExcess .. "]";
			sMessageShort = "[" .. Interface.getString("hit") .. "]";
			rAction.sResult = 'hit';
		elseif bAutofail or (rAction.nTotal < nDefenseVal - DataCommon.critical_miss) then
			local nDif = nDefenseVal - DataCommon.critical_miss - rAction.nTotal;
			sMessage = "[" .. Interface.getString("miss_crit") .. Interface.getString("by") .. nDif .. "]";
			sMessageShort = "[" .. Interface.getString("miss_crit") .. "]";
			rAction.sResult = 'fumble';
		else
			local nDif = nDefenseVal - rAction.nTotal;
			sMessage = "[" .. Interface.getString("miss") .. Interface.getString("defense_miss_" .. sTypeDefense) .. Interface.getString("by") .. nDif .. "]";
			sMessageShort = "[" .. Interface.getString("miss") .. Interface.getString("defense_miss_" .. sTypeDefense) .. "]";
			rAction.sResult = 'miss';
		end
	end

	-- If not a fumble, use the deffense
	if rAction.sResult ~= "fumble" and nodeUseDef then
		CombatManager2.useDefense(nodeUseDef, rTarget);
	end

	-- Create the print message
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	-- rMessage.text = string.gsub(rMessage.text, " %[MOD:[^]]*%]", "");

	-- Print in chat
	if not rTarget then
		rMessage.text = rMessage.text .. " " .. sMessage;
	end

	rMessage.mode = "chat_attack"
	-- Comm.deliverChatMessage(rMessage);
	MessageManager.onRollMessage(rMessage);

	if rTarget then
		notifyApplyAttack(rSource, rTarget, rRoll.bSecret, rRoll.sAttackType, rRoll.sResult, rAction.nTotal, sMessage, sMessageShort);
	end

	-- TRACK HIT
	if rAction.sResult == "hit" then
		setHitExcess(rSource, rTarget, nExcess);
	end

	-- REMOVE TARGET ON MISS OPTION
	if rTarget then
		if (rAction.sResult == "miss" or rAction.sResult == "fumble failure") then
			if rRoll.bRemoveOnMiss then
				TargetingManager.removeTarget(ActorManager.getCTNodeName(rSource), ActorManager.getCTNodeName(rTarget));
			end
		end
	end

end

function applyAttack(rSource, rTarget, bSecret, sAttackType, sResult, nTotal, sMessage, sMessageShort)
	local msgShort = {font = "msgfont"};
	local msgLong = {font = "msgfont", mode="chat_unknown"};

	msgShort.text = Interface.getString("attack") .. " (" .. sAttackType .. ") ->";
	msgLong.text = Interface.getString("attack") .. " (" .. sAttackType .. ") [" .. nTotal .. "] ->";
	if rTarget then
		msgShort.text = msgShort.text .. " [at " .. ActorManager.getDisplayName(rTarget) .. "]";
		msgLong.text = msgLong.text .. " [at " .. ActorManager.getDisplayName(rTarget) .. "]";
	end
	if sMessage ~= "" then
		msgLong.text = msgLong.text .. " " .. sMessage;
	end
	if sMessageShort ~= "" then
		msgShort.text = msgShort.text .. " " .. sMessageShort;
	end

	msgShort.icon = "roll_attack";
	msgShort.mode = "chat_unknown";
	if sResult == "fumble" then
		msgLong.mode = "chat_miss_crit";
		msgLong.icon = "chat_miss_crit";
	elseif sResult == "miss" then
		msgLong.mode = "chat_miss";
		msgLong.icon = "roll_attack_miss";
	elseif sResult == "hit" then
		msgLong.mode = "chat_success";
		msgLong.icon = "roll_attack_hit";
	else
		msgLong.mode = "chat_unknown";
		msgLong.icon = "roll_attack";
	end

	ActionsManager.outputResult(bSecret, rSource, rTarget, msgLong, msgShort);
end

aHitExcess = {};

function setHitExcess(rSource, rTarget, nValue)
	local sSourceCT = ActorManager.getCreatureNodeName(rSource);
	if sSourceCT == "" then
		return;
	end
	local sTargetCT = "";
	if rTarget then
		sTargetCT = ActorManager.getCTNodeName(rTarget);
	end

	if not aHitExcess[sSourceCT] then
		aHitExcess[sSourceCT] = {};
	end
	table.insert(aHitExcess[sSourceCT], {target=sTargetCT, value=nValue});
end

function clearHitExcess(rSource, rTarget)
	local sSourceCT = ActorManager.getCreatureNodeName(rSource);
	local sTargetCT = ActorManager.getCTNodeName(rTarget);
	if sSourceCT and sSourceCT ~= "" and aHitExcess then
		if aHitExcess and aHitExcess[sSourceCT] then
			if sTargetCT and sTargetCT ~= "" then
				for k, v in pairs(aHitExcess[sSourceCT]) do
					if v.target == sTargetCT then
						aHitExcess[sSourceCT][k] = nil;
					end
				end
			else
				if aHitExcess[sSourceCT] then
					aHitExcess[sSourceCT] = nil;
				end
			end
		end
	end
end

function getHitExcess(rSource, rTarget)
	local sSourceCT = ActorManager.getCreatureNodeName(rSource);
	if sSourceCT == "" then
		return 0;
	end
	local sTargetCT = "";
	if rTarget then
		sTargetCT = ActorManager.getCTNodeName(rTarget);
	end

	if not aHitExcess[sSourceCT] then
		return 0;
	end

	for k,v in ipairs(aHitExcess[sSourceCT]) do
		if v.target == sTargetCT then
			local nValue = v.value;
			table.remove(aHitExcess[sSourceCT], k);
			return nValue;
		end
	end

	return 0;
end
