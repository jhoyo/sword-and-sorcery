--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_SUMMON_NPC = "summon_npc";
OOB_MSGTYPE_REMOVE_NPC = "remove_npc";

local nExtraXY = 0;
local nImageGridSize = 50;

--------------------
-- SUMMON NPC
--------------------

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_SUMMON_NPC, handleSummonNPC);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_REMOVE_NPC, handleRemoveNPC);
end


function handleSummonNPC(msgOOB)
	local rActor = ActorManager.resolveActor(msgOOB.sActorNode);
	local _, nodeActor = ActorManager.getTypeAndNode(rActor);

	-- Summon
	local nodeSummon = DB.findNode(msgOOB.sIdentifier);
	local sOwner = nodeActor.getOwner();
	local nodeCT;
	for i = 1, msgOOB.nNumberSummons do
		if msgOOB.nIsPC and msgOOB.nIsPC == "1" then
            -- Summon "PCs" (mounts of PCs)
			nodeCT = CombatManager2.addPC(nodeSummon);
        else
            -- Rest of NPCs
            local nodeCT = CombatManager2.addNPC("npc", nodeSummon, nil, msgOOB.sFaction, sOwner);
            local sName = DB.getValue(nodeCT, "name", "") .. " (" .. Interface.getString("power_title_summon") .. ")";
            DB.setValue(nodeCT, "name", "string", sName);
			EffectManager.addEffect("", "", nodeCT, { sName = DataCommon.conditions["unaware"], nDuration = 1 }, true);
            if msgOOB.nID then
                DB.setValue(nodeCT, "nID", "number", msgOOB.nID);
            end
			DB.setValue(nodeCT, "initresult", "number", -99);
        end
		-- Add token to the image the PC is currently in
		addSummonedToken(nodeActor, nodeCT,i)
		-- Save mount node to remove it later
		if nodeCT and msgOOB.nIsMount and msgOOB.nIsMount == "1" then
			DB.setValue(nodeActor, "mountCTpath", "string", nodeCT.getPath());
		end
	end
end

function handleRemoveNPC(msgOOB)
	local nodeCTmount = DB.findNode(msgOOB.sIdentifier);
	if nodeCTmount then
		local token = CombatManager.getTokenFromCT(nodeCTmount);
		if token then
			token.delete();
		end
		nodeCTmount.delete();
	end
end

function performSummon(rActor, rAction)
	-- Get the summoned object node
	local _, nodeActor = ActorManager.getTypeAndNode(rActor);

	-- Message
	local msgShort = {font = "msgfont", text=Interface.getString("summoned"), mode="chat_unknown"};
	local msgLong = {font = "msgfont", text=Interface.getString("summoned"), mode="chat_unknown"};

	-- Choose correct summon
	local nIndex = rAction.level + ActionRoll.getRollResult(rActor, DataCommon.summon_label);
	local nNumberSummons = math.max(ActionRoll.getRollResult(rActor, DataCommon.number_label), rAction.number);
	local rSummon = rAction.summons[nIndex];
	if not rSummon then
		local nCurrent = -1;
		for k, v in ipairs(rAction.summons) do
			if k > nCurrent and k <= nIndex then
				nCurrent = k;
				rSummon = v;
			end
		end
	else
		nCurrent = nIndex;
	end
	if rSummon then
		msgShort.text = msgShort.text .. rSummon.sClass
		msgLong.text = msgLong.text .. rSummon.sClass .. " (" .. tostring(nCurrent) .. "): ";
		ActionRoll.clearRollResult(rActor, DataCommon.summon_label)
	else
		msgShort.text = Interface.getString("power_error_notenoughlevel");
		msgLong.text = Interface.getString("power_error_notenoughlevel");
		ActionsManager.outputResult(false, rActor, rActor, msgLong, msgShort);
		return;
	end

	-- Summon
	local nodeSummon = DB.findNode(rSummon.sIdentifier);
	if nodeSummon then
		local sName = DB.getValue(nodeSummon, "name", "");
		msgLong.text = msgLong.text .. sName;
		if nNumberSummons > 1 then
			msgLong.text = msgLong.text .. " (x" .. tostring(nNumberSummons) .. ")";
		end
		local nID = math.random(DataCommon.conc_ID_max);

	-- Concentration effect
		if rAction.nConc == 1 then
			local rConc = {};
			rConc.nDur = rAction.nDur;
			rConc.nLevel = rAction.level_cast + (ActionPower.nEffectsLastSpell or 0); -- TODO: No funciona
			rConc.sStat = rAction.stat;
			rConc.sSkill = rAction.skill;
			rConc.nID = nID;
			if rAction.bHiddenSpell then
				rConc.nHiddenSpell = 1;
			else
				rConc.nHiddenSpell = 0;
			end

			local rNewEffect = {};
			rNewEffect.sName = rAction.label .. "; " .. DataCommon.keyword_inactive["summon"] .. " " .. Interface.getString(rSummon.sClass) .. " (" .. sName .. ")";
			rNewEffect.nDuration = 0;

			EffectManagerSS.addConcentrationEffect(rActor, {rActor}, rNewEffect, rConc, false)

		end

		if rSummon.sClass == "npc" then
			local msgOOB = {};
			msgOOB.type = OOB_MSGTYPE_SUMMON_NPC;

			msgOOB.sIdentifier = rSummon.sIdentifier;
			msgOOB.sFaction = rAction.sFaction;
			msgOOB.nID = nID;
			msgOOB.nNumberSummons = nNumberSummons;

			local _, sActorNode = ActorManager.getTypeAndNodeName(rActor);
			msgOOB.sActorNode = sActorNode;

			Comm.deliverOOBMessage(msgOOB, "");

		elseif rSummon.sClass == "item" then
			local nodeInvlist = nodeActor.createChild("inventorylist");
			-- DB.copyNode(nodeItem, nodeInvlist);
			local nodeNewItem = ItemManager.addItemToList(nodeInvlist, "item", nodeSummon, false, 1);
			DB.setValue(nodeNewItem, "carried", "number", 2);
			-- if ItemManager2.isWeapon(nodeNewItem) then NOT NECESSARY
				-- CharManager.addToWeaponDB(nodeNewItem);
			-- end
			DB.setValue(nodeNewItem, "nID", "number", nID);
			local sName = DB.getValue(nodeNewItem, "name", "") .. " (" .. Interface.getString("power_title_summon") .. ")";
			DB.setValue(nodeNewItem, "name", "string", sName);
			DB.setValue(nodeNewItem, "count", "number", nNumberSummons)
			ItemManager2.makeMagicAttacks(nodeNewItem);
		end

	else
		msgShort.text = "Falied summoning, link does not correspond to any record."
		msgLong.text = "Falied summoning, link does not correspond to any record."
	end

	-- Send message
	local bSecret = false;
	ActionsManager.outputResult(bSecret, rActor, rActor, msgLong, msgShort);
end

function addMountToCT(nodeChar, nodeCharCT, nodeMount)
	if CombatManager2.inCombat() then
		local msgOOB = {};
		msgOOB.type = OOB_MSGTYPE_SUMMON_NPC;

		msgOOB.sIdentifier = nodeMount.getPath();
		msgOOB.sFaction = DB.getValue(nodeCharCT, "friendfoe", "");
		msgOOB.nNumberSummons = 1;
		msgOOB.sActorNode = nodeChar.getPath();
		msgOOB.nIsPC = 1;
		msgOOB.nIsMount = 1;
		if not ActorManager2.isPC(nodeChar) then
			msgOOB.nIsPC = 0;
		end

		Comm.deliverOOBMessage(msgOOB, "");
	end
end

function removeMountFromCT(nodeChar)
	local msgOOB = {};
    msgOOB.type = OOB_MSGTYPE_REMOVE_NPC;
    msgOOB.sIdentifier = DB.getValue(nodeChar, "mountCTpath", "");
    Comm.deliverOOBMessage(msgOOB, "");
end

function addSummonedToken(nodeActor, nodeCTsummon, nAddXY)
	nAddXY = nAddXY or 1;
	local nodeCT = ActorManager2.getCTNode(nodeActor);
	local tokenActor = CombatManager.getTokenFromCT(nodeCT);
	if tokenActor then
		local nodeContainer = tokenActor.getContainerNode();
		local x,y = tokenActor.getPosition();
		x = x + nImageGridSize * (nExtraXY + nAddXY);
		y = y + nImageGridSize * (nExtraXY + nAddXY);
		local tokenSummon = Token.addToken(nodeContainer.getPath(), DB.getValue(nodeCTsummon, "token", ""), x, y);
		TokenManager.linkToken(nodeCTsummon, tokenSummon);
		TokenManager.updateVisibility(nodeCTsummon);
	end
end