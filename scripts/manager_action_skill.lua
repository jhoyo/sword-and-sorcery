--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--
OOB_MSGTYPE_APPLYSAVE = "applysave";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYSAVE, handleApplySave);
	
	ActionsManager.registerModHandler("skill", modRoll);
	ActionsManager.registerResultHandler("skill", onRoll);
end
-- 
function handleApplySave(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);

	setSaveStatus(rSource, msgOOB.sOriginCT, msgOOB.sValue, msgOOB.nGrade, msgOOB.sEffect)
end


function performRoll(draginfo, rActor, rAction)
	local rRoll = getRoll(rActor, rAction);

	if (User.isHost() and CombatManager.isCTHidden(ActorManager.getCTNode(rActor))) or rAction.bSecret or rAction.nSecret == 1 or rRoll.bSecret then
		rRoll.bSecret = true;
	end
	-- ERROR: Secret rolls are not secret anymore, CoreRPG fault
	ActionsManager.performAction(draginfo, rActor, rRoll);
end


function getRoll(rActor, rAction)

	-- Global
	local rRoll = {};
	rRoll.sType = "skill";
	rRoll.aDice = Utilities.addEffectDice({ "d8", "d8" }, rAction.aDice, "b");
	rRoll.bSave = false;

	-- Skill
	local nodeSkillReal = nil;
	local nProf = 0;
	if rAction.bSpecialty then
		nodeSkillReal = rAction.nodeSkill.getChild("...");
		rRoll.sSkill = DB.getValue(nodeSkillReal, "name", "");
		rRoll.sSpeciality = DB.getValue(rAction.nodeSkill, "name", "");
		nProf = DB.getValue(nodeSkillReal, "prof", 0);
	else
		rRoll.sSkill = DB.getValue(rAction.nodeSkill, "name", "");
		rRoll.sSpeciality = "";
		nProf = DB.getValue(rAction.nodeSkill, "prof", 0);
	end

	-- Stat
	if rAction.sStat and rAction.sStat ~= "" then
		rRoll.sStat = DataCommon.abilities_translation[rAction.sStat:lower()];
	elseif rAction.stat and rAction.stat ~= "" then
		rRoll.sStat = DataCommon.abilities_translation[rAction.stat:lower()];
	else
		rRoll.sStat = DB.getValue(rAction.nodeSkill, "stat", "");
	end

	-- Add school to keywords if required
	if rAction.school and rAction.school ~= "" then
		if not rAction.keywords or rAction.keywords == "" then
			rAction.keywords = rAction.school;
		elseif not rAction.keywords:find(rAction.school:lower()) then
			rAction.keywords = rAction.keywords .. ", " .. rAction.school;
		end
	end

	-- Get bonus and advantage
	if rAction.bSpecialty then
		rRoll.nMod, rRoll.nAdv, rRoll.nEffects, rRoll.bSave, _, _, rRoll.aAddDice = ActorManager2.getCheck(rActor, rRoll.sStat, nodeSkillReal, rAction.nodeSkill, nil, nil, nil, rAction.keywords);
	elseif rAction.type and rAction.type == "save" then
		rRoll.nMod, rRoll.nAdv, rRoll.nEffects, _, _, _, rRoll.aAddDice = ActorManager2.getCheck(rActor, rRoll.sStat, rAction.nodeSkill, nil, nil, nil, rAction.keywords, true);
	else
		rRoll.nMod, rRoll.nAdv, rRoll.nEffects, _, _, _, rRoll.aAddDice = ActorManager2.getCheck(rActor, rRoll.sStat, rAction.nodeSkill, nil, nil, nil, rAction.keywords);
	end

	rRoll.nMod = rRoll.nMod + (rAction.nMod or rAction.mod or 0);
	rRoll.nAdv = rRoll.nAdv + (rAction.nAdv or 0);

	-- Other
	if rAction.bSecret or rAction.nSecret == 1 then
		rRoll.nSecret = 1;
		rRoll.bSecret = true;
	else
		rRoll.nSecret = 0;
	end
	rRoll.keywords = rAction.keywords;
	-- local nodeChar = ActorManager.getCreatureNode(rActor);

	-- Forced
	if rAction.nForced and rAction.nForced == 1 then
		rRoll.nForced = 1;
	end

	-- Save
	if rAction.type == "save" or rAction.type == "conc_save" then
		rRoll.bSave = true;
	end
	if rRoll.bSave then
		rRoll.sSaveEffect = rAction.sSaveEffect;
		rRoll.sOrigin = rAction.sOrigin;
		rRoll.nSave = 1;
	end

	-- Cast
	if rAction.type == "cast" then
		rRoll.nCast = 1;
		rRoll.nEffects = rAction.nEffects;
		rRoll.nLevel = rAction.nLevel;
		rRoll.nFinalLevel = rAction.nFinalLevel;
		rRoll.nOverchannel = rAction.nOverchannel;
		rRoll.sVigor = rAction.sVigor;
		rRoll.nIncreasedVigor = rAction.increased_vigor;
		rRoll.essence_cost = rAction.essence_cost;
		rRoll.essence_target = rAction.essence_target;
		rRoll.nConc = rAction.nConc;
		rRoll.nDur = rAction.nDur;
		rRoll.nActEffects = rAction.nActEffects;
		rRoll.spellName = rAction.spellName;
		rRoll.nHiddenSpell = rAction.nHiddenSpell;
		rRoll.nCostVitality = rAction.cost_vitality;
		rRoll.sSchool = rAction.school;
		rRoll.sTypeSpell = rAction.type_spell;
		rRoll.sElements = rAction.elements;
		rRoll.nIncreasedDif = rAction.increased_dif;
		rRoll.nDemanding = rAction.nDemanding;
		rRoll.sTechniques = table.concat(rAction.list_techniques or {}, ", ");
	end

	-- Tie
	if rAction.type == "tiespell" then
		rRoll.nTie = 1;
		rRoll.spellName = rAction.spellName;
		rRoll.nLevel = rAction.nLevel;
		rRoll.nDuration = rAction.nDuration or 1;
		rRoll.nDurationMult = rAction.nDurationMult or 1;
		rRoll.nID = rAction.nID;
		rRoll.nHiddenSpell = -rAction.nAdv;
	end

	-- Description
	if rRoll.bSave then
		rRoll.sDesc = Interface.getString("save"):upper() ..  "\n";
		rRoll.nSave = 1;
	elseif rAction.type == "cast" then
		rRoll.sDesc = Interface.getString("cast_spell"):upper() ..  "\n";
	elseif rAction.type == "tiespell" then
		rRoll.sDesc = Interface.getString("tie_spell"):upper() ..  "\n";
	else
		rRoll.sDesc = Interface.getString("check"):upper() .. "\n";
	end

	if rRoll.sStat ~= "" then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_groupstat") .. ": " ..  StringManager.capitalize(DataCommon.abilities_translation_inv[rRoll.sStat:lower()]);
	end

	if rRoll.sSkill ~= "" then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_groupskill") .. ": " ..  rRoll.sSkill;
	end
	if rAction.bSpecialty and not rRoll.bSave then
		rRoll.sDesc = rRoll.sDesc .. " (" .. rRoll.sSpeciality .. ")";
	end
	-- rRoll.sDesc = rRoll.sDesc .. DataCommon.mastery_strings[nProf];

	-- Add built-in bonus
	rRoll.modifier = rAction.mod or 0;
	if rRoll.modifier ~= 0 or (rAction.aDice and #rAction.aDice > 0) then
		local sString = StringManager.convertDiceToString(rAction.aDice, rRoll.modifier);
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_groupmod") .. ": " .. sString;
	end

	if rAction.nDif and (rAction.nDif ~= 0) then
		rRoll.nDif = rAction.nDif;
	end
	if rAction.sActions and rAction.sActions ~= "" then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. rAction.sActions;
	end

	return rRoll;
end

function modRoll(rSource, rTarget, rRoll)
	rRoll.aDice = Utilities.addEffectDice(rRoll.aDice, rRoll.aAddDice, "p");
	local nMod = (rRoll.nEffects or 0) - (rRoll.modifier or 0);

	-- If the check is forced by someone else, don't use desktop mods
	if rRoll.nForced and rRoll.nForced == 1 then
		ActionsManager2.encodeAdvantage(rRoll, true);
	else
		ActionsManager2.encodeAdvantage(rRoll);
		ActionsManager2.encodeDesktopMods(rRoll);
	end

	-- End description
	local nPen = ActorManager2.getPenalizer(nodeChar);
	if nPen and nPen ~= 0 then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("penal") .. ": " .. nPen;
	end

	if nmod ~= 0 or (rRoll.aAddDice and #rRoll.aAddDice > 0) then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_effect") .. ": " .. StringManager.convertDiceToString(rRoll.aAddDice, nMod);
	end

	if rRoll.nDif and rRoll.nDif ~= 0 and rRoll.nDif ~= "0" then
		rRoll.sDesc = rRoll.sDesc .. "\n- " .. Interface.getString("power_label_groupsave") .. ": " .. rRoll.nDif;
	end

end

function onRoll(rSource, rTarget, rRoll)
	ActionsManager2.decodeAdvantage(rRoll);

	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	local nTargetDC = tonumber(rRoll.nDif) or 9;
	local nTotal = ActionsManager.total(rRoll);
	local sResult = "";
	local sResultMessage = "";
	local nGrade = 0;

	rMessage.mode = "chat_skill";
	rMessage.font = "chatfont";
	-- Comm.deliverChatMessage(rMessage);
	MessageManager.onRollMessage(rMessage, rSource);

	if rRoll.nDif then

		if nTargetDC < -80 then
			sResult = "success";
			sResultMessage = Interface.getString("autosuccess");

		elseif nTargetDC > 80 then
			sResult = "failure";
			sResultMessage = Interface.getString("autofail");

		elseif (nTotal >= nTargetDC) and (nTotal < nTargetDC + DataCommon.critical_success) then
			nGrade = math.floor((nTotal - nTargetDC)/DataCommon.check_grade) + 1;
			sResult = "success";
			sResultMessage = "[" .. Interface.getString("success") .. Interface.getString("grade").. nGrade .. ")]";

		elseif nTotal >= nTargetDC + DataCommon.critical_success then
			nGrade = math.floor((nTotal - nTargetDC - DataCommon.critical_success)/DataCommon.check_grade) + 1;
			sResult = "critical success";
			sResultMessage = "[" .. Interface.getString("success_crit") .. Interface.getString("grade").. nGrade .. ")]";
			if (rRoll.nCast == '1') and rRoll.nDif then
				sResultMessage = sResultMessage .. " [" .. Interface.getString("cast_chat_irresistible") .. "]";
			end

		elseif (nTotal < nTargetDC) and (nTotal >= nTargetDC - DataCommon.critical_miss) then
			nGrade = math.floor((nTargetDC - nTotal -1)/DataCommon.check_grade) + 1;
			sResult = "failure";
			sResultMessage = "[" .. Interface.getString("miss_crit") .. Interface.getString("grade").. nGrade .. ")]";

		else
			nGrade = math.floor((nTargetDC - nTotal - DataCommon.critical_miss)/DataCommon.check_grade) + 1;
			sResult = "critical failure";
			sResultMessage = "[" .. Interface.getString("miss") .. Interface.getString("miss_crit").. nGrade .. ")]";
		end
	end

	-- Apply result
	if (rRoll.nSave == '1') and rRoll.nDif then
		-- Notify result
		notifySaveResult(rSource, rRoll.sOrigin, sResult, nGrade, rRoll.sSaveEffect, rRoll.nSecret == 1, nTargetDC);
		-- Save result
		local msgOOB = {};
		msgOOB.type = OOB_MSGTYPE_APPLYSAVE;
		msgOOB.sOriginCT = rRoll.sOrigin;
		msgOOB.sValue = sResult;
		msgOOB.nGrade = nGrade;
		msgOOB.sEffect = rRoll.sSaveEffect;
		local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
		msgOOB.sSourceType = sSourceType;
		msgOOB.sSourceNode = sSourceNode;
		Comm.deliverOOBMessage(msgOOB, "");
		
	elseif (rRoll.nCast == '1') and rRoll.nDif then
		notifyCastSpell(rSource, sResult, nGrade, rRoll, rRoll.nSecret == 1);
	elseif (rRoll.nTie == '1') and rRoll.nDif then
		notifyTieSpell(rSource, sResult, nGrade, rRoll, rRoll.nSecret == 1);
	else
		local rMessage2 = {secret=rMessage.secret, mode=DataCommon.result_to_mode[sResult], text=ActorManager.getDisplayName(rSource) .. ": " .. sResultMessage};
		Comm.deliverChatMessage(rMessage2);
	end


end

-----------------
-- NOTIFICATIONS
-----------------

aSetCastResult = {};
aSetCastSpellLevel = {};
aSetEffectiveSpellLevel = {};

function notifyCastSpell(rSource, sResult, nGrade, rRoll, bSecret)
	-- Get the data
	local nLevel = tonumber(rRoll.nLevel);
	local nEffects = tonumber(rRoll.nEffects);
	local bOverchannel = tonumber(rRoll.nOverchannel) > 0;
	local nEssence = tonumber(rRoll.essence_cost);
	local sTarget = rRoll.essence_target;
	local nConc = tonumber(rRoll.nConc);
	local nDur = tonumber(rRoll.nDur);
	local nActEffects = tonumber(rRoll.nActEffects);
	local spellName = rRoll.spellName;
	local nCostVitality = tonumber(rRoll.nCostVitality);
	local bDemanding = rRoll.nDemanding == "1";
	local aTargets = TargetingManager.getFullTargets(rSource);

	-- Start message
	local msgShort = {font = "msgfont", mode=DataCommon.result_to_mode[sResult]};
	local msgLong = {font = "msgfont", mode=DataCommon.result_to_mode[sResult]};
	msgShort.text = Interface.getString("cast_spell")  .. ": [" .. Interface.getString(DataCommon.result_to_stringres[sResult]) .. "]";
	msgLong.text = Interface.getString("cast_spell") .. " " .. spellName .. " : [" .. Interface.getString(DataCommon.result_to_stringres[sResult]) .. "]";

	-- Check if it is needed to roll in the critical fail table
	local bFatal = (sResult == "critical failure") or ((sResult == "failure") and bOverchannel);
	-- Aptitudes that modify this
	if bFatal then
		if bOverchannel then
			local nApt = ActorManager2.getAptitudeCategory(rSource, DataCommon.aptitude["overchannel"]);
			if nApt > 0 then
				nGrade = nGrade - nApt;
				local bCond1 = (sResult == "failure") and (nGrade < 1);
				local bCond2 = (sResult == "critical failure") and (ngrade < -2);
				bFatal = bCond1 or bCond2;
			end
		end
	end
	-- Fatal magic	
	if bFatal then
		-- Look forthe consequences
		if (sResult == "critical failure") and bOverchannel then
			nGrade = nGrade + 3;
		end
		nGrade = math.min(nGrade, #DataCommon.fatal_magic);
		local rFatal = DataCommon.fatal_magic[nGrade];

		if not rFatal then
			Debub.chat("ERROR: Fatal magic not found for grade " .. nGrade);
			return;
		end

		-- Text
		if rFatal.text ~= "" then
			msgLong.text = msgLong.text .. Interface.getString(rFatal.text);
		end

		-- Damages
		local rAction = {};
		rAction.label = Interface.getString("cast_chat_fatal_magic");
		rAction.sTargeting = "self";
		if #rFatal.damage > 0 then	
			rAction.sDamageTypes = DataCommon.dmgtypes_basic["energy"] .. "," .. DataCommon.dmgtypes_modifier["magic"] .. "," .. DataCommon.dmgtypes_modifier["precision"];
			rAction.aDice = rFatal.damage;

			ActionDamage.performRoll(nil, rSource, rAction);
		end
		-- rAction.nAvoidNotifyAction = 1;

		-- TODO: BUG: Unable to perform the rest of the actions. I don't know why
		if #rFatal.damagestat > 0 then			
			rAction.sDamageTypes = DataCommon.ability_gift_trans .. "," .. DataCommon.dmgtypes_modifier["magic"];
			rAction.aDice = rFatal.damagestat;

			ActionDamage.performRoll(nil, rSource, rAction);
		end

		if #rFatal.mentaltemp > 0 then			
			rAction.sDamageTypes = DataCommon.dmgtypes_special["madness"] .. "," .. DataCommon.dmgtypes_modifier["nonlethal"];
			rAction.aDice = rFatal.mentaltemp;

			ActionDamage.performRoll(nil, rSource, rAction);
		end

		if #rFatal.mentalperm > 0 then			
			rAction.sDamageTypes = DataCommon.dmgtypes_special["madness"];
			rAction.aDice = rFatal.mentalperm;

			ActionDamage.performRoll(nil, rSource, rAction);
		end
	end

	-- Costs
	if bDemanding or sResult ~= "failure" then
		local nExtraCost = tonumber(rRoll.nIncreasedVigor or "0");
		local nCost = 0;
		if rRoll.sVigor == DataCommon.standard then
			if nCostVitality == 1 then
				nCost = math.max(nLevel, 1);
			else
				nCost = ActorManager2.calculateSpellVigorCost(rSource, nLevel, rRoll.sStat, rRoll.sTypeSpell);
			end
		else
			local sTypeCost;
			nCost, sTypeCost = PowerManager.getActivityCost(rRoll.sVigor);
			if sTypeCost == "vitality" then
				nCostVitality = 1;
			elseif sTypeCost ~= "vigor" then
				nCost = 0;
			end
		end

		-- Spend Vigor / Vitality
		local rAction = {nPiercing = 0, nSelfTarget = 1, sNotification = Interface.getString("cast_chat_spellcost")};
		if nCost > 0 then
			if nCostVitality == 0 then
				rAction.sDamageTypes = DataCommon.dmgtypes_modifier["nonlethal"] .. "," .. DataCommon.dmgtypes_modifier["unavoidable"];
			else
				rAction.sDamageTypes =  DataCommon.dmgtypes_modifier["unavoidable"];
			end
			rAction.nTotal = nCost + nExtraCost;
			ActionDamage.notifyApplyDamage(nil, rSource, rAction);
		end

		-- Spend essence
		if nEssence > 0 and sTarget ~= "" then
			rAction.sDamageTypes =  DataCommon.dmgtypes_special["essence"];
			rAction.nTotal = nEssence;
			if sTarget ~= Interface.getString("essence_target_channeler") then
				for _,rTarget in pairs(aTargets) do
					ActionDamage.notifyApplyDamage(rSource, rTarget, rAction);
				end
			end
			if sTarget ~= Interface.getString("essence_target_target") then
				rAction.nSelfTarget = 0;
				ActionDamage.notifyApplyDamage(nil, rSource, rAction);
			end
		end
	end

	-- Add success effects on play
	if sResult == "success" or sResult == "critical success" then
		-- Spend global energy
		WindsManager.spendSpell();

		-- Store cast spell level
		local sSourceCT = ActorManager.getCreatureNodeName(rSource);
		aSetCastSpellLevel[sSourceCT] = tonumber(rRoll.nLevel);
		aSetEffectiveSpellLevel[sSourceCT] = tonumber(rRoll.nFinalLevel);

		-- Store result in case it is autotied
		if sResult == "critical success" then
			aSetCastResult[sSourceCT] = nGrade + 3;
		else
			aSetCastResult[sSourceCT] = nGrade;
		end

		-- Add modified effects to description
		if nEffects and nEffects > 0 then
			msgLong.text = msgLong.text .. Interface.getString("cast_chat_extra_effectsplus"):format(nEffects);
		elseif nEffects and nEffects < 0 then
			msgLong.text = msgLong.text .. Interface.getString("cast_chat_extra_effectsminus"):format(-nEffects);
		end

		-- Other characters may identify the spell
		local msgOOB = {};
		msgOOB.type = ActionCast.OOB_MSGTYPE_IDENTIFYSPELL;
		local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
		msgOOB.sSourceType = sSourceType;
		msgOOB.sSourceNode = sSourceNode;
		msgOOB.sStat = DataCommon.abilities_translation_inv[rRoll.sStat];
		msgOOB.nDif = 10 + nEffects + nLevel + tonumber(rRoll.nIncreasedDif);
		msgOOB.sName = spellName;
		msgOOB.sTechniques = rRoll.sTechniques;
		msgOOB.sLevel = tostring(nEffects + nLevel);
		msgOOB.sSchool = rRoll.sSchool;
		msgOOB.sMagic = StringManager.capitalize(msgOOB.sStat);
		msgOOB.sType = Interface.getString(rRoll.sTypeSpell);
		msgOOB.sElement = rRoll.sElements;
		if sResult == "critical success" then
			msgOOB.bCritSuccess = 1;
		end
		Comm.deliverOOBMessage(msgOOB, "");
	end

	-- Concentration
	if nConc == 1 then
		if nActEffects == 1 then
		else
			local rConc = {};
			rConc.nDur = nDur;
			rConc.nLevel = nLevel + nEffects;
			rConc.sStat = rRoll.sStat;
			rConc.sSkill = rRoll.sSkill;
			rConc.nID = math.random(DataCommon.conc_ID_max);
			rConc.nHiddenSpell = tonumber(rRoll.nHiddenSpell);

			local rNewEffect = {};
			rNewEffect.sName = spellName;
			rNewEffect.nDuration = 0;

			EffectManagerSS.addConcentrationEffect(rSource, {rSource}, rNewEffect, rConc, false)
		end
	end

	-- Add target names to description
	if #aTargets > 0 then
		msgLong.text = msgLong.text .. " -> ";
		msgShort.text = msgShort.text .. " -> ";
	end

	local bAdd = false;
	for _,rTarget in pairs(aTargets) do
		if bAdd then
			msgLong.text = msgLong.text .. ", ";
			msgShort.text = msgShort.text .. ", ";
		end
		bAdd = true;
		local node = ActorManager2.getActorAndNode(rTarget);
		local sName = DB.getValue(node, "name", "");
		msgLong.text = msgLong.text .. sName;
		msgShort.text = msgShort.text .. sName;
	end

	ActionsManager.outputResult(bSecret, rSource, nil, msgLong, msgShort);

end

function notifySaveResult(rSource, sOriginCT, sResult, nGrade, sEffect, bSecret, nDif)

	if not sOriginCT or sOriginCT == "" then
		return;
	end

	local rTarget = ActorManager.resolveActor(DB.findNode(sOriginCT));
	local msgShort = {font = "msgfont", mode=DataCommon.result_to_mode[sResult]};
	local msgLong = {font = "msgfont", mode=DataCommon.result_to_mode[sResult]};

	msgShort.text = Interface.getString("save") .. ": "
	if rTarget then
		msgShort.text = msgShort.text .. ActorManager.getDisplayName(rTarget) .. " -> ";
	end
	if rSource then
		msgShort.text = msgShort.text .. ActorManager.getDisplayName(rSource);
	end
	msgLong.text = msgShort.text .. " [Dif. " .. nDif .. "]";

	if sResult ~= "" then
		local sAux = " [" .. Interface.getString(DataCommon.result_to_stringres[sResult]);
		msgLong.text = msgLong.text .. sAux;
		msgShort.text = msgShort.text .. sAux;
		if nGrade ~= 0 then
			msgLong.text = msgLong.text .. Interface.getString("grade") .. nGrade .. ")";
		end
		msgLong.text = msgLong.text .. "]";
		msgShort.text = msgShort.text .. "]";
	end
	if sEffect ~= "" then
		msgShort.text = msgShort.text .. " [" .. Interface.getString(sEffect) .. "]";
		msgLong.text = msgLong.text .. " [" .. Interface.getString(sEffect) .. "]";
	end

	ActionsManager.outputResult(bSecret, rSource, rTarget, msgLong, msgShort);
end

function notifyTieSpell(rSource, sResult, nGrade, rRoll, bSecret)
	local bHiddenSpell = rRoll.nHiddenSpell == "1";
	local msgShort = {font = "msgfont", mode=DataCommon.result_to_mode[sResult]};
	local msgLong = {font = "msgfont", mode=DataCommon.result_to_mode[sResult]};
	msgShort.text = Interface.getString("tie_spell") .. ": ";
	msgLong.text = Interface.getString("tie_spell") .. " (" .. rRoll.spellName .. "): ";


	if sResult == "critical failure" then
		-- Remove the effects
		msgShort.text = msgShort.text .. Interface.getString("spell_ended");
		msgLong.text = msgLong.text .. Interface.getString("spell_ended");
		EffectManagerSS.removeAllEffectsByNameAndOwner(rSource, tonumber(rRoll.nID), true, true);

	elseif sResult == "failure" then
		-- Nothing happens
		msgShort.text = msgShort.text .. Interface.getString("spell_not_tied");
		msgLong.text = msgLong.text .. Interface.getString("spell_not_tied");

	elseif sResult == "success" or sResult == "critical success" then
		-- Calculate the duration
		if sResult == "critical success" then
			nGrade = nGrade + DataCommon.critical_success / DataCommon.check_grade;
		end
		local new_dur = nGrade * tonumber(rRoll.nDuration) * tonumber(rRoll.nDurationMult);
		msgShort.text = msgShort.text .. Interface.getString("spell_tied");
		msgLong.text = msgLong.text .. Interface.getString("spell_tied");
		if new_dur > 0 then
			msgLong.text = msgLong.text .. string.format(Interface.getString("spell_duration_message"), new_dur);
		end

		-- Tie the spell
		local nDif = 10 + tonumber(rRoll.nLevel) + 3 * (tonumber(rRoll.nDurationMult) - 1);
		EffectManagerSS.tieSpellEffects(rSource, tonumber(rRoll.nID), new_dur, nDif, bHiddenSpell);
	end
	if bHiddenSpell then
		msgShort = nil;
	end

	ActionsManager.outputResult(bSecret, rSource, nil, msgLong, msgShort);

end

function getCastStatus(rSource)
	local sSourceCT = ActorManager.getCreatureNodeName(rSource);
	if not aSetCastResult[sSourceCT] then
		return 0;
	end
	return aSetCastResult[sSourceCT];
end

function getCastSpellLevel(rSource)
	local sSourceCT = ActorManager.getCreatureNodeName(rSource);
	if not aSetCastSpellLevel[sSourceCT] then
		return 0;
	end
	return aSetCastSpellLevel[sSourceCT];
end

function getEffectiveSpellLevel(rSource)
	local sSourceCT = ActorManager.getCreatureNodeName(rSource);
	if not aSetEffectiveSpellLevel[sSourceCT] then
		return 0;
	end
	return aSetEffectiveSpellLevel[sSourceCT];
end

--------------
-- SAVE STATUS
--------------

aSaveStatus = {};

function setSaveStatus(rSource, sOriginCT, sValue, nGrade, sEffect)
	if not sOriginCT or sOriginCT == "" then
		return;
	end
	local sTargetCT = "";
	if rSource then
		sTargetCT = ActorManager.getCTNodeName(rSource);
	end

	if not aSaveStatus[sOriginCT] then
		aSaveStatus[sOriginCT] = {};
	else
		for k, v in pairs(aSaveStatus[sOriginCT]) do
			if v.target == sTargetCT and v.effect == sEffect then
				aSaveStatus[sOriginCT][k] = nil;
			end
		end
	end
	table.insert(aSaveStatus[sOriginCT], {target=sTargetCT, value=sValue, grade=nGrade, effect=sEffect});
end

function clearSaveStatus(rSource, rOrigin)
	local sSourceCT = ActorManager.getCTNodeName(rSource);
	local sOriginCT = "";
	if type(rOrigin) == string then
		sOriginCT = rOrigin;
	else
		sOriginCT = ActorManager.getCTNodeName(rOrigin);
	end
	if sOriginCT and sOriginCT ~= "" and aSaveStatus[sOriginCT] then
		if sSourceCT and sSourceCT ~= "" then
			for k, v in pairs(aSaveStatus[sOriginCT]) do
				if v.target == sSourceCT then
					aSaveStatus[sOriginCT][k] = nil;
				end
			end
		else
			if aSaveStatus[sOriginCT] then
				aSaveStatus[sOriginCT] = nil;
			end
		end
	end
end

function getSaveStatus(rSource, rOrigin)
	local sOriginCT = "";
	if type(rOrigin) == string then
		sOriginCT = rOrigin;
	else
		sOriginCT = ActorManager.getCTNodeName(rOrigin);
	end
	if sOriginCT == "" then
		return "failure", 1, "";
	end

	local sTargetCT = "";
	if rSource then
		sTargetCT = ActorManager.getCTNodeName(rSource);
	end

	if not aSaveStatus[sOriginCT] then
		return "failure", 1, "";
	end

	for k,v in ipairs(aSaveStatus[sOriginCT]) do
		if v.target == sTargetCT then
			local sValue = v.value;
			local nGrade = v.grade;
			local sEffect = v.effect;
			table.remove(aSaveStatus[sOriginCT], k);
			return sValue, nGrade, sEffect;
		end
	end

	return "failure", 1, "";
end

