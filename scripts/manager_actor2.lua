--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

-------------
-- GET ACTORS
-------------

function isPC(nodeActor)
	if not nodeActor then
		return false;
	end
	_, nodeActor = ActorManager.getTypeAndNode(nodeActor);

	local sPath = nodeActor.getPath();
	if sPath and sPath:sub(1,9) == "charsheet" then
		return true;
	elseif hasAptitude(nodeActor, DataCommon.aptitude["character"]) then
		return true;
	else
		nodeActor = getNodeFromCT(nodeActor);
		sPath = nodeActor.getPath();
		if sPath and sPath:sub(1,9) == "charsheet" then
			return true;
		end
	end
	return false;
end

function getNodeFromCT(nodeCT)
	local rActor = ActorManager.resolveActor(nodeCT);
	local _, nodeActor = ActorManager.getTypeAndNode(rActor);
	return nodeActor;
end

function getActorAndNode(rActor)
	local nodeActor, _;
	if type(rActor) == "table" then
		_, nodeActor = ActorManager.getTypeAndNode(rActor);
	else
		nodeActor = rActor;
		rActor = ActorManager.resolveActor(nodeActor);
	end
	return nodeActor, rActor;
end

function getCTNode(rActor)
	_, rActor = getActorAndNode(rActor);
	return ActorManager.getCTNode(rActor);
end

function getCharType(rActor)
	local nodeActor, rActor = getActorAndNode(rActor);
	local sType = DB.getValue(nodeActor, "type", "");
	if Utilities.inArray(DataCommon.creaturetype, sType) then
		return sType;
	else
		return DataCommon.creaturedefaulttype;
	end
end

function findCharByName(sName)
	local nodeList = DB.findNode("charsheet");
	for _,node in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(node, "name", "");
		if sCurrent == sName then
			return node;
		end
	end

	nodeList = DB.findNode("combattracker.list");
	for _,node in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(node, "name", "");
		if sCurrent == sName then
			return node;
		end
	end
end

function getCurrentCharacter(bReturnActor)
	-- Get node
	local nodeChar;
	if User.isHost() then
		local nodeChar =  CombatManager.getActiveCT();
	else
		local sUser = User.getUsername()
		local nodeList = DB.findNode("charsheet");
		for _,v in pairs(nodeList.getChildren()) do
			local sOwner = v.getOwner();
			if sOwner == sUser then
				nodeChar = v;
				break;
			end
		end
	end
	-- Get actor if required
	if bReturnActor then
		return getActorAndNode(nodeChar);
	end

	return nodeChar;
end


--------------------
-- STATS AND SKILLS
--------------------


function getPenalizer(rActor, sType)
	-- Get node and actor
	nodeActor, rActor = getActorAndNode(rActor);

	-- Get the base value
	local nPain = DB.getValue(nodeActor, "health.penal.vit.base", 0) + DB.getValue(nodeActor, "health.penal.vit.misc", 0);
	local nTired = DB.getValue(nodeActor, "health.penal.vigor.base", 0) + DB.getValue(nodeActor, "health.penal.vigor.misc", 0);
	local nExtra = DB.getValue(nodeActor, "health.penal.extra.misc", 0);

	-- Get status penalizers
	local nStatePain = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.conditions_number["Tired"]}, true, {});
	local nStateTired = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.conditions_number["Wounded"]}, true, {});
	local nStateExtra = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["PEN"]}, true);
	nStateExtra = math.max(nStateExtra - nStatePain - nStateTired, 0);

	-- Add sensitivity effect
	local _, _, _, nMult, _ = getSensitivity(rActor, DataCommon.pain);
	nPain = (nPain + nStatePain) * nMult;
	 _, _, _, nMult, _ = getSensitivity(rActor, DataCommon.tiredness);
	nTired = (nTired + nStateTired) * nMult;
	local nTotal = nPain + nTired + nExtra + nStateExtra;
	nPain = math.floor(nPain);
	nTired = math.floor(nTired);
	nTotal = math.floor(nTotal);
	local nTotal = nPain + nTired + nExtra + nStateExtra;
	nPain = math.floor(nPain);
	nTired = math.floor(nTired);
	nTotal = math.floor(nTotal);

	-- End result
	if not sType then
		return nTotal;
	elseif sType == "pain" then
		return nPain
	elseif sType == "tired" then
		return nTired;
	else
		return nTotal;
	end	
end


function getInit(rActor, bFromCT)
	-- Get node and actor
	if bFromCT then
		rActor = ActorManager.resolveActor(rActor);
	end
	nodeActor, rActor = getActorAndNode(rActor);

	-- Get the base value
	local _, _, nValue = CharManager.calculateInitiative(nodeActor, true);

	-- Get the state modifiers
	local aDice, nState = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["INIT"]}, false);
	local nPen = getPenalizer(rActor);
	nState = nState + nPen;

	-- Get the state advantage
	local nAdv1 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVINIT"]}, true);
	local nAdv2 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVCHECK"]}, true, {DataCommon.abilities_translation["agility"]});
	local nAdv = nAdv1 + nAdv2;

	-- Aptitudes
	if hasAptitude(rActor, DataCommon.aptitude["slow"]) then
		nAdv = nAdv - 1;
	elseif hasAptitude(rActor, DataCommon.aptitude["very_slow"]) then
		nAdv = nAdv - 1;
		nState = nState - 3;
	end
	-- (Already taken into account in initiative.total)
	-- nValue = nValue + getAptitudeCategory(rActor, DataCommon.aptitude["improved_ini"]); 

	-- States
	local tableEffects = {DataCommon.conditions["Entangled"], DataCommon.conditions["Stunned"], DataCommon.conditions["Confused"], DataCommon.conditions["Weakened"], DataCommon.conditions["Fascinated"], DataCommon.conditions["Stupified"]};
	local nEff = EffectManagerSS.numberOfEffectConditions(rActor, tableEffects);
	nAdv = nAdv - nEff;

	tableEffects = {DataCommon.conditions["Asleep"], DataCommon.conditions["Unconscious"], DataCommon.conditions["Trance"]};
	nEff = EffectManagerSS.numberOfEffectConditions(rActor, tableEffects);
	if nEff > 0 then
		nValue = 0;
		nState = -99;
		nAdv = 0;
	end

	-- In case mount has lower values, use them	
	if DB.getValue(nodeActor, "is_mounted", 0) > 0 then
		local nodeMount = nodeActor.getChild("mount_npc");
		if nodeMount then
			local nAux, nAdvMount, nStateMount, aDiceMount = getInit(nodeMount);
			local nAux, nAdvMount, nStateMount, aDiceMount = getInit(nodeMount);
			if nAux + nAdvMount < nValue + nState + nAdv then
				nValue = nAux - nStateMount;
				nAdv = nAdvMount;
				nState = nStateMount;
				aDice = aDiceMount;
				aDice = aDiceMount;
			end
		end
	end

	-- Return
	return nValue + nState, nAdv, nState, aDice;

end

function getLevel(rActor, nMastery, nLimit)
	if not rActor then
		return 0, false;
	end
	-- Get node and actor
	nodeActor, rActor = getActorAndNode(rActor);

	-- Get the sheet value
	local nValue = DB.getValue(nodeActor, "level", 0);

	-- Get the state modifier
	local nValueEff, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["LEVEL"]}, true);
	local nValueEff2, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.conditions_number["Drained"]}, true);
	nValue = math.max(nValue + nValueEff - nValueEff2, 0);
	local bLimited = false;

	-- Modified by mastery and limit
	if nMastery then
		nValue = math.ceil(nValue * nMastery / 4);
		if nMastery > 1 then
			nValue = nValue + 1;
		end
		if nLimit then
			bLimited = nValue > nLimit;
			if bLimited then
				nValue = nLimit;
			end
		end
	end

	return nValue, bLimited;

end

function getSkillNode(rActor, sSkill)
	local nodeActor, rActor = getActorAndNode(rActor);
	sSkill = sSkill:lower();

	for _, v in pairs(DB.getChildren(nodeActor, "skilllist_basic")) do
		local sName = DB.getValue(v, "name", "");
		if sName ~= "" and sName:lower() == sSkill then
			return v, false, nil;
		end
		for _, vv in pairs(DB.getChildren(v, "specialty")) do
			local sNameEsp = DB.getValue(vv, "name", "");
			if sNameEsp ~= "" and sNameEsp:lower() == sSkill then
				return v, true, vv;
			end
		end
	end

	for _, v in pairs(DB.getChildren(nodeActor, "skilllist_learned")) do
		local sName = DB.getValue(v, "name", "");
		if sName ~= "" and sName:lower() == sSkill then
			return v, false, nil;
		end
		for _, vv in pairs(DB.getChildren(v, "specialty")) do
			local sNameEsp = DB.getValue(vv, "name", "");
			if sNameEsp ~= "" and sNameEsp:lower() == sSkill then
				return v, true, vv;
			end
		end
	end

	for _, v in pairs(DB.getChildren(nodeActor, "skilllist_specific")) do
		local sName = DB.getValue(v, "name", "");
		if sName ~= "" and sName:lower() == sSkill then
			return v, false, nil;
		end
		for _, vv in pairs(DB.getChildren(v, "specialty")) do
			local sNameEsp = DB.getValue(vv, "name", "");
			if sNameEsp ~= "" and sNameEsp:lower() == sSkill then
				return v, true, vv;
			end
		end
	end

	for _, v in pairs(DB.getChildren(nodeActor, "skilllist_weaponary")) do
		local sName = DB.getValue(v, "name", "");
		if sName ~= "" and sName:lower() == sSkill then
			return v, false, nil;
		end
		for _, vv in pairs(DB.getChildren(v, "specialty")) do
			local sNameEsp = DB.getValue(vv, "name", "");
			if sNameEsp ~= "" and sNameEsp:lower() == sSkill then
				return v, true, vv;
			end
		end
	end

	for _, v in pairs(DB.getChildren(nodeActor, "skilllist_magic")) do
		local sName = DB.getValue(v, "name", "");
		if sName ~= "" and sName:lower() == sSkill then
			return v, false, nil;
		end
		for _, vv in pairs(DB.getChildren(v, "specialty")) do
			local sNameEsp = DB.getValue(vv, "name", "");
			if sNameEsp ~= "" and sNameEsp:lower() == sSkill then
				return v, true, vv;
			end
		end
	end

	return nil, nil, nil;
end


function getSaveNode(rActor, sSkill)
	--  Get the skill
	local nodeSkill = getSkillNode(rActor, sSkill);
	-- Get the speciality (if exists)
	if nodeSkill then
		for _, vv in pairs(nodeSkill.createChild("specialty").getChildren()) do
			local sNameEsp = DB.getValue(vv, "name", ""):lower();
			if sNameEsp ~= "" and sNameEsp == DataCommon.save:lower() then
				return nodeSkill, true, vv;
			end
		end
	end
	return nodeSkill, false, nil;
end


function getSkillTotal(nodeSkill, nodeSpecialty, isSideHanded, isSave)
	if not nodeSkill then
		return 0, -1, 0;
	end
	local nodeChar = nodeSkill.getChild("...");
	local nMast = DB.getValue(nodeSkill, "prof", 0);
	local nLim = DB.getValue(nodeSkill, "limited", 0);
	local nLimit = 99;
	local sNameSkill = DB.getValue(nodeSkill, "name", ""):lower();
	local sNameSpecialty = "";
	local nMisc = 0;
	local nValue = 0;
	local bLimited;
	local aDice = {};

	-- Get limits
	if nLim == 1 then
		nLimit = getLimit(nodeChar, "skill");
	elseif nLim == 2 then
		nLimit = getLimit(nodeChar, "skillmagic");
	elseif nLim == 3 then
		nLimit = getLimit(nodeChar, "skillnatural");
	elseif nLim == 4 then
		nLimit = getLimit(nodeChar, "skillhelmet");
	end
	if isSideHanded then
		local cond1 = sNameSkill == Interface.getString("skill_value_shields"):lower();
		local cond2 = sNameSkill == Interface.getString("skill_value_shields_exotic"):lower();
		if not (cond1 or cond2) then
			local nodeSide = getSkillNode(nodeChar, Interface.getString("skill_value_lefthanded"));
			local nSide = DB.getValue(nodeSide, "prof", 0);
			nMast = math.min(nMast, nSide);
		end
	end
	nValue, bLimited = getLevel(nodeChar, nMast, nLimit);

	-- Get sheet misc modifier
	if nodeSpecialty then
		nMisc = DB.getValue(nodeSpecialty, "misc", 0);
		sNameSpecialty = DB.getValue(nodeSpecialty, "name", ""):lower();
		if sNameSpecialty == DataCommon.save then
			isSave = true;
		end
	else
		nMisc = DB.getValue(nodeSkill, "misc", 0);
	end
	nValue = nValue + nMisc;

	-- State modifiers
	local rActor = ActorManager.resolveActor(nodeChar);
	local aDice2, nState = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CHECK"]}, false, {sNameSkill});
	aDice = Utilities.addEffectDice(aDice, aDice2);
	if nodeSpecialty and sNameSkill ~= sNameSpecialty then
		local aDice2, nState2 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CHECK"]}, false, {sNameSpecialty});
		nState = nState + nState2;
		aDice = Utilities.addEffectDice(aDice, aDice2);
	end
	if isSave then
		local aDice2, nState2 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["SAVE"]}, false, {sNameSkill, sNameSpecialty});
		nState = nState + nState2;
		aDice = Utilities.addEffectDice(aDice, aDice2);
	end

	-- Skill Advantage
	local nAdv = 0;
	if nMast == 0 then
		nAdv = -1;
	end
	local nAdv2, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVCHECK"]}, true, {sNameSkill});
	nAdv = nAdv + nAdv2;

	-- Specialty advantage
	if nodeSpecialty then
		nAdv2, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVCHECK"]}, true, {sNameSpecialty});
		nAdv = nAdv + nAdv2;
		if isSave then
			nAdv2, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVSAVE"]}, true, {sNameSkill, sNameSpecialty});
			nAdv = nAdv + nAdv2;
		end
	end

	-- Save aptitudes
	if isSave then
		local sApt = DataCommon.aptitudes_saves[sNameSkill];
		if sApt then
			local nApt = getAptitudeCategory(nodeChar, DataCommon.aptitude[sApt]);
			if nApt > 0 then
				nValue = nValue + 2;
			end
			if nApt > 1 then
				nAdv = nAdv + 1;
			end
		end
	end

	-- Special case: concentration
	if sNameSkill == Interface.getString("skill_value_concentration") then
		if EffectManagerSS.numberOfEffectConditions(rActor, DataCommon.conditions_end_concentration) then
			return 99, 0, -99;
		end
		local aCondConc = {DataCommon.conditions["Terrified"], DataCommon.conditions["Stunned"], DataCommon.conditions["Confused"], DataCommon.conditions["Trance"], DataCommon.conditions["Maddened"]};
		if EffectManagerSS.numberOfEffectConditions(rActor, aCondConc) then
			nAdv = nAdv - 1;
		end
		local aIDs, _ = EffectManagerSS.findConcentrationEffects(rActor);
		nState = nState - (#aIDs - 1) * DataCommon.conc_multiple_increase_DC;
	end

	-- Special case: Stealth
	if sNameSkill == Interface.getString("skill_value_stealth"):lower() and isMetallic(rActor) then
		nAdv = nAdv - 1;
	end

	-- States with certain skills
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Blinded"]) then
		if sNameSpecialty == Interface.getString("skill_value_alert_sight") then
			return -99, 0, -99;
		elseif sNameSkill == Interface.getString("skill_value_alert") then
			nAdv = nAdv - 1;
		end
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Deafened"]) then
		if sNameSpecialty == Interface.getString("skill_value_alert_hear") then
			return -99, 0, -99;
		elseif sNameSkill == Interface.getString("skill_value_alert") then
			nAdv = nAdv - 1;
		end
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Terrified"]) then
		if sNameSkill == Interface.getString("skill_value_courage") then
			nAdv = nAdv - 1;
		end
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Distracted"]) then
		if sNameSkill == Interface.getString("skill_value_alert") then
			nAdv = nAdv - 1;
		end
	end
	if sNameSkill == Interface.getString("skill_value_courage") then
		local nMorale, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["MORAL"]}, true);
		nState = nState + nMorale;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Dazzled"]) then
		if sNameSpecialty == Interface.getString("skill_value_alert_sight") then
			nState = nState - 3;
		end
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Unaware"]) then
		if sNameSpecialty == Interface.getString("skill_value_reflexes") then
			nAdv = nAdv - 1;
		end
	end

	-- Size effects
	if isSave and (sNameSkill == Interface.getString("skill_value_dodge") or sNameSkill == Interface.getString("skill_value_reflexes")) then
		local sSize = DB.getValue(nodeChar, "size", DataCommon.size_default):lower();
		local nSize = 0
		if Utilities.inArray(DataCommon.sizelabels, sSize) then
			nSize = DataCommon.size_modifier[sSize];
		else
			Debug.chat("Wrong size label", sSize);
		end
			nState = nState + nSize;
	end

	return nValue, nAdv, nState, isSave, bLimited, aDice;
end


function getCurrentAbilityValue(rActor, sAbility, bNotUseStates)
	-- Safety
	if not rActor or not sAbility or sAbility == "" then
		return 0, 0;
	end
	sAbility = sAbility:lower();
	-- Get node and actor
	nodeActor, rActor = getActorAndNode(rActor);
	local nBase = 0;
	local nDamage = 0;
	local nState = 0;
	local nMagicProt = 0;
	local nLimit = 99;
	local bGift = Utilities.inArray(DataCommon.abilities_gift, sAbility);

	-- Luck
	if sAbility == DataCommon.ability_luck or sAbility == DataCommon.ability_luck_trans then
		nBase = DB.getValue(nodeActor, "luck.current", 0);

	-- Spirit
	elseif sAbility == DataCommon.ability_spirit or sAbility == DataCommon.ability_spirit_trans then
		nBase = DB.getValue(nodeActor, "health.spirit.max", 0);
		nDamage = DB.getValue(nodeActor, "health.spirit.damage", 0);

	-- Rest of abilities
	else
		-- Get base value and damage
		sAbility = DataCommon.abilities_translation[sAbility];
		if sAbility then
			nBase = DB.getValue(nodeActor, "abilities." .. sAbility .. ".base", 0);
			nDamage = DB.getValue(nodeActor, "abilities." .. sAbility .. ".damage", 0);
		end
		

		-- Get state modifiers
		if not bNotUseStates then
			local sState = DataCommon.ability_ltos[sAbility];
			nState, _ = EffectManagerSS.getEffectsBonus(rActor, {sState}, true, {});
		end

		-- Get limit if required
		nLimit = getLimit(nodeActor, sAbility);

		-- Rest magic protection for gift abilities
		if bGift then
			nMagicProt = getMagicProt(rActor);
			nState = nState - nMagicProt;
		end
	end

	-- Tooltip
	local sTooltip = "";
	if nDamage > 0 then
		sTooltip = "\n- " .. Interface.getString("modifier_label_damage") .. ": " .. tostring(nDamage);
	end
	if bGift and nMagicProt > 0 then
		sTooltip = sTooltip .. "\n- " .. Interface.getString("char_label_magic_protection") .. ": " .. tostring(-nMagicProt);
		if nState + nMagicProt ~= 0 then
			sTooltip = sTooltip .. "\n- " .. Interface.getString("char_label_sensitivity_states") .. ": " .. tostring(nState + nMagicProt);
		end
	else		
		if nState ~= 0 then
			sTooltip = sTooltip .. "\n- " .. Interface.getString("char_label_sensitivity_states") .. ": " .. tostring(nState);
		end
	end

	-- Total
	local nStat = math.max(nBase - nDamage + nState, 0);
	local bLimited = nStat > nLimit;
	if bLimited then
		nStat = nLimit;
		sTooltip = sTooltip .. "\n- " .. Interface.getString("item_label_limit") .. ": " .. tostring(nLimit);
	end
	return nStat, nState, sTooltip, bLimited;
end

function getLimit(nodeActor, sAbility)
	-- Act only with relevant stats
	if not Utilities.inArray(DataCommon.ability_limited, sAbility) then
		return 99;
	end
	
	-- Get info
	local nLimit = 99;
	if Utilities.inArray(DataCommon.ability_limited_always, sAbility) then
		nLimit = DB.getValue(nodeActor, "encumbrance.limits.armor", 99);
	elseif Utilities.inArray(DataCommon.ability_limited_magicarmor, sAbility) then
		nLimit = DB.getValue(nodeActor, "encumbrance.limits.magic", 99);
	elseif Utilities.inArray(DataCommon.ability_limited_helmet, sAbility) then
		nLimit = DB.getValue(nodeActor, "encumbrance.limits.helmet", 99);
	elseif Utilities.inArray(DataCommon.ability_limited_nonatural, sAbility) then
		nLimit = DB.getValue(nodeActor, "encumbrance.limits.nonatural", 99);
	end
	if Utilities.inArray(DataCommon.ability_limited_weight, sAbility) then
		nLimit = math.min(nLimit, DB.getValue(nodeActor, "encumbrance.limits.weight", 99));
	end
	-- Adapt limit if required
	local nShield = 0;
	if Utilities.inArray(DataCommon.ability_limited_shield, sAbility) then
		nShield = DB.getValue(nodeActor, "encumbrance.limits.shield", 99);
	elseif Utilities.inArray(DataCommon.ability_limited_shield_nonatural, sAbility) then
		nShield = DB.getValue(nodeActor, "encumbrance.limits.shield_nonatural", 99);
	end
	if nShield < 0 then
		if nLimit > 90 then
			nLimit = 8 + nShield;
		else
			nLimit = nLimit + nShield;
		end
	end
	return nLimit;
end

function getAbility(rActor, sAbility, use_check_mod, isSave)
	-- Safety
	if not rActor or not sAbility then
		return 0, 0, 0;
	end
	-- Get node and actor
	nodeActor, rActor = getActorAndNode(rActor);

	-- Get base value
	sAbility = sAbility:lower()
	local sTranslated;
	sAbility, sTranslated = DataCommon.getRulesetAndTranslatedAbility(sAbility);
	local nValue = 0;
	local nAdv = 0;
	local nState = 0;
	local aDice = {};

	-- If gift, find the highest one
	if Utilities.inArray(DataCommon.aGift, sAbility) then
		for _, v in pairs(DataCommon.abilities_gift) do
			local nCValue, nCAdv, nCState = getAbility(rActor, v, use_check_mod, isSave);
			local nMod = nCValue + advantage2passiveBonus(nCAdv);
			if (nMod > nValue) or (nMod == nValue and nCAdv > nAdv) then
				nValue = nCValue - advantage2passiveBonus(nCAdv);
				nAdv = nCAdv;
				nState = nCState;
			end
		end

	-- Other special atributes
	elseif sAbility == DataCommon.ability_luck then
		nValue = DB.getValue(nodeActor, "luck.current", 0);
		if use_check_mod then
			aDice, nState = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CHECK"]}, false, {sAbility, sTranslated});
		end
		nAdv = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVCHECK"]}, true, {sAbility, sTranslated});
		if isSave then
			local aAddDice, nState2 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["SAVE"]}, false, {sAbility, sTranslated});
			nState = nState + nState2;
			aDice = Utilities.addEffectDice(aDice, aAddDice);
		end

	elseif sAbility == DataCommon.ability_spirit then
		nValue = DB.getValue(nodeActor, "health.spirit.max", 0) - DB.getValue(nodeActor, "health.spirit.damage", 0);
		if use_check_mod then
			aDice, nState = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CHECK"]}, false, {sAbility, sTranslated});
		end
		nAdv = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVCHECK"]}, true, {sAbility, sTranslated});
		if isSave then
			local aAddDice, nState2 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["SAVE"]}, false, {sAbility, sTranslated});
			nState = nState + nState2;
			aDice = Utilities.addEffectDice(aDice, aAddDice);
		end

	-- Default mode
	else
		nValue, nState, _, bLimited = getCurrentAbilityValue(rActor, sAbility);

		-- Get state modifiers
		if use_check_mod then
			local aAddDice, nState2 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CHECK"]}, false, {sAbility, sTranslated});
			nState = nState + nState2;
			aDice = aAddDice;
			if isSave then
				local aAddDice, nState2 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["SAVE"]}, false, {sAbility, sTranslated});
				nState = nState + nState2;
				aDice = Utilities.addEffectDice(aDice, aAddDice);
			end
		end

		-- Get advantage value
		local nAdv1 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVCHECK"]}, true, {sAbility, sTranslated});
		nAdv = nAdv1 + nAdv;
		if isSave then
			local nAdv1 = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ADVSAVE"]}, true, {sAbility, sTranslated});
			nAdv = nAdv1 + nAdv;
		end

		-- Specific states
		if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Unconscious"]) then
			if sAbility == "perception" then
				return -99, 0, -99
			end
		end
		if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Entangled"]) then
			if not isSave or sAbility == "agility" then
				nAdv = nAdv - 1;
			end
		end
		if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Weakened"]) then
			if sAbility == "strength" or sAbility == "constitution" or sAbility == "agility" or sAbility == "dexterity" then
				nAdv = nAdv - 1;
			end
		end
		if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Blinded"]) or (EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Asleep"]) and not hasAptitude(rActor, DataCommon.aptitude["light_sleep"])) then
			if sAbility == "perception" then
				nAdv = nAdv - 1;
			end
		elseif EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Dazzled"]) then
			if sAbility == "perception" then
				nState = nState - 3;
			end
		end
		if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Fascinated"]) then
			if sAbility == "perception" then
				nAdv = nAdv - 1;
			end
		end
		if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Stupified"]) then
			if sAbility == "intelligence" or sAbility == "willpower" or sAbility == "charisma" then
				nAdv = nAdv - 1;
			end
		end

		if isSave then
			if EffectManagerSS.hasAnyEffectCondition(rActor, {DataCommon.conditions["Unconscious"], DataCommon.conditions["Asleep"]}) then
				if sAbility == "strength" or sAbility == "agility" or sAbility == "dexterity" then
					return -99, 0, -99
				elseif sAbility == "willpower" then
					nAdv = nAdv - 1;
				end
			end
			if EffectManagerSS.hasAnyEffectCondition(rActor, {DataCommon.conditions["Helpless"], DataCommon.conditions["Trance"]}) then
				if "strength" or sAbility == "agility" or sAbility == "dexterity" then
					return -99, 0, -99
				end
			end
			if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Inmobilized"]) then
				if sAbility == "agility" or sAbility == "dexterity" then
					nAdv = nAdv - 1;
				end
			end
			local nMod, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["COVER"]}, true, {"", "ranged"});
			if (sAbility == "dexterity") and (nMod > 0) then
				nState = nState + nMod;
			end
		end
	end

	-- Return
	return nValue, nAdv, nState, bLimited, aDice;
end

function getCheck(rActor, sAtribute, nodeSkill, nodeSpeciality, bSideHand, bPassive, keywords, bForceSave)
	-- If everithing is empty, return 0
	if sAtribute == "" then
		local bCond1 = type(nodeSkill) == "string" and nodeSkill == "";
		local bCond2 = type(nodeSpeciality) == "string" and nodeSpeciality == "";
		if bCond1 and bCond2 then
			return 0, 0, 0
		end
	end

	-- Get the atribute current value and advantage
	local nValue, nAdv, nState, bLimited, aDice = getAbility(rActor, sAtribute, true, bForceSave);
	if nValue == -99 then
		return -99, 0, -99;
	end
	local isSave = bForceSave;
	local bUseSkill = true;
	nValue = nValue - nState

	-- Get penalizer
	local nPen = getPenalizer(rActor);

	-- If not skill node or string, no skill is used
	if not nodeSkill and not nodeSpecialty then
		bUseSkill = false;

	-- If save, find the right nodes
	elseif nodeSpeciality and nodeSpeciality == 'save' then
		nodeSkill, _, nodeSpeciality = getSaveNode(rActor, nodeSkill);
		isSave = true

	-- If skill is string instead of node, find it
	elseif nodeSpeciality and type(nodeSpeciality) == 'string' then
		nodeSkill, _, nodeSpeciality = getSkillNode(rActor, nodeSpeciality);
	elseif type(nodeSkill) == 'string' then
		nodeSkill, _, nodeSpeciality = getSkillNode(rActor, nodeSkill);
	end

	-- Get the skill current value and advantage
	local bLimitedSkill;
	if bUseSkill then
		local nProf, nSkillAdv, nSkillState, isSave, bLimitedSkill, aAddDice = getSkillTotal(nodeSkill, nodeSpeciality, bSideHand, isSave);
		nValue = nValue + nProf;
		nAdv = nAdv + nSkillAdv;
		nState = nState + nSkillState + nPen;
		aDice = Utilities.addEffectDice(aDice, aAddDice);
		-- Specific features: electric
		local bCond = isSave and sAtribute == "agility" and isMetallic(rActor);
		local nFind = 0;
		if keywords and keywords ~= "" then
			nFind, _ = keywords:lower():find(DataCommon.magic_keywords["electric"])
		end
		if bCond and nFind then
			nAdv = nAdv - 1;
		end
	end

	-- Specific states
	local bCond = Utilities.inArray({DataCommon.ability_luck, DataCommon.ability_luck_trans, DataCommon.ability_spirit, DataCommon.ability_spirit_trans}, sAbility);
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Dizzy"]) and not bCond then
		nAdv = nAdv - 1;
	end

	if bPassive then
		nValue = nValue + DataCommon.passive_base + advantage2passiveBonus(nAdv);
		nState = nState + advantage2passiveBonus(nAdv);
	end

	-- If save, add magic protection and absolute protection
	if isSave then		
		local nProt = getAbsoluteProt(rActor);
		if keywords and keywords:find(DataCommon.ability_magic_trans) then
			nProt = nProt + getMagicProt(rActor);
		end
		nState = nState + math.ceil(nProt / 2);
	end

	-- If save, add magic protection and absolute protection
	if isSave then		
		local nProt = getAbsoluteProt(rActor);
		if keywords and keywords:find(DataCommon.ability_magic_trans) then
			nProt = nProt + getMagicProt(rActor);
		end
		nState = nState + math.ceil(nProt / 2);
	end

	-- Keywords
	if keywords and keywords ~= "" then
		_, _, _, _, nAdvKey = getSensitivity(rActor, keywords, nil, false)
		if nAdvKey < 99 then
			nAdv = nAdv + nAdvKey;
		else
			nAdv = 0;
			nValue = 99;
			nState = 99;
		end
	end

	return nValue + nState, nAdv, nState, isSave, bLimited, bLimitedSkill, aDice;
end


function getAttack(rActor, rTarget, sAtribute, sSkill, rAction)
	-- Get node and actor
	nodeActor, rActor = getActorAndNode(rActor);

	-- Get skill nodes
	local nodeSkill, _, nodeSpecialty = getSkillNode(nodeActor, sSkill);

	-- Get base values
	local nValue, nAdv, nState0, _, _, _, aDice = getCheck(rActor, sAtribute, nodeSkill, nodeSpeciality,  rAction.hand  == 1);

	-- Attribute
	local sTranslated;
	sAtribute, sTranslated = DataCommon.getRulesetAndTranslatedAbility(sAtribute)

	-- Add attack specific modifiers
	local nState = 0
	if rAction.bSuperiorCover then
		nState = nState - 5;
	elseif rAction.bCover then
		nState = nState - 2;
	end

	-- Build attack filter
	local aAttackFilter = {sAtribute, sTranslated, sSkill:lower()};
	if rAction.type == 2 then
		table.insert(aAttackFilter, DataCommon.rangetypes_inv["thrown"]);
	elseif rAction.type == 1 then
		table.insert(aAttackFilter, DataCommon.rangetypes_inv["ranged"]);
	else
		table.insert(aAttackFilter, DataCommon.rangetypes_inv["melee"]);
	end
	if rAction.bOpportunity then
		table.insert(aAttackFilter, DataCommon.opportunity);
	end
	if rAction.label then
		table.insert(aAttackFilter, rAction.label:lower());
	end

	-- Add range effect
	local nRange = rAction.range or 1;
	local nExtraRange, _ = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["RANGE"], true, aAttackFilter);
	nRange = nRange + nExtraRange;

	-- Get attack effect modifiers
	local aAddDice, nState2 = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["ATK"], false, aAttackFilter);
	nState = nState + nState2;
	aDice = Utilities.addEffectDice(aDice, aAddDice)

	local nAdv2 = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["ADVATK"], true, aAttackFilter);
	nAdv = nAdv + nAdv2;

	-- Get morale modifiers
	local nMoral = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["MORAL"]}, true);
	if nMoral ~= 0 then
		nState = nState + nMoral / math.abs(nMoral);
	end

	-- Specific states
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Entangled"]) then
		nAdv = nAdv - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Terrified"]) then
		nAdv = nAdv - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Stunned"]) then
		nAdv = nAdv - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Ashamed"]) then
		nState = nState - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Blinded"]) then
		nAdv = nAdv - 1;
	elseif EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Dazzled"]) then
		nState = nState - 3;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Weakened"]) and sAtribute ~= "dexterity" then
		nAdv = nAdv - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Distracted"]) then
		nState = nState - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Turned"]) then
		nAdv = nAdv - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Dizzy"]) then
		nAdv = nAdv - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Prone"]) then
		nAdv = nAdv - 1;
	end
	if EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Hidden"]) then
		nAdv = nAdv - 1;
	end

	-- Calculate attack bonus vs target
	if rTarget and ActorManager.hasCT(rTarget) then
		local aAddDice, nState2, _ = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["ATK"], false, aAttackFilter, rTarget, true);
		nState = nState + nState2;
		aDice = Utilities.addEffectDice(aDice, aAddDice);
	end
	-- Add deffense advantage and autofail;
	local nDefAdv, bAutofail, nRanges = getDefenseAdvantage(rActor, rTarget, aAttackFilter, nRange, rAction.nLens);
	nAdv = nAdv + nDefAdv;

	-- Add weapon properties
	nState = nState + rAction.nReach + (rAction.modifier or rAction.mod or 0);
	if rAction.bLarge then
		nState = nState - 1;
		if CombatManager2.getNumberCloseAllies(rActor) > 0 then
			nAdv = nAdv - 1;
		end
	end

	return nValue + nState, nAdv, nState + nState0, bAutofail, nRanges, aDice;
end

function getCriticalAttack(rActor, rTarget, nBase, aAttackFilter)
	-- Get the base value
	local nCritAtk, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CRITATK"]}, true, aAttackFilter);
	nBase = nBase + nCritAtk;

	-- Get oponent defense and targetted effects
	if rTarget and ActorManager.hasCT(rTarget) then
		local nCritDef, _ = EffectManagerSS.getEffectsBonus(rTarget, {DataCommon.keyword_states["CRITDEF"]}, true, aAttackFilter, rActor, false);
		nCritAtk, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CRITATK"]}, true, aAttackFilter, rTarget, true);
		nBase = nBase + nCritAtk - nCritDef;
	end

	return nBase;
end


function getDefenseAdvantage(rAttacker, rDefender, aAttackFilter, nRange, nLens)
	if not rDefender then
		return 0, false, nil;
	end
	if not nLens then
		nLens = 0;
	end

	-- Check effects
	local nAdv = 0;

	if ActorManager.hasCT(rDefender) then
		-- Attack advantage
		local nAdvEff, nEffectCount = EffectManagerSS.getEffectsBonus(rAttacker, {DataCommon.keyword_states["ADVATK"]}, true, aAttackFilter, rDefender, true);
		if nEffectCount > 0 then
			nAdv = nAdv + nAdvEff;
		end
		-- Grant advantage
		nAdvEff, nEffectCount = EffectManagerSS.getEffectsBonus(rDefender, {"GRANTADVATK"}, true, aAttackFilter, rAttacker, false);
		if nEffectCount > 0 then
			nAdv = nAdv + nAdvEff;
		end
		-- Targeted Invisible
		if EffectManagerSS.hasEffect(rAttacker, DataCommon.conditions["Invisible"], rDefender, true) then
			nAdv = nAdv + 1;
		end

		-- Defender states
		if EffectManagerSS.hasEffect(rDefender, DataCommon.conditions["Confused"]) then
			nAdv = nAdv + 1;
		end

		if EffectManagerSS.hasEffect(rDefender, DataCommon.conditions["Prone"]) then
			if StringManager.contains(aAttackFilter, DataCommon.rangetypes_inv["melee"]) then
				nAdv = nAdv + 1;
			end
		end
	end

	-- Get distance between attacker and defender
	local bAutofail = false;
	local nRanges = 1;
	local nSquares = CombatManager2.getTokenDistance(rAttacker, rDefender);
	if nSquares then
		if StringManager.contains(aAttackFilter, DataCommon.rangetypes_inv["melee"]) then
			if nSquares > nRange then
				bAutofail = true;
			end
		elseif StringManager.contains(aAttackFilter, DataCommon.rangetypes_inv["ranged"]) then
			local nAbility, _, nState = getAbility(rAttacker, "perception", false);
			nAbility = math.ceil(nAbility + nState);
			nRanges = math.ceil(nSquares / nRange) - nLens;
			if nRanges <= 0 or (nRanges > 1 and nRanges <= math.ceil(nAbility/2))  then
				nAdv = nAdv - 1;
			elseif nRanges > math.ceil(nAbility/2) and nRanges <= nAbility then
				nAdv = nAdv - 2;
			elseif nRanges > nAbility then
				bAutofail = true;
			end
		elseif StringManager.contains(aAttackFilter, DataCommon.rangetypes_inv["thrown"]) then
			local nAbility, _, nState = getAbility(rAttacker, "strength", false);
			nAbility = (nAbility + nState) / 2;
			local nRanges = math.ceil(nSquares / nRange);
			if nRanges > 0 and nRanges <= nAbility then
				nAdv = nAdv - nRanges + 1;
			else
				bAutofail = true;
			end
		end
	end

	return nAdv, bAutofail, nRanges;
end


function updateDefenses(nodeChar)
	-- Get the equped weapons
	local nodeMain, nodeSide;
	local nodeList = nodeChar.createChild("weaponlist")
	local bTwoHands = false;
	for _, nodeWeapon in pairs(nodeList.getChildren()) do
		if DB.getValue(nodeWeapon, "carried", 0) == 2 then
			local nHands = DB.getValue(nodeWeapon, "hands", 0);
			if nHands == 0 then
				nodeMain = nodeWeapon;
			elseif nHands == 1 then
				nodeSide = nodeWeapon;
			else
				nodeMain = nodeWeapon;
				bTwoHands = true;
				if ItemManager2.isDoubleWeapon(nodeWeapon) then
					nodeSide = nodeWeapon;
				end
			end
		end
	end

	-- Update values
	updateDefensesOfWeapon(nodeChar, nodeMain, "main_hand", bTwoHands);
	updateDefensesOfWeapon(nodeChar, nodeSide, "side_hand", bTwoHands);

	CombatManager2.recalculateDefenses(nodeChar);
end

function updateDefensesOfWeapon(nodeChar, nodeWeapon, sHand, bTwoHands)
	-- Get the data
	sDefense = "defense." .. sHand;
	nodeChar, rActor = getActorAndNode(nodeChar);
	if not nodeChar or nodeChar.getName() == "mount_npc" then
		return;
	end

	local sAbility = "";
	local sSkill = "";
	local sPath = DB.getValue(nodeWeapon, "item_path", "");
	local nodeItem = DB.findNode(sPath);
	local nBonus = DB.getValue(nodeItem, "bonus_aux", 0) - DB.getValue(nodeItem, "dents_aux", 0);
	local nBonusDef = 0;
	local nBonusDefRanged = 0;
	local nInfused = 0;
	local uUnlimited = 0;
	local nMult = 4;
	local sName = ""
	local aPropsDef = {};

	if nodeWeapon then
		sName = DB.getValue(nodeWeapon, "name", "");
		local nodeList = nodeWeapon.createChild("attacks")
		for _,nodeAttack in pairs(nodeList.getChildren()) do
			local nType = DB.getValue(nodeAttack, "type_attack", 0);
			local aProps = ItemManager2.scanProperties(DB.getValue(nodeAttack, "properties", ""));
			local cond = (aProps["two_hands"] and bTwoHands) or (not aProps["two_hands"] and not bTwoHands);
			if nType == 0 and cond and not aProps["no_parry"] then
				sAbility = DB.getValue(nodeAttack, "attack_ability", "");
				sSkill = DB.getValue(nodeAttack, "attack_skill", "");
				local sDamage = DB.getValue(nodeAttack, "damageview", "");
				if sDamage:find(DataCommon.dmgtypes_modifier["magic"]) then
					nInfused = 1;
				end
				aPropsDef = aProps;
				break;
			end
		end

		-- Calculate bonus to attack and defense
		nBonusDef = nBonus + (aPropsDef["defense_melee"] or 0) + (aPropsDef["length"] or 0);


		nBonusDefRanged = nBonus + (aPropsDef["defense_ranged"] or -99);
		if aPropsDef["no_parry"] then
			nBonusDef = -99;
			nBonusDefRanged = -99;
		end
		if aPropsDef["unlimited"] then
			uUnlimited = 1;
		end
		local nBonusMult, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["MELEEMULT"]}, true, {Interface.getString(sHand):lower()});
		nMult = (aPropsDef["defense_mult"] or 4) + (nBonusMult or 0);
	end
	
	-- Save the info
	DB.setValue(nodeChar, sDefense .. ".name", "string", sName);
	DB.setValue(nodeChar, sDefense .. ".atributecontainer", "string", sAbility);
	DB.setValue(nodeChar, sDefense .. ".skillcontainer", "string", sSkill);
	DB.setValue(nodeChar, sDefense .. ".equip.melee", "number", nBonusDef);
	DB.setValue(nodeChar, sDefense .. ".equip.ranged", "number", nBonusDefRanged);
	DB.setValue(nodeChar, sDefense .. ".infused", "number", nInfused);
	DB.setValue(nodeChar, sDefense .. ".uses_mult", "number", nMult);
end


function getProtectionValue(rActor, aDamage, nPiercing, nMagicPiercing)
	-- Initializing
	if not nPiercing then
		nPiercing = 0;
	end
	if not nMagicPiercing then
		nMagicPiercing = 0;
	end
	-- TODO: Add effects to piercings

	-- TODO: Add effects to piercings

	-- Calculate other protections
	local nAbsProt = getAbsoluteProt(rActor);
	local sNatArmor = DB.getValue(nodeActor, "protection.natural_armor", "");


	-- loop through damage types
	local nProt = 0;
	local nTotalArmor = 0;
	local nTotalNat = 0;
	local nResist = 0;
	local nMast = -1;
	local sProt = "";
	local nodeInvList = nodeActor.createChild("inventorylist");
	for _, sDamage in pairs(aDamage) do
		if sDamage:lower() == DataCommon.dmgtypes_modifier["unavoidable"] then
			return 0;
		elseif sDamage:lower() == DataCommon.dmgtypes_modifier["magic"] then
			nAbsProt = nAbsProt + math.max(0, getMagicProt(rActor) - nMagicPiercing);
			nAbsProt = nAbsProt + math.max(0, getMagicProt(rActor) - nMagicPiercing);
		else
			-- Get the protection from armours
			for _, nodeItem in pairs(nodeInvList.getChildren()) do
				if DB.getValue(nodeItem, "carried", 0) == 2 and ItemManager2.isArmor(nodeItem) then
					-- Get data
					sProt = DB.getValue(nodeItem, "protection", "");
					local nCurrent =getProtFromString(sProt, sDamage, aDamage);
					local sProps = DB.getValue(nodeItem, "properties", ""):lower();
					local nItemRes = sProps:match(DataCommon.armor_keywords_number["resistence"]);

					-- Get mastery
					local sType = DB.getValue(nodeItem, "subtype", ""):lower();
					local nMastCurrent = -1;
					if sType ~= DataCommon.armor_clothes and sType ~= DataCommon.armor_summoned then
						local nodeSkill = getSkillNode(rActor, sType);
						nMastCurrent = DB.getValue(nodeSkill, "prof", 0);
					end

					-- Take the highest values
					if nItemRes then
						nResist = math.max(nResist, nItemRes);
					end
					if nCurrent > nProt then
						nProt = nCurrent;
					end
					if nMastCurrent > nMast then
						nMast = nMastCurrent;
					end
				end
			end
			-- Add state protection
			local nState = getEffectProt(rActor, sDamage, aDamage, DataCommon.keyword_states["PROT"]);
			-- Choose the correct max prot
			nTotalArmor = math.max(nTotalArmor, nProt + nState);
			-- Calculate the natural armor
			nProt =  getProtFromString(sNatArmor, sDamage, aDamage);
			-- Calculate natural armor from effects
			nState = getEffectProt(rActor, sDamage, aDamage, DataCommon.keyword_states["NATPROT"]);
			-- Choose the correct max prot
			nTotalNat = math.max(nTotalNat, nProt + nState);
		end
	end

	-- Add wisely natural and item armor
	nPiercing = math.max(0, nPiercing - nResist);
	local nTotalProt = math.max(nTotalArmor, nTotalNat) + math.floor(math.min(nTotalArmor, nTotalNat) / 2) - nPiercing;
	nTotalProt = math.max(nTotalProt, 0);
	nTotalProt = math.max(nTotalProt, 0);

	return nTotalProt, nAbsProt, nMast;
end

function getMagicProt(rActor)
	nodeActor, rActor = getActorAndNode(rActor);
	local nProt = DB.getValue(nodeActor, "protection.magic", 0);
	local nState, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["MPROT"]}, true);
	return nProt + nState;
end

function getAbsoluteProt(rActor)
	nodeActor, rActor = getActorAndNode(rActor);
	local nProt = DB.getValue(nodeActor, "protection.absolute", 0);
	local nState, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["APROT"]}, true);
	return nProt + nState;
end

function getProtFromString(sProt, sDamage, aDamage)
	-- Check that we are not in an exception case
	local nProt = 0;
	if sProt ~= "" then
		sProt = sProt:lower();
		for _, v in pairs(aDamage) do
			if string.match(sProt, "!" .. v:lower()) then
				return 0;
			end
		end
		-- Calculate the protection
		local sCut = string.match(sProt, "%d+ " .. sDamage:lower());
		if sCut then
			nProt = tonumber(string.match(sCut, "%d+"));
		end
		sCut = string.match(sProt, "%d+ all");
		if sCut then
			nProt = tonumber(string.match(sCut, "%d+"));
		end
	end
	return nProt;
end

function sumProtection(strings, result_string)
	-- Create array of damages and values
	aDamages = {};
	for _, str in pairs(strings) do
		local aCurrent = StringManager.split(str, ",");
		for _, prot in pairs(aCurrent) do
			local sCut = string.match(prot, "%d+ %a+");
			local nProt = tonumber(string.match(sCut, "%d+"));
			local sProt = string.match(sCut, "%a+");
			if aDamages[sProt] then
				aDamages[sProt] = math.max(aDamages[sProt], nProt);
			else
				aDamages[sProt] = nProt
			end
		end
	end

	-- Result
	if result_string then
		local sResult = "";
		add_coma = false;
		for key, value in pairs(aDamages) do
			if add_coma then
				sResult = sResult .. ", ";
			end
			sResult = sResult .. tostring(value) .. " " .. key;
			add_coma = true;
		end
		return sResult;
	else
		return aDamages;
	end
end

-- TODO: Arreglar
function getEffectProt(rActor, sDamage, aDamage, sEffectType)
	-- Get effects
	local aEffects = EffectManagerSS.getEffectsByType(rActor, sEffectType, {});
	local nProt = 0;

	-- loop in effects
	for _,v in pairs(aEffects) do
		for _,vType in pairs(v.remainder) do
			if vType:sub(1,1) == "!" and StringManager.contains(aDamage, vType:sub(2)) then
				return 0;
			elseif vType ~= DataCommon.dmgtypes_modifier["unavoidable"] and (vType == sDamage) or (vType == DataCommon.dmgtypes_special["all"]) then
				nProt = v.mod;
			end
		end
	end
	return nProt;
end

function getWeight(nodeActor)
	local sWeight = DB.getValue(nodeActor, "weight", "0");
	local sNumber = sWeight:match("(%d.)");
	if not sNumber or sNumber == "" then
		sNumber = "0";
	end
	return tonumber(sNumber);
end


function hasAptitude(rActor, sName, nRank)
	-- Safety
	if not rActor or not sName or sName == "" then
		return false;
	end
	if not nRank then
		nRank = 1;
	end
	nodeActor, rActor = getActorAndNode(rActor);
	if not User.isHost() or not nodeActor.isOwner(User.getUsername()) then
		return false;
	end

	-- Work
	local nodeList = DB.createChild(nodeActor, "disadvantagestitle");
	sName = sName:lower();
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", ""):lower();
		local nCurrent = DB.getValue(v, "category", 0);
		if sName == sCurrent and nCurrent >= nRank then
			return true;
		end
	end
	local nodeList = DB.createChild(nodeActor, "genericlist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", ""):lower();
		local nCurrent = DB.getValue(v, "category", 0);
		if sName == sCurrent and nCurrent >= nRank then
			return true;
		end
	end
	local nodeList = DB.createChild(nodeActor, "raciallist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", ""):lower();
		local nCurrent = DB.getValue(v, "category", 0);
		if sName == sCurrent and nCurrent >= nRank then
			return true;
		end
	end
	local nodeList = DB.createChild(nodeActor, "professionlist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", ""):lower();
		local nCurrent = DB.getValue(v, "category", 0);
		if sName == sCurrent and nCurrent >= nRank then
			return true;
		end
	end
	return false;
end

function getAptitudeCategory(rActor, sName)
	-- Safety
	if not rActor or not sName or sName == "" then
		return 0;
	end
	sName = sName:lower();
	local nodeActor, rActor = getActorAndNode(rActor);
	local nodeList = DB.createChild(nodeActor, "disadvantagestitle");
	local nRank = 0;
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", ""):lower();
		if sCurrent:find(sName) then
			nRank = nRank + DB.getValue(v, "category", 0);
		end
	end
	nodeList = DB.createChild(nodeActor, "genericlist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", ""):lower();
		if sCurrent:find(sName) then
			nRank = nRank + DB.getValue(v, "category", 0);
		end
	end
	nodeList = DB.createChild(nodeActor, "raciallist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", ""):lower();
		if sCurrent:find(sName) then
			nRank = nRank + DB.getValue(v, "category", 0);
		end
	end
	nodeList = DB.createChild(nodeActor, "professionlist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", ""):lower();
		if sCurrent:find(sName) then
			nRank = nRank + DB.getValue(v, "category", 0);
		end
	end
	return nRank;
end

function hasTechnique(rActor, sName)
	nodeActor, rActor = getActorAndNode(rActor);
	-- Magic techniques
	local nodeList = DB.createChild(nodeActor, "innatelist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", "");
		if sCurrent:find(sName) then
			return true;
		end
	end
	local nodeList = DB.createChild(nodeActor, "basiclist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", "");
		if sCurrent:find(sName) then
			return true;
		end
	end
	local nodeList = DB.createChild(nodeActor, "advancedlist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", "");
		if sCurrent:find(sName) then
			return true;
		end
	end
	local nodeList = DB.createChild(nodeActor, "masterlist");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", "");
		if sCurrent:find(sName) then
			return true;
		end
	end

	-- Combat techniques
	local nodeList = DB.createChild(nodeActor, "basiclistcombat");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", "");
		if sName == sCurrent then
			return true;
		end
	end
	local nodeList = DB.createChild(nodeActor, "advancedlistcombat");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", "");
		if sName == sCurrent then
			return true;
		end
	end
	local nodeList = DB.createChild(nodeActor, "masterlistcombat");
	for _,v in pairs(nodeList.getChildren()) do
		local sCurrent = DB.getValue(v, "name", "");
		if sName == sCurrent then
			return true;
		end
	end
	return false;
end

function getConcentrationMax(rActor)
	if EffectManagerSS.hasAnyEffectCondition(rActor, DataCommon.conditions_end_concentration) then
		return 0;
	end
	return math.max(1, EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["CONCENTRATION"]}, true, {}) + 1);
end

function hasConcentrationEffects(rActor)
	local aIDs, _ = EffectManagerSS.findConcentrationEffects(rActor);
	local nEffects = #aIDs;
	local bCond = nEffects > 0;
	return bCond, nEffects;
end

function getNumberEquipedWeapons(rActor, bMelee)
	-- TODO
	local nodeActor, rActor = getActorAndNode(rActor);
	local nodeList = DB.getChild(nodeActor, "weaponlist");
	local nWeapons = 0;
	if nodeList then
		for _,node in pairs(nodeList.getChildren()) do
			local nCarried = DB.getValue(node, "carried", 0);
			if nCarried == 2 and bMelee then
				local nodeAttacks = DB.getChild(node, "attacks");
				if nodeAttacks then
					local nAdd = 0;
					for _,nodeAt in pairs(nodeAttacks.getChildren()) do
						if DB.getValue(nodeAt, "type_attack", 0) == 0 then
							nAdd = 1;
						end
					end
					nWeapons = nWeapons + nAdd;
				end
			elseif nCarried == 2 then
				nWeapons = nWeapons + 1;
			end
		end
	end
	return nWeapons;
end

function rest(rActor, nValue, nTiring, sTypeRest, nHours)
	-- Get node and actor
	nodeActor, rActor = getActorAndNode(rActor);
	-- Messsage
	local aHolders = nodeActor.getHolders();
	table.insert(aHolders, "")
	local message = {mode="chat_heal", icon=DB.getValue(nodeActor, "portrait", ""), text=""};
	local sName = DB.getValue(nodeActor, "name", "");

	-- Special rests
	local sCharType = DB.getValue(nodeActor, "type", "");
	if ActorManager2.hasAptitude(DataCommon.aptitude["blood_power"]) then
		local nEndurance = DB.getValue(nodeActor, "health.endurance", 1);
		local rAction = {attackType = "", range = 0, nReroll = 0, nPiercing = 0, sTargeting = "self", nAvoidNotifyAction = 1};
		rAction.clauses = {};
		table.insert(rAction.clauses, { dice = {}, dmgtype = DataCommon.dmgtypes_modifier["unavoidable"] .. ", " .. DataCommon.dmgtypes_modifier["nonlethal"], modifier = nEndurance});
		return;
	elseif Utilities.inArray(DataCommon.no_rest_char_types, sCharType) then		
		message.text = message.text .. "\n" .. sName:upper() .. ":" .. string.format(Interface.getString("message_norest"), sCharType);
		-- Comm.deliverChatMessage(message);
		if #aHolders > 0 then
			Comm.deliverChatMessage(message, aHolders);
		else
			Comm.deliverChatMessage(message, "");
		end
		return;
	end

	-- Manage regen
	local bCond = EffectManagerSS.reactivateRegenerationEffects(rActor);
	if bCond then
		msg = {font = "msgfont", icon = "roll_trueheal", mode="chat_success", text=Interface.getString("heal_chat_regen_activate")};
		Comm.deliverChatMessage(msg);
	end
	local nodeCT = ActorManager2.getCTNode(rActor);
	DB.setValue(nodeCT, "nSpentEssenceByRegen", "number", 0);

	-- Get the correct info
	local nRecoveryVitality = 0;
	local nRecoveryVigor = 0;
	local nRecoveryRegen = 0;
	local nRecoveryMadness = 0;
	local nRecoveryMadnessPerm = 0;
	local nRecoverySpirit = 0;
	local nRecoveryTired = 0;
	local nRecoveryPain = 0;
	local nRecoverySave = 0;
	if  sTypeRest == "menu_reststory" then
		nRecoveryVitality = DataCommon.rest_story["vit"];
		nRecoveryVigor = DataCommon.rest_story["vigor"];
		nRecoveryRegen = DataCommon.rest_story["regen"];
		nRecoveryMadness = DataCommon.rest_story["mental"];
		nRecoveryMadnessPerm = DataCommon.rest_story["mental_perm"];
		nRecoverySpirit = DataCommon.rest_story["spiritual"];
		nRecoveryTired = DataCommon.rest_story["tired"];
		nRecoveryPain = DataCommon.rest_story["pain"];
		nRecoverySave = DataCommon.rest_story["save"];
		message.text = Interface.getString("message_reststory") .. "\n";

	elseif  sTypeRest == "menu_restlong" then
		nRecoveryVitality = DataCommon.rest_long["vit"][nValue];
		nRecoveryVigor = DataCommon.rest_long["vigor"][nValue];
		nRecoveryRegen = DataCommon.rest_long["regen"][nValue];
		nRecoveryMadness = DataCommon.rest_long["mental"][nValue];
		nRecoveryTired = DataCommon.rest_long["tired"][nValue];
		nRecoveryPain = DataCommon.rest_long["pain"][nValue];
		nRecoverySave = DataCommon.rest_long["save"][nValue];
		message.text = Interface.getString("message_restlong") .. tostring(nValue) .. ")\n";
	else
		nRecoveryVitality = DataCommon.rest_short["vit"][nValue];
		nRecoveryVigor = DataCommon.rest_short["vigor"][nValue];
		nRecoveryRegen = DataCommon.rest_short["regen"][nValue];
		nRecoveryTired = DataCommon.rest_short["tired"][nValue];
		nRecoverySave = DataCommon.rest_short["save"][nValue];
		message.text = Interface.getString("message_restshort") .. tostring(nValue) .. ")\n";
	end
	local sDesc = "";

	-- Heal Vitality
	local nDamageVit = DB.getValue(nodeActor, "health.vit.damage", 0);
	local nNew = nDamageVit;
	local nRegen = getRegenerationValue(rActor);
	if nDamageVit > 0 then
		if nRegen > 0 then
			local nFort = 1;
			if sType ~= "menu_restshort" then
				nFort = DB.getValue(nodeActor, "health.fortitude", 1);
			end
			local nRec = Utilities.round(nRegen * nFort * nRecoveryRegen);
			nNew = math.max(nDamageVit - nRec, 0);
			sDesc = sDesc .. Interface.getString("message_rest_regen") .. tostring(nRec);
		elseif nRecoveryVitality > 0 then
			nNew = math.max(nDamageVit - nRecoveryVitality, 0);
			sDesc = sDesc .. Interface.getString("message_rest_vit") .. tostring(nRecoveryVitality);
		end
		DB.setValue(nodeActor, "health.vit.damage", "number", nNew);
		if nNew == 0 then
			sDesc = sDesc .. Interface.getString("message_rest_all");
		end
	end

	--  Heal Vigor
	local nMaxVigor = DB.getValue(nodeActor, "health.vigor.max", 0);
	local nDamageVigor = DB.getValue(nodeActor, "health.vigor.damage", 0);
	local nRec = Utilities.round(nMaxVigor * nRecoveryVigor);
	if nRec > 0 and nDamageVigor > 0 then
		local nNew = math.max(nDamageVigor - nRec, 0);
		DB.setValue(nodeActor, "health.vigor.damage", "number", nNew);
		sDesc = sDesc .. Interface.getString("message_rest_vigor") .. tostring(nRec);
		if nNew == 0 then
			sDesc = sDesc .. Interface.getString("message_rest_all");
		end
	end

	-- Heal Essence
	local nMaxEssence = DB.getValue(nodeActor, "health.essence.max", 0);
	local nDamageEssence = DB.getValue(nodeActor, "health.essence.damage", 0);
	nRec = Utilities.round(nMaxEssence * nRecoveryVigor);
	if nRec > 0 and nDamageEssence > 0 then
		local nNew = math.max(nDamageEssence - nRec, 0);
		DB.setValue(nodeActor, "health.essence.damage", "number", nNew);
		sDesc = sDesc .. Interface.getString("message_rest_essence") .. tostring(nRec);
		if nNew == 0 then
			sDesc = sDesc .. Interface.getString("message_rest_all");
		end
	end

	-- Heal Tired
	local nTired = DB.getValue(nodeActor, "health.penal.vigor.misc", 0);
	if nRecoveryTired ~= 0 and nTired < 0 then
		local nNew = math.min(nTired + nRecoveryTired, 0);
		DB.setValue(nodeActor, "health.penal.vigor.misc", "number", nNew);
		sDesc = sDesc .. Interface.getString("message_rest_tired") .. tostring(nRecoveryTired);
		if nNew == 0 then
			sDesc = sDesc .. Interface.getString("message_rest_all");
		end
	end

	-- Heal pain
	local nPain = DB.getValue(nodeActor, "health.penal.vit.misc", 0);
	if nRecoveryPain ~= 0 and nPain < 0 then
		local nNew = math.min(nPain + nRecoveryPain, 0);
		DB.setValue(nodeActor, "health.penal.vit.misc", "number", nNew);
		sDesc = sDesc .. Interface.getString("message_rest_pain") .. tostring(nRecoveryPain);
		if nNew == 0 then
			sDesc = sDesc .. Interface.getString("message_rest_all");
		end
	end

	-- Heal temporal madness
	local nDamageSanity = DB.getValue(nodeActor, "health.sanity.damage", 0);
	if nRecoveryMadness > 0 and nDamageSanity > 0 then
		local nNew = math.max(nDamageSanity - nRecoveryMadness, 0);
		DB.setValue(nodeActor, "health.sanity.damage", "number", nNew);
		sDesc = sDesc .. Interface.getString("message_rest_madness") .. tostring(nRecoveryMadness);
		if nNew == 0 then
			sDesc = sDesc .. Interface.getString("message_rest_all");
		end
	end

	-- Heal permanent madness
	nDamageSanity = DB.getValue(nodeActor, "health.sanity.damage_perm", 0);
	if nRecoveryMadnessPerm > 0 and nDamageSanity > 0 then
		local nNew = math.max(nRecoveryMadnessPerm - nRecoveryMadness, 0);
		DB.setValue(nodeActor, "health.sanity.damage_perm", "number", nNew);
		sDesc = sDesc .. Interface.getString("message_rest_madness_perm") .. tostring(nRecoveryMadnessPerm);
		if nNew == 0 then
			sDesc = sDesc .. Interface.getString("message_rest_all");
		end
	end

	-- Heal temporal spirit
	local nDamage = DB.getValue(nodeActor, "health.spirit.damage", 0);
	if nRecoverySpirit > 0 and nDamage > 0 then
		local nNew = math.max(nDamage - nRecoverySpirit, 0);
		DB.setValue(nodeActor, "health.spirit.damage", "number", nNew);
		sDesc = sDesc .. Interface.getString("message_rest_spirit") .. tostring(nRecoverySpirit);
		if nNew == 0 then
			sDesc = sDesc .. Interface.getString("message_rest_all");
		end
	end

	-- Recharge magic items
	local nodeList = DB.createChild(nodeActor, "powers");
	for _,nodePower in pairs(nodeList.getChildren()) do
		local nUsed = DB.getValue(nodePower, "cast", 0);
		local nUsedOld = nUsed;
		local nRecovery = DB.getValue(nodePower, "charges_recovery", 0);
		if (nUsed > 0) and (nRecovery > 0) then
			if sTypeRest == "menu_reststory" then
				nUsed = 0;
			else
				local sTypeRec = DB.getValue(nodePower, "usesperiod", "");
				if sTypeRest == "menu_restlong" then
					if sTypeRec == "power_label_useperiod_daily" then
						nUsed = math.max(0, nUsed - nRecovery);
					elseif sTypeRec == "power_label_useperiod_enc" then
						nUsed = 0;
					end
				elseif sTypeRec == "power_label_useperiod_enc" then
					nUsed = math.max(0, nUsed - nRecovery);
				end
			end
			DB.setValue(nodePower, "cast", "number", nUsed)
			local sName = DB.getValue(nodePower, "name", "");
			sDesc = sDesc .. string.format(Interface.getString("message_rest_powers"), sName, nUsedOld - nUsed);
			if nUsed == 0 then
				sDesc = sDesc .. Interface.getString("message_rest_all");
			end
		end
	end

	-- TODO: Saves
	-- if rRest["save"][nValue] ~= 0 and #aHolders > 0 then
		-- message.text = message.text .. Interface.getString("rest_saves_message") .. tostring(rRest["save"][nValue]) .. "\n";
	-- end

	

	if sDesc ~= "" then
		message.text = message.text .. "\n" .. sName:upper() .. ":" .. sDesc;
		-- Comm.deliverChatMessage(message);
		if #aHolders > 0 then
			Comm.deliverChatMessage(message, aHolders);
		else
			Comm.deliverChatMessage(message, "");
		end
	end

end

function getRegenerationValue(rActor)
	local nRegen, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["REGEN"]}, true);
	return nRegen or 0;
end



function getBestGift(rActor)
	local nodeActor, rActor = getActorAndNode(rActor);

	-- Get info
	local nArcane = DB.getValue(nodeActor, "abilities.arcane.current", 0);
	local nSacred = DB.getValue(nodeActor, "abilities.sacred.current", 0);
	local nPrimal = DB.getValue(nodeActor, "abilities.primal.current", 0);
	local nMental = DB.getValue(nodeActor, "abilities.mental.current", 0);
	local nElemental = DB.getValue(nodeActor, "abilities.elemental.current", 0);

	-- Get the best gift
	local sAbility = "";
	local sSkill = "";
	local nBest = 0;
	if nArcane > 0 then
		nBest = nArcane;
		sAbility = DataCommon.abilities_translation_inv["arcane"];
		sSkill = Interface.getString("skill_value_arcaneskill");
	end
	if nSacred > nBest then
		nBest = nArcane;
		sAbility = DataCommon.abilities_translation_inv["sacred"];
		sSkill = Interface.getString("skill_value_sacredskill");
	end
	if nPrimal > nBest then
		nBest = nArcane;
		sAbility = DataCommon.abilities_translation_inv["primal"];
		sSkill = Interface.getString("skill_value_primalskill");
	end
	if nMental > nBest then
		nBest = nArcane;
		sAbility = DataCommon.abilities_translation_inv["mental"];
		sSkill = Interface.getString("skill_value_psychicskill");
	end
	if nElemental > nBest then
		nBest = nArcane;
		sAbility = DataCommon.abilities_translation_inv["elemental"];
		local nValue = -1;
		local aElements = {
			[Interface.getString("skill_value_air")] = "air_skill",
			[Interface.getString("skill_value_water")] = "water_skill",
			[Interface.getString("skill_value_fire")] = "fire_skill",
			[Interface.getString("skill_value_earth")] = "earth_skill",
			[Interface.getString("skill_value_mind")] = "mind_skill",
			[Interface.getString("skill_value_spirit")] = "spirit_skill",
		};
		for k,v in pairs(aElements) do
			local nNew = DB.getValue(nodeActor, "skilllist_magic." .. v .. ".prof", 0);
			if nNew > nValue then
				nValue = nNew;
				sSkill = k;
			end
		end
	end

	return sAbility, sSkill;

end

function isMagicUser(rActor, bFromCT)
	-- Get node and actor
	if bFromCT then
		rActor = ActorManager.resolveActor(rActor);
	end
	local nodeActor, rActor = getActorAndNode(rActor);

	-- Check if it is magic user
	local bArcane = DB.getValue(nodeActor, "abilities.arcane.current", 0) > 0;
	local bSacred = DB.getValue(nodeActor, "abilities.sacred.current", 0) > 0;
	local bPrimal = DB.getValue(nodeActor, "abilities.primal.current", 0) > 0;
	local bPsychic = DB.getValue(nodeActor, "abilities.psychic.current", 0) > 0;
	local bElemental = DB.getValue(nodeActor, "abilities.elemental.current", 0) > 0;
	local bMagic = bArcane or bSacred or bPrimal or bPsychic or bElemental;

	return bMagic
end

function isMethodicChanneler(rActor)
	local nodeActor, _ = getActorAndNode(rActor);
	return DB.getValue(nodeActor, "magic.methodic", 0) > 0;
end

function knowsSpell(rActor, sName, bFromCT)	
	-- Get node and actor
	if bFromCT then
		rActor = ActorManager.resolveActor(rActor);
	end
	local nodeActor, rActor = getActorAndNode(rActor);
	sName = sName:lower();

	-- Check
	local nodeList = nodeActor.createChild("powers");
	for _,nodeSpell in pairs(nodeList.getChildren()) do
		if sName == DB.getValue(nodeSpell, "name", ""):lower() then
			return true;
		end
	end
	return false;
end

function isMetallic(rActor, bFromCT)
	-- Get node and actor
	if bFromCT then
		rActor = ActorManager.resolveActor(rActor);
	end
	local nodeActor, rActor = getActorAndNode(rActor);

	-- Get property
	if hasAptitude(DataCommon.aptitude["metallic"]) then
		return true;
	else
		for _,vNode in pairs(DB.getChildren(nodeActor, "inventorylist")) do
			-- Weight
			local nCarried = DB.getValue(vNode, "carried", 0);
			if nCarried == 2 and ItemManager2.isArmor(vNode) then
				sProps = DB.getValue(vNode, "properties", ""):lower();
				if sProps:find(DataCommon.metallic) then
					return true;
				end
			end
		end
	end
	return false;
end

-- function isAlignment(rActor, sAlignCheck)
-- 	local nCheckLawChaosAxis = 0;
-- 	local nCheckGoodEvilAxis = 0;
-- 	local aCheckSplit = StringManager.split(sAlignCheck:lower(), " ", true);
-- 	for _,v in ipairs(aCheckSplit) do
-- 		if nCheckLawChaosAxis == 0 and DataCommon.alignment_lawchaos[v] then
-- 			nCheckLawChaosAxis = DataCommon.alignment_lawchaos[v];
-- 		end
-- 		if nCheckGoodEvilAxis == 0 and DataCommon.alignment_goodevil[v] then
-- 			nCheckGoodEvilAxis = DataCommon.alignment_goodevil[v];
-- 		end
-- 	end
-- 	if nCheckLawChaosAxis == 0 and nCheckGoodEvilAxis == 0 then
-- 		return false;
-- 	end
--
-- 	local nActorLawChaosAxis = 2;
-- 	local nActorGoodEvilAxis = 2;
-- 	local sType, nodeActor = ActorManager.getTypeAndNode(rActor);
-- 	local sField = "alignment";
-- 	local aActorSplit = StringManager.split(DB.getValue(nodeActor, sField, ""):lower(), " ", true);
-- 	for _,v in ipairs(aActorSplit) do
-- 		if nActorLawChaosAxis == 2 and DataCommon.alignment_lawchaos[v] then
-- 			nActorLawChaosAxis = DataCommon.alignment_lawchaos[v];
-- 		end
-- 		if nActorGoodEvilAxis == 2 and DataCommon.alignment_goodevil[v] then
-- 			nActorGoodEvilAxis = DataCommon.alignment_goodevil[v];
-- 		end
-- 	end
--
-- 	local bLCReturn = true;
-- 	if nCheckLawChaosAxis > 0 then
-- 		if nActorLawChaosAxis > 0 then
-- 			bLCReturn = (nActorLawChaosAxis == nCheckLawChaosAxis);
-- 		else
-- 			bLCReturn = false;
-- 		end
-- 	end
--
-- 	local bGEReturn = true;
-- 	if nCheckGoodEvilAxis > 0 then
-- 		if nActorGoodEvilAxis > 0 then
-- 			bGEReturn = (nActorGoodEvilAxis == nCheckGoodEvilAxis);
-- 		else
-- 			bGEReturn = false;
-- 		end
-- 	end
--
-- 	return (bLCReturn and bGEReturn);
-- end

function isSize(rActor, sSizeCheck)
	local sSizeCheckLower = StringManager.trim(sSizeCheck:lower());

	local sCheckOp = sSizeCheckLower:match("^[<>]?=?");
	if sCheckOp then
		sSizeCheckLower = StringManager.trim(sSizeCheckLower:sub(#sCheckOp + 1));
	end

	local nCheckSize = 0;
	if DataCommon.creaturesize[sSizeCheckLower] then
		nCheckSize = DataCommon.creaturesize[sSizeCheckLower];
	end
	if nCheckSize == 0 then
		return false;
	end

	local nActorSize = 0;
	local sType, nodeActor = ActorManager.getTypeAndNode(rActor);
	local sField = "size";
	local aActorSplit = StringManager.split(DB.getValue(nodeActor, sField, ""):lower(), " ", true);
	for _,v in ipairs(aActorSplit) do
		if nActorSize == 0 and DataCommon.creaturesize[v] then
			nActorSize = DataCommon.creaturesize[v];
			break;
		end
	end
	if nActorSize == 0 then
		nActorSize = 3;
	end

	local bReturn = true;
	if sCheckOp then
		if sCheckOp == "<" then
			bReturn = (nActorSize < nCheckSize);
		elseif sCheckOp == ">" then
			bReturn = (nActorSize > nCheckSize);
		elseif sCheckOp == "<=" then
			bReturn = (nActorSize <= nCheckSize);
		elseif sCheckOp == ">=" then
			bReturn = (nActorSize >= nCheckSize);
		else
			bReturn = (nActorSize == nCheckSize);
		end
	else
		bReturn = (nActorSize == nCheckSize);
	end

	return bReturn;
end

function getCreatureTypeHelper(sTypeCheck, bUseDefaultType)
	local aCheckSplit = StringManager.split(sTypeCheck:lower(), ", %(%)", true);

	local aTypeCheck = {};
	local aSubTypeCheck = {};

	-- Handle half races
	local nHalfRace = 0;
	for k = 1, #aCheckSplit do
		if aCheckSplit[k]:sub(1, #DataCommon.creaturehalftype) == DataCommon.creaturehalftype then
			aCheckSplit[k] = aCheckSplit[k]:sub(#DataCommon.creaturehalftype + 1);
			nHalfRace = nHalfRace + 1;
		end
	end
	if nHalfRace == 1 then
		if not StringManager.contains (aCheckSplit, DataCommon.creaturehalftypesubrace) then
			table.insert(aCheckSplit, DataCommon.creaturehalftypesubrace);
		end
	end

	-- Check each word combo in the creature type string against standard creature types and subtypes
	for k = 1, #aCheckSplit do
		for _,sMainType in ipairs(DataCommon.creaturetype) do
			local aMainTypeSplit = StringManager.split(sMainType, " ", true);
			if #aMainTypeSplit > 0 then
				local bMatch = true;
				for i = 1, #aMainTypeSplit do
					if aMainTypeSplit[i] ~= aCheckSplit[k - 1 + i] then
						bMatch = false;
						break;
					end
				end
				if bMatch then
					table.insert(aTypeCheck, sMainType);
					k = k + (#aMainTypeSplit - 1);
				end
			end
		end
		for _,sSubType in ipairs(DataCommon.creaturesubtype) do
			local aSubTypeSplit = StringManager.split(sSubType, " ", true);
			if #aSubTypeSplit > 0 then
				local bMatch = true;
				for i = 1, #aSubTypeSplit do
					if aSubTypeSplit[i] ~= aCheckSplit[k - 1 + i] then
						bMatch = false;
						break;
					end
				end
				if bMatch then
					table.insert(aSubTypeCheck, sSubType);
					k = k + (#aSubTypeSplit - 1);
				end
			end
		end
	end

	-- Make sure we have a default creature type (if requested)
	if bUseDefaultType then
		if #aTypeCheck == 0 then
			table.insert(aTypeCheck, DataCommon.creaturedefaulttype);
		end
	end

	-- Combine into a single list
	for _,vSubType in ipairs(aSubTypeCheck) do
		table.insert(aTypeCheck, vSubType);
	end

	return aTypeCheck;
end

function isCreatureType(rActor, sTypeCheck)
	local aTypeCheck = getCreatureTypeHelper(sTypeCheck, false);
	if #aTypeCheck == 0 then
		return false;
	end

	local sType, nodeActor = ActorManager.getTypeAndNode(rActor);
	local sField = "race";
	if sType ~= "pc" then
		sField = "type";
	end
	local aTypeActor = getCreatureTypeHelper(DB.getValue(nodeActor, sField, ""), true);

	local bReturn = false;
	for kCheck,vCheck in ipairs(aTypeCheck) do
		if StringManager.contains(aTypeActor, vCheck) then
			bReturn = true;
			break;
		end
	end
	return bReturn;
end




---------------
-- OTHER
---------------


function getClassLevel(nodeActor, sValue)
	local sClassName = DataCommon.class_valuetoname[sValue];
	if not sClassName then
		return 0;
	end
	sClassName = sClassName:lower();

	for _, vNode in pairs(DB.getChildren(nodeActor, "classes")) do
		if DB.getValue(vNode, "name", ""):lower() == sClassName then
			return DB.getValue(vNode, "level", 0);
		end
	end

	return 0;
end

function calculateMaxSpellLevel(rActor, sType, bRitual)
	if not rActor then
		return 0;
	end
	-- Get node and actor
	local nodeActor;
	if type(rActor) == "table" then
		_, nodeActor = ActorManager.getTypeAndNode(rActor);
	else
		nodeActor = rActor;
		rActor = ActorManager.resolveActor(nodeActor);
	end
	sType = DataCommon.abilities_translation[sType:lower()];
	local sCat, nLevel = getMagicCategoryAndLevel(rActor, sType);

	-- In rituals, if the level is too low, use the full character level
	if sCat == "" and bRitual then
		local nTotalLevel = DB.getValue(nodeActor, "level", 0);
		nLevel = math.max(nLevel, nTotalLevel * DataCommon.max_spell_mult["magic_category_weak"]);
	elseif sCat == "" then
		nLevel = -1;
	end

	return nLevel;
end

function getMagicCategoryAndLevel(rActor, sType)
	if not rActor or not sType then
		return "", 0;
	end
	-- Get node and actor
	local nodeActor;
	if type(rActor) == "table" then
		_, nodeActor = ActorManager.getTypeAndNode(rActor);
	else
		nodeActor = rActor;
		rActor = ActorManager.resolveActor(nodeActor);
	end
	sType = DataCommon.abilities_translation[sType:lower()];
	if not sType then
		return "", 0;
	end

	-- Get level by type of magic
	local nLevel = 0;
	local sCat = "";
	local nMult = 0;
	local nodeProfs = nodeActor.createChild("classes");
	for _,v in pairs(nodeProfs.getChildren()) do
		local sMagic = DB.getValue(v, "magic_type", "");
		if sMagic == sType then
			local sCatCurrent = DB.getValue(v, "magic_category", "");
			local nMultCurrent = DataCommon.max_spell_mult[sCatCurrent] or 1;
			if nMultCurrent > nMult then
				sCat = sCatCurrent;
				nMult = nMultCurrent;
			end
			local nProfLevel = DB.getValue(v, "level", 0);
			nLevel = math.max(nLevel, nProfLevel * nMultCurrent);
		end
	end

	return sCat, math.floor(nLevel);
end


function calculateSpellVigorCost(rActor, nLevel, sType, sTypeSpell)
	-- Rituals are easy
	if sTypeSpell and sTypeSpell == "power_ritual" then
		return 0;
	end

	-- Get node and actor
	local nodeActor;
	if type(rActor) == "table" then
		_, nodeActor = ActorManager.getTypeAndNode(rActor);
	else
		nodeActor = rActor;
		rActor = ActorManager.resolveActor(nodeActor);
	end
	if not nodeActor or not sType then
		return 0;
	end

	-- Get channeller category
	-- local nodeProfs = nodeActor.createChild("classes");
	-- local sCat = "magic_category_weak";
	-- local nCat = 2;
	-- for _,v in pairs(nodeProfs.getChildren()) do
	-- 	local sMagic = DB.getValue(v, "magic_type", "");
	-- 	if sMagic == sType then
	-- 		local sAux = DB.getValue(v, "magic_category", "");
	-- 		local nAux = DataCommon.initial_magic_stat[sAux];
	-- 		if nAux and nAux > nCat then
	-- 			sCat = sAux;
	-- 			nCat = nAux;
	-- 		end
	-- 	end
	-- end
	local sCat = getMagicCategoryAndLevel(rActor, sType);

	-- Get vigor reduction
	local nAptitude = getAptitudeCategory(rActor, DataCommon.aptitude["energy_absorption"]);
	local bRed = nAptitude > nLevel;
	-- local nLevelRed = 0;
	-- local bRed = false;
	-- nodeList = DB.createChild(nodeActor, "genericlist");
	-- for _,v in pairs(nodeList.getChildren()) do
	-- 	local sCurrent = DB.getValue(v, "name", "");
	-- 	local bCond = sCurrent:find(DataCommon.aptitude["energy_absorption"]);
	-- 	if bCond then
	-- 		nLevelRed = nLevelRed + DB.getValue(v, "category", 0);
	-- 		bRed = true;
	-- 	end
	-- end

	-- Calculate cost
	local nVigor = 0;
	if sCat ~= "" then
		if nLevel == 0 then
			nVigor = DataCommon.spell_level_0_cost[sCat];
			if bRed then
				nVigor = nVigor - 1;
			end
		else
			local nMult = DataCommon.spell_level_cost[sCat];
			if bRed then
				nMult = nMult - 1;
			end
			nVigor = nMult * nLevel;
		end
	end

	return nVigor;
end



function getSensitivity(rActor, sVars, rTarget, bIsDamage)
	if not rActor or not sVars then
		return 0, 0, 0, 1, 0;
	end

	-- Get node and actor
	nodeActor, rActor = getActorAndNode(rActor);

	-- Extract sheet info
	local nSens = 0;
	local nAbsorb = 0;
	local nInmune = 0;
	local sResist = "";
	local sInmune = "";
	local sAbsorb = "";
	local sVulnerable = "";
	if bIsDamage then
		sResist = DB.getValue(nodeActor, "sensitivity.damage.resist", ""):lower();
		sInmune = DB.getValue(nodeActor, "sensitivity.damage.inmune", ""):lower();
		sAbsorb = DB.getValue(nodeActor, "sensitivity.damage.absorb", ""):lower();
		sVulnerable = DB.getValue(nodeActor, "sensitivity.damage.vulnerable", ""):lower();
	else
		sResist = DB.getValue(nodeActor, "sensitivity.states.resist", ""):lower();
		sInmune = DB.getValue(nodeActor, "sensitivity.states.inmune", ""):lower();
		sAbsorb = DB.getValue(nodeActor, "sensitivity.states.absorb", ""):lower();
		sVulnerable = DB.getValue(nodeActor, "sensitivity.states.vulnerable", ""):lower();
	end

	-- Split sVars in different variables
	local aVarsTot = sVars;
	if type(aVarsTot) == "string" then
		aVarsTot = StringManager.split(sVars, ",", true);
	end
	-- if bIsDamage then
	-- 	table.insert(aVarsTot, DataCommon.all)
	-- end
	-- TODO: Manage exceptions
	-- Loop in vars
	for _,sVar in pairs(aVarsTot) do
		sVar = sVar:lower();
		local aVarsCap = {sVar, sVar:upper(), StringManager.capitalize(sVar)};

		-- Add value for each condition
		if string.find(sResist, sVar) and not string.find(sResist, "!" .. sVar) then
			local bNegate = false;
			for _,sVar2 in pairs(aVarsTot) do
				sVar2 = sVar2:lower();
				if sVar2 ~= sVar and string.find(sResist, "!" .. sVar2) then
					bNegate = true;
					break;
				end
			end
			if not bNegate then
				nSens = nSens + 1;
			end
		end

		if string.match(sInmune, sVar) and not string.match(sInmune, "!" .. sVar) then
			local bNegate = false;
			for _,sVar2 in pairs(aVarsTot) do
				sVar2 = sVar2:lower();
				if sVar2 ~= sVar and string.match(sInmune, "!" .. sVar2) then
					bNegate = true;
					break;
				end
			end
			if not bNegate then
				nInmune = nInmune + 1;
			end
		end

		if string.match(sAbsorb, sVar) and not string.match(sAbsorb, "!" .. sVar) then
			local bNegate = false;
			for _,sVar2 in pairs(aVarsTot) do
				sVar2 = sVar2:lower();
				if sVar2 ~= sVar and string.match(sAbsorb, "!" .. sVar2) then
					bNegate = true;
					break;
				end
			end
			if not bNegate then
				nAbsorb = nAbsorb + 1;
			end
		end

		if string.match(sVulnerable, sVar) and not string.match(sVulnerable, "!" .. sVar) then
			local bNegate = false;
			for _,sVar2 in pairs(aVarsTot) do
				sVar2 = sVar2:lower();
				if sVar2 ~= sVar and string.match(sVulnerable, "!" .. sVar2) then
					bNegate = true;
					break;
				end
			end
			if not bNegate then
				nSens = nSens - 1;
			end
		end

		-- Specific conditions
		if (sVar == DataCommon.pain or sVar == DataCommon.tiredness) and EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Maddened"]) then
			nInmune = nInmune + 1;
		end

		-- Effects
		local nEffect, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["RESIST"]}, true, aVarsCap, nil, nil, aVarsTot);
		nSens = nSens + nEffect;
		nEffect, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["VULN"]}, true, aVarsCap, nil, nil, aVarsTot);
		nSens = nSens - nEffect;
		nEffect, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ABSORB"]}, true, aVarsCap, nil, nil, aVarsTot);
		nAbsorb = nAbsorb + nEffect;
		nInmune = nInmune + EffectManagerSS.getNumberOfEffectsByType(rActor, DataCommon.keyword_states["INMUNE"], aVarsCap, nil, aVarsTot);

		-- Targetted effects
		if rTarget then
			nEffect, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["RESIST"]}, true, aVarsCap, rTarget, true, aVarsTot);
			nSens = nSens + nEffect;
			nEffect, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["VULN"]}, true, aVarsCap, rTarget, true, aVarsTot);
			nSens = nSens - nEffect;
			nEffect, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ABSORB"]}, true, aVarsCap, rTarget, true, aVarsTot);
			nAbsorb = nAbsorb + nEffect;
			nInmune = nInmune + EffectManagerSS.getNumberOfEffectsByType(rActor, DataCommon.keyword_states["INMUNE"], aVarsCap, rTarget, aVarsTot);
		end
	end

	-- Transform sensitivity condition sensitivity multiplier
	local nSensMult, nAdv = sensitivityCategoryToMultiplier(nSens, nInmune, nAbsorb);

	return nSens, nInmune, nAbsorb, nSensMult, nAdv;
end

function sensitivityCategoryToMultiplier(nSens, nInmune, nAbsorb)
	local nMult = 1;
	local nAdv = 0;
	if nAbsorb  > 0 then
		nAdv = 99;
		if nSens < 0 then
			nMult = -math.max(0, nAbsorb + math.min(0, nSens + nInmune)) * 0.5;
		else
			nMult = -nAbsorb * 0.5
		end
	elseif nInmune > 0 then
		nAdv = 99;
		nMult = 0;
	else
		nAdv = nSens;
		if nSens > 0 then
			nMult = 1 / (1 + nSens)
		else
			nMult = 1 - 0.5 * nSens
		end
	end

	return nMult, nAdv;
end


---------------
-- AUXILIAR
---------------

function advantage2passiveBonus(nAdv)
	local nBonus = 0;
	if nAdv == 1 then
		nBonus = nBonus + 3;
	elseif nAdv == 2 then
		nBonus = nBonus + 5;
	elseif nAdv == 3 then
		nBonus = nBonus + 6;
	elseif nAdv > 3 then
		nBonus = nBonus + 7;
	elseif nAdv == -1 then
		nBonus = nBonus - 3;
	elseif nAdv == -2 then
		nBonus = nBonus - 5;
	elseif nAdv == -3 then
		nBonus = nBonus - 6;
	elseif nAdv < -3 then
		nBonus = nBonus - 7;
	end
	return nBonus;
end

