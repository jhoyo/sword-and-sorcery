--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

SPELL_LEVELS = 10;


-------------------
-- POWER MANAGEMENT
-------------------

function resetPowers(nodeCaster, bLong)
	local aListGroups = {};

	-- Build list of power groups
	for _,vGroup in pairs(DB.getChildren(nodeCaster, "powergroup")) do
		local sGroup = DB.getValue(vGroup, "name", "");
		if not aListGroups[sGroup] then
			local rGroup = {};
			rGroup.sName = sGroup;
			rGroup.sType = DB.getValue(vGroup, "castertype", "");
			rGroup.nUses = DB.getValue(vGroup, "uses", 0);
			rGroup.sUsesPeriod = DB.getValue(vGroup, "usesperiod", "");
			rGroup.nodeGroup = vGroup;

			aListGroups[sGroup] = rGroup;
		end
	end

	-- Reset power usage
	for _,vPower in pairs(DB.getChildren(nodeCaster, "powers")) do
		local bReset = true;

		local sGroup = DB.getValue(vPower, "group", "");
		local rGroup = aListGroups[sGroup];
		local bCaster = (rGroup and rGroup.sType ~= "");

		if not bCaster then
			if rGroup and (rGroup.nUses > 0) then
				if rGroup.sUsesPeriod == "once" then
					bReset = false;
				elseif not bLong and rGroup.sUsesPeriod ~= "enc" then
					bReset = false;
				end
			else
				local sPowerUsesPeriod = DB.getValue(vPower, "usesperiod", "");
				if sPowerUsesPeriod == "once" then
					bReset = false;
				elseif not bLong and sPowerUsesPeriod ~= "enc" then
					bReset = false;
				end
			end
		end

		if bReset then
			DB.setValue(vPower, "cast", "number", 0);
		end
	end

	-- Reset spell slots
	for i = 1, SPELL_LEVELS do
		DB.setValue(nodeCaster, "powermeta.pactmagicslots" .. i .. ".used", "number", 0);
	end
	if bLong then
		for i = 1, SPELL_LEVELS do
			DB.setValue(nodeCaster, "powermeta.spellslots" .. i .. ".used", "number", 0);
		end
	end
end

function addPower(sClass, nodeSource, nodeCreature, sGroup)
	-- Validate
	if not nodeSource or not nodeCreature then
		return nil;
	end

	-- Create the powers list entry
	local nodePowers = nodeCreature.createChild("powers");
	if not nodePowers then
		return nil;
	end

	-- Create the new power entry
	local nodeNewPower = nodePowers.createChild();
	if not nodeNewPower then
		return nil;
	end

	-- Copy the power details over
	DB.copyNode(nodeSource, nodeNewPower);

	-- Determine group setting
	if sGroup then
		DB.setValue(nodeNewPower, "group", "string", sGroup);
	end

	-- Class specific handling
	if sClass == "reference_spell" or sClass == "power" then
		-- DEPRECATED: Used to add spell slot of matching spell level, but deprecated since handled by class drops and doesn't work for warlock
	else
		-- Remove level data
		DB.deleteChild(nodeNewPower, "level");

		-- Copy text to description
		local nodeText = nodeNewPower.getChild("text");
		if nodeText then
			local nodeDesc = nodeNewPower.createChild("description", "formattedtext");
			DB.copyNode(nodeText, nodeDesc);
			nodeText.delete();
		end
	end

	-- Set locked state for editing detailed record
	DB.setValue(nodeNewPower, "locked", "number", 1);

	-- Parse power details to create actions
	-- if DB.getChildCount(nodeNewPower, "actions") == 0 then
	-- 	parsePCPower(nodeNewPower);
	-- end

	-- If PC, then make sure all spells are visible
	if ActorManager.isPC(nodeCreature) then
		DB.setValue(nodeCreature, "powermode", "string", "standard");
	end

	return nodeNewPower;
end

-------------------------
-- POWER ACTION DISPLAY
-------------------------

function getPCPowerActionOutputOrder(nodeAction)
	if not nodeAction then
		return 1;
	end
	local nodeActionList = nodeAction.getParent();
	if not nodeActionList then
		return 1;
	end

	-- First, pull some ability attributes
	local sType = DB.getValue(nodeAction, "type", "");
	local nOrder = DB.getValue(nodeAction, "order", 0);

	-- Iterate through list node
	local nOutputOrder = 1;
	for k, v in pairs(nodeActionList.getChildren()) do
		if DB.getValue(v, "type", "") == sType then
			if DB.getValue(v, "order", 0) < nOrder then
				nOutputOrder = nOutputOrder + 1;
			end
		end
	end

	return nOutputOrder;
end

function getPCPowerAction(nodeAction, sSubRoll, rTarget)
	if not nodeAction then
		return;
	end
	local bMultReturn = false;
	local rActor = ActorManager.resolveActor(nodeAction.getChild("....."));

	local rAction = {};
	rAction.type = DB.getValue(nodeAction, "type", "");
	rAction.label = StringManager.trim(DB.getValue(nodeAction, "...name", ""));
	rAction.level = DB.getValue(nodeAction, "parameter", 0);
	rAction.order = 1;

	-- Default stats just in case
	local sGroupStat = "";
	local sGroupSkill = "";
	if rActor then
		local aPowerGroup = getPowerGroupRecord(rActor, nodeAction.getChild('...'));

		if aPowerGroup then
			sGroupStat = aPowerGroup.groupstat;
			sGroupSkill = aPowerGroup.groupskill;
		end
	end

	-- Attack
	if rAction.type == "attack" then

		local sAttackType = DB.getValue(nodeAction, "atkack_type", "");
		if sAttackType ~= "" then
			if sAttackType == "melee" then
				rAction.type_attack = 0;
			else
				rAction.type_attack = 1;
			end
		end
		rAction.attack_stat = StringManager.trim(DB.getValue(nodeAction, "attack_stat", "")):lower();
		rAction.attack_skill = DB.getValue(nodeAction, "attack_skill", "");
		rAction.modifier = DB.getValue(nodeAction, "attack_rollmod", "");
		rAction.height = DB.getValue(nodeAction, "heightening_attack", 0);

		-- Use base values
		if rAction.attack_stat == DataCommon.base then
			rAction.attack_stat = sGroupStat;
		end
		if rAction.attack_skill:lower() == DataCommon.base then
			rAction.attack_skill = sGroupSkill;
		end

		-- Common param
		rAction.hand = 0;
		rAction.ability = rAction.attack_stat;
		rAction.skill = rAction.attack_skill;
		rAction.range = DB.getValue(nodeAction, "range", 0);
		rAction.bWeapon = false;

		-- "Weapon" properties
		rAction.sProperties = DB.getValue(nodeAction, "properties", "");
		local nodeSpell = nodeAction.getChild("...");
		local sKeywords = getPowerKeywords(nodeSpell):lower();
		if sKeywords:find(DataCommon.magic_keywords["electric"]) then
			if rAction.sProperties == "" then
				rAction.sProperties = DataCommon.weapon_keywords["electric"]
			else
				rAction.sProperties = rAction.sProperties .. ", " .. DataCommon.weapon_keywords["electric"];
			end
		end

		rAction = ItemManager2.addPropertiesToAction(rAction, rAction.sProperties);

	-- Skill
	elseif rAction.type == "skill" then
		rAction.type = "skill";
		rAction.stat = StringManager.trim(DB.getValue(nodeAction, "skill_stat", ""));
		rAction.skill = DB.getValue(nodeAction, "skill_skill", "");
		rAction.mod = DB.getValue(nodeAction, "skill_rollmod", "");
		rAction.height = DB.getValue(nodeAction, "heightening_skill", 0);

		rAction.sType = DB.getValue(nodeAction, "skill_type", "");
		if rAction.sType == 'power_label_skill_difficulty' then
			rAction.dif = DB.getValue(nodeAction, "skill_dif", 10);
		elseif rAction.sType == 'power_label_skill_opposed' then
			rAction.opposed_stat = StringManager.trim(DB.getValue(nodeAction, "skill_opposed_stat", "")):lower();
			rAction.opposed_skill = DB.getValue(nodeAction, "skill_opposed_skill", "");
		end

		-- Default values
		if (rAction.stat or ""):lower() == DataCommon.base then
			rAction.stat = sGroupStat;
		end
		if (rAction.skill or ""):lower() == DataCommon.base then
			rAction.skill = sGroupSkill;
		end
		if (rAction.opposed_stat or ""):lower() == DataCommon.base then
			rAction.stat = sGroupStat;
		end
		if (rAction.opposed_skill or ""):lower() == DataCommon.base then
			rAction.skill = sGroupSkill;
		end

	-- SAVE
	elseif rAction.type == "save" then
		local nodeSpell = nodeAction.getChild("...");
		rAction.school = StringManager.trim(DB.getValue(nodeSpell, "school", ""));
		-- Save
		rAction.save_stat = StringManager.trim(DB.getValue(nodeAction, "save_stat", "")):lower();
		rAction.save_skill = DB.getValue(nodeAction, "save_skill", "");

		rAction.dif_stat = StringManager.trim(DB.getValue(nodeAction, "dif_stat", "")):lower();
		rAction.dif_skill = DB.getValue(nodeAction, "dif_skill", "");
		rAction.dif_mod = DB.getValue(nodeAction, "dif_mod", 0);

		rAction.keywords = getPowerKeywords(nodeSpell);

		rAction.save_effect = DB.getValue(nodeAction, "save_effect", "");

		-- Use base values
		if (rAction.dif_stat or ""):lower() == DataCommon.base then
			rAction.dif_stat = sGroupStat;
		end
		if (rAction.dif_skill or ""):lower() == DataCommon.base then
			rAction.dif_skill = sGroupSkill;
		end

	-- Cast
	elseif rAction.type == "cast" then

		-- Basic level and params
		local nodeSpell = nodeAction.getChild("...");
		rAction.min_level = DB.getValue(nodeSpell, "level", 0);
		rAction.school = StringManager.trim(DB.getValue(nodeSpell, "school", ""));
		if Input.isShiftPressed() then
			rAction.actions = "";
		else
			rAction.actions = DB.getValue(nodeSpell, "castingtime", ""):lower();
		end
		rAction.level = DB.getValue(nodeAction, "parameter", 0) + rAction.min_level;
		rAction.nConc, rAction.nDur, rAction.nActEffects = getConcDuration(nodeSpell);
		rAction.keywords = DB.getValue(nodeSpell, "keywords", ""):lower();
		rAction.type_spell = DB.getValue(nodeSpell, "type_spell", ""):lower();
		rAction.sVigor = DB.getValue(nodeSpell, "vigor_cost", "");
		rAction.sComp = DB.getValue(nodeSpell, "components", "");
		rAction.essence_cost = DB.getValue(nodeSpell, "essence_cost", 0);
		rAction.essence_target = Interface.getString(DB.getValue(nodeSpell, "essence_target", ""));
		rAction.elements = DB.getValue(nodeSpell, "elements", "");

		rAction.stat = StringManager.trim(DB.getValue(nodeAction, "stat", ""));
		rAction.skill = DB.getValue(nodeAction, "skill", "");
		rAction.range = DB.getValue(nodeSpell, "range", "");

		-- Techniques info
		rAction.overchannel = 0;
		rAction.increased_dif = 0;
		rAction.increased_vigor = 0;
		rAction.increased_unstable = 0;
		rAction.increased_actions = 0;
		rAction.disadv_test = 0;
		rAction.saved_comp = 0;
		rAction.list_techniques = {};

		local nodeListTech = DB.getChild(nodeAction, ".....listmagic");
		if nodeListTech then
			for _,v in pairs(nodeListTech.getChildren()) do
				local nActive = DB.getValue(v, "active", 0);
				local nActiveCat = DB.getValue(v, "active_cat", 0);
				local sName = DB.getValue(v, "name", "");
				if nActive > 0 then
					table.insert(rAction.list_techniques, sName);
					local nAux = DB.getValue(v, "vigor_mod", 0);
					rAction.increased_vigor = rAction.increased_vigor + nAux;
					nAux = DB.getValue(v, "dif_mod", 0);
					rAction.increased_dif = rAction.increased_dif + nAux;
					nAux = DB.getValue(v, "unstable_mod", 0);
					rAction.increased_unstable = rAction.increased_unstable + nAux;
					nAux = DB.getValue(v, "actions_mod", 0);
					rAction.increased_actions = rAction.increased_actions + nAux;
					nAux = DB.getValue(v, "disadvantage", 0);
					rAction.disadv_test = rAction.disadv_test + nAux;

					if sName:find(DataCommon.techniques["blood_magic"]) then
						if rAction.keywords == "" then
							rAction.keywords = rAction.keywords .. ", "
						end
						rAction.keywords = rAction.keywords .. StringManager.capitalize(DataCommon.techniques["blood_magic"]);
					end
					
				elseif nActiveCat > 0 then
					table.insert(rAction.list_techniques, sName .. " " .. tostring(nActiveCat));
					local nAux = DB.getValue(v, "vigor_mod", 0);
					local nPL = DB.getValue(v, "vigor_mod_aux", 0);
					if nPL > 0 then
						nAux = nAux * nActiveCat;
					end
					rAction.increased_vigor = rAction.increased_vigor + nAux;
					nAux = DB.getValue(v, "dif_mod", 0);
					nPL = DB.getValue(v, "dif_mod_aux", 0);
					if nPL > 0 then
						nAux = nAux * nActiveCat;
					end
					rAction.increased_dif = rAction.increased_dif + nAux;
					nAux = DB.getValue(v, "unstable_mod", 0);
					nPL = DB.getValue(v, "unstable_mod_aux", 0);
					if nPL > 0 then
						nAux = nAux * nActiveCat;
					end
					rAction.increased_unstable = rAction.increased_unstable + nAux;
					nAux = DB.getValue(v, "actions_mod", 0);
					rAction.increased_actions = rAction.increased_actions + nAux;
					nAux = DB.getValue(v, "disadvantage", 0);
					rAction.disadv_test = rAction.disadv_test + nAux;

					-- Special
					if sName == DataCommon.techniques["overchannel"] then
						local nProf = ActorManager2.getLevel(rActor, 2);
						rAction.overchannel = math.min(nProf, nActiveCat);
					elseif sName == DataCommon.techniques["components"] then
						rAction.saved_comp = nActiveCat;
					end

				end
			end
		end

		-- Ritual: Add extra vigor to difficulty
		if (rAction.type_spell == "power_ritual") and (rAction.increased_vigor > 0) then
			rAction.increased_vigor = math.max(0, rAction.increased_vigor - ActorManager2.getAptitudeCategory(DataCommon.aptitude["energy_absorption"]));
			rAction.increased_dif = rAction.increased_dif + rAction.increased_vigor;
			rAction.increased_vigor = 0;
		end

		-- Group default params
		if rAction.stat:lower() == DataCommon.base then
			rAction.stat = sGroupStat;
		end
		if rAction.skill:lower() == DataCommon.base then
			rAction.skill = sGroupSkill;
		end
		if aPowerGroup then
			rAction.magictype = aPowerGroup.magictype;
		else
			rAction.magictype = rAction.stat;
		end

		-- -- Array of levels
		-- if aPowerGroup then
		-- 	-- TODO
		-- end

	-- Affliction
	elseif rAction.type == "affliction" then
		rAction.path = DB.getValue(nodeAction, "link_identifier", "");
		local nodeAf = DB.findNode(rAction.path);
		if nodeAf then
			rAction.data = true;
			rAction.nameAffliction = DB.getValue(nodeAf, "name", "");
			rAction.typeAffliction = DB.getValue(nodeAf, "cycler", "");
			rAction.contagion = DB.getValue(nodeAf, "contagion", "");
			rAction.incubation = DB.getValue(nodeAf, "incubation", "");
			rAction.advance = DB.getValue(nodeAf, "advance", "");
			rAction.recovery = DB.getValue(nodeAf, "recovery", "");
			rAction.peligrosity = DB.getValue(nodeAf, "peligrosity", 12);
			rAction.initial_cat = DB.getValue(nodeAf, "initial_cat", 1);
			rAction.keywords = DB.getValue(nodeAf, "keywords", "");
			rAction.category = DB.getValue(nodeAf, "category", "");
			rAction.rarity = DB.getValue(nodeAf, "rarity", "");
			rAction.supplies = DB.getValue(nodeAf, "supplies", "");
			rAction.delivery = DB.getValue(nodeAf, "delivery", "");
			rAction.contagion = DB.getValue(nodeAf, "contagion", "");
			rAction.inmunity = DB.getValue(nodeAf, "inmunity", "");
			rAction.end_effect = DB.getValue(nodeAf, "end_effect", "");

		end
		

	-- Damage
	elseif rAction.type == "damage" then

		rAction.clauses = {};
		rAction.description = DB.getValue(nodeAction, "description", "");
		local aDamageNodes = UtilityManager.getSortedTable(DB.getChildren(nodeAction, "damagelist"));
		for _,v in ipairs(aDamageNodes) do
			local sAbility = StringManager.trim(DB.getValue(v, "stat", "")):lower();
			if sAbility:lower() == DataCommon.base then
				sAbility = sGroupStat;
			end
			local nMult = DB.getValue(v, "statmult", 1);
			local sRoll = DB.getValue(v, "roll", "");
			local nHeight = DB.getValue(v, "heightening", 0);
			local sDmgType = DB.getValue(v, "type", "");

			table.insert(rAction.clauses, { roll = sRoll, stat = sAbility, statmult = nMult, height = nHeight, dmgtype = sDmgType });
		end
		rAction.saveModify = true;

	elseif rAction.type == "heal" then
		rAction.type = "heal";
		rAction.sTargeting = DB.getValue(nodeAction, "healtargeting", "");
		rAction.description = DB.getValue(nodeAction, "description", "");
		rAction.sSubtype = DB.getValue(nodeAction, "healtype", "");
		rAction.heal_stat = StringManager.trim(DB.getValue(nodeAction, "healstat", "")):lower();
		rAction.corruption_type = StringManager.trim(DB.getValue(nodeAction, "corruption_type", "")):lower();
		rAction.bMagic = true; -- TODO: may be some exceptions
		rAction.nTemporal = 0;
		if rAction.sSubtype == "power_label_heal_vit" or rAction.sSubtype == "power_label_heal_vigor" then
			rAction.nTemporal = DB.getValue(nodeAction, "temporal", 0);
		end

		rAction.nMajorHealing = 0;
		local bCond = rAction.sSubtype == "power_label_heal_essence" or rAction.sSubtype == "power_label_heal_vigor";
		if not bCond then
			rAction.nMajorHealing = DB.getValue(nodeAction, "true_healing", 0);
		end

		local nodeSpell = nodeAction.getChild("...");
		local sKeywords = getPowerKeywords(nodeSpell):lower();
		rAction.sTypeHealing = "";
		if rAction.nMajorHealing > 0 then
			rAction.sTypeHealing = DataCommon.magic_keywords["major_heal"];
		elseif not bCond then
			rAction.sTypeHealing = DataCommon.magic_keywords["minor_heal"];
		end

		rAction.sAbility = StringManager.trim(DB.getValue(nodeAction, "stat", "")):lower();
		rAction.sRoll = DB.getValue(nodeAction, "roll", "");
		rAction.nMult = DB.getValue(nodeAction, "statmult", 1);
		rAction.nHeight = DB.getValue(nodeAction, "heightening", 0);
		rAction.nHiddenSpell = sKeywords:find(DataCommon.magic_keywords["hidden"]);

		-- Use base values
		if (rAction.sAbility or "") == DataCommon.base then
			rAction.sAbility = sGroupStat;
		end


	-- Effect
	elseif rAction.type == "effect" then
		-- Info
		rAction.sName = DB.getValue(nodeAction, "label", "");
		rAction.sApply = DB.getValue(nodeAction, "apply", "");
		rAction.sTargeting = DB.getValue(nodeAction, "targeting", "");
		rAction.nDuration = DB.getValue(nodeAction, "durmod", 0);
		rAction.sUnits = DB.getValue(nodeAction, "durunit", "");
		rAction.sTypeDuration = DB.getValue(nodeAction, "durtype", "");
		rAction.nHeight = DB.getValue(nodeAction, "parameter", 0);
		rAction.nProcessDice = DB.getValue(nodeAction, "process_dice", 0);
		rAction.stat = "";
		rAction.skill = "";
		-- Spells need some info of the parent power
		local nodeSpell = nodeAction.getChild("...");
		rAction.sPowerName = DB.getValue(nodeSpell, "name", "");
		if rActor then
			local aPowerGroup = getPowerGroupRecord(rActor, nodeSpell);
			if aPowerGroup then
				rAction.stat = aPowerGroup.groupstat;
				rAction.skill = aPowerGroup.groupskill;
			end
		end
		rAction.nDurLabel, rAction.sDurLabel = getConcDuration(nodeSpell, true);
		rAction.sTypeSpell = DB.getValue(nodeSpell, "reftype", "");
		local sKeywords = DB.getValue(nodeSpell, "keywords", ""):lower();
		rAction.bHiddenSpell = sKeywords:find(DataCommon.magic_keywords["hidden"]);
		if rAction.nDuration == 0 and rAction.sTypeDuration ~= "" then
			_, rAction.nDur, _ = getConcDuration(nodeSpell);
			rAction.nDuration, rAction.sUnits = getConcDuration(nodeSpell, true);
		end
		if rAction.nDuration > 0 and not rAction.nDur then
			rAction.nDur =  rAction.nDuration * (DataCommon.turns_per_unit[rAction.sUnits] or 1);
		end
		-- Effect cost
		rAction.nApplyCost = DB.getValue(nodeAction, "cost_apply", 0);
		rAction.sCostType = DB.getValue(nodeAction, "cost_type", "");
		rAction.sCostResource = DB.getValue(nodeAction, "cost_resource", "");
		rAction.nCostInitial = DB.getValue(nodeAction, "cost_initial", 0);
		if rAction.sCostType == "" then
			rAction.nCost = DB.getValue(nodeAction, "cost_number", 0);
		else
			local nLevel = ActionSkill.getCastSpellLevel(rActor);
			local sSpellType = DB.getValue(nodeSpell, "type_spell", "");
			rAction.nCost = ActorManager2.calculateSpellVigorCost(rActor, nLevel, sGroupStat, sTypeSpell);
		end
		
		-- Each type of power has different needs
		rAction.bConc = rAction.sTypeDuration == "effect_duration_conc";
		rAction.bRitual = rAction.sTypeDuration == "effect_duration_oncast";		
		rAction.bRandomTied = rAction.sTypeDuration == "effect_duration_autotie";

		-- Spell level
		rAction.min_level = DB.getValue(nodeSpell, "level", 0);
		rAction.level_cast = getSpellLevel(nodeSpell);
		rAction.bConc = rAction.sTypeDuration == "effect_duration_conc";
		rAction.bRitual = rAction.sTypeDuration == "effect_duration_oncast";		
		rAction.bRandomTied = rAction.sTypeDuration == "effect_duration_autotie";

		-- Abilities
		rAction.bStyle = sKeywords:find(DataCommon.magic_keywords["style"]);

		-- Get if there is a save which modifies duration
		local nodeSpell = nodeAction.getChild("...");
		local nodeActions = nodeSpell.getChild("actions");
		for _,nodeActionChild in pairs(nodeActions.getChildren()) do
			if DB.getValue(nodeActionChild, "type", "") == "save" then
				local save_effect = DB.getValue(nodeActionChild, "save_effect", "");
				if (save_effect == "power_mod_duration") or (save_effect == "power_mod_durdamage") then
					rAction.save_stat = StringManager.trim(DB.getValue(nodeActionChild, "save_stat", "")):lower();
					rAction.save_skill = DB.getValue(nodeActionChild, "save_skill", "");

					rAction.dif_stat = StringManager.trim(DB.getValue(nodeActionChild, "dif_stat", "")):lower();
					rAction.dif_skill = DB.getValue(nodeActionChild, "dif_skill", "");
					rAction.dif_mod = DB.getValue(nodeActionChild, "dif_mod", 0);
					rAction.keywords = getPowerKeywords(nodeSpell);

					-- TODO: Add techniques that increase difficulty					

					-- Use base values
					if rActor then
						local aPowerGroup = getPowerGroupRecord(rActor, nodeAction.getChild('...'));

						if aPowerGroup and (rAction.dif_stat or ""):lower() == DataCommon.base then
							rAction.dif_stat = aPowerGroup.groupstat;
						end
						if aPowerGroup and (rAction.dif_skill or ""):lower() == DataCommon.base then
							rAction.dif_skill = aPowerGroup.groupskill;
						end
					end
				end
			end
		end


	-- Summon
	elseif rAction.type == "summon" then
		local nodeSpell = nodeAction.getChild("...");
		rAction.description = DB.getValue(nodeAction, "description", "");
		rAction.sName = DB.getValue(nodeAction, "name", "");
		rAction.number = DB.getValue(nodeAction, "number", 1);
		if rActor and rActor["sType"] == "charsheet" then
			rAction.sFaction = "friend";
		end
		rAction.min_level = DB.getValue(nodeSpell, "level", 0);
		rAction.level_cast = getSpellLevel(nodeSpell);
		rAction.nConc, rAction.nDur, _ = getConcDuration(nodeSpell);
		rAction.stat = "";
		rAction.skill = "";
		if rActor then
			local aPowerGroup = getPowerGroupRecord(rActor, nodeSpell);
			if aPowerGroup then
				rAction.stat = aPowerGroup.groupstat;
				rAction.skill = aPowerGroup.groupskill;
			end
		end
		local sKeywords = DB.getValue(nodeAction, "keywords", ""):lower();
		rAction.bHiddenSpell = sKeywords:find(DataCommon.keyword_inactive["hidden"]);

		-- Get all summons
		rAction.summons = {};
		for _, v in pairs(nodeAction.createChild("summonlist").getChildren()) do
			local sSummonName = DB.getValue(v, "link_label", "");
			local sSummonClass = DB.getValue(v, "link_class", "");
			local sSummonIdentifier = DB.getValue(v, "link_identifier", "");
			local nSummonIndex = DB.getValue(v, "index", 0);
			rAction.summons[nSummonIndex] = {sName = sSummonName, sClass = sSummonClass, sIdentifier = sSummonIdentifier};
		end

	elseif rAction.type == "roll" then
		rActions = {};
		local aRolls = UtilityManager.getSortedTable(DB.getChildren(nodeAction, "rolllist"));
		for k,v in ipairs(aRolls) do
			local rAction3 = {};
			rAction3.level = DB.getValue(nodeAction, "parameter", 0);

			local sAbility = StringManager.trim(DB.getValue(v, "stat", "")):lower();
			local sRoll = DB.getValue(v, "roll", "");
			local nMult = DB.getValue(v, "statmult", 1);
			local nHeight = DB.getValue(v, "heightening", 0);
			rAction3.clauses = { { roll = sRoll, stat = sAbility, statmult = nMult, height = nHeight } };

			rAction3.type = DB.getValue(nodeAction, "type", "");
			rAction3.sName = DB.getValue(v, "label", "");
			rAction3.label = DB.getValue(nodeAction, "...name", "") .. " (" .. rAction3.sName ..")"
			rAction3.order = k;
			if sRoll ~= "" then
				table.insert(rActions, rAction3);
			end
		end

		rAction = rActions;
		bMultReturn = true;

	elseif rAction.type == "cost" then
		rAction.sActions = DB.getValue(nodeAction, "actions", "");
		rAction.sCost = DB.getValue(nodeAction, "cost", "");
	end

	return rAction, rActor, bMultReturn;
end

function performPCPowerAction(draginfo, nodeAction)
	local rAction, rActor, bMultReturn = getPCPowerAction(nodeAction);
	if rAction and bMultReturn then
		for _, rA in pairs(rAction) do
			performAction(draginfo, rActor, rA, nodeAction.getChild("..."));
		end

	elseif rAction then
		performAction(draginfo, rActor, rAction, nodeAction.getChild("..."));
	end
end

function getPCPowerAttackActionText(nodeAction)
	local rAction, rActor = PowerManager.getPCPowerAction(nodeAction);
	local sAttack = "";

	if rAction then

		-- Attack label
		if (rAction.attack_stat or "") ~= "" then
			sAttack = DataCommon.ability_ltos[rAction.attack_stat] or "";
			bAddPlus = true;
		end
		if (rAction.attack_skill or "") ~= "" then
			if bAddPlus then
				sAttack = sAttack .. " + ";
			end
			sAttack = sAttack .. rAction.attack_skill;
			bAddPlus = true;
		end
		if (rAction.modifier or "") ~= "" then
			sAttack = sAttack .. " + " .. rAction.modifier;
			if (rAction.height or 0) ~= 0 then
				if rAction.height > 0 then
					sAttack = sAttack .. " (+) ";
				else
					sAttack = sAttack .. " (-) ";
				end
				sAttack = sAttack .. tostring(rAction.height) .. " x " .. Interface.getString("power_label_heightening");
			end
		end
		if rAction.type_attack then
			local sRange = "("
			if rAction.type_attack == 1 then
				sRange = sRange .. StringManager.capitalize(DataCommon.rangetypes_inv["ranged"]);
			else
				sRange = sRange .. StringManager.capitalize(DataCommon.rangetypes_inv["melee"]);
			end
			local bRanged = (rAction.type_attack == 1) and (rAction.range > 0);
			local bMelee = (rAction.type_attack == 0) and (rAction.range > 1);
			if bMelee then
				sRange = sRange .. " " .. rAction.range .. " m) ";
			elseif bRanged then
				sRange = sRange .. " incr. " .. rAction.range .. " m) ";
			else
				sRange = sRange .. ") ";
			end
			sAttack = sRange .. sAttack;
		end
		if rAction.sProperties ~= "" then
			sAttack = sAttack .. " (" .. rAction.sProperties ..")";
		end

	end

	return sAttack;
end

function getPCPowerSkillActionText(nodeAction)
	local rAction, rActor = PowerManager.getPCPowerAction(nodeAction);
	local sSkill = "";
	if rAction and rAction.stat ~= '' then
		sSkill = DataCommon.ability_ltos[(rAction.stat or ""):lower()] or "";
		if rAction.skill ~= '' then
			sSkill = sSkill .. ' + ' .. rAction.skill;
		end
		if (rAction.mod or "") ~= "" then
			sSkill = sSkill .. " + " .. rAction.mod;
		end
		if (rAction.height or 0) ~= 0 then
			if rAction.height > 0 then
			sSkill = sSkill .. " (+) ";
			else
			sSkill = sSkill .. " (-) ";
			end
			sSkill = sSkill .. tostring(rAction.height) .. " x " .. Interface.getString("power_label_heightening");
		end
		if rAction.sType == 'power_label_skill_difficulty' then
			sSkill = sSkill .. ' VS ' .. rAction.dif;
		elseif rAction.sType == 'power_label_skill_opposed' then
			sSkill = sSkill .. ' VS ' .. (DataCommon.ability_ltos[(rAction.opposed_stat or ""):lower()] or "");
			if rAction.opposed_skill ~= '' then
				sSkill = sSkill .. ' + ' .. rAction.opposed_skill;
			end
		end
	end

	return sSkill;
end

function getPCPowerSaveActionText(nodeAction)
	local sSave = "";
	local sDif = "";
	local bAddPlus = false;

	local rAction, rActor, _ = PowerManager.getPCPowerAction(nodeAction);
	if rAction then
		-- evalAction(rActor, nodeAction.getChild("..."), rAction);

		-- Save label
		bAddPlus = false;
		if (rAction.save_stat or "") ~= "" then
			sSave = DataCommon.ability_ltos[rAction.save_stat] or "";
			bAddPlus = true;
		end
		if (rAction.save_skill or "") ~= "" then
			if bAddPlus then
				sSave = sSave .. " + ";
			end
			sSave = sSave .. rAction.save_skill;
			bAddPlus = true;
		end
		if (rAction.save_mod or 0) ~= 0 then
			if bAddPlus and rAction.save_mod > 0 then
				sSave = sSave .. " + ";
			elseif bAddPlus and rAction.save_mod < 0 then
				sSave = sSave .. " - ";
			end
			sSave = sSave .. math.abs(rAction.save_mod);
		end

		-- Difficulty label
		bAddPlus = false;
		if (rAction.dif_stat or "") ~= "" then
			sDif = DataCommon.ability_ltos[rAction.dif_stat] or "";
			bAddPlus = true;
		end
		if (rAction.dif_skill or "") ~= "" then
			if bAddPlus then
				sDif = sDif .. " + ";
			end
			sDif = sDif .. rAction.dif_skill;
			bAddPlus = true;
		end
		if (rAction.dif_mod or 0) ~= 0 then
			if bAddPlus and rAction.dif_mod > 0 then
				sDif = sDif .. " + ";
			elseif bAddPlus and rAction.dif_mod < 0 then
				sDif = sDif .. " - ";
			end
			sDif = sDif .. math.abs(rAction.dif_mod);
		end

		-- Save effect
		local sEffect = "";
		if (rAction.save_effect or "") ~= "" then
			if rAction.save_effect == "power_mod_damage" or rAction.save_effect == "power_mod_durdamage" then
				sEffect = " [Mod. damage]";
			end
				if rAction.save_effect == "power_mod_duration" or rAction.save_effect == "power_mod_durdamage" then
					sEffect = sEffect .. " [Mod. duration]";
				end
		end
		if rAction.keywords ~= "" then
			sEffect = sEffect .. " (" .. rAction.keywords .. ")";
		end

		-- Merge
		if (sSave ~= "") and (sDif ~= "") then
			sSave = sSave .. " VS " .. sDif;
		end
		if (sSave ~= "") and (sEffect ~= "") then
			sSave = sSave .. sEffect;
		end

	end

	return sSave;
end


function getPCPowerCastActionText(nodeAction)
	local rAction, rActor, _ = PowerManager.getPCPowerAction(nodeAction);
	local sEffects = "";
	local sDif = "";
	local sTech = "";
	local sRange = "";

	if rActor and rAction then
		-- Focus
		local aFilter = {rAction.label:lower()}
		if rAction.school and rAction.school ~= "" then
			table.insert(aFilter, rAction.school:lower())
		end
		if rAction.stat and rAction.stat ~= "" then
			table.insert(aFilter, rAction.stat:lower());
		end
		local nFocus, _ = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["FOCUS"]}, true, aFilter);
		local nEffects = WindsManager.getCurrentWinds();
		nEffects = nEffects + rAction.overchannel + nFocus;
		if nEffects > 0 then
			sEffects = string.format(Interface.getString("power_label_increased_effects"), nEffects);
		elseif nEffects < 0 then
			sEffects = string.format(Interface.getString("power_label_decreased_effects"), math.abs(nEffects));
		end

		-- Difficulty
		local aProps = ActionCast.scanKeywords(rAction.keywords);
		local nMaxLevelChar = ActorManager2.calculateMaxSpellLevel(rActor, rAction.stat, rAction.type_spell == "power_ritual");
		local cond1 = ActorManager2.getPenalizer(rActor) < 0;
		local cond2 = aProps["demanding"];
		local cond3 = rAction.increased_dif > 0 or rAction.overchannel > 0 or rAction.saved_comp > 0;
		if nMaxLevelChar < rAction.level then
			sDif = Interface.getString("roll_cast_autofail_level");
		elseif rAction.level + nEffects < rAction.min_level then 
			sDif =  Interface.getString("roll_cast_autofail");
		elseif not (cond1 or cond2 or cond3) then
			sDif =  Interface.getString("autosuccess");
		else
			local nDif = DataCommon.spell_cast_DC_base + rAction.level + rAction.increased_dif;
			sDif = "[Dif. " .. tostring(nDif) .. "]"
		end

		-- Range
		if rAction.range ~= "" then
			sRange = " (" .. rAction.range .. ")";
		end



		-- Techniques
		sTech = Utilities.mergeStrings(rAction.list_techniques);
		if sTech ~= "" then
			sTech = " [" .. sTech .. "]";
		end
	end

	return sDif .. sRange .. sEffects .. sTech;
end

function getPCPowerRollActionText(nodeAction)
	local rActions, rActor, _ = PowerManager.getPCPowerAction(nodeAction);
	local sString = "";
	local nActions = #rActions;

	for k, rAction in pairs(rActions) do
		local sRoll = Utilities.clauses_2_string(rAction);
		if rAction.sName then
			sString = sString .. rAction.sName .. ": " .. sRoll;
		elseif rAction.sRoll then
			sString = sString .. sRoll;
		end
		if k < nActions then
			sString = sString .. "; ";
		end
	end

	return sString;
end

function getPCPowerSummonActionText(nodeAction)
	local rAction, rActor, _ = PowerManager.getPCPowerAction(nodeAction);
	local sString = rAction.description or "";

	for k, v in pairs(rAction.summons) do
		if sString ~= "" then
			sString = sString .. "; ";
		end
		sString = sString .. tostring(k) .. ": " .. v["sName"];
	end

	return sString;
end

function getPCPowerDamageActionText(nodeAction)
	local rAction, rActor = PowerManager.getPCPowerAction(nodeAction);
	local sDamage = "";

	-- Clauses text
	if rAction and #rAction.clauses > 0 then
		sDamage = Utilities.clauses_2_string(rAction);
	end

	-- Level text
	if rAction.description ~= "" and sDamage ~= "" then
		sDamage = sDamage .. " (" .. rAction.description .. ")";
	end

	return sDamage;
end

function getPCPowerHealActionText(nodeAction)
	local rAction, rActor = PowerManager.getPCPowerAction(nodeAction);
	local sHeal = "";
	if rAction and rAction.sRoll ~= "" then
		-- evalAction(rActor, nodeAction.getChild("..."), rActionHeal);

		sHeal = Utilities.clauses_2_string(rAction, true);

		if sHeal ~= "" then
			sHeal = sHeal .. " " .. Interface.getString(rAction.sSubtype);
			if rAction.nTemporal > 0 then
				sHeal = sHeal .. " " .. Interface.getString("power_label_heal_temporal");
			elseif rAction.sSubtype == "power_label_heal_madness" or rAction.sSubtype == "power_label_heal_spirit" then
				if rAction.nMajorHealing > 0 then
					sHeal = sHeal .. " " .. Interface.getString("power_label_heal_permanent"):lower();
				else
					sHeal = sHeal .. " " .. Interface.getString("power_label_heal_temporal"):lower();
				end
			elseif not rAction.sSubtype == "power_label_heal_vigor" then
				if rAction.sSubtype == "power_label_heal_stat" then
					sHeal = sHeal .. " (" .. rAction.heal_stat .. ")";
				end
				if rAction.nMajorHealing > 0 then
					sHeal = sHeal .. " [" .. Interface.getString("power_label_heal_permanent") .. "]";
				else
					sHeal = sHeal .. " [" .. Interface.getString("power_label_heal_temporal") .. "]";
				end
			elseif rAction.sSubtype == "power_label_heal_corruption" then
				if rAction.corruption_type == "" then
					sHeal = sHeal .. " (" .. Interface.getString("corruption_body") .. ")";
				else
					sHeal = sHeal .. " (" .. Interface.getString(rAction.corruption_type) .. ")";
				end
			elseif rAction.sSubtype == "power_label_heal_stat" then
				sHeal = sHeal .. " (" .. Interface.getString(rAction.heal_stat) .. ")";
			end
			if DB.getValue(nodeAction, "healtargeting", "") == "self" then
				sHeal = sHeal .. Interface.getString("self");
			end
		end
	end
	if rAction.description ~= "" and sHeal ~= "" then
		sHeal = sHeal .. " (" .. rAction.description .. ")";
	end

	return sHeal;
end

function getPCPowerEffectActionText(nodeAction)
	if not nodeAction then
		return;
	end

	local rAction, rActor = PowerManager.getPCPowerAction(nodeAction);

	local sLabel = rAction.sName;

	local sApply = rAction.sApply;
	if sApply == "action" then
		sLabel = sLabel .. "; [ACTION]";
	elseif sApply == "roll" then
		sLabel = sLabel .. "; [ROLL]";
	elseif sApply == "single" then
		sLabel = sLabel .. "; [SINGLES]";
	end

	local sTargeting = rAction.sTargeting;
	if sTargeting == "self" then
		sLabel = sLabel .. "; [SELF]";
	end

	local sDuration = "";
	if rAction.bConc then
		sDuration = "Conc";
		if rAction.nDuration > 0 then
			sDuration = sDuration .. ", int ";
		end
	end
	if rAction.nDuration > 0 then
		sDuration = sDuration .. rAction.nDuration .. " " .. rAction.sUnits;
		if rAction.sUnits == "" then
			sDuration = sDuration .. "turns";
		end
		if rAction.nRitual then
			sDuration = sDuration .. " X success";
		elseif rAction.nRandomTied then
			sDuration = sDuration .. " X 1 to 4";
		end
	end
	if sDuration ~= "" then
		sLabel = sLabel .. " (" .. sDuration .. ")";
	end

	if rAction.nApplyCost > 0 then
		if rAction.sCostType == "" then
			sLabel = sLabel .. " (" .. Interface.getString("power_label_cost") .. ": " .. tostring(rAction.nCost);
		else
			sLabel = sLabel .. Interface.getString("power_label_cost_per_level");
		end
		if rAction.sCostResource ~= "" then
			sLabel = sLabel .. " " .. Interface.getString(rAction.sCostResource);
		end
		sLabel = sLabel .. ")"
	end

	return sLabel;
end

function getPCPowerAfflictionActionText(nodeAction)
	if not nodeAction then
		return;
	end

	local rAction, rActor = PowerManager.getPCPowerAction(nodeAction);
	if not rAction.data then
		return;
	end

	local sLabel = rAction.nameAffliction;
	local sExtra = "";
	if rAction.typeAffliction == "type_affliction_illness" then
		sExtra = rAction.contagion;
	elseif rAction.typeAffliction == "type_affliction_poison" then
		sExtra = Interface.getString(rAction.delivery);
	end
	local sType = Interface.getString(rAction.typeAffliction);
	if sType ~= "" then
		if sExtra ~= "" then
			sType = sType .. ", " .. sExtra;
		end
		sLabel = sLabel .. " (" .. sType .. ") ";
	end
	sLabel = sLabel .. " Pel. " .. tostring(rAction.peligrosity);

	return sLabel;

end



function getPCPowerCostActionText(nodeAction)
	if not nodeAction then
		Debug.chat("No hay accion")
		return "";
	end

	local rAction, rActor = PowerManager.getPCPowerAction(nodeAction);
	local sActions = DB.getValue(nodeAction, "actions", "");
	local sCost = DB.getValue(nodeAction, "cost", "");

	if sActions ~= "" and sCost ~= "" then
		sActions = sActions .. ", ";
	end
	return sActions .. sCost;
end



-------------------------
-- POWER USAGE
-------------------------

function getPowerGroupRecord(rActor, nodePower)
	local rGroup = nil;

	local sActorType, nodeActor = ActorManager.getTypeAndNode(rActor);
	local nodePowerGroup = nil;
	local sGroup = DB.getValue(nodePower, "group", "");
	for _,v in pairs(DB.getChildren(nodeActor, "powergroup")) do
		if DB.getValue(v, "name", "") == sGroup then
			nodePowerGroup = v;
		end
	end

	if nodePowerGroup then
		-- Legacy
		rGroup = {};
		rGroup.sType = "pc";
		-- Basic info
		rGroup.node = nodePowerGroup;
		rGroup.nodename = nodePowerGroup.getNodeName();
		if sGroup == "" then
			rGroup.grouptype = "";
		else
			rGroup.grouptype = DB.getValue(nodePowerGroup, "grouptype", "");
		end
		-- Default stat and skill
		rGroup.groupstat = DB.getValue(nodePowerGroup, "groupstat", ""):lower();
		rGroup.groupskill = DB.getValue(nodePowerGroup, "groupskill", "");
		rGroup.magictype = DB.getValue(nodePowerGroup, "magictype", "");
	end

	return rGroup;
end

function evalAction(rActor, nodePower, rAction)
	local aPowerGroup = getPowerGroupRecord(rActor, nodePower);

	-- Heal and damage
	if rAction.type == "roll" then
		if rAction.clauses then
			for _,vClause in ipairs(rAction.clauses) do
				-- Stat
				if vClause.stat == "base" and aPowerGroup then
					vClause.stat = aPowerGroup.groupstat;
				end

				-- Get roll and increase category
				local nAbility, _ = ActorManager2.getCurrentAbilityValue(rActor, vClause.stat);
				local nCat = math.floor(vClause.statmult * nAbility + rAction.level * vClause.height);
				vClause.dice, vClause.modifier = StringManager.convertStringToDice(vClause.roll);
				vClause.dice = Utilities.change_dice_cat(vClause.dice, nCat);
			end
		end

	elseif rAction.type == "attack" then
		rAction.aDice, rAction.modifier = StringManager.convertStringToDice(rAction.modifier);
		local nCat = math.floor(rAction.level * rAction.height);
		if nCat > 0 and #rAction.aDice == 0 then
			rAction.aDice = {"d0"};
		end
		rAction.aDice = Utilities.change_dice_cat(rAction.aDice, nCat);

	elseif rAction.type == "skill" then
		rAction.aDice, rAction.mod = StringManager.convertStringToDice(rAction.mod);
		local nCat = math.floor(rAction.level * rAction.height);
		if nCat > 0 and #rAction.aDice == 0 then
			rAction.aDice = {"d0"};
		end
		rAction.aDice = Utilities.change_dice_cat(rAction.aDice, nCat);

	elseif rAction.type == "effect" then
		-- Add name and style tag if not present
		if rAction.sPowerName ~= "" and not rAction.sName:find(rAction.sPowerName) then
			rAction.sName = rAction.sPowerName .. "; " .. rAction.sName;
		end
		if rAction.bStyle and not rAction.sName:find(DataCommon.keyword_inactive["style"]) then
			rAction.sName = rAction.sName .. "; " .. DataCommon.keyword_inactive["style"];
		end

		-- Sometimes the spell is tied automatically
		local nMult
		if rAction.bRandomTied then
			nMult = math.random(4);

		elseif rAction.bRitual then
			nMult = ActionSkill.getCastStatus(rActor);
		end
		if nMult then
			rAction.nDur = rAction.nDur * nMult;
			rAction.nDuration = rAction.nDuration * nMult;
			rAction.sName = rAction.sName .. "; " .. DataCommon.keyword_states["TIED"] .. ": " .. tostring(10 + rAction.level_cast);
			rAction.bConc = false;
		end

		-- Base atributes
		for k,v in pairs(DataCommon.ability_stol) do
			local sAux = "%[" .. k .. "%]";
			local nCounter = 0;
			while nCounter < 15 and rAction.sName:match(sAux) do
				nCounter = nCounter + 1;
				local nValue = ActorManager2.getCurrentAbilityValue(rActor, v);
				rAction.sName = rAction.sName:gsub(sAux, tostring(nValue));
			end
		end
		local nCounter = 0;
		while nCounter < 15 and rAction.sName:match("%[BASE%]") do
			nCounter = nCounter + 1;
			if not aPowerGroup then
				aPowerGroup = getPowerGroupRecord(rActor, nodePower);
			end
			if aPowerGroup and aPowerGroup.groupstat and DataCommon.ability_ltos[aPowerGroup.groupstat] then
				local nValue = ActorManager2.getCurrentAbilityValue(rActor, aPowerGroup.groupstat);
				rAction.sName = rAction.sName:gsub("%[BASE%]", tostring(nValue));
			end
		end
		-- Previous rolls
		local aLabels = ActionRoll.getRollLabels(rActor);
		local sAux = "";
		for _, sLabel in pairs(aLabels) do
			sAux = "%[" .. sLabel .. "%]";
			if rAction.sName:match(sAux) then
				local nValue = ActionRoll.getRollResult(rActor, sLabel);
				rAction.sName = rAction.sName:gsub(sAux, tostring(nValue));
				ActionRoll.clearRollResult(rActor, sLabel);
			end
		end
		rAction.sName = EffectManagerSS.evalEffect(rActor, rAction.sName);
		-- Current rolls
		local bRolls = true;
		local nCounter = 0;
		while nCounter < 15 and bRolls do
			nCounter = nCounter + 1;
			local sDice, sCat, sSign, sBonus, sExtra = rAction.sName:match("%[(%d*)d(%d+)([+-]?)(%d*)(.-)%]");
			if sCat then
				local sTotal = "%[" .. string.format("%sd%s%s%s", sDice, sCat, "%" .. sSign, sBonus) .. "%]";
				-- Add bonus
				local nResult = 0;
				if sSign ~= "" and sBonus ~= "" then
					local nSign = 1;
					if sSign == "-" then
						nSign = -1;
					end
					nResult = tonumber(sBonus) * nSign;
				end
				-- Add extra category
				local nCat = tonumber(sCat);
				if sExtra ~= "" then
					local sBefore, sSignExtra, sCatExtra, sAfter = sExtra:match("(.-)%(([+-])%)(%d+)(.*)");
					if sSignExtra ~= "" and sCatExtra ~= "" then
						local nSign = 1;
						if sSignExtra == "-" then
							nSign = -1;
						end
						nCat = nCat + nSign * tonumber(sCatExtra) * rAction.nHeight;
					end
					sTotal = "%[" .. string.format("%sd%s%s%s", sDice, sCat, "%" .. sSign, sBonus) .. sBefore .."%(%" .. sSignExtra .. "%)" .. sCatExtra .. sAfter .. "%]";
				end

				-- Roll the dice
				local nDice = 1;
				if sDice ~= "" then
					nDice = tonumber(sDice);
				end
				for ind = 1,nDice do
					nResult = nResult + CombatManager2.simpleRoll(string.format("d%d", nCat));
				end
				-- Substitue
				rAction.sName = rAction.sName:gsub(sTotal, tostring(nResult))
			else
				bRolls = false;
			end
		end

	elseif rAction.type == "heal" then
		if rAction.nTemporal and rAction.nTemporal > 0 then
			-- Check if there is an an effect in the power that sets the temporal vit/vigor tag. If not, set it to add automatically.
			local sTempLabel = DataCommon.keyword_inactive["temp_vit"];
			if rAction.sSubtype == "power_label_heal_vigor" then
				sTempLabel = DataCommon.keyword_inactive["temp_vigor"];
			end
			for _,v in pairs(nodePower.createChild("actions").getChildren()) do
				rAction.nAddEffect = 1;
				if DB.getValue(v, "type", "") == "effect" then
					if DB.getValue(v, "label", ""):find(sTempLabel) then
						rAction.nAddEffect = 0;
						break;
					end
				end
				-- Set duration and if it is concentration or not
				if rAction.nAddEffect > 0 then
					rAction.nConc, rAction.nDur = getConcDuration(nodePower);
					local sTypeSpell = DB.getValue(nodePower, "reftype", "");
					local nodeItem;
					local sItemPath = DB.getValue(nodePower, "item_path", "");
					if sItemPath ~= "" then
						nodeItem = DB.findNode(sItemPath);
					end

					if ItemManager2.isPotion(nodeItem) or not ActorManager2.isMagicUser(rActor) or DB.getValue(nodePower, "type_spell", "") == "power_ritual" then
						rAction.nConc = 0;
						rAction.nDur = rAction.nDur * math.random(4);
					end
				end
			end
		end

	elseif rAction.type == "save" then
		rAction.nMod = ModifierStack.getSum();
		rAction.nAdv = ActionsManager2.getDescktopAdvantage();
	end
end

function performAction(draginfo, rActor, rAction, nodePower)

	if not rActor or not rAction then
		return false;
	end

	evalAction(rActor, nodePower, rAction);
	local rRolls = {};
	if rAction.type == "attack" then
		rAction.type = rAction.type_attack;
		ActionAttack.performRoll(draginfo, rActor, rAction);

	elseif rAction.type == "skill" then
		rAction.nodeSkill = ActorManager2.getSkillNode(rActor, rAction.skill);
		ActionSkill.performRoll(draginfo, rActor, rAction);
		if rAction.sType == 'power_label_skill_opposed' then
			rAction.stat = rAction.opposed_stat;
			local aTargets = TargetingManager.getFullTargets(rActor);
			for _, rTarget in pairs(aTargets) do
				rAction.nodeSkill = ActorManager2.getSkillNode(rTarget, rAction.opposed_skill);
				ActionSkill.performRoll(draginfo, rTarget, rAction);
			end
		end

	elseif rAction.type == "save" then
		ActionPower.performSaveVsRoll(draginfo, rActor, rAction)

	elseif rAction.type == "cast" then
		ActionCast.perormCastSpell(draginfo, rActor, rAction)

	elseif rAction.type == "powersave" then
		ActionPower.performSaveVsRoll(draginfo, rActor, rAction)

	elseif rAction.type == "damage" then
		ActionDamage.performRoll(draginfo, rActor, rAction);

	elseif rAction.type == "heal" then
		ActionHeal.performRoll(draginfo, rActor, rAction);

	elseif rAction.type == "effect" then
		ActionEffect.performRoll(draginfo, rActor, rAction);

	elseif rAction.type == "summon" then
		ActionSummon.performSummon(rActor, rAction);

	elseif rAction.type == "roll" then
		ActionRoll.performRoll(draginfo, rActor, rAction);

	elseif rAction.type == "affliction" then
		ActionAffliction.performRoll(draginfo, rActor, rAction);

	elseif rAction.type == "cost" then
		performCost(draginfo, rActor, rAction);

	else
		Debug.chat('Action of type ', rAction.type, ' not possible')
	end
	return true;
end

function performCost(draginfo, rActor, rAction)
	-- Actions
	if rAction.sActions ~= "" then
		local aActions, sActions = ActionCast.getArrayOfActions(rAction.sActions, 0, 0);
		local nodeCT = ActorManager2.getCTNode(rActor);
		bSuccess = CombatManager2.useAction(nodeCT, aActions);
		if not bSuccess then
			return;
		end
	end

	-- Cost
	if rAction.sCost ~= "" then
		local nCost, sTypeCost = getActivityCost(rAction.sCost);
		if sTypeCost == "vitality" then
			nCostVitality = 1;
		elseif sTypeCost ~= "vigor" then
			nCost = 0;
		end

		if nCost > 0 then
			local rAction = {nPiercing = 0, nSelfTarget = 1, sNotification = Interface.getString("cast_chat_spellcost")};
			rAction.sDamageTypes =  DataCommon.dmgtypes_modifier["unavoidable"];
			if sTypeCost == "vigor" then
				rAction.sDamageTypes = rAction.sDamageTypes .. ", " .. DataCommon.dmgtypes_modifier["nonlethal"];
			end
			rAction.nTotal = nCost;
			ActionDamage.notifyApplyDamage(nil, rActor, rAction);
		end
	end

end

-------------------------
-- SPELLS AND ACTIVITIES
-------------------------

function getConcDuration(nodeSpell, bDurAndUnits)
	local sDuration = DB.getValue(nodeSpell, "duration", ""):lower();
	-- Has concentration
	local bConc = (sDuration ~= "") and (sDuration ~= Interface.getString("instantaneous"));
	local nConc = 0;
	if bConc then
		nConc = 1;
	end
	-- Concentration duration
	local nDur = 0;
	if bConc and (sDuration ~= Interface.getString("no_tiable")) then
		local sTurn = sDuration:match("(%d+) " .. Interface.getString("turn"));
		local sMin = sDuration:match("(%d+) " .. Interface.getString("minute"));
		local sHour = sDuration:match("(%d+) " .. Interface.getString("hour"));
		local sDay = sDuration:match("(%d+) " .. Interface.getString("day"));
		if bDurAndUnits then
			if sTurn then
				return tonumber(sTurn), "";
			elseif sMin then
				return tonumber(sMin), "minute";
			elseif sHour then
				return tonumber(sHour), "hour";
			elseif sDay then
				return tonumber(sDay), "day";
			end
		end
		if sTurn then
			nDur = tonumber(sTurn);
		elseif sMin then
			nDur = tonumber(sMin) * DataCommon.turns_per_unit["minute"];
		elseif sHour then
			nDur = tonumber(sHour) * DataCommon.turns_per_unit["hour"];
		elseif sDay then
			nDur = tonumber(sDay) * DataCommon.turns_per_unit["day"];
		else
			nDur = 999999;
		end
	end
	-- Effect actions
	nEffects = 0;
	for _,v in pairs(nodeSpell.createChild("actions").getChildren()) do
		if Utilities.inArray({"effect", "summon"}, DB.getValue(v, "type", "")) then
			nEffects = 1;
			break;
		end
	end
	if bDurAndUnits then
		return 0, "";
	else
		return nConc, nDur, nEffects;
	end
end

function getPowerKeywords(nodeSpell)
	local sKeywords = DB.getValue(nodeSpell, "keywords", ""):lower();

	-- Add magic
	local sType = DB.getValue(nodeSpell, "reftype", "");
	if sType == "power_spell" or sType == "power_item_spell" then
		if sKeywords == "" then
			sKeywords = DataCommon.ability_magic_trans;
		else
			sKeywords = sKeywords .. ", " .. DataCommon.ability_magic_trans;
		end
	end

	-- Add magic
	local sType = DB.getValue(nodeSpell, "reftype", "");
	if sType == "power_spell" or sType == "power_item_spell" then
		if sKeywords == "" then
			sKeywords = DataCommon.ability_magic_trans;
		else
			sKeywords = sKeywords .. ", " .. DataCommon.ability_magic_trans;
		end
	end

	-- Add school
	local sSchool = DB.getValue(nodeSpell, "school", ""):lower();
	if sSchool ~= "" then
		if sKeywords == "" then
			sKeywords = sSchool;
		else
			sKeywords = sKeywords .. ", " .. sSchool;
		end
	end

	-- Add states
	local nodeActions = nodeSpell.getChild("actions");
	local aUsed = {};
	if sKeywords:find(DataCommon.condition_mental) then
		table.insert(aUsed, DataCommon.condition_mental)
	end
	if sKeywords:find(DataCommon.condition_emotional) then
		table.insert(aUsed, DataCommon.condition_emotional)
	end

	if nodeActions then
		for _,node in pairs(nodeActions.getChildren()) do
			local sType = DB.getValue(node, "type", "");
			if sType == "effect" then
				local sEffect = DB.getValue(node, "label", "");
				for _,v in pairs(DataCommon.conditions) do
					if sEffect:find(v) and not Utilities.inArray(aUsed, v) then
						-- Add current
						if sKeywords == "" then
							sKeywords = v:lower();
						else
							sKeywords = sKeywords .. ", " .. v:lower();
						end
						table.insert(aUsed, v);
						-- Add mental and/or emotional
						if not Utilities.inArray(aUsed, DataCommon.condition_mental) and Utilities.inArray(DataCommon.conditions_mental, v) then
							sKeywords = sKeywords .. ", " .. DataCommon.condition_mental;
						end
						if not Utilities.inArray(aUsed, DataCommon.condition_emotional) and Utilities.inArray(DataCommon.conditions_emotional, v) then
							sKeywords = sKeywords .. ", " .. DataCommon.condition_emotional;
						end
					end
				end
			end
		end
	end
	return sKeywords;
end


function getCastAction(nodePower)
	local nodeList = DB.createChild(nodePower, "actions");
	for _,nodeAction in pairs(nodeList.getChildren()) do
		if DB.getValue(nodeAction, "type", "") == "cast" then
			return nodeAction;
		end
	end
	return;
end


function getHeightening(nodePower)
	local nodeActionCast = getCastAction(nodePower);
	local nHeight = 0;
	if nodeActionCast then
		nHeight = DB.getValue(nodeActionCast, "parameter", 0);
	end
	-- TODO: añadir efectos (foco, winds, etc)
	return nHeight;
end

function getSpellLevel(nodePower, bLimit)
	local nLevel = DB.getValue(nodePower, "level", 0) + getHeightening(nodePower);
	if bLimit then
		nLevel = math.max(0, math.min(10, nLevel));
	end
	return nLevel;
end

function getActivityCost(sLabel)
	local sCost = sLabel:match("(%d+)") or "0";
    local sType = (sLabel:match("(%a+)") or ""):lower();
	if sType == "" or sType == Interface.getString("vigor"):lower() then
		sType = "vigor";
	elseif sType == Interface.getString("vitality"):lower() then
		sType = "vitality";
	elseif sType == Interface.getString("essence"):lower() then
		sType = "essence";
	else
		sType = "";
	end
	return tonumber(sCost), sType;
end

-------------------------
-- POWER PARSING (TODO)
-------------------------


-------------------------
-- RECORDS
-------------------------


local aPowerLists = { "spell", "reference.spelldata" };

function getPowerType(nodeApt)
    local sType = DB.getValue(nodeApt, "type", "");
    return sType;
end

function findRefPower(sName, sType)
    -- Add modules
	local aPowerListsMod = Utilities.addModules(aPowerLists);

    -- Find the correct one
	if sType == "" then
		sType = nil;
	end
    for _,sPath in pairs(aPowerListsMod) do
		local nodeList = DB.findNode(sPath);
        if nodeList then
			for _, nodeNew in pairs(nodeList.getChildren()) do
				local bCond1 = DB.getValue(nodeNew, "name", "") == sNameNew;
				local bCond2 = not sType or getPowerType(nodeNew) == sType;
				if bCond1  and bCond2 then
                    return nodeNew
                end
            end
        end
    end
    return nil;
end

