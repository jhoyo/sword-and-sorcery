--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYHEAL = "applyheal";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYHEAL, handleApplyHeal);

	ActionsManager.registerModHandler("heal", modHeal);
	ActionsManager.registerResultHandler("heal", onHeal);
end

function handleApplyHeal(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local rTarget = ActorManager.resolveActor(msgOOB.sTargetNode);
	if rTarget then
		rTarget.nOrder = msgOOB.nTargetOrder;
	end

	local rAction  = {};
	rAction.bSecret = msgOOB.nSecret == "1";
	rAction.nTotal = tonumber(msgOOB.nTotal or "0");
	rAction.sSubtype = msgOOB.sSubtype;
	rAction.sHealStat = msgOOB.sHealStat;
	rAction.sTypeHealing = msgOOB.sTypeHealing;
	rAction.bMagic = msgOOB.nMagic == "1";	
	rAction.bTemporal = msgOOB.nTemporal == "1";
	rAction.bMajorHealing = msgOOB.nMajorHealing == "1";
	rAction.bAddEffect = msgOOB.nAddEffect == "1";
	rAction.bConc = msgOOB.nConc == "1";
	rAction.nDur = tonumber(msgOOB.nDur or "0");
	rAction.sNameSpell = msgOOB.sNameSpell;
	rAction.sCorruptionType = msgOOB.sCorruptionType or "";
	rAction.nHiddenSpell = tonumber(msgOOB.nHiddenSpell or "0");

	applyHeal(rSource, rTarget, rAction);
end

function notifyApplyHeal(rSource, rTarget, rAction)
	if not rTarget then
		return;
	end
	local sTargetType, sTargetNode = ActorManager.getTypeAndNodeName(rTarget);
	if sTargetType ~= "pc" and sTargetType ~= "ct" then
		return;
	end

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYHEAL;

	if rAction.bSecret then
		msgOOB.nSecret = 1;
	else
		msgOOB.nSecret = 0;
	end
	msgOOB.nTotal = rAction.nTotal;
	msgOOB.sTargetType = sTargetType;
	msgOOB.sTargetNode = sTargetNode;
	msgOOB.nTargetOrder = rTarget.nOrder;
	msgOOB.sHealStat = rAction.sHealStat;
	msgOOB.sSubtype = rAction.sSubtype;
	msgOOB.sTypeHealing = rAction.sTypeHealing;
	msgOOB.nMagic = rAction.nMagic;
	msgOOB.nTemporal = rAction.nTemporal;
	msgOOB.nMajorHealing = rAction.nMajorHealing;
	msgOOB.nAddEffect = rAction.nAddEffect;
	msgOOB.nConc = rAction.nConc;
	msgOOB.nDur = rAction.nDur;
	msgOOB.sNameSpell = rAction.sNameSpell;
	msgOOB.nHiddenSpell = rAction.nHiddenSpell;
	msgOOB.sCorruptionType = rAction.sCorruptionType;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	Comm.deliverOOBMessage(msgOOB, "");
end

function getRoll(rActor, rAction)
	local rRoll = {};
	rRoll.sType = "heal";
	rRoll.aDice = rAction.aDice or {};
	rRoll.nMod = rAction.nMod or 0;
	rRoll.sSubtype = rAction.sSubtype;
	rRoll.sHealStat = rAction.heal_stat;
	rRoll.nTemporal = rAction.nTemporal or 0;
	rRoll.nMajorHealing = rAction.nMajorHealing or 0;
	rRoll.sTypeHealing = rAction.sTypeHealing or "";
	rRoll.nAvoidNotifyAction = rAction.nAvoidNotifyAction;
	rRoll.nAddEffect = rAction.nAddEffect or 0;
	rRoll.nConc = rAction.nConc or 0;
	rRoll.nDur = rAction.nDur or 0;
	rRoll.sNameSpell = StringManager.capitalize(rAction.label)
	rRoll.nHiddenSpell = rAction.nHiddenSpell;
	rRoll.sCorruptionType = rAction.corruption_type or "";
	if rAction.bMagic then
		rRoll.nMagic = 1;
	else
		rRoll.nMagic = 0;
	end

	-- Handle self-targeting
	if rAction.sTargeting and rAction.sTargeting == "self" then
		rRoll.bSelfTarget = true;
	end

	-- Build description
	rRoll.sDesc = Interface.getString("heal"):upper() ;
	if rAction.order and rAction.order > 1 then
		rRoll.sDesc = rRoll.sDesc .. " #" .. rAction.order;
	end
	rRoll.sDesc = rRoll.sDesc .. "\n\n" .. rAction.label:upper();

	-- Add the dice and modifiers, and encode ability scores used
	if rAction.sRoll and rAction.sRoll ~= "" then
		rRoll.aDice, rRoll.nMod = Utilities.getDiceAndMod(rActor, rAction.sRoll, rAction.sAbility, rAction.nMult, rAction.nHeight, rAction.level);
	end
	local sHeal = "\n- " .. StringManager.convertDiceToString(rRoll.aDice, rRoll.nMod);
	if rAction.sSubtype == "power_label_heal_stat" then
		sHeal = sHeal .. " " .. rAction.heal_stat;
	else
		sHeal = sHeal .. " " .. Interface.getString(rAction.sSubtype);
		if rRoll.nTemporal > 0 and (rAction.sSubtype == "power_label_heal_vit" or rAction.sSubtype == "power_label_heal_vigor") then
			sHeal = sHeal .. " " .. Interface.getString("power_label_heal_temporal");
		elseif rAction.sSubtype == "power_label_heal_madness" or rAction.sSubtype == "power_label_heal_spirit" then
			if rRoll.nMajorHealing > 0 then
				sHeal = sHeal .. " " .. Interface.getString("power_label_heal_permanent");
			else
				sHeal = sHeal .. " " .. Interface.getString("power_label_heal_temporal");
			end
		end
	end
	rRoll.sDesc = rRoll.sDesc .. sHeal;


	return rRoll;
end

function performRoll(draginfo, rActor, rAction)
	local rRoll = getRoll(rActor, rAction);

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function modHeal(rSource, rTarget, rRoll)
	-- TODO: Modifiers to heal action



	-- decodeHealClauses(rRoll);
	-- CombatManager2.addRightClickDiceToClauses(rRoll);

	-- local aAddDesc = {};
	-- local aAddDice = {};
	-- local nAddMod = 0;

	-- -- Track how many heal clauses before effects applied
	-- local nPreEffectClauses = #(rRoll.clauses);

	-- if rSource then
	-- 	local bEffects = false;

	-- 	-- Apply general heal modifiers
	-- 	local nEffectCount;
	-- 	aAddDice, nAddMod, nEffectCount = EffectManagerSS.getEffectsBonus(rSource, {DataCommon.keyword_states["HEAL"]});
	-- 	if (nEffectCount > 0) then
	-- 		bEffects = true;
	-- 	end

	-- 	-- Apply ability modifiers
	-- 	for sAbility, sAbilityMult in rRoll.sDesc:gmatch("%[MOD: (%w+) %((%w+)%)%]") do
	-- 		local nBonusStat, nAdv, nBonusEffects = ActorManager2.getAbility(rSource, DataCommon.ability_stol[sAbility], true);
	-- 		if nBonusEffects > 0 then
	-- 			bEffects = true;
	-- 			local nMult = tonumber(sAbilityMult) or 1;
	-- 			if nBonusStat > 0 and nMult ~= 1 then
	-- 				nBonusStat = math.floor(nMult * nBonusStat);
	-- 			end
	-- 			nAddMod = nAddMod + nBonusStat;
	-- 		end
	-- 	end

	-- 	-- If effects happened, then add note
	-- 	if bEffects then
	-- 		local sEffects = "";
	-- 		local sMod = StringManager.convertDiceToString(aAddDice, nAddMod, true);
	-- 		if sMod ~= "" then
	-- 			sEffects = "[" .. Interface.getString("effects_tag") .. " " .. sMod .. "]";
	-- 		else
	-- 			sEffects = "[" .. Interface.getString("effects_tag") .. "]";
	-- 		end
	-- 		table.insert(aAddDesc, sEffects);
	-- 	end
	-- end

	-- if #aAddDesc > 0 then
	-- 	rRoll.sDesc = rRoll.sDesc .. " " .. table.concat(aAddDesc, " ");
	-- end
	-- ActionsManager2.encodeDesktopMods(rRoll);
	-- for _,vDie in ipairs(aAddDice) do
	-- 	if vDie:sub(1,1) == "-" then
	-- 		table.insert(rRoll.aDice, "-p" .. vDie:sub(3));
	-- 	else
	-- 		table.insert(rRoll.aDice, "p" .. vDie:sub(2));
	-- 	end
	-- end
	-- rRoll.nMod = rRoll.nMod + nAddMod;
end

function onHeal(rSource, rTarget, rRoll)
	if not rRoll.nAvoidNotifyAction then
		local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
		rMessage.text = rRoll.sDesc;
		rMessage.mode = "chat_heal";
		-- Comm.deliverChatMessage(rMessage);
		MessageManager.onRollMessage(rMessage);
	end

	-- Set the action info
	local rAction = {};
	rAction.bSecret = rRoll.bTower;
	-- rAction.sText = string.gsub(rMessage.text, " %[MOD:[^]]*%]", "");
	rAction.nTotal = ActionsManager.total(rRoll);
	rAction.sSubtype = rRoll.sSubtype;
	rAction.sHealStat = rRoll.sHealStat;
	rAction.sTypeHealing = rRoll.sTypeHealing;
	rAction.nMagic = rRoll.nMagic;
	rAction.nTemporal = rRoll.nTemporal;
	rAction.nMajorHealing = rRoll.nMajorHealing;
	rAction.nAddEffect = rRoll.nAddEffect;
	rAction.nConc = rRoll.nConc;
	rAction.nDur = rRoll.nDur;
	rAction.sNameSpell = rRoll.sNameSpell;
	rAction.nHiddenSpell = rRoll.nHiddenSpell;
	rAction.sCorruptionType = rRoll.sCorruptionType;

	-- Apply damage to the PC or CT entry referenced
	notifyApplyHeal(rSource, rTarget, rAction);
end


function applyHeal(rSource, rTarget, rAction)
	local bSecret = rAction.bSecret;

	-- Get characters
	local sTargetType, nodeTarget = ActorManager.getTypeAndNode(rTarget);
	if sTargetType ~= "pc" and sTargetType ~= "ct" then
		return;
	end
	local nodeTarget, rTarget = ActorManager2.getActorAndNode(rTarget);
	local nTotal = rAction.nTotal;
	local aNotifications = rAction.aNotifications or {};

	-- Reduction by magic protection
	local nMagicPiercing, _ = EffectManagerSS.getEffectsBonus(rSource, {DataCommon.keyword_states["MPIERCING"]}, true);
	local nProt = ActorManager2.getProtectionValue(rTarget, {DataCommon.dmgtypes_modifier["magic"]}, 0, nMagicPiercing);
	if nProt > 0 then
		nTotal = nTotal - nProt;
		table.insert(aNotifications, Interface.getString("heal_chat_magicprot"):format(nProt));
	end
	-- Reduction by magic resistance
	local nSens, nInmune, nAbsorb, nMult, _ = ActorManager2.getSensitivity(rTarget, DataCommon.dmgtypes_modifier["magic"], rTarget, true);
	if nAbsorb > 0 then
		local nMod = math.floor(nTotal * nMult);
		table.insert(aNotifications, Interface.getString("heal_chat_magicabs"):format(nMod));
	elseif nInmune > 0 then
		table.insert(aNotifications, Interface.getString("heal_chat_magicinmune"));
	elseif nSens > 0 then
		local nMod = nTotal - math.ceil(nTotal * nMult);
		table.insert(aNotifications, Interface.getString("heal_chat_magicres"):format(nMod));
	end
	nTotal = nTotal * nMult;

	-- Switch healing type
	local sNodeWounds = "";
	local sNodeWounds2 = "";
	local sIcon = "";
	local sMode = "chat_success";
	local sText = Interface.getString(rAction.sSubtype);
	local sText2 = "";
	local nTotal2 = 0;
	if rAction.sSubtype == "power_label_heal_vit" then
		if rAction.bTemporal then
			sNodeWounds = "health.vit.temp";
			sIcon = "roll_healtemp";
		else
			sNodeWounds = "health.vit.damage";
			if rAction.bMajorHealing then
				sIcon = "roll_trueheal";
			else
				sIcon = "roll_heal";
			end
			if nTotal > 0 then
				-- Reactivate REGEN
				local bCond = EffectManagerSS.reactivateRegenerationEffects(rTarget);
				if bCond then
					table.insert(aNotifications, Interface.getString("heal_chat_regen_activate"));
				end
			end
		end
		CharManager.updateEncumbrance(rTarget);
	elseif rAction.sSubtype == "power_label_heal_vigor" then
		if rAction.bTemporal then
			sNodeWounds = "health.vigor.temp";
			sIcon = "roll_healtemp_vigor";
		else
			sNodeWounds = "health.vigor.damage";
			sIcon = "roll_heal_vigor";
		end
		CharManager.updateEncumbrance(rTarget);
	elseif rAction.sSubtype == "power_label_heal_stat" then
		-- TODO: Opciones DON y CUALQUIERA
		local sStatTrans = DataCommon.abilities_translation[rAction.sHealStat];
		if sStatTrans then
			sNodeWounds = "abilities." .. sStatTrans .. ".damage";
			if rAction.bMajorHealing then
				sIcon = "roll_trueheal_stat";
			else
				sIcon = "roll_heal_stat";
			end
		else
			Debug.chat(rAction.sHealStat, "is not a valid Ability string")
			return;
		end
	elseif rAction.sSubtype == "power_label_heal_essence" then
		sNodeWounds = "health.essence.damage";
		sIcon = "roll_heal_essence";
	elseif rAction.sSubtype == "power_label_heal_madness" then
		if rAction.bMajorHealing then
			sNodeWounds = "health.sanity.damage";
			sNodeWounds2 = "health.sanity.damage_perm";
			sIcon = "roll_trueheal_madness";
			sText2 = sText .. " " .. Interface.getString("power_label_heal_permanent");
		else
			sNodeWounds = "health.sanity.damage";
			sIcon = "roll_heal_madness";
		end
		sText = sText .. " " .. Interface.getString("power_label_heal_temporal");
	elseif rAction.sSubtype == "power_label_heal_spirit" then
		if rAction.bMajorHealing then
			sNodeWounds = "health.spirit.damage";
			sNodeWounds2 = "health.spirit.damage_perm";
			sIcon = "roll_trueheal_spirit";
			sText2 = sText .. " " .. Interface.getString("power_label_heal_permanent");
		else
			sNodeWounds = "health.spirit.damage";
			sIcon = "roll_heal_spirit";
		end
		sText = sText .. " " .. Interface.getString("power_label_heal_temporal");
	elseif rAction.sSubtype == "power_label_heal_barrier" then
		sNodeWounds = "health.barrier";
		sIcon = "roll_barrier";
		rAction.bTemporal = true;
		sText = sText .. " " .. Interface.getString("power_label_heal_barrier");
		
	elseif rAction.sSubtype == "power_label_heal_corruption" then
		sText = sText .. " " .. Interface.getString("power_label_heal_corruption")
		if rAction.sCorruptionType == "" then
			sNodeWounds = "health.corruption.body";
			sIcon = "char_body";
			sText = sText .. " (" .. Interface.getString("corruption_body") .. ")";
		elseif rAction.sCorruptionType == "corruption_mind" then
			sNodeWounds = "health.corruption.mind";
			sIcon = "component_concentration";
			sText = sText .. " (" .. Interface.getString("corruption_mind") .. ")";
		else
			sNodeWounds = "health.corruption.spirit";
			sIcon = "char_soul";
			sText = sText .. " (" .. Interface.getString("corruption_soul") .. ")";
		end
	else
		-- TODO: Damage
		return;
	end
	-- Handle minor and major healing
	if not (rAction.sSubtype == "power_label_heal_vigor" or rAction.sSubtype == "power_label_heal_essence" or rAction.bTemporal) then
		table.insert(aNotifications, " [" .. rAction.sTypeHealing .. "]");
		if not rAction.bMajorHealing and (rAction.sSubtype == "power_label_heal_vit" or rAction.sSubtype == "power_label_heal_stat") then
			sMode = "chat_unknown";
			local nPen = DB.getValue(nodeTarget, "health.penal.vigor.misc", 0);
			DB.setValue(nodeTarget, "health.penal.vigor.misc", "number", nPen - 1);
			table.insert(aNotifications, string.format(Interface.getString("heal_chat_penalizer"), nPen - 1));
		end
	end

	-- Handle effect removal
	if nTotal > 0 then
		if rAction.sSubtype == "power_label_heal_vit" and not rAction.bTemporal then
			EffectManager.removeEffect(ActorManager.getCTNode(rTarget), "dying");
		elseif rAction.sSubtype == "power_label_heal_vigor" and not rAction.bTemporal then
			EffectManager.removeEffect(ActorManager.getCTNode(rTarget), "unconscious");
		end
	end

	-- Get current values
	local nWounds, nWounds2 = 0, 0;
	if sNodeWounds ~= "" then
		nWounds = DB.getValue(nodeTarget, sNodeWounds, 0);
	end
	if sNodeWounds2 ~= "" then
		nWounds2 = DB.getValue(nodeTarget, sNodeWounds2, 0);
	end
	-- Apply
	if rAction.bTemporal then
		-- Temporal values are replaced if total is higher
		if nTotal > nWounds then
			DB.setValue(nodeTarget, sNodeWounds, "number", nTotal);

			-- Add effect if required
			if rAction.bAddEffect then
				if rAction.bConc then
					local rConc = {};
					rConc.nDur = rAction.nDur;
					rConc.nLevel = ActionSkill.getCastStatus(rSource)
					rAction.sStat, rAction.sSkill = ActorManager2.getBestGift(rSource);
					rConc.nID = math.random(DataCommon.conc_ID_max);
					rConc.nHiddenSpell = tonumber(rAction.nHiddenSpell);

					local rNewEffect = {};
					rNewEffect.sName = rAction.sNameSpell .. "; ";
					if rAction.sSubtype == "power_label_heal_vit" then
						rNewEffect.sName = rNewEffect.sName .. DataCommon.keyword_inactive["temp_vit"];
					elseif rAction.sSubtype == "power_label_heal_vigor" then
						rNewEffect.sName = rNewEffect.sName .. DataCommon.keyword_inactive["temp_vigor"];
					end 
					rNewEffect.nDuration = 0;

					EffectManagerSS.addConcentrationEffect(rSource, {rTarget}, rNewEffect, rConc, false)
				end
				
			end

			-- Add effect if required
			if rAction.bAddEffect then
				if rAction.bConc then
					local rConc = {};
					rConc.nDur = rAction.nDur;
					rConc.nLevel = ActionSkill.getCastStatus(rSource)
					rAction.sStat, rAction.sSkill = ActorManager2.getBestGift(rSource);
					rConc.nID = math.random(DataCommon.conc_ID_max);
					rConc.nHiddenSpell = tonumber(rAction.nHiddenSpell);

					local rNewEffect = {};
					rNewEffect.sName = rAction.sNameSpell .. "; ";
					if rAction.sSubtype == "power_label_heal_vit" then
						rNewEffect.sName = rNewEffect.sName .. DataCommon.keyword_inactive["temp_vit"];
					elseif rAction.sSubtype == "power_label_heal_vigor" then
						rNewEffect.sName = rNewEffect.sName .. DataCommon.keyword_inactive["temp_vigor"];
					end 
					rNewEffect.nDuration = 0;

					EffectManagerSS.addConcentrationEffect(rSource, {rTarget}, rNewEffect, rConc, false)
				end
				
			end
			
		else
			table.insert(aNotifications, Interface.getString("heal_chat_previoushigh"):format(nWounds));
		end
	else
		-- Heal reduces damage
		if nWounds == 0 then
			if sNodeWounds2 ~= "" and nWounds2 > 0 then
				if nTotal >= nWounds2 then
					table.insert(aNotifications, Interface.getString("heal_chat_restored"));
					nTotal2 = nWounds2;
					DB.setValue(nodeTarget, sNodeWounds2, "number", 0);
				else
					DB.setValue(nodeTarget, sNodeWounds2, "number", nWounds2 - nTotal);
					nTotal2 = nTotal;
				end
			else
				table.insert(aNotifications, Interface.getString("heal_chat_undamaged"));
			end
		elseif nTotal >= nWounds then
			DB.setValue(nodeTarget, sNodeWounds, "number", 0);
			nTotal = nTotal - nWounds;
			if sNodeWounds2 ~= "" and nWounds2 > 0 then
				if nTotal >= nWounds2 then
					table.insert(aNotifications, Interface.getString("heal_chat_restored"));
					DB.setValue(nodeTarget, sNodeWounds2, "number", 0);
					nTotal2 = nWounds2;
				else
					DB.setValue(nodeTarget, sNodeWounds2, "number", nWounds2 - nTotal);
					nTotal2 = nTotal;
				end
			else
				table.insert(aNotifications, Interface.getString("heal_chat_restored"));				
			end
		else
			DB.setValue(nodeTarget, sNodeWounds, "number", nWounds - nTotal);
		end
	end
	messageHeal(rSource, rTarget, sIcon, sMode, nTotal, sText, aNotifications);
	if nTotal2 > 0 then
		messageHeal(rSource, rTarget, sIcon, sMode, nTotal2, sText2, aNotifications);
	end

	-- TODO: Message

	
end

function messageHeal(rSource, rTarget, sIcon, sMode, nTotal, sHeal, aNotifications)
	local msgShort = {font = "msgfont", icon=sIcon, mode=sMode};
	local msgLong = {font = "msgfont", icon=sIcon, mode=sMode};
	msgShort.text = tostring(nTotal) .. " " .. sHeal;
	msgLong.text = tostring(nTotal) .. " " .. sHeal;

	if rTarget then
		msgShort.text = msgShort.text .. " ->" .. " [to " .. ActorManager.getDisplayName(rTarget) .. "]";
		msgLong.text = msgLong.text .. " ->" .. " [to " .. ActorManager.getDisplayName(rTarget) .. "]";
	end

	if #aNotifications > 0 then
		msgLong.text = msgLong.text .. " " .. table.concat(aNotifications, " ");
	end

	ActionsManager.outputResult(bSecret, rSource, rTarget, msgLong, msgShort);
end



--
-- UTILITY FUNCTIONS
--

function encodeHealClauses(rRoll)
	for _,vClause in ipairs(rRoll.clauses) do
		local sDice = StringManager.convertDiceToString(vClause.dice, vClause.modifier);
		rRoll.sDesc = rRoll.sDesc .. string.format(" [CLAUSE: (%s) (%s) (%s)]", sDice, vClause.stat or "", vClause.statmult or 1);
	end
end

function decodeHealClauses(rRoll)
	-- Process each type clause in the damage description
	rRoll.clauses = {};
	for sDice, sStat, sStatMult in string.gmatch(rRoll.sDesc, "%[CLAUSE: %(([^)]*)%) %(([^)]*)%) %(([^)]*)%)]") do
		local rClause = {};
		rClause.dice, rClause.modifier = StringManager.convertStringToDice(sDice);
		rClause.stat = sStat;
		rClause.statmult = tonumber(sStatMult) or 1;

		table.insert(rRoll.clauses, rClause);
	end

	-- Remove heal clause information from roll description
	rRoll.sDesc = string.gsub(rRoll.sDesc, " %[CLAUSE:[^]]*%]", "");
end

function getHealTypeName(sString)
	local sType = "";
	if sString == "power_label_heal_vit" then
		sType = "heal";
	elseif sString == "power_label_heal_vigor" then
		sType = "recover";
	elseif sString == "power_label_heal_vit_temp" then
		sType = "tempvit";
	elseif sString == "power_label_heal_vigor_temp" then
		sType = "tempvigor";
	elseif sString == "power_label_heal_stat" then
		sType = "stat";
	elseif sString == "power_label_heal_essence" then
		sType = "renewal";
	elseif sString == "power_label_heal_madness_temp" then
		sType = "madness_temp";
	elseif sString == "power_label_heal_spirit_temp" then
		sType = "spirit_temp";
	elseif sString == "power_label_heal_madness_perm" then
		sType = "madness_perm";
	elseif sString == "power_label_heal_spirit_perm" then
		sType = "spirit_perm";
	elseif sString == "power_label_heal_damage" or sString == "" then
		sType = "damage";
	end
	
	return sType;
end

function getDescriptionName(sType)
	local sDesc = "";if sType:match("heal") then
		sDesc = Interface.getString("power_label_heal_vit");
	elseif sType:match("recover") then
		sDesc = Interface.getString("power_label_heal_vigor");
	elseif sType:match("tempvit") then
		sDesc = Interface.getString("power_label_heal_vit_temp");
	elseif sType:match("tempvigor") then
		sDesc = Interface.getString("power_label_heal_vigor_temp");
	elseif sType:match("stat") then
		sDesc = Interface.getString("power_label_heal_stat");
	end
	return sDesc;
end

function isHealName(sName)
	if not sName or type(sName) ~= "string" then
		return false;
	end
	local bResult = sName == "heal" or sName == "recover" or sName == "stat" or sName == "renewal" or sName:match("temp") or sName:match("madness") or sName:match("spirit") ;
	return bResult;
end

function isHealMagicName(sName)
	if not sName or type(sName) ~= "string" then
		return false;
	end
	local bResult = isHealName(sName) and sName:match("magic");
	return bResult;
end

function avoidProt(sType)
	local bCond = false;
	if sType:match("stat") then
		bCond = true;
	end
	return bCond;
end

function applyRegeneration(rTarget, aDice, nMod, bTrue, nTurns)

	-- If not wounded, then return
	if #(aDice) == 0 and nMod == 0 then
		return;
	end
	local nPercentWounded= ActorHealthManager2.getPercentWounded(rTarget, true);
	if nPercentWounded == 0 then
		return;
	end

	-- Manage different turns at the same time
	local aDiceFinal = {}
	if (nTurns or 1) > 1 then
		nMod = nMod * nTurns;
		for ind = 1,nTurns do
			aDiceFinal = Utilities.concatenateArrays(aDiceFinal, aDice);
		end
	else
		aDiceFinal = aDice;
	end

	-- Regeneration cost
	local rAction = {};
	rAction.bSecret = false;
	rAction.bSelfTarget = true;
	if not bTrue then
		-- Minor regeneration spends Vigor
		rAction.sDamageTypes = DataCommon.dmgtypes_modifier["unavoidable"] .. ", " .. DataCommon.dmgtypes_modifier["nonlethal"];
		rAction.nTotal = nTurns or 1;
		rAction.aNotifications = {Interface.getString("damage_chat_regeneration_minor")};
		ActionDamage.applyDamage(rTarget, rTarget, rAction);
	else
		-- Major regeneration may spend essence
		rAction.nTotal = 1;
		local nodeCT = ActorManager2.getCTNode(rTarget);
		local nSpentEssenceByRegen = DB.getValue(nodeCT, "nSpentEssenceByRegen", 0);

		if nSpentEssenceByRegen == 0 then
			local nodeActor = ActorManager2.getActorAndNode(rTarget);
			local nCurrentEssence = DB.getValue(nodeActor, "health.essence.max") - DB.getValue(nodeActor, "health.essence.damage") - DB.getValue(nodeActor, "encumbrance.essence");
			if nCurrentEssence > 0 then
				rAction.sDamageTypes = DataCommon.dmgtypes_modifier["unavoidable"] .. ", " .. DataCommon.dmgtypes_special["essence"];
				rAction.aNotifications = {Interface.getString("damage_chat_regeneration_major")};
				ActionDamage.applyDamage(rTarget, rTarget, rAction);
				DB.setValue(nodeCT, "nSpentEssenceByRegen", "number", 1);
			else
				msg = {font = "msgfont", icon = "roll_trueheal", mode="chat_miss", text=Interface.getString("heal_chat_regeneration_notenoughessence")};
				Comm.deliverChatMessage(msg);
				return;
			end
		end
	end

	-- Regenerate
	rAction = {};
	rAction.label = Interface.getString("heal_chat_regeneration");
	rAction.sTypeHealing = rAction.label;
	rAction.sSubtype = "power_label_heal_vit";
	rAction.sTargeting = "self";
	if #aDiceFinal == 0 then
		rAction.nAvoidNotifyAction = 1;
	-- else
	-- 	rAction.nAvoidNotifyAction = 0; 
	end
	rAction.nMajorHealing = 1;
	rAction.aDice = aDiceFinal;
	rAction.nMod = nMod;

	ActionHeal.performRoll(nil, rTarget, rAction);
end
