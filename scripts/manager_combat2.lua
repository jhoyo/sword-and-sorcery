--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_NOTIFYSPEED = "notifyspeed";
OOB_MSGTYPE_USEDEFENSE = "usedefense";
OOB_MSGTYPE_CALCULATEDEF = "calculatedef";

function onInit()
	CombatManager.setCustomSort(CombatManager.sortfuncDnD);
	CombatManager.setCustomPreDeleteCombatantHandler(onPreCombatantDelete);
	CombatManager.rollTypeInit = rollTypeInit -- Override CoreRPG
	 
	CombatRecordManager.setRecordTypePostAddCallback("npc", onNPCPostAdd);
	CombatRecordManager.setRecordTypePostAddCallback("charsheet", onPCPostAdd);
	ActorCommonManager.setRecordTypeSpaceReachCallback("npc", getNPCSpaceReach);

	-- CombatManager.setCustomAddNPC(addNPC);
	-- CombatManager.setCustomAddPC(addPC);
	-- CombatManager.setCustomNPCSpaceReach(getNPCSpaceReach);

	CombatManager.setCustomRoundStart(onRoundStart);
	CombatManager.setCustomTurnStart(onTurnStart);
	CombatManager.setCustomTurnEnd(onTurnEnd);
	CombatManager.setCustomCombatReset(resetInit);

	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_NOTIFYSPEED, currentSpeedMessage);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_USEDEFENSE, handleUseDefense);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_CALCULATEDEF, handleRecalculateDefense);

	-- Interface.setDistanceDiagMult(1.5); -- Seems not to work
end


function onPreCombatantDelete(nodeCT)
	Debug.chat("Quitando")
	if not nodeCT then
		return;
	end

	-- Remember node name
	local sNode = nodeCT.getNodeName();

	-- Clear any effects first, so that saves aren't triggered when initiative advanced
	EffectManagerSS.removeAllEffectsOfOwner(nodeCT);

	-- Move to the next actor, if this CT entry is active
	if DB.getValue(nodeCT, "active", 0) == 1 then
		CombatManager.nextActor();
	end

	-- Add death markers
	if ActorHealthManager.isDyingOrDead(nodeCT) then
		Debug.chat("Añadiendo marcador de muerte")
		ImageDeathMarkerManager.addMarker(nodeCT);
	end
end

function handleUseDefense(msgOOB)
	local rActor = ActorManager.resolveActor(msgOOB.sSourceNode);
	local sDefNode = msgOOB.sDefNode;
	local nodeDefense = DB.findNode(sDefNode);
	if nodeDefense then
		local nCurrent = nodeDefense.getValue() or 0;
		nodeDefense.setValue(nCurrent + 1);
		if rActor then
			recalculateDefenses(rActor);
		end
	end
end

function startCombat()
	-- Set round to 1
	DB.setValue(DB.getRoot(), CombatManager.CT_ROUND, "number", 1);

	-- Actions that must be performed individually
	local nodeList = DB.findNode(CombatManager.CT_LIST);
	for _,nodeCT in pairs(nodeList.getChildren()) do
		-- Reset defenses
		local rActor = ActorManager.resolveActor(nodeCT);
		resetDefenseUses(rActor);
		-- Reset actions
		resetUsedActions(nodeCT);
	end

	-- New initiatives
	local nodeActive = CombatManager.getActiveCT();
	DB.setValue(nodeActive, "active", "number", 0);
	rollInit();
end

function endCombat()
	-- Deselect current CT entry
	local nodeActive = CombatManager.getActiveCT();
	DB.setValue(nodeActive, "active", "number", 0);

	-- No rounds
	DB.setValue(DB.getRoot(), CombatManager.CT_ROUND, "number", 0);

	-- Actions that must be performed individually
	local nodeList = DB.findNode(CombatManager.CT_LIST);
	for _,nodeCT in pairs(nodeList.getChildren()) do
		-- Reset init
		local nBonus = DB.getValue(nodeCT, "initiative.show", 0);
		DB.setValue(nodeCT, "initresult", "number", nBonus);
		-- Reset defenses
		local rActor = ActorManager.resolveActor(nodeCT);
		resetDefenseUses(rActor);
		-- Reset actions
		resetUsedActions(nodeCT);
		-- Remove style effects
		EffectManager.removeEffect(nodeCT, DataCommon.keyword_inactive["style"]);
		-- Regeneration 
		DB.setValue(nodeCT, "nSpentEssenceByRegen", "number", 0);
		local aDice, nMod = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["REGEN"], false, {}, rActor, false, nil, true);

		ActionHeal.applyRegeneration(rActor, aDice or {}, nMod, false, DataCommon.endcombat_regen_vitality);

		aDice, nMod = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["TRUEREGEN"], false, {}, rActor, false, nil, true);
		ActionHeal.applyRegeneration(rActor, aDice or {}, nMod, true, DataCommon.endcombat_regen_vitality);

		DB.setValue(nodeCT, "nSpentEssenceByRegen", "number", 0);
		
	end

	-- Do not show date for just 5 minutes
	CalendarManager.adjustMinutes(DataCommon.endcombat_minutes)

end


function currentSpeedMessage(msgOOB)
	local rActor = ActorManager.resolveActor(msgOOB.sSourceNode);
	local nodeActor, rActor = ActorManager2.getActorAndNode(rActor);

	-- Get the info
	local nSprint = DB.getValue(nodeActor, "speed.show.sprint", 0);
	local nRun = DB.getValue(nodeActor, "speed.show.run", 0);
	local nWalk = DB.getValue(nodeActor, "speed.show.walk", 0);
	local nFast = DB.getValue(nodeActor, "speed.show.fast", 0);
	local nCareful = DB.getValue(nodeActor, "speed.show.careful", 0);

	-- Build the text
	local sMessage = "";
	local sMode = "chat_success";
	if nSprint > 0 then
		sMessage = Interface.getString("char_speed_sprint") .. ": " .. tostring(nSprint) .. "; ";
	else
		sMode = "chat_unknown";
	end
	if nRun > 0 then
		sMessage = sMessage .. Interface.getString("char_speed_run") .. ": " .. tostring(nRun) .. "; ";
	else
		sMode = "chat_miss";
	end
	if nFast > 0 then
		sMessage = sMessage .. Interface.getString("char_speed_fast") .. ": " .. tostring(nFast) .. "; ";
	end
	if nWalk > 0 then
		sMessage = sMessage .. Interface.getString("char_speed_walk") .. ": " .. tostring(nWalk);
	else
		sMode = "chat_miss_crit";
		sMessage = Interface.getString("char_speed_careful") .. ": " .. tostring(nCareful);
	end

	-- Find the target of the message
	local sTarget = nodeActor.getOwner();

	-- Message
	msg = {icon = "action_move", mode = sMode, text=sMessage};
	Comm.deliverChatMessage(msg, "");
	if sTarget ~= "" then
		Comm.deliverChatMessage(msg, sTarget);
	end
end


-----------------
-- ACTIONS
-----------------


function getUsedActions(nodeCT)
	local nTotal = DB.getValue(nodeCT, "total_used", 0);
	if OptionsManager.isOption("Turns", "cost") then
		return 0, 0, 0, nTotal;
	else
		local nActions = DB.getValue(nodeCT, "main_actions_used", 0);
		local nFast = DB.getValue(nodeCT, "fast_actions_used", 0);
		local nReactions = DB.getValue(nodeCT, "reactions_used", 0);
		local nAttacks = DB.getValue(nodeCT, "attack_actions_used", 0);
		return nActions, nFast, nReactions, nTotal, nAttacks;
	end
end

function getMaxActions(nodeCT, sType)
	-- Direct
	local nActions = DataCommon.max_actions["action"];
	local nFast = DataCommon.max_actions["fast"] + DB.getValue(nodeCT, "extra_fast", 0);
	local nReactions = DataCommon.max_actions["reaction"];
	local nTiring = DataCommon.max_actions["tiring"];
	local nReserved = DB.getValue(nodeCT, "reserved_actions", 0);
	local nAttacks = 0;

	-- Aptitudes
	if not OptionsManager.isOption("Turns", "cost") then
		local rActor = ActorManager.resolveActor(nodeCT); 
		if not sType or sType == "action" then
			if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["more_actions"]) then
				nActions = nActions + 1;
			end
			if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["less_actions"]) then
				nActions = nActions - 1;
			end
		end
		if not sType or sType == "reaction" then
			if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["more_reactions"]) then
				nReactions = nReactions + 1;
			end
			if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["less_reactions"]) then
				nReactions = nReactions - 1;
			end
		end
		if not sType or sType == "fast" then
			if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["less_fast"]) then
				nFast = nFast - 1;
			end
			if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["more_fast"]) then
				nFast = nFast + 1;
			end
			if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["slow"]) then
				nFast = nFast - 1;
			end
		end
		if not sType or sType == "fast" or sType == "reaction" then
			if ActorManager2.hasAptitude(rActor, DataCommon.aptitude["very_slow"]) then
				nFast = nFast - 1;
				nReactions = nReactions - 1;
			end
		end
		if not sType or sType == "tiring" then
			nTiring = nTiring + ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["untiring"]);
		end

		if not sType or sType == "attack" then
			nAttacks = nAttacks + ActorManager2.getAptitudeCategory(rActor, DataCommon.aptitude["multiattack"]);
		end

		-- States
		local nState = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {});
		if not sType or sType == "action" then
			nActions = nActions + nState + EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {DataCommon.type_actions["action"]});
		end
		if not sType or sType == "fast" then	
			nFast = nFast + nState + EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {DataCommon.type_actions["fast"]});
		end
		if not sType or sType == "reaction" then
			nReactions = nReactions + nState + EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {DataCommon.type_actions["reaction"]});
		end
		if not sType or sType == "tiring" then
			nTiring = nTiring + nState + EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {DataCommon.type_actions["tiring"]});
		end

		-- States
		local nState = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {});
		if not sType or sType == "action" then
			nActions = nActions + nState + EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {DataCommon.type_actions["action"]});
		end
		if not sType or sType == "fast" then	
			nFast = nFast + nState + EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {DataCommon.type_actions["fast"]});
		end
		if not sType or sType == "reaction" then
			nReactions = nReactions + nState + EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {DataCommon.type_actions["reaction"]});
		end
		if not sType or sType == "tiring" then
			nTiring = nTiring + nState + EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["ACTIONS"]}, true, {DataCommon.type_actions["tiring"]});
		end

		-- Modify by initiative
		local nInit = DB.getValue(nodeCT, "init_start_turn", 0);
		if sType ~= "tiring" then
			if nInit < 0 and nInit > -11 then
				nFast = nFast - 1;
			elseif nInit < -10 then
				local nFive = math.ceil(nInit / 5);
				local nTwo = math.floor(nFive / 2);
				local nTwo2 = math.ceil(nFive / 2);
				nActions = nActions + nTwo;
				nFast = nFast + nTwo2 - nTwo;
			elseif OptionsManager.isOption("Turns", "once") then
				local nTen = math.floor(nInit / 10);
				local nRem = math.fmod(nInit, 10);
				nActions = nActions + nTen;
				nReactions = nReactions + nTen;
				nFast = nFast + nTen + ((nRem>4 and 1 or 0));
			end
		end


		-- Check no max is below 0
		nActions = math.max(nActions, 0);
		nFast = math.max(nFast, 0);
		nReactions = math.max(nReactions, 0);
		nTiring = math.max(nTiring, 0);

	else
		-- When actions by cost, only show one of these
		nActions = 1;
		nFast = 1;
		nReactions = 1; 
	end

	-- Some effects remove the possibility of actions and reactions
	local bNoActions = EffectManagerSS.hasAnyEffectCondition(nodeCT, DataCommon.conditions_not_actions);
	local bNoReactions;
	if not sType or sType == "reaction" then
		bNoReactions = EffectManagerSS.hasAnyEffectCondition(nodeCT, DataCommon.conditions_not_reactions);
	end
	if bNoActions then
		nActions = 0;
		nFast = 0;
		nReactions = 0;
	elseif bNoReactions then
		nReactions = 0;
	end


	-- Returns
	if not sType then
		return nActions, nFast, nReactions, nTiring, nReserved, nAttacks;
	elseif sType == "action" then
		return nActions;
	elseif sType == "fast" then
		return nFast;
	elseif sType == "reaction" then
		return nReactions;
	elseif sType == "tiring" then
		return nTiring;
	elseif sType == "attack" then
		return nAttacks;
	end

end

function resetActions(nodeCT)
	local nValue = DB.getValue(nodeCT, "reserved_actions", 0);
	DB.setValue(nodeCT, "reserved_actions", "number", nValue+1);
	DB.setValue(nodeCT, "reserved_actions", "number", nValue);
end


function useAction(nodeCT, sType, bMaybeFast, bAttack)
	if sType and type(sType) == "string" then
		-- Certain Initiative type uses init as cost
		if OptionsManager.isOption("Turns", "cost") then
			Debug.chat("To be implemented")
			return;
		end

		-- Get info
		local nActions, nFast, nReactions, nTotalActions, nAttacksUsed = getUsedActions(nodeCT);
		local nTotal = getMaxActions(nodeCT, sType);
		local sText = "";
		local sMode = "";
		local sIcon = "";
		local rActor = ActorManager.resolveActor(nodeCT);
		local nMaxActions = getMaxActions(nodeCT, "tiring");
		local nReserved = DB.getValue(nodeCT, "reserved_actions", 0);
		local bVigor = false;
		local bTotal = false;

		-- Sometimes an action should be transformed to fast
		if bMaybeFast and sType == "action" then
			if DB.getValue(nodeCT, "action_transformed", 0) == 0 then
				sType = "fast";
				DB.setValue(nodeCT, "action_transformed", "number", 1);
			else
				DB.setValue(nodeCT, "action_transformed", "number", 0);
			end
		end

		-- Action
		if sType == "action" then
			if nActions < nTotal then
				DB.setValue(nodeCT, "main_actions_used", "number", nActions + 1);
				bTotal = true;
				bVigor = nTotalActions >= nMaxActions;
			else
				sText = "error_action";
				sMode = "chat_miss";
				sIcon = "action_action";
			end
		elseif sType == "fast" then
			if nFast < nTotal then
				DB.setValue(nodeCT, "fast_actions_used", "number", nFast + 1);
				bTotal = true;
				bVigor = nTotalActions >= nMaxActions;
			elseif nActions < getMaxActions(nodeCT, "action") then
				DB.setValue(nodeCT, "extra_fast", "number", DB.getValue(nodeCT, "extra_fast", 0) + 1);
				DB.setValue(nodeCT, "extra_fast", "number", DB.getValue(nodeCT, "extra_fast", 0) + 1);
				DB.setValue(nodeCT, "main_actions_used", "number", nActions + 1);
				bTotal = true;
				bVigor = nTotalActions >= nMaxActions;
				sText = "error_substitution";
				sMode = "chat_unknown";
				sIcon = "action_fast";
			else
				sText = "error_fast";
				sMode = "chat_miss";
				sIcon = "action_fast";
			end
		elseif sType == "reserved" then
			if nReserved > 0 then
				DB.setValue(nodeCT, "reserved_actions", "number", nReserved - 1);
			else				
				return useAction(nodeCT, "action");
			end
		elseif sType == "reserved" then
			if nReserved > 0 then
				DB.setValue(nodeCT, "reserved_actions", "number", nReserved - 1);
			else				
				return useAction(nodeCT, "action");
			end
		elseif sType == "reaction" then
			if nReactions < nTotal then
				DB.setValue(nodeCT, "reactions_used", "number", nReactions + 1);
				bTotal = true;
				bVigor = nTotalActions >= nMaxActions;
			else
				sText = "error_reaction";
				sMode = "chat_miss";
				sIcon = "action_reaction";
			end
		end

		-- Message of not available
		if sText ~= "" then
			local msg = {font = "msgfont", icon = "action_action", mode=sMode, text=Interface.getString(sText)};
			local msg = {font = "msgfont", icon = "action_action", mode=sMode, text=Interface.getString(sText)};
			Comm.deliverChatMessage(msg);
		end
		-- Spend vigour and actions
		if bAttack then
			local nAttacks = getMaxActions(nodeCT, "attack");
			if nAttacks > nAttacksUsed then
				DB.setValue(nodeCT, "attack_actions_used", "number", nAttacksUsed + 1);
				bVigor = false;
				bTotal = false;
			end			
		end
		if bTotal then
			DB.setValue(nodeCT, "total_used", "number", nTotalActions + 1);
		end
		if bVigor then
			local rAction = {};
			rAction.bSecret = false;
			rAction.nTotal = 1;
			rAction.sDamageTypes = DataCommon.dmgtypes_modifier["unavoidable"] .. ", " .. DataCommon.dmgtypes_modifier["nonlethal"];
			rAction.bSelfTarget = true;
			rAction.aNotifications = {Interface.getString("actions_chat_used"):format(nTotalActions+1)};
			rAction.aNotifications = {Interface.getString("actions_chat_used"):format(nTotalActions+1)};
			ActionDamage.applyDamage(rActor, rActor, rAction);
		end
		return sMode ~= "chat_miss";

	-- Several actions used at the same time
	elseif type(sType) == "table" then
		-- Check if more actions than the availables are going to be used. In that case, reserve actions
		local bActions = true;
		local nActions = DB.getValue(nodeCT, "main_actions_used", 0);
		local nReserved = DB.getValue(nodeCT, "reserved_actions", 0);
		local nMaxActions = getMaxActions(nodeCT, "action");
		local nAvailable = nMaxActions + nReserved - nActions;
		for _,sAction in pairs(sType) do
			bActions = bActions and sAction == "action";
		end
		if bActions and #sType > nAvailable then
			local nNewReserved = nMaxActions - nActions;
			if nNewReserved > 0 then
				for ind = 1, nNewReserved do
					useAction(nodeCT, "action");
				end
				for ind = 1, nNewReserved do
					recoverAction(nodeCT, "reserved");
				end

				local msg = {font = "msgfont", icon = "action_action", mode="chat_unknown", text=Interface.getString("actions_chat_reserved"):format(nNewReserved)};
				Comm.deliverChatMessage(msg);
			else
				local msg = {font = "msgfont", icon = "action_action", mode="chat_miss", text=Interface.getString("actions_chat_noactions")};
				Comm.deliverChatMessage(msg);
			end
			return false;
		end

		-- Iterate to use them
		local bSuccess = true;
		for _,sAction in pairs(sType) do
			if #sType > 1 and sAction == "action" then
				bSuccess = bSuccess and CombatManager2.useAction(nodeCT, "reserved");
			else
				bSuccess = bSuccess and CombatManager2.useAction(nodeCT, sAction);
			end
		end
		return bSuccess;
	end
end

function recoverAction(nodeCT, sType, bAvoidRecover)
	if sType and type(sType) == "string" then
		-- Get info
		local nUsedActions, nUsedFast, nUsedReactions, nTotalActions = getUsedActions(nodeCT);
		local rActor = ActorManager.resolveActor(nodeCT);
		local nMaxActions = getMaxActions(nodeCT, "tiring");
		local bVigor = false;
		
		-- Recover action
		if sType == "action" and nUsedActions > 0 then
			DB.setValue(nodeCT, "main_actions_used", "number", nUsedActions - 1);
			if not bAvoidRecover then
				DB.setValue(nodeCT, "total_used", "number", nTotalActions - 1);
				bVigor = nTotalActions > nMaxActions;
			end
		elseif sType == "fast" and nUsedFast > 0 then
			DB.setValue(nodeCT, "fast_actions_used", "number", nUsedFast - 1);
			if not bAvoidRecover then
				DB.setValue(nodeCT, "total_used", "number", nTotalActions - 1);
				bVigor = nTotalActions > nMaxActions;
			end
		elseif sType == "reserved" then
			local nReserved = DB.getValue(nodeCT, "reserved_actions", 0);
			DB.setValue(nodeCT, "reserved_actions", "number", nReserved + 1);
		elseif sType == "reaction" and nUsedReactions > 0 then
			DB.setValue(nodeCT, "reactions_used", "number", nUsedReactions - 1);
			if not bAvoidRecover then
				DB.setValue(nodeCT, "total_used", "number", nTotalActions - 1);
				bVigor = nTotalActions > nMaxActions;
			end
		end
		-- Recover vigour if required
		if bVigor then
			local rAction  = {};
			rAction.bSecret = false;
			rAction.nTotal = 1;
			rAction.sSubtype = "power_label_heal_vigor";
			rAction.sHealStat = "";
			rAction.sTypeHealing = "";
			rAction.bMagic = false;	
			rAction.bTemporal = false;
			rAction.bMajorHealing = true;
			rAction.aNotifications = {Interface.getString("actions_chat_recovered")}
			ActionHeal.applyHeal(rActor, rActor, rAction);
		end
	end
end

function resetUsedActions(nodeCT)
	DB.setValue(nodeCT, "extra_fast", "number", 0);
	if OptionsManager.isOption("Turns", "once") then
		-- Trick to trigger update of actions when it is its turn
		DB.setValue(nodeCT, "main_actions_used", "number", 1);
	end
	DB.setValue(nodeCT, "main_actions_used", "number", 0);
	DB.setValue(nodeCT, "fast_actions_used", "number", 0);
	DB.setValue(nodeCT, "reactions_used", "number", 0);
	DB.setValue(nodeCT, "total_used", "number", 0);
	DB.setValue(nodeCT, "acted", "number", 0);
	DB.setValue(nodeCT, "action_transformed", "number", 0);
	DB.setValue(nodeCT, "attack_actions_used", "number", 0);
end

function resetUsedActionsActed(nodeCT)
	recoverAction(nodeCT, "action", true);
	recoverAction(nodeCT, "fast", true);
	recoverAction(nodeCT, "reaction", true);
	DB.setValue(nodeCT, "acted", "number", 1);
end

function inCombat()
	local nRound = DB.getValue(DB.getRoot(), CombatManager.CT_ROUND, 0);
	if nRound < 1 then
		return false;
	end
	local nodeList = DB.findNode(CombatManager.CT_LIST);
	for _, node in pairs(nodeList.getChildren()) do
		if CombatManager.getFactionFromCT(node) ~= "friend" then
			-- TODO: Check if the character can attack (armed, not unconscious, prone, asleep, etc.)
			return true;
		end
	end
	return false;
end

--
-- TURN FUNCTIONS
--

function onRoundStart(nCurrent)
	rollInit();
end

function onTurnStart(nodeEntry)
	if not nodeEntry then
		return;
	end
	local rActor = ActorManager.resolveActor(nodeEntry);

	-- Store current init
	DB.setValue(nodeEntry, "init_start_turn", "number", DB.getValue(nodeEntry, "initresult", 0));

	-- Recalculate speed and print them in screen
	CharManager.calculateSpeed(rActor);
	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_NOTIFYSPEED;
	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rActor);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;
	Comm.deliverOOBMessage(msgOOB, "");


	-- If already acted, just recover some actions
	local bActed = DB.getValue(nodeEntry, "acted", 0) == 1;
	if bActed then
		resetUsedActionsActed(nodeEntry);

	-- Real start turn
	else
		-- Reset actions
		resetUsedActions(nodeEntry);

		-- Reset defenses
		resetDefenseUses(rActor);

		-- Start turn of effects
		for _,nodeEffect in pairs(DB.getChildren(nodeEntry, "effects")) do
			EffectManagerSS.onEffectActorStartTurn(nodeEntry, nodeEffect);
		end

		-- Check if Concentration check is needed
		if EffectManagerSS.numberOfEffectConditions(rActor, DataCommon.conditions_concentration_start_turn) > 0 then
			ActionDamage.makeConcentrationChecks(rActor);
		end
		-- Check for death saves (TODO)
	end
	resetActions(nodeEntry);

	-- Save that character acted
	DB.setValue(nodeEntry, "acted", "number", 1)
end

function onTurnEnd(nodeEntry)
	-- End turn of effects
	if OptionsManager.isOption("EffectDuration", "end") then
		for _,nodeEffect in pairs(DB.getChildren(nodeEntry, "effects")) do
			EffectManagerSS.onEffectActorEndTurn(nodeEntry, nodeEffect);
		end
	end
end


function nextActorNotEndTurn()
	-- Check that the user can pass turn
	local nodeActive = CombatManager.getActiveCT();
	local rActor = ActorManager.resolveActor(nodeActive);
	local nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
	if nodeActor and not nodeActor.isOwner() then
		return;
	end
	if not nodeActive and not User.isHost() then
		return;
	end
	local nIndexActive = 0;

	-- Check the skip hidden NPC option
	local bSkipHidden = OptionsManager.isOption("CTSH", "on");

	-- Determine the next actor
	local nodeNext = nil;
	local aEntries = CombatManager.getSortedCombatantList();
	if #aEntries > 0 then
		if nodeActive then
			for i = 1,#aEntries do
				if aEntries[i] == nodeActive then
					nIndexActive = i;
					break;
				end
			end
		end
		if bSkipHidden then
			local nIndexNext = 0;
			for i = nIndexActive + 1, #aEntries do
				if DB.getValue(aEntries[i], "friendfoe", "") == "friend" then
					nIndexNext = i;
					break;
				else
					if not CombatManager.isCTHidden(aEntries[i]) then
						nIndexNext = i;
						break;
					end
				end
			end
			if nIndexNext > nIndexActive then
				nodeNext = aEntries[nIndexNext];
				for i = nIndexActive + 1, nIndexNext - 1 do
					CombatManager.showTurnMessage(aEntries[i], false);
				end
			end
		else
			nodeNext = aEntries[nIndexActive + 1];
		end
	end

	-- If next actor available, advance effects, activate and start turn
	if nodeNext then
		-- Check if the current character has high enough initiative to act again
		local nCurrent = DB.getValue(nodeActive,"initresult", 0);
		local nNext = DB.getValue(nodeNext,"initresult", 0);

		if (nCurrent >= 10) and (nCurrent - nNext > 10) then
			-- Act again
			DB.setValue(nodeActive,"initresult", "number", nCurrent-10);
			CombatManager.onTurnStartEvent(nodeActive);

		else

			-- -- Process effects in between current and next actors
			-- if nodeActive then
			-- 	CombatManager.onInitChangeEvent(nodeActive, nodeNext);
			-- else
			-- 	CombatManager.onInitChangeEvent(nil, nodeNext);
			-- end

			-- Start turn for next actor
			if User.isHost() then
				CombatManager.requestActivation(nodeNext, bSkipBell);
				CombatManager.onTurnStartEvent(nodeNext);
			else
				CombatManager.notifyEndTurn();
			end

			-- Reduce initiative to act again if necessary
			if nCurrent >= 10 then
				DB.setValue(nodeActive,"initresult", "number", nCurrent-10);
			else
				-- End turn for current actor
				CombatManager.onTurnEndEvent(nodeActive);
			end
		end

	elseif not bNoRoundAdvance then
		if bSkipHidden then
			for i = nIndexActive + 1, #aEntries do
				CombatManager.showTurnMessage(aEntries[i], false);
			end
		end
		CombatManager.nextRound(1);
	end

end

function isCurrentTurn(nodeEntry)
	return CombatManager.getActiveCT() == nodeEntry;
end

--
-- ADD FUNCTIONS
--

function parseResistances(sResistances)
	local aResults = {};
	sResistances = sResistances:lower();

	for _,v in ipairs(StringManager.split(sResistances, ";\r", true)) do
		local aResistTypes = {};

		for _,v2 in ipairs(StringManager.split(v, ",", true)) do
			if StringManager.isWord(v2, DataCommon.dmgtypes) then
				table.insert(aResistTypes, v2);
			else
				local aResistWords = StringManager.parseWords(v2);

				local i = 1;
				while aResistWords[i] do
					if StringManager.isWord(aResistWords[i], DataCommon.dmgtypes) then
						table.insert(aResistTypes, aResistWords[i]);
					elseif StringManager.isWord(aResistWords[i], "cold-forged") and StringManager.isWord(aResistWords[i+1], "iron") then
						i = i + 1;
						table.insert(aResistTypes, "cold-forged iron");
					elseif StringManager.isWord(aResistWords[i], "from") and StringManager.isWord(aResistWords[i+1], "nonmagical") and StringManager.isWord(aResistWords[i+2], { "weapons", "attacks" }) then
						i = i + 2;
						table.insert(aResistTypes, "!magic");
					elseif StringManager.isWord(aResistWords[i], "that") and StringManager.isWord(aResistWords[i+1], "aren't") then
						i = i + 2;

						if StringManager.isWord(aResistWords[i], "silvered") then
							table.insert(aResistTypes, "!silver");
						elseif StringManager.isWord(aResistWords[i], "adamantine") then
							table.insert(aResistTypes, "!adamantine");
						elseif StringManager.isWord(aResistWords[i], "cold-forged") and StringManager.isWord(aResistWords[i+1], "iron") then
							i = i + 1;
							table.insert(aResistTypes, "!cold-forged iron");
						end
					end

					i = i + 1;
				end
			end
		end

		if #aResistTypes > 0 then
			table.insert(aResults, table.concat(aResistTypes, ", "));
		end
	end

	return aResults;
end

function setTokenSize(nodeEntry, sSize)
	if not sSize or sSize == "" then
		sSize = StringManager.trim(DB.getValue(nodeEntry, "size", ""));
	end
	sSize = sSize:lower();
	if sSize and EffectManagerSS.valueInTable(sSize, DataCommon.sizelabels) then
		local nSize = DataCommon.creaturesizespace[sSize];
		DB.setValue(nodeEntry, "space", "number", nSize);
		local token = CombatManager.getTokenFromCT(nodeEntry);
		if token then
			token.setScale(nSize);
		end
	end
end

function onNPCPostAdd(tCustom)
	-- Parameter validation
	if not tCustom.nodeRecord or not tCustom.nodeCT then
		return;
	end

	-- Add effects
	local list = tCustom.nodeRecord.createChild("effectlist");
	for _, effect in pairs(list.getChildren()) do
		local sName = DB.getValue(effect, "name", "");
		if sName ~= "" then
			EffectManager.addEffect("", "", tCustom.nodeCT, { sName = sName, nDuration = 0, nGMOnly = 1 }, true);
		end
	end

	-- Determine size
	setTokenSize(tCustom.nodeCT);

	-- Roll initiative and sort
	rollEntryInit(tCustom.nodeCT);

	-- Change name to random number to avoid repeat
	-- local sName = tCustom.sFinalName .. " " .. extraNPCname();
	-- DB.setValue(tCustom.nodeCT, "name", "string", sName);

end

function addNPC(sClass, nodeNPC, sName, sFaction, sOwner)
	local tCustom = {nodeRecord = nodeNPC, sFaction=sFaction, sClass=sClass};

	-- Override CombatRecordManager.addNPC to be able to access modified tCustom
	if not tCustom.nodeRecord then
		Debug.chat("no hay nodeRecord")
		return;
	end
	tCustom.nodeCT = CombatManager.createCombatantNode();
	if not tCustom.nodeCT then
		Debug.chat("no hay nodeCT")
		return;
	end

	DB.copyNode(tCustom.nodeRecord, tCustom.nodeCT);
	DB.setValue(tCustom.nodeCT, "locked", "number", 1);

	-- Remove any combatant specific information
	DB.setValue(tCustom.nodeCT, "active", "number", 0);
	DB.setValue(tCustom.nodeCT, "tokenrefid", "string", "");
	DB.setValue(tCustom.nodeCT, "tokenrefnode", "string", "");
	DB.deleteChildren(tCustom.nodeCT, "effects");

	CombatRecordManager.handleStandardCombatAddFields(tCustom);
	CombatRecordManager.handleStandardCombatAddSpaceReach(tCustom);
	onNPCPostAdd(tCustom);

	-- Set PC ownership and visibility
	if sOwner then
		DB.setOwner(tCustom.nodeCT, sOwner);
	elseif sFaction ~= "friend" then
		DB.setValue(tCustom.nodeCT, "tokenvis", "number", 0);
	end

	return tCustom.nodeCT;
end

function onPCPostAdd(tCustom)
	-- Set ownership
	local sOwner = tCustom.nodeRecord.getOwner();
	if sOwner then
		DB.setOwner(tCustom.nodeCT, sOwner);
	end

	-- Set ownership
	local sOwner = tCustom.nodeRecord.getOwner();
	if sOwner then
		DB.setOwner(tCustom.nodeCT, sOwner);
	end

	-- Add effects
	local list = tCustom.nodeRecord.createChild("effectlist");
	for _, effect in pairs(list.getChildren()) do
		local sName = DB.getValue(effect, "name", "");
		if sName ~= "" then
			EffectManager.addEffect("", "", tCustom.nodeCT, { sName = sName, nDuration = 0 }, true);
		end
	end

	-- Add item effects
	local list = tCustom.nodeRecord.createChild("inventorylist");
	for _, item in pairs(list.getChildren()) do
		local nCarried = DB.getValue(item, "carried", 0);
		local listEffects = item.createChild("effectlist");
		local nEffects = listEffects.getChildCount();
		if nCarried == 2 and nEffects > 0 then
			local sName = DB.getValue(item, "name", "");
			if sName ~= "" then
				sName = sName .. "; ";
			end
			for _, effect in pairs(listEffects.getChildren()) do
				local sEffect = DB.getValue(effect, "name", "");
				if sEffect ~= "" then
					EffectManager.addEffect("", "", tCustom.nodeCT, { sName = sName .. sEffect, nDuration = 0 }, true);
				end
			end
		end
	end

	-- Roll initiative and sort
	rollEntryInit(nodeEntry)
end

function addPC(nodePC)
	-- Parameter validation
	if not nodePC then
		return;
	end

	-- Check if PC is already present
	local aCurrentCombatants = CombatManager.getCombatantNodes();
	for _, nodeCT in pairs(aCurrentCombatants) do
		nodeCombatant = ActorManager2.getNodeFromCT(nodeCT);
		if DB.getPath(nodeCombatant) == DB.getPath(nodePC) then
			return;
		end
	end

	-- Create a new combat tracker window
	local nodeEntry = DB.createChild("combattracker.list");
	if not nodeEntry then
		return;
	end
	nodeEntry.setPublic(true);

	-- Set up the CT specific information
	DB.setValue(nodeEntry, "link", "windowreference", "charsheet", nodePC.getPath());
	DB.setValue(nodeEntry, "friendfoe", "string", "friend");

	local sToken = DB.getValue(nodePC, "token", nil);
	if not sToken or sToken == "" then
		sToken = "portrait_" .. nodePC.getName() .. "_token"
	end
	DB.setValue(nodeEntry, "token", "token", sToken);

	-- Set ownership
	local sOwner = nodePC.getOwner();
	if sOwner then
		DB.setOwner(nodeEntry, sOwner);
	end

	-- Add effects
	local list = nodePC.createChild("effectlist");
	for _, effect in pairs(list.getChildren()) do
		local sName = DB.getValue(effect, "name", "");
		if sName ~= "" then
			EffectManager.addEffect("", "", nodeEntry, { sName = sName, nDuration = 0 }, true);
		end
	end

	-- Add item effects
	local list = nodePC.createChild("inventorylist");
	for _, item in pairs(list.getChildren()) do
		local nCarried = DB.getValue(item, "carried", 0);
		local listEffects = item.createChild("effectlist");
		local nEffects = listEffects.getChildCount();
		if nCarried == 2 and nEffects > 0 then
			local sName = DB.getValue(item, "name", "");
			if sName ~= "" then
				sName = sName .. "; ";
			end
			for _, effect in pairs(listEffects.getChildren()) do
				local sEffect = DB.getValue(effect, "name", "");
				if sEffect ~= "" then
					EffectManager.addEffect("", "", nodeEntry, { sName = sName .. sEffect, nDuration = 0 }, true);
				end
			end
		end
	end

	-- Roll initiative and sort
	rollEntryInit(nodeEntry)
	return nodeEntry;
end

function extraNPCname()
	local sString = tostring(math.random(DataCommon.nMaxNPC));
	return sString;
end

--
-- RESET FUNCTIONS
--

function resetInit()
	function resetCombatantInit(nodeCT)
		DB.setValue(nodeCT, "initresult", "number", 0);
		DB.setValue(nodeCT, "reaction", "number", 0);
	end
	CombatManager.callForEachCombatant(resetCombatantInit);
end

function resetHealth(nodeCT, bLong)
	if bLong then
		DB.setValue(nodeCT, "wounds", "number", 0);
		DB.setValue(nodeCT, "hptemp", "number", 0);
		DB.setValue(nodeCT, "deathsavesuccess", "number", 0);
		DB.setValue(nodeCT, "deathsavefail", "number", 0);

		EffectManager.removeEffect(nodeCT, "Stable");

		local nExhaustMod = EffectManagerSS.getEffectsBonus(ActorManager.resolveActor(nodeCT), {"EXHAUSTION"}, true);
		if nExhaustMod > 0 then
			nExhaustMod = nExhaustMod - 1;
			EffectManagerSS.removeEffectByType(nodeCT, "EXHAUSTION");
			if nExhaustMod > 0 then
				EffectManager.addEffect("", "", nodeCT, { sName = "EXHAUSTION: " .. nExhaustMod, nDuration = 0 }, false);
			end
		end
	end
end

function clearExpiringEffects()
	function checkEffectExpire(nodeEffect)
		local sLabel = DB.getValue(nodeEffect, "label", "");
		local nDuration = DB.getValue(nodeEffect, "duration", 0);
		local sApply = DB.getValue(nodeEffect, "apply", "");

		if nDuration ~= 0 or sApply ~= "" or sLabel == "" then
			nodeEffect.delete();
		end
	end
	CombatManager.callForEachCombatantEffect(checkEffectExpire);
end

----------------------------
-- DEFENSES
----------------------------

function recalculateDefenses(rActor)
	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_CALCULATEDEF;
	msgOOB.sActorType, msgOOB.sActorNode = ActorManager.getTypeAndNodeName(rActor);
	Comm.deliverOOBMessage(msgOOB, "");
end

function handleRecalculateDefense(msgOOB)
	-- Get base info
	local rActor = ActorManager.resolveActor(msgOOB.sActorNode);
	nodeActor, rActor = ActorManager2.getActorAndNode(rActor);
	if not nodeActor or nodeActor.getName() == "mount_npc" then
		return;
	end

	local nPen = ActorManager2.getPenalizer(rActor);
	DB.setValue(nodeActor, "defense.penal", "number", nPen);
	local nBase = DataCommon.defense_base;
	DB.setValue(nodeActor, "defense.base", "number", nBase);

	local sSize = DB.getValue(nodeActor, "size", DataCommon.size_default):lower();
	local nSize = 0;
	if Utilities.inArray(DataCommon.sizelabels, sSize) then
		nSize = DataCommon.size_modifier[sSize];
	end
	local nCover = EffectManagerSS.getEffectsBonus(rActor, {DataCommon.keyword_states["COVER"]}, true, {});

	local nMiscNofilter = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEF"], true, {});
	local nMiscMelee = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEF"], true, {DataCommon.rangetypes_inv["melee"]}) - nMiscNofilter + nSize + nCover;
	local nMiscDist = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEF"], true, {DataCommon.rangetypes_inv["distance"]}) - nMiscNofilter + nSize + nCover;

	-- Dodge
	local aFilter = {DataCommon.defenses_names["dodge"]};
	local nMultDist = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEFRANGEDMULT"], true, aFilter);
	if nMultDist == 0 then
		nMultDist = DataCommon.defense_mult_ranged["dodge"];
	end
	DB.setValue(nodeActor, "defense.dodge.mult", "number", nMultDist);

	local sSkill = Interface.getString("skill_value_dodge");
	if DB.getValue(nodeActor, "is_mounted", 0) > 0 then
		sSkill = Interface.getString("skill_value_mount");
		aFilter = {DataCommon.defenses_names["mount"]};
	end
	DB.setValue(nodeActor, "defense.dodge.skillcontainer", "string", sSkill);
	local nodeSkill = ActorManager2.getSkillNode(nodeActor, sSkill);
	local nSkill = ActorManager2.getSkillTotal(nodeSkill, nil, false);
	DB.setValue(nodeActor, "defense.dodge.skill.melee", "number", nSkill);
	DB.setValue(nodeActor, "defense.dodge.skill.ranged", "number", nSkill * nMultDist);

	local nStat = ActorManager2.getCurrentAbilityValue(nodeActor, "agility", false);
	DB.setValue(nodeActor, "defense.dodge.ability.melee", "number", nStat);
	DB.setValue(nodeActor, "defense.dodge.ability.ranged", "number", nStat * nMultDist);

	local nMisc = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEF"], true, aFilter);
	DB.setValue(nodeActor, "defense.dodge.misc.melee", "number", nMisc + nMiscMelee);
	DB.setValue(nodeActor, "defense.dodge.misc.ranged", "number", nMisc + nMiscDist);

	local nUsesMult = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEFMULT"], true, aFilter);
	if nUsesMult == 0 then
		nUsesMult = DataCommon.defense_mult_base["dodge"];
	end
	DB.setValue(nodeActor, "defense.dodge.uses_mult", "number", nUsesMult);
	local nUses = DB.getValue(nodeActor, "defense.dodge.uses", 0);

	local nTotal = math.floor(nBase + nStat + nSkill + nPen + nMisc - (nUsesMult * nUses) + nMiscMelee);
	DB.setValue(nodeActor, "defense.dodge.melee", "number", math.max(10, nTotal));
	nTotal = math.floor(nBase + nMultDist * (nStat + nSkill) + nPen + nMisc - (nUsesMult * nUses) + nMiscDist);
	DB.setValue(nodeActor, "defense.dodge.ranged", "number", math.max(10, nTotal));

	-- Main hand
	aFilter = {DataCommon.defenses_names["main_hand"]};

	local nEquipMelee = DB.getValue(nodeActor, "defense.main_hand.equip.melee", 0);
	local nEquipRanged = DB.getValue(nodeActor, "defense.main_hand.equip.ranged", 0);
	
	nMultDist = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEFRANGEDMULT"], true, aFilter);
	if nMultDist == 0 and nEquipRanged > 0 then
		nMultDist = 1;
	elseif nMultDist == 0 then
		nMultDist = DataCommon.defense_mult_ranged["main_hand"];
	end
	DB.setValue(nodeActor, "defense.main_hand.mult", "number", nMultDist);

	local sStat = DB.getValue(nodeActor, "defense.main_hand.atributecontainer", "");
	nStat = ActorManager2.getCurrentAbilityValue(nodeActor, sStat, false);
	DB.setValue(nodeActor, "defense.main_hand.ability.melee", "number", nStat);
	DB.setValue(nodeActor, "defense.main_hand.ability.ranged", "number", nStat * nMultDist);

	sSkill = DB.getValue(nodeActor, "defense.main_hand.skillcontainer", "");
	nodeSkill = ActorManager2.getSkillNode(nodeActor, sSkill);
	nSkill = ActorManager2.getSkillTotal(nodeSkill, nil, false);
	DB.setValue(nodeActor, "defense.main_hand.skill.melee", "number", nSkill);
	DB.setValue(nodeActor, "defense.main_hand.skill.ranged", "number", nSkill * nMultDist);

	nMisc = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEF"], true, aFilter) - nMiscNofilter;
	DB.setValue(nodeActor, "defense.main_hand.misc.melee", "number", nMisc + nMiscMelee);
	DB.setValue(nodeActor, "defense.main_hand.misc.ranged", "number", nMisc + nMiscDist);

	nUsesMult = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEFMULT"], true, aFilter);
	if nUsesMult == 0 then
		nUsesMult = DataCommon.defense_mult_base["main_hand"];
	end
	DB.setValue(nodeActor, "defense.main_hand.uses_mult", "number", nUsesMult);
	nUses = DB.getValue(nodeActor, "defense.main_hand.uses", 0);

	nTotal = math.floor(nBase + nStat + nSkill + nPen + nMisc - (nUsesMult * nUses) + nMiscMelee + nEquipMelee);
	DB.setValue(nodeActor, "defense.main_hand.melee", "number", math.max(10, nTotal));
	nTotal = math.floor(nBase + nMultDist * (nStat + nSkill) + nPen + nMisc - (nUsesMult * nUses) + nMiscDist + nEquipRanged);
	DB.setValue(nodeActor, "defense.main_hand.ranged", "number", math.max(10, nTotal));

	-- Side hand
	aFilter = {DataCommon.defenses_names["side_hand"]};

	nEquipMelee = DB.getValue(nodeActor, "defense.side_hand.equip.melee", 0);
	nEquipRanged = DB.getValue(nodeActor, "defense.side_hand.equip.ranged", 0);

	nMultDist = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEFRANGEDMULT"], true, aFilter);
	if nMultDist == 0 and nEquipRanged > 0 then
		nMultDist = 1;
	elseif nMultDist == 0 then
		nMultDist = DataCommon.defense_mult_ranged["side_hand"];
	end
	DB.setValue(nodeActor, "defense.side_hand.mult", "number", nMultDist);

	sStat = DB.getValue(nodeActor, "defense.side_hand.atributecontainer", "");
	nStat = ActorManager2.getCurrentAbilityValue(nodeActor, sStat, false);
	DB.setValue(nodeActor, "defense.side_hand.ability.melee", "number", nStat);
	DB.setValue(nodeActor, "defense.side_hand.ability.ranged", "number", nStat * nMultDist);

	sSkill = DB.getValue(nodeActor, "defense.side_hand.skillcontainer", "");
	nodeSkill = ActorManager2.getSkillNode(nodeActor, sSkill);
	nSkill = ActorManager2.getSkillTotal(nodeSkill, nil, true);
	DB.setValue(nodeActor, "defense.side_hand.skill.melee", "number", nSkill);
	DB.setValue(nodeActor, "defense.side_hand.skill.ranged", "number", nSkill * nMultDist);

	nMisc = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEF"], true, aFilter) - nMiscNofilter;
	DB.setValue(nodeActor, "defense.side_hand.misc.melee", "number", nMisc + nMiscMelee);
	DB.setValue(nodeActor, "defense.side_hand.misc.ranged", "number", nMisc + nMiscDist);

	nUsesMult = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEFMULT"], true, aFilter);
	if nUsesMult == 0 then
		nUsesMult = DataCommon.defense_mult_base["side_hand"];
	end
	DB.setValue(nodeActor, "defense.side_hand.uses_mult", "number", nUsesMult);
	nUses = DB.getValue(nodeActor, "defense.side_hand.uses", 0);

	-- Debug.chat("nBase + nStat + nSkill + nPen + nMisc + nUsesMult * nUses + nMiscMelee + nEquipMelee", nBase, nStat, nSkill, nPen, nMisc, nUsesMult * nUses, nMiscMelee, nEquipMelee)

	nTotal = math.floor(nBase + nStat + nSkill + nPen + nMisc - (nUsesMult * nUses) + nMiscMelee + nEquipMelee);
	DB.setValue(nodeActor, "defense.side_hand.melee", "number", math.max(10, nTotal));
	nTotal = math.floor(nBase + nMultDist * (nStat + nSkill) + nPen + nMisc - (nUsesMult * nUses) + nMiscDist + nEquipRanged);
	DB.setValue(nodeActor, "defense.side_hand.ranged", "number", math.max(10, nTotal));

	-- Unaware
	aFilter = {DataCommon.defenses_names["unaware"]};
	nMultDist = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEFRANGEDMULT"], true, aFilter);
	if nMultDist == 0 then
		nMultDist = DataCommon.defense_mult_ranged["unaware"];
	end
	DB.setValue(nodeActor, "defense.unaware.mult", "number", nMultDist);

	nStat = ActorManager2.getCurrentAbilityValue(nodeActor, "perception", false);
	DB.setValue(nodeActor, "defense.unaware.ability", "number", nStat * nMultDist);

	sSkill = Interface.getString("skill_value_alert");
	nodeSkill = ActorManager2.getSkillNode(nodeActor, sSkill);
	nSkill = ActorManager2.getSkillTotal(nodeSkill, nil, false);
	DB.setValue(nodeActor, "defense.unaware.skill", "number", nSkill * nMultDist);

	nMisc = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEF"], true, aFilter) - nMiscNofilter;
	DB.setValue(nodeActor, "defense.unaware.misc.melee", "number", nMisc + nMiscMelee);
	DB.setValue(nodeActor, "defense.unaware.misc.ranged", "number", nMisc + nMiscDist);
	
	nTotal = math.floor(nBase + nMultDist * (nStat + nSkill) + nPen + nMisc + nMiscMelee);
	DB.setValue(nodeActor, "defense.unaware.melee", "number", math.max(10, nTotal));
	nTotal = math.floor(nBase + nMultDist * (nStat + nSkill) + nPen + nMisc + nMiscDist);
	
	DB.setValue(nodeActor, "defense.unaware.ranged", "number", math.max(10, nTotal));

	-- Helpless
	aFilter = {DataCommon.defenses_names["helpless"]};

	nMisc = EffectManagerSS.getEffectsBonus(rActor, DataCommon.keyword_states["DEF"], true, aFilter) - nMiscNofilter;
	DB.setValue(nodeActor, "defense.helpless.misc.melee", "number", nMisc + nMiscMelee);
	DB.setValue(nodeActor, "defense.helpless.misc.ranged", "number", nMisc + nMiscDist);

	nTotal = math.floor(nBase + nMisc + nMiscMelee);
	DB.setValue(nodeActor, "defense.helpless.melee", "number", math.max(10, nTotal));
	nTotal = math.floor(nBase + nMisc + nMiscDist);
	DB.setValue(nodeActor, "defense.helpless.ranged", "number", math.max(10, nTotal));

	-- Get which is higher
	calculateMaxDefenses(rActor);

end

function calculateMaxDefenses(rDefender)

	-- Get defender node
	nodeDefender, rDefender = ActorManager2.getActorAndNode(rDefender);

	-- Calculate states that produces unawareness
	local bEntangled = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Entangled"]);
	local bStunned = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Stuned"]);
	local bBlinded = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Blinded"]);
	local bUnaware = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Unaware"]);
	local bFascinated = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Fascinated"]);
	bUnaware = bEntangled or bStunned or bBlinded or bUnaware or bFascinated;

	-- Calculate states that produces helplessness
	local bAsleep = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Asleep"]);
	local bUnconscious = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Unconscious"]);
	local bHelpless = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Helpless"]);
	local bParalized = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Paralyzed"]);
	local bPetrified = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Petrified"]);
	local bTrance = EffectManagerSS.hasEffect(nodeDefender, DataCommon.conditions["Trance"]);
	bHelpless = bHelpless or bAsleep or bUnconscious or bParalized or bPetrified or bTrance;

	-- Calculate first the helpless defense
	local nMaxMelee = DB.getValue(nodeDefender, "defense.helpless.melee", DataCommon.defense_base);
	local sMaxMelee = DataCommon.defenses_names["helpless"];
	local nMaxRanged = DB.getValue(nodeDefender, "defense.helpless.ranged", DataCommon.defense_base);
	local sMaxRanged = DataCommon.defenses_names["helpless"];

	-- If not forced to helpless, look for unaware
	if not bHelpless then
		local nAuxDef = DB.getValue(nodeDefender, "defense.unaware.melee", DataCommon.defense_base);
		if nAuxDef > nMaxMelee then
			nMaxMelee = nAuxDef;
			sMaxMelee = DataCommon.defenses_names["unaware"];
		end
		nAuxDef = DB.getValue(nodeDefender, "defense.unaware.ranged", DataCommon.defense_base);
		if nAuxDef > nMaxRanged then
			nMaxRanged = nAuxDef;
			sMaxRanged = DataCommon.defenses_names["unaware"];
		end

		-- If not unaware, look for better defenses
		if not bUnaware then
			nAuxDef = DB.getValue(nodeDefender, "defense.dodge.melee", DataCommon.defense_base);
			if nAuxDef > nMaxMelee then
				nMaxMelee = nAuxDef;
				sMaxMelee = DataCommon.defenses_names["dodge"];
			end
			nAuxDef = DB.getValue(nodeDefender, "defense.dodge.ranged", DataCommon.defense_base);
			if nAuxDef > nMaxRanged then
				nMaxRanged = nAuxDef;
				sMaxRanged = DataCommon.defenses_names["dodge"];
			end

			nAuxDef = DB.getValue(nodeDefender, "defense.main_hand.melee", DataCommon.defense_base);
			if nAuxDef >= nMaxMelee then
				nMaxMelee = nAuxDef;
				sMaxMelee = DataCommon.defenses_names["main_hand"];
			end
			nAuxDef = DB.getValue(nodeDefender, "defense.main_hand.ranged", DataCommon.defense_base);
			if nAuxDef >= nMaxRanged then
				nMaxRanged = nAuxDef;
				sMaxRanged = DataCommon.defenses_names["main_hand"];
			end

			nAuxDef = DB.getValue(nodeDefender, "defense.side_hand.melee", DataCommon.defense_base);
			if nAuxDef >= nMaxMelee then
				nMaxMelee = nAuxDef;
				sMaxMelee = DataCommon.defenses_names["side_hand"];
			end
			nAuxDef = DB.getValue(nodeDefender, "defense.side_hand.ranged", DataCommon.defense_base);
			if nAuxDef >= nMaxRanged then
				nMaxRanged = nAuxDef;
				sMaxRanged = DataCommon.defenses_names["side_hand"];
			end

		end
	end

	-- Set values
	DB.setValue(nodeActor, "defense.current.melee", "number", nMaxMelee);
	DB.setValue(nodeActor, "defense.current.melee_name", "string", sMaxMelee);
	DB.setValue(nodeActor, "defense.current.ranged", "number", nMaxRanged);
	DB.setValue(nodeActor, "defense.current.ranged_name", "string", sMaxRanged);

end

function useDefense(nodeDefense, rActor)
	if nodeDefense then
		local msgOOB = {};
		msgOOB.type = OOB_MSGTYPE_USEDEFENSE;
		msgOOB.sDefNode = DB.getPath(nodeDefense);

		local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rActor);
		msgOOB.sSourceType = sSourceType;
		msgOOB.sSourceNode = sSourceNode;

		Comm.deliverOOBMessage(msgOOB, "");
	end
end


function getDefenseValue(rAttacker, rDefender, rRoll)
	if not rDefender or not rRoll then
		return nil, nil;
	end

	-- Update defender defenses
	recalculateDefenses(rDefender);


	-- Get defender node
	-- local sDefenderType, nodeDefender = ActorManager.getTypeAndNode(rDefender);	
	nodeDefender, rDefender = ActorManager2.getActorAndNode(rDefender);

	-- Get the type of attack
	local sTypeAttack = ".melee";
	local aFilter = {DataCommon.rangetypes_inv["melee"]};
	if tonumber(rRoll.type) > 0 then
		sTypeAttack = ".ranged";
	end
	-- Debug.chat("sTypeAttack",sTypeAttack)

	-- Get targeted modifiers
	local nMiscNofilter = EffectManagerSS.getEffectsBonus(rDefender, DataCommon.keyword_states["DEF"], true, aFilter, rAttacker, true);
	local nMiscDist = EffectManagerSS.getEffectsBonus(rDefender, DataCommon.keyword_states["DEF"], true, nil, rAttacker, true);

	-- Calculate states that produces unawareness
	local bUnawareCond = EffectManagerSS.hasAnyEffectCondition(nodeDefender, DataCommon.conditions_unaware);
	local bUnawareAtt = EffectManagerSS.hasAnyEffectCondition(rAttacker, DataCommon.conditions_unaware_attack);
	local bUnaware = bUnawareCond or bUnawareAtt;
	if bUnaware and ActorManager2.hasAptitude(rDefender, DataCommon.aptitude["blind_fight"]) then
		bUnaware = false;
		nMiscNofilter = nMiscNofilter - 2;
	end

	-- Debug.chat("bUnaware",bUnaware)

	-- Calculate states that produces helplessness
	local bHelpless =  EffectManagerSS.hasAnyEffectCondition(nodeDefender, DataCommon.conditions_helpless);

	-- Calculate first the helpless defense
	local nDefense = DB.getValue(nodeDefender, "defense.helpless" .. sTypeAttack, DataCommon.defense_base);
	local nDefense = DB.getValue(nodeDefender, "defense.helpless" .. sTypeAttack, DataCommon.defense_base);
	local nodeUseDef = nil;
	aFilter = {DataCommon.defenses_names["helpless"]};
	local nMisc = EffectManagerSS.getEffectsBonus(rDefender, DataCommon.keyword_states["DEF"], true, aFilter, rAttacker, true);
	nDefense = nDefense + nMisc - nMiscNofilter;
	local sTypeDefense = "helpless";
	-- Debug.chat("helpless", nDefense)

	-- If not forced to helpless, look for unaware
	if not bHelpless then
		local nAuxDef = DB.getValue(nodeDefender, "defense.unaware".. sTypeAttack, DataCommon.defense_base);
		aFilter = {DataCommon.defenses_names["unaware"]};
		nMisc = EffectManagerSS.getEffectsBonus(rDefender, DataCommon.keyword_states["DEF"], true, aFilter, rAttacker, true);
		nAuxDef = nAuxDef + nMisc - nMiscNofilter;
		if nAuxDef > nDefense then
			nDefense = nAuxDef;
			sTypeDefense = "unaware";
		end
		-- Debug.chat("unaware", nAuxDef)

		-- If not unaware, look for better defenses
		if not bUnaware then
			nAuxDef = DB.getValue(nodeDefender, "defense.dodge"..sTypeAttack, DataCommon.defense_base);
			aFilter = {DataCommon.defenses_names["dodge"]};
			nMisc = EffectManagerSS.getEffectsBonus(rDefender, DataCommon.keyword_states["DEF"], true, aFilter, rAttacker, true);
			nAuxDef = nAuxDef + nMisc - nMiscNofilter;
			if nAuxDef > nDefense then
				nDefense = nAuxDef;
				nodeUseDef = DB.getChild(nodeDefender, "defense.dodge.uses");
				sTypeDefense = "dodge";
				-- Debug.chat("nodeUseDef dodge", nodeUseDef)
			end
			-- Debug.chat("dodge", nAuxDef)

			-- If can parry, look for better defenses
			local bMainInfused = DB.getValue(DB.getChild(nodeDefender, "main_hand"), 'infused', 0) == 1;
			local bSideInfused = DB.getValue(DB.getChild(nodeDefender, "side_hand"), 'infused', 0) == 1;
			-- Debug.chat("rRoll.bIncorporeal", rRoll.bIncorporeal)
			if not rRoll.bIncorporeal or rRoll.bIncorporeal == "" or bMainInfused then
				nAuxDef = DB.getValue(nodeDefender, "defense.main_hand" .. sTypeAttack, DataCommon.defense_base);
				-- Debug.chat("main hand", nAuxDef)
				aFilter = {DataCommon.defenses_names["main_hand"]};
				nMisc = EffectManagerSS.getEffectsBonus(rDefender, DataCommon.keyword_states["DEF"], true, aFilter, rAttacker, true);
				nAuxDef = nAuxDef + nMisc - nMiscNofilter;
				if nAuxDef >= nDefense then
					nDefense = nAuxDef;
					nodeUseDef = DB.getChild(nodeDefender, "defense.main_hand.uses");
					sTypeDefense = "main_hand";
					-- Debug.chat("nodeUseDef main", nodeUseDef)
				end
			end

			-- If can parry with the second hand, look for better defenses
			if not rRoll.bIncorporeal or rRoll.bIncorporeal == "" or bSideInfused then
				nAuxDef = DB.getValue(nodeDefender, "defense.side_hand" .. sTypeAttack, DataCommon.defense_base);
				aFilter = {DataCommon.defenses_names["side_hand"]};
				nMisc = EffectManagerSS.getEffectsBonus(rDefender, DataCommon.keyword_states["DEF"], true, aFilter, rAttacker, true);
				nAuxDef = nAuxDef + nMisc - nMiscNofilter;
				if nAuxDef >= nDefense then
					nDefense = nAuxDef;
					nodeUseDef = DB.getChild(nodeDefender, "defense.side_hand.uses");
					sTypeDefense = "side_hand";
					-- Debug.chat("nodeUseDef side", nodeUseDef)
				end
			end

		end
	end

	-- Adjustments
	if bStunned then
		nDefense = nDefense - 3;
	end
	nDefense = nDefense + nMiscDist

	-- Results
	return nDefense, nodeUseDef, sTypeDefense;
end


function resetDefenseUses(rActor)
	-- Get node and actor
	nodeActor, rActor = ActorManager2.getActorAndNode(rActor);

	-- Reset uses
	DB.setValue(nodeActor, "defense.dodge.uses", "number", 0);
	DB.setValue(nodeActor, "defense.main_hand.uses", "number", 0);
	DB.setValue(nodeActor, "defense.side_hand.uses", "number", 0);
	DB.setValue(nodeActor, "defense.unaware.uses", "number", 0);

	-- Recalculate values
	recalculateDefenses(rActor);
end

---------------
-- OTHER
---------------


function rest(bLong)
	CombatManager.resetInit();
	clearExpiringEffects();

	for _,v in pairs(CombatManager.getCombatantNodes()) do
		local bHandled = false;
		local sClass, sRecord = DB.getValue(v, "link", "", "");
		if sClass == "charsheet" and sRecord ~= "" then
			local nodePC = DB.findNode(sRecord);
			if nodePC then
				CharManager.rest(nodePC, bLong);
				bHandled = true;
			end
		end

		if not bHandled then
			resetHealth(v, bLong);
		end
	end
end

function simpleRoll(sDice, nAdv)
	local nRes = 0;
	local bMinus = false;
	if sDice and sDice:sub(1,1) == "-" then
		bMinus = true;
		sDice = sDice:sub(2);
	end

	-- Ventaja 0, hacer tirada
	if (nAdv or 0) == 0 then
		if sDice == 'dT' then
			nRes = math.random(8) + math.random(8);
			if nRes == 2 then
				local nExtra = math.random(8) - 1;
				nRes = nRes - nExtra;
				while nExtra == 7 do
					nExtra = math.random(8) - 1;
					nRes = nRes - nExtra;
				end
			elseif nRes == 16 then
				local nExtra = math.random(8) - 1;
				nRes = nRes + nExtra;
				while nExtra == 7 do
					nExtra = math.random(8) - 1;
					nRes = nRes + nExtra;
				end
			end

		elseif string.match(sDice, "d%d+") then
			nRes = math.random(tonumber(string.match(sDice, "%d+")));
		end

	-- Advantage or disadvantage, roll several times
	else
		local nThrows = math.abs(nAdv) + 1;
		if nAdv > 0 then
			nRes = -9999;
		else
			nRes = 9999;
		end
		for ind = 1, nThrows do
			local nTemp = simpleRoll(sDice, 0);
			if nAdv > 0 then
				nRes = math.max(nRes, nTemp);
			else
				nRes = math.min(nRes, nTemp);
			end
		end

	end

	if bMinus then
		nRes = -nRes;
	end

	return nRes;
end


function rollRandomInit(nInit, nAdv, aAddDice)
	-- Roll
	local nInitResult = simpleRoll("dT", nAdv);

	-- Add aditional dice
	for _, v in pairs(aAddDice) do
		nInitResult =  nInitResult + simpleRoll(v);
	end

	--  Add  modifier and return
	nInitResult = nInitResult + nInit;
	return nInitResult;
end

function rollEntryInit(nodeEntry)
	if not nodeEntry then
		return;
	end

	-- Get base and effect modifiers
	local rActor = ActorManager.resolveActor(nodeEntry);
	local nInit, nAdv, _, aAddDice = ActorManager2.getInit(rActor);
	local nInit, nAdv, _, aAddDice = ActorManager2.getInit(rActor);

	-- Roll  initiative
	local nInitResult = rollRandomInit(nInit, nAdv, aAddDice);

	-- Set result
	DB.setValue(nodeEntry, "initresult", "number", nInitResult);

	-- Set the initiative of their effects as the current one
	EffectManagerSS.setEffectsInitiative(nodeEntry, nInitResult);

	-- Se that actor has not acted this turn
	DB.setValue(nodeEntry, "acted", "number", 0);

	return nInitResult, nInit, nAdv;

end

function rollInit(sType)
	local nReturn = CombatManager.rollTypeInit(sType, rollEntryInit);
end

-- Override CoreRPG
function rollTypeInit(sType, fRollCombatantEntryInit, ...)
	local tCombatantNodesToRoll = {};

	-- Calculate which combatants to roll initiative for
	for _,nodeCT in pairs(CombatManager.getCombatantNodes()) do
		local bRoll = true;
		if sType then
			local rActor = ActorManager.resolveActor(nodeCT);
			if sType == "pc" then
				if not ActorManager.isPC(rActor) then
					bRoll = false;
				end
			elseif not ActorManager.isRecordType(rActor, sType) then
				bRoll = false;
			end
		end
		if bRoll then
			table.insert(tCombatantNodesToRoll, nodeCT);
		end
	end

	-- Reset all entries to default "empty" value for initiative
	-- Must reset all before rolling to support initiative grouping
	-- for _,nodeCT in ipairs(tCombatantNodesToRoll) do
	-- 	DB.setValue(nodeCT, "initresult", "number", -10000);
	-- end

	-- Then, roll all initiatives
	local tResults = {};
	local tBonus = {};
	local tAdv = {};
	for _,nodeCT in ipairs(tCombatantNodesToRoll) do
		local nInitResult, nInit, nAdv = fRollCombatantEntryInit(nodeCT, ...);
		table.insert(tResults, nInitResult);
		table.insert(tBonus, nInit);
		table.insert(tAdv, nAdv);
	end

	-- Message
	if OptionsManager.isOption("ShowInit", "yes") then
		local sText = Interface.getString("initiative_upper") .. "\n";
		for k,nodeCT in ipairs(tCombatantNodesToRoll) do
			local sName = DB.getValue(nodeCT, "name", "");
			local nInitResult = tResults[k];
			local nInit = tBonus[k];
			local nAdv = tAdv[k];
			if sName ~= "" and nInitResult and nInit and nAdv then
				local nRoll = nInitResult - nInit;
				sText = sText .. "\n- " .. sName .. ": " .. tostring(nInit) .. " + "  .. tostring(nRoll);
				if nAdv < -1 then
					sText = sText .. string.format(" (D%d)", -nAdv);
				elseif nAdv == -1 then
					sText = sText .. "(D)";
				elseif nAdv == 1 then
					sText = sText .. "(V)";
				elseif nAdv > 1 then
					sText = sText .. string.format(" (V%d)", nAdv);
				end
				sText = sText .. " = " .. tostring(nInitResult);
			end

		end
		local msg = {font = "msgfont", secret = sType and sType == "npc", mode = "chat_heal", text = sText};
		Comm.deliverChatMessage(msg);
	end
end

---------------------
-- DISTANCES
---------------------

function getNPCSpaceReach(rActor)
	local nSpace = GameSystem.GameSystem.getDistanceUnitsPerGrid();
	local nReach = nSpace;

	-- Size
	local nodeNPC = ActorManager2.getActorAndNode(rActor);
	local sSize = StringManager.trim(DB.getValue(nodeNPC, "size", ""):lower());
	if sSize == DataCommon.sizelabels["large"] then
		nReach = nSpace * 2;
		nSpace = nSpace * 2;
	elseif sSize == DataCommon.sizelabels["huge"] then
		nReach = nSpace * 2;
		nSpace = nSpace * 3;
	elseif sSize == DataCommon.sizelabels["gargantuan"] then
		nReach = nSpace * 3;
		nSpace = nSpace * 4;
	elseif sSize == DataCommon.sizelabels["tiny"] then
		nReach = 0;
		nSpace = nSpace * 0.5;
	end

	--  Weapon reach
	local bAttackRanged = EffectManagerSS.hasEffectCondition(rActor, DataCommon.point_blank_shot);
	local bArmed = false;
	local nodeList = nodeNPC.createChild("weaponlist");
	for _,node in pairs(nodeList.getChildren()) do
		if DB.getValue(node, "carried", 0) == 2 then
			for _,nodeAttack in pairs(node.createChild("attacks").getChildren()) do
				if DB.getValue(nodeAttack, "type", 1) == 1 then
					bArmed = true;
					local nExtra = DB.getValue(nodeAttack, "range", 0);
					nReach = math.max(nReach, nExtra);
				elseif bAttackRanged then
					bArmed = true;
				end
			end
		end		
	end

	return nSpace, nReach, bArmed;
end

function getMeleeReach(nodeActor)
	local _, nReach, bArmed = getNPCSpaceReach(nodeActor);
	if not nReach then
		nReach = 0;
	end
	-- TODO: Add weapon reach
	return nReach, bArmed;
end

function getTokenDistance(rSource, rTarget)
	local nDist = nil;
	if ActorManager.hasCT(rSource) and ActorManager.hasCT(rTarget) then
		local nodeSource = ActorManager.getCTNode(rSource);
		local nodeTarget = ActorManager.getCTNode(rTarget);
		if DB.getValue(nodeSource, "name", "") == DB.getValue(nodeTarget, "name", "") then
			return nil;
		end
		if nodeSource and nodeTarget then
			local tokenSource = CombatManager.getTokenFromCT(nodeSource);
			local tokenTarget = CombatManager.getTokenFromCT(nodeTarget);
			if tokenSource and tokenTarget then
				nDist = Token.getDistanceBetween(tokenSource, tokenTarget);
			end
		end
	end

	return nDist;
end

function replaceCurrentToken(nodeCT, sNewToken)
	-- Get old token info
	local tokenOld = Token.getToken(DB.getValue(nodeCT, "tokenrefnode"), DB.getValue(nodeCT, "tokenrefid"));
	if tokenOld then
		local nodeContainer = oldToken.getContainerNode();
		local x, y = oldToken.getPosition();
		local tokenNew = Token.addToken(nodeContainer.getPath(), DB.sNewToken, x, y);
		TokenManager.linkToken(nodeCT, tokenNew);
	end
end



-- OLD
-- function getTokenDistance(rSource, rTarget)
-- 	local nDist = nil;
-- 	if ActorManager.hasCT(rAttacker) and ActorManager.hasCT(rDefender) then
-- 		local nodeAttacker = ActorManager.getCTNode(rAttacker);
-- 		local nodeDefender = ActorManager.getCTNode(rDefender);
-- 		if DB.getValue(nodeAttacker, "name", "") == DB.getValue(nodeDefender, "name", "") then
-- 			return nil;
-- 		end
-- 		if nodeAttacker and nodeDefender then
-- 			local tokenAttacker = CombatManager.getTokenFromCT(nodeAttacker);
-- 			local tokenDefender = CombatManager.getTokenFromCT(nodeDefender);
-- 			if tokenAttacker and tokenDefender then
-- 				local nodeAttackerContainer = tokenAttacker.getContainerNode();
-- 				local nodeDefenderContainer = tokenDefender.getContainerNode();
				
-- 				local nDU = GameSystem.getDistanceUnitsPerGrid();
-- 				local nAttackerSpace = math.ceil(DB.getValue(nodeAttacker, "space", nDU) / nDU);
-- 				local nDefenderSpace = math.ceil(DB.getValue(nodeDefender, "space", nDU) / nDU);
-- 				local xAttacker, yAttacker = tokenAttacker.getPosition();
-- 				local xDefender, yDefender = tokenDefender.getPosition();
-- 				local nGrid = nodeAttackerContainer.getGridSize();

-- 				local xDiff = math.abs(xAttacker - xDefender);
-- 				local yDiff = math.abs(yAttacker - yDefender);
-- 				local gx = math.floor(xDiff / nGrid);
-- 				local gy = math.floor(yDiff / nGrid);

-- 				nDist = math.sqrt(gx*gx + gy*gy);
-- 				nDist = nDist - (nAttackerSpace / 2);
-- 				nDist = nDist - (nDefenderSpace / 2);
-- 				nDist = Utilities.round(nDist);
-- 				nDist = math.min(0, nDist)

-- 			end
-- 		end
-- 	end

-- 	return nDist;
-- end

function isUnabled(rActor)
	local aEffects = {
		DataCommon.conditions["Entangled"],
		DataCommon.conditions["Inmovilized"],
		DataCommon.conditions["Paralyzed"],
		DataCommon.conditions["Prone"],
		DataCommon.conditions["Confused"],
		DataCommon.conditions["Unaware"],
		DataCommon.conditions["Stupified"],
		DataCommon.conditions["Unconscious"],
		DataCommon.conditions["Helpless"],
		DataCommon.conditions["Unwilled"],
		DataCommon.conditions["Trance"],
		DataCommon.conditions["Dead"],
	};
	return EffectManagerSS.hasAnyEffectCondition(rActor, aEffects);
end

function getNumberUnitsInMelee(rActor, bHasReaction)
	-- Actor info
	local nodeActor = ActorManager.getCTNode(rActor);
	local sActorName = DB.getValue(nodeActor, "name", "");
	local sFaction = CombatManager.getFactionFromCT(nodeActor);

	local nChars = 0;
	local nodeList = DB.findNode(CombatManager.CT_LIST);
	for _, node in pairs(nodeList.getChildren()) do
		local sName = DB.getValue(node, "name", "");
		if sName ~= sActorName then
			rActor = ActorManager.resolveActor(node);
			-- Distance
			local nDist = getTokenDistance(node, nodeActor) or 0;
			local nReach, bArmed = getMeleeReach(node);
			local bDist = nDist <= nReach;
			-- Faction
			local sFactionOther = CombatManager.getFactionFromCT(node);
			local bCondFaction = EffectManagerSS.hasEffectCondition(rActor, DataCommon.conditions["Maddened"]) or (sFaction == "") or (sFactionOther == "") or (sFaction ~= sFactionOther);
			-- States
			local bCondUnabled = not EffectManagerSS.hasAnyEffectCondition(rActor, DataCommon.conditions_not_in_melee);
			-- Has reaction
			local _, _, nReactions = CombatManager2.getUsedActions(node);
			local nTotalReactions = CombatManager2.getMaxActions(node, "reaction");
			local bReaction = nTotalReactions > nReactions;

			if bCondFaction and bDist and bArmed and bCondUnabled and bReaction then
				nChars = nChars + 1;
			end
		end
	end
	return nChars;
end

function isInMelee(rActor, bHasReaction)
	return getNumberUnitsInMelee(rActor, bHasReaction) > 0;
end

function getNumberCloseAllies(rActor)
	local nodeList = DB.findNode(CombatManager.CT_LIST);
	local nodeActor = ActorManager.getCTNode(rActor);
	local sActorName = DB.getValue(nodeActor, "name", "");
	local sFaction = CombatManager.getFactionFromCT(nodeActor);
	local nChars = 0;
	for _, node in pairs(nodeList.getChildren()) do
		local sName = DB.getValue(node, "name", "");
		if sName ~= sActorName then
			local nDist = getTokenDistance(node, nodeActor);
			local nReach = getNPCSpaceReach(node);
			local sFactionOther = CombatManager.getFactionFromCT(node);
			local bCondFaction = (sFaction == "") or (sFactionOther == "") or (sFaction == sFactionOther);
			local bCondUnabled = EffectManagerSS.hasAnyEffectCondition(node, DataCommon.conditions_not_obstacle);
			if bCondFaction and nDist and nReach and not bCondUnabled and nDist <= nReach then
				nChars = nChars + 1;
			end
		end
	end
	return nChars;
end


--
--	XP FUNCTIONS
--

function calcBattleXP(nodeBattle)
	local sTargetNPCList = LibraryData.getCustomData("battle", "npclist") or "npclist";

	local nXP = 0;
	for _, vNPCItem in pairs(DB.getChildren(nodeBattle, sTargetNPCList)) do
		local sClass, sRecord = DB.getValue(vNPCItem, "link", "", "");
		if sRecord ~= "" then
			local nodeNPC = DB.findNode(sRecord);
			if nodeNPC then
				nXP = nXP + (DB.getValue(vNPCItem, "count", 0) * DB.getValue(nodeNPC, "xp", 0));
			else
				local sMsg = string.format(Interface.getString("enc_message_refreshxp_missingnpclink"), DB.getValue(vNPCItem, "name", ""));
				ChatManager.SystemMessage(sMsg);
			end
		end
	end

	DB.setValue(nodeBattle, "exp", "number", nXP);
end

function calcBattleCR(nodeBattle)
	calcBattleXP(nodeBattle);

	local nXP = DB.getValue(nodeBattle, "exp", 0);

	local sCR = "";
	if nXP > 0 then
		if nXP <= 10 then
			sCR = "0";
		elseif nXP <= 25 then
			sCR = "1/8";
		elseif nXP <= 50 then
			sCR = "1/4";
		elseif nXP <= 100 then
			sCR = "1/2";
		elseif nXP <= 200 then
			sCR = "1";
		elseif nXP <= 450 then
			sCR = "2";
		elseif nXP <= 700 then
			sCR = "3";
		elseif nXP <= 1100 then
			sCR = "4";
		elseif nXP <= 1800 then
			sCR = "5";
		elseif nXP <= 2300 then
			sCR = "6";
		elseif nXP <= 2900 then
			sCR = "7";
		elseif nXP <= 3900 then
			sCR = "8";
		elseif nXP <= 5000 then
			sCR = "9";
		elseif nXP <= 5900 then
			sCR = "10";
		elseif nXP <= 7200 then
			sCR = "11";
		elseif nXP <= 8400 then
			sCR = "12";
		elseif nXP <= 10000 then
			sCR = "13";
		elseif nXP <= 11500 then
			sCR = "14";
		elseif nXP <= 13000 then
			sCR = "15";
		elseif nXP <= 15000 then
			sCR = "16";
		elseif nXP <= 18000 then
			sCR = "17";
		elseif nXP <= 20000 then
			sCR = "18";
		elseif nXP <= 22000 then
			sCR = "19";
		elseif nXP <= 25000 then
			sCR = "20";
		elseif nXP <= 33000 then
			sCR = "21";
		elseif nXP <= 41000 then
			sCR = "22";
		elseif nXP <= 50000 then
			sCR = "23";
		elseif nXP <= 62000 then
			sCR = "24";
		elseif nXP <= 75000 then
			sCR = "25";
		elseif nXP <= 90000 then
			sCR = "26";
		elseif nXP <= 105000 then
			sCR = "27";
		elseif nXP <= 120000 then
			sCR = "28";
		elseif nXP <= 135000 then
			sCR = "29";
		elseif nXP <= 155000 then
			sCR = "30";
		else
			sCR = "31+";
		end
	end

	DB.setValue(nodeBattle, "cr", "string", sCR);
end

--
--	COMBAT ACTION FUNCTIONS
--

function addRightClickDiceToClauses(rRoll)
	if #rRoll.clauses > 0 then
		local nOrigDamageDice = 0;
		for _,vClause in ipairs(rRoll.clauses) do
			nOrigDamageDice = nOrigDamageDice + #vClause.dice;
		end
		if #rRoll.aDice > nOrigDamageDice then
			local v = rRoll.clauses[#rRoll.clauses].dice;
			for i = nOrigDamageDice + 1,#rRoll.aDice do
				table.insert(rRoll.clauses[1].dice, rRoll.aDice[i]);
			end
		end
	end
end
