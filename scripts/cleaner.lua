--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

-- powers.*.extra_levels, linked_item
-- encumbrance.limit, helmet
-- spell.*.reftype_cycler, type
-- aptitude.*.cycler, type
-- technique.*.reftype, refsubtype, refcombattype


aListsChars = {"combattracker.list", "charsheet", "npc"};
aInLists = {"skilllist_weaponary", "skilllist_magic", "skilllist_specific", "skilllist_learned", "skilllist_basic"};

aChars = {
    "encumbrance.limit", "encumbrance.helmet", "powers.*.extra_levels", "powers.*.linked_item"
};
aWeaponFields = {"attack_bonus", "item_path", "infused", "damagelist", "is_identified"};
aRaces = {"actions_action", "actions_fast", "actions_movement", "actions_reaction"}
aSubraces = aRaces;
aCT = {"main_actions", "fast_actions", "move_actions", "reactions", "tiring_actions", "tiring_actions_used"}

aAptitudeLists = {
    ["disadvantagelist"] = "type_aptitude_flaw",
    ["genericlist"] = "type_aptitude_generic",
    ["raciallist"] = "type_aptitude_racial",
    ["professionlist"] = "type_aptitude_professional",
}

--------------
-- MAIN
--------------


function onInit()
    revertChangesToModules();
end

function deleteCustom()
    revertChangesToModules();
    -- fixSkills()
    loopInChars();
end

function loopInChars(sType)
    local aOut = {};
    for _,sList in pairs(aListsChars) do
        if not sType or sType == sList then
            local nodeList = DB.findNode(sList);
            for _,nodeChar in pairs(nodeList.getChildren()) do
                -- DO TO CHARS
                updateAptitudes(nodeChar);
                updatePowers(nodeChar);
                updateItems(nodeChar);
            end
        end
    end
end


--------------------------
-- UPDATE INDIVIDUAL ITEMS
--------------------------

local aAptitudeUpdateFields = {
    ["level"] = "string", 
    ["extra_exp"] = "number", 
    ["requirements"] = "string", 
    ["text"] = "formattedtext",
    ["subtype"] = "string",
};
local aPowerUpdateFields = {
    ["source"] = "string", 
    ["level"] = "number", 
    ["type_spell"] = "string", 
    ["rarity"] = "string", 
    ["school"] = "string", 
    ["elements"] = "string", 
    ["components"] = "string", 
    ["castingtime"] = "string", 
    ["range"] = "string", 
    ["duration"] = "string", 
    ["vigor_cost"] = "string", 
    ["essence_cost"] = "number", 
    ["essence_target"] = "string", 
    ["keywords"] = "string", 
    ["description"] = "formattedtext"};


function updateField(nodeOld, nodeNew, aFields)
    if not nodeOld or not nodeNew or not aFields then
        return;
    end
    for sField, sType in pairs(aFields) do
        DB.setValue(nodeOld, sField, sType, sValue);
    end
end

local aListAptitudes = {"disadvantagelist", "genericlist", "raciallist", "professionlist"};

function updateAptitudes(nodeChar)
    Debug.chat("Updating Aptitudes")
    for _, sList in pairs(aListAptitudes) do
        local nodeList = nodeChar.getChild(sList) do
            if nodeList then
                for _, nodeApt in pairs(nodeList.getChildren()) do
                    local sName = DB.getValue(nodeApt, "name", "");
                    local sType = AptitudeManager.getAptitudeType(nodeApt);
                    local nodeRefApt = AptitudeManager.findRefAptitude(sName, sType);
                    if nodeRefApt then
                        Debug.chat("Updating ", sName, sType)
                        updateField(nodeApt, nodeRefApt, aAptitudeUpdateFields)
                    end
                end
            end
        end
    end
end

function updatePowers(nodeChar)
    local nodeList = nodeChar.getChild("powers");
    if nodeList then
        Debug.chat("Updating powers")
        for _, nodeOld in pairs(nodeList.getChildren()) do
            local sName = DB.getValue(nodeOld, "name", "");
            local sType = PowerManager.getPowerType(nodeOld);
            local nodeRef = PowerManager.findRefPower(sName, sType);
            if nodeRef then
                Debug.chat("Updating ", sName, sType)
                updateField(nodeOld, nodeRef, aPowerUpdateFields);

                local nodeHeightList = nodeOld.createChild("heightenings");
                DB.deleteChildren(nodeHeightList);
                local nodeHeightListRef = DB.createChild(nodeRef, "heightenings");
                for _,nodeHeighRef in pairs(nodeHeightListRef.getChildren()) do
                    local nodeHeight = nodeHeightList.createChild();
                    DB.setValue(nodeNew, "leveltag", "string", DB.getValue(nodeHeighRef, "leveltag", ""));
                    DB.setValue(nodeNew, "description", "string", DB.getValue(nodeHeighRef, "description", ""));
                end
            end
        end
    end
end

function updateItems(nodeChar)
    Debug.chat("Updating items")

    local nodeList = nodeChar.getChild("inventorylist");
    if nodeList then
        updateItemsSub(nodeList);
    end
    nodeList = nodeChar.getChild("mount.inventorylist");
    if nodeList then
        updateItemsSub(nodeList);
    end
end
function updateItemsSub(nodeList)
    for _, nodeOld in pairs(nodeList.getChildren()) do
        local nodeOrig = ItemManager2.getOriginalItem(nodeOld);
        if nodeOrig then
            local sName = DB.getValue(nodeOrig, "name", "");
            local nodeRef = ItemManager2.findRefItem(sName);
            if nodeRef then
                Debug.chat("Updating ", DB.getValue(nodeOrig, "name", ""));
                updateField(nodeOrig, nodeRef, aItemUpdateFields);
            end
        else
            local sName = DB.getValue(nodeOld, "name", "");
            local nodeRef = ItemManager2.findRefItem(sName);
            if nodeRef then
                Debug.chat("Updating ", sName)
                updateField(nodeOld, nodeRef, aItemUpdateFields);
            end
        end
    end
end

function updateTechniques(nodeChar)
    Debug.chat("Updating techniques")
    local nodeList = nodeChar.getChild("listmagic");
    if nodeList then
        updateTechniquesSub(nodeList);
    end
    nodeList = nodeChar.getChild("basiclistcombat");
    if nodeList then
        updateTechniquesSub(nodeList);
    end
    nodeList = nodeChar.getChild("advancedlistcombat");
    if nodeList then
        updateTechniquesSub(nodeList);
    end
    nodeList = nodeChar.getChild("masterlistcombat");
    if nodeList then
        updateTechniquesSub(nodeList);
    end
end
function updateTechniquesSub(nodeList)
    for _, nodeOld in pairs(nodeList.getChildren()) do
        local sName = DB.getValue(nodeOld, "name", "");
        -- TODO: Find ref technique
    end
end


------------------------
-- OLD
----------------------

function fixSkills()
    local arrayMagic = {Interface.getString("skill_value_arcaneskill"), Interface.getString("skill_value_air"), Interface.getString("skill_value_water"), Interface.getString("skill_value_spirit"), Interface.getString("skill_value_fire"), Interface.getString("skill_value_earth"), Interface.getString("skill_value_mind"), };
    local arrayNature = {Interface.getString("skill_value_primalskill"),}
    local arrayHelmet = {Interface.getString("skill_value_psychicskill"),}

    for _, nodeChar in pairs(getArrayOfChars()) do
        Debug.chat("Updating ", nodeChar)
            local nodeList = nodeChar.getChild("skilllist_magic") do
            if nodeList then
                for _, nodeOld in pairs(nodeList.getChildren()) do
                    local sName = DB.getValue(nodeOld, "name", "")
                    if Utilities.inArray(arrayMagic, sName) then
                        DB.setValue(nodeOld, "limited", "number", 2);
                    elseif Utilities.inArray(arrayNature, sName) then
                        DB.setValue(nodeOld, "limited", "number", 3);
                    elseif Utilities.inArray(arrayHelmet, sName) then
                        DB.setValue(nodeOld, "limited", "number", 4);
                    end
                end
            end
        end
    end
end

function revertChangesToModules()
    local aModules = Module.getModules();
    for _,sModule in pairs(aModules) do
        Module.revert(sModule);
    end
end

function getArrayOfChars()
    local aOut = {};
    for _,sList in pairs(aLists) do
        local nodeList = DB.findNode(sList);
        for _,node in pairs(nodeList.getChildren()) do
            table.insert(aOut, node);
        end
    end
    return aOut;
end

function cleanCharacters()
    for _,sList in pairs(aLists) do
        local nodeList = DB.findNode(sList);
        for _,node in pairs(nodeList.getChildren()) do
            for _, sField in pairs(aChars) do
                local oldNode = node.getChild(sField);
                if oldNode then
                    Debug.chat("Deleted node: ", oldNode)
                    oldNode.delete();
                end
            end
        end
    end
end


function cleanCT()
    local nodeList = DB.findNode("combattracker.list");
    for _,node in pairs(nodeList.getChildren()) do
        for _, sField in pairs(aCT) do
            local nodeOld = DB.getChild(node, sField);
            if nodeOld then
                Debug.chat("Deleted node: ", nodeOld)
                nodeOld.delete();
            end
        end
    end
end


function clean_races()
    local nodeList = DB.findNode("race");
    for _,node in pairs(nodeList.getChildren()) do
        for _, sField in pairs(aRaces) do
            local nodeOld = DB.getChild(node, sField);
            if nodeOld then
                Debug.chat("Deleted node: ", nodeOld)
                nodeOld.delete();
            end
        end
        local nodeList2 = node.createChild("subraces")
        for _,node2 in pairs(nodeList2.getChildren()) do
            for _, sField2 in pairs(aSubraces) do
                local nodeOld = DB.getChild(node2, sField2);
                if nodeOld then
                    Debug.chat("Deleted node: ", nodeOld)
                    nodeOld.delete();
                end
            end
        end
    end
end

function clean_weapons()    
    for _,sList in pairs(aLists) do
        local nodeList = DB.findNode(sList);
        -- Debug.chat("Lista: ", nodeList)
        for _,node in pairs(nodeList.getChildren()) do
            local nodeWeapons = node.createChild("weaponlist");
            -- Debug.chat("Lista armas: ", nodeWeapons)
            for _, nodeWeapon in pairs(nodeWeapons.getChildren()) do
                -- Debug.chat("Armas: ", nodeWeapon)
                for _,sField in pairs(aWeaponFields) do
                    local nodeOld = DB.getChild(nodeWeapon, sField);
                    if nodeOld then
                        Debug.chat("Deleted node: ", nodeOld)
                        nodeOld.delete();
                    end
                end
            end
        end
    end
end


function deleteNodes()
    Debug.chat("Deleting nodes")

    for _,sList in pairs(aLists) do
        local nodeList = DB.findNode(sList);
        for _,node in pairs(nodeList.getChildren()) do
            for _, sPath in pairs(aPaths) do
                local oldNode = node.getChild(sPath);
                if oldNode then
                    Debug.chat("Deleted node: ", oldNode)
                    oldNode.delete();
                end
            end
            oldNode = node.getChild("powergroup");
            if oldNode then
                for _,n in pairs(oldNode.getChildren()) do
                    local node2 = n.getChild("prepares");
                    if node2 then
                        Debug.chat("Deleted node: ", node2)
                        node2.delete();
                    end
                end
            end
        end
    end
end

function deleteUnusedSkills()
    Debug.chat("Deleting nodes custom")
    for _,sList in pairs(aLists) do
        local nodeList = DB.findNode(sList);
        for _,node in pairs(nodeList.getChildren()) do
            for _,sInList in pairs(aInLists) do
                local nodeInList = node.createChild(sInList)
                for _,nodeItem in pairs(nodeInList.getChildren()) do
                    local nProf = DB.getValue(nodeItem, "profession", 0);
                    local nMast = DB.getValue(nodeItem, "prof", 0);
                    local nLim = DB.getValue(nodeItem, "limited", 0);
                    if nProf + nMast + nLim == 0 then
                        Debug.chat("Deleting ", nodeItem)
                        nodeItem.delete();
                    end
                end
            end
        end
    end
end

----------------------
-- UPDATE FIELDS
----------------------






function fixCharAptitudes()
    for _,sList in pairs(aLists) do
        local nodeList = DB.findNode(sList);
        for _,nodeChar in pairs(nodeList.getChildren()) do
            for sField, sValue in pairs(aAptitudeLists) do
                local nodeAptlist = nodeChar.createChild(sField);
                for _,nodeApt in pairs(nodeAptlist.getChildren()) do
                    DB.setValue(nodeApt, "reftype", "string", sValue);
                end
            end
        end
    end
end