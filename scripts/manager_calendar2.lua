--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

MINUTES_IN_HOUR = 60;
HOURS_IN_DAY = 24;
DAYS_IN_MONTH = 30;
MONTHS_IN_YEAR = 12;
DAYS_IN_YEAR = 365;

function onInit()
    CalendarManager.adjustMinutes = adjustMinutes;
    CalendarManager.adjustHours = adjustHours;
    CalendarManager.adjustDays = adjustDays;
    CalendarManager.adjustMonths = adjustMonths;
    CalendarManager.adjustYears = adjustYears;
    CalendarManager.setCurrentDay = setCurrentDay;
    CalendarManager.setCurrentMonth = setCurrentMonth;
end

-----------------
-- GET INFO
-----------------

function getTime()
	local nHour = DB.getValue("calendar.current.hour", 0);
	local nMinute = DB.getValue("calendar.current.minute", 0);
    return nHour, nMinute;
end

function getDate()
	local nDay = DB.getValue("calendar.current.day", 0);
	local nMonth = DB.getValue("calendar.current.month", 0);
	local nYear = DB.getValue("calendar.current.year", 0);
	local sEpoch = DB.getValue("calendar.current.epoch", "");

    return sEpoch, nYear, nMonth, nDay;
end

-----------------
-- MESSAGES
-----------------
function outputDateAndTime()
    local msg = {sender = "", font = "chatfont", icon = "portrait_gm_token", mode = "story"};
	msg.text = Interface.getString("message_calendardate") .. " " .. CalendarManager.getCurrentDateString() .. "; " .. CalendarManager.getCurrentTimeString();
	Comm.deliverChatMessage(msg);
end


-----------------
-- ADJUST TIME
-----------------
function adjustMinutes(n, bIsCallback)
    -- Manage fractions
    n = Utilities.round(n);

    -- Core RPG part
	local nAdjMinutes = DB.getValue("calendar.current.minute", 0) + n;	
	local nHourAdj = 0;	
	if nAdjMinutes >= 60 then
		nHourAdj = math.floor(nAdjMinutes / MINUTES_IN_HOUR);
		nAdjMinutes = nAdjMinutes % MINUTES_IN_HOUR;
	elseif nAdjMinutes < 0 then
		nHourAdj = -math.floor(-nAdjMinutes / MINUTES_IN_HOUR) - 1;
		nAdjMinutes = nAdjMinutes % MINUTES_IN_HOUR;
	end	
	if nHourAdj ~= 0 then
		CalendarManager.adjustHours(nHourAdj);
	end	
	DB.setValue("calendar.current.minute", "number", nAdjMinutes);

    -- Advance time for effects
    if not bIsCallback and n > 0 then
        local nTurns = Utilities.round(n * DataCommon.turns_per_unit["minute"]);
        EffectManagerSS.passTimeReduceEffectDuration(nTurns);
    end
end

function adjustHours(n, bIsCallback)
    -- Manage fractions
    local nFloor = math.floor(n);
    local nDif = n - nFloor;
    if nDif > 0 then
        local nRest = nDif * MINUTES_IN_HOUR;
        CalendarManager.adjustMinutes(nRest);
    end
    n = nFloor;

    -- CoreRPG part
    local nAdjHours = DB.getValue("calendar.current.hour", 0) + n;	
	local nDayAdj = 0;	
	if nAdjHours >= HOURS_IN_DAY then
		nDayAdj = math.floor(nAdjHours / HOURS_IN_DAY);
		nAdjHours = nAdjHours % HOURS_IN_DAY;
	elseif nAdjHours < 0 then
		nDayAdj = -math.floor(-nAdjHours / HOURS_IN_DAY) - 1;
		nAdjHours = nAdjHours % HOURS_IN_DAY;
	end	
	if nDayAdj ~= 0 then
		CalendarManager.adjustDays(nDayAdj);
	end
	DB.setValue("calendar.current.hour", "number", nAdjHours);

    -- Advance time for effects
    if not bIsCallback and n > 0 then
        local nTurns = Utilities.round(n * DataCommon.turns_per_unit["hour"]);
        EffectManagerSS.passTimeReduceEffectDuration(nTurns);
    end
end

function adjustDays(n, bIsCallback)
    -- Manage fractions
    local nFloor = math.floor(n);
    local nDif = n - nFloor;
    if nDif > 0 then
        local nRest = nDif * HOURS_IN_DAY;
        adjustHours(nRest, true);
    end
    n = nFloor;

    -- CoreRPG part
    local nAdjDay = DB.getValue("calendar.current.day", 0) + n;	
	local nDaysInMonth = CalendarManager.getDaysInMonth(DB.getValue("calendar.current.month", 0));
	if nDaysInMonth == 0 then
		return;
	end	
	if nAdjDay > nDaysInMonth then
		while nAdjDay > nDaysInMonth do
			nAdjDay = nAdjDay - nDaysInMonth;
			CalendarManager.adjustMonths(1);

			nDaysInMonth = CalendarManager.getDaysInMonth(DB.getValue("calendar.current.month", 0));
			if nDaysInMonth == 0 then
				break;
			end
		end
	elseif nAdjDay <= 0 then
		while nAdjDay <= 0 do
			CalendarManager.adjustMonths(-1);
			nDaysInMonth = CalendarManager.getDaysInMonth(DB.getValue("calendar.current.month", 0));
			if nDaysInMonth == 0 then
				break;
			end
			nAdjDay = nAdjDay + nDaysInMonth;
		end
	end	
	DB.setValue("calendar.current.day", "number", nAdjDay);

    -- Advance time for effects
    if not bIsCallback and n > 0 then
        local nTurns = Utilities.round(n * DataCommon.turns_per_unit["day"]);
        EffectManagerSS.passTimeReduceEffectDuration(nTurns);
    end
end

function adjustMonths(n, bIsCallback)
    -- Manage fractions
    local nFloor = math.floor(n);
    local nDif = n - nFloor;
    if nDif > 0 then
        local nRest = nDif * DAYS_IN_MONTH;
        adjustDays(nRest, true);
    end
    n = nFloor;

    -- CoreRPG part
    local nAdjMonth = DB.getValue("calendar.current.month", 0) + n;
	local nMonthsInYear = CalendarManager.getMonthsInYear();	
	if nMonthsInYear > 0 then
		local nYearAdj = 0;

		if nAdjMonth > nMonthsInYear then
			nYearAdj = math.floor((nAdjMonth - 1) / nMonthsInYear);
			nAdjMonth = ((nAdjMonth - 1) % nMonthsInYear) + 1;
		elseif nAdjMonth <= 0 then
			nYearAdj = -math.floor(-(nAdjMonth - 1) / nMonthsInYear) - 1;
			nAdjMonth = ((nAdjMonth - 1) % nMonthsInYear) + 1;
		end
		
		if nYearAdj ~= 0 then
			CalendarManager.adjustYears(nYearAdj);
		end
	end	
	DB.setValue("calendar.current.month", "number", nAdjMonth);
    
    -- Advance time for effects
    if not bIsCallback and n > 0 then
        local nTurns = Utilities.round(n * DataCommon.turns_per_unit["month"]);
        EffectManagerSS.passTimeReduceEffectDuration(nTurns);
    end
end

function adjustYears(n, bIsCallback)
    -- Manage fractions
    local nFloor = math.floor(n);
    local nDif = n - nFloor;
    if nDif > 0 then
        local nRest = nDif * DAYS_IN_YEAR;
        adjustDays(nRest, true);
    end
    n = nFloor;

    -- CoreRPG part
    DB.setValue("calendar.current.year", "number", DB.getValue("calendar.current.year", 0) + n);
    
    -- Advance time for effects
    if not bIsCallback and n > 0 then
        local nTurns = Utilities.round(n * DataCommon.turns_per_unit["year"]);
        EffectManagerSS.passTimeReduceEffectDuration(nTurns);
    end
end

-- TODO: This two functions should be combined in one in case you change simultaneously month and day. However, too much work for tiny reward for now
function setCurrentDay(nDay, bIsCallback)
    local nCurrent = DB.getValue("calendar.current.day", nil, 0);
	DB.setValue("calendar.current.day", "number", nDay);
    if not bIsCallback and nDay - nCurrent > 0 then
        local nTurns = Utilities.round((nDay - nCurrent) * DataCommon.turns_per_unit["day"]);
        EffectManagerSS.passTimeReduceEffectDuration(nTurns);
    end
end
function setCurrentMonth(nMonth, bIsCallback)
    local nCurrent = DB.getValue("calendar.current.month", nil, 0);
	DB.setValue("calendar.current.month", "number", nMonth);
    if not bIsCallback and nMonth - nCurrent > 0 then
        local nTurns = Utilities.round((nMonth - nCurrent) * DataCommon.turns_per_unit["month"]);
        EffectManagerSS.passTimeReduceEffectDuration(nTurns);
    end
end




