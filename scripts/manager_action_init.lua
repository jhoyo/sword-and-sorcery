--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYINIT = "applyinit";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYINIT, handleApplyInit);

	ActionsManager.registerModHandler("init", modRoll);
	ActionsManager.registerResultHandler("init", onResolve);
end

function handleApplyInit(msgOOB)
	local rSource = ActorManager.resolveActor(msgOOB.sSourceNode);
	local nTotal = tonumber(msgOOB.nTotal) or 0;

	DB.setValue(ActorManager.getCTNode(rSource), "initresult", "number", nTotal);
end

function notifyApplyInit(rSource, nTotal)
	if not rSource then
		return;
	end

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_APPLYINIT;

	msgOOB.nTotal = nTotal;

	local sSourceType, sSourceNode = ActorManager.getTypeAndNodeName(rSource);
	msgOOB.sSourceType = sSourceType;
	msgOOB.sSourceNode = sSourceNode;

	Comm.deliverOOBMessage(msgOOB, "");
end

function getRoll(rActor, bSecretRoll)
	local rRoll = {};
	rRoll.sType = "init";
	rRoll.aDice = { "d8", "d8" };
	rRoll.nMod = 0;

	rRoll.sDesc = "[INIT]";

	rRoll.bSecret = bSecretRoll;

	-- Determine the modifier and ability to use for this roll
	local sActorType, nodeActor = ActorManager.getTypeAndNode(rActor);
	if nodeActor then
		local nEffects;
		rRoll.nMod, rRoll.nAdv, nEffects, aDice = ActorManager2.getInit(rActor);
		rRoll.aDice = Utilities.addEffectDice(rRoll.aDice, aDice)

		if nEffects and nEffects ~= 0 then
			rRoll.sDesc = rRoll.sDesc .. " [EFFECTS: " .. nEffects .. "]";
		end
	end

	local nPen = ActorManager2.getPenalizer(rActor);
	if nPen and nPen ~= 0 then
		rRoll.nMod = rRoll.nMod + nPen;
		rRoll.sDesc = rRoll.sDesc .. " [PEN " .. nPen .. "]";
	end

	return rRoll;
end

function performRoll(draginfo, rActor, bSecretRoll)
	local rRoll = getRoll(rActor, bSecretRoll);

	ActionsManager.performAction(draginfo, rActor, rRoll);
end

function modRoll(rSource, rTarget, rRoll)

	if rSource then

		-- Determine general effect modifiers
		local bEffects = false;
		local aAddDice, _, _ = EffectManagerSS.getEffectsBonus(rSource, {DataCommon.keyword_states["INIT"]});
		if aAddDice then
			bEffects = true;
			for _,vDie in ipairs(aAddDice) do
				if vDie:sub(1,1) == "-" then
					table.insert(rRoll.aDice, "-p" .. vDie:sub(3));
				else
					table.insert(rRoll.aDice, "p" .. vDie:sub(2));
				end
			end
		end
	end

	ActionsManager2.encodeAdvantage(rRoll, nAdv);
end

function onResolve(rSource, rTarget, rRoll)
	ActionsManager2.decodeAdvantage(rRoll);

	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	-- Comm.deliverChatMessage(rMessage);
	MessageManager.onRollMessage(rMessage);

	local nTotal = ActionsManager.total(rRoll);
	notifyApplyInit(rSource, nTotal);
end
