--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

OOB_MSGTYPE_CALCULATEWINDS = "applywinds";
nCurrentTotal = 0;

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_CALCULATEWINDS, handleApplyWinds);

	ActionsManager.registerModHandler("winds", modRoll);
	ActionsManager.registerResultHandler("winds", onResolve);

	local node = DB.findNode("charsheet");

	DB.addHandler(DB.getPath(node, "*.abilities.*.current"), "onUpdate", whoCanGetWinds);
	whoCanGetWinds();
end

function onClose()
	DB.removeHandler(DB.getPath(node, "*.abilities.*.current"), "onUpdate", whoCanGetWinds);
end



function whoCanGetWinds()
	-- Only Host can change holders
	if User.isHost() then
		-- Loop through character sheets
		local node = DB.findNode("windsofmagic");
		if not node then
			return;
		end

		local nodeCharsheet = DB.findNode("charsheet");
		if nodeCharsheet then
			for k, v in pairs(nodeCharsheet.getChildren()) do
				local bArcane = DB.getValue(v, "abilities.arcane.current", 0) > 0;
				local bSacred = DB.getValue(v, "abilities.sacred.current", 0) > 0;
				local bPrimal = DB.getValue(v, "abilities.primal.current", 0) > 0;
				local bPsychic = DB.getValue(v, "abilities.psychic.current", 0) > 0;
				local bElemental = DB.getValue(v, "abilities.elemental.current", 0) > 0;
				local bMagic = bArcane or bSacred or bPrimal or bPsychic or bElemental;

				-- If magic user, can see winds
				local username = v.getOwner();
				if bMagic then
					node.addHolder(username);
				end
				
			end
		end
	end

end

function handleApplyWinds(msgOOB)
	local nTotal = tonumber(msgOOB.nTotal) or 0;
	nCurrentTotal = transformResultInCategory(nTotal);
	setAllValues();

end

function setAllValues()
	if User.isHost() then
		local nEffects = DataCommon.magic_increased_effects[nCurrentTotal];
		local nMisscast = DataCommon.magic_increased_misscast[nCurrentTotal];
		local sName = DataCommon.magic_names[nCurrentTotal];

		local node = DB.findNode("windsofmagic");
		DB.setValue(node, "result_label", "string", sName);
		DB.setValue(node, "result_effects", "number", nEffects);
		DB.setValue(node, "result_misscast", "number", nMisscast);
		calculateRemainingSpells();
	end
end

function calculateRemainingSpells()
	local node = DB.findNode("windsofmagic");
	local nNewValue = math.random(8) + 2;
	DB.setValue(node, "result_remaining", "number", nNewValue);
end

function transformResultInCategory(nTotal)
	local nCat = 1;
	for k, v in pairs(DataCommon.magic_min) do
		if nTotal >= v then
			nCat = k;
		else
			break;
		end
	end
	return nCat;
end

function notifyApplyWinds(rSource, nTotal)

	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_CALCULATEWINDS;

	msgOOB.nTotal = nTotal;

	Comm.deliverOOBMessage(msgOOB, "");
end

function getRoll()
	local rRoll = {};
	rRoll.sType = "winds";
	rRoll.aDice = { "d8", "d8" };
	rRoll.nMod = DB.getValue(DB.findNode("windsofmagic"), "modifier", 0);
	rRoll.sDesc = "[CURRENTS] ";
	rRoll.bSecret = true;
	return rRoll;
end

function performRoll(draginfo)
	local rRoll = getRoll();

	ActionsManager.performAction(draginfo, nil, rRoll);
end

function modRoll(rSource, rTarget, rRoll)

	ActionsManager2.encodeAdvantage(rRoll, nAdv);

end

function onResolve(rSource, rTarget, rRoll)
	ActionsManager2.decodeAdvantage(rRoll);

	local nTotal = ActionsManager.total(rRoll);
	local nCat = transformResultInCategory(nTotal);
	local sName = DataCommon.magic_names[nCat];
	rRoll.sDesc = rRoll.sDesc .. sName;

	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	Comm.deliverChatMessage(rMessage);


	notifyApplyWinds(rRoll.node, nTotal);
end

function getCurrentWinds()
	local node = DB.findNode("windsofmagic");
	local nEffects = DB.getValue(node, "result_effects", 0);
	local nMisscast = DB.getValue(node, "result_misscast", 0);
	local nRemaining = DB.getValue(node, "result_remaining", 0);
	return nEffects, nMisscast, nRemaining;
end

function spendedAllSpells()
	if nCurrentTotal > 1 then
		nCurrentTotal = nCurrentTotal - 1;
	end
	setAllValues(nil);
end

function spendSpell()
	local node = DB.findNode("windsofmagic");
	local nRemaining = DB.getValue(node, "result_remaining", 0);
	if nRemaining > 1 then
		DB.setValue(node, "result_remaining", "number", nRemaining - 1);
	else
		spendedAllSpells();
	end
end
