--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onSortCompare(w1, w2)
	local nodeW1 = w1.getDatabaseNode();
	local nodeW2 = w2.getDatabaseNode();

	local sName1 = DB.getValue(nodeW1, "name", ""):lower();
	local sName2 = DB.getValue(nodeW2, "name", ""):lower();
	if sName1 ~= sName2 then
		return sName1 > sName2;
	end
end

function onListChanged()
	update();
end

function update()
	local bEditMode = (window.items_iedit.getValue() == 1);
	for _,w in ipairs(getWindows()) do
		w.idelete.setVisibility(bEditMode);
	end
end

function addEntry(bFocus)
	local w = createWindow();
	if bFocus and w then
		w.name.setFocus();
	end
	return w;
end

function updateItemIcons()
	for _,v in pairs(getWindows()) do
		v.itemtype.update();
	end
end
