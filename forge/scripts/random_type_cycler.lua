--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function update()
  -- Get current type
  local sType = getStringValue();
  local bWeapon = sType == "";
  local bArmor = sType == "forge_item_armor";
  local bPotion = sType == "forge_magicitem_potion";
  local bScroll = sType == "forge_magicitem_scroll";
  local bWand = sType == "forge_magicitem_wand";
  local bRod = sType == "forge_magicitem_rod";
  local bOther = sType == "forge_magicitem_wondrousitem";
  bItem = bWeapon or bArmor or bOther;
  bSpell = bPotion or bScroll or bWand or bRod;

  -- Update visibility
  window.subtype.setVisible(bItem);

  window.number_spells_label.setVisible(bWand or bRod);
  window.number.setVisible(bWand or bRod);

  window.level_label.setVisible(bSpell);
  window.level_min.setVisible(bSpell);
  window.level_minus.setVisible(bSpell);
  window.level_max.setVisible(bSpell);
  window.type_level.setVisible(bSpell);
  window.heightening_label.setVisible(bSpell);
  window.heightening.setVisible(bSpell);

  window.magic_type_label.setVisible(bSpell);
  window.magic_type.setVisible(bSpell);
  window.spell_school_label.setVisible(bSpell);
  window.spell_school.setVisible(bSpell);

  window.spell_type_label.setVisible(bSpell);
  window.spell_type.setVisible(bSpell);
  window.spell_rarity_label.setVisible(bSpell);
  window.spell_rarity.setVisible(bSpell);
  window.exclusive_label.setVisible(bSpell);
  window.exclusive_rarity.setVisible(bSpell);

  window.mult_charges_label.setVisible(bWand or bRod);
  window.mult_charges_min.setVisible(bWand or bRod);
  window.mult_charges_minus.setVisible(bWand or bRod);
  window.mult_charges_max.setVisible(bWand or bRod);
  window.mult_recharge_label.setVisible(bRod);
  window.mult_recharge_min.setVisible(bRod);
  window.mult_recharge_minus.setVisible(bRod);
  window.mult_recharge_max.setVisible(bRod);
end

function onInit()
  if super and super.onInit then
    super.onInit();
  end
  update();
end

function onValueChanged()
  if super and super.onValueChanged then
    super.onValueChanged();
  end
  update();
end
