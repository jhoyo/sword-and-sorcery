--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local FORGE_PATH_BASE_ITEMS = "forge.magicitem.baseitems";
local FORGE_PATH_TEMPLATES = "forge.magicitem.templates";

local RANDOM_PATH_ITEMS = "forge.randomitem.items";
local RANDOM_PATH = "forge.randomitem";

OOB_MSGTYPE_FORGE = "forge";

local items = {};
local templates = {};

-------------------
-- FORGE
-------------------

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_FORGE, handleForge);

	-- ActionsManager.registerModHandler("damage", modDamage);
	-- ActionsManager.registerPostRollHandler("damage", onDamageRoll);
	-- ActionsManager.registerResultHandler("damage", onDamage);
	if User.isHost() then
		local node = DB.findNode(FORGE_PATH_BASE_ITEMS)
		if node then
			node.setPublic(true);
			DB.findNode(FORGE_PATH_TEMPLATES).setPublic(true);
		end
	end
end

function handleForge(msgOOB)
	if msgOOB.sType == "addBaseItem" then
		addBaseItem(msgOOB.sClass, msgOOB.sRecord)
	elseif msgOOB.sType == "addTemplate" then
		addTemplate(msgOOB.sClass, msgOOB.sRecord)
	elseif msgOOB.sType == "forgeMagicItem" then
		forgeMagicItem();
	elseif msgOOB.sType == "reset" then
		reset();
	end
end

function reset()
	if User.isHost() then
		DB.deleteChildren(FORGE_PATH_BASE_ITEMS);
		DB.deleteChildren(FORGE_PATH_TEMPLATES);
	else
		local msgOOB = {};
		msgOOB.type = OOB_MSGTYPE_FORGE;
		msgOOB.sType = "reset";
		Comm.deliverOOBMessage(msgOOB, "");
	end
end

function addBaseItem(sClass, sRecord)
	if User.isHost() then
		local nodeSource = DB.findNode(sRecord);
		if nodeSource then
			local nodeTarget = DB.createChild(FORGE_PATH_BASE_ITEMS);
			copyNode = DB.copyNode(nodeSource, nodeTarget);
			DB.setValue(nodeTarget, "locked", "number", 1);
			DB.setValue(nodeTarget, "refclass", "string", sClass);
		end
	else
		local msgOOB = {};
		msgOOB.type = OOB_MSGTYPE_FORGE;
		msgOOB.sType = "addBaseItem";
		msgOOB.sClass = sClass;
		msgOOB.sRecord = sRecord;
		Comm.deliverOOBMessage(msgOOB, "");
	end
end

function addTemplate(sClass, sRecord)
	if User.isHost() then
		local nodeSource = DB.findNode(sRecord);
		if nodeSource then
			local nodeTarget = DB.createChild(FORGE_PATH_TEMPLATES);
			copyNode = DB.copyNode(nodeSource, nodeTarget);
			DB.setValue(nodeTarget, "locked", "number", 1);
			DB.setValue(nodeTarget, "refclass", "string", sClass);
			DB.setValue(nodeTarget, "refpath", "string", sRecord);
		end
	else
		local msgOOB = {};
		msgOOB.type = OOB_MSGTYPE_FORGE;
		msgOOB.sType = "addTemplate";
		msgOOB.sClass = sClass;
		msgOOB.sRecord = sRecord;
		Comm.deliverOOBMessage(msgOOB, "");
	end
end

function forgeMagicItem(winForge)
	-- Other users
	if not User.isHost() then
		local msgOOB = {};
		msgOOB.type = OOB_MSGTYPE_FORGE;
		msgOOB.sType = "forgeMagicItem";
		Comm.deliverOOBMessage(msgOOB, "");
		return;
	end

	-- Cache item and template nodes
	items = {};
	for _,v in pairs(DB.getChildren(FORGE_PATH_BASE_ITEMS)) do
		table.insert(items, v);
	end
	templates = {};
	for _,v in pairs(DB.getChildren(FORGE_PATH_TEMPLATES)) do
		table.insert(templates, v);
	end

	-- Reset status
	if winForge then
		winForge.status.setValue("");
	end

	-- Validate forging data
	if (#items ~= 0) and (#templates ~= 0) then
		createMagicItem();
		if winForge then
			winForge.statusicon.setStringValue("ok");
			winForge.status.setValue(Interface.getString("forge_item_message_success"));
		end
	else
		if winForge then
			winForge.statusicon.setStringValue("error");
			winForge.status.setValue(Interface.getString("forge_item_message_missing"));
		end
	end

end

function getDisplayType(node)
	local sIcon = "";

	local bMagicItem = (DB.getValue(node, "rarity", "") ~= "");

	local sTypeLower = StringManager.trim(DB.getValue(node, "type", ""):lower());
	if StringManager.contains({"armor", "weapon", "tool"}, sTypeLower) then
		sIcon = sTypeLower;
	elseif StringManager.contains({"mount", "vehicle"}, sTypeLower) then
		bMagicItem = false;
		sIcon = sTypeLower;
	elseif StringManager.contains({"potion", "ring", "rod", "scroll", "staff", "wand"}, sTypeLower) then
		bMagicItem = true;
		sIcon = sTypeLower;
	elseif sTypeLower == "wondrous item" then
		bMagicItem = true;
		sIcon = "wondrousitem";
	elseif DB.getValue(node, "refclass", "") == "reference_spell" or DB.getValue(node, "refclass", "") == "power" then
		bMagicItem = true;
		sIcon = "spell";
	end

	if bMagicItem and (sIcon ~= "") then
		sIcon = "m" .. sIcon;
	end

	return sIcon;
end

function getCompatibilityType(node)
	local sNameLower =  StringManager.trim(DB.getValue(node, "name", "")):lower();
	local sTypeLower = StringManager.trim(DB.getValue(node, "type", "")):lower();
	local sSubTypeLower = StringManager.trim(DB.getValue(node, "subtype", "")):lower();

	if sTypeLower == "armor" then
		return "armor";
	elseif sTypeLower == "weapon" or ((sTypeLower == "adventuring gear") and (sSubTypeLower == "ammunition")) then
		return "weapon";
	elseif StringManager.contains({"rod", "staff", "wand"}, sTypeLower) or ((sTypeLower == "adventuring gear") and (sSubTypeLower == "arcane focus")) then
		return "arcane focus";
	elseif sTypeLower == "ring" then
		return "ring";
	elseif sTypeLower == "potion" or
				((sTypeLower == "adventuring gear") and
				(sNameLower:match("potion") or sNameLower:match("bottle") or sNameLower:match("flask") or
				sNameLower:match("vial") or sNameLower:match("oil"))) then
		return "potion";
	elseif sTypeLower == "scroll" or ((sTypeLower == "adventuring gear") and (sNameLower:match("scroll") or sNameLower:match("paper"))) then
		return "scroll";
	elseif sTypeLower == "wondrous item" then
		return "adventuring gear";
	elseif sTypeLower == "adventuring gear" then
		return "adventuring gear";
	end

	return "";
end

function isCompatible()
	-- TODO, now manual
	-- local sCompatibilityType = nil;
	--
	-- -- Check to make sure templates are all the same type
	-- for _,v in ipairs(templates) do
	-- 	local sTemplateCompatibilityType = getCompatibilityType(v);
	-- 	if sCompatibilityType then
	-- 		if (sCompatibilityType ~= sTemplateCompatibilityType) then
	-- 			return false;
	-- 		end
	-- 	else
	-- 		sCompatibilityType = sTemplateCompatibilityType;
	-- 	end
	-- end
	-- if not sCompatibilityType then
	-- 	return false;
	-- end
	--
	-- -- Check to make sure items are compatible type to the templates
	-- for _,v in ipairs(items) do
	-- 	if sCompatibilityType ~= getCompatibilityType(v) then
	-- 		return false;
	-- 	end
	-- end

	return true;
end


function createMagicItem()

	-- Check
	if (#templates == 0) or (#items == 0) then
		return;
	end

	-- Cycle through each base item to apply templates
	for _,nodeItem in ipairs(items) do
		-- Create new node
		local nodeNew = DB.findNode("item").createChild();
		DB.copyNode(nodeItem, nodeNew);
		nodeNew.setPublic(true);
		local bWeapon, _, _ = ItemManager2.isWeapon(nodeNew);
		local rMultRunes = {};
		local nSpells = 0;
		local nMat = 0;

		-- Add modifications
		for k,nodeMod in ipairs(templates) do
			local sClass = DB.getValue(nodeMod, "refclass", "");

			-- Modification
			if sClass == "itemtemplate" then
				local sType = DB.getValue(nodeMod, "type", "");
				local bRune = sType == "template_rune";
				local bMat = sType == "template_material";
				local sKey = DB.getValue(nodeMod, "features", "");
				local sName = DB.getValue(nodeMod, "name", "");
				DB.setValue(nodeNew, "modified", "number", 1);

				-- Warn multiple materials
				if bMat and nMat == 0 then
					nMat = 1;
				elseif bMat then
					Debug.chat("WARNING: several materials shouldn't be used: ", sName);
					-- ::continue::
				end

				-- Basic features
				local val = DB.getValue(nodeNew, "name", "") .. DB.getValue(nodeMod, "namelabel", "") or (" " .. sName);
				DB.setValue(nodeNew, "name", "string", val);

				local sAux = DB.getValue(nodeNew, "rarity", "");
				val = DataCommon.rarity[sAux] + DB.getValue(nodeMod, "rarity", 0);
				nNew = math.min(DataCommon.rarity_max, math.max(DataCommon.rarity_min, val));
				val = DataCommon.rarity_inv[nNew];
				DB.setValue(nodeNew, "rarity", "string", val);

				-- Physical features
				local sAux = DB.getValue(nodeNew, "cost", "");
				local nMult = DB.getValue(nodeMod, "cost_mult", 0);
				local sAdd = DB.getValue(nodeMod, "cost", "");
				local rCost = ItemManager2.cost_2_array(sAux);
				if bRune then
					local nLevel = DB.getValue(nodeMod, "level", 0);
					sAdd = DataCommon.rune_price[nLevel];
					rAdd = ItemManager2.cost_2_array(sAdd);
					if sKey:find(DataCommon.rune_keywords["cheap"]) then
						rAdd = ItemManager2.multiply_cost(rAdd, DataCommon.rune_prize_mod["cheap"]);
					elseif sKey:find(DataCommon.rune_keywords["rare"]) then
						rAdd = ItemManager2.multiply_cost(rAdd, DataCommon.rune_prize_mod["rare"]);
					elseif sKey:find(DataCommon.rune_keywords["secret"]) then
						rAdd = ItemManager2.multiply_cost(rAdd, DataCommon.rune_prize_mod["secret"]);
					end
					if sKey:find(DataCommon.rune_keywords["multiple"]) then
						if rMultRunes[sName] then
							rAdd = ItemManager2.multiply_cost(rAdd, DataCommon.rune_prize_mod["multiple"]);
						else
							rMultRunes[sName] = true;
						end
					end
					rCost = ItemManager2.add_costs(rCost, rAdd);
				elseif nMult > 0 then
					rCost = ItemManager2.multiply_cost(rCost, nMult);
				elseif sAdd ~= "" then
					rAdd = ItemManager2.cost_2_array(sAdd);
					rCost = ItemManager2.add_costs(rCost, rAdd);
				end
				sAux = ItemManager2.array_2_cost(rCost);
				DB.setValue(nodeNew, "cost", "string", sAux);

				local val = DB.getValue(nodeNew, "weight", 0) * DB.getValue(nodeMod, "weight", 1);
				DB.setValue(nodeNew, "weight", "number", val);

				local val = DB.getValue(nodeNew, "hardness", 0) + DB.getValue(nodeMod, "hardness", 0);
				DB.setValue(nodeNew, "hardness", "number", val);

				local val = DB.getValue(nodeNew, "dents", 0) + DB.getValue(nodeMod, "dents", 0);
				DB.setValue(nodeNew, "dents", "number", val);

				-- Weapons, armor and tools
				local val = DB.getValue(nodeNew, "init", 0) + DB.getValue(nodeMod, "init", 0);
				if not bRune then
					val = math.min(val, 0);
				end
				DB.setValue(nodeNew, "init", "number", val);

				local val = DB.getValue(nodeNew, "bonus", 0) + DB.getValue(nodeMod, "bonus", 0);
				DB.setValue(nodeNew, "bonus", "number", val);

				local val = DB.getValue(nodeNew, "bonus_aux", 0) + DB.getValue(nodeMod, "bonus_aux", 0);
				DB.setValue(nodeNew, "bonus_aux", "number", val);

				local nCat = DB.getValue(nodeMod, "damage_cat", 0);
				local nDice = DB.getValue(nodeMod, "damage_dice", 0);
				local sDamageType = DB.getValue(nodeMod, "damagetype", "");
				local sProp = DB.getValue(nodeMod, "properties", "");
				local attackList = nodeNew.createChild("attacks");
				for _, attack in pairs(attackList.getChildren()) do
					local sDamage = DB.getValue(attack, "damage_attack", "");
					sDamage = modify_damage(sDamage, nCat, nDice, sDamageType, bRune);
					DB.setValue(attack, "damage_attack", "string", sDamage);
					local sProps = DB.getValue(attack, "properties_attack", "");
					if bWeapon and sProp ~= "" then
						DB.setValue(attack, "properties_attack", "string", merge_properties(sProps, sProp));
					end
				end

				if not bWeapon and sProp ~= "" then
					local sProps = DB.getValue(nodeNew, "properties", "")
					DB.setValue(nodeNew, "properties", "string", merge_properties(sProps, sProp));
				end

				local val = DB.getValue(nodeNew, "limit", 3) + DB.getValue(nodeMod, "limit", 0);
				DB.setValue(nodeNew, "limit", "number", val);

				local sProtItem = DB.getValue(nodeNew, "protection", "");
				local sProtMod = DB.getValue(nodeMod, "protection", "");
				local sProt = add_protection(sProtItem, sProtMod);
				DB.setValue(nodeNew, "protection", "string", sProt);

				-- Magic
				if bRune then
					local nEss = tonumber(sKey:lower():match(DataCommon.rune_keywords["essence"]) or "0");
					if nEss > 0 then
						DB.setValue(nodeNew, "infused", "number", 1);
						local nEssence = DB.getValue(nodeNew, "essence", 0);
						DB.setValue(nodeNew, "essence", "number", nEss + nEssence);
					end
				end

				local nodeListMod = nodeMod.createChild("powers");
				local nodeListNew = nodeNew.createChild("powers");
				for _, v in pairs(nodeListMod.getChildren()) do
					local nodeNewItem = nodeListNew.createChild();
					DB.copyNode(v, nodeNewItem);
				end

				local nodeListMod = nodeMod.createChild("effectlist");
				local nodeListNew = nodeNew.createChild("effectlist");
				for _, v in pairs(nodeListMod.getChildren()) do
					local nodeNewItem = nodeListNew.createChild();
					DB.copyNode(v, nodeNewItem);
				end

				-- Description
				local sDesc = DB.getValue(nodeNew, "description", "");
				if k == 1 then
					local sTypeLabel = "template_upgrade";
					if bRune then
						sTypeLabel = "template_rune";
					elseif bMat then
						sTypeLabel = "template_material";
					end
					sDesc = sDesc .. "<h>" .. Interface.getString(sTypeLabel) .. "</h>";
				end
				sDesc = sDesc .. "<p>- " .. sName .. "</p>";
				DB.setValue(nodeNew, "description", "formattedtext", sDesc);

			-- Spell
			else
				-- Get info
				local nodeListNew = nodeNew.createChild("powers");
				local sNameItem = DB.getValue(nodeNew, "name", ""):lower();
				local nLevel = DB.getValue(nodeMod, "level", 0);
				local sNameSpell = DB.getValue(nodeMod, "name", "");
				local sPath = DB.getValue(nodeMod, "refpath", "");
				local sRarity = DB.getValue(nodeMod, "rarity", "");

				local sType = "";
				if sNameItem:find(DataCommon.scroll) then
					sType = "scroll";
				elseif sNameItem:find(DataCommon.potion) then
					sType = "potion";
				elseif sNameItem:find(DataCommon.wand) then
					sType = "wand";
				elseif sNameItem:find(DataCommon.staff) then
					sType = "staff";
				end

				-- Add item and link
				local nodeNewItem = nodeListNew.createChild();
				local nodeLink = nodeNewItem.createChild("link", "windowreference");
				nodeLink.setValue("power", sPath);
				DB.setValue(nodeNewItem, "link_label", "string", sNameSpell);
				DB.setValue(nodeNewItem, "link_identifier", "string", sPath);

				-- Name
				sNameItem = StringManager.capitalize(sNameItem);
				if nSpells == 0 then
					sNameItem = sNameItem .. Interface.getString("of");
				else
					sNameItem = sNameItem .. ", ";
				end
				DB.setValue(nodeNew, "name", "string", sNameItem .. sNameSpell);
				nSpells = nSpells + 1;

				-- Conumabble items features
				if sType == "scroll" or sType == "potion" then
					local sPrize = "";
					local nRarity = DataCommon.rarity_cons[nLevel];
					local nWeight = DataCommon.weight_cons[nLevel];

					if sType == "scroll" then
						sPrize = DataCommon.prize_scroll[nLevel];
					elseif sType == "potion" then
						sPrize = DataCommon.prize_potion[nLevel];
					end

					if sRarity == "spell_rare" then
						local rCost = ItemManager2.cost_2_array(sPrize);
						rCost = ItemManager2.multiply_cost(rCost, DataCommon.prize_rare_mod[sType]);
						sPrize = ItemManager2.array_2_cost(rCost);
						nRarity = nRarity + DataCommon.rarity_rare_mod[sType];
					elseif sRarity == "spell_lost" then
						local rCost = ItemManager2.cost_2_array(sPrize);
						rCost = ItemManager2.multiply_cost(rCost, DataCommon.prize_mythic_mod[sType]);
						sPrize = ItemManager2.array_2_cost(rCost);
						nRarity = nRarity + DataCommon.rarity_mythic_mod[sType];
					end
					nRarity = math.min(DataCommon.rarity_max, math.max(DataCommon.rarity_min, nRarity));

					DB.setValue(nodeNew, "cost", "string", sPrize);
					DB.setValue(nodeNew, "rarity", "string", DataCommon.rarity_inv[nRarity]);
					DB.setValue(nodeNew, "weight", "number", nWeight);
				end

				-- Other stuff
				DB.setValue(nodeNewItem, "charges", "number", DataCommon.forge_charges[sType]);
				DB.setValue(nodeNewItem, "type", "string", DataCommon.forge_recharge[sType]);
			end

		end
	end
end

function merge_properties(sItem, sMod)
	aStrings = StringManager.split(sMod, ",");
	sLower = sItem:lower();
	local snItem, nCount;
	for _, s in pairs(aStrings) do
		local str, sMod = StringManager.trim(s:lower()):match("(%a+) ([+-]?%d+)");
		if str then
			snItem = sLower:match(str .. " ([+-]?%d+)");
		end
		if sMod ~= nil and snItem ~= nil then
			local sOrig = str .. " " .. snItem;
			local sNew = str .. " " .. tostring(tonumber(sMod) + tonumber(snItem));
			sItem, nCount = sItem:gsub(sOrig, sNew);
			if not nCount then
				sItem, nCount = sItem:gsub(StringManager.capitalize(sOrig), sNew);
			end
			if not nCount then
				sItem, nCount = sItem:gsub(sOrig:upper(), sNew);
			end
			if not nCount then
				sItem = sLower:gsub(sOrig, sNew);
			end
		else
			if sItem ~= "" then
				sItem = sItem .. ", ";
			end
			sItem = sItem .. s
		end
	end
	return sItem;
end

function add_protection(sProtItem, sProtMod)
	-- Easy parts
	if sProtItem == "" then
		return "";
	elseif sProtMod == "" then
		return sProtItem;
	end
	-- Extract modify prot
	sProt = "";
	coma = false
	for _, v in pairs(DataCommon.dmgtypes) do
		local nValueMod = tonumber(sProtMod:match("([+-]?%d+) " .. v) or "0");
		local nValueItem = tonumber(sProtItem:match("([+-]?%d+) " .. v) or "0");
		if nValueMod + nValueItem > 0 then
			if coma then
				sProt = sProt .. ", ";
			end
			sProt = sProt .. tostring(nValueMod + nValueItem) .. " " .. v;
			coma = true;
		end
	end
	return sProt;
end




function getItemDescBonus(sDesc)
	local sDescLower = (sDesc or ""):lower();

	local sBonus = sDescLower:match("%+(%d+)%sbonus%sto%sattack");
	if not sBonus then
		sBonus = sDescLower:match("%+(%d+)%sbonus%sto%sthe%sattack");
	end
	if not sBonus then
		sBonus = sDescLower:match("%+(%d+)%sbonus%sto%sac");
	end

	return tonumber(sBonus) or 0;
end


function addMagicItemToCampaign(rMagicItem)
	local nodeTarget;
	if rMagicItem.isTemplate then
		nodeTarget = DB.createdChild("itemtemplate");
	else
		nodeTarget = DB.createChild("item");
	end

	DB.setValue(nodeTarget, "locked", "number", 1);
	DB.setValue(nodeTarget, "name", "string", rMagicItem.sName);
	DB.setValue(nodeTarget, "type", "string", rMagicItem.sType);
	DB.setValue(nodeTarget, "subtype", "string", rMagicItem.sSubType);
	DB.setValue(nodeTarget, "rarity", "string", rMagicItem.sRarity);
	DB.setValue(nodeTarget, "weight", "number", rMagicItem.nWeight);
	DB.setValue(nodeTarget, "cost", "string", rMagicItem.sCost);
	DB.setValue(nodeTarget, "description", "formattedtext", rMagicItem.sDescription);

	local sTypeLower = rMagicItem.sType:lower();
	if sTypeLower == "armor" then
		DB.setValue(nodeTarget, "bonus", "number", rMagicItem.nBonus);
		DB.setValue(nodeTarget, "ac", "number", rMagicItem.nAC);
		DB.setValue(nodeTarget, "dexbonus", "string", rMagicItem.sDexBonus);
		DB.setValue(nodeTarget, "stealth", "string", rMagicItem.sStealth);
		DB.setValue(nodeTarget, "strength", "string", rMagicItem.sStrength);
	elseif sTypeLower == "weapon" then
		DB.setValue(nodeTarget, "bonus", "number", rMagicItem.nBonus);
		DB.setValue(nodeTarget, "damage", "string", rMagicItem.sDamage);
		DB.setValue(nodeTarget, "properties", "string", rMagicItem.sProperties);
	end

	if rMagicItem.isTemplate then
		Interface.openWindow("itemtemplate", nodeTarget);
	else
		Interface.openWindow("item", nodeTarget);
	end
end


----------------
-- RANDOM ITEM
----------------

function reset_random()
	DB.deleteChildren(RANDOM_PATH_ITEMS);
end

function add_to_parcel()
	local nodeList = DB.findNode(RANDOM_PATH_ITEMS);
	local sParcel = DB.getValue(RANDOM_PATH .. ".parcel", "")
	local nodeTargetList = DB.findNode(sParcel .. ".itemlist");

	for _, nodeItem in pairs(nodeList.getChildren()) do
		local nodeSource = nodeTargetList.createChild();
		local sName = DB.getValue(nodeItem, "name", "");
		_ = DB.copyNode(nodeSource, nodeItem);
		DB.setValue(nodeSource, "count", "number", 1);
		DB.setValue(nodeSource, "name", "string", sName);
		DB.setValue(nodeItem, "name", "string", sName);
	end
end

function create_random()
	-- Get type
	local nodeWindow = DB.findNode(RANDOM_PATH);
	local sType = DB.getValue(nodeWindow, "type", "");
	local bWeapon = sType == "";
	local bArmor = sType == "forge_item_armor";
	local bPotion = sType == "forge_magicitem_potion";
	local bScroll = sType == "forge_magicitem_scroll";
	local bWand = sType == "forge_magicitem_wand";
	local bRod = sType == "forge_magicitem_rod";
	local bOther = sType == "forge_magicitem_wondrousitem";
	local bItem = bWeapon or bArmor or bOther;
	local bSpell = bPotion or bScroll or bWand or bRod;

	-- Get info
	local sSubtype = DB.getValue(nodeWindow, "subtype", ""):lower();

	local nNumber = DB.getValue(nodeWindow, "number", 1);
	if bPotion or bScroll then
		nNumber = 1;
	end

	local nLevelMin = DB.getValue(nodeWindow, "level_min", 0);
	local nLevelMax = DB.getValue(nodeWindow, "level_max", 0);
	local sTypeLevel = DB.getValue(nodeWindow, "type_level", "");
	local nHeightening = DB.getValue(nodeWindow, "heightening", 0);
	if nLevelMax == 0 then
		nLevelMax = nLevelMin;
	end
	if bScroll then
		nHeightening = 0;
	end

	local sMagicType = DB.getValue(nodeWindow, "magic_type", "");
	local sSpellSchool = DB.getValue(nodeWindow, "spell_school", ""):lower();
	if sMagicType == "" and not bPotion then
		sMagicType = DataCommon.abilities_gift[math.random(1, 5)];
	end
	if sMagicType ~= "" then
		sMagicType = Interface.getString(sMagicType):lower();
	end

	local sSpellType = DB.getValue(nodeWindow, "spell_type", "");
	local sSpellRarity = DB.getValue(nodeWindow, "spell_rarity", "");
	local nExclusiveRarity = DB.getValue(nodeWindow, "exclusive_rarity", 0);
	-- if sSpellType ~= "" then
	-- 	sSpellType = Interface.getString(sSpellType):lower();
	-- end
	-- if sSpellRarity ~= "" then
	-- 	sSpellRarity = Interface.getString(sSpellRarity):lower();
	-- end

	local nMultChargesMin = DB.getValue(nodeWindow, "mult_charges_min", 1);
	local nMultChargesMax = DB.getValue(nodeWindow, "mult_charges_max", 0);
	local nMultRechargeMin = DB.getValue(nodeWindow, "mult_recharge_min", 1);
	local nMultRechargeMax = DB.getValue(nodeWindow, "mult_recharge_max", 0);
	if nMultChargesMax == 0 then
		nMultChargesMax = nMultChargesMin;
	end
	if nMultRechargeMax == 0 then
		nMultRechargeMax = nMultRechargeMin;
	end

	-- Derived variables
	local aLevelWeights = {};
	local aLevels = {}
	local nLevelMinWeight = nLevelMin;
	if nHeightening == 1 then
		nLevelMinWeight = 0;
	end
	local nProb = 1;
	if sTypeLevel == "random_item_level_equal" then
		for ind = nLevelMinWeight, nLevelMax do
			aLevelWeights[ind] = nProb;
			if ind >= nLevelMin then
				for aux = 1, nProb do
					table.insert(aLevels, ind);
				end
			end
			if ind < nLevelMin then
				nProb = nProb * 2;
			end
		end
	elseif sTypeLevel == "random_item_level_sequential" then
		for ind = nLevelMinWeight, nLevelMax do
			aLevelWeights[ind] = nProb;
			if ind >= nLevelMin then
				for aux = 1, nProb do
					table.insert(aLevels, ind);
				end
			end
			nProb = nProb * 2;
		end
	elseif sTypeLevel == "random_item_level_sequential_down" then
		for ind = nLevelMax, nLevelMinWeight do
			aLevelWeights[ind] = nProb;
			if ind >= nLevelMin then
				for aux = 1, nProb do
					table.insert(aLevels, ind);
				end
			end
			if ind > nLevelMin then
				nProb = nProb * 2;
			else
				nProb = nProb / 2;
			end
		end
	else
		local nCenter = (nLevelMin + nLevelMax) / 2
		for ind = nLevelMinWeight, nLevelMax do
			aLevelWeights[ind] = nProb;
			if ind >= nLevelMin then
				for aux = 1, nProb do
					table.insert(aLevels, ind);
				end
			end
			if ind < nCenter then
				nProb = nProb * 2;
			else
				nProb = nProb / 2;
			end
		end
	end
	local nIndex = math.random(1, #aLevels);
	local nLevelIntended = aLevels[nIndex];

	local aRarities = {
		["spell_lost"] = 0,
		["spell_rare"] = 0,
		["spell_common"] = 0,
		[""] = 0,
	}
	if SpellRarity == "" then
		aRarities["spell_common"] = 1;
		aRarities["spell_rare"] = 1;
		aRarities["spell_lost"] = 1;
		aRarities[""] = 1;
	elseif nExclusiveRarity == 1 or sSpellRarity == "spell_common" then
		aRarities[sSpellRarity] = 1;
	else
		if sSpellRarity == "spell_rare" then
			aRarities["spell_common"] = 2;
			aRarities["spell_rare"] = 1;
		else
			aRarities["spell_common"] = 4;
			aRarities["spell_rare"] = 2;
			aRarities["spell_lost"] = 1;
		end
	end

	local nMultCharges = math.random(nMultChargesMin, nMultChargesMax);
	local nMultRecharge = math.random(nMultRechargeMin, nMultRechargeMax);

	-- Get loaded modules
	local aSpellLists = {"spell"};
	for _,module in pairs(Module.getModules()) do
		local rProps = Module.getModuleInfo(module);
		if rProps.loaded then
			table.insert(aSpellLists, "spell@" .. module);
		end
	end

	-- ACT
	-- Choose randomly attached spells
	local nodeItem = nil;
	local aSpells = {};
	if bSpell then
		-- Make a list with all possible spells
		local aPossibleSpells = {};
		for _, sList in pairs(aSpellLists) do
			local nodeListSpells = DB.findNode(sList);
			if nodeListSpells then
				for _,nodeSpell in pairs(nodeListSpells.getChildren()) do
					-- Get spell relevant data
					local nLevel = DB.getValue(nodeSpell, "level", 0);
					local sMagic = DB.getValue(nodeSpell, "source", ""):lower();
					local sRarity = DB.getValue(nodeSpell, "rarity", ""):lower();
					local sSchool = DB.getValue(nodeSpell, "school", ""):lower();
					local sTypeAux = DB.getValue(nodeSpell, "type_spell", ""):lower();
					local bHeight = false;
					for _, h in pairs(nodeSpell.createChild("heightenings").getChildren()) do
						local sHeight = DB.getValue(h, "leveltag", "");
						if sHeight == "+" or sHeight == "++" then
							bHeight = true;
						end
					end

					-- Filter
					local bCond1 = (nLevel <= nLevelMax) and ((nLevel >= nLevelMin) or ((nHeightening == 1) and bHeight));
					local bCond2 = (sMagicType == "") or sMagic:find(sMagicType);
					local bCond3 = aRarities[sRarity] > 0;
					local bCond4 = (sSpellSchool == "") or sSchool:find(sSpellSchool);
					local bCond5 = (sSpellType == "") or sSpellType == sTypeAux;
					local bInclude = bCond1 and bCond2 and bCond3 and bCond4 and bCond5;
					if (bScroll and (sSpellType == Interface.getString("power_power"))) or (not bScroll and (sSpellType == Interface.getString("power_ritual"))) then
						bInclude = false;
					end

					-- Add spell to list
					if bInclude then
						local nInclude = aLevelWeights[nLevel] * aRarities[sSpellRarity];
						for ind = 1,nInclude do
							table.insert(aPossibleSpells, nodeSpell);
						end
					end
				end
			end
		end

		-- Add spells randomly
		for ind = 1,nNumber do
			local nIndex = math.random(1,#aPossibleSpells);
			table.insert(aSpells, aPossibleSpells[nIndex]);
		end
	end

	-- CREATE OBJECT
	-- Create node
	local nodeList = DB.findNode(RANDOM_PATH_ITEMS);
	local nodeTarget = nodeList.createChild();
	if nodeItem then
		DB.copyNode(nodeItem, nodeTarget);
	end

	-- Modify item
	if bScroll then
		createScroll(nodeTarget, aSpells[1], false, sMagicType);
	elseif bPotion then
		createPotion(nodeTarget, aSpells[1], false, nLevelIntended)
	elseif bWand then
		createWand(nodeTarget, aSpells, false, nLevelIntended, sMagicType, nMultCharges)
	elseif bRod then
		createStaff(nodeTarget, aSpells, false, nLevelIntended, sMagicType, nMultCharges, nMultRecharge)
	end 

end

function addBoundSpells(nodeItem, aSpells, sType, nCharges, nRecharge, sRecharge)	
	local nodeListNew = nodeItem.createChild("powers");
	for _, nodeSpell in pairs(aSpells) do
		-- Get info
		local sNameSpell = DB.getValue(nodeSpell, "name", "");
		local sPath = DB.getPath(nodeSpell);

		-- Act
		local nodeNewSpell = nodeListNew.createChild();
		DB.setValue(nodeNewSpell, "link_label", "string", sNameSpell);
		DB.setValue(nodeNewSpell, "link_identifier", "string", sPath);
		local nodeLink = nodeNewSpell.createChild("link", "windowreference");
		nodeLink.setValue("power", sPath);

		-- Optional
		if sType then
			DB.setValue(nodeNewSpell, "type", "string", sType);
		end
		if nCharges then
			DB.setValue(nodeNewSpell, "charges", "number", nCharges);
		end
		if nRecharge then
			DB.setValue(nodeNewSpell, "recovery", "number", nRecharge);
		end
		if sRecharge then
			DB.setValue(nodeNewSpell, "recovery_period", "string", sRecharge);
		end

	end
end

function createScroll(nodeItem, nodeSpell, bIdentified, sMagic)
	-- Get spell info
	local sNameSpell = DB.getValue(nodeSpell, "name", "");
	local nLevel = DB.getValue(nodeSpell, "level", 0);
	local sRarity = DB.getValue(nodeSpell, "rarity", ""):lower();
	local sSource = DB.getValue(nodeSpell, "source", "");
	local sSpellDesc = DB.getValue(nodeSpell, "description", "");

	-- Add bound spells
	addBoundSpells(nodeItem, {nodeSpell});

	-- Name
	DB.setValue(nodeItem, "refclass", "string", "item");
	local sName = Interface.getString("forge_magicitem_scroll") .. Interface.getString("of") .. sNameSpell;
	DB.setValue(nodeItem, "name", "string", sName);
	if not bIdentified then
		DB.setValue(nodeItem, "isidentified", "number", 0);
		DB.setValue(nodeItem, "nonid_name", "string", Interface.getString("forge_magicitem_scroll") .. " (???)");
	end

	-- Other properties
	DB.setValue(nodeItem, "type", "string", Interface.getString("forge_magicitem_expendable"));
	DB.setValue(nodeItem, "subtype", "string", Interface.getString("forge_magicitem_scroll"));
	DB.setValue(nodeItem, "hardness", "number", 0);
	DB.setValue(nodeItem, "dents", "number", 1);

	local sPrize = DataCommon.prize_scroll[nLevel];
	local nRarity = DataCommon.rarity_cons[nLevel];
	local nWeight = DataCommon.weight_cons[nLevel];
	if sRarity == "spell_rare" then
		local rCost = ItemManager2.cost_2_array(sPrize);
		rCost = ItemManager2.multiply_cost(rCost, DataCommon.prize_rare_mod["scroll"]);
		sPrize = ItemManager2.array_2_cost(rCost);
		nRarity = nRarity + DataCommon.rarity_rare_mod["scroll"];
	elseif sRarity == "spell_lost" then
		local rCost = ItemManager2.cost_2_array(sPrize);
		rCost = ItemManager2.multiply_cost(rCost, DataCommon.prize_mythic_mod["scroll"]);
		sPrize = ItemManager2.array_2_cost(rCost);
		nRarity = nRarity + DataCommon.rarity_mythic_mod["scroll"];
	end
	nRarity = math.min(DataCommon.rarity_max, math.max(DataCommon.rarity_min, nRarity));
	DB.setValue(nodeItem, "cost", "string", sPrize);
	DB.setValue(nodeItem, "rarity", "string", DataCommon.rarity_inv[nRarity]);
	DB.setValue(nodeItem, "weight", "number", nWeight);

	-- Language
	if sMagic and sMagic == "" then
		aMagic = StringManager.split(sSource, ",", true);
		local nIndex = math.random(1, #aMagic);
		sMagic = aMagic[nIndex];
	end
	-- TODO: Substitute languages for Mortis, Druidic, Infernal, etc.

	-- Description
	sDesc = "";
	if sMagic then
		sDesc =  "<h>" .. Interface.getString("char_language") .. "</h><p>" .. sMagic .. "</p>";
	end
	sDesc = sDesc .. "<h>" .. sNameSpell .. "</h>" .. sSpellDesc;
	DB.setValue(nodeItem, "description", "formattedtext", sDesc);
end


function createPotion(nodeItem, nodeSpell, bIdentified, nLevelForge)
	-- Get spell info
	local sNameSpell = DB.getValue(nodeSpell, "name", "");
	local nLevel = DB.getValue(nodeSpell, "level", 0);
	local sRarity = DB.getValue(nodeSpell, "rarity", ""):lower();
	local sSpellDesc = DB.getValue(nodeSpell, "description", "");
	if nLevelForge then
		nLevel = math.max(nLevel, nLevelForge);
	end

	-- Add bound spells
	addBoundSpells(nodeItem, {nodeSpell});

	-- Name
	DB.setValue(nodeItem, "refclass", "string", "item");
	local sName = Interface.getString("forge_magicitem_potion") .. Interface.getString("of") .. sNameSpell;
	if nLevelForge and nLevelForge > nLevel then
		sName = sName .. " (" .. Interface.getString("label_level") .. " " .. tostring(nLevel) .. ")";
	end
	DB.setValue(nodeItem, "name", "string", sName);
	if not bIdentified then
		DB.setValue(nodeItem, "isidentified", "number", 0);
		DB.setValue(nodeItem, "nonid_name", "string", Interface.getString("forge_magicitem_potion") .. " (???)");
	end

	-- Other properties
	DB.setValue(nodeItem, "type", "string", Interface.getString("forge_magicitem_expendable"));
	DB.setValue(nodeItem, "subtype", "string", Interface.getString("forge_magicitem_scroll"));
	DB.setValue(nodeItem, "hardness", "number", 1);
	DB.setValue(nodeItem, "dents", "number", 1);

	local sPrize = DataCommon.prize_potion[nLevel];
	local nRarity = DataCommon.rarity_cons[nLevel];
	local nWeight = DataCommon.weight_cons[nLevel];
	if sRarity == "spell_rare" then
		local rCost = ItemManager2.cost_2_array(sPrize);
		rCost = ItemManager2.multiply_cost(rCost, DataCommon.prize_rare_mod["potion"]);
		sPrize = ItemManager2.array_2_cost(rCost);
		nRarity = nRarity + DataCommon.rarity_rare_mod["potion"];
	elseif sRarity == "spell_lost" then
		local rCost = ItemManager2.cost_2_array(sPrize);
		rCost = ItemManager2.multiply_cost(rCost, DataCommon.prize_mythic_mod["potion"]);
		sPrize = ItemManager2.array_2_cost(rCost);
		nRarity = nRarity + DataCommon.rarity_mythic_mod["potion"];
	end
	nRarity = math.min(DataCommon.rarity_max, math.max(DataCommon.rarity_min, nRarity));
	DB.setValue(nodeItem, "cost", "string", sPrize);
	DB.setValue(nodeItem, "rarity", "string", DataCommon.rarity_inv[nRarity]);
	DB.setValue(nodeItem, "weight", "number", nWeight);

	-- Description
	sDesc = "<h>" .. sNameSpell .. "</h>" .. sSpellDesc;
	DB.setValue(nodeItem, "description", "formattedtext", sDesc);
end


function createWand(nodeItem, aSpells, bIdentified, nLevelForge, sMagic, nMult)
	local nMaxLevel = -1;
	local nMaxRarity = -1;
	local nMaxRarityGlobal = -1;
	local nMaxIndex = 0;
	local aNames = {};
	local aLevels = {};
	local aRarities = {};

	-- Runic language
	sDesc = "";
	if sMagic and sMagic ~= "" then
		sDesc =  "<h>" .. Interface.getString("char_language") .. "</h><p>" .. sMagic .. "</p>";
		-- TODO: Substitute languages for Mortis, Druidic, Infernal, etc.
	end

	-- Add bound spells
	addBoundSpells(nodeItem, aSpells, "item_power_type_charges", DataCommon.forge_charges["wand"] * nMult);

	for k,nodeSpell in pairs(aSpells) do
		-- Get spell info
		local sName = DB.getValue(nodeSpell, "name", "");
		local nLevel = DB.getValue(nodeSpell, "level", 0);
		local sRarity = DB.getValue(nodeSpell, "rarity", ""):lower();
		local sSpellDesc = DB.getValue(nodeSpell, "description", "");
		local nRarity = DataCommon.spell_rarity[sRarity];

		-- Store properties
		table.insert(aNames, sName);
		if nLevel > nMaxLevel or (nLevel == nMaxLevel and nRarity > nMaxRarity) then
			nMaxIndex = k;
			nMaxLevel = nLevel;
			nMaxRarity = nRarity;
		end
		if nRarity > nMaxRarityGlobal then
			nMaxRarityGlobal = nRarity;
		end
		table.insert(aLevels, nLevel);
		table.insert(aRarities, nRarity);

		-- Description
		sDesc = sDesc .. "<h>" .. sName .. "</h>" .. sSpellDesc;
	end
	DB.setValue(nodeItem, "description", "formattedtext", sDesc);

	-- Level
	nMaxLevel = math.max(nMaxLevel, nLevelForge)
	nLevelRune = nMaxLevel + nMult - 1;

	-- Name
	DB.setValue(nodeItem, "refclass", "string", "item");
	sName = Interface.getString("forge_magicitem_wand") .. Interface.getString("of");
	for k,v in pairs(aNames) do
		if k == #aNames then
			sName = sName .. Interface.getString("and");
		elseif k > 1 then
			sName = sName .. ", ";
		end
		sName = sName .. v;
	end
	sName = sName .. Interface.getString("of") .. Interface.getString("level") .. " " .. tostring(nMaxLevel);
	DB.setValue(nodeItem, "name", "string", sName);
	if not bIdentified then
		DB.setValue(nodeItem, "isidentified", "number", 0);
		DB.setValue(nodeItem, "nonidentified", "string", Interface.getString("forge_magicitem_scroll") .. " (???)");
	end

	-- Prize
	local sBasePrize = DataCommon.initial_prize["wand"];
	local nRunes = #aLevels;
	local sRunePrize = "";
	for k,v in pairs(aLevels) do
		if k == nIndex then
			local sPrize = DataCommon.rune_price[nLevelRune];
			sPrize = ItemManager2.multiply_cost(sPrize, DataCommon.rune_prize_mod[aRarities[k]] * DataCommon.rune_prize_mod["cheap"]);
			sRunePrize = ItemManager2.add_costs(sRunePrize, sPrize);
		else
			local sPrize = DataCommon.rune_price[v];
			sPrize = ItemManager2.multiply_cost(sPrize, DataCommon.rune_prize_mod[aRarities[k]] * DataCommon.rune_prize_mod["cheap"] * DataCommon.rune_prize_mod["multiple"]);
			sRunePrize = ItemManager2.add_costs(sRunePrize, sPrize);
		end
	end
	DB.setValue(nodeItem, "cost", "string", ItemManager2.add_costs(sBasePrize, sRunePrize));

	-- Rarity
	nMaxRarity = math.max(2 + math.ceil(nRunes/2) + nMaxRarity, 7);
	nMaxRarity = math.min(nMaxRarity, DataCommon.rarity_cons[nMaxLevel + nMult - 1])
	DB.setValue(nodeItem, "rarity", "string", DataCommon.rarity_inv[nMaxRarity]);

	-- Other
	DB.setValue(nodeItem, "weight", "number", 0.25);
	DB.setValue(nodeItem, "infused", "number", 1);
	DB.setValue(nodeItem, "essence", "number", 1);
	DB.setValue(nodeItem, "dents", "number", 1);
	DB.setValue(nodeItem, "hardness", "number", 3);
end

function createStaff(nodeItem, aSpells, bIdentified, nLevelForge, sMagic, nMult, nMultRecharge)
	local nMaxLevel = -1;
	local nMaxRarity = -1;
	local nMaxRarityGlobal = -1;
	local nMaxIndex = 0;
	local aNames = {};
	local aLevels = {};
	local aRarities = {};

	-- Runic language
	sDesc = "";
	if sMagic and sMagic ~= "" then
		sDesc =  "<h>" .. Interface.getString("char_language") .. "</h><p>" .. sMagic .. "</p>";
		-- TODO: Substitute languages for Mortis, Druidic, Infernal, etc.
	end

	-- Add bound spells
	addBoundSpells(nodeItem, aSpells, "item_power_type_charges", DataCommon.forge_charges["staff"] * nMult, nMultRecharge);

	for k,nodeSpell in pairs(aSpells) do
		-- Get spell info
		local sName = DB.getValue(nodeSpell, "name", "");
		local nLevel = DB.getValue(nodeSpell, "level", 0);
		local sRarity = DB.getValue(nodeSpell, "rarity", ""):lower();
		local sSpellDesc = DB.getValue(nodeSpell, "description", "");
		local nRarity = DataCommon.spell_rarity[sRarity];

		-- Store properties
		table.insert(aNames, sName);
		if nLevel > nMaxLevel or (nLevel == nMaxLevel and nRarity > nMaxRarity) then
			nMaxIndex = k;
			nMaxLevel = nLevel;
			nMaxRarity = nRarity;
		end
		if nRarity > nMaxRarityGlobal then
			nMaxRarityGlobal = nRarity;
		end
		table.insert(aLevels, nLevel);
		table.insert(aRarities, nRarity);

		-- Description
		sDesc = sDesc .. "<h>" .. sName .. "</h>" .. sSpellDesc;
	end
	DB.setValue(nodeItem, "description", "formattedtext", sDesc);

	-- Level
	nMaxLevel = math.max(nMaxLevel, nLevelForge)
	nLevelRune = nMaxLevel + nMult + nMultRecharge - 2;

	-- Name
	DB.setValue(nodeItem, "refclass", "string", "item");
	sName = Interface.getString("forge_magicitem_rod") .. Interface.getString("of");
	for k,v in pairs(aNames) do
		if k == #aNames then
			sName = sName .. Interface.getString("and");
		elseif k > 1 then
			sName = sName .. ", ";
		end
		sName = sName .. v;
	end
	sName = sName .. Interface.getString("of") .. Interface.getString("level") .. " " .. tostring(nMaxLevel);
	DB.setValue(nodeItem, "name", "string", sName);
	if not bIdentified then
		DB.setValue(nodeItem, "isidentified", "number", 0);
		DB.setValue(nodeItem, "nonidentified", "string", Interface.getString("forge_magicitem_scroll") .. " (???)");
	end

	-- Prize
	local sBasePrize = DataCommon.initial_prize["staff"];
	local nRunes = DataCommon.rune_space_per_level[nLevelRune];
	if nRunes > 1 then
		sBasePrize = ItemManager2.multiply_cost(sBasePrize, DataCommon.base_prize_multiplier[nRunes], true);
	end
	local sRunePrize = "";
	for k,v in pairs(aLevels) do
		if k == nIndex then
			local sPrize = DataCommon.rune_price[nLevelRune];
			sPrize = ItemManager2.multiply_cost(sPrize, DataCommon.rune_prize_mod[aRarities[k]]);
			sRunePrize = ItemManager2.add_costs(sRunePrize, sPrize);
		else
			local sPrize = DataCommon.rune_price[v];
			sPrize = ItemManager2.multiply_cost(sPrize, DataCommon.rune_prize_mod[aRarities[k]] * DataCommon.rune_prize_mod["multiple"]);
			sRunePrize = ItemManager2.add_costs(sRunePrize, sPrize);
		end
	end
	DB.setValue(nodeItem, "cost", "string", ItemManager2.add_costs(sBasePrize, sRunePrize));

	-- Rarity
	nMaxRarity = math.max(2 + math.ceil((nRunes + nExtraRunes)/2) + nMaxRarity, 7);
	nMaxRarity = math.min(nMaxRarity, DataCommon.rarity_cons[nMaxLevel + nMult - 1])
	DB.setValue(nodeItem, "rarity", "string", DataCommon.rarity_inv[nMaxRarity]);

	-- Other
	DB.setValue(nodeItem, "weight", "number", 0.4);
	DB.setValue(nodeItem, "infused", "number", 1);
	DB.setValue(nodeItem, "essence", "number", 1);
	DB.setValue(nodeItem, "dents", "number", 2);
	DB.setValue(nodeItem, "hardness", "number", 3);
end
